<?php

/***************************************************************************

*                            Dolphin Smart Community Builder

*                              -----------------

*     begin                : Mon Mar 23 2006

*     copyright            : (C) 2006 BoonEx Group

*     website              : http://www.boonex.com/

* This file is part of Dolphin - Smart Community Builder

*

* Dolphin is free software. This work is licensed under a Creative Commons Attribution 3.0 License. 

* http://creativecommons.org/licenses/by/3.0/

*

* Dolphin is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;

* without even the implied warranty of  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

* See the Creative Commons Attribution 3.0 License for more details. 

* You should have received a copy of the Creative Commons Attribution 3.0 License along with Dolphin, 

* see license.txt file; if not, write to marketing@boonex.com

***************************************************************************/


require_once( '../inc/header.inc.php' );
require_once( BX_DIRECTORY_PATH_INC . 'design.inc.php' );
require_once( BX_DIRECTORY_PATH_INC . 'admin_design.inc.php' );

$logged['admin'] = member_auth( 1 );

$_page['header'] = "Database tools";
$_page['header_text'] = "Manage Database";

$copyright =  '#############################################################################';
$copyright =  '#                            Dolphin Web Community Software';
$copyright =  '#     begin                : Mon Mar 23 2006';
$copyright =  '#     copyright            : (C) 2007 BoonEx Group';
$copyright =  '#     website              : http://www.boonex.com';
$copyright =  '#';
$copyright =  '#############################################################################';
$copyright =  '#############################################################################';
$copyright =  '#';
$copyright =  '#   This is a free software; you can modify it under the terms of BoonEx ';
$copyright =  '#   Product License Agreement published on BoonEx site at http://www.boonex.com/downloads/license.pdf';
$copyright =  '#   You may not however distribute it for free or/and a fee. ';
$copyright =  '#   This notice may not be removed from the source code. You may not also remove any other visible ';
$copyright =  '#   reference and links to BoonEx Group as provided in source code.';
$copyright =  '#';
$copyright =  '#############################################################################';
$copyright .= "# 	Default character set is: ".db_get_encoding()."\n";
$copyright .= "# 	MySQL client info: ".mysql_get_client_info()."\n";
$copyright .= "# 	MySQL host info: ".mysql_get_host_info()."\n";
$copyright .= "# 	MySQL protocol version: ".mysql_get_proto_info()."\n";
$copyright .= "# 	MySQL server version: ".mysql_get_server_info()."\n";
$copyright .= "\n\n\n";

set_time_limit( 36000 );

// backup type dependent data ouput
function output_backup_data( $data )
{
	global $fp;
	global $status_text;

	if ( 'server' == $_POST['savetype'] )
	{
		$fres = fwrite($fp, $data);
		if ( $fres === false )
			$status_text .= "Cannot write to file!\n";
	}
	elseif ( 'client' == $_POST['savetype'] )
	{
		print $data;
	}
	elseif ( 'show' == $_POST['savetype'] )
	{
		print htmlspecialchars($data);
	}
}

// make table or database backup
function make_backup()
{
	// make table back
	if ( "YES" == $_POST['TablesBackup'] )
	{
		if ( "0" == $_POST['tbl_op'] )
		{
			backup_table_structure($_POST['tbl']);
		}
		elseif ( "1" == $_POST['tbl_op'] )
		{
			backup_table_content($_POST['tbl']);
		}
		elseif ( "2" == $_POST['tbl_op'] )
		{
			backup_table($_POST['tbl']);
		}
	}

	// make database backup
	if ( "YES" == $_POST['DatabasesBackup'] )
	{
		if ( "0" == $_POST['db_op'] )
		{
			backup_database_structure($_POST['tbl']);
		}
		elseif ( "1" == $_POST['db_op'] )
		{
			backup_database($_POST['tbl']);
		}
	}
}

$status_text = '';
$db_backup = ("YES" == $_POST['DatabasesBackup']);
$table_backup = ("YES" == $_POST['TablesBackup']);
$table_show_flag = false;
$db_show_flag = false;
if ( !$demo_mode && ($db_backup || $table_backup) )
{
	if ( "server" == $_POST['savetype'] )
	{
		if ( $table_backup )
			$sqlfile = $dir['root'].'backup/'.date("Y-m-d_H-i-s").'_'.$_POST['tbl'].'.sql';
		else
			$sqlfile = $dir['root'].'backup/db_'.date("Y-m-d_H-i-s").'.sql';
		if ( ($fp = @fopen($sqlfile, 'w')) === false )
		{
			$status_text .= "Cannot open file <b>{$sqlfile}</b>!\n";
		}
		else
		{
			output_backup_data( $copyright );

			make_backup();

			$status_text .= "Data succefully dumped into file <b>{$sqlfile}</b>\n";
			fclose($fp);
		}
	}
	else if ( "client" == $_POST['savetype'] )
	{
		$sqlfile = date("Y-m-d_H:i:s").'_'.$_POST['tbl'].'.sql';

		header("Content-Type: text/plain");
		// header("Content-Length: ".filesize($file));
		header("Content-Disposition: attachment;filename=\"".$sqlfile."\"");

		output_backup_data( $copyright );

		make_backup();

		exit();
	}
	else if ( "show" == $_POST['savetype'])
	{
		if ( $table_backup )
			$table_show_flag = true;
		else
			$db_show_flag = true;
	}
}

if (isset($_POST['clearnews']))
{
	if (isset($_POST['newsage']) && $_POST['newsage'] > 0)
	{
		$query = "DELETE FROM NewsFeedTrack 
			WHERE UNIX_TIMESTAMP() - date > {$_POST['newsage']} * 30 * 24 * 60 * 60";
		$res = db_res($query);
		$deletedRows = mysql_affected_rows();
		$deletedRowsPer = (isset($_POST['newscount']) && $_POST['newscount'] != 0) ?
			floor($deletedRows * 100 / $_POST['newscount']) : 0;
	}
}

TopCodeAdmin();
/*
ContentBlockHead("Please Pay Attention");
?>
<b>Attention!!!</b> Before backup anything please read our manual.<br />
You can find it here: <a href="http://www.aewebworks.com/login/howto.php?action=topic&amp;id=44">How To</a>
<?
ContentBlockFoot();
*/
ContentBlockHead("Tables Tools");


if ( strlen($status_text) && $table_backup )
	echo "
<center>
	<div style=\"margin: 6px;\" class=\"err\">{$status_text}</div>
</center>";
?>

<center>
<form style="padding: 0px 0px 0px 0px; margin: 0px 0px 0px 0px" method="post" action="<? echo $_SERVER[PHP_SELF]; ?>">
	<input type="hidden" name="TablesBackup" value="YES" />
	<table width="100%" cellspacing="2" cellpadding="3" class="text">
		<tr class="panel">
			<td colspan="2">&nbsp;<b>Tables backup tools</b></td>
		</tr>
		<tr class="table">
			<td colspan="2" align="center" width="50%">Choose operation and table:</td>
		</tr>
		<tr class="table">
			<td align="right" width="50%">
				<select name="tbl_op">
					<option value="0">Backup structure only</option>
					<option value="1">Backup content only</option>
					<option value="2">Backup structure and content</option>
				</select>
			</td>
			<td align="left" width="50%">
				<select name="tbl">
<?php
$tbls = db_list_tables();
while ($tbl = mysql_fetch_row($tbls))
{
	echo "
					<option value=\"{$tbl['0']}\">{$tbl['0']}</option>";
}

?>
				</select>
			</td>
		</tr>
		<tr class="table">
			<td colspan="2" align="center" width="50%">
				<input type="radio" name="savetype" value="server" id="table_savetype_server" checked="checked" style="vertical-align: middle" /><label for="table_savetype_server">Save to server</label>&nbsp;
				<input type="radio" name="savetype" value="client" id="table_savetype_client" style="vertical-align: middle" /><label for="table_savetype_client">Save to your PC</label>&nbsp;
				<input type="radio" name="savetype" value="show" id="table_savetype_show" style="vertical-align: middle" /><label for="table_savetype_show">Show on the screen</label>
			</td>
		</tr>
	</table>
	<br />
	<input type="submit" value="Backup table" class="no" />
</form>
<br />
</center>

<?php
if ( $table_show_flag )
{
?>
<center>
	<textarea cols="90" rows="30" name="content" style="font-family: 'Courier New' 10px;" readonly="readonly"><? make_backup(); ?></textarea>
</center>
<?
}

ContentBlockFoot();
ContentBlockHead("Databases Tools");


if ( strlen($status_text) && $db_backup )
	echo "
<center>
	<div style=\"margin: 6px;\" class=\"err\">{$status_text}</div>
</center>";
?>

<center>
<form style="padding: 0px 0px 0px 0px; margin: 0px 0px 0px 0px" method="post" action="<? echo $_SERVER[PHP_SELF]; ?>">
	<input type="hidden" name="DatabasesBackup" value="YES" />
	<table width="100%" cellspacing="2" cellpadding="3" class="text">
		<tr class="panel">
			<td colspan="2">&nbsp;<b>Databases backup tools</b></td>
		</tr>
		<tr class="table">
			<td align="right" width="50%">Choose operation:</td>
			<td align="left" width="50%">
				<select name="db_op">
					<option value="0">Backup structure only</option>
					<option value="1">Backup structure and content</option>
				</select>
			</td>
		</tr>
		<tr class="table">
			<td colspan="2" align="center" width="50%">
				<input type="radio" name="savetype" value="server" id="db_savetype_server" checked="checked" style="vertical-align: middle" /><label for="db_savetype_server">Save to server</label>&nbsp;
				<input type="radio" name="savetype" value="client" id="db_savetype_client" style="vertical-align: middle" /><label for="db_savetype_client">Save to your PC</label>&nbsp;
				<input type="radio" name="savetype" value="show" id="db_savetype_show" style="vertical-align: middle" /><label for="db_savetype_show">Show on the screen</label>
			</td>
		</tr>
	</table>
	<br>
	<input type="submit" value="Backup database" class="no" />
</form>
<br />
</center>

<?php
if ( $db_show_flag )
{
?>
<center>
	<textarea cols="90" rows="30" name="content" style="font-family: 'Courier New' 10px;" readonly="readonly"><? make_backup(); ?></textarea>
</center>
<?
}

ContentBlockFoot();

ContentBlockHead("Database Tools");

if ( strlen($status_text) && $db_backup )
	echo "
<center>
	<div style=\"margin: 6px;\" class=\"err\">{$status_text}</div>
</center>";
	
// Get news count from database
$newsCount = db_value("SELECT COUNT(date) FROM NewsFeedTrack");

?>

<center>
<form style="padding: 0px 0px 0px 0px; margin: 0px 0px 0px 0px" method="post" action="<? echo $_SERVER[PHP_SELF]; ?>">
	<? if (isset($deletedRows))	{?>
	<div style="text-align:center; color:green"><? echo "$deletedRows records ($deletedRowsPer%) deleted."; ?></div>
	<? } ?>
	<table width="100%" cellspacing="2" cellpadding="3" class="text">
		<tr class="panel">
			<td colspan="2">&nbsp;<b>News Feed Cleaner</b></td>
		</tr>
		<tr class="table">
			<td colspan="2">Now News Feed table contains <? echo $newsCount; ?> records.</td>
		</tr>
		<tr class="table">
			<td align="right" width="50%">Delete records older then </td>
			<td>
				<select name="newsage">
					<option value="3">3 month</option>
					<option value="6">6 month</option>
					<option value="9">9 month</option>
					<option value="12">1 year</option>
				</select>
			</td>
		</tr>
	</table>
	<br>
	<input type="hidden" name="newscount" value="<? echo $newsCount; ?>"/>
	<input type="submit" name="clearnews" value="Delete" class="no" />
</form>
<br />
</center>

<?php
if ( $db_show_flag )
{
?>
<center>
	<textarea cols="90" rows="30" name="content" style="font-family: 'Courier New' 10px;" readonly="readonly"><? make_backup(); ?></textarea>
</center>
<?
}

ContentBlockFoot();

BottomCode();

function append_back_quotes( &$item, $key )
{
	$item = "`{$item}`";
}

function backup_table_structure($table)
{
	$def .= "CREATE TABLE `$table` (\n";

	$res = db_res("SHOW FIELDS FROM $table");
	while($row = mysql_fetch_assoc($res))
	{
		$def .= "	`{$row['Field']}` {$row['Type']}";
		if ($row['Default'] != "") $def .= " DEFAULT '{$row['Default']}'";
		if ($row['Null'] != "YES") $def .= " NOT NULL";
		if ($row['Extra'] != "") $def .= " {$row['Extra']}";
		$def .= ",\n";
 	}

	$def = ereg_replace(",\n$", "", $def);

	$res = db_res("SHOW KEYS FROM `$table`");
	while($row = mysql_fetch_assoc($res))
	{
		$kname=$row['Key_name'];
		if(($kname != "PRIMARY") && ($row['Non_unique'] == 0)) $kname="UNIQUE|$kname";
		if(!isset($index[$kname])) $index[$kname] = array();
		$index[$kname][] = $row['Column_name'];
	}

	while(list($x, $columns) = @each($index))
	{
		array_walk( $columns, 'append_back_quotes' );
		$def .= ",\n";
		if($x == "PRIMARY")
			$def .= "   PRIMARY KEY (" . implode($columns, ", ") . ")";
		else if (substr($x,0,6) == "UNIQUE")
			$def .= "   UNIQUE ".substr($x,7)." (" . implode($columns, ", ") . ")";
		else
			$def .= "   KEY $x (" . implode($columns, ", ") . ")";
	}

	$def .= "\n);";

	output_backup_data( stripslashes($def) );
}

function backup_table_content($table)
{
	$res = db_res("SELECT * FROM `$table`");
	while($row = mysql_fetch_row($res))
	{
		$insert = "INSERT INTO `$table` VALUES (";

		for($j=0; $j<mysql_num_fields($res);$j++)
		{
			if(!isset($row[$j]))
				$insert .= "NULL,";
			else if($row[$j] != "")
				$insert .= "'".addslashes($row[$j])."',";
			else $insert .= "'',";
		}

		$insert = ereg_replace(",$","",$insert);
		$insert .= ");\n";
		output_backup_data( $insert );
	}
}

function backup_table($table)
{
	backup_table_structure( $table );
	output_backup_data( "\n\n" );
	backup_table_content( $table );
	output_backup_data( "\n\n" );
}

function backup_database_structure()
{
	global $db;

	output_backup_data( "CREATE DATABASE `{$db['db']}`;\n" );
	$tables = db_list_tables();
	$num_tables = @mysql_num_rows( $tables );
	$i = 0;
	while ( $i < $num_tables )
	{
		$table = mysql_tablename( $tables, $i );
		backup_table_structure( $table );
		output_backup_data( "\n\n" );
		$i++;
	}
}

function backup_database_content()
{
	$tables = db_list_tables();
	$num_tables = @mysql_num_rows( $tables );
	$i = 0;
	while ( $i < $num_tables )
	{
		$table = mysql_tablename( $tables, $i );
		backup_table_content( $table );
		output_backup_data( "\n\n" );
		$i++;
	}
}

function backup_database()
{
	global $db;

	output_backup_data( "CREATE DATABASE `{$db['db']}`;\n" );
	$tables = db_list_tables();
	$num_tables = @mysql_num_rows( $tables );
	$i = 0;
	while ( $i < $num_tables )
	{
		$table = mysql_tablename( $tables, $i );
		backup_table_structure( $table );
		output_backup_data( "\n\n" );
		backup_table_content( $table );
		output_backup_data( "\n\n" );
		$i++;
	}
}

?>