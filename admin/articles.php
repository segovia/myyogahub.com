<?

/***************************************************************************
*                            Dolphin Smart Community Builder
*                              -----------------
*     begin                : Mon Mar 23 2006
*     copyright            : (C) 2006 BoonEx Group
*     website              : http://www.boonex.com/
* This file is part of Dolphin - Smart Community Builder
*
* Dolphin is free software. This work is licensed under a Creative Commons Attribution 3.0 License. 
* http://creativecommons.org/licenses/by/3.0/
*
* Dolphin is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
* without even the implied warranty of  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the Creative Commons Attribution 3.0 License for more details. 
* You should have received a copy of the Creative Commons Attribution 3.0 License along with Dolphin, 
* see license.txt file; if not, write to marketing@boonex.com
***************************************************************************/

require_once( '../inc/header.inc.php' );
require_once( BX_DIRECTORY_PATH_INC . 'design.inc.php' );
require_once( BX_DIRECTORY_PATH_INC . 'admin_design.inc.php' );
require_once( BX_DIRECTORY_PATH_INC . 'utils.inc.php' );
require_once( BX_DIRECTORY_PATH_INC . 'articles.inc.php');


//$_page['header'] = "Articles";
$_page['header_text'] = "Manage site articles";
$_page['css_name'] = 'articles.css';

$_page['extraCodeInHead'] = '
	<!-- tinyMCE -->
	<script language="javascript" type="text/javascript" src="' . $site['plugins'] . 'tiny_mce/tiny_mce.js"></script>
	<script language="javascript" type="text/javascript">
	// Notice: The simple theme does not use all options some of them are limited to the advanced theme
	tinyMCE.init({
		mode : "textareas",
		theme : "advanced",
		content_css : "' . $site['base'] . 'css/tiny_mce.css",
		editor_selector : "articl",
		plugins : "table,save,advhr,advimage,advlink,emotions,iespell,insertdatetime,preview,zoom,media,searchreplace,print,contextmenu,paste,directionality,fullscreen",
		theme_advanced_buttons1_add : "fontselect,fontsizeselect",
		theme_advanced_buttons2_add_before: "cut,copy,paste,pastetext,pasteword,separator,search,replace,separator",
		theme_advanced_buttons2_add : "separator,insertdate,inserttime,separator,forecolor,backcolor",
		theme_advanced_buttons3_add_before : "tablecontrols,separator",
		theme_advanced_buttons3_add : "emotions",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_statusbar_location : "bottom",
		theme_advanced_disable : "link,unlink,insertanchor,image,subscript,superscript,help,anchor,code,styleselect",
		plugi2n_insertdate_dateFormat : "%Y-%m-%d",
	    plugi2n_insertdate_timeFormat : "%H:%M:%S",
		paste_use_dialog : false,
		theme_advanced_resizing : false,
		theme_advanced_resize_horizontal : false,
		theme_advanced_link_targets : "_something=My somthing;_something2=My somthing2;_something3=My somthing3;",
		paste_auto_cleanup_on_paste : true,
		paste_convert_headers_to_strong : false,
		paste_strip_class_attributes : "all",
		paste_remove_spans : false,
		paste_remove_styles : false

		});
	</script>
	<!-- /tinyMCE -->

';

$logged['admin'] = member_auth( 1 );

/*echo '<hr>';
print_r($logged);
echo '<hr>';
*/

	if( $_POST['add_category'] )
	{
		$sCategorySubject = process_db_input( $_POST['caption'] );
		$sCategoryDesc = process_db_input( $_POST['description'] );

		$sAddQuery = "INSERT INTO `ArticlesCategory` SET `CategoryName` = '$sCategorySubject', `CategoryDescription` = '$sCategoryDesc';";
		if( db_res( $sAddQuery ) )
		{
			$sActionText = 'Category Added';
		}
		else
		{
			$sActionText = 'Category didn\'t add';
		}
	}
	elseif( $_POST['edit_category'] )
	{
		$sCategorySubject = process_db_input( $_POST['caption'] );
		$sCategoryDesc = process_db_input( $_POST['description'] );
		$iCategoryID = (int)$_POST['categoryID'];

		$sAddQuery = "UPDATE `ArticlesCategory` SET `CategoryName` = '$sCategorySubject', `CategoryDescription` = '$sCategoryDesc' WHERE `CategoryID` = '$iCategoryID' LIMIT 1;";
		if( db_res( $sAddQuery ) )
		{
			$sActionText = 'Category Udated';
		}
		else
		{
			$sActionText = 'Category didn\'t updated';
		}
	}
	elseif( $_POST['add_article'] )
	{
		$sArticleTitle = process_db_input( $_POST['title'] );
		$sArticle = process_db_input( $_POST['article'] );
		$iCategoryID = (int)$_POST['categoryID'];
		if( $_POST['flag'] == 'HTML')
		{
			$sFlag = 'HTML';
		}
		else
		{
			$sFlag = 'Text';
		}


		$sAddQuery = "INSERT INTO `Articles` SET `Title` = '$sArticleTitle', `Text` = '$sArticle', `CategoryID` = '$iCategoryID', `Date` = NOW(), `ArticleFlag` = '$sFlag';";
		if( db_res( $sAddQuery ) )
		{
			$sActionText = 'Article Added';
		}
		else
		{
			$sActionText = 'Article didn\'t added';
		}
	}
	elseif( $_POST['edit_article'] )
	{
		$sArticleTitle = process_db_input( $_POST['title'] );
		$sArticle = process_db_input( $_POST['article'] );
		$iCategoryID = (int)$_POST['categoryID'];
		$iArticleID = (int)$_POST['articleID'];
		if( $_POST['flag'] == 'HTML')
		{
			$sFlag = 'HTML';
		}
		else
		{
			$sFlag = 'Text';
		}


		$sAddQuery = "UPDATE `Articles` SET `Title` = '$sArticleTitle', `Text` = '$sArticle', `CategoryID` = '$iCategoryID', `Date` = NOW(), `ArticleFlag` = '$sFlag' WHERE `ArticlesID` = '$iArticleID';";
		if( db_res( $sAddQuery ) )
		{
			$sActionText = 'Article Upadated';
		}
		else
		{
			$sActionText = 'Article didn\'t Updated';
		}
	}



TopCodeAdmin();
	ContentBlockHead("Articles");
		echo getArticlesAdminContent();
	ContentBlockFoot();
BottomCode();







function getArticlesAdminContent()
{
	global $site;
	global $sActionText;

	$ret = '';

//	$ret .= '<div id="artBlock">' . "\n";

		if (strlen($sActionText)!=0)
		{
			$ret .= '<div class="categoryAction">' . "\n";
				$ret .= $sActionText . "\n";
			$ret .= '</div>' . "\n";
		}
		$ret .= '<div>' . "\n";
		switch ($_GET['action'] )
		{
			case 'addcategory':
				$ret .= getArticlesCategoryEditForm() . "\n";
			break;

			case 'categoryedit':
				$iCategoryID = (int)$_REQUEST['catID'];
				$ret .= getArticlesCategoryEditForm( $iCategoryID ) . "\n";
			break;

			case 'viewcategory':
				$iCategoryID = (int)$_REQUEST['catID'];
				$ret .= getArticlesList( $iCategoryID ) . "\n";
			break;

			case 'viewarticle':
				$iArticleID = (int)$_REQUEST['articleID'];
				$ret .= getArticle( $iArticleID );
			break;

			case 'addarticle':
				$ret .= getArticleEditForm();
			break;

			case 'categorydelete':
				$iCategoryID = (int)$_REQUEST['catID'];
				deleteCategory( $iCategoryID );
				$ret = getArticlesCategiriesList();
			break;

			case 'editarticle':
				$iArticleID = (int)$_REQUEST['articleID'];
				$ret .= getArticleEditForm( $iArticleID );
			break;

			case 'deletearticle':
				$iArticleID = (int)$_REQUEST['articleID'];
				deleteArticle( $iArticleID );
				$ret .= getArticlesCategiriesList();
			break;

			default:
				$ret .= getArticlesCategiriesList() . "\n";
			break;
		}


		$ret .= '</div>' . "\n";
//	$ret .= '</div>' . "\n";


	return $ret;
}

function getArticlesCategoryEditForm( $iCategoryID = '' )
{
	global $site;

	$ret = '';
	//$ret .= print_r($_POST, true) . "\n";

	if( (int)$iCategoryID )
	{
		$sEditCategoryQuery = "
			SELECT
					`CategoryID`,
					`CategoryName`,
					`CategoryDescription`
			FROM
					`ArticlesCategory`
			WHERE
					`CategoryID` = '$iCategoryID'
			LIMIT 1;
		";
		$aCategory = db_arr( $sEditCategoryQuery );
	}

	$ret .= '<div class="navigationLinks">' . "\n";
		$ret .= '<span>' . "\n";
			$ret .= '<a href="articles.php">' . "\n";
				$ret .= 'Articles' . "\n";
			$ret .= '</a>' . "\n";
		$ret .= '</span>' . "\n";
	$ret .= '</div>' . "\n";


	$ret .= '<script type="text/javascript">

			function checkForm()
			{
				var el;
				var hasErr = false;
				var fild = "";

				el = document.getElementById("caption");
				if( el.value.length < 3 )
				{
					el.style.backgroundColor = "pink";
					hasErr = true;
					fild += "\n Category Title ";
				}
				else
				{
					el.style.backgroundColor = "#fff";
				}

				el = document.getElementById("description");
				if( el.value.length < 3 )
				{
					el.style.backgroundColor = "pink";
					hasErr = true;
					fild += "\n Category Description";
				}
				else
				{
					el.style.backgroundColor = "#fff";
				}

				if (hasErr)
				{
					alert( "Please fill next fields first!" + fild )
					return false;

				}
				else
				{
					return true;
				}
			}

	</script>' . "\n";


	$ret .= '<div class="articlesFormBlock">' . "\n";
		$ret .= '<form method="post" action="' . $site['url_admin'] . 'articles.php" onsubmit="return checkForm();">' . "\n";
			$ret .= '<div>' . "\n";
				$ret .= 'Category Caption' . "\n";
			$ret .= '</div>' . "\n";
			$ret .= '<div>' . "\n";
				$ret .= '<input type="text" name="caption" id="caption" class="catCaption" value="' . process_line_output( $aCategory['CategoryName'] ) . '" />' . "\n";
			$ret .= '</div>' . "\n";
			$ret .= '<div>' . "\n";
				$ret .= 'Category Description' . "\n";
			$ret .= '</div>' . "\n";
			$ret .= '<div>' . "\n";
				$ret .= '<textarea name="description"  id="description" class="catDesc">' . process_text_output( $aCategory['CategoryDescription'] ) . '</textarea>' . "\n";
			$ret .= '</div>' . "\n";
			$ret .= '<div>' . "\n";
				$ret .= '<input type="submit" value="Submit">' . "\n";
				if( (int)$iCategoryID )
				{
					$ret .= '<input type="hidden" name="edit_category" value="true" />' . "\n";
					$ret .= '<input type="hidden" name="categoryID" value="' . $iCategoryID . '" />' . "\n";
				}
				else
				{
					$ret .= '<input type="hidden" name="add_category" value="true" />' . "\n";
				}
			$ret .= '</div>' . "\n";
		$ret .= '</form>' . "\n";
	$ret .= '</div>' . "\n";


	return $ret;
}

function getArticleEditForm( $iArticleID = '' )
{
	global $site;
	global $site;

	$rCatories = getArticlesCategiriesList( true );

	if( (int)$iArticleID )
	{
		$articleQuery = "

			SELECT
					`Articles`.`ArticlesID`,
					`Articles`.`CategoryID`,
					`Articles`.`Date`,
					`Articles`.`Title`,
					`Articles`.`Text`,
					`Articles`.`ArticleFlag`,
					`ArticlesCategory`.`CategoryName`
			FROM `Articles`
			INNER JOIN `ArticlesCategory` ON  `ArticlesCategory`.`CategoryID` = `Articles`.`CategoryID`
			WHERE `Articles`.`ArticlesID` = '$iArticleID';
		";
		$aArticle = db_arr( $articleQuery );
	}


	$ret = '';
	$ret .= '<div class="navigationLinks">' . "\n";
		$ret .= '<span>' . "\n";
			$ret .= '<a href="' . $site['url_admin'] . 'articles.php">' . "\n";
				$ret .= 'Articles' . "\n";
			$ret .= '</a>' . "\n";
		$ret .= '</span>' . "\n";
		if( $iArticleID && strlen( $aArticle['CategoryName'] ) )
		{
			$ret .= '<span>' . "\n";
				$ret .= '&gt;' . "\n";
			$ret .= '</span>' . "\n";
			$ret .= '<span>' . "\n";
				$ret .= '<a href="' . $site['url_admin'] . 'articles.php?catID=' . $aArticle['CategoryID'] . '&amp;action=viewcategory">' . "\n";
					$ret .= $aArticle['CategoryName'] . "\n";
				$ret .= '</a>' . "\n";
			$ret .= '</span>' . "\n";
			$ret .= '<span>' . "\n";
				$ret .= '&gt;' . "\n";
			$ret .= '</span>' . "\n";
			$ret .= '<span>' . "\n";
				$ret .= 'Edit Article' . "\n";
			$ret .= '</span>' . "\n";
		}

	$ret .= '</div>' . "\n";


	//$ret .= print_r( $_POST, true );
	$ret .= '<script type="text/javascript">

			function checkForm()
			{
				var el;
				var hasErr = false;
				var fild = "";
				el = document.getElementById("articleTitle");
				if( el.value.length < 3 )
				{
					el.style.backgroundColor = "pink";
					hasErr = true;
					fild += "\n Article Title";
				}
				else
				{
					el.style.backgroundColor = "#fff";
				}

				el = document.getElementById("articleBody");
				if( el.value.length < 3 )
				{
					el.style.backgroundColor = "pink";
					hasErr = true;
					fild += "\n Article text";
				}
				else
				{
					el.style.backgroundColor = "#fff";
				}

				el = document.getElementById("categoryID");
				if( el.value.length < 1 )
				{
					el.style.backgroundColor = "pink";
					hasErr = true;
					fild += "\n Category ";
				}
				else
				{
					el.style.backgroundColor = "#fff";
				}

				el = document.getElementById("flag");
				if( el.value.length < 1 )
				{
					el.style.backgroundColor = "pink";
					hasErr = true;
					fild += "\n Text type ";
				}
				else
				{
					el.style.backgroundColor = "#fff";
				}

				if (hasErr)
				{
					alert( "Please fill next fields first!" + fild )
					return false;

				}
				else
				{
					return true;
				}
			}





	</script>' . "\n";
	$ret .= '<div class="articlesFormBlock">' . "\n";
		$ret .= '<form method="post" action="' . $site['url_admin'] . 'articles.php" onsubmit="return checkForm();">' . "\n";
			$ret .= '<div>' . "\n";
				$ret .= 'Article Title' . "\n";
			$ret .= '</div>' . "\n";
			$ret .= '<div>' . "\n";
				$ret .= '<input type="text" name="title" id="articleTitle" class="catCaption" value="' . process_line_output( $aArticle['Title'] ) . '" />' . "\n";
			$ret .= '</div>' . "\n";
			$ret .= '<div>' . "\n";
				$ret .= 'Article' . "\n";
			$ret .= '</div>' . "\n";
			$ret .= '<div style="margin-bottom:7px;">' . "\n";
				$ret .= '<textarea name="article" id="articleBody"  class="articl">' . $aArticle['Text']  . '</textarea>' . "\n";
			$ret .= '</div>' . "\n";
			$ret .= '<div style="margin-bottom:7px;">' . "\n";
				$ret .= '<select name="categoryID" id="categoryID">' . "\n";
					$ret .= '<option value="">Select Category</option>' . "\n";
					while ( $aCategory = mysql_fetch_assoc( $rCatories ) )
					{
						if( $aArticle['CategoryID'] == $aCategory['CategoryID'] )
						{
							$sSelectedCategory = ' selected="selected"';
						}
						else
						{
							$sSelectedCategory = '';
						}
						$ret .= '<option value="' . $aCategory['CategoryID'] . '"' . $sSelectedCategory . '>' . process_line_output( strmaxtextlen( $aCategory['CategoryName'], 50 ) ) . '</option>' . "\n";
					}
				$ret .= '</select>' . "\n";
			$ret .= '</div>' . "\n";
			$ret .= '<div style="margin-bottom:7px;">' . "\n";
				$textSelected = ( $aArticle['ArticleFlag'] == 'Text' ) ? ' selected="selected"' : '';
				$htmlSelected = ( $aArticle['ArticleFlag'] == 'HTML' ) ? ' selected="selected"' : '';
				$ret .= '<select name="flag" id="flag">' . "\n";
					$ret .= '<option value="">Print As</option>' . "\n";
					$ret .= '<option value="Text"' . $textSelected . '>Text</option>' . "\n";
					$ret .= '<option value="HTML"' . $htmlSelected . '>HTML</option>' . "\n";
				$ret .= '</select>' . "\n";
			$ret .= '</div>' . "\n";
			$ret .= '<div>' . "\n";
				$ret .= '<input type="submit" value="Submit">' . "\n";

				if( (int)$iArticleID )
				{
					$ret .= '<input type="hidden" name="edit_article" value="true" />' . "\n";
					$ret .= '<input type="hidden" name="articleID" value="' . $iArticleID . '" />' . "\n";
				}
				else
				{
					$ret .= '<input type="hidden" name="add_article" value="true" />' . "\n";
				}

			$ret .= '</div>' . "\n";

		$ret .= '</form>' . "\n";
	$ret .= '</div>' . "\n";

	return $ret;
}

function deleteCategory( $iCategoryID )
{
	global $logged;

	if( $logged['admin'] )
	{
		$sCategoryDeleteQuery = "
			DELETE FROM `ArticlesCategory` WHERE `CategoryID` = '$iCategoryID' LIMIT 1;
		";
		$sCategoriesArticlesDeleteQuery = "
			DELETE FROM `Articles` WHERE `CategoryID` = '$iCategoryID';
		";

		if( db_res( $sCategoriesArticlesDeleteQuery ) )
		{
			echo '<div style="color:green; text-align:center;">Articles Deleted Successfully</div>';
		}
		else
		{
			echo '<divstyle="color:red; text-align:center;">Articles are not deleted</div>';
		}

		if( db_res( $sCategoryDeleteQuery ) )
		{
			echo '<div style="color:green; text-align:center;">Category Deleted Successfully</div>';
		}
		else
		{
			echo '<divstyle="color:red; text-align:center;">Category are not deleted</div>';
		}

	}
	else
	{
		return '';
	}
}

function deleteArticle( $iArticleID )
{
	global $logged;

	if( $logged['admin'] )
	{
		$sArticleDeleteQuery = "
			DELETE FROM `Articles` WHERE `ArticlesID` = '$iArticleID';
		";
		if( db_res( $sArticleDeleteQuery ) )
		{
			echo '<div style="color:green; text-align:center;">Article Deleted Successfully</div>';
		}
		else
		{
			echo '<divstyle="color:red; text-align:center;">Article are not deleted</div>';
		}
	}
	else
	{
		return '';
	}

}

?>
