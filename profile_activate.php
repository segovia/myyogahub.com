<?

/***************************************************************************

*                            Dolphin Smart Community Builder

*                              -----------------

*     begin                : Mon Mar 23 2006

*     copyright            : (C) 2006 BoonEx Group

*     website              : http://www.boonex.com/

* This file is part of Dolphin - Smart Community Builder

*

* Dolphin is free software. This work is licensed under a Creative Commons Attribution 3.0 License. 

* http://creativecommons.org/licenses/by/3.0/

*

* Dolphin is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;

* without even the implied warranty of  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

* See the Creative Commons Attribution 3.0 License for more details. 

* You should have received a copy of the Creative Commons Attribution 3.0 License along with Dolphin, 

* see license.txt file; if not, write to marketing@boonex.com

***************************************************************************/


require_once( 'inc/header.inc.php' );
require_once( BX_DIRECTORY_PATH_INC . 'design.inc.php' );
require_once( BX_DIRECTORY_PATH_INC . 'profiles.inc.php' );


// --------------- page variables

$_page['name_index'] 	= 40;
$_page['css_name']		= 'profile_activate.css';

$ID		= (int)$_GET['ConfID'];
$ConfCode	= $_GET['ConfCode'];
$ConfEmail = $_GET['ConfEmail'];

if ( !$ID && !$ConfCode )
	exit;

$logged['member']	= member_auth( 0, false );

$_page['header'] = _t("_Email confirmation");
$_page['header_text'] = _t("_Email confirmation Ex");


// --------------- page components

$_ni = $_page['name_index'];
$_page_cont[$_ni]['page_main_code'] = PageCompPageMainCode();

// --------------- [END] page components

PageCode();

// --------------- page components functions

/**
 * page code function
 */
function PageCompPageMainCode()
{
	global $ID;
	global $ConfCode;
	global $site;
	global $newusernotify;
	global $dir;
	global $p_arr;
	$autoApproval_ifJoin = isAutoApproval('join');

	$p_arr = getProfileInfo( $ID );

	if ( !$p_arr )
	{
		$_page['header'] = _t("_Error");
		$_page['header_text'] = _t("_Profile Not found");
		$ret =  '<table width="100%" cellpadding="4" cellspacing="4"><td align="center" class="text2">';
		$ret .= _t('_Profile Not found Ex');
		$ret.= "</td></table>";
		return $ret;
	}

	ob_start();

	echo '<table width="100%" cellpadding="4" cellspacing="4"><td align="center" class="text2">';

	if ( $p_arr['Status'] == 'Unconfirmed' )
	{
		$ConfCodeReal = base64_encode( base64_encode( md5( $p_arr[Email]) ) );
		if ( strcmp( $ConfCode, $ConfCodeReal ) != 0 )
		{
?>
<b><?= ($_GET['ConfEmail'] == '1') ? _t("_Email confirmation failed") 
	: _t("_Profile activation failed")?></b><br /><br />
<?=_t("_EMAIL_CONF_FAILED_EX")?><br />
<center><form action="<? echo $_SERVER[PHP_SELF]; ?>" method=get>
<input type=hidden name="ConfID" value="<? echo $ID; ?>">
<?	if ($_GET['ConfEmail'] == '1') { ?>
	<input type="hidden" name="ConfirmEmail" value="1"/>
<? } ?>
<table class=text>
    <td><b><?=_t("_Confirmation code")?>:</b> </td>
    <td><input class=no name="ConfCode"></td>
    <td>&nbsp;</td>
    <td><input class=no type="submit" value=" <?=_t("_Submit")?> "></td>
</table>
</form></center>
<?
		}
		else
		{				
			if ( $autoApproval_ifJoin )
				$status = 'Active';
			else
				$status = 'Approval';


			$update = db_res( "UPDATE `Profiles` SET `Status` = '$status' WHERE `ID` = '$ID';" );
			createUserDataFile( $ID );
			reparseObjTags( 'profile', $ID );
			
			// Makes this member logged in after confirmation success
			$memberPassword = db_value("SELECT Password FROM Profiles WHERE ID=$ID LIMIT 1");
			setcookie( "memberID", $ID, time() + 3600, '/' );
			setcookie( "memberPassword", $memberPassword, time() + 3600, '/' );
			$update_res = db_res( "UPDATE `Profiles` SET `LastLoggedIn` = NOW() WHERE `ID` = $ID" );
			$p_arr = getProfileInfo($ID);


			
			// Promotional membership
			if ( getParam('enable_promotion_membership') == 'on' )
			{
				$memership_days = getParam('promotion_membership_days');
				setMembership( $p_arr['ID'], MEMBERSHIP_ID_PROMOTION, $memership_days, true );
			}

if (isset($_GET['ConfEmail']) && $_GET['ConfEmail'] == '1')
	header('location:member.php');
else
	echo ($_GET['ConfirmEmail'] == '1') ? _t('_EMAIL_CONF_SUCCEEDED_SHORT') : _t("_EMAIL_CONF_SUCCEEDED", $ID); 
?>
<br /><br />
<center><a href="member.php"><b><?=_t("_Continue")?> >></b></a></center>
<?
			if ( $newusernotify )
			{
				$realName = (1 < strlen($p_arr['RealName'])) ? $p_arr['RealName'] : $p_arr['NickName'];

				$message	= "New user " . $realName . " with email " . $p_arr['Email'] ." has been confirmed,\n";
				$message	.= "his/her ID is " . $p_arr['ID'] . ".\n";
				$message	.= "--\n";
				$message	.= $site['title'] . " mail delivery system\n";
				$message	.= "<Auto-generated e-mail, please, do not reply>\n";

				$subject	= "New user confirmed";

				sendMail( getParam('notifications_email'), $subject, $message );
			}
		}
	}
	else
		echo ($_GET['ConfEmail'] == '1') ? _t('_EMAIL_ALREADY_CONFIRMED') : _t('_ALREADY_ACTIVATED');

	echo "</td></table>";

	$ret = ob_get_contents();
    ob_end_clean();

	return $ret;
}

?>