<?php

if (!isset($_REQUEST['events']) || 
	$_REQUEST['events'] != 'upcoming' && $_REQUEST['events'] != 'featured')
	exit;

header("Content-type: text/xml; charset=utf-8");

require_once "inc/header.inc.php";
require_once "inc/db.inc.php";
require_once "inc/profiles.inc.php";
require_once "phptal/PHPTAL.php";

define('UPCOMING_EVENTS_FEED_NUMBER', '');
define('FEATURED_EVENTS_FEED_NUMBER', '');

function PureDescription($description)
{
	$description = preg_replace('/<[^>]+>/s', ' ', $description);
	$description = preg_replace('/\s{2,}/s', ' ', $description);
	$description = RemoveHTMLEntities($description);
	return trim($description);
}

try
{

$feedType = (isset($_REQUEST['type']) && $_REQUEST['type'] == 'atom') ? 'atom' : 'rss';
$eventsType = $_REQUEST['events'];

$tal = new PHPTAL("tal/eventfeed-$feedType.tal");

if ($eventsType == 'upcoming')
{
	$limit = (defined('UPCOMING_EVENTS_FEED_NUMBER') && UPCOMING_EVENTS_FEED_NUMBER) ?
		' LIMIT ' . UPCOMING_EVENTS_FEED_NUMBER : '';
	$filter = "`Featured` = '0'";
}
else
{
	$limit = (defined('FEATURED_EVENTS_FEED_NUMBER') && FEATURED_EVENTS_FEED_NUMBER) ?
		' LIMIT ' . FEATURED_EVENTS_FEED_NUMBER : '';
	$filter = "`Featured` = '1'";
}

$eventsQuery = "SELECT * FROM SDatingEvents 
	WHERE $filter AND `EventEnd` > NOW() AND `EventLevel`='1'  
	ORDER BY `EventStart`
	$limit";
$itemsRes = mysql_query($eventsQuery);
if (!$itemsRes)
	throw new Exception('MySQL Error');
	
$dateFormat = ($feedType == 'rss') ? DATE_RFC822 : DATE_ISO8601;

$itemLinkTmplOrig = ($feedType == 'rss') ? "<link>{$site['url']}events.php?action=show_info&amp;event_id=%eventID%</link>" 
	: "{$site['url']}events.php?action=show_info&event_id=%eventID%";
if (useSEF)
	$itemLinkTmpl = ($feedType == 'rss') ? "<link>{$site['url']}%ownerNick%/event/%urltitle%.html</link>" 
		: "{$site['url']}%ownerNick%/event/%urltitle%.html";
else
	$itemLinkTmpl = $itemLinkTmplOrig;

$items = fill_assoc_array($itemsRes);
foreach ($items as &$item)
{
	$item['title'] = $item['Title'];
	$ownerNick = getNickName($item['ResponsibleID']);
	
	if ($item['urltitle'] == '')
		$item['link'] = str_replace('%eventID%', $item['ID'], $itemLinkTmplOrig);
	else
		$item['link'] = str_replace(array('%ownerNick%', '%urltitle%'), array($ownerNick, $item['urltitle']), $itemLinkTmpl);
	
	$rssImage = ($item['RSSImage'] == 1) ? "{$site['sdatingImage']}rss/{$item['ID']}.jpg" : '';
	if ($rssImage)
	{
		$item['enclosure'] = $rssImage;
		$rssImagePath = "{$dir['sdatingImage']}rss/{$item['ID']}.jpg";
		$item['rssimagesize'] = filesize($rssImagePath);
	}
	$fullDescription = PureDescription($item['Description']);
	if (strlen($fullDescription) > 300)
		$description = substr($fullDescription, 0, 300) . '[...]';
	else
		$description = $fullDescription;
	$item['description'] = "<![CDATA[$description]]>";
	$item['fulldesc'] = "<![CDATA[$fullDescription]]>";
	$item['pubDate'] = gmdate($dateFormat, strtotime($item['EventStart']));
	if ($item['exclusive'])
		$item['comments'] = 'Exclusive';
}

// Fills channel array by event attributes
$channel = array();

if ($eventsType == 'upcoming')
{
	$channel['title'] = "Upcoming Events";
	if ($feedType == 'rss')
	{
		$channel['link'] = "<link>{$site['url']}events.php?show_events=upcoming&amp;action=show</link>";
		$channel['description'] = "YogaHub Upcoming Events";
	}
	else 
		$channel['link'] = "{$site['url']}events.php?show_events=upcoming&amp;action=show";
}
else
{
	$channel['title'] = "Featured Events";
	if ($feedType == 'rss')
	{
		$channel['link'] = "<link>{$site['url']}events.php?show_events=upcoming&amp;action=show</link>";
		$channel['description'] = "YogaHub Featured Events";
	}
	else
		$channel['link'] = "{$site['url']}events.php?show_events=featured&amp;action=show";
}
$channel['eventsType'] = $eventsType;

$memcache = $memcacher->obj();
$lastUpdatedTime = $memcache->get('eventsLastUpdatedTime');
if (!$lastUpdatedTime)
	$lastUpdatedTime = time();
$eventsCacheKey = $eventsType . '|' . $lastUpdatedTime;

// Set tal variables
$tal->items = $items;
$tal->channel = $channel;
$tal->eventsCacheKey = $eventsCacheKey;

echo $tal->execute();

}
catch(Exception $e)
{
	echo $e->getMessage();
}

?>