<?

/***************************************************************************
*                            Dolphin Smart Community Builder
*                              -----------------
*     begin                : Mon Mar 23 2006
*     copyright            : (C) 2006 BoonEx Group
*     website              : http://www.boonex.com/
* This file is part of Dolphin - Smart Community Builder
*
* Dolphin is free software. This work is licensed under a Creative Commons Attribution 3.0 License. 
* http://creativecommons.org/licenses/by/3.0/
*
* Dolphin is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
* without even the implied warranty of  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the Creative Commons Attribution 3.0 License for more details. 
* You should have received a copy of the Creative Commons Attribution 3.0 License along with Dolphin, 
* see license.txt file; if not, write to marketing@boonex.com
***************************************************************************/

require_once( 'inc/header.inc.php' );
require_once( BX_DIRECTORY_PATH_INC . 'design.inc.php' );
require_once( BX_DIRECTORY_PATH_INC . 'profiles.inc.php' );

$_page['name_index']	= 44;
$_page['css_name']		= 'cart_pop.css';

$_page['header'] = _t('_Your Shopping Cart');
$_page['header_text'] = _t('_Your Shopping Cart');

// --------------- page variables and login

$logged['member'] = member_auth( 0 );

// --------------- GET/POST actions

$ID = (int)$_COOKIE['memberID'];
$actID = (int)$_GET['ID'];
$action = "";

if ( !$_GET['action'] )
{
	echo '<script language="JavaScript">window.close();</script>';
    exit;
}
else
{
	switch( $_GET['action'] )
	{
    case 'empty':
		$count = 0;
		while ( $_COOKIE["cartentries$ID"][$count] )
		{
		    setcookie( "cartentries$ID" . "[$count]", $_COOKIE["cartentries$ID"][$count++], time() - 3600, '/' );
		}
		$action = _t('_Shopping cart emptied.');
    break;

    case 'add':
		if ( !actID )
		{
		    $action = _t('_No member specified');
		    break;
		}
   		if ( contact_allowed($ID, $actID) )
   		{
   			$action = _t('_MEMBER_ALREADY_CONTACTED');
   			break;
   		}
		$i = 0;
		while ( $_COOKIE["cartentries$ID"][$i] )
		    if ( $_COOKIE["cartentries$ID"][$i++] == $actID )
		    {
				$action = _t('_MEMBER_ALREADY_IN_CART');
				break;
		    }

		if ( !$action )
		{
		    setcookie( "cartentries$ID" . "[" . count( $_COOKIE["cartentries$ID"] ) . "]", $actID, time() + ( 10000 * 3600 ), '/' );
		    $action = _t('_MEMBER_ADDED_TO_CART');
		}
	    break;
	}
}

// --------------- [ END ] GET/POST actions

// --------------- page components

$_ni = $_page['name_index'];
$_page_cont[$_ni]['page_main_code'] = DesignBoxContent($_page['header_text'], PageMainCode(), $oTemplConfig -> PageCartPop_db_num );
$_page_cont[$_ni]['body_onload'] = 'javascript:pause();';

// --------------- [END] page components

PageCode();

// --------------- page components functions

/**
 * Cart page main code
 */
function PageMainCode()
{
	global $action;

	ob_start();

?>

<script type="text/javascript" language="JavaScript">
	<!--
	var amount = 8
	function pause()
	{
		myID = setTimeout( "close_win()", amount * 1000 )
	}
	function close_win()
	{
		window.close()
	}
	//-->
</script>
<center><?= $action ?></center>
<center><?= gp_text() ?></center>

<?

	$ret = ob_get_contents();
	ob_end_clean();

	return $ret;
}

?>