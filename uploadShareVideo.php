<?php



/***************************************************************************

*                            Dolphin Smart Community Builder

*                              -----------------

*     begin                : Mon Mar 23 2006

*     copyright            : (C) 2006 BoonEx Group

*     website              : http://www.boonex.com/

* This file is part of Dolphin - Smart Community Builder

*

* Dolphin is free software. This work is licensed under a Creative Commons Attribution 3.0 License. 

* http://creativecommons.org/licenses/by/3.0/

*

* Dolphin is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;

* without even the implied warranty of  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

* See the Creative Commons Attribution 3.0 License for more details. 

* You should have received a copy of the Creative Commons Attribution 3.0 License along with Dolphin, 

* see license.txt file; if not, write to marketing@boonex.com

***************************************************************************/



require_once('inc/header.inc.php');

require_once( BX_DIRECTORY_PATH_INC . 'design.inc.php' );

require_once( BX_DIRECTORY_PATH_INC . 'admin.inc.php' );

require_once( BX_DIRECTORY_PATH_INC . 'images.inc.php' );

require_once( BX_DIRECTORY_PATH_INC . 'sharing.inc.php' );

require_once( BX_DIRECTORY_PATH_ROOT . 'ray/modules/movie/inc/functions.inc.php' );

require_once( BX_DIRECTORY_PATH_INC . 'videoservices.inc.php' );


$_page['name_index']	= 82;

$_page['css_name']		= 'viewVideo.css';



$_page['extra_js'] = '';



if ( !( $logged['admin'] = member_auth( 1, false ) ) )

{

	if ( !( $logged['member'] = member_auth( 0, false ) ) )

	{

		if ( !( $logged['aff'] = member_auth( 2, false ) ) )

		{

			$logged['moderator'] = member_auth( 3, false );

		}

	}

}





$_page['header'] = _t( "_upload Video" );

$_page['header_text'] = _t("_upload Video");



$_ni = $_page['name_index'];



$member['ID'] = (int)$_COOKIE['memberID'];

checkCommunityPermissions($member['ID']);

$member['Password'] = $_COOKIE['memberPassword'];

define('MAXTITLELENGTH', 255);
define('MAXTAGSLENGTH', 100);
define('MAXDESCLENGTH', 500);

if (isset($_POST['upload']))
{
	$editMode = (isset($_POST['action']) && $_POST['action'] == 'edit');
	
	$postTitle = process_db_input($_POST['title']);
	$postTags = process_db_input($_POST['tags']);
	$postDesc = process_db_input($_POST['description']);
	
	// Check size of title, description and tags
	if (strlen($_POST['title']) > MAXTITLELENGTH)
		$errorMessage = "Sorry, title is very long";
	elseif (strlen($_POST['tags']) > MAXTAGSLENGTH)
		$errorMessage = "Sorry, tags is very long";
	elseif (strlen($_POST['description']) > MAXDESCLENGTH)
		$errorMessage = "Sorry, description is very long";
	else 
	{
		$embedCode = '';
		$type = '';
		$htmlSource = '';
		$errorMessage = ($editMode) ? 'OK' : 'Sorry, this video service is not yet supported.<br>
			An email with request to support this video service was sent to MyYogaHub development team.<br><br>
			<form action='.$_SERVER["PHP_SELF"].' method="post" name="uploadFailedForm">
				<input type="hidden" name="title" value="'.htmlentities($_POST['title']).'"/>
				<input type="hidden" name="tags" value="'.htmlentities($_POST['tags']).'"/>
				<input type="hidden" name="description" value="'.htmlentities($_POST['description']).'"/>
				<input type="hidden" name="uploadfailed" value="1"/>
				<a href="javascript:document.forms.uploadFailedForm.submit();">Try uploading another video.</a>
			</form>';
		if (isset($_POST['embedSource']) && $_POST['embedSource'] != '')
		{
			foreach ($embed as $embedKey => $embedData)
			{
				if (array_key_exists('regexEmbedUrl', $embedData) &&
					(preg_match($embedData['regexEmbedUrl'], $_POST['embedSource'], $embedUrlArr)))
				{
					$_POST['sharingUrl'] =  urldecode($embedUrlArr['embedUrl']);
					break;
				}
				elseif (array_key_exists('regexEmbedCode', $embedData) &&
					(preg_match($embedData['regexEmbedCode'], $_POST['embedSource'], $embedCodeArr)))
				{
					$type = $embedKey;
					$errorMessage = 'OK';
					if (array_key_exists('urlByCode', $embedData))
					{
						$link = str_replace('{embedCode}', $embedCodeArr['embedCode'], $embedData['urlByCode']);
						if ($type == 'yahoovideo')
							$link = str_replace('{embedCode2}', $embedCodeArr['embedCode2'], $link);
						$htmlSource = file_get_contents($link, "r");
					}
					break;
				}
			}
		}
		if (isset($_POST['sharingUrl']) && $_POST['sharingUrl'] != '')
		{
			// add "http://" to sharingUrl if no specified
			if (strpos($_POST['sharingUrl'], 'http://') !== 0)
				$_POST['sharingUrl'] = 'http://'.$_POST['sharingUrl'];
			
			foreach ($embed as $embedKey => $embedData)
				if (strpos($_POST['sharingUrl'], $embedData['regexUrl']) !== false)
				{
					$htmlSource = file_get_contents($_POST['sharingUrl'], "r");
					if ($embedData['getCodeAction']['method'] == 'RegexURL')
					{
						if (preg_match($embedData['getCodeAction']['regex'], $_POST['sharingUrl'], $embedCodeArr))
						{
							$type = $embedKey;
							$errorMessage = 'OK';
							break;
						}
					}
					else // method = RegexPage
					{
						if (preg_match($embedData['getCodeAction']['regex'], $htmlSource, $embedCodeArr))
						{
							$type = $embedKey;
							$errorMessage = 'OK';
							break;
						}
					}
				}
		}
	}
	
	if ($errorMessage != 'OK')
	{
		if( DB_DO_EMAIL_ERROR_REPORT )
		{
			$sMailBody = "Video sharing error\n";
			$sMailBody .= "Called script: {$_SERVER['PHP_SELF']}\n\n";
			$sMailBody .= "Request parameters:\n" . print_r( $_POST['sharingUrl'], true ) . "\n\n";
			$sMailBody .= "--\nAuto-report system\n";
			
			sendMail( $site['bugReportMail'], "Video sharing error", $sMailBody );
		}
		$_page_cont[$_ni]['page_main_code'] = $errorMessage;
	}
	elseif (is_array($embedCodeArr))
	{
		$embedCode = $embedCodeArr['embedCode'];
		$now = time();
		
		// check for the saving same video to database by same user
		$assocType = (isset($_POST['eventID']) && $_POST['eventID'] > 0) ? 'event' : 'no';
		$sameVideoCheckQuery = "SELECT ID FROM `RayMovieFiles` 
			WHERE Owner={$member['ID']} AND Type='$type' AND EmbedId='$embedCode' AND AssocType='$assocType'
			LIMIT 1";
		if (db_value($sameVideoCheckQuery))
			$_page_cont[$_ni]['page_main_code'] = 'You have shared this video already';
		else 
		{
			// Update old video with new embed code
			if ($editMode)
			{
				$eventFields = (isset($_POST['eventID']) && $_POST['eventID'] > 0) ? ", AssocID={$_POST['eventID']}, AssocType='event'" : '';
				$updateVideoQuery = "UPDATE `RayMovieFiles` 
					SET Title = '$postTitle', Description = '$postDesc', Tags = '$postTags',
						Date = $now, Type = '$type', EmbedId = '$embedCode', Permission = {$_POST['videoPerm']}
					WHERE ID = {$_POST['medID']} LIMIT 1";
				$updateResult = (db_res($updateVideoQuery)) ? 1 : 0;
				$lastId = $_POST['medID'];
			}
			else // upload new video
			{
				if (isset($_POST['eventID']) && $_POST['eventID'] > 0)
				{
					$eventFields = ', AssocID, AssocType';
					$eventValues = ", {$_POST['eventID']}, 'event'";
				}
				else 
				{
					$eventFields = '';
					$eventValues = '';
				}
				$urltitle = getSEFUrl($postTitle, sefVideoWords);
				$urltitle = uniqueSEFUrl($urltitle, $member['ID'], 'Owner', 'RayMovieFiles');
				$insertEmbedCodeQuery = "INSERT INTO `RayMovieFiles` 
					(Title, Tags, Description, Date, Owner, Type, EmbedId, Permission, urltitle $eventFields) 
					VALUES ('$postTitle', '$postTags', '$postDesc',	$now, {$member['ID']}, '$type', 
						'$embedCode', {$_POST['videoPerm']}, '$urltitle' $eventValues)";

				if (db_res($insertEmbedCodeQuery))
					$_page_cont[$_ni]['page_main_code'] = 'Video was added successfully';
	
				// get id of the inserted movie
				$lastId = mysql_insert_id();
			}

			// tries get image url and save the image
			$imageUrl = '';
			if ($type == 'youtube')
				$imageUrl = "http://i.ytimg.com/vi/$embedCode/default.jpg";
			elseif ($type == 'acceptabletv')
				$imageUrl = "http://frame.revver.com/frame/156x117/$embedCode.jpg";
			elseif ($type == 'googlevideo' && array_key_exists('regexImageUrl', $embedData) && 
				preg_match($embedData['regexImageUrl'], $htmlSource, $imageUrlArr))
			{
				$hexImageUrl = $imageUrlArr['imageUrl'];
				$imageUrl = ReplaceHexChars($hexImageUrl);
			}
			elseif (array_key_exists('regexImageUrl', $embedData) && 
				preg_match($embedData['regexImageUrl'], $htmlSource, $imageUrlArr))
				$imageUrl = $imageUrlArr['imageUrl'];
			if ($imageUrl != '')
			{
				$imageUrl = str_replace("&amp;", "&", $imageUrl);
				copy($imageUrl, 'ray/modules/movie/files/'. $lastId .'_small.jpg');
			}
			
			if ($editMode)
				header("Location:{$site['url']}viewVideo.php?fileID={$_POST['medID']}&updateResult=$updateResult");
			else
			{
				$videoTitle = (trim($postTitle) == '') ? 'Untitled' : trim($postTitle);
				TrackNewAction(5, $lastId, $member['ID'], $videoTitle, '', "{$lastId}_small.jpg");
			}
		}
	}
	elseif ($editMode) // Update old video without embed code
	{
		$updateVideoQuery = "UPDATE `RayMovieFiles` 
			SET Title = '$postTitle', Description = '$postDesc', Tags = '$postTags', Permission = {$_POST['videoPerm']}
			WHERE ID = {$_POST['medID']} LIMIT 1";
		if (db_res($updateVideoQuery))
			header("Location:{$site['url']}viewVideo.php?fileID={$_POST['medID']}&updateResult=1");
		else 
			header("Location:{$site['url']}viewVideo.php?fileID={$_POST['medID']}&updateResult=0");
	}
}
else 
	$_page_cont[$_ni]['page_main_code'] = PageMainCode();


PageCode();



function PageMainCode()
{
	global $site;
	global $member;
	global $embed;

	// Check is visitor have Unconfirmed status
	$status = db_value("SELECT `Status` FROM `Profiles` WHERE ID={$member['ID']} LIMIT 1");
	$isUnconfirmed = ($status == 'Unconfirmed');	
	
	if ($isUnconfirmed)
		return '<div style="color:red">Sorry, you cannot upload video while your account status is unconfirmed.</div>';
	
	$sCode = '<script language="javascript" type="text/javascript" src="inc/js/videoservices.js"></script>';
	
	$sCode .= '<script>
	
		var embed = new Array();';
		$i = 0;
		foreach ($embed as $embedKey => $embedData)
		{
			$regexUrl = $embed[$embedKey]['regexUrl'];
			$regexCorrectEmbed = $embed[$embedKey]['regexCorrectEmbed'];
			$embedName = $embed[$embedKey]['name'];
			$sCode .= "embed[$i] = new Array(3);";
			$sCode .= "embed[$i]['regexUrl'] = '$regexUrl';";
			$sCode .= "embed[$i]['regexCorrectEmbed'] = '$regexCorrectEmbed';";
			$sCode .= "embed[$i]['name'] = '$embedName';";
			$i++;
		}

	$sCode .= '</script>';
	
	$sCode .= '<div id="agreement" style="text-align: center;"><div style="font-weight: bold;">'.
		_t("_Media upload Agreement",_t("_Video")).'</div><div><textarea rows="20" cols="80" readonly="true">'.
		_t("_License Agreement",$site['url']).'</textarea></div><div><input type="submit" id="agree" value="'.
		_t("_I agree").'" onclick="document.getElementById(\'uploadForm\').style.display = \'block\'; 
		document.getElementById(\'agreement\').style.display = \'none\';""></div></div>';

	if ($_POST['uploadfailed'])
	{
		$postTitle = htmlentities($_POST['title']);
		$postTags = htmlentities($_POST['tags']);
		$postDesc = htmlentities($_POST['description']);
	}
		
	//$sCode .= '<div id="uploadForm" style="text-align: center;display:none;">'.getApplicationContent('movie','editor',array('id' => $member['ID'],'password'=>$member['Password'])).'</div>';
	$sCode .= "<form enctype=\"multipart/form-data\" name=\"uploadVideoForm\" method=\"post\" action=\"{$_SERVER['PHP_SELF']}\">";
	$sCode .= '<div id="uploadForm" class="uploadForm">';
	
	$sCode .= '<div style="float:left; position:relative; left:570px"><b>Video Permissions</b><div style="margin-top:5px">
		<input type="radio" name="videoPerm" value="0" checked/>public<br/>
		<input type="radio" name="videoPerm" value="1"/>friends only
		</div></div>';	
	
	$sCode .= '<table style="position:relative; right:100px">';

	$sCode .= '<tr><td class="uploadText"><span style="color:red">*</span> '. _t("_Title").
		': </td><td><input type="text" name="title" class="uploadInput" value="'.$postTitle.'"
		onkeyup="CheckSubmitEnabled()" maxlength="' . MAXTITLELENGTH . '"/></td>';
	$sCode .= '<tr><td class="uploadText">'. _t("_Description").
		': </td><td colspan="2"><textarea name="description" class="uploadInput" 
		onKeyUp=\'return UpdateMaxDescChars('.MAXDESCLENGTH.');\'>'.$postDesc.'</textarea></td></tr>
		<tr><td/><td style="text-align:center"><div id="descCounter" class="uploadInput"><span>'. 
		MAXDESCLENGTH .	'</span> chars left</div></td></tr>';
	$sCode .= '<tr><td class="uploadText">'. _t("_Tags").
		': </td><td colspan="2"><input type="text" name="tags" class="uploadInput" value="'.$postTags.'"/></td></tr>';
	$sCode .= '<tr><td class="uploadText" maxlength="'. MAXTAGSLENGTH .'">Enter the url to the video: '.
		'<input type="radio" name="shareType" value="shareTypeUrl" onclick="ToggleShareType(this.value)" checked/></td>'.
		'<td><input type="text" name="sharingUrl" class="uploadInput" onkeyup="CheckSubmitEnabled()"/></td>'.
		'<td style="vertical-align:top"><span id="urlErrorMsg"></span></td></tr>';
	$sCode .= '<tr><td class="uploadText">or specify embed source: ' .
		'<input type="radio" name="shareType" value="shareTypeEmbed" onclick="ToggleShareType(this.value)"/></td>'.
		'<td><textarea name="embedSource" class="uploadInput" onkeyup="CheckSubmitEnabled()" disabled></textarea></td>'.
		'<td style="vertical-align:top"><span id="embedErrorMsg"></span></td></tr>';	
	$sCode .= '<tr><td/><td colspan="2" style="height:25px; vertical-align: top">'.
		'<span style="color:red">*</span> - required fields</td></tr>';
	$sCode .= '<input type="hidden" name="eventID" value="'.$_GET['eventID'].'"/>';
	$sCode .= '<tr><td/><td style="text-align:center"><input type="submit" name="upload" value="Submit Video" disabled/></td><td/></tr>';	

	$sCode .= '</table>';
	$sCode .= '<input type="hidden" name="medProfId" value="'.$member['ID'].'"/>';
	$sCode .= '</form>';
	
	if (isset($_POST['uploadfailed']))
		$sCode .= "<script>document.getElementById('agreement').style.display = 'none';
			document.getElementById('uploadForm').style.display = 'block'</script>";
	
	$sCode .= '<script>CheckSubmitEnabled(); ToggleShareType(null); UpdateMaxDescChars('.MAXDESCLENGTH.');</script>';
	
	return $sCode;
}



?>
