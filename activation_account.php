<?

require_once( 'inc/header.inc.php' );
require_once( BX_DIRECTORY_PATH_INC . 'design.inc.php' );
require_once( BX_DIRECTORY_PATH_INC . 'profiles.inc.php' );

// --------------- page variables and login

$_page['name_index']	= 35;
$logged['member'] = member_auth(0);

$_page['header'] = _t( "Account Activation" );
$_page['header_text'] = _t( "Community Account Activation" );

// --------------- page components

$_ni = $_page['name_index'];
$_page_cont[$_ni]['page_main_code'] = PageCompPageMainCode();

// --------------- [END] page components

PageCode();

// --------------- page components functions

/**
 * page code function
 */
function PageCompPageMainCode()
{
	global $su_config;
	
	$memberID = (int)$_COOKIE['memberID'];
	$p_arr = getProfileInfo( $memberID );

	$res = _t( "_ACCOUNT_CONF_SUCCEEDED", $memberID);
	if (isset($_REQUEST['referer']))
	{
		$referer = urldecode($_REQUEST['referer']);
		$res .= "<br/><a href=\"$referer\">Continue what you were previously doing.</a>";
	}
	
	return $res;
}
?>