<?php

require_once(BX_DIRECTORY_PATH_CLASSES . 'BxDolMedia.php');

class UploadEventPhoto extends BxDolMedia
{
	function UploadEventPhoto($iProfileID, $iEventID)
	{
		$this->sMediaType = 'eventphoto';
		$this->iProfileID = $iProfileID; 
		$this->iEventID = $iEventID;
		parent::BxDolMedia();
		
		// Get event owner id
		$this->iEventOwnerID = db_value("SELECT ResponsibleID FROM SDatingEvents WHERE ID=$iEventID LIMIT 1");
		
		// Override media dir and url with event owner id in path
		$this->sMediaDir = $this->aMediaConfig['dir'][$this->sMediaType] . $this->iEventOwnerID . '/';
		$this->sMediaUrl = $this->aMediaConfig['url'][$this->sMediaType] . $this->iEventOwnerID . '/'; 

		$aMember = getProfileInfo($this->iProfileID);
		$sSex = $aMember['Sex'];
		if($sSex == 'male')
		{
			$this->sSexIcon = 'man_small.gif';
			$this->sSexPic = 'man_big.gif';
		}
		else
		{
			$this->sSexIcon = 'woman_small.gif';
			$this->sSexPic = 'woman_big.gif';
		}
	}

	function getMediaPage($iMediaID = '')
	{
		global $site, $tmpl;

		$ret = '';
		$bShowMenu = false;
		$bWithoutJS = false;

		if($this->iMediaCount > 0)
		{
			$iMediaID = ($iMediaID > 0) ? $iMediaID : $this->aMedia['0']['PrimPhoto'];
			$aCurPhoto = $this->getElementArrayByID($iMediaID);
			if(empty($aCurPhoto))
			{
				$sPhotoUrl = $site['url']."templates/tmpl_{$tmpl}/images/icons/group_no_pic.gif";
				$bShowMenu = false;
				$bWithoutJS = true;
			}
			else
			{
				$ret .= '<div class="mediaTitle" id="sTitleDiv">';
				$ret .= process_line_output($aCurPhoto['med_title']);
				$ret .= '</div>';
				$sPhotoUrl = $this->sMediaUrl . 'photo_' . $aCurPhoto['med_file'];
				$iPhotoRating = $this->getRating($aCurPhoto['med_rating_sum'], $aCurPhoto['med_rating_count']);
				$bShowMenu = true;
			}
		}
		else
		{
			$sPhotoUrl = $site['url']."templates/tmpl_{$tmpl}/images/icons/group_no_pic.gif";
			$bShowMenu = false;
		}

		$ret .= $this->getJSCode($aCurPhoto);

		$ret .= '<div class="ratingParentBlock">';
		$ret .= '<div class="ratingBlock" style="height:' . $this->aMediaConfig['size']['photoHeight'] . 'px; width:10px;">';
		$ret .= '<div class="ratingInner" id="sPhotoRatingDiv" style="height:' . $iPhotoRating . '%;">';
		$ret .= '<div class="ratingTextBlock">';
		$ret .= '<div id="sRatingTextDiv" style="height:0%;">';
		
		$iPhotoRatingSum = ($aCurPhoto['med_rating_sum'] > 0) ? $aCurPhoto['med_rating_sum'] : 0 ;
		$iPhotoRatingCount = ($aCurPhoto['med_rating_count'] > 0) ? $aCurPhoto['med_rating_count'] : 0;

		$ret .= _t('_votes') . '&nbsp;' . $iPhotoRatingCount . '<br>';
		$ret .= _t('_ratio') . '&nbsp;' . $iPhotoRatingSum;
		$ret .= '</div>' . "\n";
		$ret .= '</div>' . "\n";
		$ret .= '</div>' . "\n";
		$ret .= '</div>' . "\n";
		$ret .= '</div>' . "\n";

		$ret .= '<div class="photoBlock" id="photoKeeper">';
		$ret .= '<img src="' . getTemplateIcon('spacer.gif') . '" style="width:' . 
			$this->aMediaConfig['size']['photoWidth'] . 'px; height:' . $this->aMediaConfig['size']['photoHeight'] . 
			'px; background-image:url(' . $sPhotoUrl . ');" class="photo" id="temPhotoID" alt="" />';
		$ret .= '</div>';

		if($bShowMenu)
		{
			$ret .= '<div class="photoMenu">';
			$ret .= '<form action="' . $_SERVER['PHP_SELF'] . '" method="post" name="actionForm">';
			$ret .= '<input type="submit" name="deletePhoto" value="' . _t('_Delete') . '" onclick="return confirm(\'' . _t('_are you sure?') . '\');" />';
			$ret .= '<input type="submit" name="makePrim" value="' . _t('_Make Primary') . '" />';
			$ret .= '<input type="hidden" id="photoID" name="photoID" value="' . $aCurPhoto['med_id'] . '" />';
			$ret .= '<input type="hidden" name="eventID" value="' . $this->iEventID . '" />';
			$ret .= '<input type="hidden" name="show" value="eventphoto"/>';
			$ret .= '</form>';
			$ret .= '</div>';
		}

		$ret .= '<div class="clear_both"></div>';
		$ret .= '<div class="iconBlock">';

		if($bWithoutJS)
			$ret .= $this->getIconsList();
		else
			$ret .= $this->_getIconsList();
			
		$ret .= '</div>';

		return $ret;
	}

	function getJSCode($aCurPhoto)
	{
		$ret = '';

		$ret .= '<script type="text/javascript">
			function setImage()
			{
				var imgCode;
				var oOldImg = document.getElementById("temPhotoID");
				oOldImg.style.backgroundImage = "url(' . $this->sMediaUrl . 'photo_' . $aCurPhoto['med_file'] . ')";
				return false;
			}

			function changePhoto(sFile, iMediaID, sTitle, iRate, rateSum, rateCount)
			{
				var oOldImg = document.getElementById("temPhotoID");
				oOldImg.style.backgroundImage = "url(' . $this->sMediaUrl . 'photo_"+sFile+")";
				changeTitle(sTitle);
				changeRate(iRate, rateSum, rateCount);
				changeMediaID(iMediaID);
				return false;
			}

			function changeTitle(sTitle)
			{
				var oTitleDiv = document.getElementById("sTitleDiv");
				oTitleDiv.innerHTML = stripSlashes(sTitle);
			}

			function changeRate(iRate, rateSum, rateCount)
			{
				var oRateDiv = document.getElementById("sPhotoRatingDiv");
				var oRateTextDiv = document.getElementById("sRatingTextDiv");

				oRateDiv.style.position = "relative";
				oRateDiv.style.height = iRate + "%";
				oRateDiv.style.top =  100 - iRate + "%";

				oRateTextDiv.innerHTML = "' . _t('_votes') . ' "+rateCount+"<br>' . _t('_ratio') . ' "+rateSum;
			}

			function changeMediaID(iMediaID)
			{
				var oPhotoID = document.getElementById("photoID");
				oPhotoID.value = iMediaID;
			}
		</script>';

		return $ret;
	}

	function _getIconsList()
	{
		global $site, $tmpl;
		
		for($i = 0; $i < $this->aMediaConfig['max']['eventphoto']; $i++)
		{
			$sIconSrc = $this->sMediaDir . 'icon_' . $this->aMedia[$i]['med_file'];
			if(extFileExists($sIconSrc))
			{
				$iPhotoRatingSum = ($this->aMedia[$i]['med_rating_sum'] > 0) ? $this->aMedia[$i]['med_rating_sum'] : 0 ;
				$iPhotoRatingCount = ($this->aMedia[$i]['med_rating_count'] > 0) ? $this->aMedia[$i]['med_rating_count'] : 0 ;
				$iPhotoRating = $this->getRating($iPhotoRatingSum, $iPhotoRatingCount);

				$sIconUrl = $this->sMediaUrl . 'icon_' . $this->aMedia[$i]['med_file'];																																																																								//function changePhoto(sFile, imgDiv, sTitle, titleDiv, iRate, rateDiv)
				$atrib = '
					\'' . $this->aMedia[$i]['med_file']  . '\',
					' . $this->aMedia[$i]['med_id'] . ',
					\'' . process_line_output(addslashes($this->aMedia[$i]['med_title'])) . '\',
					' . $iPhotoRating . ',
					' . $iPhotoRatingSum .  ',
					'  . $iPhotoRatingCount . '
				';

				$ret .= '<img src="' . getTemplateIcon('spacer.gif') . '" style="width:' . 
					$this->aMediaConfig['size']['iconWidth'] . 'px; height:' . $this->aMediaConfig['size']['iconHeight'] . 
					'px; background-image:url(' . $sIconUrl . '); cursor:pointer;"  alt="" class="icons" onmouseover="this.className=\'iconsHover\'" onmouseout="this.className=\'icons\'" onclick="return changePhoto(' . $atrib . ');" />';
			}
			else
			{
				$sIconUrl = $site['url']."templates/tmpl_{$tmpl}/images/icons/group_no_pic.gif";
				$ret .= '<img src="' . getTemplateIcon('spacer.gif') . '" style="width:' . $this->aMediaConfig['size']['iconWidth'] . 'px; height:' . $this->aMediaConfig['size']['iconHeight'] . 'px; background-image:url(' . $sIconUrl . ');" alt="" class="icons" />';
			}
		}

		return $ret;
	}

	function getIconsList()
	{
		global $site, $tmpl;
		
		$ret = '';

		for($i = 0; $i < $this->aMediaConfig['max']['eventphoto']; $i++)
		{
			$sIconSrc = $this->sMediaDir . 'icon_' . $this->aMedia[$i]['med_file'];
			if(extFileExists($sIconSrc))
			{
				$sIconUrl = $this->sMediaUrl . 'icon_' . $this->aMedia[$i]['med_file'];
				$ret .= '<a href="' . $this->aMediaConfig['url']['media'] . 
					'?show=eventphoto&eventID='. $this->iEventID .'&photoID=' . $this->aMedia[$i]['med_id'] . '">';
				$ret .= '<img src="' . getTemplateIcon('spacer.gif') . '" style="width:' . $this->aMediaConfig['size']['iconWidth'] . 'px; height:' . $this->aMediaConfig['size']['iconHeight'] . 'px; background-image:url(' . $sIconUrl . '); cursor:pointer;"  alt="" class="icons" onmouseover="this.className=\'iconsHover\'" onmouseout="this.className=\'icons\'" />';
				$ret .= '</a>';
			}
			else
			{
				$sIconUrl = $site['url']."templates/tmpl_{$tmpl}/images/icons/group_no_pic.gif";
				$ret .= '<img src="' . getTemplateIcon('spacer.gif') . '" style="width:' . $this->aMediaConfig['size']['iconWidth'] . 'px; height:' . $this->aMediaConfig['size']['iconHeight'] . 'px; background-image:url(' . $sIconUrl . ');" alt="" class="icons" />';
			}
		}

		return $ret;
	}

	function validateMediaArray($aMedia)
	{
		foreach($aMedia as $iKey => $aValue)
		{
			$sIconSrc = $this->sMediaDir . 'icon_' . $aValue['med_file'];
			$sThumbSrc = $this->sMediaDir . 'thumb_' . $aValue['med_file'];
			$sPhotoSrc = $this->sMediaDir . 'photo_' . $aValue['med_file'];
			if(!extFileExists($sIconSrc) || !extFileExists($sThumbSrc) || !extFileExists($sPhotoSrc))
			{
				if($aValue['med_id'] == $aValue['PrimPhoto'])
					$this->oMediaQuery->resetEventPrimPhoto($this->iEventID);
				unset($aMedia[$iKey]);
			}
		}

		return array_values($aMedia);
	}

	function uploadMedia()
	{
		global $dir;

		$sMediaDir = $this->getProfileMediaDir();
		if(!$sMediaDir)	
			return false;

		$sFileName = time();

		$ext = moveUploadedImage($_FILES, 'eventphoto', $sMediaDir . $sFileName, $this->aMediaConfig['max']['photoFile'], false);

		if (getParam('enable_watermark') == 'on')
		{
			$iTransparent = getParam('transparent1');
			$sWaterMark = $dir['profileImage'] . getParam('Water_Mark');

			if (strlen(getParam('Water_Mark')) && file_exists($sWaterMark))
			{
				$sFile = $sMediaDir . $sFileName . $ext;
				applyWatermark($sFile, $sFile, $sWaterMark, $iTransparent);
			}
		}

		if(strlen($ext) && !(int)$ext)
		{
			imageResize($sMediaDir . $sFileName . $ext, $sMediaDir . 'icon_' . $sFileName . $ext, $this->aMediaConfig['size']['iconWidth'], $this->aMediaConfig['size']['iconHeight'], true);
			imageResize($sMediaDir . $sFileName . $ext, $sMediaDir . 'thumb_' . $sFileName . $ext, $this->aMediaConfig['size']['thumbWidth'], $this->aMediaConfig['size']['thumbHeight'], true);
			imageResize($sMediaDir . $sFileName . $ext, $sMediaDir . 'photo_' . $sFileName . $ext, $this->aMediaConfig['size']['photoWidth'], $this->aMediaConfig['size']['photoHeight'], true);

			$this->insertMediaToDb($sFileName . $ext);

			if($this->iMediaCount == 0 || $this->aMedia['0']['PrimPhoto'] == 0 )
			{
				$iLastID = mysql_insert_id();
				$this->oMediaQuery->setEventPrimaryPhoto($this->iEventID, $iLastID);
			}
			@unlink($sMediaDir . $sFileName . $ext);
		}
	}

	function deleteMedia($iPhotoID)
	{
		global $dir;
		
		// Get media filename and event id for this photo
		$mediaPhotoInfo = db_arr("SELECT med_file, med_prof_id FROM media WHERE med_id = $iPhotoID LIMIT 1");

		// Get event photo filename in events table
		$eventPhotoFilename = db_value("SELECT PhotoFilename FROM SDatingEvents WHERE ID={$mediaPhotoInfo['med_prof_id']} LIMIT 1");

		// Check is deleted photo equal event photo
		if (strpos($eventPhotoFilename, substr($mediaPhotoInfo['med_file'], 0, 10)) !== false)
		{
			$sIconSrc = $dir['sdatingImage'] . $eventPhotoFilename;
			$sThumbSrc = $dir['sdatingImage'] . 'thumb_' . $eventPhotoFilename;
			$sPhotoSrc = $dir['sdatingImage'] . 'icon_' . $eventPhotoFilename;
			
			@unlink($sIconSrc);
			@unlink($sThumbSrc);
			@unlink($sPhotoSrc);
			
			$res = db_res("UPDATE SDatingEvents SET PhotoFilename = '' WHERE ID={$mediaPhotoInfo['med_prof_id']} LIMIT 1");
		}
		
		// Get alternative photo
		foreach($this->aMedia as $aValue)
		{
			if($iPhotoID != $aValue['med_id'])
			{
				$alternativePhoto = $aValue;
				break;
			}
		}
		
		// Delete from media profile
		$aPhotos = $this->getElementArrayByID($iPhotoID);
		$sIconSrc = $this->sMediaDir . 'icon_' . $aPhotos['med_file'];
		$sThumbSrc = $this->sMediaDir . 'thumb_' . $aPhotos['med_file'];
		$sPhotoSrc = $this->sMediaDir . 'photo_' . $aPhotos['med_file'];
		$this->oMediaQuery->deleteMedia($this->iEventID, $iPhotoID, $this->sMediaType);

		if($aPhotos['med_id'] == $aPhotos['PrimPhoto'])
		{
			if (is_array($alternativePhoto))
				$this->oMediaQuery->setEventPrimaryPhoto($this->iEventID, $alternativePhoto['med_id']);
			else
				$this->oMediaQuery->resetEventPrimPhoto($this->iEventID);
		}

		@unlink($sIconSrc);
		@unlink($sThumbSrc);
		@unlink($sPhotoSrc);
	}

	function makePrimPhoto($iPhotoID)
	{
		$this->oMediaQuery->setEventPrimaryPhoto($this->iEventID, $iPhotoID);
	}
}

?>