<?

require_once( BX_DIRECTORY_PATH_CLASSES . 'BxDolProfileView.php' );
require_once( BX_DIRECTORY_PATH_ROOT . 'profilePhotos.php' );

require_once( BX_DIRECTORY_PATH_CLASSES . 'BxDolClassifieds.php' );
require_once( BX_DIRECTORY_PATH_CLASSES . 'BxDolEvents.php' );
require_once( BX_DIRECTORY_PATH_ROOT . 'phptal/PHPTAL.php' );

class rssFetcher
{
	function __construct($rssUrl, $limitItems, $inNewWindow, 
		$maxCharsInDesc, $maxCharsInCat = 27)
	{
		$this->rssUrl = $rssUrl;
		$this->limitItems = $limitItems;
		$this->inNewWindow = $inNewWindow;
		$this->maxCharsInDesc = $maxCharsInDesc;
		$this->maxCharsInCat = $maxCharsInCat;
	}

	private $fetched = NULL;
	private $rssUrl;
	private $limitItems = 0;
	private $inNewWindow;
	private $maxCharsInDesc;
	private $maxCharsInCat;
     
	public function fetched()
	{
		global $php_date_format;
		global $RSSSettings;
		
		include_once('simplepie/simplepie.inc');
		include_once('simplepie/idn/idna_convert.class.php');	
		
		$feed = new SimplePie();
		$feed->set_feed_url($this->rssUrl);
		$feed->enable_cache(false);
		$feed->init();
		$feed->handle_content_type();
	
		if ($this->limitItems > 0)
			$items = $feed->get_items(0, $this->limitItems);
		else
			$items = $feed->get_items();
		$itemsCount = count($items);
		
		if ($this->fetched == NULL)
		{
			$resRSS['items'] = array();
			foreach ($items as $item)
			{
				$curRSSItem = array();
				$curRSSItem['title'] = $item->get_title();
				$curRSSItem['link'] = $item->get_link();
				$curRSSItem['pubdate'] = $item->get_date($php_date_format);
				$curRSSItem['category'] = $item->get_category()->term;
		
				if ($this->maxCharsInDesc == 0)
					$curRSSItem['description'] = '';
				else 
				{
					$curRSSItem['description'] = strip_tags($item->get_description());
					if (strlen($curRSSItem['description']) > $this->maxCharsInDesc)
					{
						$curRSSItem['description'] = trim(substr($curRSSItem['description'], 0, $this->maxCharsInDesc - 13));
						
						// Trim to last word
						$wordLimiters = array(' ', ',', '.', '?', ':');
						foreach ($wordLimiters as $wordLimiter)
						{
							$limiterPos = strrpos($curRSSItem['description'], $wordLimiter);
							if ($limiterPos !== false)
							{
								$curRSSItem['description'] = trim(substr($curRSSItem['description'], 0, $limiterPos));
								break;
							}
						}
						$curRSSItem['description'] .=	'...';
					} 
					$curRSSItem['description'] .= '&nbsp;<nobr><a href="'.
						$curRSSItem['link'].'" '.$this->inNewWindow.'>Read More</a></nobr>';
				}				
				array_push($resRSS['items'], $curRSSItem);
			}
			$this->fetched = $resRSS;
		}
		return $this->fetched;
	}
}

class BxBaseProfileView extends BxDolProfileView
{
	private $charsInLine = 50;	

	function defineTimeInterval ($iTime)
	{
		$iTime = time() - $iTime;
		if ( $iTime < 60 )
			$sCode = "$iTime "._t("_seconds ago");
		else
		{
			$iTime = round( $iTime / 60 ); // minutes
			if ( $iTime < 60 )
				$sCode = "$iTime "._t("_minutes ago");
			else
			{
				$iTime = round( $iTime / 60 ); //hours
				if ( $iTime < 24 )
					$sCode = "$iTime "._t("_hours ago");
				else
				{
					$iTime = round( $iTime / 24 ); //days
					$sCode = "$iTime "._t("_days ago");
				}
			}
		}
		return $sCode;
	}

	function BxBaseProfileView( $ID )
	{
		BxDolProfileView::BxDolProfileView( $ID );		
	}
	
	function genProfileCSS( $ID )
	{
	    global $site;

	    $ret = '';

	    $query = "SELECT * FROM `ProfilesSettings` WHERE `IDMember` = '$ID'";
	    $arr = db_arr( $query );
	    if ( $arr['IDMember'] )
	    {
	    	if ($arr['BackgroundFilename'] != '')
	    		$bgColorStyle = '';
	    	else
	    		$bgColorStyle = (!$arr['BackgroundColor'] || $arr['BackgroundColor'] == 'default') ?
	    			"background-image: url({$site['images']}bgcolor.gif);"
	    			: "background-color: {$arr['BackgroundColor']};";
			$ret = 	"<style type=\"text/css\">
			    body
			    {
			    	background-image: url( {$site['profileBackground']}{$arr['BackgroundFilename']});
			    	$bgColorStyle
			    }
			    div#right_column_content
			    {
			    	color: {$arr['FontColor']};
			    	font-size: {$arr['FontSize']}px;
			    	font-family: {$arr['FontFamily']};
			    }
			    div#divUnderCustomization
			    {
			    	color: {$arr['FontColor']};
			    	font-size: {$arr['FontSize']}px;
			    	font-family: {$arr['FontFamily']};
			    }
				</style>";
	    }
			
	    return $ret;
	}
	
	function genColumns()
	{
		global $php_date_format;
		$php_date_format = getParam( 'php_date_format');
		
		ob_start();
		
		?>
		<div id="thin_column">
			<? $this -> showColumnBlocks( 1 ); ?>
		</div>
		
		<div id="thick_column">
			<? $this -> showColumnBlocks( 2 ); ?>
		</div>
		<?
		
		return ob_get_clean();
	}
	
	function showColumnBlocks( $column )
	{
		global $logged, $p_arr, $memberID;
		
		$rBlocks = db_res( "SELECT * FROM `ProfileCompose` WHERE `Column`=$column ORDER BY `Order`" );
		while( $aBlock = mysql_fetch_assoc( $rBlocks ) )
		{
			if ($aBlock['Visible'] == 'memb' && $memberID != $p_arr['ID']) continue;
			$func = 'showBlock' . $aBlock['Func'];
			$this -> $func( $aBlock['Caption'], $aBlock['Content'] );
		}
	}
	
	function showBlockPhoto( $sCaption )
	{
		global $memberID;
		global $p_arr;
		global $site;
		
		$ID = $this -> _iProfileID;
		$oPhotos = new ProfilePhotos($ID);
		$oPhotos -> getActiveMediaArray();
/*
		//perform photo voting
		if( $_REQUEST['voteSubmit'] && $_REQUEST['photoID'] )
		{
			$oPhotos -> setVoting();
			$oPhotos -> getActiveMediaArray();
		}
*/
		$iPhotoID = (int)$_REQUEST['photoID'];
		$ret = $oPhotos -> getMediaBlock( $iPhotoID );
		
		if ($memberID == $this -> _iProfileID)
			$addProfilePhotoLink = "<div class='title_content'>
				<a class=\"title_content_link\" href=\"{$site['url']}photos_gallery.php?ID=$ID\">Add Profile Photo</a></div>";
		else 
			$addProfilePhotoLink = '';
		
		echo DesignBoxContent( _t( $sCaption, $p_arr['NickName'] ), $ret, 1, $addProfilePhotoLink);
	}


	function showBlockRSS( $sCaption, $sContent )
    {
        global $p_arr, $site;

		list( $sUrl, $iNum ) = explode( '#', $sContent );
		$iNum = (int)$iNum;
        
        $sUrl = str_replace(array('{SiteUrl}', '{NickName}'),array($site['url'], $p_arr['NickName']), $sUrl);

		$ret = genRSSHtmlOut( $sUrl, $iNum );
		
		echo DesignBoxContent( _t($sCaption), $ret, 1 );
	}
	
	function showBlockEcho( $sCaption, $sContent )
	{
		echo DesignBoxContent( _t($sCaption), $sContent, 1 );
	}
	
	function showBlockLookingForDetails( $sCaption )
	{
		$aFields  = $this -> collectProfileFieldsByCateg( 4 );
		$sDetails = $this -> showProfileFields( $aFields );
		
		if( strlen( $sDetails ) )
		{
			ob_start();
			?>
				<div id="profile_details_wrapper">
					<div class="clear_both"></div>
					<?= $sDetails ?>
					<div class="clear_both"></div>
				</div>
			<?
			$ret = ob_get_clean();
			
			echo DesignBoxContent( _t( $sCaption ), $ret, 1 );
		}
	}
	
	function showBlockProfilePolls( $sCaption )
	{
		$sqlPolls = "SELECT `id_poll` FROM `ProfilesPolls` WHERE `id_profile` = {$this -> _iProfileID}";
		$rPolls = db_res( $sqlPolls );
		
		if( !mysql_num_rows( $rPolls ) )
			return ;
		
		$ret = '<div id="profile_poll_wrap">';
		while( $aPoll = mysql_fetch_assoc( $rPolls ) )
		{
			$ret .= ShowPoll( $aPoll['id_poll'] );
			$ret .= '<div class="clear_both"></div>';
		}
		$ret .= '</div>';
		
		//$show_hide = $this -> genShowHideItem( 'profile_poll_wrap' );
		
		echo DesignBoxContent( _t( $sCaption ), $ret, 1, $show_hide );
	}
	
	function genShowHideItem( $wrapperID, $default = '' )
	{
		if( !$default )
			$default = _t( '_Hide' );
		
		return '
		<div class="caption_item">
			<a href="javascript:void(0);"
			  onclick="el = document.getElementById(\'' . $wrapperID . '\'); if( el.style.display == \'none\' ) {el.style.display = \'block\'; this.innerHTML = \'' . _t( '_Hide' ) . '\';} else {el.style.display = \'none\'; this.innerHTML = \'' . _t( '_Show' ) . '\';}"
			  >' . $default . '</a>
		</div>';
	}
	
	function showBlockActionsMenu( $sCaption )
	{
		global $logged;
		global $p_arr;
		
		$confirmBlockMember = _t('_confirmBlockMember');
		
		$oTemplMenu = new BxTemplMenu( $this -> oTemplConfig );
		
		if( !$logged['member'] or !$p_arr )
			return '';
		
		$memberID  = (int)$_COOKIE['memberID'];
		$profileID = (int)$p_arr['ID'];
		
		if( $memberID == $profileID )
			return '';
			
		// Check is this profile in blocklist of visitor
		$isBlocked = db_value("SELECT ID FROM BlockList WHERE ID=".$memberID." AND Profile=$profileID LIMIT 1"); 
		
		$blockCaption = ($isBlocked) ? _t('_Unblock') : _t('_Block');
		$blockAction = ($isBlocked) ? "window.open( 'list_pop.php?action=unblock&amp;ID=$profileID',  '', 'width={$this -> oTemplConfig -> popUpWindowWidth},height={$this -> oTemplConfig -> popUpWindowHeight},menubar=no,status=no,resizable=no,scrollbars=yes,toolbar=no,location=no' );"
			: "if (confirm('{$confirmBlockMember}')) { window.open( 'list_pop.php?action=block&amp;ID=$profileID',  '', 'width={$this -> oTemplConfig -> popUpWindowWidth},height={$this -> oTemplConfig -> popUpWindowHeight},menubar=no,status=no,resizable=no,scrollbars=yes,toolbar=no,location=no' );}";
		
		/* * * * Ray IM Integration * * * */
		
		$check_res_im = checkAction( $memberID, ACTION_ID_USE_RAY_IM );

		if( ( getParam( 'enable_ray' ) == 'on' ) and
		   get_user_online_status( $profileID ) and
		   ( $check_res_im[CHECK_ACTION_RESULT] == CHECK_ACTION_RESULT_ALLOWED ) )
		{
			$sSndPassword = getPassword( $memberID );
			$IMNow = '';
			//$IMNow = $oTemplMenu -> getMenuItem( 'action_im.gif', _t( '_ChatNow', $p_arr['NickName'] ), "javascript:void(0);", '', '', "openRayWidget( 'im', 'user', '$memberID', '$sSndPassword', '$profileID' );" );
		}
		else
			$IMNow = '';
		
		/* * * * Ray IM Integration [END]* * * */
		
		$ret = '<div class="menuBlock">';
			$ret .= '<div class="menu_item_block">';
			$ret .= '<div class="menu_item_block_left">';
				$ret .= $oTemplMenu -> getMenuItem( 'action_send.gif', _t('Send Message'),     "compose.php?ID=$profileID" );
				$ret .= $oTemplMenu -> getMenuItem( 'action_fave.gif', _t('Add to Favorites'), "list_pop.php?action=hot&amp;ID=$profileID", '', '', '');
				$ret .= $oTemplMenu -> getMenuItem( 'action_friends.gif', _t('Add to Friends'),"list_pop.php?action=friend&amp;ID=$profileID", '', '', '');
				/*$ret .= $oTemplMenu -> getMenuItem( 'action_greet.gif', _t('_Greet'),     "javascript:void(0);", '', '', "window.open( 'greet.php?sendto=$profileID',                  '', 'width={$this -> oTemplConfig -> popUpWindowWidth},height={$this -> oTemplConfig -> popUpWindowHeight},menubar=no,status=no,resizable=no,scrollbars=yes,toolbar=no,location=no' );" );*/
				/*$ret .= $IMNow;*/
				/*if ( !$this -> oTemplConfig -> bAnonymousMode )
					$ret .= $oTemplMenu -> getMenuItem( 'action_email.gif', _t('Contact Info'),   "javascript:void(0);", '', '', "window.open( 'freemail.php?ID=$profileID', '', 'width={$this -> oTemplConfig -> popUpWindowWidth},height={$this -> oTemplConfig -> popUpWindowHeight},menubar=no,status=no,resizable=no,scrollbars=yes,toolbar=no,location=no' );" );*/
				$ret .= '</div>';
				$ret .= '<div class="menu_item_block_right">';
				/*$ret .= $oTemplMenu -> getMenuItem( 'action_photos.gif', _t('_ProfilePhotos'), "photos_gallery.php?ID=$profileID");
				$ret .= $oTemplMenu -> getMenuItem( 'action_videos.gif', _t('_ProfileVideos'),   "javascript:void(0);", '', '', "openRayWidget( 'video', 'player', '$profileID' );" );
				//$ret .= $oTemplMenu -> getMenuItem( 'action_videos.gif', _t('_ProfileVideos'), "media_gallery.php?show=video&ID=$profileID");
				$ret .= $oTemplMenu -> getMenuItem( 'action_music.gif', _t('_ProfileMusic'), "javascript:void(0);", '', '', "openRayWidget( 'mp3', 'player', '$profileID', '" . getPassword( $memberID ) . "', '$memberID');");*/
				$ret .= $oTemplMenu -> getMenuItem( 'action_share.gif', _t('Share Profile'),   "javascript:void(0);", '', '', "return launchTellFriendProfile($profileID);" );
				$ret .= $oTemplMenu -> getMenuItem( 'action_report.gif', _t('_Report'),   "javascript:void(0);", '', '', "window.open( 'list_pop.php?action=spam&amp;ID=$profileID',   '', 'width={$this -> oTemplConfig -> popUpWindowWidth},height={$this -> oTemplConfig -> popUpWindowHeight},menubar=no,status=no,resizable=no,scrollbars=yes,toolbar=no,location=no' );" );		
				$ret .= $oTemplMenu -> getMenuItem( 'action_block.gif', $blockCaption,    "javascript:void(0);", '', '', $blockAction);
			$ret .= '</div>';
			$ret .= '<div class="clear_both"></div>';
			$ret .= '</div>';
		$ret .= '</div>';
		echo DesignBoxContent( _t( $sCaption ), $ret, 1 );
	}
	
	function showBlockRateProfile( $sCaption )
	{
		global $site;
		global $votes;
		
        // Check if profile votes enabled
        if (!$votes || !$this->oVotingView->isEnabled()) return;

        $ret = $this->oVotingView->getBigVoting();

        echo DesignBoxContent( _t( $sCaption ), $ret, 1 );
	}
	
	function showBlockProfileDetails( $sCaption )
	{
		require_once(BX_DIRECTORY_PATH_ROOT . 'profiles/superuserconfig.php');
		
		global $prof;
		global $enable_zodiac;
		global $p_arr;
		global $site;
		global $su_config;
		
		$aFields  = $this -> collectProfileFieldsByCateg( 0 );
		$sDetails = $this -> showProfileFields( $aFields );
		
		$sTagsAddon = "";
		$sTagsQuery = "SELECT `Tags` FROM `Profiles` WHERE `ID`={$p_arr['ID']}";
		$rTags = db_value($sTagsQuery);
		$aTags = explode(',', $rTags);
		
		foreach ($aTags as $tag)
		{
			$tag = trim($tag);
			$sTagsAddon .= "<a href='" . $site['url'] . "search_result.php?tag=" . $tag . "'>" . $tag . "</a>, ";
		}
			
		$sTagsAddon = rtrim ($sTagsAddon, ", "); 
		ob_start();
		?>
			<div id="profile_details_wrapper">
				<table>
					<?= $sDetails ?>
					<tr>
						<td class="profile_td_1">Interests:</td>
						<td class="profile_td_2"><?php echo $sTagsAddon ?></td>
					</tr>
				</table>
			</div>
		<?
		$ret = ob_get_clean();
		
		if ((int)$_COOKIE['memberID'] == $this->_iProfileID)
			$editLink = <<<EOF
				<div class="title_content">
					<a href="{$su_config['url']}me"><img src="{$site['icons']}description_edit.png" 
						class="captionIcon" alt="Edit"/></a>
					<a href="{$su_config['url']}me" class="title_content_link">Edit</a>
				</div>
EOF;
		else
			$editLink = '';

		echo DesignBoxContent(_t($sCaption, $p_arr['NickName']), $ret, 1, $editLink);
	}
	
	function showBlockDescriptions( $sCaption )
	{	
		require_once(BX_DIRECTORY_PATH_ROOT . 'profiles/superuserconfig.php');
		
		global $site;
		global $member;
		global $su_config;
		
		if( strlen(  $this -> _aProfile['DescriptionMe'] ) )
		{
			$text = '<div class="discr">' . 
				'<div class="rss_item_header">' . process_text_output( $this -> _aProfile['Headline'] ) . '</div>' .
				process_smiles($this->_aProfile['DescriptionMe']) .
			'</div>';
			
			if ((int)$_COOKIE['memberID'] == $this->_iProfileID)
				$editLink = <<<EOF
					<div class="title_content">
						<a href="{$su_config['url']}edit/1/1"><img src="{$site['icons']}description_edit.png" 
							class="captionIcon" alt="Edit"/></a>
						<a href="{$su_config['url']}edit/1/1" class="title_content_link">Edit</a>
					</div>
EOF;
			else
				$editLink = '';
				
			echo DesignBoxContent(_t( $sCaption ),  $text, 1, $editLink);
		}
		
		if( strlen( $this -> _aProfile['DescriptionYou'] ) )
		{
			$text = '<div class="discr">' . process_smiles( process_text_output( $this -> _aProfile['DescriptionYou'] ) ) . '</div>';
			echo DesignBoxContent( _t( '_Ideal match description' ), $text, 1 );
		}
	}
	
	function showBlockProfileBlog( $sCaption )
	{
		global $site;
		global $short_date_format;
		global $p_arr;
		global $logged;
		global $tmpl;
		global $php_date_format;
		global $RSSSettings;

		$imageName = 'small_folder';
		$ret = '';
		$iBlogLimitChars = (int)getParam("max_blog_preview");
		$ID = $this->_iProfileID;
		$ownerNick = $this->getNickName();		
		$visitorID = (int)$_COOKIE['memberID'];
		$bOwner = $visitorID == $ID;

		// Check blog to public view
		$isLocked = db_value("SELECT Locked FROM Blogs WHERE OwnerID=$ID LIMIT 1");
		if ($isLocked == 1)
		{
			$bFriend = is_friends($visitorID, $ID);
			if (!$bFriend && !$bOwner && !$logged['admin'])
				//$ret = MsgBox(_t('_private_blog_for_friends'), 16, 2,
				$ret = MsgBox('Locked for friends only', 16, 2,
					"<img src=\"{$site['icons']}lock.gif\" style=\"height:40px; margin-right:5px; vertical-align:middle\"/>");
		}
		elseif ($isLocked == 2)
		{
			$bHotlistFriend = is_hotlist_friends($ID, $visitorID);
			if (!$bHotlistFriend && !$bOwner && !$logged['admin'])
				//$ret = MsgBox(_t('_private_blog_by_invitation'), 16, 2,
				$ret = MsgBox('Locked by invitation only', 16, 2,
					"<img src=\"{$site['icons']}lock.gif\" style=\"height:40px; margin-right:5px; vertical-align:middle\"/>");
		}
		elseif ($isLocked == 3)
		{
			if (!$logged['member'] && !$logged['admin'])
				$ret = MsgBox(_t('_private_blog'), 16, 2,
					"<img src=\"{$site['icons']}lock.gif\" style=\"height:40px; margin-right:5px; vertical-align:middle\"/>");
		}
		elseif ($isLocked == 4)
		{
			if (!$bOwner && !$logged['admin'])
				$ret = MsgBox('Blog Locked', 16, 2,
					"<img src=\"{$site['icons']}lock.gif\" style=\"height:40px; margin-right:5px; vertical-align:middle\"/>");
		}				
		if ($ret == '')
		{
		$postsCount = db_value("SELECT `PostsOnProfilePage` FROM `ProfilesSettings` WHERE `IDMember` = $ID");
		if (!$postsCount) $postsCount = 2;
			
		$sQuery = "
			SELECT DISTINCT
				`BlogPosts`.`PostID`,
				`BlogPosts`.`CategoryID`,
				`BlogPosts`.`PostText`,
				UNIX_TIMESTAMP( `BlogPosts`.`PostDate` ) AS `PostDate`,
				`BlogPosts`.`PostCaption`,
				`BlogPosts`.`urltitle`,
				`BlogCategories`.`CategoryName`,
				`BlogCategories`.`CategoryPhoto`,
				COUNT( `BlogPostComments`.`CommentID` ) AS `CommentsNum`
			FROM `BlogCategories`
			INNER JOIN `BlogPosts` ON
				 `BlogCategories`.`CategoryID` = `BlogPosts`.`CategoryID`
			LEFT JOIN `BlogPostComments` ON
				`BlogPosts`.`PostID` = `BlogPostComments`.`PostID`
			WHERE
				`BlogCategories`.`OwnerID`  = {$ID} AND
				`BlogPosts`.`PostReadPermission` = 'public' AND
				`BlogPosts`.`PostStatus`         = 'approval'
			GROUP BY `BlogPosts`.`PostID`
			ORDER BY `BlogPosts`.`PostDate` DESC
			LIMIT $postsCount
			";
		
		$rBlogs = db_res( $sQuery );
		
		if( !mysql_num_rows( $rBlogs ) )
			$ret = '';
		else
		{
		ob_start();
		?>
		<div id="container_blogs">
		<?
		
		$defaultCatIcon = 'folder_small.png';
		
		$LinesInSummaryOnProfile = db_value("SELECT `LinesInSummaryOnProfile` 
			FROM `ProfilesSettings` WHERE `IDMember`=$ID");
		if (!$LinesInSummaryOnProfile) $LinesInSummaryOnProfile = 3;
			
		$maxCharsInPostText = $LinesInSummaryOnProfile * $this->charsInLine;
		
		while( $aBlog = mysql_fetch_assoc( $rBlogs ) )
		{
			$categoryIcon = ($aBlog['CategoryPhoto']) ? "small_{$aBlog['CategoryPhoto']}" : $defaultCatIcon;
			$sLinkMore = '';
			$postLink = (useSEF && $aBlog['urltitle'] != '') ? "{$site['url']}$ownerNick/{$aBlog['urltitle']}.html"
				: "{$site['url']}blogs.php?action=show_member_post&amp;ownerID=$ID&amp;post_id={$aBlog['PostID']}"; 
			$sBlogSnippet = strip_tags( $aBlog['PostText'] );
			if (strlen($sBlogSnippet) > $maxCharsInPostText)
			{
				$sLinkMore = "... <a href=\"$postLink\">"._t('_Read more')."</a>";
				$sBlogSnippet = trim(substr($sBlogSnippet, 0, 
					$maxCharsInPostText - 13)) . $sLinkMore;
			}
			
			?>
				<div class="blogBlock">
					<div class="blogHead">
					<?= "<a href=\"$postLink\" class=\"bottom_text\">{$aBlog['PostCaption']}</a>"?>					
					</div>
					<div class="blogInfo">
						<span style="float:left"><img src="<?= getTemplateIcon( 'clock.gif' ) ?>" alt="date" /><?= date( $php_date_format, $aBlog['PostDate'] ) . ' ' ?></span>
						<span style="float:left"><?= _t( '_in Category', getTemplateIcon($categoryIcon), 'blogs.php?action=show_member_blog&ownerID='.$ID.'&category='.$aBlog['CategoryID'], $aBlog['CategoryName'] ) . '; ' ?></span>
						<span style="float:left"><?= _t( '_comments N', getTemplateIcon( 'add_comment.gif' ), $aBlog['CommentsNum'] ) ?></span>
					</div>
					<div class="blogSnippet">
						<?= $sBlogSnippet ?>
					</div>
				</div>
			<?
		}		
		?>
		</div>
		<?
		$ret = ob_get_clean();
	
		//$show_hide = $this -> genShowHideItem( 'container_blogs' );
		if (useSEF)
			$blogLink = "<div class=\"caption_item\" style=\"padding-right:10px\">
				<a href=\"{$site['url']}$ownerNick/blog\">View Blog</a></div>";
		else
			$blogLink = "<div class=\"caption_item\" style=\"padding-right:10px\">
				<a href=\"{$site['url']}blogs.php?action=show_member_blog&ownerID=$ID\">View Blog</a></div>";
		$title_extra = $show_hide . $blogLink;
		}
		}
		if ($ret != '')
		{
			$blogTitle = db_value("SELECT Title FROM Blogs WHERE OwnerID=$ID LIMIT 1");
			$caption = ($blogTitle) ? $blogTitle : _t( $sCaption, $p_arr['NickName']);
			echo DesignBoxContent($caption, $ret, 1, $title_extra);
		}
		else
			echo '';
		
		/***********************
		// RSS Blog Feed
		***********************/
		
		if (db_value("SELECT `StudioFeatures` FROM `Profiles` WHERE `ID`=$ID"))
		{
			$RSSSettings = db_arr("SELECT `ShowRSSBlogFeed`, `RSSBlogFeedURL`, `RSSPostsOnProfilePage`, 
				`RSSTitle`, `RSSFeedHomepage`, `RSSCategoryIcon`, `RSSLinesInSummary`
				FROM `ProfilesSettings` WHERE `IDMember`=$ID");
			if ($RSSSettings['ShowRSSBlogFeed'])
			{
				$RSSPostsCount = ($RSSSettings['RSSPostsOnProfilePage'] > 0) ? $RSSSettings['RSSPostsOnProfilePage'] : 5;
				$RSSInNewWindow = ' target="_blank"';
				$maxCharsInDesc = $RSSSettings['RSSLinesInSummary'] * $this->charsInLine;
				
				$tal = new PHPTAL(BX_DIRECTORY_PATH_ROOT . "templates/tmpl_{$tmpl}/rssblogfeed.html");
				$tal->rssFetcher = new rssFetcher($RSSSettings['RSSBlogFeedURL'], 
					$RSSPostsCount, $RSSInNewWindow, $maxCharsInDesc);
				$tal->inNewWindow = $RSSInNewWindow;
				$tal->settingsID = md5($RSSSettings['RSSBlogFeedURL'] . 
					$RSSSettings['RSSFeedHomepage'] . $RSSSettings['RSSCategoryIcon'] . 
					$RSSSettings['RSSPostsOnProfilePage'] .	$RSSSettings['RSSLinesInSummary']);
				$categoryIcon = ($RSSSettings['RSSCategoryIcon'] != '') ? $RSSSettings['RSSCategoryIcon']
					: "{$imageName}1.png";
				$tal->categoryIcon = "templates/tmpl_{$tmpl}/images/icons/$categoryIcon";
						
				try {
				     $ret = $tal->execute();
				}
				catch (Exception $e) {
				     $ret = $e->getMessage();
				}
				
				$RSSTitle = (trim($RSSSettings['RSSTitle']) != '') ? $RSSSettings['RSSTitle'] : 'RSS Blog Feed';
				$title_extra = '<div class="caption_item" style="padding-right:10px">
					<a href="'.$RSSSettings['RSSFeedHomepage'].'"'.$RSSInNewWindow.'>View Blog</a></div>';
				
				echo DesignBoxContent($RSSTitle, $ret, 1, $title_extra);
			}
		}
	}
	
	function showBlockClassifieds( $sCaption )
	{
		global $site;
		global $short_date_format;
		global $php_date_format;
		
		//$iBlogLimitChars = (int)getParam("max_classified_preview");
		$iBlogLimitChars = (int)getParam("max_blog_preview");
		$ID = $this -> _iProfileID;
		$sQuery = "
			SELECT DISTINCT
			`ClassifiedsAdvertisements`.`ID`,
			`ClassifiedsAdvertisements`.`Subject`,
			`ClassifiedsAdvertisements`.`Media`,
			`Profiles`.`NickName`,
			UNIX_TIMESTAMP( `ClassifiedsAdvertisements`.`DateTime` ) as `DateTime_f`,
			`ClassifiedsAdvertisements`.`DateTime`,
			`Classifieds`.`Name`, `Classifieds`.`ID` AS `CatID`,
			`ClassifiedsSubs`.`NameSub`, `ClassifiedsSubs`.`ID` AS `SubCatID`,
			`ClassifiedsAdvertisements`.`Message`,
			COUNT(`ClsAdvComments`.`ID`) AS 'CommCount'
			FROM `ClassifiedsAdvertisements`
			LEFT JOIN `ClassifiedsSubs`
			ON `ClassifiedsSubs`.`ID`=`ClassifiedsAdvertisements`.`IDClassifiedsSubs`
			LEFT JOIN `Classifieds`
			ON `Classifieds`.`ID`=`ClassifiedsSubs`.`IDClassified`
			LEFT JOIN `Profiles` ON `Profiles`.`ID`=`ClassifiedsAdvertisements`.`IDProfile`
			LEFT JOIN `ClsAdvComments` ON `ClsAdvComments`.`IDAdv`=`ClassifiedsAdvertisements`.`ID`
			WHERE
			`ClassifiedsAdvertisements`.`IDProfile`  = {$ID} AND
			`ClassifiedsAdvertisements`.`Status` = 'active'
			GROUP BY `ClassifiedsAdvertisements`.`ID`
			ORDER BY `DateTime` DESC
			LIMIT 5
		";

		$rBlogs = db_res( $sQuery );
		
		if( !mysql_num_rows( $rBlogs ) )
			return '';
		
		ob_start();
		?>
		<div id="container_classifieds">
		<?

		$oClassifieds = new BxDolClassifieds();

		while( $aBlog = mysql_fetch_assoc( $rBlogs ) )
		{
			$sPic = $oClassifieds->getImageCode($aBlog['Media'],TRUE);

			$sLinkMore = '';
			if( strlen( $aBlog['Message']) > $iBlogLimitChars ) 
				//$sLinkMore = "... <a href=\"".$site['url']."blog.php?owner=".$ID."&show=blog&blogID=".$aBlog['PostID']."\">"._t('_Read more')."</a>";
				$sLinkMore = "... <a href=\"".$site['url']."classifieds.php?ShowAdvertisementID=".$aBlog['ID']."\">"._t('_Read more')."</a>";

			$sBlogSnippet = substr( strip_tags( $aBlog['Message'] ), 0, $iBlogLimitChars ) . $sLinkMore;
			?>
				<div class="thumbnail_block" style="float:left;width:45px;height:45px;margin-right:10px;margin-top:10px;position:relative;">
					<?= $sPic ?>
				</div>
				<div class="blog_wrapper" style="width:290px;float:left;position:relative;">
					<div class="blogHead1" style="">
						<? echo '<a href="' . $site['url'] . 'classifieds.php?ShowAdvertisementID=' . $aBlog['ID'] . '" class="bottom_text">' ?>
							<?= $aBlog['Subject'] ?>
						</a>
					</div>
					<div class="blogInfo">
						<span><img src="<?= getTemplateIcon( 'clock.gif' ) ?>" alt="date" /><?= date( $php_date_format, $aBlog['DateTime_f'] ) . ' ' ?></span>
						<span><?= _t( '_in Category', getTemplateIcon( 'folder_small.png' ), 'classifieds.php?bClassifiedID='.$aBlog['CatID'], process_line_output($aBlog['Name']) ).' / '.
						'<a href="classifieds.php?bSubClassifiedID=' . $aBlog['SubCatID'].'">'.process_line_output($aBlog['NameSub']).'</a>' ?></span>
						<span><?= _t( '_comments N', getTemplateIcon( 'add_comment.gif' ), $aBlog['CommCount'] ) ?></span>
					</div>
					<div class="blogSnippet">
						<?= $sBlogSnippet ?>
					</div>
				</div>
				<div class="clear_both"></div>
			<?
		}		
		?>
		</div>
		<?
		$ret = ob_get_clean();
		
		//$show_hide = $this -> genShowHideItem( 'container_classifieds' );
		
		echo DesignBoxContent( _t( $sCaption ), $ret, 1, $show_hide );
		
	}

	function showBlockEvents( $sCaption )
	{
		global $site;
		global $short_date_format;
		global $hide_past_events;
		global $num_events_on_profile;
		global $php_date_format;
		global $prof;
		
		//$iBlogLimitChars = (int)getParam("max_classified_preview");
		$iBlogLimitChars = (int)getParam("max_blog_preview");
		$ID = $this -> _iProfileID;
		
		$now = time();
				
		$upcomingEventsOnly = '';
		if ($hide_past_events) // select upcoming events only
			$upcomingEventsOnly = " AND (UNIX_TIMESTAMP(`EventStart`) > $now OR 
				UNIX_TIMESTAMP(`EventEnd`) > $now)";
		
		$aMember['ID'] = (int)$_COOKIE['memberID'];
		$bOwner = ($aMember['ID'] == $ID) ? true : false;
		$bFriend = is_friends($aMember['ID'], $ID);

		// Select "public only" events
		$publicOnly = (!$bOwner && !$bFriend && !$logged['admin']) ? 
			" AND `ReadPermission` = 'public'" : '';
		
		// Filter number of events on Profile page
		$numEvents = intval($num_events_on_profile);
		$limitEvents = ($numEvents) ? $numEvents : 5;
		
		$sQuery = "
			SELECT DISTINCT `SDatingEvents`. * , `Profiles`.`NickName`,
				(UNIX_TIMESTAMP() - UNIX_TIMESTAMP(`EventStart`)) AS `sec`,
				(UNIX_TIMESTAMP() - UNIX_TIMESTAMP(`EventEnd`)) AS `secAgo`,
				(NOW() > `EventStart`) AS `EventBegan`,
				(NOW() < `EventEnd`) AS `EventNotFinished`
			FROM `SDatingEvents` 
			LEFT JOIN `Profiles` ON `Profiles`.`ID` = `SDatingEvents`.`ResponsibleID` 
			WHERE `SDatingEvents`.`ResponsibleID` = {$ID} AND `SDatingEvents`.`Status` = 'Active' 
				$upcomingEventsOnly 
				$publicOnly
			ORDER BY `EventNotFinished` DESC, `EventStart` 
			LIMIT $limitEvents
		";
		
		$rEvents = db_res( $sQuery );
		$eventsCount = mysql_num_rows($rEvents);
		if (!$eventsCount) return '';
		
		$ret = '<div id="container_events">';

		$oEvents = new BxDolEvents();
		while($event = mysql_fetch_assoc($rEvents))
		{
			$sPic = $oEvents->GetEventPicture($event['ID']);
			$eventLinkTmpl = ($event['urltitle'] && useSEF) ?	
				"<a href=\"{$site['url']}".getNickName($event['ResponsibleID']).
					"/event/{$event['urltitle']}.html\" %1%>%0%</a>"
				: "<a href=\"events.php?action=show_info&amp;event_id={$event['ID']}\" %1%>%0%</a>";
			$eventTitleLink = str_replace(array('%0%', '%1%'), array($event['Title'],
				'style="margin-left:10px; font-weight:bold; text-decoration:none"'), $eventLinkTmpl);
			
			$sDateTime = date( $php_date_format, strtotime($event['EventStart'] ) );
			if (!$event['EventBegan'])
				$eventStart = '('._format_when($event['sec']).')';
			elseif (!$event['EventNotFinished'])
				$eventStart = '(<span style="color:red">'._format_when($event['secAgo']).'</span>)';
			else 
			{
				$eventEnd = date( $php_date_format, strtotime($event['EventEnd'] ) );			
				$eventStart = " to $eventEnd";
			}
			$sEventsStart = "$sDateTime $eventStart";
			
			$sCountry = ($event['Country']!='') ? _t('__'. $prof['countries'][$event['Country']]) : '';
			$sCity = ($event['City']!='') ? ', '.process_line_output($event['City']) : '';
			$sPlace = ($event['Place']!='') ? ', '.process_line_output($event['Place']) : '';

			$ret .= <<<EOF
			<table style="margin-bottom:5px">
				<tr>
					<td valign='top'>$sPic</td>
					<td valign='top'>
						<div class="oneline">$eventTitleLink</div>
						<div style="margin-left:10px">$sEventsStart</div>
						<div class="oneline" style="margin-left:10px">{$sCountry}{$sCity}{$sPlace}</div>
					</td>
				</tr>
			</table>
EOF;
		}		
		$ret .= '</div>';
		
		//$show_hide = $this -> genShowHideItem( 'container_events' );
		
		$upcomingEventsQuery = "
			SELECT COUNT(`id`)
			FROM `SDatingEvents` 
			WHERE `SDatingEvents`.`ResponsibleID` = $ID 
				AND `SDatingEvents`.`Status` = 'Active'
				AND (UNIX_TIMESTAMP(`EventStart`) > $now OR UNIX_TIMESTAMP(`EventEnd`) > $now) 
				$publicOnly";
		
		$upcomingEvents = db_value($upcomingEventsQuery);
		if ($upcomingEvents)
		{
			$eventsStr = ($upcomingEvents > 1) ? 'Events' : 'Event';
			$eventsLink = "<div class='all_events_link'>
				<a href=\"{$site['url']}events.php?show_events=upcoming&amp;action=show&amp;ID=$ID\">
					$upcomingEvents Upcoming $eventsStr
				</a></div>";
		}
		else
		{
			$pastEventsQuery = "
				SELECT COUNT(`id`)
				FROM `SDatingEvents` 
				WHERE `ResponsibleID` = $ID AND `Status` = 'Active'	AND `Featured` = '0'
					$upcomingEventsOnly
					$publicOnly";	
			$pastEvents = db_value($pastEventsQuery);
			
			$eventsStr = ($pastEvents > 1) ? 'Events' : 'Event';	
			$eventsLink = "<div class='all_events_link'>
				<a href=\"{$site['url']}events.php?show_events=past&amp;action=show&amp;ID=$ID\">
					$pastEvents Past $eventsStr
				</a></div>";
		}
		
		echo DesignBoxContent(_t( $sCaption ), $ret, 1, $show_hide, $eventsLink);
	}

	function showBlockGroups( $sCaption )
	{
		global $site;
		global $short_date_format;
		global $php_date_format;
		
		//$iBlogLimitChars = (int)getParam("max_classified_preview");
		$iBlogLimitChars = (int)getParam("max_blog_preview");
		$ID = $this -> _iProfileID;
		$sQuery = "
			SELECT DISTINCT `Groups`.`ID`, `Groups`.`Name`, `Groups`.`Desc`,
			UNIX_TIMESTAMP( `Groups`.`created` ) as `DateTime_f`,
			`Profiles`.`NickName`,
			`GroupsCateg`.`Name` AS 'CategName', `GroupsCateg`.`ID` AS `CategID`
			FROM `GroupsMembers`, `Groups`
			LEFT JOIN `GroupsCateg` ON `GroupsCateg`.`ID` = `Groups`.`categID` 
			LEFT JOIN `Profiles` ON `Profiles`.`ID` = `Groups`.`creatorID` 
			WHERE 
			`GroupsMembers`.`memberID` = {$ID} AND
			`GroupsMembers`.`groupID`  = `Groups`.`ID` AND
			`GroupsMembers`.`status`   = 'Active'
			ORDER BY `created` DESC 
			LIMIT 5
		";
		
		$rBlogs = db_res( $sQuery );
		
		if( !mysql_num_rows( $rBlogs ) )
			return '';
		
		ob_start();
		?>
		<div id="container_groups">
		<?

		$oEvents = new BxDolEvents();

		while( $aBlog = mysql_fetch_assoc( $rBlogs ) )
		{
			$sPic = $oEvents->GetGroupPicture($aBlog['ID']);

			$sLinkMore = '';
			if( strlen( $aBlog['Description']) > $iBlogLimitChars ) 
				//$sLinkMore = "... <a href=\"".$site['url']."blog.php?owner=".$ID."&show=blog&blogID=".$aBlog['PostID']."\">"._t('_Read more')."</a>";
				$sLinkMore = "... <a href=\"".$site['url']."group.php?ID=".$aBlog['ID']."\">"._t('_Read more')."</a>";

			$sBlogSnippet = substr( strip_tags( $aBlog['Desc'] ), 0, $iBlogLimitChars ) . $sLinkMore;
			?>
				<div class="thumbnail_block" style="float:left;width:45px;height:45px;margin-right:10px;margin-top:10px;position:relative;">
					<?= $sPic ?>
				</div>
				<div class="blog_wrapper" style="width:290px;float:left;position:relative;">
					<div class="blogHead1">
						<? echo '<a href="' . $site['url'] . 'group.php?ID=' . $aBlog['ID'] . '" class="bottom_text">' ?>
							<?= $aBlog['Name'] ?>
						</a>
					</div>
					<div class="blogInfo">
						<span><img src="<?= getTemplateIcon( 'clock.gif' ) ?>" alt="date" /><?= date( $php_date_format, $aBlog['DateTime_f'] ) . ' ' ?></span>
						<span><?= _t( '_in Category', getTemplateIcon( 'folder_small.png' ), 'groups_browse.php?categID='.$aBlog['CategID'], process_line_output($aBlog['CategName']) ) ?></span>
					</div>
					<div class="blogSnippet">
						<?= $sBlogSnippet ?>
					</div>
				</div>
				<div class="clear_both"></div>
			<?
		}		
		?>
		</div>
		<?
		$ret = ob_get_clean();
		
		//$show_hide = $this -> genShowHideItem( 'container_groups' );

		echo DesignBoxContent( _t( $sCaption ), $ret, 1, $show_hide );
	}

	function showBlockComments( $sCaption )
	{
		global $site;
		global $logged;
		global $sNav;
		
		ob_start();
		
		?>
		<div id="comments_wrapper">
			<?= $this -> getComments(); ?>
			<?= $sNav ?>
			<?= $this -> getCommentActions() ?>
			<?= $this -> getAddCommentForm() ?>
			<!-- commentNavigation(3, 0) -->
		</div>
		<?

		$ret = ob_get_clean();
		
		if (isset($_GET['commPage']))
			$ret .= "<script>if (window.addEventListener) window.addEventListener('load', ShowComments, false);
				else if (document.addEventListener) document.addEventListener('load', ShowComments, false);
				else if (window.attachEvent) window.attachEvent('onload',ShowComments); 
		
				function ShowComments()
				{
					location.hash = '#comments_wrapper';
				}
				</script>";	
		//$show_hide = $this -> genShowHideItem( 'comments_wrapper' );
		
		// Check is visitor in blocklist of profile owner
		$visitorID = (int)$_COOKIE['memberID'];
		$isBlocked = db_value("SELECT ID FROM BlockList WHERE ID=".$this->_iProfileID." AND Profile=$visitorID LIMIT 1");
		
		// Check is visitor have Unconfirmed status
		$status = db_value("SELECT `Status` FROM `Profiles` WHERE ID=$visitorID LIMIT 1");
		$isUnconfirmed = ($status == 'Unconfirmed');
		
		$leaveCommentLink = ($logged['member'] && !$isBlocked && !$isUnconfirmed) ? "<div class='title_content'>
			<a href='". $_SERVER['REQUEST_URI'] ."#post_comment_form' class='title_content_link'
				onclick=\"javascript: UpdateField('replyTO','0'); 
				document.getElementById('post_comment_form').style.display = 'block'; 
				document.getElementById('add_comment_label').style.display = 'none';;\">
				"._t('_Post Comment').'</a>' : '';
		
		echo DesignBoxContent( _t( $sCaption ), $ret, 1, $show_hide, $leaveCommentLink);
	}
	
	function showBlockShareMusic( $sCaption )
	{
		echo '<div id="show_shareMusic">';
		echo PageCompShareMusicContent( $sCaption, $this -> _iProfileID );
		echo '</div>';
	}
	
	function showBlockSharePhotos( $sCaption )
	{
		echo '<div id="show_sharePhotos">';
		echo PageCompSharePhotosContent( $sCaption, $this -> _iProfileID );
		echo '</div>';
	}
	
	function showBlockShareVideos( $sCaption )
	{
		echo '<div id="show_shareVideos">';
		echo PageCompShareVideosContent( $sCaption, $this -> _iProfileID );
		echo '</div>';
	}

	function showBlockFriends( $sCaption )
	{
		echo '<div id="show_friends">';
		echo PageCompFriendsContent( $sCaption, $this -> _iProfileID );
		echo '</div>';
	}

	function getComments( $iReplyTo = 0)
	{
		global $site;
		global $sNav;
		
		$iDivis = 5;
		$iCurr  = 1;

		if ($iReplyTo)
			$sLimit = '';
		else 
		{
			if (!isset($_GET['commPage']))
				$sLimit =  ' LIMIT 0,'.$iDivis;
			elseif ($_GET['commPage'] > 0)
			{
				$iCurr = (int)$_GET['commPage'];
				$sLimit =  ' LIMIT '.($iCurr - 1)*$iDivis.','.$iDivis;
			}
			else 
				$sLimit = '';
		}

		if (!$iReplyTo)
		{
			$iNums = db_value("SELECT COUNT(`ID`) FROM `ProfilesComments` 
				WHERE `Recipient` = {$this->_iProfileID} AND `ReplyTO` = 0");
			$iRepliesNums = db_value("SELECT COUNT(`ID`) FROM `ProfilesComments` 
				WHERE `Recipient` = {$this->_iProfileID} AND `ReplyTO` != 0");
			$iTotalComments = $iNums + $iRepliesNums;
			$sNav = ($iNums > $iDivis && $sLimit != '') ? commentNavigation($iNums, $iDivis, $iCurr, $this->_iProfileID) : '';
		}
		
		if ($sNav != '' && $sLimit != '')
		{
			$commPagePos = strpos($_SERVER['REQUEST_URI'], '&commPage');	
			$baseUrl = "{$site['url']}profile.php?ID={$this->_iProfileID}";
			$sNav .= "<a href=\"$baseUrl&commPage=0\" style=\"float:left; clear:left; 
				margin-bottom:15px; width:140px; padding-left:10px\">View all $iTotalComments comments</a>";
		}
	
		$sQuery = "
			SELECT
				`ProfilesComments`.`ID`,
				`ProfilesComments`.`Date`,
				UNIX_TIMESTAMP(`Date`) AS `UnixDate`,
				`ProfilesComments`.`IP`,
				`ProfilesComments`.`Text`,
				`ProfilesComments`.`New`,
				`ProfilesComments`.`ReplyTO`,
				`Profiles`.`ID` AS `senderID`,
				`Profiles`.`NickName` AS `senderNick`
			FROM `ProfilesComments`
			LEFT JOIN `Profiles` ON
				`ProfilesComments`.`Sender` = `Profiles`.`ID`
			WHERE
				`ProfilesComments`.`Recipient` = {$this->_iProfileID} AND
				`ReplyTO` = $iReplyTo
			ORDER BY `Date` DESC
			$sLimit
			";
		$rComments = db_res( $sQuery );
		ob_start();
		while( $aComment = mysql_fetch_assoc( $rComments ) )
		{
			$commentActions = $this->getCommentActions($aComment['ID'], $iReplyTo);
		?>
			<div class="commentUnit">
				<div class="userPic">
					<?php echo get_member_icon($aComment['senderID'])?>
				</div>
				<div class="commentMain">
					<?= $commentActions ?>
					<div class="commentInfo">
					<a href="<?= getProfileLink($aComment['senderID']) ?>">
						<?= trim (process_line_output($aComment['senderNick']))?>
					</a>
					</div>
					<br/>
					<div class="newsDate" style="float:left; margin-top:3px;">
						<?php echo gmdate("j M H:i", $aComment['UnixDate']); ?>
					</div>		
			<?
			if( $this -> owner && $aComment['New'] )
			{
				?>
					<span class="commentNew"><?= _t("_new") ?></span>
				<?
				db_res( "UPDATE `ProfilesComments` SET `New` = '0' WHERE `ID` = {$aComment['ID']}" );
			}
			
			?>				
					<div class="commentText">
						<?= process_smiles( $aComment['Text'])  ?>
					</div>
				</div>			
			</div>
			<?
			if( $answers = $this -> getComments( $aComment['ID'] ) )
			{
				?>
			<div id="replies_to_<?= $aComment['ID'] ?>" class="comment_replies">
				<?= $answers ?>
			</div>
				<?
			}
		}
		
		return ob_get_clean();
	}

	function getCommentActions($ID = 0, $iReplyTo = false)
	{
		global $logged;
	
		ob_start();
		?>
				<div class="comment_actions" >
		<?
		// Check is visitor in blocklist of profile owner
		$visitorID = (int)$_COOKIE['memberID'];
		$isBlocked = db_value("SELECT ID FROM BlockList WHERE ID=".$this->_iProfileID." AND Profile=$visitorID LIMIT 1");
		
		// Check is visitor have Unconfirmed status
		$status = db_value("SELECT `Status` FROM `Profiles` WHERE ID=$visitorID LIMIT 1");
		$isUnconfirmed = ($status == 'Unconfirmed');
		
		if( $ID )
		{
			if( $logged['member'] && !$isBlocked && !$iReplyTo)
			{
				$linkToPostCommentForm = "{$_SERVER['REQUEST_URI']}#post_comment_form";
				?>
					<a href="<?= $linkToPostCommentForm ?>" class="replyComment"
					  onclick='javascript: UpdateField("replyTO","<?= $ID ?>"); 
					  	document.getElementById("post_comment_form").style.display = "block";
					  	document.getElementById("add_comment_label").style.display = "none";'>
					<?= _t( '_answer' ) ?></a><br/>
				<?
			}
			
			if( $logged['admin'] || $this -> owner )
			{
				?>
					<a style="padding-top:3px" href="<?= $_SERVER['PHP_SELF'] ?>?ID=<?= $this -> _iProfileID ?>&amp;action=commentdelete&amp;commentID=<?= $ID ?>&amp;comm_page=<?= $this -> comm_page ?>"
					  onclick="return confirm( '<?= _t( '_are you sure?' ) ?>' );">
					<?= _t( '_delete' ) ?></a>
				<?
			}
		}
		else
		{			
			if( $logged['member'] && !$isBlocked && !$isUnconfirmed)
			{
				?>
					</div>
					<div class="comment_add_comment">
					<a href="javascript:void(0);" id="add_comment_label"
					  onclick="javascript: UpdateField('replyTO','0');
					  	document.getElementById('post_comment_form').style.display = 'block'; this.style.display = 'none';"
					  ><?= _t( '_Post Comment' ) ?></a>
				<?
			}
		}
		?>
				</div>
		<?
		
		return ob_get_clean();
	}

	function getAddCommentForm( $ID = 0 )
	{
		$ret = '';
		
		if( $this -> comm_page )
			$sFormAdd = '&amp;comm_page=' . $this -> comm_page;
		else
			$sFormAdd = '';
			
		$showAdvancedTextEditor = false;
		if ($showAdvancedTextEditor)
			$textEditorCode = 
				'<script src="inc/rte/js/richtext_mini.js" type="text/javascript" language="javascript"></script>
				<script src="inc/rte/js/config-comments.js" type="text/javascript" language="javascript"></script>
				<script>
					initRTE("'. jsAddSlashes('') .'");
				</script>';
		else 
			$textEditorCode = 
				'<textarea name="commenttext" class="comment_textarea" id="commenttext_to_'. $ID .'"></textarea>';
			
		ob_start();
		?>
				<div style="margin-left:8px; " class="addcomment_textarea" id="post_comment_form">
					<form method="post" action="<?= $_SERVER['PHP_SELF'] ?>?ID=<?= $this -> _iProfileID . $sFormAdd ?>">
<!--					<form method="post" action="<?= 'profile.php' ?>?ID=<?= $this -> _iProfileID . $sFormAdd ?>"> -->
						<div id="textEditorContainer"><?= $textEditorCode ?></div>
						<div class="addcomment_submit" style="text-align:center;">
							<input type="hidden" name="ID" value="<?= $this -> _iProfileID ?>" />
							<input type="hidden" name="replyTO" id="replyTO" value="<?= $ID ?>" />
							<input type="submit" name="commentsubmit" value="Post" />
							<input type="button" value="Cancel" onClick="
								javascript: document.getElementById('post_comment_form').style.display = 'none';
								document.getElementById('add_comment_label').style.display = 'block'" />
						</div>
					</form>
				</div>
				<script>document.getElementById('post_comment_form').style.display = 'none';</script>
		<?
		
		return ob_get_clean();
	}
	
	function getPagination()
	{
		return 'pagination...';
	}

	function collectProfileFieldsByCateg( $categ )
	{
		$rFields = db_res( "SELECT * FROM `ProfilesDesc` WHERE `visible` AND ( FIND_IN_SET('0',show_on_page) OR FIND_IN_SET('7',show_on_page) ) ORDER BY `order`" );
		$aFields = array();
		$doCollect = false;
		
		while( $aField = mysql_fetch_assoc( $rFields ) )
		{
			if( is_numeric( $aField['name'] ) and (int)$aField['name'] == $categ )
			{
				$doCollect = true; //begin collect fields
				continue;
			}
			
			if( !$doCollect )
				continue; //do not collect
			
			if( is_numeric( $aField['name'] ) )
				break; //stop collect fields
			
			$aFields[] = $aField; //do collect
		}
		
		return $aFields;
	}
	
	function showProfileFields( $aFields )
	{
		global $p_arr, $site, $su_config;
		
		// Check whether profile is global member with filling profile info
		$filledSex = true;
		$filledCountry = true;
		if ($p_arr['globalid'])
		{
			// Get profile fields from global site
			$profileData = unserialize(file_get_contents(
				"{$su_config['url']}get_profile_fields/{$p_arr['globalid']}/{$p_arr['Password']}"));
			if (is_array($profileData) && $profileData['globalstate'] != 'medium' && 
				$profileData['globalstate'] != 'full')
			{
				$filledSex = false;
				if (!$profileData['country'])
					$filledCountry = false;
			}
		}
		
		$rd = 1;
		$first_row = 1;
		
	    $aRedundantFields = array();
		
		$ret = '';
		
	    foreach( $aFields as $arrpd )
		{		
	        $fname = get_input_name ( $arrpd );
			$sRealFName = get_field_name ( $arrpd );
			
			// Hide Sex and Country if global member yet has not set this fields
			if (($fname == 'Sex' && !$filledSex) || ($fname == 'Country' && !$filledCountry)) continue;

			if ( !in_array( $sRealFName, $aRedundantFields ) )
			{
		        if ( $arrpd['get_value_db'] )
		        {
		            $funcbody = $arrpd['get_value_db'];
		            $func = create_function('$arg0',$funcbody);

		            $p_arr[$fname] = $func($p_arr);
		        }

				if( !strlen( $p_arr[$fname] ) )
					continue;
				
		        //if ( !strlen($p_arr[$fname]) ) $p_arr[$fname] = $p_arr[$fname];
				$not_first_row = 0;
				
		        switch ($arrpd['type'])
		        {
			        case 'set': // set of checkboxes
			            $ret .= print_row_set ( $first_row, $arrpd, $p_arr[$fname], "table", $rd, 2, "50%" );
			            break;
			        case 'rb': // radio buttons
			            $ret .= print_row_radio_button ( $first_row, $arrpd, $p_arr[$fname], "table", $rd, 2, "50%" );
			            break;
			        case 'r': // reference to array for combo box
						if ( $fname == 'Country' )
							$imagecode = '<img src="'. ($site['flags'].strtolower($p_arr[$fname])) .'.gif" alt="flag" />';
						else
							$imagecode = '';
			            $ret .= print_row_ref ( $first_row, $arrpd, $p_arr[$fname], "table", $rd, 2, "50%", 0, '', $imagecode );
			            break;
					case '0': // divider
					    $ret .= print_row_delim( $first_row, $arrpd, "panel", 2 );
				            $not_first_row = 1;
				            $first_row = 1;
					    break;
			        case 'e': // enum combo box
			            $ret .= print_row_enum( $first_row, $arrpd, $p_arr[$fname], "table", '', $rd, 2, "50%" );
			            break;
			        case 'en': // enum combo box with numbers
			            $ret .= print_row_enum_n( $first_row, $arrpd,$p_arr[$fname], "table", $rd, 2, "50%" );
			            break;
			        case 'eny': // enum combo box with years
			            $ret .= print_row_enum_years( $first_row, $arrpd, $p_arr[$fname], "table", $rd, 2, "50%", '', $sRealFName );
				    	$aRedundantFields[] = $sRealFName;
			            break;
					case 'date':
			            $ret .= print_row_date( $first_row, $arrpd, $p_arr[$fname], "table", $rd, 2, "50%", '', $sRealFName );
						break;
			        case 'c': // input box
			            $p_arr[$fname] = process_line_output( $p_arr[$fname] );
						
			            if( strlen( $p_arr[$fname] ) )
			            {
							if ( 'HomePage' == $fname )
								$p_arr[$fname] = '<a href="http://' . $p_arr[$fname] . '">' . $p_arr[$fname] . '</a>';
							$ret .= print_row_edit( $first_row, $arrpd, $p_arr[$fname], "table", $rd, 2, "50%" );
			            }
			            break;   
			        case 'p': // input box password
			            $p_arr[$fname] = process_line_output( $p_arr[$fname] );
			            $ret .= print_row_pwd( $first_row, $arrpd, $p_arr[$fname], "table", $rd, 2, "50%" );
			            break;
					default:
					    $not_first_row = 1;
				}
				if ( !$not_first_row && $first_row == 1 )
					$first_row = 0;
		    }
	    }
		return $ret;
	}
	
	function showBlockMp3( $sCaption )
	{
		global $logged;
		
		if( $logged['member'] )
		{
			$iMemberId = (int)$_COOKIE['memberID'];
			$ret = getApplicationContent('mp3', 'player', array('id' => $this -> _iProfileID, 'password' => getPassword($iMemberId), 'vId' => $iMemberId));
			echo DesignBoxContent( _t( $sCaption ), '<div align="center">' . $ret . '</div>', 1, $show_hide );
		}
	}

	function showBlockOtherSitesLink($sCaption)
	{
		global $site;
		
		$ID = $this->_iProfileID;

		// Get saved mylinks for this member from database
		$query = "
			SELECT * FROM MyLinks 
			WHERE owner = $ID
				AND TRIM(title) != '' 
				AND TRIM(url) != '' 
			ORDER BY position";
		$savedLinks = fill_assoc_array(db_res($query));
	
		if ((int)$_COOKIE['memberID'] != $ID && !count($savedLinks)) return false;
		
		if ((int)$_COOKIE['memberID'] == $ID)
			$editLink = <<<EOF
				<div class="title_content">
					<a href="profile_customize.php?show=links"><img src="{$site['icons']}description_edit.png" 
						class="captionIcon" alt="Edit"/></a>
					<a href="profile_customize.php?show=links" class="title_content_link">Edit</a>
				</div>
EOF;
		else
			$editLink = '';
	
		$content = '<div id="othersites">'.PageCompOtherSitesLinkContent($savedLinks).'</div>';	
		
		// Check whether is allowed to user to specify mylinks box title
		if (_t($sCaption) == '{mylinks_user_defined_title}')
		{
			// Set user defined box title if not empty or default value otherwise
			$mylinksBoxTitle = trim(db_value("SELECT MyLinksBoxTitle FROM ProfilesSettings WHERE IDMember = $ID"));
			$sCaption = ($mylinksBoxTitle != '') ? $mylinksBoxTitle : _t('_Link to my other sites default');
			echo DesignBoxContent($sCaption, $content, 1, $editLink);
		}
		else
			echo DesignBoxContent( _t( $sCaption), $content, 1, $editLink);
	}

	function showBlockNewsFeed($sCaption)
	{
		global $site;
		
		$ID = $this->_iProfileID;
		
		// Get news count on profile page
		$newsCountQuery = "SELECT NewsOnProfilePage 
			FROM ProfilesSettings 
			WHERE IDMember=$ID 
			LIMIT 1";
		$newsCount = db_value($newsCountQuery);
		if (!$newsCount) $newsCount = 10;

		$content = PrintNewsFeed($ID, false, $newsCount);
		
		if ((int)$_COOKIE['memberID'] == $ID)
			$editLink = <<<EOF
				<div class="title_content">
					<a href="profile_customize.php?show=newsfeed"><img src="{$site['icons']}description_edit.png" 
						class="captionIcon" alt="Edit"/></a>
					<a href="profile_customize.php?show=newsfeed" class="title_content_link">Edit</a>
				</div>
EOF;
		else
			$editLink = '';
		
		echo DesignBoxContent( _t( $sCaption), $content, 1, $editLink);
		
	}
}
?>