<?

/***************************************************************************
*                            Dolphin Smart Community Builder
*                              -------------------
*     begin                : Mon Mar 23 2006
*     copyright            : (C) 2007 BoonEx Group
*     website              : http://www.boonex.com
* This file is part of Dolphin - Smart Community Builder
*
* Dolphin is free software; you can redistribute it and/or modify it under
* the terms of the GNU General Public License as published by the
* Free Software Foundation; either version 2 of the
* License, or  any later version.
*
* Dolphin is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
* without even the implied warranty of  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
* You should have received a copy of the GNU General Public License along with Dolphin,
* see license.txt file; if not, write to marketing@boonex.com
***************************************************************************/

$aChat  = db_arr("SELECT `Name` FROM `Modules` WHERE `Type` = 'chat'");
$aForum = db_arr("SELECT `Name` FROM `Modules` WHERE `Type` = 'forum'");

$aGlobalVars['enable_chat']				= is_array($aChat) ?  1 : 0;
$aGlobalVars['enable_forum']			= is_array($aForum) ? 1 : 0;

/**
 * Determite if the current menu item is active
 *  $link_list   - list of related links, for actual link first link used.
 *                 link separator is ";"
 *  $path        - path to link file
 *  $dir         - is link to the file ( for example http://url/dir/file.html
 *                 or  link to the dir ( for example http://url/dir/ )
 *
 *  returns 1 if current link is active link
 **/
function IsMenuItemActive( $link_list, $path = "", $dir = 0 )
{
	global $link_arr;

    $inactive = 0;

    $link_arr = split(";",$link_list);

    $n = count ( $link_arr );
    for ( $n=0 ; $link = $link_arr[$n] ; ++$n )
    {
        $tmp = split("\?",$link);
        $link = $tmp[0];

        $arr1 = split("/",$_SERVER[SCRIPT_FILENAME]);
        $arr2 = split("/",$link);

        if ( $dir )
        {
            $s1 = $arr1[count($arr1)-2];
            $s2 = $arr2[count($arr2)-2];
        }
        else
        {
            $s1 = $arr1[count($arr1)-1];
            $s2 = $arr2[count($arr2)-1];
        }

        if ( strcasecmp($s1,$s2) == 0 )
        {
            $inactive = 1;
            break;
        }
    }

    return $inactive;
}

/**
 * Put menu item code for regular menu(vertical), this code automaticaly detect active menu item
 *  $suffix      - color of box ( end part of image for design box )
 *                 for example: left<$suffix>.gif
 *  $text        - text of menu item
 *  $onclick     - onClick event code
 *  $link_list   - list of related links, for actual link first link used.
 *                 link separator is ";"
 *  $path        - path to link file
 *  $dir         - is link to the file ( for example http://url/dir/file.html
 *                 or  link to the dir ( for example http://url/dir/ )
 *  $target      - target option of <A> html element
 **/
function MenuItem( $text, $link_list, $path, $icon_name, $target = "", $onclick='' )
{
	global $site;
	global $link_arr;

	$ret = '';

	if ( strlen( $target )  ) $target  = " target=\"$target\" ";
	if ( strlen( $onclick ) ) $onclick = " onclick=\"$onclick\" ";
	if ( !strlen( $path ) && !strlen($onclick) ) $path = $site['url'];

	$inactive = IsMenuItemActive( $link_list, $path, $dir);

			if( !$inactive )
			{
				$ret .= '<div class="menu_item_line" onmouseover="this.className=\'menu_item_line_active\'" onmouseout="this.className=\'menu_item_line\'">';
					$link = $link_arr['0'];
					$ret .= '<a href="' . $path . $link . '" class="menu_item_link" ' . $target . $onclick . '>';
						$ret .= $text;
					$ret .= '</a>';
				$ret .= '</div>';
			}

			else
			{
				$ret .= '<div class="menu_item_line">';
					$ret .= $text;
				$ret .= '</div>';
			}

	return $ret;
}

/**
 * Put menu item code for main menu(horizontal), this code automaticaly detect active menu item
 *  $text        - text of menu item
 *  $onclick     - onClick event code
 *  $linkt       - list of related links.
 *  $path        - path to link file
 *  $dir         - is link to the file ( for example http://url/dir/file.html
 *                 or  link to the dir ( for example http://url/dir/ )
 **/
function TopCodeMenuItem( $text, $link, $path='', $target='' )
{
    global $site;
    global $link_arr;
    global $logged;

	if ( !strlen( $path ) && !strlen($onclick) ) $path = $site['url'];

	$active = IsMenuItemActive( $link, $path );


    $ret = "";
	if ( !$active )
    {
    	$link = $link_arr['0'];
		$ret .= '<li>';
    		$ret .= '<div class="active" onmouseover="this.className=\'over\'" onmouseout="this.className=\'active\'">';
    			if (('_Chat' == $text)&&($logged['member']))
    			{
    				$ret .= '<a href="javascript:void(0);" onClick=\'javascript: window.open("' . $path . $link . '", "' . _t("$text") . '", "dependent,left=150,right=50,height=550,width=790,resizable=yes,scrollbars=no");\'>'. _t("$text") . '</a>';
    			}
    			else
    			{
    			$ret .= '<a href="' . $path . $link . '" ' . $target . ' title="' . _t("$text") . '">' . _t("$text") . '</a>';
    			}
    		$ret .= '</div>';
    	$ret .= '</li>';
	}
	else
	{
    	$ret .= '<li>';
    		$ret .= '<div class="inactive">';
    			$ret .= _t("$text");
    		$ret .= '</div>';
    	$ret .= '</li>';
	}

    return $ret;
}


/**
 * Menu for logged in member : it is different for different pages
 **/
function MenuLoggedMemberCustom( $CustomMenuFunc )
{
	global $site;

    $out = $CustomMenuFunc;

    return DesignBoxContent( _t("_Custom menu"), $out, 1 );
 }

/**
 * Main menu for admin
 **/
function MenuLoggedAdmin()
{
	global $site;
	global $oTemplConfig;

    $ret = "";

    $ret .= '<div class="menu_item_block">';
    $ret .= MenuItem( "Control panel", 'index.php', $site['url_admin'], '_member_panel.gif' );
    $ret .= MenuItem( "Global Settings", 'global_settings.php', $site['url_admin'], '_admin_global_set.gif' );
    $ret .= MenuItem( "Profiles", 'profiles.php', $site['url_admin'], '_admin_profiles.gif' );
    if ( $oTemplConfig -> bEnableCustomization )
        $ret .= MenuItem( "Post Moderate", 'post_mod_profiles.php', $site['url_admin'], '_admin_profiles.gif' );

    $ret .= MenuItem( "Affiliates", 'partners.php', $site['url_admin'], '_admin_partners.gif' );
    if ( !$oTemplConfig -> bFreeMode )
        $ret .= MenuItem( "Finance", 'finance.php', $site['url_admin'], '_admin_finance.gif' );
    $ret .= MenuItem( "Pricing Policy", 'contact_discounts.php', $site['url_admin'], '_admin_contact_dscoun.gif' );
    $ret .= MenuItem( "Manage PPs", 'payment_providers.php', $site['url_admin'], '_admin_manage_pp.gif' );
    $ret .= MenuItem( "Mass mailer", 'notifies.php', $site['url_admin'], '_admin_notifies.gif'  );
    $ret .= MenuItem( "Membership Levels", 'memb_levels.php', $site['url_admin'], '_admin_mem_levels.gif' );
    $ret .= MenuItem( "Index Compose", 'index_compose.php', $site['url_admin'], '_admin_index_compose.gif'  );
    $ret .= MenuItem( "Profile Fields", 'profile_fields.php', $site['url_admin'], '_admin_profile_fields.gif' );
    $ret .= MenuItem( "Split Join", 'split_join.php', $site['url_admin'], '_admin_split_join.gif' );
    $ret .= MenuItem( "Language File", 'lang_file.php', $site['url_admin'], '_admin_lang_file.gif' );
    $ret .= MenuItem( "CSS File", 'css_file.php', $site['url_admin'], '_admin_css_file.gif' );
    $ret .= MenuItem( "Links", 'links.php', $site['url_admin'], '_admin_links.gif' );
    $ret .= MenuItem( "Banners", 'banners.php', $site['url_admin'], '_admin_banners.gif' );
    $ret .= MenuItem( "News", 'news.php', $site['url_admin'], '_admin_news.gif' );
    $ret .= MenuItem( "Articles", 'articles.php', $site['url_admin'], '_admin_articles.gif' );
    $ret .= MenuItem( "Feedback", 'story.php', $site['url'], '_admin_feedback.gif' );
    $ret .= MenuItem( "Polls", 'polls.php', $site['url_admin'], '_admin_polls.gif' );
    $ret .= MenuItem( "Quotes", 'quotes.php', $site['url_admin'], '_admin_quotes.gif' );
    $ret .= MenuItem( "Log out", 'logout.php?action=admin_logout', $site['url'], '_logout.gif' );
    $ret .= "</div>";


    return DesignBoxContent( 'admin menu', $ret, 1 );
}

/**
 * Main menu for Affiliate
 **/
function MenuLoggedAff()
{
    global $site;


    $out = "";
    $out .= '<div class="menu_item_block">';
    $out .= MenuItem( "Home", 'index.php', '', '_home.gif' );
    $out .= MenuItem( "Control panel", 'index.php', $site['url_aff'], '_member_panel.gif' );
    $out .= MenuItem( "Profiles", 'profiles.php', $site['url_aff'], '_admin_profiles.gif' );
    $out .= MenuItem( "Finance", 'finance.php', $site['url_aff'], '_admin_finance.gif' );
    $out .= MenuItem( "Help", 'help.php', $site['url_aff'], '_admin_links.gif' );
    $out .= MenuItem( "Log out", 'logout.php?action=aff_logout', $site['url'], '_logout.gif' );
    $out .= "</div>";

    return DesignBoxContent( 'affiliate menu', $out, 1);
}


/**
 * Main menu for logged in member
 **/
function MenuLoggedMember()
{
	global $site;
	global $p_arr;
	global $oTemplConfig;

	$gid = (int)$_COOKIE['memberID'];

	$ret = MemberMenuDesign($gid);

    return DesignBoxContent( _t('_Member menu'), $ret, 1);
}

/**
 * Main menu for NOT logged in member
 **/
function MenuMember()
{
    global $site;

    $ret  = '';
    $ret .= MemberMenuDesign(0);

    return DesignBoxContent( _t('_Visitor menu'), $ret, 1);
}


/**
 * Put custom menu function code for communicator page
 **/
function CustomMenuFuncCC()
{
    global $site;

    $ret .= '<div class="menu_item_block">';
    $ret .= MenuItem( _t("_Communicator"), 'cc.php', '', '_communicator.gif' );
    $ret .= MenuItem( _t("_My Inbox"), 'inbox.php', '', '_inbox.gif' );
    $ret .= MenuItem( _t("_My Outbox"), 'outbox.php', '', '_outbox.gif' );
    $ret .= MenuItem( _t("_New Message"), 'compose.php', '', '_compose_message.gif' );
    $ret .= '</div>';

    return $ret;
}


/**
 * Custom Menu Function for Profile
 */
function CustomMenuFuncProfile2()
{
	global $site;
	global $p_arr;
	global $logged;
	global $oTemplConfig;

	$ret .= '<div class="menu_item_block">';
	$ret .= MenuItem(_t("_View profile"), 'profile.php?ID=' . $p_arr['ID'], '', '_view_profile.gif', '_blank' );
	$ret .= ( $oTemplConfig -> bEnableGuestbook ) ? MenuItem( _t("_guestbook"), 'guestbook.php?owner=' . $p_arr['ID'], '', '_guestbook.gif', '_blank' ) : '';
	if ( $logged['admin'] )
	{
		$ret .= ( $oTemplConfig -> bEnableBlog ) ? MenuItem( _t("_blog"), 'blog.php?owner=' . $p_arr['ID'], '', '_blog.gif', '_blank' ) : '';
		$ret .= ( $oTemplConfig -> bEnableGallery ) ? MenuItem( _t("_Gallery"), 'gallery.php?owner=' . $p_arr['ID'], '', '_gallery.gif', '_blank' ) : '';
	}

	$ret .= "</div>";

	return $ret;
}


/**
 * Custom Menu Function for Profile
 */
function CustomMenuFuncProfile()
{
	global $site;
	global $p_arr;
	global $ID;
	global $oTemplConfig;
	global $contact_allowed;
	global $logged;
	global $user_is_online;
	global $aBlogConfig;

	$ret = "";
	$ret .= '<div class="menu_item_block">';

    if ( $oTemplConfig -> bEnableIm && $user_is_online )
	{
		$ret .= MenuItem(_t("_Private message"), 'javascript:void(0);', '', '_private_message.gif', '', "launchAddToIM({$p_arr['ID']});" );
	}

	if ( ($p_arr['Video'] && $oTemplConfig->bEnableVideoUpload))
	{
		$ret .= MenuItem( _t("_view video"), 'javascript:void(0);', '', '_view_video.gif', '', "javascript: window.open( 'video_pop.php?ID={$p_arr['ID']}', '', 'width={$oTemplConfig -> popUpWindowWidth},height={$oTemplConfig -> popUpWindowHeight},menubar=no,status=no,resizable=no,scrollbars=yes,toolbar=no,location=no' );" );
	}
	elseif( $oTemplConfig -> bEnableVideoUpload && getParam( 'enable_ray_pro' ) == "on")
	{
		$ret .= MenuItem( _t("_view video"), 'javascript:void(0);', '', '_view_video.gif', '', "javascript: window.open( 'video_pop.php?ID={$p_arr['ID']}', 'video', 'width={$oTemplConfig -> popUpWindowWidth},height={$oTemplConfig -> popUpWindowHeight}, menubar=no,status=no,resizable=no,scrollbars=no,toolbar=no,location=no' );" );
	}

	if ( $p_arr['Sound'] && $oTemplConfig -> bEnableAudioUpload )
	{
		$ret .= MenuItem( _t("_listen voice"), 'javascript:void(0);', '', '_listen_voice.gif', '', "javascript: window.open( 'sound_pop.php?ID={$p_arr['ID']}', '', 'width={$oTemplConfig -> popUpWindowWidth},height={$oTemplConfig -> popUpWindowHeight},menubar=no,status=no,resizable=no,scrollbars=yes,toolbar=no,location=no,screenX=100,screenY=100' );" );
	}

	$ret .= MenuItem( _t('_get_media'), 'media_gallery.php?ID=' . $p_arr['ID'], '', '', '_blank' );
	if ( !$oTemplConfig -> bAnonymousMode )
	{
		//$ret .= MenuItem(_t("_GET_EMAIL"), 'freemail.php?ID=' . $p_arr['ID'], '', '_get_email.gif', '_blank' );
		$ret .= MenuItem(_t("_GET_EMAIL"), 'javascript:void(0);', '', '_get_email.gif', '', "javascript: window.open( 'freemail.php?ID={$p_arr['ID']}', '', 'width={$oTemplConfig -> popUpWindowWidth},height={$oTemplConfig -> popUpWindowHeight},menubar=no,status=no,resizable=no,scrollbars=yes,toolbar=no,location=no' );" );
	}
	$ret .= MenuItem( _t("_SEND_MESSAGE"), 'compose.php?ID=' . $p_arr['ID'], '', '_compose_message.gif', '_blank' );

	if ( !$oTemplConfig -> bMembershipOnly && !$oTemplConfig -> bFreeMode && $logged['member'] && !$contact_allowed )
	$ret .= MenuItem( _t("_ADD_TO_CART"), 'javascript:void(0);', '', '_cart.gif', '', "javascript: window.open( 'cart_pop.php?action=add&amp;ID={$p_arr['ID']}', 'cart_$ID', 'width={$oTemplConfig -> popUpWindowWidth},height={$oTemplConfig -> popUpWindowHeight},menubar=no,status=no,resizable=no,scrollbars=yes,toolbar=no, location=no' );" );

	//$ret .= MenuItem( _t("_send a kiss"), "vkiss.php?sendto=$p_arr[ID]", '', '_send_kiss.gif'  );
	$ret .= MenuItem( _t("_send a kiss"), 'javascript:void(0);', '', '_send_kiss.gif', '', "javascript: window.open( 'vkiss.php?sendto={$p_arr['ID']}', '', 'width={$oTemplConfig -> popUpWindowWidth},height={$oTemplConfig -> popUpWindowHeight},menubar=no,status=no,resizable=no,scrollbars=yes,toolbar=no,location=no' );"  );
	$ret .= MenuItem( _t("_email to frend"), 'javascript:void(0);', '', '_email_to_friend.gif', '', "return launchTellFriendProfile($ID);" );
	if( $oTemplConfig -> bEnableGuestbook )
	{
		$ret .= MenuItem( _t("_guestbook"), 'guestbook.php?owner=' . $p_arr['ID'], '', '_guestbook.gif' );
	}
	if( $oTemplConfig -> bEnableBlog )
	{
		$ret .= MenuItem( _t("_blog"), 'blog.php?owner=' . $p_arr['ID'], '', '_blog.gif' );
	}
	if( $oTemplConfig -> bEnableGallery )
	{
		$ret .= MenuItem( _t("_Gallery"), 'gallery.php?owner=' . $p_arr['ID'], '', '_gallery.gif' );
	}
	if ( $logged['member'] )
	{
		$ret .= MenuItem( _t("_block member"), 'javascript:void(0);', '', '_block_member.gif', '', "javascript:window.open( 'list_pop.php?action=block&amp;ID=$ID', '', 'width={$oTemplConfig -> popUpWindowWidth},height={$oTemplConfig -> popUpWindowHeight},menubar=no,status=no,resizable=no,scrollbars=yes,toolbar=no, location=no' );" );
		$ret .= MenuItem( _t("_friend member"), 'javascript:void(0);', '', '_friend_member.gif', '', "javascript:window.open( 'list_pop.php?action=friend&amp;ID=$ID', '', 'width={$oTemplConfig -> popUpWindowWidth},height={$oTemplConfig -> popUpWindowHeight},menubar=no,status=no,resizable=no,scrollbars=yes,toolbar=no, location=no' );" );
		$ret .= MenuItem( _t("_hot member"), 'javascript:void(0);', '', '_hot_member.gif', '', "javascript:window.open( 'list_pop.php?action=hot&amp;ID=$ID', '', 'width={$oTemplConfig -> popUpWindowWidth},height={$oTemplConfig -> popUpWindowHeight},menubar=no,status=no,resizable=no,scrollbars=yes,toolbar=no, location=no' );");
		$ret .= MenuItem( _t("_spam member"), 'javascript:void(0);', '', '_spam_report.gif', '', "javascript:window.open( 'list_pop.php?action=spam&amp;ID=$ID', '', 'width={$oTemplConfig -> popUpWindowWidth},height={$oTemplConfig -> popUpWindowHeight},menubar=no,status=no,resizable=no,scrollbars=yes,toolbar=no, location=no' );");
	}
	if ( $logged['admin'] )
	{
		$ret .= MenuItem(_t("_edit profile"), 'profile_edit.php?ID=' . $p_arr['ID'], '', '_edit_profile.gif', '_blank' );
	}

	$ret .= "</div>";

	return $ret;
}

function MenuLoggedModerator()
{
    global $site;
    global $_page;

    // Generate menu string.
    $ret = "";
    $ret .= '<div class="menu_item_block">';
    $ret .= MenuItem(	"Moderator panel", 'index.php', $site[url].'moderators/', '_member_panel.gif' );
    $ret .= MenuItem( _t("_Log Out"), 'logout.php?action=moderator_logout', $site['url'], '_logout.gif' );
    $ret .= "</div>";

    return DesignBoxContent('moderator menu', $ret, 1);
}

function TopMenuItems()
{
	global $tmi_letters;
	global $logged;
	global $site;


	$ret = '';


	$ret .= '<ul>';

	$ret .= TopCodeMenuItem( '_Home', 'index.php', $site['url'] );
	if ( $logged['member'] )
	{
		$ret .= TopCodeMenuItem( '_Control Panel', 'member.php', $site['url'] );
	}
	else
	{
		$ret .= TopCodeMenuItem( '_Join', 'join_form.php', $site['url'] );
	}

	if ( $logged['member'] )
	{
		$ret .= TopCodeMenuItem( '_FAQ', 'faq.php', $site['url'] );
	}
	else
	{
		$ret .= TopCodeMenuItem( '_Log In', 'member.php', $site['url'] );
	}

	$ret .= TopCodeMenuItem( '_Search', 'search.php', $site['url'] );
	$ret .= TopCodeMenuItem( '_Chat', 'aemodule.php?ModuleType=chat', $site['url'] );
	$ret .= TopCodeMenuItem( '_browse', 'browse.php', $site['url'] );
	$ret .= TopCodeMenuItem( '_rate', 'rate.php', $site['url'] );


	$ret .= '</ul>';


	return $ret;
}

?>