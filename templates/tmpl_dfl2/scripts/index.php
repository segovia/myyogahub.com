<?
/***************************************************************************
*                            Dolphin Smart Community Builder
*                              -------------------
*     begin                : Mon Mar 23 2006
*     copyright            : (C) 2007 BoonEx Group
*     website              : http://www.boonex.com
* This file is part of Dolphin - Smart Community Builder
*
* Dolphin is free software; you can redistribute it and/or modify it under
* the terms of the GNU General Public License as published by the
* Free Software Foundation; either version 2 of the
* License, or  any later version.
*
* Dolphin is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
* without even the implied warranty of  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
* You should have received a copy of the GNU General Public License along with Dolphin,
* see license.txt file; if not, write to marketing@boonex.com
***************************************************************************/

// --------------- page variables

$_ni = $_page['name_index'];
$_page_cont[$_ni]['compose_index_col1'] = PageCompCreateBlocks('compose_index_col1');
$_page_cont[$_ni]['compose_index_col2'] = PageCompCreateBlocks('compose_index_col2');

$_page_cont[$_ni]['daily_quotes']		= quote_get();
$_page_cont[$_ni]['promo_caption'] = _t("_web_community_site");
$_page_cont[$_ni]['powered_by_Dolphin'] = _t("_powered_by_Dolphin", 'http://www.boonex.com/products/dolphin/');
$_page_cont[$_ni]['welcome_and_join'] = _t("_welcome_and_join", $site['url'], 'join_form.php' );
$_page_cont[$_ni]['promo_list_1'] = _t('_promo_list_1');
$_page_cont[$_ni]['promo_list_2'] = _t('_promo_list_2');
$_page_cont[$_ni]['promo_list_3'] = _t('_promo_list_3');
$_page_cont[$_ni]['promo_list_4'] = _t('_promo_list_4');
$_page_cont[$_ni]['promo_list_5'] = _t('_promo_list_5');
$_page_cont[$_ni]['welcome'] = _t('_Welcome');
$_page_cont[$_ni]['member_login'] = _t('_Member Login');
$_page_cont[$_ni]['login_section'] = LoginSection($logged);
$_page_cont[$_ni]['top_mem_stat'] = PageCompTopMemberStat();
$_page_cont[$_ni]['latest_members'] =  PageCompLatestMembers();
$_page_cont[$_ni]['latest_loged'] =  PageCompLastLoggedMembers();

// --------------- [END] page components

PageCode();


// --------------- page components functions

/**
 * dynamically generates index page blocks
 */
function PageCompCreateBlocks( $gl_name )
{
	if ( getParam($gl_name) )
		$blocks = split(',', getParam($gl_name));
	else
		return '';

    $ret = '';
    foreach ( $blocks as $value )
    {
    	$func = 'PageComp' . $value;
    	$ret .= "<div id=\"$value\">" . $func() . "</div>";
	}

	return $ret;
}

/**
 * members statistic block
 */
function PageCompMemberStat( )
{
	return "";
	die;
	global $site;
	global $prof;
	global $oTemplConfig;


	$free_mode = getParam("free_mode") == "on" ? 1 : 0;

	// members statistics
	$total_c = strlen( $_POST['total_c'] ) ? $_POST['total_c'] : getParam( "default_country" );
	$total_c2 = strlen( $_POST['total_c2'] ) ? $_POST['total_c2'] : getParam( "default_country" );
	$total_arr = db_arr( "SELECT COUNT(ID) FROM `Profiles` WHERE Status = 'Active'" );
	$total_arr_week = db_arr( "SELECT COUNT(ID) FROM `Profiles` WHERE Status = 'Active' AND (TO_DAYS(NOW()) - TO_DAYS(LastReg)) <= 7" );
	if ( !$free_mode )
		$total_arr_gold = getMembersCount( MEMBERSHIP_ID_STANDARD, '', true );
	$total_c_arr = db_arr( "SELECT COUNT(ID) FROM `Profiles` WHERE Status = 'Active' AND `Country` = '". process_db_input($total_c2) ."'" );
	$total_members = $total_arr[0];
	$total_c_members = $total_c_arr[0];

	$total_c_members_onl = get_users_online_number('c', $total_c );
	$total_arr_chatting = get_users_online_number('t');
	$members_online = get_users_online_number();

	$c_arr = $prof['countries'];

//#########################################################
	$ret = '';
	$ret .= '<div class="membes_statistic_block">';
		$ret .= '<div class="clear_both"></div>';
		$ret .= '<div style="position:relative; float:left;"><img src="' . $site['icons'] . 'group.gif" alt="" /></div>';
		$ret .= '<div style="position:relative; float:left; white-space:nowrap; left:5px; font-weight:bold; color:#000;">' . _t("_Currently Online") . '</div>';
		$ret .= '<div class="clear_both"></div>';
		$ret .= '<div style="position:relative; border-bottom:1px solid silver; margin:5px 0px;"></div>';
		$ret .= '<div class="member_stat_block">';
			$ret .= '<div><img src="' . $site['icons'] . 'us.gif" alt="" /><span style="margin-left:5px;">' . _t("_Total") . '&nbsp;-&nbsp;' .  $members_online . '</span></div>';
			$ret .= '<div><img src="' . $site['icons'] . 'us.gif" alt="" /><span style="margin-left:5px;">' . _t("_Chatting") . '&nbsp;-&nbsp;' .  $total_arr_chatting . '</span></div>';
			$ret .= '<div><img src="' . $site['icons'] . 'us.gif" alt="" /><span style="margin-left:5px;">' . $total_c_members_onl . '&nbsp;' . _t("_members") . '&nbsp;' . _t("_from") . '</span></div>';
			$ret .= '<div style="padding-left:20px;">';
				$ret .= '<form action="'.$_SERVER['PHP_SELF'].'" method="post" name="cForm">';
				$ret .= '<input type="hidden" name="total_c" value="' . $total_c . '" />';
				$ret .= '<select style="width:123px;" name="total_c" onchange="javascript: document.forms[\'cForm\'].submit();">';
				foreach ( $c_arr as $key => $value )
				{
					$ret .= '<option value="' . $key . '"';
					if ( $key == $total_c )
						$ret .= ' selected="selected"';
					$ret .= '>' . _t( '__'.$value ) . '</option>';
				}
				$ret .= '</select>';
				$ret .= '</form>';
			$ret .= '</div>';
		$ret .= '</div>';
	$ret .= '</div>';
//#########################################################
	$ret .= '<div style="margin-top:10px;">';
		$ret .= '<div class="clear_both"></div>';
		$ret .= '<div style="position:relative; float:left;"><img src="' . $site['icons'] . 'group.gif" alt="" /></div>';
		$ret .= '<div style="position:relative; float:left; white-space:nowrap; left:5px; font-weight:bold; color:#000;">' . _t("_Total Registered") . '</div>';
		$ret .= '<div class="clear_both"></div>';
		$ret .= '<div style="position:relative; border-bottom:1px solid silver; margin:5px 0px;"></div>';
		$ret .= '<div class="member_stat_block">';
			$ret .= '<div><img src="' . $site['icons'] . 'us.gif" alt="" /><span style="margin-left:5px;">' . _t("_Total") . '&nbsp;-&nbsp;' .  $total_arr[0] . '</span></div>';
			if ( !$free_mode )
				$ret .= '<div><img src="' . $site['icons'] . 'us.gif" alt="" /><span style="margin-left:5px;">' . _t("_Gold Members") . '&nbsp;-&nbsp;' .  $total_arr_gold  . '</span></div>';

			$ret .= '<div><img src="' . $site['icons'] . 'us.gif" alt="" /><span style="margin-left:5px;">' . _t("_New this week") . '&nbsp;-&nbsp;' . $total_arr_week[0]  . '</span></div>';
			$ret .= '<div><img src="' . $site['icons'] . 'us.gif" alt="" /><span style="margin-left:5px;">' . $total_c_members . '&nbsp;' . _t("_members") . '&nbsp;' . _t("_from") . '</span></div>';
			$ret .= '<div style="padding-left:20px;">';
				$ret .= '<form action="'.$_SERVER['PHP_SELF'].'" method="post" name="cForm2">';
				$ret .= '<input type="hidden" name="total_c2" value="' . $total_c2 . '" />';
				$ret .= '<select style="width:123px;" name="total_c2" onchange="javascript: document.forms[\'cForm2\'].submit();">';
				foreach ( $c_arr as $key => $value )
				{
					$ret .= '<option value="' . $key . '"';
					if ( $key == $total_c2 )
						$ret .= ' selected="selected"';
					$ret .= '>' . _t( '__'.$value ) . '</option>';
				}
				$ret .= '</select>';
				$ret .= '</form>';
			$ret .= '</div>';
		$ret .= '</div>';
	$ret .= '</div>';
//#########################################################
	return DesignBoxContent ( _t("_members"), $ret, $oTemplConfig -> PageCompMemberStat_db_num);

}



/**
 * members statistic block
 */
function PageCompTopMemberStat( )
{
	global $site;
	global $prof;
	global $oTemplConfig;


	$free_mode = getParam("free_mode") == "on" ? 1 : 0;

	// members statistics
	$total_c = strlen( $_POST['total_c'] ) ? $_POST['total_c'] : getParam( "default_country" );
	$total_c2 = strlen( $_POST['total_c2'] ) ? $_POST['total_c2'] : getParam( "default_country" );
	$total_arr = db_arr( "SELECT COUNT(ID) FROM `Profiles` WHERE Status = 'Active'" );
	$total_arr_week = db_arr( "SELECT COUNT(ID) FROM `Profiles` WHERE Status = 'Active' AND (TO_DAYS(NOW()) - TO_DAYS(LastReg)) <= 7" );
	if ( !$free_mode )
		$total_arr_gold = getMembersCount( MEMBERSHIP_ID_STANDARD, '', true );
	$total_c_arr = db_arr( "SELECT COUNT(ID) FROM `Profiles` WHERE Status = 'Active' AND `Country` = '". process_db_input($total_c2) ."'" );
	$total_members = $total_arr[0];
	$total_c_members = $total_c_arr[0];

	$total_c_members_onl = get_users_online_number('c', $total_c );
	$total_arr_chatting = get_users_online_number('t');
	$members_online = get_users_online_number();

	$c_arr = $prof['countries'];

//#########################################################
	$ret = '';
	$ret .= '<div class="membes_statistic_block">';
		$ret .= '<div class="clear_both"></div>';
		//$ret .= '<div style="position:relative; float:left;"><img src="' . $site['icons'] . 'group.gif" alt="" /></div>';
		$ret .= '<div style="position:relative; float:left; white-space:nowrap; left:5px; font-weight:bold; color:#000;">' . _t("_Members_Online") . ":$members_online</div>";
		$ret .= '<div class="clear_both"></div>';
	//	$ret .= '<div style="position:relative; border-bottom:1px solid silver; margin:5px 0px;"></div>';
		
	$ret .= '</div>';
//#########################################################
	$ret .= '<div class="membes_statistic_block">';
		$ret .= '<div class="clear_both"></div>';
		
		$ret .= '<div style="position:relative; float:left; white-space:nowrap; left:5px; font-weight:bold; color:#000;">' . _t("_Total Registered") . ":$total_arr[0]</div>";
		$ret .= '<div class="clear_both"></div>';
		$ret .= '</div>';
		$ret .= '<div class="membes_statistic_block">';
		$ret .= '<div style="position:relative; float:left; white-space:nowrap; left:5px; font-weight:bold; color:#000;">' . _t("_New this week") . ":$total_arr_week[0] </div>";
		$ret .= '<div class="clear_both"></div>';
		
	$ret .= '</div>';
//#########################################################
	//return DesignBoxContent ( _t("_members"), $ret, $oTemplConfig -> PageCompMemberStat_db_num);
	return $ret;
}


/**
 * Top Rated Profiles block
 */
function PageCompTopRated()
{
    global $site;
	global $max_voting_mark;
	global $oTemplConfig;
	global $max_thumb_height;
	global $max_thumb_width;

	// most rated profiles

	$rate_max = get_max_votes_profile();

	$rate_memb_week   = db_arr( "SELECT Headline, NickName, Member, COUNT(*) AS `count`, SUM(Mark)/COUNT(*) AS mark, ID, Pic_0_addon  FROM `Votes` INNER JOIN Profiles ON (ID = Member) WHERE Status = 'Active' AND TO_DAYS(NOW()) - TO_DAYS(`Date`) <= 7 GROUP BY Member HAVING `count` > 0 ORDER BY Mark DESC LIMIT 1" );
	$rate_memb_month  = db_arr( "SELECT Headline, NickName, Member, COUNT(*) AS `count`, SUM(Mark)/COUNT(*) AS mark, ID, Pic_0_addon  FROM `Votes` INNER JOIN Profiles ON (ID = Member) WHERE Status = 'Active' AND TO_DAYS(NOW()) - TO_DAYS(`Date`) <= 30 GROUP BY Member HAVING `count` > 0 ORDER BY Mark DESC LIMIT 1" );

	//$rate_memb_week   = db_arr( "SELECT Headline, NickName, Member, COUNT(*) AS `count`, SUM(Mark)/COUNT(*) AS mark, ID, Pic_0_addon  FROM `Votes` INNER JOIN Profiles ON (ID = Member) WHERE Status = 'Active' AND TO_DAYS(NOW()) - TO_DAYS(`Date`) <= 7 GROUP BY Member ORDER BY Mark DESC,`count` DESC LIMIT 1" );
	//$rate_memb_month  = db_arr( "SELECT Headline, NickName, Member, COUNT(*) AS `count`, SUM(Mark)/COUNT(*) AS mark, ID, Pic_0_addon  FROM `Votes` INNER JOIN Profiles ON (ID = Member) WHERE Status = 'Active' AND TO_DAYS(NOW()) - TO_DAYS(`Date`) <= 30 GROUP BY Member ORDER BY Mark DESC,`count` DESC LIMIT 1" );


	$ret = '';
	//$ret .= '[' . $rate_memb_week['ID'] . ']';
	$ret .= '<div id="prof_of_week">';
		$ret .= '<div class="top_rated_head">';
			$ret .= _t("_Profile of the week");
		$ret .= '</div>';
		if( 0 < $rate_memb_week['ID'] )
		{
			$ret .= get_member_thumbnail($rate_memb_week['ID'], left);
			$ret .= '<div class="rate_block_position">';
				$ret .= DesignProgressPos( _t("_score") . ':&nbsp;' . $rate_memb_week['mark'], $oTemplConfig -> index_progressbar_w, $max_voting_mark, $rate_memb_week['mark'] );
				$ret .= DesignProgressPos( _t("_votes") . ':&nbsp;' . $rate_memb_week['count'], $oTemplConfig -> index_progressbar_w, $rate_max, $rate_memb_week['count'], 2);
			$ret .= '</div>';
		}
		else
		{
			$ret .= '<div class="top_prof_not_avail">';
				$ret .= '<div class="no_result">';
					$ret .= '<div>';
						$ret .= _t("_no_top_week");
					$ret .= '</div>';
				$ret .= '</div>';
			$ret .= '</div>';
		}
	$ret .= '</div>';
	$ret .= '<div class="top_rated_divider"></div>';
//#####################################################################
	$ret .= '<div id="prof_of_month">';
		$ret .= '<div class="top_rated_head">';
			$ret .= _t("_Profile of the month");
		$ret .= '</div>';
		if( 0 < $rate_memb_month['ID'] )
		{
			$ret .= get_member_thumbnail($rate_memb_month['ID'], left);
			$ret .= '<div class="rate_block_position">';
				$ret .= DesignProgressPos( _t("_score") . ':&nbsp;' . $rate_memb_month['mark'], $oTemplConfig -> index_progressbar_w, $max_voting_mark, $rate_memb_month['mark'] );
				$ret .= DesignProgressPos( _t("_votes") . ':&nbsp;' . $rate_memb_month['count'], $oTemplConfig -> index_progressbar_w, $rate_max, $rate_memb_month['count'], 2 );
			$ret .= '</div>';
		}
		else
		{
			$ret .= '<div class="top_prof_not_avail">';
				$ret .= '<div class="no_result">';
					$ret .= '<div>';
						$ret .= _t("_no_top_month");
					$ret .= '</div>';
				$ret .= '</div>';
			$ret .= '</div>';
		}
	$ret .= '</div>';


	return DesignBoxContent ( _t("_top rated"), $ret, $oTemplConfig -> PageCompTopRated_db_num );

}

/**
 * News Letters block
 */
function PageCompNewsLetters()
{
	global $site;
	//global $tmpl;
	global $oTemplConfig;



	$ret = '';
	$ret .= '<div class="text">' . _t("_SUBSCRIBE_TEXT", $site['title']) . '</div>';
	$ret .= '<div class="email_here" style="border:0px solid red; text-align:center; margin-bottom:5px;">' . _t("_YOUR_EMAIL_HERE") . ':</div>';
	$ret .= '<form action="' . $_SERVER['PHP_SELF'] . '" method="post">';
		$ret .= '<input type="hidden" name="total_c2" value="' . $total_c2 . '" />';
		$ret .= '<input type="hidden" name="total_c" value="' . $total_c . '" />';
		$ret .= '<div class="input" style="border:0px solid red; text-align:center; margin-bottom:5px;">';
			$ret .= '<input name="subscribe" type="text" size="18" />';
		$ret .= '</div>';
		$ret .= '<div class="input" style="border:0px solid red; text-align:center; padding:1px;">';
			$ret .= '<input type="image" src="' . $site['images'] . 'newsletter_submit.jpg" style="border:none;" />';
			$ret .= '<input type="hidden" name="subscribe_submit" value="true" />' . "\n";
		$ret .= '</div>';
	$ret .= '</form>';

	return DesignBoxContent ( _t("_newsletter"), $ret, $oTemplConfig -> PageCompNewsLetters_db_num );

}

/**
 * Success story  block
 */
function PageCompSuccessStory()
{
    global $site;
	global $oTemplConfig;


	//get random success story
	$story_limit_chars	= getParam("max_story_preview");
	$sStoryQuery = "
		SELECT		`Profiles`.`ID`,
					`Profiles`.`NickName`,
					LEFT (`Stories`.`Text`, '$story_limit_chars') AS Text,
					`Stories`.`Header`,
					`Stories`.`ID` AS storyID
		FROM		`Stories`
		INNER JOIN	`Profiles` ON ( `Profiles`.`ID` = `Stories`.`Sender` )
		WHERE		`Stories`.`active` = 'on'
		AND			`Profiles`.`Status` = 'Active'
		ORDER BY RAND()
		LIMIT 1
	";
	$story_arr			= db_assoc_arr($sStoryQuery);
	$story_count		= db_arr("SELECT COUNT(ID) FROM `Stories` WHERE `active` = 'on'");

	$ret = '';
	if( $story_arr )
	{
		if ( strlen($story_arr['Text']) == $story_limit_chars ) $story_arr['Text'] .= "...";
		$ret .= '<div class="subject">';
			$ret .= '<a href="' . $site['url'] . 'story_view.php?ID=' . $story_arr['storyID'] . '" class="bottom_text">';
				$ret .= process_line_output( $story_arr['Header'] );
			$ret .= '</a>';
		$ret .= '</div>';
		$ret .= '<div class="text">';
			$ret .= process_text_withlinks_output( $story_arr['Text'] );
		$ret .= '</div>';
		$ret .= '<div class="author">';
			$ret .= '<a href="' . $site['url'] . 'profile.php?ID=' . $story_arr['ID'] . '">';
				$ret .= process_line_output( $story_arr['NickName'] );
			$ret .= '</a>';
		$ret .= '</div>';
	}
	else
	{
		$ret .= '<div class="no_result"><div>';
			$ret .= _t("_No success story available.");
		$ret .= '</div></div>';
	}
	if( $story_count['0'] > 1 )
	{
		$ret .= '<div style="position:relative; text-align:center;">';
			$ret .= '<a href="' . $site['url'] . 'stories.php">' . _t("_Read more") . '</a>';
		$ret .= '</div>';
	}

	return DesignBoxContent ( _t("_success story"), $ret, $oTemplConfig -> PageCompSuccessStory_db_num);
}

/**
 * Latest News block
 */
function PageCompNews()
{
    global $site;
	global $oTemplConfig;
	global $short_date_format;

	// news
	$news_limit_chars = getParam("max_news_preview");
	$max_news_on_home = getParam("max_news_on_home");

	$news_res = db_res("SELECT LEFT (`Text`, $news_limit_chars) AS `Text`, LEFT(`Header`, 35) AS `Header`,  `News`.`ID` AS `newsID`, DATE_FORMAT(`Date`, '$short_date_format' ) AS 'Date' FROM `News` ORDER BY `Date` DESC LIMIT $max_news_on_home");

	$news_count = db_arr("SELECT COUNT(ID) FROM `News`");
	$news_counter = $news_count['0'];

	$ret = '';

	if( $news_counter > 0 )
	{
		while( $news_arr = mysql_fetch_assoc($news_res) )
		{
			$ret .= '<div class="news_title">';
				$ret .= '<img src="' . $site['icons'] . 'news.gif" alt="" />';
				$ret .= '<span style="position:relative; left:5px; bottom:3px;">';
					$ret .= '<a href="' . $site['url'] . 'news_view.php?ID=' . $news_arr['newsID'] . '">';
						$ret .= process_line_output( $news_arr['Header']  );
					$ret .= '</a>';
				$ret .= '</span>';
			$ret .= '</div>';
			$ret .= '<div class="news_text">';
				$ret .= process_text_withlinks_output( $news_arr['Text'] );
			$ret .= '</div>';
			$ret .= '<div class="news_date">' . $news_arr['Date'] . '</div>';
			$ret .= '<div class="news_divider"></div>';
		}

		if( $news_counter > $max_news_on_home )
		{
			$ret .= '<div style="position:relative; text-align:center;">';
				$ret .= '<a href="' . $site['url'] . 'news.php">' . _t("_Read news in archive") . '</a>';
			$ret .= '</div>';
		}
	}
	else
	{
		$ret .= '<div class="no_result"><div>' . _t("_No news available") . '</div></div>';
	}


	return DesignBoxContent( _t("_latest news"), $ret, $oTemplConfig -> PageCompNews_db_num );
}

/**
 * Survey block
 */
function PageCompSurvey()
{
    global $site;
    global $oTemplConfig;

	// survey
	$survey_arr = db_arr("SELECT `Question`, `ID` FROM `polls_q` WHERE `Active` = 'on' ORDER BY RAND() LIMIT 1");
	$survey_a_res = db_res("SELECT `Answer`, `IDanswer` FROM `polls_a` WHERE ID = '" . (int)$survey_arr['ID'] . "'");


	$ret = '';
	if( $survey_arr )
	{
		$poll_question = process_line_output( $survey_arr['Question'] );
		$ret .= '<div class="survey_block">';
			$ret .= '<div class="survey_question">' . $poll_question . '</div>';
			$ret .= '<div class="survey_answer_block">';
			$ret .= '<form method="post" action="poll.php">';
				$ret .= '<input type="hidden" name="ID" value="' . $survey_arr['ID'] . '" />';
				$j = 1;
				while ( $survey_a_arr = mysql_fetch_array($survey_a_res) )
				{
					if( ($j%2) == 0 )
					{
						$add = 'style="background-color:#E6E6E6;"';
					}
					else
					{
						$add = '';
					}

					$answer_text = process_line_output( $survey_a_arr['Answer'] );
					$ret .= '<div class="survey_answer" ' . $add . '>';
						$ret .= '<input type="radio" name="vote" id="ans' . $survey_a_arr['IDanswer'] . '" value="' . $survey_a_arr['IDanswer'] . '"  style="background-color:transparent;"/>';
						$ret .= '<span style="margin-left:5px;"><label for="ans' . $survey_a_arr['IDanswer'] . '">' . $answer_text . '</label></span>';
					$ret .= '</div>';

					$j ++;
				}
				$ret .= '<div style="margin-top:10px; height:auto; line-height:18px; vertical-align:middle; text-align:center; border:0px solid red;">';
					$ret .= '<span>';
						$ret .= '<input type="image" src="' . $site['images'] . 'survey_submit.gif" style="border:none;" />';
					$ret .= '</span><br />';
					$ret .= '<span style="margin-bottom:2px;">';
						$ret .= ' <a href="poll.php?ID=' . $survey_arr['ID'] . '">' . _t("_Results") . '</a> | <a href="polls.php">' . _t("_Polls") . '</a>';
					$ret .= '</span>';
				$ret .= '</div>';
			$ret .= '</form>';
			$ret .= '</div>';
		$ret .= '</div>';
	}
	else
	{
		$ret .= '<div class="no_result"><div>' . _t("_No polls available") . '</div></div>';
	}

	return DesignBoxContent (  _t("_survey"), $ret, $oTemplConfig -> PageCompSurvey_db_num );

}

/**
 * Featured members block block
 */
function PageCompFeatured()
{
	global $site;
	global $prof;
	global $oTemplConfig;
	global $max_thumb_width;
	global $max_thumb_height;


	$feature_num 	= getParam('featured_num');
	$feature_mode 	= getParam("feature_mode");

	// get random featured profiles
	//$max_thumb_width = getParam("thumb_width");
	//$max_thumb_height = getParam ("thumb_height");

	if ( $feature_num )
	{
		$featured_res = db_res( "SELECT `Profiles`.`ID`, `Pic_0_addon`, `ProfileType`, `NickName`, `Headline`, `Sex`, `Country`, `DateOfBirth`, `DateOfBirth2` FROM `Profiles` WHERE `Status` = 'Active' AND `Featured` = '1' ORDER BY RAND() LIMIT $feature_num" );

		$ret = "";
		if( mysql_num_rows( $featured_res ) > 0 )
		{

			$j=1;
			while( $featured_arr = mysql_fetch_assoc( $featured_res ) )
			{
				if ( $p_arr['ProfileType'] == "couple")
				{
					$age1_str = _t("_y/o", age( $featured_arr['DateOfBirth'] ));
					$age2_str = _t("_y/o", age( $featured_arr['DateOfBirth2'] ));
					$y_o_sex = $age1_str ._t('_'.$featured_arrr['Sex']) .'; '. $age2_str ._t('_'.$featured_arr['Sex2']);
				}
				else
				{
					$age_str = _t("_y/o", age( $featured_arr['DateOfBirth'] ));
					$y_o_sex = $age_str . '&nbsp;' . _t("_".$featured_arr['Sex']);
				}


				$featured_coutry = _t("__".$prof['countries'][$featured_arr['Country']]);


				if( $j%2 == '0' )
				{
					$style_add = 'style="border:none; left:20px; width:265px;"';
				}
				else
				{
					$style_add = '';
				}

				$ret .= '<div class="featured_block" ' . $style_add . '>';
					$ret .= get_member_thumbnail( $featured_arr['ID'], left );
					$ret .= '<div class="featured_info">';
						$ret .= '<div class="featured_nickname">';
							$ret .= '<a href="' . $site['url'] . 'profile.php?ID=' . $featured_arr['ID'] . '">';
								$ret .= $featured_arr['NickName'];
							$ret .= '</a>';
						$ret .= '</div>';
						$ret .= '<div>';
							$ret .= $y_o_sex . '<br />' . $featured_coutry;
						$ret .= '</div>';
					$ret .= '</div>';
				$ret .= '</div>';

				$j++;
			}
		}
		else
		{
			$ret .= '<div class="no_result">';
				$ret .= '<div>';
					$ret .= _t("_No results found");
				$ret .= '</div>';
			$ret .= '</div>';
		}

	}

	return DesignBoxContent( _t("_featured members"), $ret, $oTemplConfig -> PageCompFeatured_db_num );
}


function PageCompQuickSearchIndex()
{
    global $site;
    global $oTemplConfig;
    global $search_start_age;
	global $search_end_age;

    $gl_search_start_age    = (int)$search_start_age;
    $gl_search_end_age      = (int)$search_end_age;

    if ( $_COOKIE['memberID'] > 0 )
    {
        $arr_sex = db_arr("SELECT Sex FROM Profiles WHERE ID = ".(int)$_COOKIE['memberID']);
        $member_sex = $arr_sex[Sex];
    }
    else
        $member_sex = 'male';


	$ret = '';
	$ret .= '<form action="search_result.php" method="get">';
	$ret .= '<div class="qsi_line">';
		$ret .= '<div class="qsi_first">';
			$ret .= _t("_I am a");
		$ret .= '</div>';
		$ret .= '<div class="qsi_second">';
			$ret .= '<select name="Sex" style="width:100px;">';
				$ret .= SelectOptions("Sex", $member_sex);
			$ret .= '</select>';
		$ret .= '</div>';
	$ret .= '</div>';

	$ret .= '<div class="qsi_line">';
		$ret .= '<div class="qsi_first">';
			$ret .= _t("_seeking a");
		$ret .= '</div>';
		$ret .= '<div class="qsi_second">';
			$ret .= '<select name="LookingFor" style="width:100px;">';
				$ret .= SelectOptions("LookingFor", ($member_sex=='male' ? 'female':'male') );
			$ret .= '</select>';
		$ret .= '</div>';
	$ret .= '</div>';

	$ret .= '<div class="qsi_line">';
		$ret .= '<div class="qsi_first">';
			$ret .= _t("_aged");
		$ret .= '</div>';
		$ret .= '<div class="qsi_second">';
			$ret .= '<span style="position:absolute; top:0px; left:0px;">';
				$ret .= '<select name="DateOfBirth_start" style="width:38px;">';
				for ( $i = $gl_search_start_age ; $i < $gl_search_end_age ; $i++ )
				{
					$sel = $i == $gl_search_start_age ? 'selected="selected"' : '';
					$ret .= '<option value="' . $i . '" ' . $sel . '>' . $i . '</option>';
				}
				$ret .= '</select>';
			$ret .= '</span>';
			$ret .= '<div style="position:absolute; top:2px; left:45px;">';
				$ret .= _t("_to");
			$ret .= '</div>';
			$ret .= '<span style="position:absolute; top:0px; right:0px;">';
				$ret .= '<select name="DateOfBirth_end" style="width:38px;">';
				for ( $i = $gl_search_start_age ; $i <= $gl_search_end_age ; $i++ )
				{
					$sel = ($i == $gl_search_end_age) ? 'selected="selected"' : '';
					$ret .= '<option value="' . $i . '" ' . $sel . '>' . $i . '</option>';
				}
				$ret .= '</select>';
			$ret .= '</span>';
		$ret .= '</div>';
	$ret .= '</div>';

	$ret .= '<div class="qsi_line">';
		$ret .= '<div class="qsi_first">';
			$ret .= _t("_within");
		$ret .= '</div>';
		$ret .= '<div class="qsi_second">';
			$ret .= '<span style="position:absolute; top:0px; left:0px;">';
				$ret .= '<input type="text" name="distance" style="width:40px;" />';
			$ret .= '</span>';
			$ret .= '<span style="position:absolute; top:0px; right:0px;" >';
				$ret .= '<select name="metric" style="width:55px;">';
					$ret .= '<option selected="selected" value="miles">' . _t("_miles") . '</option>';
					$ret .= '<option value="km">' . _t("_km") . '</option>';
				$ret .= '</select>';
			$ret .= '</span>';
		$ret .= '</div>';
	$ret .= '</div>';

	$ret .= '<div class="qsi_line">';
		$ret .= '<div class="qsi_first">';
			$ret .= _t("_from ZIP");
		$ret .= '</div>';
		$ret .= '<div class="qsi_second">';
			$ret .= '<input type="text" name="zip" style="width:97px;" />';
		$ret .= '</div>';
	$ret .= '</div>';

	$ret .= '<div class="qsi_line">';
		$ret .= '<div class="qsi_first">';
			$ret .= '<label for="qsi_photos_only">' . _t("_With photos only") . '</label>';
		$ret .= '</div>';
		$ret .= '<div class="qsi_second">';
			$ret .= '<input type="checkbox" name="photos_only" id="qsi_photos_only" style="width:15px; height:15px; padding:0px; margin:0px;" />';
		$ret .= '</div>';
	$ret .= '</div>';

	$ret .= '<div class="qsi_line" style="text-align:center; margin-top:5px; height:auto;">';
		$ret .= '<input type="image" src="' . $site['images'] . 'qsi_submit.jpg" style="border:none;" />';
	$ret .= '</div>';
	$ret .= '</form>';

	return DesignBoxContent( _t("_Quick Search"), $ret, $oTemplConfig -> DesignQuickSearchIndex_db_num);
}

function PageCompTopMembers()
{
    global $site;
    global $prof;
    global $enable_match;
    global $logged;
    global $pa_icon_preload;
	global $tmpl;
	global $oTemplConfig;

	// number of profiles
	$max_num	= (int)getParam("top_members_max_num");
	$mode		= process_db_input( getParam("top_members_mode") );

	//	Get Sex from GET data
	if ( $_GET['Sex'] && $_GET['Sex'] != "all" )
	{
		$sex = process_db_input( htmlspecialchars_adv($_GET['Sex'] ));
		if ( 'couple' == $sex )
			$query_add = " AND `ProfileType` = 'couple' ";
		else
			$query_add = " AND `Sex` = '$sex' AND `ProfileType` != 'couple' ";
	}
	else
	{
		$sex = "all";
		$query_add = "";
	}

	$query = "SELECT Profiles.ID, Profiles.Headline, Profiles.Country, Profiles.Occupation,
		Profiles.City, Profiles.Sex, Profiles.NickName, Profiles.Children, Profiles.MerchantPrice,
		LEFT( Profiles.DescriptionMe, 180 ) AS DescriptionMe, LEFT( Profiles.DescriptionYou, 100 ) AS DescriptionYou,
		Profiles.DateOfBirth, Profiles.Pic_0_addon, Profiles.Sound, Profiles.ProfileType, Profiles.Sex2, Profiles.DateOfBirth2";

	//$ret = "<form name=\"toplist\" action=\"{$_SERVER['PHP_SELF']}\" method=\"get\">";
	if ( strlen($_GET['Mode']) )
		$mode = htmlspecialchars_adv($_GET['Mode']);

	switch ( $mode )
	{
		// online members
		case 'online':
			$online_time = (int)getParam( "member_online_time" );
			$query .= " FROM `Profiles` WHERE `LastNavTime` > SUBDATE(NOW(), INTERVAL $online_time MINUTE) AND `Status` = 'Active' $query_add LIMIT $max_num";
			$menu .= _t("_Online Members") ." | ".
					"<a href=\"{$_SERVER['PHP_SELF']}?Mode=rand&amp;Sex=$sex\" class=\"top_members_menu\">". _t("_Random Members") ."</a>" . " | ".
					"<a href=\"{$_SERVER['PHP_SELF']}?Mode=last&amp;Sex=$sex\" class=\"top_members_menu\">". _t("_Latest Members") ."</a>" . " | ".
					"<a href=\"{$_SERVER['PHP_SELF']}?Mode=top&amp;Sex=$sex\" class=\"top_members_menu\">". _t("_Top Members") ."</a>";
			break;

		// random members
		case 'rand':
			$query .= " FROM `Profiles` WHERE `Status` = 'Active' $query_add ORDER BY RAND() LIMIT $max_num";
			$menu .=	"<a href=\"{$_SERVER['PHP_SELF']}?Mode=online&amp;Sex=$sex\" class=\"top_members_menu\">". _t("_Online Members") ."</a>" . " | ".
					_t("_Random Members") ." | ".
					"<a href=\"{$_SERVER['PHP_SELF']}?Mode=last&amp;Sex=$sex\" class=\"top_members_menu\">". _t("_Latest Members") ."</a>" . " | ".
					"<a href=\"{$_SERVER['PHP_SELF']}?Mode=top&amp;Sex=$sex\" class=\"top_members_menu\">". _t("_Top Members") ."</a>";
			break;

		// last registered members
		case 'last':
			$query .= " FROM `Profiles` WHERE `Status` = 'Active' $query_add ORDER BY `LastReg` DESC LIMIT $max_num";
			$menu .=	"<a href=\"{$_SERVER['PHP_SELF']}?Mode=online&amp;Sex=$sex\" class=\"top_members_menu\">". _t("_Online Members") ."</a>" . " | ".
					"<a href=\"{$_SERVER['PHP_SELF']}?Mode=rand&amp;Sex=$sex\" class=\"top_members_menu\">". _t("_Random Members") ."</a>" . " | ".
					_t("_Latest Members") ." | ".
					"<a href=\"{$_SERVER['PHP_SELF']}?Mode=top&amp;Sex=$sex\" class=\"top_members_menu\">". _t("_Top Members") ."</a>";
			break;

		// top rated members
		case 'top':
			$query .= ", COUNT(*) AS `count`, SUM(Mark) / COUNT(*) AS `mark` FROM `Votes` INNER JOIN `Profiles` ON (`ID` = `Member`) WHERE `Status` = 'Active' $query_add GROUP BY `Member` HAVING `count` > 2 ORDER BY `Mark` DESC LIMIT $max_num";
			$menu .=	"<a href=\"{$_SERVER['PHP_SELF']}?Mode=online&amp;Sex=$sex\" class=\"top_members_menu\">". _t("_Online Members") ."</a>" . " | ".
					"<a href=\"{$_SERVER['PHP_SELF']}?Mode=rand&amp;Sex=$sex\" class=\"top_members_menu\">". _t("_Random Members") ."</a>" . " | ".
					"<a href=\"{$_SERVER['PHP_SELF']}?Mode=last&amp;Sex=$sex\" class=\"top_members_menu\">". _t("_Latest Members") ."</a>" . " | ".
					_t("_Top Members");
			break;
	}

	$ret .= '<div id="top_members_menu">' . $menu . '</div>';

	$ret .= '<div class="top_members_select">';
	$ret .= "<form name=\"toplist\" action=\"{$_SERVER['PHP_SELF']}\" method=\"get\">";
	$ret .=_t("_Sex") . ":&nbsp;&nbsp;
		<select name=\"Sex\" onchange=\"javascript: document.forms['toplist'].submit();\">
			". SelectOptions("Sex", $sex) ."
			<option value=\"all\" ". ( $sex == "all" ? "selected=\"selected\"" : "") .">". _t("_all") ."</option>
		</select>
		<input type=\"hidden\" name=\"Mode\" value=\"$mode\" />

		</form>\n";
	$ret .= '</div>';

	$result = db_res( $query );

	// Read search template from the file

	$fn_sr = "{$dir['root']}templates/tmpl_{$tmpl}/topmebers_index.html";

	$fs = filesize( $fn_sr );
	$f = fopen( $fn_sr, "r" );
	$templ_search = fread( $f, $fs );
	fclose( $f );

	$j = 1;
	if( mysql_num_rows($result) > 0 )
	{
		while ( $p_arr = mysql_fetch_array( $result ) )
		{
			$ret .= PrintSearhResult( $p_arr, $templ_search );

			if( $j != $max_num )
			{
				$ret .= '<div class="top_member_divider"></div>';
			}

			$j ++;
		}
	}
	else
	{
		$ret .= '<div class="no_result">';
			$ret .= '<div>';
				$ret .= _t("_No results found");
			$ret .= '</div>';
		$ret .= '</div>';
	}

	return DesignBoxContent( _t("_Top Members"), $ret,$oTemplConfig -> PageCompTopMembers_db_num );
}



function PageCompLastLoggedMembers()
{
    global $site;
    global $prof;
    global $enable_match;
    global $logged;
    global $pa_icon_preload;
	global $tmpl;
	global $oTemplConfig;

	// number of profiles
	//$max_num	= (int)getParam("top_members_max_num");
	$max_num	= 10;
	$mode		= process_db_input( getParam("top_members_mode") );

	//	Get Sex from GET data
	if ( $_GET['Sex'] && $_GET['Sex'] != "all" )
	{
		$sex = process_db_input( htmlspecialchars_adv($_GET['Sex'] ));
		if ( 'couple' == $sex )
			$query_add = " AND `ProfileType` = 'couple' ";
		else
			$query_add = " AND `Sex` = '$sex' AND `ProfileType` != 'couple' ";
	}
	else
	{
		$sex = "all";
		$query_add = "";
	}

	$query = "SELECT Profiles.ID, Profiles.Headline, Profiles.Country, Profiles.Occupation,
		Profiles.City, Profiles.Sex, Profiles.NickName, Profiles.Children, Profiles.MerchantPrice,
		LEFT( Profiles.DescriptionMe, 180 ) AS DescriptionMe, LEFT( Profiles.DescriptionYou, 100 ) AS DescriptionYou,
		Profiles.DateOfBirth, Profiles.Pic_0_addon, Profiles.Sound, Profiles.ProfileType, Profiles.Sex2, Profiles.DateOfBirth2";
		$query .= " FROM `Profiles` WHERE `Status` = 'Active' $query_add ORDER BY `LastLoggedIn` DESC LIMIT $max_num";

	//$ret = "<form name=\"toplist\" action=\"{$_SERVER['PHP_SELF']}\" method=\"get\">";
	

	$ret .= '<div id="top_members_menu">' . $menu . '</div>';

	

	$result = db_res( $query );

	// Read search template from the file

	$fn_sr = "{$dir['root']}templates/tmpl_{$tmpl}/lastloged_index.html";

	$fs = filesize( $fn_sr );
	$f = fopen( $fn_sr, "r" );
	$templ_search = fread( $f, $fs );
	fclose( $f );

	$j = 1;
	
	require_once( BX_DIRECTORY_PATH_ROOT . 'profilePhotos.php' );
	
	
	if( mysql_num_rows($result) > 0 )
	{
		while ( $p_arr = mysql_fetch_array( $result ) )
		{
			$ret .="<div class='lastloged_block'>";
			/*print_r($p_arr);  icon
			die;*/
			$oPhoto = new ProfilePhotos( $p_arr['ID'] );
			$oPhoto -> getActiveMediaArray();
			$aFile = $oPhoto -> getPrimaryPhotoArray();
			
			if( extFileExists( $oPhoto -> sMediaDir . 'icon_' . $aFile['med_file'] ) )
			{
				$sFileName = $oPhoto -> sMediaUrl . 'icon_' . $aFile['med_file'];
				$ret .= "<img src ='$sFileName'/>";
				
			}
	
			$ret.="";
			$ret .= PrintSearhResult( $p_arr, $templ_search );

			if( $j != $max_num )
			{
				$ret .= '<div class="top_member_divider"></div>';
			}

			$j ++;
		}
	}
	else
	{
		$ret .= '<div class="no_result">';
			$ret .= '<div>';
				$ret .= _t("_No results found");
			$ret .= '</div>';
		$ret .= '</div>';
	}

	return DesignBoxContent( _t("_Last_logged"), $ret,$oTemplConfig -> PageCompTopMembers_db_num );
}



function PageCompLatestMembers()
{
    global $site;
    global $prof;
    global $enable_match;
    global $logged;
    global $pa_icon_preload;
	global $tmpl;
	global $oTemplConfig;

	// number of profiles
	//$max_num	= (int)getParam("top_members_max_num");
	$max_num = 5;
	$mode		= process_db_input( getParam("top_members_mode") );

	//	Get Sex from GET data
	if ( $_GET['Sex'] && $_GET['Sex'] != "all" )
	{
		$sex = process_db_input( htmlspecialchars_adv($_GET['Sex'] ));
		if ( 'couple' == $sex )
			$query_add = " AND `ProfileType` = 'couple' ";
		else
			$query_add = " AND `Sex` = '$sex' AND `ProfileType` != 'couple' ";
	}
	else
	{
		$sex = "all";
		$query_add = "";
	}

	$query = "SELECT Profiles.ID, Profiles.Headline, Profiles.Country, Profiles.Occupation,
		Profiles.City, Profiles.Sex, Profiles.NickName, Profiles.Children, Profiles.MerchantPrice,
		LEFT( Profiles.DescriptionMe, 180 ) AS DescriptionMe, LEFT( Profiles.DescriptionYou, 100 ) AS DescriptionYou,
		Profiles.DateOfBirth, Profiles.Pic_0_addon, Profiles.Sound, Profiles.ProfileType, Profiles.Sex2, Profiles.DateOfBirth2";
$query .= " FROM `Profiles` WHERE `Status` = 'Active' AND `PrimPhoto`<>0 $query_add ORDER BY `LastReg` DESC LIMIT $max_num";
	
	/*//$ret = "<form name=\"toplist\" action=\"{$_SERVER['PHP_SELF']}\" method=\"get\">";
	if ( strlen($_GET['Mode']) )
		$mode = htmlspecialchars_adv($_GET['Mode']);

	switch ( $mode )
	{
		// online members
		case 'online':
			$online_time = (int)getParam( "member_online_time" );
			$query .= " FROM `Profiles` WHERE `LastNavTime` > SUBDATE(NOW(), INTERVAL $online_time MINUTE) AND `Status` = 'Active' $query_add LIMIT $max_num";
			$menu .= _t("_Online Members") ." | ".
					"<a href=\"{$_SERVER['PHP_SELF']}?Mode=rand&amp;Sex=$sex\" class=\"top_members_menu\">". _t("_Random Members") ."</a>" . " | ".
					"<a href=\"{$_SERVER['PHP_SELF']}?Mode=last&amp;Sex=$sex\" class=\"top_members_menu\">". _t("_Latest Members") ."</a>" . " | ".
					"<a href=\"{$_SERVER['PHP_SELF']}?Mode=top&amp;Sex=$sex\" class=\"top_members_menu\">". _t("_Top Members") ."</a>";
			break;

		// random members
		case 'rand':
			$query .= " FROM `Profiles` WHERE `Status` = 'Active' $query_add ORDER BY RAND() LIMIT $max_num";
			$menu .=	"<a href=\"{$_SERVER['PHP_SELF']}?Mode=online&amp;Sex=$sex\" class=\"top_members_menu\">". _t("_Online Members") ."</a>" . " | ".
					_t("_Random Members") ." | ".
					"<a href=\"{$_SERVER['PHP_SELF']}?Mode=last&amp;Sex=$sex\" class=\"top_members_menu\">". _t("_Latest Members") ."</a>" . " | ".
					"<a href=\"{$_SERVER['PHP_SELF']}?Mode=top&amp;Sex=$sex\" class=\"top_members_menu\">". _t("_Top Members") ."</a>";
			break;

		// last registered members
		case 'last':
			$query .= " FROM `Profiles` WHERE `Status` = 'Active' $query_add ORDER BY `LastReg` DESC LIMIT $max_num";
			$menu .=	"<a href=\"{$_SERVER['PHP_SELF']}?Mode=online&amp;Sex=$sex\" class=\"top_members_menu\">". _t("_Online Members") ."</a>" . " | ".
					"<a href=\"{$_SERVER['PHP_SELF']}?Mode=rand&amp;Sex=$sex\" class=\"top_members_menu\">". _t("_Random Members") ."</a>" . " | ".
					_t("_Latest Members") ." | ".
					"<a href=\"{$_SERVER['PHP_SELF']}?Mode=top&amp;Sex=$sex\" class=\"top_members_menu\">". _t("_Top Members") ."</a>";
			break;

		// top rated members
		case 'top':
			$query .= ", COUNT(*) AS `count`, SUM(Mark) / COUNT(*) AS `mark` FROM `Votes` INNER JOIN `Profiles` ON (`ID` = `Member`) WHERE `Status` = 'Active' $query_add GROUP BY `Member` HAVING `count` > 2 ORDER BY `Mark` DESC LIMIT $max_num";
			$menu .=	"<a href=\"{$_SERVER['PHP_SELF']}?Mode=online&amp;Sex=$sex\" class=\"top_members_menu\">". _t("_Online Members") ."</a>" . " | ".
					"<a href=\"{$_SERVER['PHP_SELF']}?Mode=rand&amp;Sex=$sex\" class=\"top_members_menu\">". _t("_Random Members") ."</a>" . " | ".
					"<a href=\"{$_SERVER['PHP_SELF']}?Mode=last&amp;Sex=$sex\" class=\"top_members_menu\">". _t("_Latest Members") ."</a>" . " | ".
					_t("_Top Members");
			break;
	}
	
	
//	$query .= " FROM `Profiles` WHERE `Status` = 'Active' $query_add ORDER BY `LastReg` DESC LIMIT $max_num";
			/*$menu .=	"<a href=\"{$_SERVER['PHP_SELF']}?Mode=online&amp;Sex=$sex\" class=\"top_members_menu\">". _t("_Online Members") ."</a>" . " | ".
					"<a href=\"{$_SERVER['PHP_SELF']}?Mode=rand&amp;Sex=$sex\" class=\"top_members_menu\">". _t("_Random Members") ."</a>" . " | ".
					_t("_Latest Members") ." | ".
					"<a href=\"{$_SERVER['PHP_SELF']}?Mode=top&amp;Sex=$sex\" class=\"top_members_menu\">". _t("_Top Members") ."</a>";
			*/
			$menu="";

	
	
	//$ret .= '<div id="top_members_menu">' . $menu . '</div>';

	/*$ret .= '<div class="top_members_select">';
	$ret .= "<form name=\"toplist\" action=\"{$_SERVER['PHP_SELF']}\" method=\"get\">";
	$ret .= '</div>';*/

	$result = db_res( $query );

	// Read search template from the file

	$fn_sr = "{$dir['root']}templates/tmpl_{$tmpl}/latest_index.html";

	$fs = filesize( $fn_sr );
	$f = fopen( $fn_sr, "r" );
	$templ_search = fread( $f, $fs );
	fclose( $f );

	$j = 1;
	if( mysql_num_rows($result) > 0 )
	{
		while ( $p_arr = mysql_fetch_array( $result ) )
		{
			$ret .= PrintSearhResult( $p_arr, $templ_search );

			if( $j != $max_num )
			{
				//$ret .= '<div class="top_member_divider"></div>';
			}

			$j ++;
		}
	}
	else
	{
		$ret .= '<div class="no_result">';
			$ret .= '<div>';
				$ret .= _t("_No results found");
			$ret .= '</div>';
		$ret .= '</div>';
	}

	return DesignBoxContent( _t("_Latest Members"), $ret,$oTemplConfig -> PageCompTopMembers_db_num );
}

function PageCompProfilePoll()
{

    global $oTemplConfig;
    global $prof;

    $query = "SELECT `id_poll`, `id_profile`, `Profiles`.`ID`, `Profiles`.`NickName`, `Profiles`.`DateOfBirth`, `Profiles`.`ProfileType`, `DateOfBirth`, `DateOfBirth2`, `Sex`, `Sex2`, `Country` FROM `ProfilesPolls` LEFT JOIN `Profiles` ON id_profile = Profiles.ID ";

    $mode = strlen($_GET['ppoll_mode']) ? $_GET['ppoll_mode'] : 'last';

    switch ( $mode )
    {

	// random members
	case 'rand':
		$query .= " ORDER BY RAND() LIMIT 2";
		$menu =		_t("_random polls") ." | ".
				"<a href=\"{$_SERVER['PHP_SELF']}?ppoll_mode=last\" class=\"ppoll_menu\">". _t("_latest polls") ."</a>" . " | ".
				"<a href=\"{$_SERVER['PHP_SELF']}?ppoll_mode=top\" class=\"ppoll_menu\">". _t("_top polls") ."</a>";
		break;

	// last registered members
	case 'last':
		$query .= " ORDER BY id_poll DESC LIMIT 2";
		$menu =		"<a href=\"{$_SERVER['PHP_SELF']}?ppoll_mode=rand\" class=\"ppoll_menu\">". _t("_random polls") ."</a>" . " | ".
				_t("_latest polls") ." | ".
				"<a href=\"{$_SERVER['PHP_SELF']}?ppoll_mode=top\" class=\"ppoll_menu\">". _t("_top polls")  ."</a>";
		break;

	// top rated members
	case 'top':
		$query .= " ORDER BY poll_total_votes DESC LIMIT 2";
		$menu =		"<a href=\"{$_SERVER['PHP_SELF']}?ppoll_mode=rand\" class=\"ppoll_menu\">". _t("_random polls") ."</a>" . " | ".
				"<a href=\"{$_SERVER['PHP_SELF']}?ppoll_mode=last\" class=\"ppoll_menu\">". _t("_latest polls") ."</a>" . " | ".
				_t("_top polls");
		break;
    }


	$ret = '';
	$ret .= '<div id="ppoll_menu">' . $menu . '</div>';

	$poll_res = db_res( $query );
	if ( mysql_num_rows($poll_res) == 0 )
	{
		$ret .= '<div class="no_result"><div>';
			$ret .= _t("_No profile polls available.");
		$ret .= '</div></div>';
	}
	else while ( $poll_arr = mysql_fetch_array( $poll_res ) )
	{
		if ( $poll_arr['ProfileType'] == "couple")
		{
			$age1_str = _t("_y/o", age( $poll_arr['DateOfBirth'] ));
			$age2_str = _t("_y/o", age( $poll_arr['DateOfBirth2'] ));
			$y_o_sex = $age1_str ._t('_'.$poll_arr['Sex']) .'; '. $age2_str ._t('_'.$poll_arr['Sex2']);
		}
		else
		{
			$age_str = _t("_y/o", age( $poll_arr['DateOfBirth'] ));
			$y_o_sex = $age_str . '&nbsp;' . _t("_".$poll_arr['Sex']);
		}


		$poll_coutry = _t("__".$prof['countries'][$poll_arr['Country']]);


		$ret .= '<div class="pollBody">';
			$ret .= ShowPoll( $poll_arr['id_poll'] );
		$ret .= '</div>';
		$ret .= '<div class="pollInfo">';
			$ret .= get_member_thumbnail( $poll_arr['ID'], left );
			$ret .= '<div class="featured_info">';
				$ret .= '<div class="featured_nickname">';
					$ret .= $poll_arr['NickName'];
				$ret .= '</div>';
				$ret .= '<div>';
					$ret .= $y_o_sex . '<br />' . $poll_coutry;
				$ret .= '</div>';
			$ret .= '</div>';
		$ret .= '</div>';
		$ret .= '<div style="clear:both; margin:5px;">&nbsp;</div>';
	}

	return DesignBoxContent ( _t("_Polls"), $ret, $oTemplConfig -> PageCompProfilePoll_db_num );

}

// --------------- [END] page components functions

?>