<?

/***************************************************************************
*                            Dolphin Smart Community Builder
*                              -------------------
*     begin                : Mon Mar 23 2006
*     copyright            : (C) 2007 BoonEx Group
*     website              : http://www.boonex.com
* This file is part of Dolphin - Smart Community Builder
*
* Dolphin is free software; you can redistribute it and/or modify it under 
* the terms of the GNU General Public License as published by the 
* Free Software Foundation; either version 2 of the 
* License, or  any later version.      
*
* Dolphin is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
* without even the implied warranty of  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details. 
* You should have received a copy of the GNU General Public License along with Dolphin, 
* see license.txt file; if not, write to marketing@boonex.com
***************************************************************************/


/**
 * Return code for the login section for frt
 **/
function LoginSection($logged)
{
	global $site;
	
	$ret = '';
	
	if( $logged['member'] )
	{
		$ret .= '<div class="logged_section_block">';
			//$ret .= '<div style="margin-left:20px;"></div>';
			$ret .= '<div>';
				$ret .= '<a href="' . $site['url'] . 'member.php" class="logout">' . _t("_Control Panel") . '</a>';
			$ret .= '</div>';
			$ret .= '<div>';
				$ret .= '|&nbsp;|';
			$ret .= '</div>';
			$ret .= '<div>';
				$ret .= '<a href="' . $site['url'] . 'logout.php?action=member_logout" class="logout">' . _t("_Log Out") . '</a>';
			$ret .= '</div>';
		$ret .= '</div>';
	}
	elseif( $logged['admin'])
	{
		$ret .= '<div class="logged_section_block">';
			$ret .= '<div>';
				$ret .= '<a href="' . $site['url_admin'] . 'index.php" class="logout">Admin Panel</a>';
			$ret .= '</div>';
			$ret .= '<div>';
				$ret .= '|&nbsp;|';
			$ret .= '</div>';
			$ret .= '<div>';
				$ret .= '<a href="' . $site['url'] . 'logout.php?action=admin_logout" class="logout">' . _t("_Log Out") . '</a>';
			$ret .= '</div>';
		$ret .= '</div>';
	}
	elseif($logged['aff'])
	{
		$ret .= '<div class="logged_section_block">';
			$ret .= '<div>';
				$ret .= '<a href="' . $site['url'] . 'aff/index.php" class="logout">Affiliate Panel</a>';
			$ret .= '</div>';
			$ret .= '<div>';
				$ret .= '|&nbsp;|';
			$ret .= '</div>';
			$ret .= '<div>';
				$ret .= '<a href="' . $site['url'] . 'logout.php?action=aff_logout" class="logout">' . _t("_Log Out") . '</a>';
			$ret .= '</div>';
		$ret .= '</div>';
	}
	elseif($logged['moderator'])
	{
		$ret .= '<div class="logged_section_block">';
			$ret .= '<div>';
				$ret .= '<a href="' . $site['url'] . 'moderators/index.php" class="logout">Moderator Panel</a>';
			$ret .= '</div>';
			$ret .= '<div>';
				$ret .= '|&nbsp;|';
			$ret .= '</div>';
			$ret .= '<div>';
				$ret .= '<a href="' . $site['url'] . 'logout.php?action=moderator_logout" class="logout">' . _t("_Log Out") . '</a>';
			$ret .= '</div>';
		$ret .= '</div>';
	}
	else
	{
	
		
		
		$ret .= '<div id="index_login_form">';
			$ret .= '<div id="login_section_block">';
				$ret .= '<form method="post" action="' . $site['url'] . 'member.php">';
					$ret .= '<div class="username">';
						$ret .= _t("_username") . ':';
					$ret .= '</div>';
					
					$ret .= '<div class="input" >';
						$ret .= '<input name="ID" value="" type="text" class="login_form_input"/>';
					$ret .= '</div>';
					
					$ret .= '<div class="username">';
						$ret .= _t("_Password") . ':';
					$ret .= '</div>';
					
					$ret .= '<div class="input">';
						$ret .= '<input name="Password" value="" type="password" class="login_form_input" />';
					$ret .= '</div>';
					
					/*$ret .= '<div style="margin-bottom:7px;">';
						$ret .= '<a href="http://www.yogahub.com/Problems-Signing-Up.html">' . _t('_forgot_username_or_password') . '?</a>';
					$ret .= '</div>';*/
					
					$ret .= '<div style="text-align:center;">';
						$ret .= '<input type="image" src="' . $site['images'] . 'login_button.jpg" style="border:0px;" />';
					$ret .= '</div>';
				$ret .= '</form>';
			$ret .= '</div>';
		$ret .= '</div>';
		
		/*$ret .= '<div id="index_login_join">';
			$ret .= _t('_not_a_member') . '<br /><strong><a href="' . $site['url'] . 'join_form.php">' . _t('_Join now') . '</a></strong>';
		$ret .= '</div>';	*/	
	}
	
	return $ret;
}


function getProfileOnlineStatus( $user_is_online )
{
	global $site;
	

	$ret = '';
	if( $user_is_online )
	{
		$ret .= '<img src="' . $site['icons'] . 'online.gif" alt="' . _t("_Online") . '" title="' . _t("_Online") . '" style="position:static;" />';
	}
	else
	{
	
		$ret .= '<img src="' . $site['icons'] . 'offline.gif" alt="'. _t("_Offline") . '" title="'. _t("_Offline") . '" style="position:static;" />';
	}
	
	return $ret;
	
}

function getProfileMatch( $memberID, $profileID )
{
	global $oTemplConfig;
	
	$match_n = match_prifiles($memberID, $profileID);
	$ret = '';
	$ret .= DesignProgressPos ( _t("_XX match", $match_n), $oTemplConfig->iProfileViewProgreeBar, 100, $match_n );
		
	
	
	return $ret;
}

function getProfileZodiac( $profileDate )
{
	$ret = '';
			$ret .= ShowZodiacSign( $profileDate );
	
	return $ret;
}

function PageStaticComponents()
{
	// do nothing
}

?>