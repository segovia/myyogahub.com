<?

/***************************************************************************
*                            Dolphin Smart Community Builder
*                              -------------------
*     begin                : Mon Mar 23 2006
*     copyright            : (C) 2007 BoonEx Group
*     website              : http://www.boonex.com
* This file is part of Dolphin - Smart Community Builder
*
* Dolphin is free software; you can redistribute it and/or modify it under 
* the terms of the GNU General Public License as published by the 
* Free Software Foundation; either version 2 of the 
* License, or  any later version.      
*
* Dolphin is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
* without even the implied warranty of  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details. 
* You should have received a copy of the GNU General Public License along with Dolphin, 
* see license.txt file; if not, write to marketing@boonex.com
***************************************************************************/



/***
 javascript variables:
***/

$site['js_init'] = '
<script type="text/javascript" language="javascript">
<!--

  dpoll_progress_bar_color = "#ff0000";
    
//-->
</script>';

/***
 template variables
***/

// path to the images used in the template
$site['images']		= "templates/tmpl_dfl2/images/";
$site['zodiac']		= "templates/tmpl_dfl2/images/zodiac/";
$site['icons']		= $site['url'] . "templates/tmpl_dfl2/images/icons/";
$site['css_dir']	= "templates/tmpl_dfl2/css/";
$site['smiles']		= $site['smiles'] . 'default/';


require_once("BxTemplConfig.php");
$oTemplConfig = new BxTemplConfig($site);

?>
