function isdefined(variable)
{
    return (typeof(window[variable]) == "undefined")?  false: true;
}

if (isdefined('jQuery'))
{
	jQuery.validator.addMethod("alphanumeric", function(value, element){
		return !value.match(/[^\w\d]/);
		},
		"Alphanumeric characters only."
	);
	
	jQuery.validator.addMethod("questionExcept", function(value, element, param){
		return value != param;
		},
		"Please select security question"
	);
	
	jQuery.validator.addMethod("heardExcept", function(value, element, param){
		return value != param;
		},
		"Please specify How did you hear about us"
	);
	
	$(document).ready(function(){
		  $("#activateForm").validate({
		    	onkeyup: false,
				errorElement: "div",
				errorClass: "invalidComment"
		  })
		});
	
	$(document).ready(function(){
		$("#b_city, #interests, #headline, #description, #sec_question, #sec_answer").focus(showBottomHint);
		$("#interests, #description, #sec_question").focus(showRightHint);
	});

	$(document).ready(function(){
		$("#interests, #description, #sec_question").blur(hideRightHint);
	});
}

function showBottomHint() {
	var bottomHintID = $(this).attr("id") + '_bottom_hint';
	var bottomHint = $("div[id='"+bottomHintID+"']");
	bottomHint.show();	
}

function showRightHint() {
	var rightHintsText = new Array();
	rightHintsText["sec_answer"] = 'Make sure your answer is memorable to you but hard for others to guess!';
	rightHintsText["interests"] = 'These will be displayed on your public profile and allow you to easily find other people in the community who share similar interests.';
	rightHintsText["description"] = 'This will be displayed on your community profile and can be updated at any time.';
	rightHintsText["sec_question"] = 'Choose an answer that is memorable, but not easy to guess. If you write your own question, do not choose a question that is common or obvious.';
	rightHintsText["timezoneoffset"] = 'Your time zone helps us provide you with content that is relevant to where you live.';

	var rightHint = $("#rightHint");
	var rightHintText = $("#rightHintText");
	rightHintText.html(rightHintsText[$(this).attr("id")]);

	// Prepare hint position
	var leftDistToInput = 220; // force horiz. position rightHint
	var topDistToInput = 10; // force vertical position rightHint
	var offset = $(this).offset();
	var hintTop = offset.top - topDistToInput;
	var hintLeft = $(this).width() + leftDistToInput;
	rightHint.css({top:hintTop, left:hintLeft});
	rightHint.fadeIn("slow");
}

function hideRightHint() {
	$("#rightHint").hide();
}

function selectValue(selectElementID, valueToSelect, defaultValue)
{
	var selectElement = document.getElementById(selectElementID);
	if (!selectElement) return false;
	
	var floatValue = parseFloat(valueToSelect);
	var valueToCompare = (isNaN(floatValue)) ? valueToSelect : floatValue;
	var selected = false;
	
	for (var i = 0; i < selectElement.options.length; i++)
	{
		if (selectElement.options[i].value == valueToCompare.toString())
		{
			selectElement.options[i].selected = 'selected';
			selected = true;
			break;
		}
	}
	
	if (defaultValue && !selected)
		selectElement.value = defaultValue;
}

function showOwnQuestion(selectedValue)
{
	var ownQuestionRow = document.getElementById('ownQuestionRow');
	ownQuestionRow.style.display = (selectedValue == 'own_question') ? '' : 'none';
}