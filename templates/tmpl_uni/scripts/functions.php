<?



/***************************************************************************

*                            Dolphin Smart Community Builder

*                              -----------------

*     begin                : Mon Mar 23 2006

*     copyright            : (C) 2006 BoonEx Group

*     website              : http://www.boonex.com/

* This file is part of Dolphin - Smart Community Builder

*

* Dolphin is free software. This work is licensed under a Creative Commons Attribution 3.0 License.

* http://creativecommons.org/licenses/by/3.0/

*

* Dolphin is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;

* without even the implied warranty of  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

* See the Creative Commons Attribution 3.0 License for more details.

* You should have received a copy of the Creative Commons Attribution 3.0 License along with Dolphin,

* see license.txt file; if not, write to marketing@boonex.com

***************************************************************************/



$_page_cont[0]['dol_orca_ray'] = getParam( 'enable_boonex_footers' ) ? '

<div class="bottomImages">

	<img src="__images__small_dol.png" alt="" title="" class="dolphinLogo" />

	<img src="__images__small_orca.png" alt="" title="" class="dolphinLogo" />

	<img src="__images__small_ray.png" alt="" title="" class="dolphinLogo" />

</div>' : '';



/**

 * Return code for the login section for frt

 **/

function LoginSection($logged)

{

        global $site;

        global $memberID;

        global $logged;



        $ret = '';



        if( $logged['member'] )

        {

                $ret .= '<div class="logged_member_block">';

                        $ret .= get_member_icon( $memberID, 'none' );

                        $ret .= '<div class="hello_member" >';

                                $ret .= _t( '_Hello member', getNickName( $memberID ) );

                                $ret .= "<br>";

                                $ret .= '<a href="' . $site['url'] . 'member.php" class="logout">' . _t("_Control Panel") . '</a>';

                                $ret .= ' &nbsp; ';

                                $ret .= '<a href="' . $site['url'] . 'logout.php?action=member_logout" class="logout">' . _t("_Log Out") . '</a>';

                        $ret .= '</div>';

                $ret .= '</div>';

        }

        elseif( $logged['admin'])

        {

                $ret .= '<div class="logged_section_block">';

                        $ret .= '<span>';

                                $ret .= '<a href="' . $site['url_admin'] . 'index.php" class="logout">Admin Panel</a>';

                        $ret .= '</span>';

                        $ret .= '<span>';

                                $ret .= '|&nbsp;|';

                        $ret .= '</span>';

                        $ret .= '<span>';

                                $ret .= '<a href="' . $site['url'] . 'logout.php?action=admin_logout" class="logout">' . _t("_Log Out") . '</a>';

                        $ret .= '</span>';

                $ret .= '</div>';

        }

        elseif($logged['aff'])

        {

                $ret .= '<div class="logged_section_block">';

                        $ret .= '<span>';

                                $ret .= '<a href="' . $site['url'] . 'aff/index.php" class="logout">Affiliate Panel</a>';

                        $ret .= '</span>';

                        $ret .= '<span>';

                                $ret .= '|&nbsp;|';

                        $ret .= '</span>';

                        $ret .= '<span>';

                                $ret .= '<a href="' . $site['url'] . 'logout.php?action=aff_logout" class="logout">' . _t("_Log Out") . '</a>';

                        $ret .= '</span>';

                $ret .= '</div>';

        }

        elseif($logged['moderator'])

        {

                $ret .= '<div class="logged_section_block">';

                        $ret .= '<span>';

                                $ret .= '<a href="' . $site['url'] . 'moderators/index.php" class="logout">Moderator Panel</a>';

                        $ret .= '</span>';

                        $ret .= '<span>';

                                $ret .= '|&nbsp;|';

                        $ret .= '</span>';

                        $ret .= '<span>';

                                $ret .= '<a href="' . $site['url'] . 'logout.php?action=moderator_logout" class="logout">' . _t("_Log Out") . '</a>';

                        $ret .= '</span>';

                $ret .= '</div>';

        }

        else

        {

                $ret .= '<div class="login_block">';

                        $ret .= '<form method="post" action="' . $site['url'] . 'member.php">';

                                $ret .= '<div class="clear_both"></div>';



                                $ret .= '<div class="login_line">';

                                        $ret .= _t('_Nickname') . ':';

                                        $ret .= '<input  name="ID" value="" type="text"class="login_area" />';

                                $ret .= '</div>';

                                $ret .= '<div class="login_line">';

                                        $ret .= _t('_Password') . ':';

                                        $ret .= '<input name="Password" value="" type="password" class="login_area" />';

                                $ret .= '</div>';

                                $ret .= '<div class="forgotDiv">';

                                        $ret .= '<a href="http://www.yogahub.com/Problems-Signing-Up.html">' . 
                                        	_t('_forgot_your_password') . '?</a>';

                                $ret .= '</div>';

                                $ret .= '<input class="login_button" type="image" src="' . $site['images'] . 'button_login_index.gif" />';

                                $ret .= '<div class="join_now">' . _t('_or') . ' <a href="' . $site['url'] . 'join_form.php">' . _t( '_Join now' ) . '</a></div>';



                                $ret .= '<div class="clear_both"></div>';

                        $ret .= '</form>';

                $ret .= '</div>';

        }



        return DesignBoxContent( _t('_Member Login'), $ret, 1 );

}





function getProfileOnlineStatus( $user_is_online, $bDrawMargin=FALSE ) {

        global $site;



        $sMarginsAddon = ($bDrawMargin) ? ' style="margin-right:10px;margin-bottom:10px;" ' : '';

        if( $user_is_online ) {

                $sRet .= '<img src="' . $site['icons'] . 'online.gif" alt="' . _t("_Online") . '" title="' . _t("_Online") . '" class="online_offline_bulb" '. $sMarginsAddon .' />';

        } else {

                $sRet .= '<img src="' . $site['icons'] . 'offline.gif" alt="'. _t("_Offline") . '" title="'. _t("_Offline") . '" class="online_offline_bulb" '. $sMarginsAddon .' />';

        }

        return $sRet;

}



function getProfileMatch( $memberID, $profileID )

{

        global $oTemplConfig;



        $match_n = match_profiles($memberID, $profileID);

        $ret = '';

        $ret .= DesignProgressPos ( _t("_XX match", $match_n), $oTemplConfig->iProfileViewProgressBar, 100, $match_n );;



        return $ret;

}



function getProfileZodiac( $profileDate )

{

        $ret = '';

                        $ret .= ShowZodiacSign( $profileDate );



        return $ret;

}



function PageStaticComponents()

{

        // do nothing

}



// added by Ian
if (!function_exists("PageCompListMembers"))
{
function PageCompListMembers( $list, $table, $sqlWho, $sqlWhom, $sqlSelectAdd = '', $sqlWhereAdd = '', $sqlOrderAdd = '',$ReturnOnlyCounter=false)
{
	global $site;
	global $dir;
	global $tmpl;
	
	$memberID = (int)$_COOKIE['memberID'];
	
	if ($memberID != "")
		{
		
	$form = "{$table}_{$list}";
	
	if( $list == '' )
	{
		$query = "
			SELECT
				IF( `$table`.`$sqlWho` = $memberID, `$table`.`$sqlWhom`, `$table`.`$sqlWho` ) AS `$sqlWhom`,
				`Profiles`.`NickName`
				$sqlSelectAdd
			FROM `$table`
			LEFT JOIN `Profiles`
				ON `Profiles`.`ID` = IF( `$table`.`$sqlWho` = $memberID, `$table`.`$sqlWhom`, `$table`.`$sqlWho` )
			WHERE
				( `$table`.`$sqlWho` = $memberID OR `$table`.`$sqlWhom` = $memberID )
				$sqlWhereAdd
			ORDER BY $sqlOrderAdd `Profiles`.`NickName` ASC
		";
	}
	else
	{
		$query = "
			SELECT
				`$table`.`$sqlWho`,
				`$table`.`$sqlWhom`,
				`Profiles`.`NickName`
				$sqlSelectAdd
			FROM `$table`
			LEFT JOIN `Profiles`
				ON `Profiles`.`ID` = `$table`.`$sqlWhom`
			WHERE
				`$table`.`$sqlWho` = $memberID
				$sqlWhereAdd
			ORDER BY $sqlOrderAdd `Profiles`.`NickName` ASC
		";
	}

	$rMembers = db_res( $query );
	if( $num_res = mysql_num_rows( $rMembers ) )
	{
		$sWhole = file_get_contents( "{$dir['root']}templates/tmpl_{$tmpl}/contacts_tmpl.html" );
		
		$aMyTmpl = preg_split( "/\{\/?InsertRows\}/", $sWhole );
		$tmplRow = $aMyTmpl[1];
		$sWhole  = "{$aMyTmpl[0]}{InsertRows /}{$aMyTmpl[2]}";
		
		$InsertRows = '';
		$tr_class = 'odd';
		while( $aMember = mysql_fetch_assoc( $rMembers ) )
		{
			$aReplace = array();
			
			$aReplace['ID']            = $aMember[$sqlWhom];
			$aReplace['CheckBoxName']  = "mem[{$aMember[$sqlWhom]}]";
			
			if( $aMember[$sqlWhom] > 0 )
			{
				$aReplace['Thumbnail']     = get_member_icon( $aMember[$sqlWhom], 'left' );
				$aReplace['NickName']      = "<a href=\"".getProfileLink($aMember[$sqlWhom])."\">{$aMember['NickName']}</a>";
				$aReplace['SendGreet']     = sendKissPopUp( $aMember[$sqlWhom] );

				$aReplace['SendMsg']       = "
				  <a href=\"{$site['url']}compose.php?ID={$aMember[$sqlWhom]}\" title=\""._t('_Send Message')."\">
					<img src=\"".getTemplateIcon( 'compose.png' )."\" alt=\""._t('_Send Message')."\" />
				  </a>";
				
			}
			else
			{
				$aReplace['Thumbnail']     = '' ;
				$aReplace['NickName']      = _t( '_Visitor' );
				$aReplace['SendGreet']     = '';
				$aReplace['SendMsg']       = '';
			}
			
			$aReplace['Times']         = $aMember['Number'] ? _t( "_N times", $aMember['Number'] ) : '&nbsp;';
			$aReplace['PicNew']        = $aMember['New'] ? '<img src="'.getTemplateIcon('new.gif').'" class="pic_new" alt="new"/>' : '';
			$aReplace['Date']          = $aMember['Arrived'] ? $aMember['Arrived'] : '&nbsp;';
			$aReplace['tr_class']      = $tr_class;
			
			$sInsertRow = $tmplRow;
			foreach( $aReplace as $key => $val )
				$sInsertRow = str_replace( "{{$key}}", $val, $sInsertRow );
			
			$sInsertRows .= $sInsertRow;
			$tr_class = ( $tr_class == 'odd' ? 'even' : 'odd' );
		}
		
		
		$aReplace = array();
		
		$aReplace['InsertRows /'] = $sInsertRows;
		$aReplace['Self']         = $_SERVER['PHP_SELF'] . '?show=' . $_GET['show'] . '&amp;list=' . $_GET['list'];
		$aReplace['FormName']     = "{$form}_form";
		$aReplace['CheckAll']     = _t('_Check all');
		$aReplace['UncheckAll']   = _t('_Uncheck all');
		$aReplace['Actions']      = getButtons( $form );
		
		foreach( $aReplace as $key => $val )
			$sWhole = str_replace( "{{$key}}", $val, $sWhole );
		
		// unset "new" flag for kisses
		if( $table == 'VKisses' and $list == 'me' )
			db_res( "UPDATE `VKisses` SET `New`='0' WHERE `Member`=$memberID AND `New`='1'" );
	}
	else
	{
		/*
		$sWhole = file_get_contents( "{$dir['root']}templates/tmpl_{$tmpl}/contacts_tmpl_nores.html" );
		$aReplace = array();
		
		$aReplace['NoResults'] = _t('_No members found here');
		
		foreach( $aReplace as $key => $val )
			$sWhole = str_replace( "{{$key}}", $val, $sWhole );
			*/
		$sWhole = _t('_No members found here');
	}
	
	if ($ReturnOnlyCounter)
		return $num_res;
	
	$sWhole = str_replace( '{TableCaption}', getTableName( $form, $num_res ), $sWhole );
	
	return $sWhole;
		}
	else 
		return 0;
}
}


if (!function_exists("getTableName"))
{
function getTableName( $form, $num_res )
{
	switch( $form )
	{
		case 'HotList_i':        $ret = _t( '_MEMBERS_YOU_HOTLISTED' ); break;
		case 'FriendList_i':     $ret = _t( '_MEMBERS_INVITE_YOU_FRIENDLIST' ); break;
		case 'BlockList_i':      $ret = _t( '_MEMBERS_YOU_BLOCKLISTED' ); break;
		case 'VKisses_i':        $ret = _t( '_MEMBERS_YOU_KISSED' ); break;
		case 'ProfilesTrack_i':  $ret = _t( '_MEMBERS_YOU_VIEWED' ); break;
		
		case 'HotList_me':       $ret = _t( '_MEMBERS_YOU_HOTLISTED_BY' ); break;
		case 'FriendList_me':    $ret = _t( '_MEMBERS_YOU_INVITED_FRIENDLIST' ); break;
		case 'BlockList_me':     $ret = _t( '_MEMBERS_YOU_BLOCKLISTED_BY' ); break;
		case 'VKisses_me':       $ret = _t( '_MEMBERS_YOU_KISSED_BY' ); break;
		case 'ProfilesTrack_me': $ret = _t( '_MEMBERS_YOU_VIEWED_BY' ); break;
		
		case 'FriendList_':      $ret = _t( '_Friend list' ); break;
	}
	
	if( $num_res )
		$ret .= ": $num_res";
	
	return $ret;
}
}

if (!function_exists("getButtons"))
{
function getButtons( $form )
{
	$aButton = array();
	
	switch( $form )
	{
		case 'HotList_i':        $aButton['del'] = _t('_Delete');      break;
		case 'FriendList_i':     $aButton['del'] = _t("_Back Invite"); break;
		case 'BlockList_i':      $aButton['del'] = _t("_Unblock");     break;
		case 'VKisses_i':        $aButton['del'] = _t('_Delete');      break;
		case 'ProfilesTrack_i':  $aButton['del'] = _t('_Delete');      break;
		
		case 'HotList_me':       $aButton['add'] = _t("_Add to Hot List"); break;
		case 'FriendList_me':    $aButton['add'] = _t("_Add to Friend List"); 
		                         $aButton['del'] = _t("_Reject Invite");   break;
		case 'BlockList_me':     $aButton['add'] = _t("_Block");           break;
		case 'VKisses_me':       $aButton['del'] = _t('_Delete');          break;
		case 'ProfilesTrack_me': $aButton['del'] = _t('_Delete');          break;
		
		case 'FriendList_':      $aButton['del'] = _t("_Delete from Friend List"); break;
	}
	
	$ret = '';
	
	foreach( $aButton as $sAct => $sTitle )
		$ret .= " <input type=\"submit\" name=\"{$form}_{$sAct}\" value=\"$sTitle\" disabled=\"disabled\" class=\"submit_button\" /> ";
	
	return $ret;
}
}


function HelloMemberSection()
{
        global $logged;
        global $site, $dir;
        global $loginWarning;
        global $relocateToReferer;
        global $su_config;

        ob_start();

        if( $logged['member'] )
        {
            $memberID = (int)$_COOKIE['memberID'];
            $iLet = getNewLettersNum($memberID);
            $sNewLet = $iLet > 0 ? $iLet : '0' ;
                
		?>

            <div class="no_login_actions" >
			<div align="right">
                <table cellpadding="0" cellspacing="0">
                <tr>
                <td align="right" valign="top">
                <div align="right" style="color:#999999;">
                <a href="<?= $su_config['url'] ?>me" style="font-size:9px;" >My Dashboard</a> |
                <a href="profile_edit.php?ID=<?=$memberID?>" style="font-size:9px;">Settings</a> |
                <a href="contacts.php" style="font-size:9px;">Contacts</a> |
                
                <a  href="logout.php?action=member_logout" style="font-size:9px;">Logout</a>
                <!--<a href="<? echo getProfileLink( $memberID ) ?>" class="header2">Profile</a> |-->
                
                </div>
                </td>
                <td rowspan="2"><?= get_member_icon($memberID, 'right') ?></td>                
                </tr>
                <tr>
                <td align="right" valign="top">                	
                <div align="right" style="padding-top:10px;">
                <nobr>
                	<table>
                	<tr>
                	<td>
                	<span class="headerNickname"><?
                
                $maxNicknameLength = 10;
                $nickname = getNickName( $memberID );
                if (strlen($nickname) > $maxNicknameLength)
                $nickname = substr($nickname, 0, $maxNicknameLength - 3) . "...";

                print $nickname;
                
                ?></span> |</td>
                <td ><a  href="mail.php?mode=inbox">
                <img src="images/ico_Email.gif" width="15" height="14" border="0"></a></td>
                <td style="font-size:9px;"><a class="inboxLink" href="mail.php?mode=inbox">Inbox:</a> 
                <a class="header3" href="mail.php?mode=inbox"><?=$sNewLet?></a> |</td>
                <td ><a href="contacts.php?show=friends_inv&list=me">
                <img src="images/ico_friends.gif" width="15" height="14" border="0"></a></td>
                <td style="font-size:9px;"><a class="inboxLink" href="contacts.php?show=friends_inv&list=me"> Requests:</a>
                <a class="header3" href="contacts.php?show=friends_inv&list=me">
                <?=PageCompListMembers("me","FriendList","Profile","ID","","AND `Check` = 0","",true);?></a></td>
                </tr>
                </table>
                </nobr>
                </div>
                </td>
                </tr>
                </table>
               
                </div>
                
                       
				<!--
                        <div class="hello_member" ></div>



                        <div class="hello_actions">

                                <span><a href="member.php"><?= _t('_My account') ?></a></span>

                                <span><a href="mail.php?mode=inbox"><?= _t('_My Mail') ?></a><?=' '.$sNewLet;?></span>

                                <span><a href="<? echo getProfileLink( $memberID ) ?>"><?= _t('_My Profile') ?></a></span>

                                <span></span>

                        </div>
                        -->

                </div>

            <?
        }
	elseif( $logged['admin'] )
	{
            ?>

                <div class="topMemberBlock">

					<div class="thumb" >

                        <img style="width: 45px; height: 45px; background-image: url(<?= getTemplateIcon( 'man_small.gif' ) ?>);"

						  src="<?= getTemplateIcon( 'spacer.gif' ) ?>" alt="avatar" />

					</div>

                        <div class="hello_member"><?= _t( '_Hello member', 'admin' ) ?></div>



                        <div class="hello_actions">

                                <span><a href="admin/index.php"><?= _t('_Admin Panel') ?></a></span>

                                <span><a href="logout.php?action=admin_logout"><?= _t('_Log Out2') ?></a></span>

                        </div>

                </div>

                <?
	}
        else
        {
        		require_once("{$dir['root']}profiles/superuserconfig.php");
        		
                ?>
                        <div class="no_hello_actions" >
                        
                        <div style="padding-bottom:10px; text-align:right">
                        	<span style="font-weight:bold; color:#0000ff">Not a member yet?</span>
                        	<span><a href="<?= $su_config['url'] ?>registerlight" style="color:#0000ff;">
                        		<?= _t( '_Join Now Top' ) ?></a>
                        	</span>
                        <!--<a href="<?= $site['url'] ?>member.php"><?= _t( '_Member Login' ) ?></a>-->
                        </div>
					
					<div id="loginform">
                        <form action="member.php" method="post" name="form1">
                        <? if ($loginWarning != '') { ?>
                        <input type="hidden" name="relocate" value="<?= ($relocateToReferer) ? 
                        	$_SERVER['HTTP_REFERER'] : $_SERVER['REQUEST_URI']; ?>" />
                        <? } ?>
                        <input type="text" name="ID" id="ID" class="login_form_input" style="width:80px;" value="Username" onfocus="document.form1.ID.value='';" />
                        <input type="password" id="Password" name="Password" maxlength="8" class="login_form_input" value="Password" onfocus="document.form1.Password.value='';" style="width:80px;" />
                        <input type="submit" class="login_form_submit" name="LoginSubmit" value="Log in"/>
                        </form>
                        </div>
                       
                        </div>
        <?
    }

    return ob_get_clean();
}



function MsgBox( $text, $fontSize=0, $padding=0, $img='')

{

        global $site;

        global $tmpl;

		if ($fontSize)
			$styleStr = "font-size:{$fontSize}px;";
		if ($padding)
			$styleStr .= "padding:{$padding}px;";

        ob_start();

        ?>

                <table class="MsgBox" cellpadding="0" cellspacing="0">

                        <tr>

                                <td class="corder"><img src="<?= "{$site['url']}templates/tmpl_$tmpl/images/msgbox_cor_lt.png" ?>" /></td>

                                <td class="top_side"><img src="<?= getTemplateIcon( 'spacer.gif' ) ?>" alt="" /></td>

                                <td class="corder"><img src="<?= "{$site['url']}templates/tmpl_$tmpl/images/msgbox_cor_rt.png" ?>" /></td>

                        </tr>

                        <tr>

                                <td class="left_side"><img src="<?= getTemplateIcon( 'spacer.gif' ) ?>" alt="" /></td>

                                <td class="msgbox_content"><div class="msgbox_text" style="<?= $styleStr ?>"><?= $img. $text ?></div></td>

                                <td class="right_side"><img src="<?= getTemplateIcon( 'spacer.gif' ) ?>" alt="" /></td>

                        </tr>

                        <tr>

                                <td class="corner"><img src="<?= "{$site['url']}templates/tmpl_$tmpl/images/msgbox_cor_lb.png" ?>" /></td>

                                <td class="bottom_side"><img src="<?= getTemplateIcon( 'spacer.gif' ) ?>" alt="" /></td>

                                <td class="corner"><img src="<?= "{$site['url']}templates/tmpl_$tmpl/images/msgbox_cor_rb.png" ?>" /></td>

                        </tr>

                </table>

        <?



        return ob_get_clean();

}



?>