<?php

/***************************************************************************
*                            Dolphin Smart Community Builder
*                              -----------------
*     begin                : Mon Mar 23 2006
*     copyright            : (C) 2006 BoonEx Group
*     website              : http://www.boonex.com/
* This file is part of Dolphin - Smart Community Builder
*
* Dolphin is free software. This work is licensed under a Creative Commons Attribution 3.0 License. 
* http://creativecommons.org/licenses/by/3.0/
*
* Dolphin is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
* without even the implied warranty of  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the Creative Commons Attribution 3.0 License for more details. 
* You should have received a copy of the Creative Commons Attribution 3.0 License along with Dolphin, 
* see license.txt file; if not, write to marketing@boonex.com
***************************************************************************/

require_once('inc/header.inc.php');
require_once( BX_DIRECTORY_PATH_INC . 'design.inc.php' );
require_once( BX_DIRECTORY_PATH_INC . 'admin.inc.php' );
require_once( BX_DIRECTORY_PATH_INC . 'images.inc.php' );
require_once( BX_DIRECTORY_PATH_INC . 'sharing.inc.php' );
require_once( BX_DIRECTORY_PATH_INC . 'videoservices.inc.php' );
require_once( BX_DIRECTORY_PATH_ROOT . "templates/tmpl_{$tmpl}/scripts/BxTemplProfileView.php" );

if (isset($_POST['submitthumb']))
{
	$target = "ray/modules/movie/files/{$_POST['fileID']}_small.jpg";
	$err_msg = '';

	if ($_FILES['uploadthumb']['size'] > 70000 || $_FILES['uploadthumb']['size'] == 0) // check file size
		$err_msg = "Sorry, your file is too large.";
	elseif ($_FILES['uploadthumb']['type'] != "image/jpeg") // check file type
		$err_msg = "Please select jpg file.";
	elseif (!move_uploaded_file($_FILES['uploadthumb']['tmp_name'], $target))
		$err_msg = "Sorry, there was a problem uploading your file.";

	if ($err_msg != '')
	{
		$_page_cont[$_ni]['page_main_code'] = $err_msg;
		PageCode();
		exit;
	}
	else 
	{
		$userIDParam = (isset($_POST['userID'])) ? "?userID={$_POST['userID']}" : '';
		header("Location: {$_SERVER['PHP_SELF']}$userIDParam");
	}
	
}

$userID = 0;
if (isset($_GET['userID']))
	$userID = (int)$_GET['userID'];
elseif (isset($_GET['sefNick']))
	$userID = getID($_GET['sefNick'], 0);

if ($userID)
{
	$oProfile = new BxTemplProfileView($userID);
	$_page['extra_css'] = $oProfile -> genProfileCSS($userID);
}

$_page['name_index']	= 82;
$_page['css_name']		= 'viewVideo.css';

$oVotingView = new BxTemplVotingView('gvideo', 0, 0);
$_page['extra_js'] 	= $oVotingView->getExtraJs();

if ( !( $logged['admin'] = member_auth( 1, false ) ) )
{
	if ( !( $logged['member'] = member_auth( 0, false ) ) )
	{
		if ( !( $logged['aff'] = member_auth( 2, false ) ) )
			$logged['moderator'] = member_auth( 3, false );
	}
}


$_page['header'] = _t( "_browseVideo" );
$searchSect = <<<EOF
<div class="caption_item">
	<form name="VideosSearchForm" action="{$_SERVER['PHP_SELF']}" method="get">
	<input type="text" name="search" size="23" value="{$_GET['search']}"/>
	<input type="submit" value="Search"/>
</form></div>
EOF;
$headerText = (isset($_GET['search'])) ? 'Search Results for "'.$_GET['search'].'"'
	: _t("_browseVideo");
$_page['header_text'] = '<div style="float:left">'.$headerText.'</div>'.$searchSect;

$_ni = $_page['name_index'];

$member['ID'] = (int)$_COOKIE['memberID'];

syndicateVideoFeed($userID);

$aWhere = array();
$aWhere[] = '1';

if ($userID)
	$aWhere[] = "`RayMovieFiles`.`Owner`=$userID";   
elseif (isset($_GET['eventID']))
{
	$iEventID = (int)$_GET['eventID'];
	$aWhere[] = "`RayMovieFiles`.`AssocID`=$iEventID AND `RayMovieFiles`.`AssocType`='event'";   
}

if (isset($_GET['tag']))
{
	$sTag = htmlspecialchars_adv($_GET['tag']);
	$aWhere[] = "`RayMovieFiles`.`Tags` like '%$sTag%'";
}

// Search
if (isset($_GET['search']) && $keyword = trim($_GET['search']))
	$aWhere[] = "(RayMovieFiles.Title LIKE '%$keyword%' OR 
		RayMovieFiles.Description LIKE '%$keyword%'	OR RayMovieFiles.Tags LIKE '%$keyword%')";

if (isset($_GET['action']))
{
	if ($_GET['action'] == 'changethumb')
	{
		$userIDInput = (isset($_GET['userID'])) ? '<input type="hidden" name="userID" value="'.$_GET['userID'].'"/>' : '';
		$uploadFileForm = '<form enctype="multipart/form-data" action="'.$_SERVER['PHP_SELF'].'" method="post">
			<input type="hidden" name="MAX_FILE_SIZE" value="70000" />
			<input type="hidden" name="fileID" value="'.$_GET['fileID'].'"/>'.$userIDInput.
			'<div>Please choose a thumbnail file (JPG only, up to 70 kB): <input name="uploadthumb" type="file" accept="image/jpeg"/>
				<div style="text-align:center; margin-top: 10px"><input type="submit" name="submitthumb" value="Upload"/></div>
			</div>
		</form>';
		$_page_cont[$_ni]['page_main_code'] = $uploadFileForm;

		PageCode();
		exit;
	}
	elseif ($_GET['action'] == 'edit')
	{
		define('MAXTITLELENGTH', 255);
		define('MAXTAGSLENGTH', 100);
		define('MAXDESCLENGTH', 500);
		
		if (!isset($_GET['fileID']) || !($_GET['fileID'] > 0))
		{
			$_page_cont[$_ni]['page_main_code'] = 'Video file not specified!';
			PageCode();
			exit;
		}
		
		// Get video info from database
		$query = "SELECT Title, Tags, Description, Permission FROM RayMovieFiles WHERE ID={$_GET['fileID']} LIMIT 1";
		$fileInfo = fill_assoc_array(db_res($query));

		if (!is_array($fileInfo))
		{
			$_page_cont[$_ni]['page_main_code'] = 'Video file not found!';
			PageCode();
			exit;
		}
		$fileInfo = $fileInfo[0];
		
		$uploadFileForm = '<script language="javascript" type="text/javascript" src="inc/js/videoservices.js"></script>';
		$uploadFileForm .= 	'<script>
			var embed = new Array();';
			$i = 0;
			foreach ($embed as $embedKey => $embedData)
			{
				$regexUrl = $embed[$embedKey]['regexUrl'];
				$regexCorrectEmbed = $embed[$embedKey]['regexCorrectEmbed'];
				$embedName = $embed[$embedKey]['name'];
				$uploadFileForm .= "embed[$i] = new Array(3);";
				$uploadFileForm .= "embed[$i]['regexUrl'] = '$regexUrl';";
				$uploadFileForm .= "embed[$i]['regexCorrectEmbed'] = '$regexCorrectEmbed';";
				$uploadFileForm .= "embed[$i]['name'] = '$embedName';";
				$i++;
			}
		$uploadFileForm .= '</script>';
		
		$permPublic = (!$fileInfo['Permission']) ? ' checked' : '';
		$permFriends = ($fileInfo['Permission'] == 1) ? ' checked' : '';
		
		$uploadFileForm .= "<form enctype=\"multipart/form-data\" name=\"uploadVideoForm\" 
			method=\"post\" action=\"{$site['url']}uploadShareVideo.php\">" .
			'<div>
				<div style="float:left; position:relative; left:570px"><b>Video Permissions</b><div style="margin-top:5px">
					<input type="radio" name="videoPerm" value="0"'. $permPublic . '/>public<br/>
					<input type="radio" name="videoPerm" value="1"'. $permFriends .'/>friends only
				</div></div>
			<table style="position:relative; right:100px">
				<tr><td class="uploadText"><span style="color:red">*</span> '. _t("_Title").
					': </td><td colspan="2"><input type="text" name="title" class="uploadInput" value="'.$fileInfo['Title'].'"
					onkeyup="CheckSubmitEnabled(true)" maxlength="' . MAXTITLELENGTH . '"/></td></tr>
				<tr><td class="uploadText">'. _t("_Description").
					': </td><td colspan="2"><textarea name="description" class="uploadInput" 
					onKeyUp=\'return UpdateMaxDescChars('.MAXDESCLENGTH.');\'>'.$fileInfo['Description'].'</textarea></td></tr>
				<tr><td/><td style="text-align:center"><div id="descCounter" class="uploadInput"><span>'. 
					MAXDESCLENGTH .	'</span> chars left</div></td></tr>
				<tr><td class="uploadText">'. _t("_Tags").
					': </td><td colspan="2"><input type="text" name="tags" class="uploadInput" value="'.$fileInfo['Tags'].'"/></td></tr>
				<tr><td colspan="2" style="text-align:center; vertical-align:top; color:grey; font-weight:bold; height:25px">
					Leave URL and embed code empty to keep old video</td><td/></tr>
				<tr><td class="uploadText" maxlength="'. MAXTAGSLENGTH .'">Enter the url to the video: 
					<input type="radio" name="shareType" value="shareTypeUrl" onclick="ToggleShareType(this.value, true)" checked/></td>
					<td><input type="text" name="sharingUrl" class="uploadInput" onkeyup="CheckSubmitEnabled(true)"/></td>
					<td style="vertical-align:top"><span id="urlErrorMsg"></span></td></tr>
				<tr><td class="uploadText">or specify embed source: 
					<input type="radio" name="shareType" value="shareTypeEmbed" onclick="ToggleShareType(this.value, true)"/></td>
					<td><textarea name="embedSource" class="uploadInput" onkeyup="CheckSubmitEnabled(true)" disabled></textarea></td>
					<td style="vertical-align:top"><span id="embedErrorMsg"></span></td></tr>
				<tr><td/><td colspan="2" style="height:25px; vertical-align: top">
					<span style="color:red">*</span> - required fields</td></tr>
				<tr><td/><td style="text-align:center"><input type="submit" name="upload" value="Save"/></td><td/></tr>
			</table></div>
			<input type="hidden" name="action" value="edit"/>
			<input type="hidden" name="medID" value="'.$_GET['fileID'].'"/>
			<input type="hidden" name="medProfId" value="'.$member['ID'].'"/>
			</form>';
		$uploadFileForm .= '<script>UpdateMaxDescChars('.MAXDESCLENGTH.');</script>';
		
		$_page_cont[$_ni]['page_main_code'] = $uploadFileForm;
		PageCode();
		exit;		
	}
	else
	{
		$sAct = htmlspecialchars_adv($_GET['action']);
		$sReferer = $_GET['referer'];
		$sAddon = defineBrowseAction($sAct,'Video',$member['ID']);
	}
}

$sqlWhere = "WHERE " . implode( ' AND ', $aWhere ).$sAddon." AND `Approved`= 'true'";
if ($member['ID'] != $userID && !is_friends($member['ID'], $userID))
	$sqlWhere .= ' AND Permission = 0';

$iTotalNum = db_value("SELECT COUNT( * ) FROM `RayMovieFiles` $sqlWhere");
if (!$iTotalNum)
{
	if ($userID > 0) // member video
	{
		// Get profile owner nickname
		$ownerNick = (isset($_GET['sefNick'])) ? $_GET['sefNick'] : getNickName($userID);
	
		if ( $member['ID'] == $userID) // my videos
			$_page_cont[$_ni]['page_main_code'] = "Sorry, $ownerNick it looks like you have not setup a video gallery yet.<br/><br/>
				Would you like to <a href=\"uploadShareVideo.php\">upload your first video</a> to your video gallery? ";
		else
			$_page_cont[$_ni]['page_main_code'] = "Sorry, it looks like $ownerNick has not setup a video gallery yet.";
	}
	else // event video
		$_page_cont[$_ni]['page_main_code'] = _t( '_Sorry, nothing found' );
	
	PageCode();
	exit;
}

$iPerPage = (int)$_GET['per_page'];
if( !$iPerPage )
	$iPerPage = 10;
$iTotalPages = ceil( $iTotalNum / $iPerPage );

$iCurPage = (int)$_GET['page'];

if( $iCurPage > $iTotalPages )
	$iCurPage = $iTotalPages;

if( $iCurPage < 1 )
	$iCurPage = 1;

$sLimitFrom = ( $iCurPage - 1 ) * $iPerPage;

$sqlOrderBy = 'ORDER BY `medID` DESC';

if (isset($_GET['rate']))
{
	$oVotingView = new BxTemplVotingView ('gvideo', 0, 0);
	
	$aSql        = $oVotingView->getSqlParts('`RayMovieFiles`', '`ID`');
	$sHow        = $_GET['rate'] == 'top' ? "DESC" : "ASC";
	$sqlOrderBy  = $oVotingView->isEnabled() ? "ORDER BY `voting_rate` $sHow, `voting_count` $sHow, `medDate` $sHow" : $sqlOrderBy ;
	$sqlFields   = $aSql['fields'];
	$sqlLJoin    = $aSql['join'];
}	
$sqlLimit = "LIMIT $sLimitFrom, $iPerPage";

$sQuery = "
	SELECT
		`RayMovieFiles`.`ID`    as `medID`,
		`RayMovieFiles`.`Owner` as `medProfId`,
		`RayMovieFiles`.`Title` as `medTitle`,
		`RayMovieFiles`.`Date`  as `medDate`,
		`RayMovieFiles`.`Views` as `medViews`,
		`RayMovieFiles`.`Type` as `videoType`,
		`RayMovieFiles`.`EmbedId` as `embedId`,
		`RayMovieFiles`.`Permission`,
		`RayMovieFiles`.`urltitle`,
		`Profiles`.`NickName`, `Profiles`.`ID` as OwnerID,
		SDatingEvents.ResponsibleID as eventOwnerID
		$sqlFields
	FROM `RayMovieFiles`
	LEFT JOIN `Profiles` ON
		`Profiles`.`ID` = `RayMovieFiles`.`Owner`
	LEFT JOIN SDatingEvents ON SDatingEvents.ID=RayMovieFiles.AssocID
	$sqlLJoin
	$sqlWhere
	$sqlOrderBy
	$sqlLimit
	";

$rData = db_res($sQuery);

$_page_cont[$_ni]['page_main_code'] = PageCompPageMainCode();

PageCode();

function PageCompPageMainCode()
{
	global $site;
	global $rData;
	global $iTotalPages;
	global $iCurPage;
	global $iPerPage;
	global $member;
	global $embed;
	global $userID;
	
	$sCode = '';
	if (mysql_num_rows($rData))
	{
		$videoTitleTmplOrig = "{$site['url']}viewVideo.php?fileID=%videoID%";
		$videoTitleTmpl = (useSEF) ? "{$site['url']}%ownerNick%/video/%urltitle%.html" : $videoTitleTmplOrig;
		while ($aData = mysql_fetch_array($rData))
		{
			if ($aData['Permission'] == 1 && !is_friends($member['ID'], $aData['OwnerID']) && !($member['ID']==$aData['OwnerID']))
				continue;
			
			$oVotingView = new BxTemplVotingView ('gvideo', $aData['medID']);
		    if( $oVotingView->isEnabled())
	    	{
				$sRate = $oVotingView->getSmallVoting (0);
				$sShowRate = '<div class="galleryRate">'. $sRate . '</div>';
			}
			if ($aData['urltitle'] == '')
				$sHref = str_replace('%videoID%', $aData['medID'], $videoTitleTmplOrig);
			else
				$sHref = str_replace(array('%ownerNick%', '%videoID%', '%urltitle%'), 
					array($aData['NickName'], $aData['medID'], $aData['urltitle']), $videoTitleTmpl);
			
			if ($aData['videoType'] == 'file')
				 $sImg  = '<a href="'.$sHref.'"><img src="'.$site['url'].'ray/modules/movie/files/'.$aData['medID'].'_small.jpg"></a>';
			else 
			{
				$imagePath = "ray/modules/movie/files/{$aData['medID']}_small.jpg";
				if (file_exists($imagePath))
					$sImg  = '<a href="'.$sHref.'"><img src="'.$site['url'].$imagePath.'"></a>';
				else 
					$sImg  = '<table cellpadding="0" cellspacing="0" style="width:110px; height:80px; background-color:white">
						<tr><td style="text-align:center">
						<a id="thumbUnavailable" href="'.$sHref.'">Thumbnail Unavailable</a></td></tr></table>';
			}
			
			$sVidTitle = strlen($aData['medTitle']) > 0 ? $aData['medTitle'] : _t("_Untitled");
			
			$userIDParam = (isset($_GET['userID'])) ? "&userID={$_GET['userID']}" : '';
			if ($member['ID'] && ($member['ID'] == $aData['medProfId'] || $member['ID'] == $aData['eventOwnerID']))
			{
				$sChangeThumbLink = '<div style="display:block; float:right; text-align:right"><a href="'.$_SERVER['PHP_SELF'].
					'?action=changethumb&fileID='.$aData['medID'].$userIDParam.'">Change<br/>Thumbnail</a></div>';
				$sEditLink = '<div style="display:block; float:right; margin-top:15px"><a href="'.$_SERVER['PHP_SELF'].
					'?action=edit&fileID='.$aData['medID'].'">Edit</a></div>';
				$sDelLink = '<div style="display:block; float:right; clear:right; margin-top: 5px"><a href="'.$_SERVER['PHP_SELF'].'?action=del&fileID='.$aData['medID'].'"
					onClick="return confirm( \''._t("_confirmDeleteVideo").'\');">'._t("_Delete").'</a></div>';
			}
			else 
			{
				$sChangeThumbLink = '';
				$sEditLink = '';
				$sDelLink = '';
			}
	
			if (strlen($sVidTitle) > 43)
				$sVidTitle = substr($sVidTitle, 0, 40) . '...';
			$sCode .= '<div class="browseUnit">';
				$sCode .= '<table cellpadding="0" cellspacing="0"><tr><td>';
				$sCode .= '<div class="lastFilesPic">'.$sImg.'</div>';
				$sCode .= '</td><td>';
				
				$sCode .= '<div class="videoInfo">';
				$sCode .= '<div class="videoInfoTitle">
					<a href="'.$sHref.'"><b>'.$sVidTitle.'</b></a></div>';
				$sCode .= '<div class="videoInfoItem">'._t("_By").': <a href="'.
					getProfileLink($aData['medProfId']).'">'.$aData['NickName'].'</a></div>';
				$sCode .= '<div class="videoInfoItem">'._t("_Added").': <b>'.defineTimeInterval($aData['medDate']).'</b></div>';
				$sCode .= '<div class="videoInfoItem">'._t("_Views").': <b>'.$aData['medViews'].'</b></div>';
				$sCode .= $sShowRate;
				$sCode .= '</div>';
				$sCode .= '</td><td>';
				
				$sCode .= '<div class="videoInfoActions">';
				$sCode .= $sChangeThumbLink;
				$sCode .= $sEditLink;
				$sCode .= $sDelLink;
				$sCode .= '</div>';			
				$sCode .= '</td></tr></table>';
			$sCode .= '</div>';	
			
		}
	}
	
	// generate pagination
	if( $iTotalPages > 1)
	{
		if ($userID)
			$sRequest = "userID=$userID";
			
		$aFields = array('tag', 'rate', 'search');
		foreach ($aFields as $field)
		{
			if (isset($_GET[$field]))
			{
				if ($sRequest != '') $sRequest .= "&amp;";
				$sRequest .= "{$field}=" . htmlentities( process_pass_data( $_GET[$field] ) );
			}
		}
		
		$sRequest = "{$_SERVER['PHP_SELF']}?$sRequest";
	
		$perPageHtml = '<div style="text-align: center; position: relative; 
			margin-top:15px; height:23px">'. _t('_Results per page').':
		<select name="per_page" onchange="window.location=\'' . $sRequest . '&amp;per_page=\' + this.value;">
			<option value="10"' . ( $iPerPage == 10 ? ' selected="selected"' : '' ) . '>10</option>
			<option value="20"' . ( $iPerPage == 20 ? ' selected="selected"' : '' ) . '>20</option>
			<option value="50"' . ( $iPerPage == 50 ? ' selected="selected"' : '' ) . '>50</option>
			<option value="100"' . ( $iPerPage == 100 ? ' selected="selected"' : '' ) . '>100</option>
		</select>
		</div>';
		
		$pagination = genPagination($iTotalPages, $iCurPage, ($sRequest . '&amp;page={page}&amp;per_page='.$iPerPage));
	}
	else
	{
		$perPageHtml = '';
		$pagination = '';
	}
	
	$res = <<<EOF
<div style="position: relative; float: left;">
	<center>$pagination</center>
	$sCode
	<div class="clear_both"></div>
	<center>{$perPageHtml}{$pagination}</center> 
</div>
EOF;
		
	return $res;
}

/**
 * Search embed code in embed array for specified url
 * @param $url string Url of video to search embed code 
 * @return array ('type', 'embedCode')
 */
function _getEmbedData($url)
{
	global $embed;
	
	foreach ($embed as $embedKey => $embedData)
		if (strpos($url, $embedData['regexUrl']) !== false)
		{
			if ($embedData['getCodeAction']['method'] == 'RegexURL')
			{		
				if (preg_match($embedData['getCodeAction']['regex'], $url, $embedCodeArr))
				{
					$embedCode = (isset($embedCodeArr['embedCode'])) ? $embedCodeArr['embedCode'] : false;
					return array('type' => $embedKey, 'embedCode' => $embedCode);
				}
			}
			else // method = RegexPage
			{
				$htmlSource = file_get_contents($url, "r");				
				if (preg_match($embedData['getCodeAction']['regex'], $htmlSource, $embedCodeArr))
				{
					$embedCode = (isset($embedCodeArr['embedCode'])) ? $embedCodeArr['embedCode'] : false;
					return array('type' => $embedKey, 'embedCode' => $embedCode);
				}
			}
		}
		
	return array();
}

function syndicateVideoFeed($ownerID)
{
	$fetchInterval = 600; // 10 min 
	
	// Get syndicate video url and permissions
	$syndVideoData = db_arr("SELECT synd_video_url, synd_video_perm 
		FROM ProfilesSettings WHERE IDMember = $ownerID");

	if (!isset($syndVideoData['synd_video_url']) || 
		!trim($syndVideoData['synd_video_url'])) return;
		
	$syndVideoUrl = trim($syndVideoData['synd_video_url']);
	$syndVideoPerm = (isset($syndVideoData['synd_video_perm'])) ? 
		$syndVideoData['synd_video_perm'] : 0;

	$videoFeedData = db_arr("SELECT * FROM profile_videofeed 
		WHERE userid = $ownerID AND synd_url = '$syndVideoUrl'");

	// Check for last fetching time
	if (!empty($videoFeedData) && $syndVideoUrl == $videoFeedData['synd_url'] &&
		time() - $videoFeedData['synd_last_run'] < $fetchInterval)
		return;

	// Parse rss feed
	include_once('simplepie/simplepie.inc');
	include_once('simplepie/idn/idna_convert.class.php');	
	
	$feed = new SimplePie();
	$feed->set_feed_url($syndVideoUrl);
	$feed->enable_cache(false);	
	$feed->init();		
	$feed->handle_content_type();
	
	$items = $feed->get_items();	
	$itemsKeys = array_keys($items);
	$itemsCount = $feed->get_item_quantity();

	// Search new videos by hash to syndicate with existing videos
	$indexSaveFrom = $itemsCount - 1;
	if (!empty($videoFeedData) && $videoFeedData['synd_hash'])
		for ($i = $indexSaveFrom; $i >= 0; $i--)
		{
			$curVideo = $items[$itemsKeys[$i]];
			$curHash = md5($curVideo->get_title() . $curVideo->get_link());	
			if ($curHash == $videoFeedData['synd_hash'])
			{					
				$indexSaveFrom = $i - 1;
				break;
			}
		}
	if ($indexSaveFrom < 0) return;

	// Loop from the last(oldest) item to the first(newest) item.
	// It's guarantee that last hash will be for the newest video
	for ($i = $indexSaveFrom; $i >= 0; $i--)
	{
		$item = $items[$itemsKeys[$i]];
		
		// Prepare video data to save
		$videoTitle = mysql_escape_string($item->get_title());
		$urltitle = getSEFUrl($videoTitle, sefVideoWords);
		$urltitle = uniqueSEFUrl($urltitle, $ownerID, 'Owner', 'RayMovieFiles');
		$embedData = _getEmbedData(urldecode($item->get_link()));
		if (empty($embedData)) continue;
		$videoService = $embedData['type'];
		$embedCode = $embedData['embedCode'];
		
		// Get video thumb link from content	
		$imgLinkMatches = preg_match('/<img[^>]*src=.?(http.*?)"/i', 
			html_entity_decode($item->get_content()), $matches);
		$imageUrl = (empty($matches)) ? '' : $matches[1];

		// Save video
		$now = time();
		$saveQuery = "INSERT INTO `RayMovieFiles` 
			(Title, Tags, Description, Date, Owner, Type, EmbedId, Permission, urltitle) 
			VALUES ('$videoTitle', '', '',	$now, $ownerID, '$videoService', 
				'$embedCode', $syndVideoPerm, '$urltitle')";
		if (db_res($saveQuery))
		{
			$lastId = mysql_insert_id();
			if ($imageUrl && strpos($imageUrl, 'ThumbnailServer') === false)
			{
				$videoFileName = "{$lastId}_small.jpg";
				$imageUrl = str_replace("&amp;", "&", $imageUrl);
				copy($imageUrl, "ray/modules/movie/files/$videoFileName");
			}
			else
				$videoFileName = '';
				
			TrackNewAction(5, $lastId, $ownerID, $videoTitle, '', $videoFileName);
		}	
	}
	
	if (!$item) return;
	
	// Update syndicate video hash (hash of the last video) and last run time
	$lastHash = md5($item->get_title() . $item->get_link());
	db_res("INSERT INTO profile_videofeed 
		(userid, synd_hash, synd_last_run, synd_url)
		VALUES ($ownerID, '$lastHash', UNIX_TIMESTAMP(), '$syndVideoUrl')
		ON DUPLICATE KEY UPDATE synd_hash = '$lastHash', synd_last_run = UNIX_TIMESTAMP()");
}

?>