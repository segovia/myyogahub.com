<?php
/***************************************************************************
 *                            Dolphin Web Community Software
 *                              -------------------
 *     begin                : Mon Mar 23 2006
 *     copyright            : (C) 2006 BoonEx Group
 *     website              : http://www.boonex.com
 *
 *     
 *
 ****************************************************************************/

/***************************************************************************
 *
 *   This is a free software; you can modify it under the terms of BoonEx 
 *   Product License Agreement published on BoonEx site at http://www.boonex.com/downloads/license.pdf
 *   You may not however distribute it for free or/and a fee. 
 *   This notice may not be removed from the source code. You may not also remove any other visible 
 *   reference and links to BoonEx Group as provided in source code.
 *
 ***************************************************************************/

@list($url, $vars) = explode('?', $_SERVER['REQUEST_URI']);
if( $url == '/' )
{
	require_once('index.php');
	exit;
}


$urlArr = explode('/', $_SERVER['REQUEST_URI']);
$rewriteNick = (strlen(trim($urlArr[count($urlArr) - 1])) ? $urlArr[count($urlArr) - 1] : $urlArr[count($urlArr) - 2]);
if ( !get_magic_quotes_gpc() )
{
	$rewriteNick = addslashes($rewriteNick);
}

$flag = 0;
$tmp = $rewriteNick;

if (preg_match("/^rewrite_name\.php\?ID=(\d+)/",$rewriteNick,$m))
{
	$flag = 1;
	$rewriteNick = " `ID` = '" . (int)($m[1]) . "'";
}
elseif (preg_match("/^profile\.php\?ID=(\d+)/",$rewriteNick,$m))
{
	$flag = 2;
	$rewriteNick = " `ID` = '" . (int)($m[1]) . "'";
}
elseif (ereg("([a-z0-9]+.php)",$rewriteNick,$m)) {
	Header( "Location: {$rewriteNick}" );
	exit;
}
else
{
	$rewriteNick = " `NickName` = '$rewriteNick'";
}

require_once( "inc/header.inc.php" );
require_once( BX_DIRECTORY_PATH_INC .'db.inc.php' );

$sRequest = "SELECT `ID`,`NickName` FROM `Profiles` WHERE {$rewriteNick}";
$profArr = db_arr( $sRequest );

if ($profArr)
{
	$_REQUEST['ID'] = $profArr['ID'];
	if( $flag )
	{
		header("Location: " . $site['url'] . $profArr['NickName']);
	}
	require_once( BX_DIRECTORY_PATH_ROOT. 'profile.php' );
	exit();
}
else
{
	header("HTTP/1.0 404 Not Found");
	header("Status: 404 Not Found");
	header("Location: /404.php");
	//echo "Page not found $tmp";
}

?>
