<?php
header("Content-type: text/xml; charset=utf-8");

require_once "inc/header.inc.php";
require_once "inc/db.inc.php";
require_once "inc/admin.inc.php";
require_once "phptal/PHPTAL.php";

define('BLOG_FEED_ITEMS_NUMBER', 10);

function PureDescription($description)
{
	$description = preg_replace('/<[^>]+>/s', ' ', $description);
	$description = preg_replace('/\s{2,}/s', ' ', $description);
	$description = RemoveHTMLEntities($description);
	return trim($description);
}

try
{

// Is redirect from SEF url
if (isset($_GET['sefNick']))
{
	$ownerID = getID($_GET['sefNick']);
	if (!$ownerID)
		throw new Exception('Member not found');
}	
elseif (!isset($_REQUEST['owner']))
	throw new Exception('Parameters not specified');
if (!$ownerID)
	$ownerID = $_REQUEST['owner'];

if (!isset($_REQUEST['type']) || $_REQUEST['type'] != 'rss' && $_REQUEST['type'] != 'atom')
	throw new Exception('Incorrect feed type');
$feedType = $_REQUEST['type'];	

$tal = new PHPTAL("tal/blogfeed-$feedType.tal");

$postsQuery = "SELECT * FROM BlogPosts 
	LEFT JOIN BlogCategories
		ON BlogPosts.CategoryID = BlogCategories.CategoryID
	WHERE BlogCategories.OwnerID=$ownerID
		AND	PostReadPermission='public'
	ORDER BY PostDate DESC
	LIMIT " . BLOG_FEED_ITEMS_NUMBER;	
$itemsRes = mysql_query($postsQuery);
if (!$itemsRes)
	throw new Exception('MySQL Error');
	
$dateFormat = ($feedType == 'rss') ? DATE_RFC822 : DATE_ISO8601;

if (useSEF)
	$itemLinkTmpl = ($feedType == 'rss') ? "<link>{$site['url']}{$_GET['sefNick']}/%urltitle%.html</link>" 
		: "{$site['url']}{$_GET['sefNick']}/%urltitle%.html";
else
	$itemLinkTmpl = ($feedType == 'rss') ? "<link>{$site['url']}blogs.php?action=show_member_post&amp;ownerID=$ownerID&amp;post_id=%postid%</link>" 
		: "{$site['url']}blogs.php?action=show_member_post&ownerID=$ownerID&post_id=%postid%";

// Use standart link if urltitle is empty
$itemLinkTmplStd = ($feedType == 'rss') ? "<link>{$site['url']}blogs.php?action=show_member_post&amp;ownerID=$ownerID&amp;post_id=%postid%</link>" 
	: "{$site['url']}blogs.php?action=show_member_post&ownerID=$ownerID&post_id=%postid%";
	
$items = fill_assoc_array($itemsRes);
foreach ($items as &$item)
{
	$item['title'] = $item['PostCaption'];
	if ($item['urltitle'] == '')
		$item['link'] = str_replace('%postid%', $item['PostID'], $itemLinkTmplStd);
	else
		$item['link'] = str_replace(array('%urltitle%', '%postid%'), array($item['urltitle'], $item['PostID']), $itemLinkTmpl);
	$item['description'] = "<![CDATA[". PureDescription($item['PostText']) ."]]>";
	$item['pubDate'] = gmdate($dateFormat, strtotime($item['PostDate']));
}

// Fills channel array by blog attributes
$channel = array();

$ownerDataQuery = "SELECT NickName FROM Profiles WHERE ID = $ownerID";
$ownerDataRes = mysql_query($ownerDataQuery);
if (!$ownerDataRes)
	throw new Exception('MySQL Error');
$ownerData = mysql_fetch_assoc($ownerDataRes);
if (!$ownerData['NickName'])
	throw new Exception('Member not found');

$channel['title'] = "Blog Posts of {$ownerData['NickName']}";
if (useSEF)
	$channel['link'] = ($feedType == 'rss') ? "<link>{$site['url']}{$_GET['sefNick']}/blog</link>"
		: "{$site['url']}{$_GET['sefNick']}/blog";
else
	$channel['link'] = ($feedType == 'rss') ? "<link>{$site['url']}blogs.php?action=show_member_blog&amp;ownerID=$ownerID</link>"
		: "{$site['url']}blogs.php?action=show_member_blog&ownerID=$ownerID";	
$channel['description'] = "Blog Posts of {$ownerData['NickName']}";
$channel['buildDate'] = $items[0]['pubDate'];
$channel['owner'] = $ownerData['NickName'];

// Set tal variables
$tal->items = $items;
$tal->channel = $channel;

echo $tal->execute();

}
catch(Exception $e)
{
	echo $e->getMessage();
}

?>