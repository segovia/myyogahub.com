<?



/***************************************************************************



*                            Dolphin Smart Community Builder



*                              -----------------



*     begin                : Mon Mar 23 2006



*     copyright            : (C) 2006 BoonEx Group



*     website              : http://www.boonex.com/



* This file is part of Dolphin - Smart Community Builder



*



* Dolphin is free software. This work is licensed under a Creative Commons Attribution 3.0 License. 



* http://creativecommons.org/licenses/by/3.0/



*



* Dolphin is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;



* without even the implied warranty of  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.



* See the Creative Commons Attribution 3.0 License for more details. 



* You should have received a copy of the Creative Commons Attribution 3.0 License along with Dolphin, 



* see license.txt file; if not, write to marketing@boonex.com



***************************************************************************/





require_once( 'inc/header.inc.php' );

require_once( BX_DIRECTORY_PATH_INC . 'design.inc.php' );

require_once( BX_DIRECTORY_PATH_INC . 'profiles.inc.php' );



// --------------- page variables and login



$_page['name_index'] 	= 34;

$_page['css_name']		= 'unregister.css';



$logged['member'] = member_auth(0);



$_page['header'] = _t("_Delete account");

$_page['header_text'] = _t("_Delete account");



// --------------- page components



$_ni = $_page['name_index'];

$_page_cont[$_ni]['page_main_code'] = PageCompPageMainCode();





// --------------- [END] page components



PageCode();



// --------------- page components functions



/**

 * page code function

 */

function PageCompPageMainCode()
{
	global $site;

	// Get member nickname
	$nickName = db_value("SELECT NickName FROM Profiles WHERE ID={$_COOKIE['memberID']} LIMIT 1");

	if ( $_POST['DELETE'] )
	{
		profile_delete($_COOKIE['memberID']);

	    setcookie( 'memberID', $_COOKIE['memberID'], time() - 3600, '/' );
		setcookie( 'memberPassword', $_COOKIE['memberPassword'], time() - 3600, '/' );

		// Send email about deleted account
		$emailSubject = str_replace(array('{NickName}', '{fullName}'),
			array($nickName, $_POST['fullname']), 
			getParam('t_DeleteAccountMail_subject'));
		$emailMessage = str_replace(array('{memberID}', '{NickName}', '{fullName}', '{reason}'),
			array($_COOKIE['memberID'], $nickName, $_POST['fullname'], $_POST['reason']), 
			getParam('t_DeleteAccountMail'));
		$emailNotify = getParam('notifications_email');
		$emailNotifyTitle = getParam('notifications_email_title');
		$sMailHeader = "MIME-Version: 1.0\r\n" . "Content-type: text/html; charset=UTF-8\r\n" . 
			"From: $emailNotify <$emailNotifyTitle>";
		$sMailParameters = "-f$emailNotifyTitle";

		mail($site['email_notify'], getParam('t_DeleteAccountMail_subject'), $emailMessage, $sMailHeader, $sMailParameters);

		return "<center>"._t("_DELETE_SUCCESS", $site[url])."</center>";
	}

	ob_start();

	echo spacer(1,5);
?>

<script>

function CheckDeleteAccount()
{
	var submitButton = document.forms.deleteAccountForm.deleteAccountSubmit;
	var agree = document.forms.deleteAccountForm.deleteAgree.checked;
	var initials = document.forms.deleteAccountForm.initials;
	var fullName = document.forms.deleteAccountForm.fullname;
	var reason = document.forms.deleteAccountForm.reason;

	submitButton.disabled = (!agree || initials.value.length == 0 || fullName.value.length == 0 || reason.value.length == 0);
}

</script>


<table width="100%" cellpadding="4" cellspacing="4">

	<td align="center" class="text2">

		<form action="<? echo $_SERVER['PHP_SELF']; ?>" method="post" name="deleteAccountForm">

			<input type="hidden" name="DELETE" value="1">

			<div style="text-align:left"/>

				<?= _t("_DELETE_TEXT"); ?><br /><br />

				<input type="checkbox" onclick="CheckDeleteAccount()" name="deleteAgree"/>

				Yes, I <?= $nickName ?> have read and fully understand the consequences of this action and I still want to delete my account. 

				<br/>

				<table>
					<tr><td>User Initials:</td><td><input type="text" name="initials" onkeyup="CheckDeleteAccount()"/></td></tr>
					<tr><td>Full Name:</td><td><input type="text" name="fullname" onkeyup="CheckDeleteAccount()"/></td></tr>
				</table>
				<br/>Please tell us in as many words as possible, why you have you decided to delete your account?
				<textarea style="width:300px" name="reason" onkeyup="CheckDeleteAccount()"/></textarea>
			</div>

			<br/>

			<center>

				<input class="no" type="submit" onclick="return confirm('This is your last chance to cancel this action, if you click okay your account will be permanently deleted.');" value="<?= _t("_Delete account"); ?>" disabled="1" name="deleteAccountSubmit">

				<input type="button" value="Cancel" onclick="history.back();"/>

				<br />

				<br />

			</center>

		</form>

	</td>

</table>



<?php



    $ret = ob_get_clean();



    return $ret;

}



?>