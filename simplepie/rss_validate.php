<?php

/*******
/* Print 'OK' if $_POST['RSSBlogFeedURL'] is correct RSS source and '' otherwise
*******/

include_once('simplepie.inc');
include_once('idn/idna_convert.class.php');

// Set error reporting for this
error_reporting(E_ERROR);

$feed = new SimplePie();
$feed->set_feed_url($_POST['RSSBlogFeedURL']);
$feed->enable_cache(false);
$feedInitialized = $feed->init();

// Restore original error reporting value
@ini_restore('error_reporting');

echo ($feedInitialized) ? 'OK' : $feed->error;

?>