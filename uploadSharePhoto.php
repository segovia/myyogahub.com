<?php



/***************************************************************************

*                            Dolphin Smart Community Builder

*                              -----------------

*     begin                : Mon Mar 23 2006

*     copyright            : (C) 2006 BoonEx Group

*     website              : http://www.boonex.com/

* This file is part of Dolphin - Smart Community Builder

*

* Dolphin is free software. This work is licensed under a Creative Commons Attribution 3.0 License. 

* http://creativecommons.org/licenses/by/3.0/

*

* Dolphin is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;

* without even the implied warranty of  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

* See the Creative Commons Attribution 3.0 License for more details. 

* You should have received a copy of the Creative Commons Attribution 3.0 License along with Dolphin, 

* see license.txt file; if not, write to marketing@boonex.com

***************************************************************************/



require_once('inc/header.inc.php');

require_once( BX_DIRECTORY_PATH_INC . 'design.inc.php' );

require_once( BX_DIRECTORY_PATH_INC . 'admin.inc.php' );

require_once( BX_DIRECTORY_PATH_INC . 'images.inc.php' );

require_once( BX_DIRECTORY_PATH_INC . 'tags.inc.php' );





$_page['name_index']	= 82;

$_page['css_name']		= 'viewPhoto.css';



$_page['extra_js'] = '';



if ( !( $logged['admin'] = member_auth( 1, false ) ) )

{

	if ( !( $logged['member'] = member_auth( 0, false ) ) )

	{

		if ( !( $logged['aff'] = member_auth( 2, false ) ) )

		{

			$logged['moderator'] = member_auth( 3, false );

		}

	}

}





$_page['header'] = _t( "_upload Photo" );

$_page['header_text'] = _t("_upload Photo");



$_ni = $_page['name_index'];



$member['ID'] = (int)$_COOKIE['memberID'];

checkCommunityPermissions($member['ID']);


$sStatus = '';

if (isset($_POST['upload']) && isset($_POST['medProfId']))
{
	$editMode = (isset($_POST['action']) && $_POST['action'] == 'edit');
	
	if (isset($_POST['actionPhoto']) && $_POST['actionPhoto'] == 'avatar')
	{
		// Set this photo as avatar
		require_once(BX_DIRECTORY_PATH_ROOT . 'uploadPhoto.php');
		
		$_FILES['photo'] = $_FILES['uploadFile'];
		$oMedia = new UploadPhoto($_POST['medProfId']);
		$oMedia->getMediaArray();
		$oMedia->uploadMedia();
		updateAvatarsInMemcache($_POST['medProfId']);
		
		if ($_POST['actionPhoto'] == 'avatar')
			header("Location:{$site['url']}upload_media.php?show=photo");
	}
	
	// Update old photo info without uploading new photo
	if ($editMode && isset($_POST['medID']) && $_FILES['uploadFile']['tmp_name'] == '') 
	{
		$postTitle = process_db_input($_POST['title']);
		$postTags = process_db_input($_POST['tags']);
		$postDesc = process_db_input($_POST['description']);

		$updatePhotoQuery = "UPDATE sharePhotoFiles 
			SET medTitle = '$postTitle', medTags = '$postTags', medDesc = '$postDesc', 
				Permission = {$_POST['photoPerm']} $eventFields
			WHERE medID = {$_POST['medID']} LIMIT 1";
		$updateResult = (db_res($updatePhotoQuery)) ? 1 : 0;

		header("Location:{$site['url']}viewPhoto.php?fileID={$_POST['medID']}&updateResult=$updateResult");
	}
	else // upload new photo
	{
		$assocID = isset($_POST['eventID']) ? $_POST['eventID'] : 0;
		$sStatus = '<div>'._t("_File was uploaded").'</div>';
		$iUser = (int)$_POST['medProfId'];
		$sFile = isset($_POST['title']) && strlen($_POST['title']) ? 
			process_db_input($_POST['title']) : 'Untitled';
		$sDesc = isset($_POST['description']) && strlen($_POST['description']) ? 
			process_db_input($_POST['description']) : '';
		$sTags = isset($_POST['tags']) && strlen($_POST['tags']) ? 
			process_db_input($_POST['tags']) : '';
		$sStatus = uploadFile($sFile, $sDesc, $sTags, $iUser, $assocID, $_POST['photoPerm']);
	}
}

$_page_cont[$_ni]['page_main_code'] = $sStatus.PageMainCode();

PageCode();


function PageMainCode()
{
	global $site;
	global $member;

	// Check is visitor have Unconfirmed status
	$memberInfo = db_arr("SELECT * FROM `Profiles` WHERE ID={$member['ID']}");
	$isUnconfirmed = ($memberInfo['Status'] == 'Unconfirmed');	

	if ($isUnconfirmed)
		return '<div style="color:red">Sorry, you cannot upload photos while your account status is unconfirmed.</div>';

	$sCode = '<div id="agreement" style="text-align: center;"><div style="font-weight: bold;">'._t("_Media upload Agreement",_t("_Photo")).'</div><div><textarea rows="20" cols="80" readonly="true">'._t("_License Agreement",$site['url']).'</textarea></div><div><input type="submit" id="agree" value="'._t("_I agree").'" onclick="document.getElementById(\'uploadShareMain\').style.display = \'block\'; 

	document.getElementById(\'agreement\').style.display = \'none\';""></div></div>';

	$sCode .= '<div id="uploadShareMain" style="display: none;">';

	

	$sCode .= "<form enctype=\"multipart/form-data\" method=\"post\" action=\"{$_SERVER['PHP_SELF']}\">";
	
	$sCode .= "<table><tr><td width='520px'>";

	$sCode .= '<div class="uploadLine"><div class="uploadText">'._t("_Title").': </div>
		<div style="float:left"><input type="text" name="title" class="uploadForm"/></div></div>';

	$sCode .= '<div class="uploadLine"><div class="uploadText">'._t("_Description").': </div>
		<div style="float:left"><textarea name="description" class="uploadForm"/></textarea></div></div>';

	$sCode .= '<div class="uploadLine"><div class="uploadText">'._t("_Tags").': </div>
		<div style="float:left"><input type="text" name="tags" class="uploadForm"/></div></div>';

	$sCode .= '<div class="uploadLine"><div class="uploadText">'._t("_Select").': </div>
		<div style="float:left"><input type="file" name="uploadFile" size="40"/></div></div>';

	$sCode .= "</td><td style='vertical-align: top; padding-left: 20px;'>";	
	$sCode .= '<b>Photo Permissions</b><div style="margin-top:5px">
		<input type="radio" name="photoPerm" value="0" checked/>public<br/>
		<input type="radio" name="photoPerm" value="1"/>friends only
		</div></td></tr>';	
	
	if (!$memberInfo['PrimPhoto'])
		$sCode .= '<tr><td colspan="2"><div style="margin:0 0 10px 100px">
It appears you do not yet have a profile photo, would you like to:<br/><br/>	
<input type="radio" name="actionPhoto" value="avatar" checked="1"> Make this image your primary profile photo?<br/>
<input type="radio" name="actionPhoto" value="photo"> Continue and only upload this image to your photo gallery?<br/>
<input type="radio" name="actionPhoto" value="photo_avatar"> Upload this image to both your photo gallery and make it your primary profile photo?
</div></td>';		
	
	$sCode .= "</tr></table>";
	
	$sCode .= '<input type="hidden" name="medProfId" value="'.$member['ID'].'"/>';
	
	$sCode .= '<input type="hidden" name="eventID" value="'.$_GET['eventID'].'"/>';
	
	$sCode .= '<div style="text-align:center"><input type="submit" name="upload" value="'._t("_Upload File").'"/></div>';

	$sCode .= '</form>';

	$sCode .= '</div>';

	return $sCode;
}



function uploadFile($sFile, $sDesc, $sTags, $iUser, $assocID=0, $permission)
{
	global $dir;
	global $editMode;

	if( $_FILES['uploadFile']['error'] != 0 )
	{
		if ($editMode)
			header("Location:{$site['url']}viewPhoto.php?fileID={$_POST['medID']}&updateResult=0");
		else
			$sCode = '<div class="uploadStatus">'._t("_File upload error").'</div>';
	}
	else
	{
		$aFileInfo = getimagesize( $_FILES['uploadFile']['tmp_name'] );

		if(!$aFileInfo)
		{
			if ($editMode)
				header("Location:{$site['url']}viewPhoto.php?fileID={$_POST['medID']}&updateResult=0");
			else
				$sCode = '<div class="uploadStatus">'._t("_You uploaded not image file").'</div>';
		}
		else
		{
			$ext = false;

			switch( $aFileInfo['mime'] )
			{
				case 'image/jpeg': $ext = 'jpg'; break;
				case 'image/gif':  $ext = 'gif'; break;
				case 'image/png':  $ext = 'png'; break;
				default:           $ext = false;
			}

			if( !$ext )
			{
				if ($editMode)
					header("Location:{$site['url']}viewPhoto.php?fileID={$_POST['medID']}&updateResult=0");
				else
					$sCode = '<div class="uploadStatus">'._t("_You uploaded not JPEG, GIF or PNG file").'</div>';
			}
			else
			{
				$sCode = '<div class="uploadStatus">'._t("_Upload successful").'</div>';

				$sActive = getParam("enable_shPhotoActivation") == 'on' ? 'true' : 'false' ;

				if ($editMode)
				{
					$eventFields = ($assocID > 0) ? ", AssocID=$assocID, AssocType='event'" : '';
					$sQuery = "UPDATE `sharePhotoFiles` 
						SET `medProfId`=$iUser, `medTitle`='$sFile', `medExt`='$ext',
							`medDesc`='$sDesc', `medTags`='$sTags', `medDate`=NOW(), 
							`Approved`='$sActive', `Permission`=$permission $eventFields
						WHERE `medID`={$_POST['medID']} LIMIT 1";
				}
				else
				{
					if ($assocID > 0)
					{
						$assocFields = ',`AssocID`,`AssocType`';
						$assocValues = ", $assocID, 'event'";
					}
					else 
					{
						$assocFields = '';
						$assocValues = '';
					}
					$urltitle = getSEFUrl($sFile, sefPhotoWords);
					$urltitle = uniqueSEFUrl($urltitle, $iUser, 'medProfId', 'sharePhotoFiles');
					$sQuery = "INSERT INTO `sharePhotoFiles` 
						(`medProfId`,`medTitle`,`medExt`,`medDesc`,`medTags`,`medDate`,`Approved`,`Permission`,`urltitle` $assocFields) 
						VALUES($iUser,'$sFile','$ext','$sDesc','$sTags',NOW(),'$sActive',$permission,'$urltitle'  $assocValues)";
				}

				$updateResult = (db_res($sQuery)) ? 1 : 0;

				$iNew = ($editMode) ? $_POST['medID'] : mysql_insert_id();

				reparseObjTags( 'photo', $iNew );

				$sNewFileName = $dir['sharingImages'] . $iNew.'.'.$ext;
				$sNewMainName = $dir['sharingImages'] . $iNew.'_m.'.$ext;
				$sNewThumbName = $dir['sharingImages'] . $iNew.'_t.'.$ext;

				if( !move_uploaded_file( $_FILES['uploadFile']['tmp_name'], $sNewFileName ))
				{
					if ($editMode)
						header("Location:{$site['url']}viewPhoto.php?fileID={$_POST['medID']}&updateResult=0");
					else
						$sCode = '<div class="uploadStatus">'._t("_Couldn\'t move file").'</div>';
				}
				else
				{
					$iWidth  = (int)getParam("max_photo_width");
					$iHeight = (int)getParam("max_photo_height");

					$iThumbW = (int)getParam("max_thumb_width");
					$iThumbH = (int)getParam("max_thumb_height");

					if( imageResize( $sNewFileName, $sNewMainName, $iWidth, $iHeight ) != IMAGE_ERROR_SUCCESS)
					{
						if ($editMode)
							header("Location:{$site['url']}viewPhoto.php?fileID={$_POST['medID']}&updateResult=0");
						else
							$sCode = '<div class="uploadStatus">'._t("_Upload failed").'</div>';
					}
					else
					{
						if (!$editMode)
						{
							$photoTitle = ($sFile == '') ? 'Untitled' : $sFile;
							TrackNewAction(3, $iNew, $iUser, $photoTitle, '', $iNew.'_t.'.$ext);
						}

						imageResize( $sNewMainName, $sNewThumbName, $iThumbW, $iThumbH );
						
						if (isset($_POST['actionPhoto']) && $_POST['actionPhoto'] == 'photo_avatar')
						{
							// Set this photo as avatar
							require_once(BX_DIRECTORY_PATH_ROOT . 'uploadPhoto.php');
							
							$oMedia = new UploadPhoto($_POST['medProfId']);
							$oMedia->getMediaArray();
							$sMediaDir = $oMedia->getProfileMediaDir();
							$sFileName = time();
							copy($sNewMainName, "{$sMediaDir}photo_$sFileName.$ext");
							copy($sNewThumbName, "{$sMediaDir}thumb_$sFileName.$ext");
							imageResize("{$sMediaDir}photo_$sFileName.$ext", "{$sMediaDir}icon_$sFileName.$ext", 
								$oMedia->aMediaConfig['size']['iconWidth'], $oMedia->aMediaConfig['size']['iconHeight'], true );
							
							$oMedia->insertMediaToDb("$sFileName.$ext");
							$oMedia->oMediaQuery->setPrimaryPhoto($_POST['medProfId'], mysql_insert_id());
							updateAvatarsInMemcache($_POST['medProfId']);
						}
						
						if ($editMode)
							header("Location:{$site['url']}viewPhoto.php?fileID={$_POST['medID']}&updateResult=$updateResult");
						else
						{
							if ($assocID > 0)
								header("Location:events.php?action=show_info&event_id=$assocID");
							else 
								header("Location:viewPhoto.php?fileID=".$iNew);
						}

						exit;
					}	
				}
			}
		}
	}

	return $sCode;
}

?>