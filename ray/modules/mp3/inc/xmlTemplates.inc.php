<?
/***************************************************************************
*
* IMPORTANT: This is a commercial product made by BoonEx Ltd. and cannot be modified for other than personal usage.
* This product cannot be redistributed for free or a fee without written permission from BoonEx Ltd.
* This notice may not be removed from the source code.
*
***************************************************************************/

$aXmlTemplates = array (
	"cat" => array (
		2 => '<cat id="#1#"><title><![CDATA[#2#]]></title></cat>',
		3 => '<cat id="#1#" parent="#2#"><path><![CDATA[#3#]]></path></cat>'
	),
		
	"file" => array (
		2 => '<file id="#1#" file="#2#" />',
		4 => '<file id="#1#" time="#2#" file="#3#"><title><![CDATA[#4#]]></title></file>',
		6 => '<file id="#1#" time="#2#" file="#3#"><title><![CDATA[#4#]]></title><tags><![CDATA[#5#]]></tags><desc><![CDATA[#6#]]></desc></file>',
		7 => '<file id="#1#" time="#2#" file="#3#" reports="#4#" profile="#5#" approved="#6#"><title><![CDATA[#7#]]></title></file>'
	),
		
	"result" => array (
		1 => '<result value="#1#" />',
		2 => '<result value="#1#" status="#2#" />'		
	)
);
?>