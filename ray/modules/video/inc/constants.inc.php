<?
/***************************************************************************
*
* IMPORTANT: This is a commercial product made by BoonEx Ltd. and cannot be modified for other than personal usage.
* This product cannot be redistributed for free or a fee without written permission from BoonEx Ltd.
* This notice may not be removed from the source code.
*
***************************************************************************/

        if(!defined("APPROVE_VAL")) define("APPROVE_VAL", "approval");
        if(!defined("ACTIVE_VAL")) define("ACTIVE_VAL", "active");

        $aInfo = array(
                'title' => "Video Recorder",
                'version' => "3.0.0000",
                'code' => "video_3.0.0000_free",
                'author' => "Boonex",
                'authorUrl' => "http://www.boonex.com"
        );
        $aModules = array(
                'recorder' => array(
                        'caption' => 'Video Recorder',
                        'parameters' => array('id', 'password'),
                        'js' => array(),
                        'inline' => false,
                        'vResizable' => false,
                        'hResizable' => false,
                        'reloadable' => true,
                        'layout' => array('top' => 0, 'left' => 0, 'width' => 545, 'height' => 355),
                                                'minSize' => array('width' => 545, 'height' => 355),
                        'div' => array(
                                'id' => '',
                                'name' => '',
                                'style' => array()
                        )
                ),
                'player' => array(
                        'caption' => 'Video Player',
                        'parameters' => array('id'),
                        'js' => array(),
                        'inline' => false,
                        'vResizable' => false,
                        'hResizable' => false,
                        'reloadable' => false,
                        'layout' => array('top' => 0, 'left' => 0, 'width' => 375, 'height' => 355),
                                                'minSize' => array('width' => 375, 'height' => 355),
                        'div' => array(
                                'id' => '',
                                'name' => '',
                                'style' => array()
                        )
                ),
                'admin' => array(
                        'caption' => 'Video Player Admin',
                        'parameters' => array('nick', 'password'),
                        'js' => array(),
                        'inline' => false,
                        'vResizable' => false,
                        'hResizable' => false,
                        'reloadable' => false,
                        'layout' => array('top' => 0, 'left' => 0, 'width' => 400, 'height' => 250),
                                                'minSize' => array('width' => 400, 'height' => 250),
                        'div' => array(
                                'id' => '',
                                'name' => '',
                                'style' => array()
                        )
                )
        );
?>