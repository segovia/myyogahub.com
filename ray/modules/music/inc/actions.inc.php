<?
/***************************************************************************
*
* IMPORTANT: This is a commercial product made by BoonEx Ltd. and cannot be modified for other than personal usage.
* This product cannot be redistributed for free or a fee without written permission from BoonEx Ltd.
* This notice may not be removed from the source code.
*
***************************************************************************/

$sId = isset($_REQUEST['id']) ? $_REQUEST['id'] : "";
$sViewerId = isset($_REQUEST['viewer']) ? $_REQUEST['viewer'] : "";
$sFile = isset($_REQUEST['file']) ? $_REQUEST['file'] : "0";
$sSecondFile = isset($_REQUEST['file2']) ? $_REQUEST['file2'] : "0";
$sTitle = isset($_REQUEST['title']) ? $_REQUEST['title'] : "Unknown title";
$sTags = isset($_REQUEST['tags']) ? $_REQUEST['tags'] : "";
$sDesc = isset($_REQUEST['description']) ? $_REQUEST['description'] : "";
$sTime = isset($_REQUEST['time']) ? $_REQUEST['time'] : "0";
$sCategory = isset($_REQUEST['category']) ? $_REQUEST['category'] : "-1";
$sNick = isset($_REQUEST['nick']) ? $_REQUEST['nick'] : "";
$sPassword = isset($_REQUEST['password']) ? $_REQUEST['password'] : "";

$sSkin = isset($_REQUEST['skin']) ? $_REQUEST['skin'] : "";
$sLanguage = isset($_REQUEST['language']) ? $_REQUEST['language'] : "english";

switch ($sAction)
{
	/**
	* gets skins
	*/
	case 'getSkins':
		$sContents = printFiles($sModule, "skins");
		break;

	/**
	* Sets default skin.
	*/
	case 'setSkin':
		setCurrentFile($sModule, $sSkin, "skins");
		break;

	/**
	* gets languages
	*/
	case 'getLanguages':
		$sContents = printFiles($sModule, "langs");
		break;

	/**
	* Sets default language.
	*/
	case 'setLanguage':
		setCurrentFile($sModule, $sLanguage, "langs");
		break;

	/**
	* Get mp3 config
	*/
	case 'config':
		$sFileName = $sModulesPath . $sModule . "/xml/config.xml";
		$rHandle = fopen($sFileName, "rt");
		$sContents = fread($rHandle, filesize($sFileName)) ;
		fclose($rHandle);
		$sContents = str_replace("#filesUrl#", $sFilesUrl, $sContents);
		break;

	/**
	* Authorize admin
	*/
	case 'adminAuthorize':
		$sContents .= parseXml($aXmlTemplates['result'], loginAdmin($sNick, $sPassword));
		break;

	/**
	* Authorize user.
	*/
	case 'userAuthorize':
		$sContents .= parseXml($aXmlTemplates['result'], loginUser($sId, $sPassword));
		break;

	/**
	* Add category
	*/
	case 'addCategory':
		$bResult = true;
		if($sCategory != '0')
		{
			$res = getResult("SELECT * FROM `" . MODULE_DB_PREFIX . "Categories` WHERE `ID`='" . $sCategory . "' LIMIT 1");
			$bResult = (mysql_num_rows($res) > 0);
		}
		if($bResult)
		{
			getResult("INSERT INTO `" . MODULE_DB_PREFIX . "Categories` VALUES('', '" . $sCategory . "', '" . $sTitle . "')");
			$iCategoryId = getLastInsertId();
			$sContents = parseXml($aXmlTemplates['result'], $iCategoryId);
		}
		else $sContents = parseXml($aXmlTemplates['result'], FALSE_VAL);
		break;

	/**
	* Get category contents
	*/
	case 'getCategoryContents':
		if($sCategory != "0")
		{
			$aCategory = getArray("SELECT * FROM `" . MODULE_DB_PREFIX . "Categories` WHERE `ID`='" . $sCategory . "' LIMIT 1");
			$sPath = getPath($sCategory);
			$sContents = parseXml($aXmlTemplates['cat'], $aCategory['ID'], $aCategory['Parent'], $sPath);
		}
		else $sContents = parseXml($aXmlTemplates['cat'], "0", "0", "/");
		$res = getResult("SELECT * FROM `" . MODULE_DB_PREFIX . "Categories` WHERE `Parent`='" . $sCategory . "' ORDER BY `Title`");
		$sContent = "";
		for($i=0; $i<mysql_num_rows($res); $i++)
		{
			$aCat = mysql_fetch_assoc($res);
			$sContent .= parseXml($aXmlTemplates['cat'], $aCat['ID'], $aCat['Title']);
		}
		$sContents .= makeGroup($sContent, "categories");
		$res = getResult("SELECT * FROM `" . MODULE_DB_PREFIX . "Files` WHERE `CategoryId`='" . $sCategory . "' ORDER BY `Title`");
		$sContent = "";
		for($i=0; $i<mysql_num_rows($res); $i++)
		{
			$aFile = mysql_fetch_assoc($res);
			$sContent .= parseXml($aXmlTemplates['file'], $aFile['ID'], $aFile['Time'], $aFile['ID'] . MP3_EXTENSION, $aFile['Title']);
		}
		$sContents .= makeGroup($sContent, "files");
		break;

	/**
	* Delete categories and then files
	*/
	case 'deleteCategories':
		if($sCategory == "") $bResult = false;
		else
		{
			$aCategories = explode(",", $sCategory);
			for($i=0; $i<count($aCategories); $i++)
				$bResult = deleteCategory($aCategories[$i]);
		}
		//break shouldn't be here

	/**
	* Delete files (reported files)
	*/
	case 'deleteFiles':
		if($sFile == "" && $sCategory == "")
			$sContents = parseXml($aXmlTemplates['result'], "msgErrorDelete", FAILED_VAL);
		elseif($sFile != "")
		{
			$aFiles = explode(",", $sFile);
			for($i=0; $i<count($aFiles); $i++)
				$bResult = deleteFile($aFiles[$i]);
			$sContents = parseXml($aXmlTemplates['result'], "", SUCCESS_VAL);
		}
		break;

	/**
	* Get user's playlist by ID
	*/
	case 'getPlayList':
		$res = getResult("SELECT * FROM `" . MODULE_DB_PREFIX . "Files` INNER JOIN `" . MODULE_DB_PREFIX . "PlayLists` ON `ID` = `FileId` WHERE `" . MODULE_DB_PREFIX . "PlayLists`.`Owner` = '" . $sId . "' ORDER BY `Order`");
		$bForEdit = $sViewerId == $sId;
		$sContents = "";
		for($i=0; $i<mysql_num_rows($res); $i++)
		{
			$aFile = mysql_fetch_assoc($res);
			$sFilename = $aFile['ID'] . MP3_EXTENSION;
			if(($bForEdit || $aFile['Approved'] == TRUE_VAL) && file_exists($sFilesPath . $sFilename))
				$sContents .= parseXml($aXmlTemplates['file'], $aFile['ID'], $aFile['Time'], $sFilename, $aFile['Title'], $aFile['Tags'], $aFile['Description']);
		}
		$sContents = makeGroup($sContents, "files");
		break;

	/**
	* Get user's playlist by ID
	*/
	case 'getFile':
		$sFilename = $sFile . MP3_EXTENSION;
		if(!file_exists($sFilesPath . $sFilename))
		{
			$sContents = parseXml($aXmlTemplates['result'], "msgFileNotFound", FAILED_VAL);
			break;
		}
		$aFile = getArray("SELECT * FROM `" . MODULE_DB_PREFIX . "Files` WHERE `ID` = '" . $sFile . "' LIMIT 1");
		if($aFile['Approved'] != TRUE_VAL)
		{
			$sContents = parseXml($aXmlTemplates['result'], "msgFileNotApproved", FAILED_VAL);
			break;
		}
		$sContents = parseXml($aXmlTemplates['result'], "", SUCCESS_VAL);
		$sContents .= parseXml($aXmlTemplates['file'], $aFile['ID'], $aFile['Time'], $sFilename, $aFile['Title'], $aFile['Tags'], $aFile['Description']);
		break;

	/**
	* Change playlist elements playing order
	*/
	case 'changeOrder':
		$res = getResult("SELECT `FileId`, `Order` FROM `" . MODULE_DB_PREFIX . "PlayLists` WHERE `Owner`='" . $sId . "' AND (`FileId`='" . $sFile . "' OR `FileId`='" . $sSecondFile . "')");
		$oSong1 = mysql_fetch_assoc($res);
		$oSong2 = mysql_fetch_assoc($res);
		getResult("UPDATE `" . MODULE_DB_PREFIX . "PlayLists` SET `Order`='" . $oSong1['Order'] . "' WHERE `FileId`='" . $oSong2['FileId'] . "' AND `Owner`='" . $sId . "'");
		getResult("UPDATE `" . MODULE_DB_PREFIX . "PlayLists` SET `Order`='" . $oSong2['Order'] . "' WHERE `FileId`='" . $oSong1['FileId'] . "' AND `Owner`='" . $sId ."'");
		$sContents .= parseXml($aXmlTemplates['result'], TRUE_VAL);
		break;

	/**
	* Report file
	*/
	case 'reportFile':
		getResult("UPDATE `" . MODULE_DB_PREFIX . "Files` SET `Reports`=(`Reports` + 1) WHERE `ID`='" . $sFile . "'");
		$sContents .= parseXml($aXmlTemplates['result'], TRUE_VAL);
		break;

	/**
	* Ignore reported files
	*/
	case 'ignoreReportedFiles':
		$aFiles = explode(",", $sFile);
		for($i=0; $i<count($aFiles); $i++)
			getResult("UPDATE `" . MODULE_DB_PREFIX . "Files` SET `Reports`='0' WHERE `ID`='" . $aFiles[$i] . "'");
		$sContents = parseXml($aXmlTemplates['result'], TRUE_VAL);
		break;

	/**
	* Get reported files
	*/
	case 'getReportedFiles':
		$sContents = "";
		$res = getResult("SELECT * FROM `" . MODULE_DB_PREFIX . "Files` WHERE `Reports` <> '0' ORDER BY `Title`");
		for($i=0; $i<mysql_num_rows($res); $i++)
		{
			$aFile = mysql_fetch_assoc($res);
			if($aFile['Owner'] == 0) $sProfile = "";
			else
			{
				$aUserInfo = getUserInfo($aFile['Owner']);
				$sProfile = $aUserInfo['profile'];
			}
			$sContents .= parseXml($aXmlTemplates['file'], $aFile['ID'], $aFile['Time'], $aFile['ID'] . MP3_EXTENSION, $aFile['Reports'], $sProfile, $aFile['Approved'], $aFile['Title']);
		}
		$sContents = makeGroup($sContents, "files");
		break;

	/**
	* Upload user's file
	*/
	case 'uploadFile':
		$sTempFileName = $sFilesPath . $sId . TEMP_FILE_NAME;
		@unlink($sTempFileName);
		if(is_uploaded_file($_FILES['Filedata']['tmp_name']))
			move_uploaded_file($_FILES['Filedata']['tmp_name'], $sTempFileName);
		break;

	case 'getTempFile':
		$sContents = parseXml($aXmlTemplates['result'], "msgErrorUpload", FAILED_VAL);
		$sFilename = $sId . TEMP_FILE_NAME;
		if(file_exists($sFilesPath . $sFilename))	$sContents = parseXml($aXmlTemplates['result'], $sFilename, SUCCESS_VAL);
		else 										$sContents = parseXml($aXmlTemplates['result'], "msgErrorUpload", FAILED_VAL);
		break;
	
	case 'initFile':
		$sContents = parseXml($aXmlTemplates['result'], "msgErrorUpload", FAILED_VAL);
		$sTempFileName = $sFilesPath . $sId . TEMP_FILE_NAME;
		if(!file_exists($sTempFileName)) break;
		$sConvert = isset($_REQUEST['convert']) ? $_REQUEST['convert'] : TRUE_VAL;
		$bJustRename = $sConvert != TRUE_VAL;
		if(!convert($sId, $bJustRename))
		{
			deleteTempFiles($sId);
			break;
		}
		$sAutoApprove = getSettingValue($sModule, "autoApprove");
		getResult("INSERT INTO `" . MODULE_DB_PREFIX . "Files`(`CategoryId`, `Title`, `Tags`, `Description`, `Date`, `Owner`, `Approved`) VALUES ('" . $sCategory . "', '" . $sTitle . "', '" . $sTags . "', '" . $sDesc . "', '" . time() . "', '" . $sId . "', '" . $sAutoApprove . "')");
		$sFileId = getLastInsertId();
		if(!renameFile($sId, $sFileId))
		{
			deleteTempFiles($sId);
			getResult("DELETE FROM `" . MODULE_DB_PREFIX . "Files` WHERE `ID`='" . $sFileId . "' LIMIT 1");
			break;
		}
		$sNewFileName = $sFileId . MP3_EXTENSION;
		deleteTempFiles($sId, true);
		parseTags($sFileId);
			
		if($sId != "0")
		{
			getResult("UPDATE `" . MODULE_DB_PREFIX . "PlayLists` SET `Order`=`Order`+1 WHERE `Owner` = '" . $sId . "'");
			getResult("INSERT INTO `" . MODULE_DB_PREFIX . "PlayLists` VALUES('" . $sFileId . "', '" . $sId . "', '1')");
		}
		$sContents = parseXml($aXmlTemplates['result'], "", SUCCESS_VAL);
		$sContents .= parseXml($aXmlTemplates['file'], $sFileId, $sNewFileName);
		break;

	case 'updateFile':
		getResult("UPDATE `" . MODULE_DB_PREFIX . "Files` SET `Title`='" . $sTitle . "', `Tags`='" . $sTags . "', `Description`='" . $sDesc . "' WHERE `ID`='" . $sFile . "' LIMIT 1");
		parseTags($sFile);
		break;

	/**
	* set user's uploaded file time
	*/
	case 'updateFileTime':
		getResult("UPDATE `" . MODULE_DB_PREFIX . "Files` SET `Time`='" . $sTime . "' WHERE `ID`='" . $sFile . "'");
		$sContents = parseXml($aXmlTemplates['result'], TRUE_VAL);
		break;
	
	/**
	* Delete files from playlist
	*/
	case 'deleteFromPlayList':
		$aFiles = explode(",", $sFile);
		$sQuery = "SELECT `ID` FROM `" . MODULE_DB_PREFIX . "Files` WHERE `Owner`='" . $sId . "' AND (0";
		$sQuery1 = "DELETE FROM `" . MODULE_DB_PREFIX . "PlayLists` WHERE `Owner`='" . $sId . "' AND (0";
		for($i=0; $i<count($aFiles); $i++)
		{
			$sQuery .= " OR `ID`=".$aFiles[$i];
			$sQuery1 .= " OR `FileId`=".$aFiles[$i];
		}
		$sQuery .= ")";
		$sQuery1 .= ")";
		$res = getResult($sQuery);
		getResult($sQuery1);
		$bResult = true;
		for($i=0; $i<mysql_num_rows($res); $i++)
		{
			$aFile = mysql_fetch_assoc($res);
			$bResult = deleteFile($aFile["ID"]);
		}
		$sContents = parseXml($aXmlTemplates['result'], $bResult ? TRUE_VAL : FALSE_VAL);
		break;

	/**
	* add files to playlist
	*/
	case 'addFilesToPlayList':
		$aFiles = explode(",", $sFile);
		$iFilesCount = count($aFiles);
		if($iFilesCount == 0)
		{
			$sContents = parseXml($aXmlTemplates['result'], FALSE_VAL);
			break;
		}
		getResult("UPDATE `" . MODULE_DB_PREFIX . "PlayLists` SET `Order`=`Order`+" . $iFilesCount . " WHERE `Owner` = '" . $sId . "'");
		for($i=0; $i<$iFilesCount; $i++)
			getResult("INSERT INTO `" . MODULE_DB_PREFIX . "PlayLists` VALUES('" . $aFiles[$i] . "', '" . $sId . "', '" . ($i+1) . "')");
		$sContents = parseXml($aXmlTemplates['result'], TRUE_VAL);
		break;
}
?>