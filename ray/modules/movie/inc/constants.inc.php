<?
/***************************************************************************
*
* IMPORTANT: This is a commercial product made by BoonEx Ltd. and cannot be modified for other than personal usage.
* This product cannot be redistributed for free or a fee without written permission from BoonEx Ltd.
* This notice may not be removed from the source code.
*
***************************************************************************/
	if(!defined("TEMP_FILE_NAME")) define("TEMP_FILE_NAME", "_temp");
	if(!defined("THUMB_FILE_NAME")) define("THUMB_FILE_NAME", "_small");
	if(!defined("SAVE_EXTENSION")) define("SAVE_EXTENSION", ".mpg");
	if(!defined("PLAY_EXTENSION")) define("PLAY_EXTENSION", ".flv");
	if(!defined("IMAGE_EXTENSION")) define("IMAGE_EXTENSION", ".jpg");
	if(!defined("VIDEO_WIDTH")) define("VIDEO_WIDTH", 350);
	if(!defined("VIDEO_HEIGHT")) define("VIDEO_HEIGHT", 250);

	$aInfo = array(
		'title' => "Video Player",
		'version' => "1.0.0000",
		'code' => "movie_1.0.0000_free",
		'author' => "Boonex",
		'authorUrl' => "http://www.boonex.com"
	);
	$aModules = array(
		'player' => array(
			'caption' => 'Video Player',
			'parameters' => array('file'),
			'js' => array(),
			'inline' => true,
			'vResizable' => false,
			'hResizable' => false,
			'reloadable' => true,
			'layout' => array('top' => 0, 'left' => 0, 'width' => 350, 'height' => 435),
									'minSize' => array('width' => 350, 'height' => 435),
			'div' => array(
				'id' => '',
				'name' => '',
				'style' => array()
			)
		),
		'editor' => array(
			'caption' => 'Video Uploader',
			'parameters' => array('id', 'password'),
			'js' => array(),
			'inline' => true,
			'vResizable' => false,
			'hResizable' => false,
			'reloadable' => true,
			'layout' => array('top' => 0, 'left' => 0, 'width' => 440, 'height' => 570),
									'minSize' => array('width' => 440, 'height' => 570),
			'div' => array(
				'id' => '',
				'name' => '',
				'style' => array()
			)
		)
	);
?>