<?php

$hostToCheck = @$_GET['host'];

if ($hostToCheck)
{
	header("Content-Type: text/plain");
	print_r(file_get_contents($hostToCheck));
	exit;
}
?>

<h3>Host checker</h3>

<?php
$hosts = array("www.yogahub.com", "yogahub.com", "myyogahub.com", "yogahub.com", "shopyogahub.com",
	"www.shopyogahub.com", "yogahub.org", "www.yogahub.org");

foreach($hosts as $host)
{
	echo "$host => " . gethostbyname($host) . "<br/>";
}