<?php



/***************************************************************************

*                            Dolphin Smart Community Builder

*                              -----------------

*     begin                : Mon Mar 23 2006

*     copyright            : (C) 2006 BoonEx Group

*     website              : http://www.boonex.com/

* This file is part of Dolphin - Smart Community Builder

*

* Dolphin is free software. This work is licensed under a Creative Commons Attribution 3.0 License. 

* http://creativecommons.org/licenses/by/3.0/

*

* Dolphin is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;

* without even the implied warranty of  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

* See the Creative Commons Attribution 3.0 License for more details. 

* You should have received a copy of the Creative Commons Attribution 3.0 License along with Dolphin, 

* see license.txt file; if not, write to marketing@boonex.com

***************************************************************************/



require_once( 'inc/header.inc.php' );

require_once( BX_DIRECTORY_PATH_INC . 'design.inc.php' );

require_once( BX_DIRECTORY_PATH_INC . 'profiles.inc.php' );

require_once( BX_DIRECTORY_PATH_INC . 'members.inc.php' );

require_once( BX_DIRECTORY_PATH_INC . 'utils.inc.php' );



// --------------- page variables and login







$logged['member'] = member_auth( 0 );



$_page['name_index'] 	= 19;

$_page['header'] 		= _t( "_COMPOSE_H" );

$_page['header_text'] 	= _t( "_COMPOSE_H1" );

$_page['css_name']		= 'compose.css';



//$_page['extra_js'] = $oTemplConfig -> sTinyMceEditorCompactJS;
//$_page['extra_js'] = $oTemplConfig -> sTinyMceEditorMiniJS;



// --------------- page components



$en_credits = getParam('en_credits') == 'on' ? 1 : 0;



$_ni = $_page['name_index'];

$_page_cont[$_ni]['page_main_code'] = DesignBoxContent( '', PageCompPageMainCode(), $oTemplConfig -> PageCompose_db_num );



// --------------- [END] page components



PageCode();



// --------------- page components functions



/**

* page code function

*/

function PageCompPageMainCode()
{
	global $site, $logged, $en_credits, $en_dest_choice;

	$ret = '';
	$en_inbox_notify 		= getParam("enable_inbox_notify");
	$en_dest_choice 		= getParam("enable_msg_dest_choice");
	$free_mode 				= getParam( "free_mode" );
	$membership_only 		= getParam( "membership_only" );
	$msg_credits 			= getParam( "msg_credits" );
	$member['ID'] 			= (int)$_COOKIE['memberID'];
	$member['Password'] 	= $_COOKIE['memberPassword'];
	if (isset($_REQUEST['global']) && $_REQUEST['global'] == 1)
		$recipientID = isset($_REQUEST['ID']) ? getMemberIDByGlobalID($_REQUEST['ID']) 
			: getMemberIDByGlobalID($_REQUEST['id']); // in referer after autologin
	else
		$recipientID = getID($_REQUEST['ID'], 0);
		
	$eventID				= $_REQUEST['eventID'];
	if ($eventID)
	{
		$eventData = db_arr("SELECT * FROM SDatingEvents WHERE ID = $eventID");
		
		// Check whether event exists
		if (!$eventData)
			return _t_err('Event Not Found');
	}		
		
	if ($eventID && !$logged['admin'])
	{
		// Check for event owner
		if ($member['ID'] != $eventData['ResponsibleID'])
			return _t_err('You do not have permissions to send broadcast for this event');
	}
		
	$recipient = getProfileInfo( $recipientID );
	//$recipient_query 		= "SELECT `ID`, `RealName`, `NickName`, `Email`, `EmailFlag`, `Status`  FROM `Profiles` WHERE `ID` = '$recipientID'";
	//$recipient				= db_arr( "$recipient_query" );
	$contact_allowed 		= contact_allowed($member['ID'], $recipientID);
	$sCharactersLeftC 		= _t('_characters_left');
	$max_message_size  		= getParam( "max_inbox_message_size" );

	// Check if credits could be used for message sending
	if ( $en_credits && !$free_mode && !$membership_only && $member['ID'] && !$contact_allowed )
		$could_use_credits = true;
	else
		$could_use_credits = false;

	// Check if member can send messages
	$check_res = checkAction( $member['ID'], ACTION_ID_SEND_MESSAGE );
	if ( $check_res[CHECK_ACTION_RESULT] != CHECK_ACTION_RESULT_ALLOWED
		&& !$could_use_credits && !$contact_allowed )
	{
		$ret = '
			<table width="100%" cellpadding="4" cellspacing="4" border="0">
				<tr>
					<td align="center">' . $check_res[CHECK_ACTION_MESSAGE] . '</td>
				</tr>
			</table>';
		return $ret;
	}

	// Set if credits should be used anyway
	$must_use_credits = ($could_use_credits && $check_res[CHECK_ACTION_RESULT] != CHECK_ACTION_RESULT_ALLOWED);

	//ob_start();
	$ret = '';
	if ( $_POST['action'] == "send" && strlen($_POST['text']) )
	{
		$action_result = "";

		// Check if recipient found
		if (!$recipient && !$eventID)
		{
			$ret = "
				<table width=\"100%\" cellpadding=\"4\" cellspacing=\"4\" align=\"center\">
					<tr>
						<td align=center>". _t("_COMPOSE_REJECT_MEMBER_NOT_FOUND") ."</td>
					</tr>
				</table>\n";
			return $ret;
		}

		if ($eventID)
		{
			// Get event participants
			$participants = fill_assoc_array(db_res(
				"SELECT * FROM SDatingParticipants 
				LEFT JOIN Profiles ON Profiles.ID = SDatingParticipants.IDMember
				WHERE IDEvent = $eventID"));
			if (count($participants) > 0)
				$recipient = $participants;
		} 
		
		// Perform sending
		if ($eventID && is_array($recipient))
		{
			$bad_send_result_code = false;
			$bad_send_result_count = 0;
			$ok_send_result_count = 0;
			foreach ($recipient as $r)
			{
				// Don't self message to the sender
				if ($r['IDMember'] == $member['ID']) continue;
				
				$send_result = MemberSendMessage( $member, $r, $must_use_credits, true );
				
				// Save unsuccessful send result but don't break sending to other recipients
				if ($send_result != 0)
				{
					$bad_send_result_code = $send_result;
					$bad_send_result_count++;
				}
				else
					$ok_send_result_count++;
			}
			if ($bad_send_result_count)
				$send_result = $bad_send_result_code;
		}
		else
			$send_result = MemberSendMessage( $member, $recipient, $must_use_credits );

		switch ( $send_result )
		{
			case 1:
				$action_result .= _t_err( "_FAILED_TO_SEND_MESSAGE" );
				$hide_form = '0';
				break;
			case 3:
				$action_result .= _t_err( "_You have to wait for PERIOD minutes before you can write another message!", 1 );
				$hide_form = '0';
				break;
			case 5:
				$action_result .= _t_err( "_FAILED_TO_SEND_MESSAGE_BLOCK" );
				$hide_form = '0';
				break;
			case 10:
				$action_result .= ($eventID) ? _t_err("Failed to send message to $bad_send_result_count member(s) as their status is set to inactive.<br/>Message has been successfully sent to $ok_send_result_count member(s).") 
					: _t_err( "_FAILED_TO_SEND_MESSAGE_NOT_ACTIVE" );
				$hide_form = '0';
				break;
			case 21:
				$action_result .= _t_err( "_FAILED_TO_SEND_MESSAGE_NO_CREDITS" );
				$hide_form = '0';
				break;
			default:
				$action_result .= ($eventID) ? _t_action("Message has been successfully sent to $ok_send_result_count member(s).") 
					: _t_action( "_MESSAGE_SENT" );
				$hide_form = '1';
				break;
		}
    }
    if (isset($_POST['eventID']))
    	$hide_form = '1';
	$ret .= '<div class="com_box"">';
	if ( strlen($action_result) )
		$ret .= $action_result;

	if ( $recipient &&  $hide_form != '1' )
	{
		$ret .= '<div class="clear_both"></div>';
		$ret .= ProfileDetails( $recipient['ID'] );
		$ret .= '<div class="clear_both"></div>';
	}

	if( '1' != $hide_form )
	{
		$sSubject = (isset($_REQUEST['subject'])) ? process_db_input($_REQUEST['subject']) : '';
		ob_start()

		?>

		<script type="text/javascript">
			<!--
			function changeDest(control)
			{
				if ( control.value == 'lovemail' )
				{
					z = document.getElementById("id0004");
					z.disabled = false;
				}
				else
				{
					xxx = document.getElementById("id0004");
					xxx.disabled = true;
				}
			}

			function checkForm()
			{
				var el;
				var hasErr = false;
				var fild = "";
				el = document.getElementById("inpSubj");
				
				if( el.value.length < 3 )
				{
					el.style.backgroundColor = "pink";
					el.style.border = "1px solid silver";
					hasErr = true;
					fild += " <?= _t('_Subject') ?>";
				}
				else
					el.style.backgroundColor = "#fff";

				if (hasErr)
				{
					alert("<?= /*_t('_please_fill_next_fields_first')*/
						'Subject must be at least 3 characters!' ?>");
					return false;
				}
				else
					return true;
					
				return false;
			}
			
			function checkForEmail(inputText)
			{
				var errorMsg = document.getElementById('composeErrorMsg');
				var composeButton = document.getElementById('composeSubmit');
				if (emailCheck(inputText))
				{
					errorMsg.style.display = 'block';
					composeButton.disabled = true;
				}
				else
				{
					errorMsg.style.display = 'none';
					composeButton.disabled = false;
				}
			}
			//-->
		</script>

		<form name="compose_form" method="post"
		  action="<?= $_SERVER['PHP_SELF'] . ( $recipient ? "?ID={$recipient['ID']}" : "" ) ?>" onsubmit="return checkForm();">
		 <?php if ($eventID) {?>
		 	<input type="hidden" name="eventID" value="<?=$eventID?>"/>
		 	<input type="hidden" name="eventURL" value="<?=$_SERVER['HTTP_REFERER']?>"/>
		 <?php }?> 
			<table class="composeTable">
				<tr style="vertical-align:top">
		<?
		if ( !$recipient )
		{
			?>
					<td class="form_label"><?= _t( "_SEND_MSG_TO" )?>
						<?php if (!$eventID) {?> <br/><span class="fieldComment">Enter a member username</span> <?php } ?>
					</td>
					<td class="form_value">
					<?php if ($eventID) {?>Participants of &quot;<?= $eventData['Title'] ?>&quot;<?php } 
						else { ?>
						<input class="inpMessageTo" type="text" name="ID" 
							onkeyup="checkForEmail(this.value)" style="float:left; margin-top:2px"/>
						<span id="composeErrorMsg">
							Messages are not permitted to be sent directly to an email address. 
							You must provide the valid username of another MyYogaHub.com community member. 
							Click <a href="contacts.php" target="_blank">here</a> to view your friends list.
						</span>
					<?php }?>
					</td>
				</tr>
				<tr>
			<?
		}
		?>
					<td class="form_label"><?= _t('_Subject') ?>:</td>
					<td class="form_value">
						<input class="inpSubj" id="inpSubj" name="mes_subject" type="text" value="<?= $sSubject; ?>" />
					</td>
				</tr>
				<tr>
					<td class="form_label"><?= _t( "_Message" ) ?>:</td>
					<td class="form_value">
					<textarea class="blogText" id="blogText" name="text"
						onKeyUp="return charCounter('blogText', <?= $max_message_size ?>, 'msg_counter')"></textarea>
					<div style="width:340px; text-align:right; margin-top:3px">
						<span id="msg_counter" name="msg_counter" style="font-weight:bold">
							<?= $max_message_size ?></span> <?= $sCharactersLeftC ?>
					</div>
					<!--
					<div id="compose_message" style="display:none;">
						<textarea class="comment_textarea" id="blogText" name="text"></textarea>
					</div>
					-->
					</td>
				</tr>
		<?

		if ( 'on' == $en_dest_choice )
		{
			if ( $_POST['notify'] == 'on' )
				$notify_sel = "checked";
			else
				$notify_sel = "";

			switch( $_POST['sendto'] )
			{
				case 'email':
					$email_sel = ' checked="checked" ';
					$lovemail_sel = "";
					$both_sel = "";
					break;
				case 'lovemail':
					$email_sel = "";
					$lovemail_sel = ' checked="checked"';
					$both_sel = "";
					break;
				default:
					$email_sel = "";
					$lovemail_sel = "";
					$both_sel = ' checked="checked"';
					break;
			}
			
			$javascript = ( $en_inbox_notify ? "" : "onClick=\"javascript: changeDest(this);\"" );
			$notify_dis = "";
			
			if ( !$lovemail_sel && $en_dest_choice )
			{
				$notify_sel = "";
				$notify_dis = ' disabled="disabled"';
			}
			?>
			<!-- 
				<tr>
					<td>&nbsp;</td>
					<td nowrap="nowrap">
						<input type="radio" id="id0001" name="sendto" value="email"  <?= $javascript . $email_sel ?> />
						<label for="id0001"><?= _t( "_Send to e-mail" )?></label>
					</td>
				</tr>
			-->
				<tr>
					<td>&nbsp;</td>
					<td>
						<input type="radio" id="id0002" name="sendto" value="lovemail" <?= $javascript . $lovemail_sel ?> />
						<label for="id0002"><?= _t( "_Send to communicator" ) ?></label>
						<input type="checkbox" id="id0004" name="notify" <?= $notify_sel  . $notify_dis ?> />
						<label for="id0004"><?=  _t( "_Notify by e-mail" ) ?></label>
					</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td nowrap="nowrap" colspan="3" align="left" style="padding-left:5px;">
						<input type="radio" id="id0003" name="sendto" value="both" <?= $javascript .  $both_sel ?> />
						<label for=id0003> <?= _t( "_both2" ) ?></label>
					</td>
				</tr>
			<?
		}
		else
		{
			?>
				<input type="hidden" name="sendto" value="lovemail" />
			<?
		}
		?>
				<tr>
					<td colspan="2" class="form_colspan">
						<input class="button" type="submit" id="composeSubmit" value="<?=_t( "_Send" ) ?>" />
					</td>
				</tr>
			</table>
			<input type="hidden" name="action" value="send" />
		</form>
		<?
		$ret .= ob_get_clean();
	}
	else
		//$ret .= '<div style="margin:15px 0px; text-align:center;">' . _t('_to_compose_new_message', $recipient['NickName'], $recipient['ID'], $site['url'] ) . '</div>';
		$ret .= '';

	$ret .= '</div>';
	if (isset($_POST['eventID']))
		$ret .= '<br/><center><a href="'.$_POST['eventURL'].'">Return to Event</a></center>';

	if ( 'on' == $en_dest_choice )
		$ret .= "<script>if (window.addEventListener) window.addEventListener('load', ShowPostCommentForm, false);
			else if (document.addEventListener) document.addEventListener('load', ShowPostCommentForm, false);
			else if (window.attachEvent) window.attachEvent('onload',ShowPostCommentForm);
			
			function ShowPostCommentForm()
			{
				if (document.getElementById('compose_message'))
					document.getElementById('compose_message').style.display = 'block';
			}
			</script>";	
	
	
	return $ret;
}

/**
 * Send message
 */

function MemberSendMessage( $member, $recipient, $must_use_credits = false, $isBroadcast = false )
{
	global $site, $en_credits, $en_dest_choice, $MySQL;

	$max_message_size  	= getParam( "max_inbox_message_size" );
	$max_messages  		= getParam( "max_inbox_messages" );
	$msg_credits 		= getParam( "msg_credits" );

	// Check if recipient is active
	if( 'Active' != $recipient['Status'] )
		return 10;

	if ($en_dest_choice != 'on')
		$_POST['sendto'] = 'both';
		
	if (!isset($_POST['sendto']))
		$_POST['sendto'] = 'lovemail';

		
	// Check if member is blocked
	/*
	$checkBlockQuery = "
		SELECT
				`sourceID`,
				`targetID`
		FROM
				`ProfilesRelations`
		WHERE
				`sourceID` = '{$member['ID']}'
		AND
				`targetID` = '{$recipient['ID']}'
		AND
				`action` = 'block'
		LIMIT 1;
	";
*/
	if (!$isBroadcast && db_arr( 
			"SELECT `ID`, `Profile` FROM `BlockList` 
			WHERE `Profile` = {$member['ID']} AND `ID` = '{$recipient['ID']}';"))
		return 5;

	// If must use credits then check for enough amount
	if ( $must_use_credits && getProfileCredits( $member['ID'] ) < (float)$msg_credits )
		return 21;

	// Antispam ))
	if (!$isBroadcast && db_arr(
			"SELECT `ID` FROM `Messages` 
			WHERE `Sender` = {$member[ID]} AND date_add(`Date`, INTERVAL 1 MINUTE) > Now()"))
		return 3;

	// Get sender info
	$sender = getProfileInfo( $member['ID'] ); //db_arr( "SELECT `NickName` FROM `Profiles` WHERE `ID` = '{$member['ID']}';" );
	$senderNick = $sender ? ucwords($sender['NickName']) : _t("_Visitor");
	$senderProfileLink = getProfileLink($member['ID']);
	
	$aPlus = array();
	$aPlus['ProfileReference'] = $sender ? 
		"<a href=\"$senderProfileLink\">$senderNick</a> <a href=\"$senderProfileLink\">($senderProfileLink)</a>" 
		: '<strong>'. _t("_Visitor") .'</strong>';
	$aPlus['isComposeMessage'] = 1;

	// Don't send notification if message is sending to email
	if ( $_POST['notify'] && !($_POST['sendto'] == "email" || $_POST['sendto'] == "both") )
	{
		$message_text = getParam("t_Compose");
		$subject = getParam('t_Compose_subject');
		$aPlus['senderNickName'] = $sender ? $sender['NickName'] : _t("_Visitor");		
		$notify_res = sendMail( $recipient['Email'], $subject, $message_text, $recipient['ID'], $aPlus);
		if ( !$notify_res )
			echo "<div class=\"err\">". _t("_Notification send failed") ."</div><br />\n";
	}

	// Send message to communicator
	if ( $_POST['sendto'] == "lovemail" || $_POST['sendto'] == "both" )
	{
		// Restrict with total messages count
		$messages_count = db_arr( "SELECT COUNT(*) AS `mess_count` FROM `Messages` WHERE `Recipient` = '{$recipient['ID']}'" );
		$messages_count = $messages_count['mess_count'];
		if ( ($messages_count - 1) > $max_messages )
		{
			$del_res = db_res( "SELECT `ID` FROM `Messages` WHERE `Recipient` = '{$recipient['ID']}' ORDER BY `Date` ASC LIMIT ". ($messages_count - $max_messages + 1) );
			while ( $del_arr = mysql_fetch_array($del_res) )
				db_res( "DELETE FROM `Messages` WHERE `ID` = {$del_arr['ID']}" );
		}

		// Insert message into database
		$message_text = process_db_input($_POST['text']);
		$message_subject = strmaxwordlen(process_db_input($_POST['mes_subject']), 30);
		$result = db_res( "INSERT INTO `Messages` ( `Date`, `Sender`, `Recipient`, `Text`, `Subject`, `New` ) VALUES ( NOW(), {$member['ID']}, {$recipient['ID']}, '$message_text', '$message_subject', '1' )" );
		$insertedMessageID = mysql_insert_id($MySQL->link);
		
		if ($result && defined('updateInboxInMemcache') && updateInboxInMemcache)
		{
			$recipientGlobalID = db_value("SELECT `globalid` FROM `Profiles` WHERE `ID` = {$recipient['ID']}");
			if ($recipientGlobalID)
			{
				// Increase inbox number in memcache		
				require_once(BX_DIRECTORY_PATH_CLASSES . 'class.yhInteractor.php');
				$yhInteractor = new yhInteractor();			
				$yhInteractor->setInboxNumber($recipientGlobalID, getNewLettersNum($recipient['ID']));
			}
		}
	}
	
	// Send message to email
	if ($_POST['sendto'] == "email" || $_POST['sendto'] == "both")
	{
		$message_text = getParam("t_Message");
		$subject = "[YH] $senderNick sent you a message on YogaHub";
		$messageLink = "{$site['url']}messages_inbox.php?message=$insertedMessageID";
		$proflieEditLink = "{$site['url']}profile_edit.php?ID={$recipient['ID']}";
		$aPlus['InnerSubject'] = $_POST['mes_subject'];
		$aPlus['MessageText'] = str_replace("\n", "<br/>", strmaxtextlen($_POST['text'], 100));
		$aPlus['MessageLink'] = "<a href='$messageLink'>$messageLink</a>";
		$aPlus['ProfileEditLink'] = "<a href='$proflieEditLink'>$proflieEditLink</a>";
		
		$result = sendMail( $recipient['Email'], $subject, $message_text, $recipient['ID'], $aPlus );
	}	

	// If sending successful then mark as performed action
	if ( $result )
	{
		checkAction( $member['ID'], ACTION_ID_SEND_MESSAGE, true );
		if ( $must_use_credits )
			decProfileCredits( $member['ID'], $msg_credits );
	}
	else
		return 1;

	return 0;
}

?>