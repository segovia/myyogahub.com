<?php

$K = 'asd;lfoweir2324sdfsdf';

if (!isset($_POST['globalid']) ||
	!isset($_POST['firstname']) || $_POST['firstname'] == '' ||
	!isset($_POST['lastname']) || $_POST['lastname'] == '' ||
	!isset($_POST['email']) || $_POST['email'] == '' ||
	!isset($_POST['username']) || $_POST['username'] == '' ||
	!isset($_POST['password']) || $_POST['password'] == '' ||
	!isset($_POST['passPhrase']) || $_POST['passPhrase'] == '') exit;

function getStrippedPostValue($postName)
{
	return (get_magic_quotes_gpc()) ? stripslashes($_POST[$postName]) : $_POST[$postName];
}	

function valueForDb($value)
{
	if (function_exists('mysql_real_escape_string'))
		return mysql_real_escape_string($value);
	elseif (function_exists('mysql_escape_string'))
		return mysql_escape_string($value);
	else
		return addslashes($value);
}

// Strip post values to use in passPhrase
$firstName = getStrippedPostValue('firstname');
$lastName = getStrippedPostValue('lastname');
$email = getStrippedPostValue('email');
$userName = getStrippedPostValue('username');
$password = getStrippedPostValue('password');	
	
$passPhrase = md5($_POST['globalid'] . $firstName . $lastName . $email . $K . strtoupper($userName) . $password);
if ($passPhrase != $_POST['passPhrase']) exit;

require_once('../includes/config.php');

$conn = mysql_connect("{$config['MasterServer']['servername']}:{$config['MasterServer']['port']}", 
	$config['MasterServer']['username'], $config['MasterServer']['password']);
if ($conn === false) exit;

// Prepare values to insert in database
$firstName = valueForDb($firstName);
$lastName = valueForDb($lastName);
$email = valueForDb($email);
$userName = valueForDb($userName);
$password = md5(valueForDb($password));
$country = valueForDb($_POST['billadd_country']);

// Fill default values for other db fields
$usergroupid = 2;
$showvbcode = 1;
$showbirthday = 0;
$usertitle = 'Junior Member';
$reputationlevelid = 5;
$languageid = 1;
$timezoneoffset = -5;
$now = time();

// Generates a new user salt string
define('SALT_LENGTH', 3);
$salt = '';
for ($i = 0; $i < SALT_LENGTH; $i++)
	$salt .= chr(rand(33, 126));

// Hash the md5'd password with the salt
$password = md5($password . $salt);

$query = "INSERT INTO {$config['Database']['tableprefix']}user 
	(usergroupid, globalid, username, password, passworddate, email, showvbcode, 
	showbirthday, usertitle, joindate, reputationlevelid, timezoneoffset, languageid, salt)
	VALUES ($usergroupid, {$_POST['globalid']}, '$userName', '$password', 
		FROM_UNIXTIME($now), '$email', $showvbcode, $showbirthday, '$usertitle',
		$now, $reputationlevelid, $timezoneoffset, $languageid, '$salt')";
$result = mysql_db_query($config['Database']['dbname'], $query, $conn);		

if (mysql_affected_rows() > 0) 
{
	// Get userid by globalid
	$query = "SELECT userid FROM {$config['Database']['tableprefix']}user
		WHERE globalid={$_POST['globalid']}";
	$result = mysql_db_query($config['Database']['dbname'], $query, $conn);	
	$useridArr = mysql_fetch_array($result);
	$userid = $useridArr['userid'];	
	
	$query = "INSERT INTO {$config['Database']['tableprefix']}userfield 
		(userid, field2) VALUES ($userid, '$country')";
	$result = mysql_db_query($config['Database']['dbname'], $query, $conn);		
	
	if (mysql_affected_rows() > 0) 
		echo 'ok';
}

?>