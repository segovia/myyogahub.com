<?php

class yhInteractor
{
	private $K = 'asd;lfoweir2324sdfsdf';
	private $cacheInboxUnreadLifetime = 10800; // 3 hours
	private $cacheInboxUnreadPrefix = 'myh-inbox-';
	private $cacheRequestsLifetime = 10800; // 3 hours
	private $cacheRequestsPrefix = 'myh-requests-';
	private $cacheSmallAvatarLifetime = 10800; // 3 hours
	private $cacheSmallAvatarPrefix = 'myh-smallavatar-';
	private $cacheMiddleAvatarLifetime = 10800; // 3 hours
	private $cacheMiddleAvatarPrefix = 'myh-middleavatar-';
	private $cacheFeaturedMembersLifetime = 1800; // 30 min
	private $cacheFeaturedMembersName = 'myh-random-featuredmembers';
	private $cacheFeaturedEventsLifetime = 43200; // 12 hours
	private $cacheFeaturedEventsName = 'myh-featuredevents';	
	private $cacheBlogsLifetime = 1800; // 30 min
	private $cacheBlogsName = 'yh-blogs';			
	
	function getInboxUnread($globalid)
	{
		global $su_config, $memcacher;

		// Using Memcache to load Inbox from MYH
		$memcache = $memcacher->obj();
	
		// Record in Memcache for this globalid
		$cachedInboxNumber = $memcache->get($this->cacheInboxUnreadPrefix . $globalid);

		if (is_numeric($cachedInboxNumber))
			return $cachedInboxNumber;
		
		$getInboxScriptPath = "{$su_config['main_host']}myyogahub.com/profiles/get-unread-inbox.php";
		$hash = md5(strtoupper($this->K) . $globalid . $this->K);
		$inboxNumber = file_get_contents("$getInboxScriptPath?globalid=$globalid&hash=$hash");
		
		// Save inbox number to Memcache
		$memcache->set($this->cacheInboxUnreadPrefix . $globalid, $inboxNumber,	0, $this->cacheInboxUnreadLifetime);
		
		return (int)$inboxNumber;
	}
	
	function getRequests($globalid)
	{
		global $su_config, $memcacher;

		$memcache = $memcacher->obj();
		$cachedRequestsNumber = $memcache->get($this->cacheRequestsPrefix . $globalid);
		if (is_numeric($cachedRequestsNumber))
			return $cachedRequestsNumber;

		$getRequestsScriptPath = "{$su_config['main_host']}myyogahub.com/profiles/get-requests.php";
		$hash = md5(strtoupper($this->K) . $globalid . $this->K);
		$requestsNumber = file_get_contents("$getRequestsScriptPath?globalid=$globalid&hash=$hash");
		
		$memcache->set($this->cacheRequestsPrefix . $globalid, $requestsNumber,	0, $this->cacheRequestsLifetime);
		
		return (int)$requestsNumber;
	}
	
	function getSmallAvatar($globalid, $useCache = true)
	{
		global $su_config, $memcacher;
		
		$memcache = $memcacher->obj();
		if ($useCache)
		{
			$cachedSmallAvatar = $memcache->get($this->cacheSmallAvatarPrefix . $globalid);	
			if ($cachedSmallAvatar)
				return $cachedSmallAvatar;
		}

		$getSmallAvatarScriptPath = "{$su_config['main_host']}myyogahub.com/profiles/get-small-avatar.php";		
		$hash = md5(strtoupper($this->K) . $globalid . $this->K);
		$smallAvatar = file_get_contents("$getSmallAvatarScriptPath?globalid=$globalid&hash=$hash");
		
		$memcache->set($this->cacheSmallAvatarPrefix . $globalid, $smallAvatar,	0, $this->cacheSmallAvatarLifetime);
		
		return $smallAvatar;
	}

	function getMiddleAvatar($globalid, $useCache = true)
	{
		global $su_config, $memcacher;

		$memcache = $memcacher->obj();
		if ($useCache)
		{
			$cachedMiddleAvatar = $memcache->get($this->cacheMiddleAvatarPrefix . $globalid);	
			if ($cachedMiddleAvatar)
				return $cachedMiddleAvatar;
		}

		$getMiddleAvatarScriptPath = "{$su_config['main_host']}myyogahub.com/profiles/get-middle-avatar.php";		
		$hash = md5(strtoupper($this->K) . $globalid . $this->K);
		$middleAvatar = file_get_contents("$getMiddleAvatarScriptPath?globalid=$globalid&hash=$hash");
		
		$memcache->set($this->cacheMiddleAvatarPrefix . $globalid, $middleAvatar, 0, $this->cacheMiddleAvatarLifetime);
		
		return $middleAvatar;
	}
	
	function getFeaturedMembers($useCache = true)
	{
		global $su_config, $memcacher;

		$memcache = $memcacher->obj();
		if ($useCache)
		{
			$cachedFeaturedMembers = $memcache->get($this->cacheFeaturedMembersName);	
			if ($cachedFeaturedMembers)
				return $cachedFeaturedMembers;
		}

		$getFeaturedMembersScriptPath = "{$su_config['main_host']}myyogahub.com/profiles/get-featured-members.php";		
		$hash = md5(strtoupper($this->K) . $this->K);
		$featuredMembers = file_get_contents("$getFeaturedMembersScriptPath?hash=$hash");
		
		$memcache->set($this->cacheFeaturedMembersName, $featuredMembers, 0, $this->cacheFeaturedMembersLifetime);
		
		return $featuredMembers;		
	}
	
	function getFeaturedEvents($useCache = true)
	{
		global $su_config, $memcacher;
		
		$maxEventsToSave = 4;
		
		$memcache = $memcacher->obj();
		if ($useCache)
		{
			$cachedFeaturedEvents = $memcache->get($this->cacheFeaturedEventsName);	
			if ($cachedFeaturedEvents)
				return $cachedFeaturedEvents;
		}
		
		$featuredEvents = array();
		$featuredEventsFeedUrl = "{$su_config['main_host']}myyogahub.com/eventfeeds.php?type=rss&events=featured";		
			
		// Parse featured events feed into array
		include_once('../simplepie/simplepie.inc');
		include_once('../simplepie/idn/idna_convert.class.php');	
		
		$feed = new SimplePie();
		$feed->set_feed_url($featuredEventsFeedUrl);
		$feed->enable_cache(false);	
		$feed->init();		
		$feed->handle_content_type();
		$items = $feed->get_items();
		
		$i = 0;
		foreach($items as $item)
		{	
			$featuredEvent = array();
			$featuredEvent['title'] = $item->get_title();
			$featuredEvent['link'] = $item->get_link();
			$exclusiveEvent = ($item->data['child']['']['comments'][0]['data'] == 'Exclusive');
			$imgLink = $item->get_enclosure();
			if ($imgLink && (!array_key_exists('eventImage', $featuredEvents) || $exclusiveEvent))
			{
				$featuredEvents['eventImage'] = $imgLink->get_link();
				$featuredEvents['eventImageLink'] = $featuredEvent['link'];
			}
			$featuredEvents[] = $featuredEvent;
			$i++;
			if ($i == $maxEventsToSave) break;
		}
		$featuredEvents = serialize($featuredEvents);
	
		$memcache->set($this->cacheFeaturedEventsName, $featuredEvents, 0, $this->cacheFeaturedEventsLifetime);
		
		return $featuredEvents;		
	}
	
	function getBlogs($useCache = true)
	{
		global $su_config, $memcacher;
	
		$memcache = $memcacher->obj();
		if ($useCache)
		{
			$cachedBlogs = $memcache->get($this->cacheBlogsName);	
			if ($cachedBlogs)
				return $cachedBlogs;
		}
		
		$getBlogsScriptPath = "{$su_config['main_host']}yogahub.com/get_blogs.php";	
		$hash = md5(strtoupper($this->K) . $this->K);
		$blogs = file_get_contents("$getBlogsScriptPath?hash=$hash");		
/*
		$maxBlogsToSave = 4;
		$blogs = array();
		$blogsFeedUrl = "http://feeds.feedburner.com/Yogahub?type=rss";	// TODO: admin setting	
			
		// Parse blogs feed into array
		include_once('../simplepie/simplepie.inc');
		include_once('../simplepie/idn/idna_convert.class.php');	
		
		$feed = new SimplePie();
		$feed->set_feed_url($blogsFeedUrl);
		$feed->enable_cache(false);	
		$feed->init();		
		$feed->handle_content_type();
		$items = $feed->get_items();
		
		$i = 0;
		foreach($items as $item)
		{
			$blog = array();
			$blog['title'] = $item->get_title();
			$blog['link'] = $item->get_link();
			$imgLink = $item->get_enclosure();
			if (!array_key_exists('blogImage', $blogs))
			{
				if ($imgLink)
				{
					$blogs['blogImage'] = $imgLink->get_link();
					$blogs['blogImageLink'] = $blog['link'];
				}
				else
				{
					// Try to get blog image from blog content
					$blog['content'] = $item->get_content();
					$imgLinkMatches = preg_match('/<img[^>]*src="(.*)"/i', $blog['content'], $matches);
					if (count($matches) > 1)
					{
						$blogs['blogImage'] = $matches[1];
						$blogs['blogImageLink'] = $blog['link'];
					}
				}
			}
			$blogs[] = $blog;
			$i++;
			if ($i == $maxBlogsToSave) break;
		}
		$blogs = serialize($blogs);
*/	
		$memcache->set($this->cacheBlogsName, $blogs, 0, $this->cacheBlogsLifetime);
		
		return $blogs;		
	}
}

?>