<?php

/**
 * Whether this is a XMLRPC Request
 *
 * @var bool
 */
define('XMLRPC_REQUEST', true);
define('RPCAPIKEY', 'asd;lfoweir2324sdfsdf');
define('RPCAPI_DEBUG', false);

class RpcApi
{
	public function __construct()
	{
		global $vbulletin, $db;
		
		$this->function = trim(@$_REQUEST['api_function']);
		$this->params = trim(@$_REQUEST['api_params']);	
		if (!RPCAPI_DEBUG)
			$this->hash = trim(@$_REQUEST['api_hash']);

		// All parameters must be specified
		if (!$this->function)
			$this->finish('API Function not specified');
		if (!$this->params)
			$this->finish('API Parameters not specified');
			
		if (!RPCAPI_DEBUG && !$this->hash)
			$this->finish('API Hash not specified');
			
		// Validate call hash
		if (!RPCAPI_DEBUG)
		{
			$expectedHash = md5($this->function . $this->params . RPCAPIKEY);
			if ($expectedHash !== $this->hash)
				$this->finish('Wrong API Call Hash value');
		}
			
		// Decode params
		$this->params = unserialize($this->params);
		if (empty($this->params) || !array($this->params))
			$this->finish("Can't parse params");
			
		// Check if method exists
		if (!method_exists($this, $this->function)) 
			$this->finish("Requested function does not exist");
		$this->function = array($this, $this->function);
		if (!is_callable($this->function))
			$this->finish("Requested function does not exist");
			
		// Connect and assign forum database
		define('VB_AREA', 'Forum');
		define('THIS_SCRIPT', 'index');
		define('CWD', '..');
		require_once('../includes/init.php');
		
		if (!$db)
			$this->finish("Can not connect to forum database");
		$this->forumDb = $db;	
		
		$this->vbulletin = $vbulletin;		
	}
	
	private $function = '';
	private $params = '';
	private $hash = '';
	
	private $forumDb = null;
	private $vbulletin = null;
	
	protected function finish($message = 'OK', $data = false)
	{
		$result = array('message' => $message, 'data' => $data);
		header('Content-type: text/plain');
		header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
		echo json_encode($result);
		exit;
	}
	
	protected function success($data)
	{
		$this->finish('OK', $data);
	}
	
	public function serveRequest()
	{
		call_user_func($this->function);
		
		// If function have not used finish() method, we can do it
		$this->finish(); 
	}
	
	/*** FORUM HELPERS ***/
	
	// Returns forum user's data by globalid	
	protected function forumUserDataByGlobalId($globalid)
	{
		$forumUserData = $this->forumDb->query_first("
			SELECT * FROM " . TABLE_PREFIX . "user WHERE globalid = $globalid");
		if (!$forumUserData)
			$this->finish("User with globalid = $globalid not found on forum");
			
		return $forumUserData;
	}
	
	/*** REMOTE FUNCTIONS ***/
	
	public function isalive()
	{
		$this->finish();
	}

	/**
	 * Adds new post to new thread (if $type='thread') 
	 * or to the specified thread (if $type='reply')
	 * @param $type string Type of the post: 'thread' or 'reply'
	 * @return void
	 */
	private function do_post($type = 'thread')
	{
		global $vbulletin, $forumperms, $antiFloodOff;
		
		// Get forum userid by globalid	
		if (!isset($this->params['globalid']))
			$this->finish('globalid not specified');
		$globalid = (int)trim($this->params['globalid']);
		$forumUserData = $this->forumUserDataByGlobalId($globalid);
		$forumUserId = $forumUserData['userid'];
		
		// Log in specified user on forum
		$vbulletin = $this->vbulletin;
		require_once('../includes/functions_login.php');
		if (!verify_authentication_by_globalid($globalid))
			$this->finish('Can not log in specified user on forum');
		
		// Prepare params to post
		$now = time();
		$postHash = md5($now . $vbulletin->userinfo['userid'] . $vbulletin->userinfo['salt']);
		if ($type == 'thread')
		{
			$threadinfo = array();
			$forumid = $this->params['forumid'];
			$post = array(
				'f' => $this->params['forumid'],
				'do' => 'postthread'
			);
		}
		else // type is 'reply'
		{
			$threadid = $this->params['threadid'];
			$threadinfo = fetch_threadinfo($threadid);
			if (empty($threadinfo) || !isset($threadinfo['forumid']))
				$this->finish("Thread with id = $threadid not found");
			$threadinfo['visible'] = !$this->params['needsModeration'];
			$forumid = $threadinfo['forumid'];
			$post = array(
				'f' => $forumid,
				'threadid' => $threadid,
				'do' => 'postreply'
			);			
		}
		$post = array_merge($post, array(
			'loggedinuser' => $forumUserId,
			'title' => $this->params['title'],
			'message' => $this->params['message'],
			'posthash' => $postHash,
			'wysiwyg' => 0,
			'taglist' => '',
			's' => '',
			'poststarttime' => $now));

		require_once('../includes/functions_newpost.php');
		$errors = array();
		$foruminfo = fetch_foruminfo($forumid);
		$forumperms = ($this->params['needsModeration']) ? 0 : 131072;
		$antiFloodOff = true;
		
		build_new_post($type, $foruminfo, $threadinfo, array(), $post, $errors);
	}
	
	public function postNewThread()
	{
		$this->do_post('thread');
	}
	
	public function postToThread()
	{
		$this->do_post('reply');
	}
}

$rpcApi = new RpcApi();
$rpcApi->serveRequest();