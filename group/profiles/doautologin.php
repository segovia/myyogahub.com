<?php

function goToMainPage()
{
	header("Location: ../index.php?yhf_rd=1");
	exit;
}

if (!isset($_GET['loginkey']) || !isset($_GET['returnurl'])) 
	goToMainPage();

// Get globalid
require_once('superuserconfig.php');

$globalid = file_get_contents(
	"{$su_config['url']}index.php/authmanager/getuserid_by_loginkey/{$_GET['loginkey']}");
if (!intval($globalid))
	goToMainPage();

require_once('../includes/config.php');	
	
// Check whether user with globalid exists on forum
$conn = mysql_connect("{$config['MasterServer']['servername']}:{$config['MasterServer']['port']}", 
	$config['MasterServer']['username'], $config['MasterServer']['password']);
if ($conn === false) 
	goToMainPage();

// Get userid by globalid
$query = "SELECT userid FROM {$config['Database']['tableprefix']}user
	WHERE globalid = $globalid";
$result = mysql_db_query($config['Database']['dbname'], $query, $conn);	
$userid = mysql_result($result, 'userid');
if (!intval($userid))
	goToMainPage();
	
// Do login
$DOING_AUTOLOGIN = true;
include('../login.php');
	
header("Location: {$_GET['returnurl']}");

?>