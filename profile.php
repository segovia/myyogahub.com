<?

/***************************************************************************
*                            Dolphin Smart Community Builder
*                              -----------------
*     begin                : Mon Mar 23 2006
*     copyright            : (C) 2006 BoonEx Group
*     website              : http://www.boonex.com/
* This file is part of Dolphin - Smart Community Builder
*
* Dolphin is free software. This work is licensed under a Creative Commons Attribution 3.0 License. 
* http://creativecommons.org/licenses/by/3.0/
*
* Dolphin is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
* without even the implied warranty of  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the Creative Commons Attribution 3.0 License for more details. 
* You should have received a copy of the Creative Commons Attribution 3.0 License along with Dolphin, 
* see license.txt file; if not, write to marketing@boonex.com
***************************************************************************/

if (!session_id()) session_start();

require_once( 'inc/header.inc.php' );
require_once( BX_DIRECTORY_PATH_INC . 'design.inc.php' );
require_once( BX_DIRECTORY_PATH_INC . 'profiles.inc.php' );
require_once( BX_DIRECTORY_PATH_INC . 'members.inc.php' );
require_once( BX_DIRECTORY_PATH_INC . 'profile_disp.inc.php' );
require_once( BX_DIRECTORY_PATH_INC . 'utils.inc.php' );
require_once( BX_DIRECTORY_PATH_ROOT . "templates/tmpl_{$tmpl}/scripts/BxTemplProfileView.php" );

$_page['name_index']	= 7;
$_page['css_name']		= 'profile_view.css';

if ( !( $logged['admin'] = member_auth( 1, false ) ) )
	if ( !( $logged['member'] = member_auth( 0, false ) ) )
		if ( !( $logged['aff'] = member_auth( 2, false )) )
			$logged['moderator'] = member_auth( 3, false );

$profileID = getID( $_REQUEST['ID'] );

if( $logged['member'] )
	$memberID = (int)$_COOKIE['memberID'];
else
	$memberID = 0;

// If ID is not specified then show random 10 profiles
if ( !$profileID )
{
	$_page['header'] = "{$site['title']} ". _t("_Member Profile");
	$_page['header_text'] = _t("_View profile");
	$_page['name_index'] = 0;
	$_page_cont[0]['page_main_code'] = MsgBox( _t( '_Nothing' ) );
	PageCode();
	exit;
}

// Check if member can view profile
$contact_allowed = contact_allowed($memberID, $profileID);
$check_res = checkAction( $memberID, ACTION_ID_VIEW_PROFILES, true );
$status = get_user_status($memberID);

if ($check_res[CHECK_ACTION_RESULT] != CHECK_ACTION_RESULT_ALLOWED && $status != 'Suspended'
	&& !$logged['admin'] && !$logged['moderator'] && $memberID != $profileID && !$contact_allowed )
{
	$_page['header'] = "{$site['title']} "._t("_Member Profile");
	$_page['header_text'] = "{$site['title']} "._t("_Member Profile");
	$_page['name_index'] = 0;
	$_page_cont[0]['page_main_code'] = '<center>'. $check_res[CHECK_ACTION_MESSAGE] .'</center><br />';
	PageCode();
	exit;
}

if ($memberID && $memberID == $profileID)
	checkCommunityPermissions($memberID);

$oProfile = new BxTemplProfileView( $profileID );

$_page['extra_js'] 	=
	$oTemplConfig -> sTinyMceEditorProfileJS .
	$oProfile->oVotingView->getExtraJs() .
	'<script type="text/javascript">urlIconLoading = "'.getTemplateIcon('loading.gif').'";</script>
	<script src="inc/js/dynamic_core.js.php"></script>';

$_page['extra_css'] = $oProfile->genProfileCSS($profileID);
$p_arr              = $oProfile -> _aProfile;

if ( !($p_arr['ID'] && ($logged['admin'] || $logged['moderator'] || $oProfile -> owner || $p_arr['Status'] = 'Active') ) )
{
	$_page['header'] = "{$site['title']} ". _t("_Member Profile");
	$_page['header_text'] = "{$site['title']} ". _t("_Member Profile");
	$_page['name_index'] = 0;
	$_page_cont[0]['page_main_code'] = _t_err("_Profile NA");
	PageCode();
	exit;
}

//Ajax loaders

if( $_GET['show_only'] )
{
	switch( $_GET['show_only'] )
	{
		case 'shareMusic':
			$sCaption = db_value( "SELECT `Caption` FROM `ProfileCompose` WHERE `Func` = 'ShareMusic'" );
			echo PageCompShareMusicContent( $sCaption, $profileID );
		break;
		case 'sharePhotos':
			$sCaption = db_value( "SELECT `Caption` FROM `ProfileCompose` WHERE `Func` = 'SharePhotos'" );
			echo PageCompSharePhotosContent($sCaption, $profileID);
		break;
		case 'shareVideos':
			$sCaption = db_value( "SELECT `Caption` FROM `ProfileCompose` WHERE `Func` = 'ShareVideos'" );
			echo PageCompShareVideosContent($sCaption, $profileID);
		break;
		case 'friends':
			$sCaption = db_value( "SELECT `Caption` FROM `ProfileCompose` WHERE `Func` = 'Friends'" );
			echo PageCompFriendsContent($sCaption, $profileID);
		break;
	}
	
	exit;
}




$_page['header']      = htmlspecialchars_adv( $p_arr['Headline'] ) . " - " . process_line_output( $p_arr['NickName'] );
//$_page['header_text'] = process_line_output( $p_arr['Headline'] );

//post comment
if( $_POST['commentsubmit'] )
	$ret .= addComment($profileID);

//delete comment
if( $_GET['action'] == 'commentdelete' )
	$ret .= deleteComment( (int)$_GET['commentID'] );

// track profile views
if ( $track_profile_view && $memberID && !$oProfile -> owner )
{
    db_res( "DELETE FROM `ProfilesTrack` WHERE `Member` = {$memberID} AND `Profile` = $profileID", 0);
    db_res( "INSERT INTO `ProfilesTrack` SET `Arrived` = NOW(), `Member` = {$memberID}, `Profile` = $profileID", 0);
}

$_ni = $_page['name_index'];
$_page_cont[$_ni]['gui_message'] = guiMessage();
$_page_cont[$_ni]['page_main_code'] = $oProfile -> genColumns();

$userProfileSQL = "
	SELECT `Headline`, `DescriptionMe`, `Tags`
	FROM `Profiles`
	WHERE `ID` = {$memberID}";
$userProfileArr = db_arr( $userProfileSQL );

// create meta description from user's headline and description
$fullDesc = $userProfileArr['Headline'] . ' ' . $userProfileArr['DescriptionMe'];
$wordsCount = getParam('profileDescMaxLength');
preg_match_all("/\w+/", $fullDesc, $matches, PREG_OFFSET_CAPTURE);
if (count($matches[0]) > $wordsCount)
{
	$lastWordOffset = $matches[0][$wordsCount][1];
	$meta_description = substr($fullDesc, 0, $lastWordOffset);
}
else 
	$meta_description = $fullDesc;

// create keywords from user's tags and global tags
$maxUserTags = getParam('MaxUserTagsInKeywords');
$userTagsArr = explode(',', $userProfileArr['Tags'], $maxUserTags + 1);
$userTags = '';
for ($i = 0; $i < $maxUserTags; $i++)
{
	$tag = trim($userTagsArr[$i]);
	if ($tag == '') continue;
	
	if (strlen($userTags) > 0) $userTags .= ', ';
	$userTags .= $tag;
}
	
$maxGlobalTags = getParam('MaxGlobalTagsInKeywords');
$globalTagsArr = db_arr( "SELECT `VALUE` FROM `GlParams` WHERE `Name`='MetaKeyWords' " );
$globalTagsArr = explode(',', $globalTagsArr['VALUE'], $maxGlobalTags + 1);
$globalTags = '';
for ($i = 0; $i < $maxGlobalTags; $i++)
{
	$tag = trim($globalTagsArr[$i]);
	if ($tag == '') continue;
	
	if (strlen($globalTags) > 0) $globalTags .= ', ';
	$globalTags .= $tag;
}

$meta_keywords = $userTags . ', ' . $globalTags;
	
PageCode();



function addComment( $profileID )
{
	global $logged;
	global $oProfile;
	
	if( $logged['member'] )
		$record_sender = (int)$_COOKIE['memberID'];
	else
		return;
	
	$period = 1; // time period before user can add another record (in minutes)
	$record_maxlength = 1600; // max length of record
	
	// Test if IP is defined
	$ip = getVisitorIP();
	if( $ip == '0.0.0.0' )
		return _t_err("_sorry, i can not define you ip adress. IT'S TIME TO COME OUT !");
	
	// get record text
	$record_text = addslashes( clear_xss( trim( process_pass_data( $_POST['commenttext']))));
	if( strlen($record_text) < 2 )
		return _t_err("_enter_message_text");
	
	// Test if last message is old enough
	$last_count = db_value( "SELECT COUNT(*) FROM `ProfilesComments` WHERE `IP` = '{$ip}' AND (UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(`Date`) < {$period}*60)" );
	if( $last_count != 0 )
		return _t_err("_You have to wait for PERIOD minutes before you can write another message!", $period);

	$replyTO = (int)$_POST['replyTO'];
	
	// Get comment recipient id
	$recipientID = $oProfile -> _iProfileID;
	if ($recipientID)
	{
		// Check whether email notification on new profile comment was specified by recipient
		$recipientData = db_arr("SELECT `NewProfileCommentNotification`, `Email` FROM `Profiles` WHERE `ID`=$recipientID");
		if ($recipientData && ($recipientData['NewProfileCommentNotification'] == 'Yes'))
		{
			// Send mail to owner of this photo about new comment
			$message = getParam("t_NewProfileCommentMail");
			$subject = getParam('t_NewProfileCommentMail_subject');
			$nick = ucwords(getNickName($record_sender));
			$profileLink = getProfileLink($record_sender);
			$subject = str_ireplace('{Friend}', "$nick", $subject);
			
			$sender = getProfileInfo($record_sender);
			$aPlus = array();
			$aPlus['ProfileReference'] = $sender ? "$nick <a href=\"$profileLink\">($profileLink)</a>" : '<strong>'. _t("_Visitor") .'</strong>';

		    sendMail($recipientData['Email'], $subject, $message, $recipientID, $aPlus);
		}
	}
	
	// Perform insertion
	$query = "
		INSERT INTO `ProfilesComments` SET
			`Date` = NOW(),
			`IP` = '$ip',
			`Sender` = $record_sender,
			`Recipient` = {$oProfile -> _iProfileID},
			`Text` = '$record_text',
			`New` = '1',
			`ReplyTO` = $replyTO
		";
	db_res( $query );
	
	// Track profile comment in newsfeed table
	$recipientInfo = getProfileInfo($recipientID);
	TrackNewAction(12, mysql_insert_id(), $record_sender, '', $record_text, '', $recipientID, $recipientInfo['NickName']);
}

function deleteComment( $commentID )
{
	global $logged;
	global $oProfile;

	$commentID = (int)$commentID;

	if( $oProfile -> owner || $logged['admin'] )
	{
		$del = db_res( "SELECT `ID` FROM `ProfilesComments` WHERE `ReplyTO` = '$commentID' ");
		while ( $del_arr = mysql_fetch_array($del))
			deleteComment( $del_arr['ID'] );
		
		db_res("DELETE FROM `ProfilesComments` WHERE `ID` = '$commentID'");
		
		// Delete from newsfeed table
		db_res("DELETE FROM `NewsFeedTrack` WHERE `mediaid` = $commentID AND `type` = 12");
	}
}

?>
