<?php



/***************************************************************************

*                            Dolphin Smart Community Builder

*                              -----------------

*     begin                : Mon Mar 23 2006

*     copyright            : (C) 2006 BoonEx Group

*     website              : http://www.boonex.com/

* This file is part of Dolphin - Smart Community Builder

*

* Dolphin is free software. This work is licensed under a Creative Commons Attribution 3.0 License. 

* http://creativecommons.org/licenses/by/3.0/

*

* Dolphin is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;

* without even the implied warranty of  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

* See the Creative Commons Attribution 3.0 License for more details. 

* You should have received a copy of the Creative Commons Attribution 3.0 License along with Dolphin, 

* see license.txt file; if not, write to marketing@boonex.com

***************************************************************************/

require_once('inc/header.inc.php');
require_once( BX_DIRECTORY_PATH_INC . 'design.inc.php' );
require_once( BX_DIRECTORY_PATH_INC . 'admin.inc.php' );
require_once( BX_DIRECTORY_PATH_INC . 'images.inc.php' );
require_once( BX_DIRECTORY_PATH_INC . 'sharing.inc.php' );

$_page['name_index']	= 81;
$_page['css_name']		= 'viewPhoto.css';

$oVotingView = new BxTemplVotingView('gphoto', 0, 0);
$_page['extra_js'] 	= $oTemplConfig -> sTinyMceEditorMiniJS . $oVotingView->getExtraJs();

if ( !( $logged['admin'] = member_auth( 1, false ) ) )
{
	if ( !( $logged['member'] = member_auth( 0, false ) ) )
	{
		if ( !( $logged['aff'] = member_auth( 2, false ) ) )
		{
			$logged['moderator'] = member_auth( 3, false );
		}
	}
}

$_page['header'] = _t( "_view Photo" );
$_ni = $_page['name_index'];

$member['ID'] = (int)$_COOKIE['memberID'];

$iFile = 0;
if (isset($_GET['fileID']))
	$iFile = (int)$_GET['fileID'];
elseif (isset($_GET['sefNick']) && isset($_GET['sefTitle']))
{
	$ownerID = getID($_GET['sefNick']);
	if ($ownerID)
		$iFile = db_value("SELECT medID FROM sharePhotoFiles 
			WHERE medProfId = $ownerID AND urltitle = '{$_GET['sefTitle']}'
			LIMIT 1");
}

if (!$iFile)
{
	$sCode = MsgBox( _t( '_No file' ) );
	$_page['name_index'] = 0;
	$_page_cont[0]['page_main_code'] = $sCode;
	PageCode();
	exit();
}

// Add comment
if (isset($_POST['commentAdd']) && isset($_POST['commentText']) && strlen($_POST['commentText']) > 0)
{
	$iFileID = (int)$_POST['fileID'];
	$iUser = (int)$_POST['profileID'];
	$sText = process_db_input(clear_xss(trim(process_pass_data($_POST['commentText']))));

	addMediaComment($iFileID, $iUser, $sText, 'Photo');

	trackMediaComment($iFileID, $iUser, $sText, 'Photo', mysql_insert_id());
	
	// Get photo owner id
	$ownerID = db_value("SELECT `medProfId` FROM `sharePhotoFiles` WHERE `medID`={$_POST['fileID']}");
	if ($ownerID)
	{
		// Check whether email notification on new photo comment was specified by owner
		$ownerData = db_arr("SELECT `NewPhotoCommentNotification`, `Email` FROM `Profiles` WHERE `ID`=$ownerID");
		if ($ownerData && ($ownerData['NewPhotoCommentNotification'] == 'Yes'))
		{
			// Send mail to owner of this photo about new comment
			$message = getParam("t_NewPhotoCommentMail");
			$subject = getParam('t_NewPhotoCommentMail_subject');
			$nick = ucwords(getNickName($iUser));
			$profileLink = getProfileLink($iUser);
			$subject = str_ireplace('{Friend}', "$nick", $subject);
			
			$sender = getProfileInfo($iUser);
			$aPlus = array();
			$aPlus['ProfileReference'] = $sender ? "$nick <a href=\"$profileLink\">($profileLink)</a>" : '<strong>'. _t("_Visitor") .'</strong>';

		    sendMail($ownerData['Email'], $subject, $message, $ownerID, $aPlus);
		}
	}
}

// Edit comment
if (isset($_POST['commentEdit']) && isset($_POST['commentText']) && strlen($_POST['commentText']) > 0)
{
	$iFileID = (int)$_POST['fileID'];
	$commentText = process_db_input($_POST['commentText']);
	
	db_res("UPDATE `sharePhotoComments` SET `commText`='$commentText' WHERE `commID`={$_POST['commID']} LIMIT 1");
}

// Get photo owner id
$ownerID = db_value("SELECT `medProfId` FROM `sharePhotoFiles` WHERE `medID`=$iFile");

// Increase Views number of this photo if visitor not owner and not comment manipulation
if ($member['ID'] != $ownerID && !isset($_POST['commentAdd']) && 
	!isset($_POST['commentEdit']) && !isset($_GET['deleteComm']) && !isset($_GET['commentDeleted']))
	db_res("UPDATE `sharePhotoFiles` SET `medViews` = `medViews` + 1 WHERE `medID`='$iFile'");

	$sQuery = "
		SELECT  `sharePhotoFiles`.*,
				UNIX_TIMESTAMP(`sharePhotoFiles`.`medDate`) as `medDate`,
				COUNT( `share1`.`medID` ) AS `medCount`,
				`Profiles`.`NickName`, `Profiles`.`ID` as OwnerID
		FROM `sharePhotoFiles`
		LEFT JOIN `sharePhotoFiles` AS `share1` USING ( `medProfId` )
		INNER JOIN `Profiles` ON `Profiles`.`ID`=`sharePhotoFiles`.`medProfId`
		WHERE `sharePhotoFiles`.`medID` = $iFile
		GROUP BY `share1`.`medProfId`
	";
$aFile = db_arr($sQuery);
if (is_array($aFile))
{
	$isFriends = is_friends($member['ID'], $aFile['OwnerID']);
	$isOwner = $member['ID'] == $aFile['OwnerID'];

	$_page['header'] = $aFile['medTitle'];
	
	$meta_description = trim(BreakLinesToSpaces($aFile['medDesc']));

	$meta_keywords = SeparateKeywords($aFile['medTags']);

	if (isset($_GET['updateResult']))
		$updateResult = ($_GET['updateResult'] == 1) ? MsgBox('Photo was updated successfully') : MsgBox('Update photo failed');
	else 
		$updateResult = '';	

	// Check for comment deleting
	if (isset($_GET['deleteComm']) && $_GET['deleteComm'] > 0)
	{
		$commentOwner = ($member['ID'] == db_value("SELECT `ProfileID` FROM `sharePhotoComments` WHERE `commID`={$_GET['deleteComm']}"));
		if ($isOwner || $commentOwner)
		{
			db_res("DELETE IGNORE FROM `sharePhotoComments` WHERE `commID`={$_GET['deleteComm']}");
			
			// Delete from NewsFeed table
			db_res("DELETE FROM `NewsFeedTrack` WHERE `type`=4 AND `commentid`={$_GET['deleteComm']} LIMIT 1");
		}
			
		header("location:{$_SERVER['PHP_SELF']}?fileID=$iFile&commentDeleted=1");
	}
		
	$_page_cont[$_ni]['pageSet1'] = $updateResult . PageCompCreateBlocks(1);

	$_page_cont[$_ni]['pageSet2'] = PageCompCreateBlocks(2);

	PageCode();
}
else
{
	$sCode = MsgBox( _t( '_No file' ) );
	$_page['name_index'] = 0;
	$_page_cont[0]['page_main_code'] = $sCode;
	PageCode();
	exit();
}

/*---------------------------- functions of block drawing ---------------------------------------------*/

function PageCompCreateBlocks($iCol = 1)
{
	global $logged;
	global $aFile;	
	global $isBlocked;
	global $isUnconfirmed;

	$sVisible = ($logged['member']) ? 'memb' : 'non';

	$sCode = '';
	$sPos = ' style = "float: left;"';

	$sQuery = "SELECT * FROM `sharePhotoCompose` WHERE `Column`='$iCol' AND FIND_IN_SET( '$sVisible', `Visible` ) ORDER BY `Order`";

	$rCompose = db_res($sQuery);
		
	// Check is visitor in blocklist of photo owner
	$visitorID = (int)$_COOKIE['memberID'];
	$isBlocked = db_value("SELECT ID FROM BlockList WHERE ID=".$aFile['medProfId']." AND Profile=$visitorID LIMIT 1");
	
	// Check is visitor have Unconfirmed status
	$isUnconfirmed = (db_value("SELECT `Status` FROM `Profiles` WHERE ID = $visitorID") == 'Unconfirmed');

	while ($aCompose = mysql_fetch_array($rCompose))

	{

		$func  = 'PageComp' . $aCompose['Func'];

		$sFunc = $func( $aCompose['Content'] );
		
		if ($sFunc == '') continue;

		if ($aCompose['Func'] == 'ViewComments')
			{
			if( $logged['member'] && !$isBlocked && !$isUnconfirmed)
				{
				$leaveCommentLink = "<div class='title_content'>
					<a href='". $_SERVER['REQUEST_URI'] ."#answer_form_to_0' class='title_content_link'
						onclick=\"document.getElementById('answer_form_to_0').style.display = 'block'; 
						document.getElementById('commentLink').style.display = 'none';;\">
						"._t('_Post Comment').'</a>';
				}
			else 
				$leaveCommentLink = '';
			}
		$sCode .= DesignBoxContent(_t($aCompose['Caption']),$sFunc,1, '', $leaveCommentLink);

	}

	return "<div id=\"col$iCol\"".$sPos.">".$sCode."</div>";
}



function PageCompRSS( $sContent )
{
	list( $sUrl, $iNum ) = explode( '#', $sContent );
	$iNum = (int)$iNum;

	return genRSSHtmlOut( $sUrl, $iNum );
}



function PageCompEcho( $sContent )
{
	return $sContent;
}


function PageCompViewFile()
{
	global $aFile;
	global $site;
	global $isFriends;
    global $isOwner;
    
    if ($aFile['Permission'] == 1 && !$isFriends && !$isOwner) 
    	return MsgBox('Locked to the public', 16, 2, 
			"<img src=\"{$site['icons']}lock.gif\" style=\"height:50px; margin-right:5px; vertical-align:middle\"/>");
    
	$sImage = $site['sharingImages'].$aFile['medID'].'_m.'.$aFile['medExt'];

	$sCode = '<div id="viewFile" style="background-image: url(\''.$sImage.'\')">&nbsp;</div>';

	return $sCode;
}


function PageCompActionList()
{
	global $site;
	global $aFile;
	global $member;
	global $isFriends;
    global $isOwner;
    global $logged;
    
    if ($aFile['Permission'] == 1 && !$isFriends && !$isOwner) return '';

	$sMain = 'viewPhoto.php?fileID='.$aFile['medID'];
	
	// Get photo owner
	$photoOwner = db_value("SELECT medProfId FROM sharePhotoFiles WHERE medID = {$aFile['medID']} LIMIT 1");
	$isOwner = ($member['ID'] == $photoOwner);
	
	// Get event owner if photo is in event photos gallery
	$photoEventOwner = db_value("SELECT ResponsibleID FROM SDatingEvents
		LEFT JOIN sharePhotoFiles ON sharePhotoFiles.AssocID=SDatingEvents.ID 
		WHERE SDatingEvents.ID = sharePhotoFiles.AssocID
			AND sharePhotoFiles.medID = {$aFile['medID']} AND sharePhotoFiles.AssocType='event' LIMIT 1");
	$isEventOwner = ($member['ID'] == $photoEventOwner);

    $sOnclick = "javascript: window.open( 'photoActions.php?fileID={$aFile['medID']}&{action}', 'photo', 'width=500, height=380, menubar=no,status=no,resizable=yes,scrollbars=yes,toolbar=no,location=no' );";

	$aActions = array(
		'Fave'=>array('icon'=>'action_fave.gif','link'=>'javascript:void(0);','onClick'=>str_replace('{action}','action=favorite',$sOnclick)),
		'Share'=>array('icon'=>'action_share.gif','link'=>'javascript:void(0);','onClick'=>str_replace('{action}','action=share',$sOnclick)),
		'Report'=>array('icon'=>'action_report.gif','link'=>'javascript:void(0);','onClick'=>str_replace('{action}','action=report',$sOnclick)),
		'Original_Size'=>array('icon'=>'action_download.gif','link'=>$site['sharingImages'].$aFile['medID'].'.'.$aFile['medExt'],'onClick'=>''),
		'Delete'=>array('icon'=>'delete.png',
			'link'=>"{$site['url']}browsePhoto.php?action=del&fileID={$aFile['medID']}&referer={$site['url']}browsePhoto.php?userID={$member['ID']}",
			'onClick'=>"javascript:return confirm('"._t('_confirmDeletePhoto')."');"),
		'Edit'=>array('icon'=>'action_edit.png', 'link'=>"{$site['url']}browsePhoto.php?action=edit&fileID={$aFile['medID']}")
		);

	$sCode = '<div id="actionList">';

	foreach ($aActions as $sKey => $sVal)
	{
		if (($sKey == 'Delete' || $sKey == 'Edit') && (!$member['ID'] || (!$isOwner && !$isEventOwner))) continue;
		if ($sKey == 'Fave' && !$logged['member']) continue;
		$sCode .= '<div><img src="'.$site['icons'].$sVal['icon'].'"><a href="'.$sVal['link'].'" onclick="'.$sVal['onClick'].'">'._t('_'.$sKey).'</a></div>';
	}

	$sCode .= '</div><div class="clear_both"></div>';

	return $sCode;
}



function PageCompRate()
{
	global $aFile;
    global $iFile;
    global $member;
    global $isFriends;
    global $isOwner;
    
    if ($aFile['Permission'] == 1 && !$isFriends && !$isOwner) return '';

	$sCode = '<center>' . _t('_Gallery photos rating is not enabled') . '</center>';

    $oVotingView = new BxTemplVotingView ('gphoto', (int)$iFile);

    if( $oVotingView->isEnabled())
        $sCode = $oVotingView->getBigVoting ();

	return $sCode;
}



function PageCompViewComments()
{
	global $site;
	global $aFile;
	global $member;
	global $logged;
	global $isFriends;
    global $isOwner;
	global $isBlocked;
	global $isUnconfirmed;
    
    if ($aFile['Permission'] == 1 && !$isFriends && !$isOwner) return '';

    $curURL = "{$_SERVER['PHP_SELF']}?fileID={$aFile['medID']}";
    
	$iDivis = 5;
	$iCurr  = 1;

	if (!isset($_GET['commPage']))
		$sLimit =  ' LIMIT 0,'.$iDivis;
	elseif ($_GET['commPage'] > 0)
	{
		$iCurr = (int)$_GET['commPage'];
		$sLimit =  ' LIMIT '.($iCurr - 1)*$iDivis.','.$iDivis;
	}
	else 
		$sLimit = '';

	$sQuery = "SELECT UNIX_TIMESTAMP(`commDate`) AS `commDate`,
					  `commID`,
					  `commText`,
					  `profileID`,
					  `Profiles`.`NickName`
					  FROM
					  `sharePhotoComments`
					  INNER JOIN `Profiles` ON `Profiles`.`ID`=`sharePhotoComments`.`profileID`
					  WHERE `medID`='{$aFile['medID']}' ORDER BY `commDate` DESC";
	$rComments = db_res($sQuery);
	$iNums = mysql_num_rows($rComments);
	$sNav = ($iNums > $iDivis && $sLimit != '') ? commentNavigation($iNums,$iDivis,$iCurr) : '';
	$sQuery .= $sLimit;
	$rComments = db_res($sQuery);
	$sCode = '<script src="inc/js/dynamic_core.js.php" type="text/javascript"></script>';
	$sCode .= '<div id="comments">';

	while($aComments = mysql_fetch_array($rComments))
	{
		$sMessageBR = jsNewLinesClear(addslashes(htmlspecialchars($aComments['commText'])));
		$deleteLink = "$curURL&deleteComm={$aComments['commID']}";
		if ($member['ID'] == $aComments['profileID']) 
			$actions = "<a href=\"{$_SERVER['REQUEST_URI']}#answer_form_to_1\" 
				onclick=\"UpdateFieldStyle('answer_form_to_1','block');
				UpdateField('commID', {$aComments['commID']});
				UpdateFieldStyle('commentLink','none');
				UpdateFieldTiny('commenttext_to_1', '$sMessageBR');\">Edit</a> | <a href=\"$deleteLink\">Delete</a>";
		elseif ($isOwner)
			$actions = '<a href="'.$deleteLink.'">Delete</a>';
		else 
			$actions = '';
		
		$sCode .= '<div class="commentUnit">';
			$sCode .= '<div class="userPic">'.get_member_icon($aComments['profileID'],'left').'</div>';
			$sCode .= '<div class="commentMain"><div class="commentInfo">
				<a href="'.getProfileLink($aComments['profileID']).'">'.$aComments['NickName'].'</a></div>';
			//$sCode .= '(' . defineTimeInterval($aComments['commDate']).')</div>';
			$sCode .= '<br/><div class="newsDate" style="float:left; margin-top:3px;">'. gmdate("j M H:i", $aComments['commDate']).'</div>';
			$sCode .= '<div class="commentText">'.$aComments['commText'].'</div></div>';
			if ($actions != '')
				$sCode .= '<div style="float:left; clear:left; width:100px">'.$actions.'</div>';
			$sCode .= '<div class="clear_both"></div>';
		$sCode .= '</div>';	
	}
	
	$sCode .= $sNav;
	if ($sNav != '')
		$sCode .= "<a href=\"{$_SERVER['PHP_SELF']}?fileID={$aFile['medID']}&amp;commPage=0\"
			style=\"float:left; clear:left; margin-bottom:15px; width:140px; padding-left:10px\">View all $iNums comments</a>";
	
	if ($logged['member'] && !$isBlocked && !$isUnconfirmed)
	{
		$sCode .= '<div class="comment_add_comment" >
			<a id="commentLink" href="javascript:void(0);" 
				onclick="UpdateFieldStyle(\'answer_form_to_0\',\'block\');
				this.style.display = \'none\';">'. _t( '_Post Comment' ) .'</a></div>';

		$sCode .= '<div style="display:none; margin-left:8px;" class="addcomment_textarea" id="answer_form_to_0">
					<form method="post" action="'. $_SERVER['PHP_SELF'] . '?fileID='. $aFile['medID'] .'">
						<textarea name="commentText" class="comment_textarea" id="commenttext_to_0"></textarea>
						<div class="addcomment_submit" style="text-align:center;">
							<input type="hidden" name="profileID" value="'.$member['ID'].'">
							<input type="hidden" name="fileID" value="'.$aFile['medID'].'">
							<input type="submit" name="commentAdd" value="Post" />
							<input type="button" value="Cancel" onClick="
								UpdateFieldStyle(\'answer_form_to_0\',\'none\');
								UpdateFieldStyle(\'commentLink\',\'block\')" />
						</div>
					</form>
				</div>';
		$sCode .= '<div style="display:none; margin-left:8px;" class="addcomment_textarea" id="answer_form_to_1">
					<form method="post" action="'. $_SERVER['PHP_SELF'] . '?fileID='. $aFile['medID'] .'">
						<textarea name="commentText" class="comment_textarea" id="commenttext_to_1"></textarea>
						<div class="addcomment_submit" style="text-align:center;">
							<input type="hidden" name="profileID" value="'.$member['ID'].'">
							<input type="hidden" name="fileID" value="'.$aFile['medID'].'">
							<input type="hidden" name="commID" id="commID" value="0">
							<input type="submit" name="commentEdit" value="Post" />
							<input type="button" value="Cancel" onClick="
								UpdateFieldStyle(\'answer_form_to_1\',\'none\');
								UpdateFieldStyle(\'commentLink\',\'block\')" />
						</div>
					</form>
				</div>';
	}

	$sCode .= '</div>';

	if (isset($_GET['commPage']))
		$sCode .= "<script>if (window.addEventListener) window.addEventListener('load', ShowComments, false);
			else if (document.addEventListener) document.addEventListener('load', ShowComments, false);
			else if (window.attachEvent) window.attachEvent('onload',ShowComments); 
			
			function ShowComments()
			{
				location.hash = '#comments';
			}
			</script>";
	
	return $sCode;
}


function PageCompFileInfo()
{
	global $site;
	global $aFile;
	global $isFriends;
    global $isOwner;
    

	$sImage = '<img src="'.$site['sharingImages'].$aFile['medID'].'.'.$aFile['medExt'].'">';

	$sTitle = strlen($aFile['medTitle']) > 0 ? $aFile['medTitle'] : _t("_Untitled");

	$sCode .= '<div id="videoInfo">';

	$sCode .= '<div id="fileTop">';

		$sCode .= '<div class="fileTitle">'.$sTitle.'</div>';

		$sCode .= '<div class="userPic" style="margin-left:0px">'.get_member_icon($aFile['medProfId'],'left').'</div>';

		$sCode .= '<div class="fileUserInfo"><a href="'.getProfileLink($aFile['medProfId']).'">'.$aFile['NickName'].'</a></div>';

		$sCode .= '<div>'._t("_Photos").': <b>'.$aFile['medCount'].'</b></div>';

			

	$sCode .= '</div>';

	$sCode .= '<div class="clear_both"></div>';

			

	$sCode .= '<div id="serviceInfo">';

		$sCode .= '<div>'._t("_Added").': <b>'.defineTimeInterval($aFile['medDate']).'</b></div>';

		$sCode .= '<div>'._t("_Views").': '.$aFile['medViews'].'</div>';
		
		if ($aFile['Permission'] == 0 || ($aFile['Permission'] == 1 && ($isFriends || $isOwner)))
		{
			$sCode .= '<div>'._t("_URL").': <input type="text" onClick="this.focus(); this.select();" readonly="true" value="'.$site['url'].'viewPhoto.php?fileID='.$aFile['medID'].'"/></div>';
			$sCode .= '<div>'._t("_Embed").' : <input type="text" onClick="this.focus(); this.select();" readonly="true" value="'.htmlspecialchars($sImage).'"></div>';
			$sCode .= '<div>'._t("_Tags").': '.getTagLinks($aFile['medTags'],'Photo').'</div>';
			$sCode .= '<div>'._t("_Description").': '.$aFile['medDesc'].'</div>';
		}

	$sCode .= '</div>';

	$sCode .= '</div>';

	return $sCode;

}



function PageCompLastFiles()
{
	global $site;
	global $aFile;

	$iLimit = 2;

	$sQuery = "SELECT `medID`,
					  `medTitle`,
					  UNIX_TIMESTAMP(`medDate`) as `medDate`,
					  `medExt`,
					  `medViews`,
					  `urltitle`
					  FROM `sharePhotoFiles` 
					  WHERE `medProfId`='{$aFile['medProfId']}' 
					  AND `medID`<>'{$aFile['medID']}' AND `Approved`='true' ORDER BY `medDate` DESC LIMIT $iLimit";
	$rLast = db_res($sQuery);

	$ownerNick = getNickName($aFile['medProfId']);
	
	$sLinkMore =  '';
	if ($aFile['medCount'] - 1 > $iLimit)
	{
		$hrefMore = (useSEF) ? "$ownerNick/photos" : "browsePhoto.php?userID={$aFile['medProfId']}";
		$sLinkMore = '<a href="'.$hrefMore.'">'._t("_See all photos of this user").'</a>';
	}

	$sCode = '<div id="lastFiles">';

	$photoTitleTmplOrig = "{$site['url']}viewPhoto.php?fileID=%photoID%";
	$photoTitleTmpl = (useSEF) ? "{$site['url']}$ownerNick/photo/%urltitle%.html" : $photoTitleTmplOrig;
	
	while ($aLast = mysql_fetch_array($rLast))
	{
		$sImage = $site['sharingImages'].$aLast['medID'].'_t.'.$aLast['medExt'];
		$sTitle = strlen($aLast['medTitle']) > 0 ? $aLast['medTitle'] : _t("_Untitled");

		$oVotingView = new BxTemplVotingView ('gphoto', $aLast['medID']);
	    if( $oVotingView->isEnabled() )
    	{
			$sRate = $oVotingView->getSmallVoting(0);
			$sShowRate = '<div class="galleryRate">'. $sRate . '</div>';
		}

		if ($aLast['urltitle'] == '')
			$photoLink = str_replace('%photoID%', $aLast['medID'], $photoTitleTmplOrig);
		else
			$photoLink = str_replace(array('%photoID%', '%urltitle%'), array($aLast['medID'], $aLast['urltitle']), $photoTitleTmpl);
		$sCode .= '<div class="lastFileUnit">';
			$sCode .= '<div class="lastFilesPic" style="background-image: url(\''.$sImage.'\');">&nbsp;</div>';
			$sCode .= '<div><a href="'.$photoLink.'"><b>'.$sTitle.'</b></a></div>';
			$sCode .= '<div>'._t("_Added").': <b>'.defineTimeInterval($aLast['medDate']).'</b></div>';
			$sCode .= '<div>'._t("_Views").': <b>'.$aLast['medViews'].'</b></div>';
			$sCode .= $sShowRate;
		$sCode .= '</div>';
		$sCode .= '<div class="clear_both"></div>';
	}
	$sCode .= '<div class="lastFilesLink">'.$sLinkMore.'</div>';
	$sCode .= '</div>';

	return $sCode;
}



?>

