<?php

require_once('inc/header.inc.php');
require_once( BX_DIRECTORY_PATH_INC . 'design.inc.php' );
require_once( BX_DIRECTORY_PATH_INC . 'admin.inc.php' );

$_page['name_index']	= 58;
$_page['css_name']		= 'profile_customize.css';
$_page['extra_js'] = '';

if ( !( $logged['admin'] = member_auth( 1, false ) ) )
{
	if ( !( $logged['member'] = member_auth( 0, false ) ) )
	{
		if ( !( $logged['aff'] = member_auth( 2, false ) ) )
		{
			$logged['moderator'] = member_auth( 3, false );
		}
	}
}

$_page['header'] = 'Event Links';
$_page['header_text'] = 'Event Links';

$_ni = $_page['name_index'];


$member['ID'] = (int)$_COOKIE['memberID'];

checkCommunityPermissions($member['ID']);

$_page_cont[$_ni]['page_main_code'] = PageMainCode();

PageCode();


function PageMainCode()
{
	global $site, $member, $logged;
		
	$eventID = (isset($_REQUEST['eventID'])) ? $_REQUEST['eventID'] : 0;

	if (!$eventID)
		return _t_err('Event Not Found');
		
	if (!$logged['admin'])
	{
		// Check for event owner
		$eventOwnerID = db_value("SELECT ResponsibleID FROM SDatingEvents WHERE ID = $eventID");
		if ($member['ID'] != $eventOwnerID)
			return _t_err('You do not have permissions to edit links for this event');
	}
	
	$res = '';
	$links_maxrows = 10;
	$linksImageName = 'small_folder';
	
	// Save posted links
	if ($_POST['linksBoxTitle'])
	{
		$linksBoxTitle = addslashes($_POST['linksBoxTitle']);
		db_res("UPDATE SDatingEvents SET `LinksBoxTitle` = '$linksBoxTitle' WHERE ID = $eventID");
		
		// Clear all links of this event
		db_res("DELETE FROM eventlinks WHERE eventid = $eventID");
	
		// Get all posted links
		$links = getPostedLinks($links_maxrows, $linksImageName);
		
		// Save links to database
		saveLinks($links, $eventID, 'eventlinks', 'eventid');

		// Redirect back to event
		header("Location: {$_POST['eventURL']}");
	}
	
	// Get event's links box title
	$linksBoxTitle = db_value("SELECT LinksBoxTitle FROM SDatingEvents WHERE ID = $eventID");
	if (trim($linksBoxTitle) == '')
		$linksBoxTitle = 'Event Links';

	$linksBoxTitleInput = '<span style="float:left; margin-bottom:10px">Box Title 
		<input type="text" name="linksBoxTitle" value="'. $linksBoxTitle .'"/></span>';
		
	// Get links for this event
	$eventLinks = fill_assoc_array(db_res("SELECT * FROM eventlinks WHERE eventid = $eventID ORDER BY position, title"));

	// Make links content
	$links = '<div class="customize_box_mylinks">' . $linksBoxTitleInput . 
		getLinksMacro($eventLinks, $links_maxrows) . '</div>';
		
	// Submit input
	$submit = <<<EOF
	<div class="customize_footer_box">
		<input type="submit" value="Save Changes" class="button"/><br/><br/>
	</div>
EOF;
		
	$res .= <<<EOF
	<div class="customize_content">
		<form id="eventLinks" name="eventLinks" action="{$_SERVER['PHP_SELF']}" method="post">
		<input type="hidden" name="eventID" value="$eventID"/>
		<input type="hidden" name="eventURL" value="{$_SERVER['HTTP_REFERER']}"/>
			$links
			$submit
		</form>
	</div>
EOF;
		
	return $res;
}

?>