<?php

$K = 'asd;lfoweir2324sdfsdf';

if (!isset($_POST['globalid']) ||
	!isset($_POST['firstName']) || $_POST['firstName'] == '' ||
	!isset($_POST['lastName']) || $_POST['lastName'] == '' ||
	!isset($_POST['email']) || $_POST['email'] == '' ||
	!isset($_POST['userName']) || $_POST['userName'] == '' ||
	!isset($_POST['password']) || $_POST['password'] == '' ||
	!isset($_POST['passPhrase']) || $_POST['passPhrase'] == '') exit;

function getStrippedPostValue($postName)
{
	return (get_magic_quotes_gpc()) ? stripslashes($_POST[$postName]) : $_POST[$postName];
}	

function valueForDb($value)
{
	if (function_exists('mysql_real_escape_string'))
		return mysql_real_escape_string($value);
	elseif (function_exists('mysql_escape_string'))
		return mysql_escape_string($value);
	else
		return addslashes($value);
}

// Strip post values to use in passPhrase
$firstName = getStrippedPostValue('firstName');
$lastName = getStrippedPostValue('lastName');
$email = getStrippedPostValue('email');
$userName = getStrippedPostValue('userName');
$password = getStrippedPostValue('password');

$passPhrase = md5($_POST['globalid'] . $firstName . $lastName . $email . $K . strtoupper($userName) . $password);
if ($passPhrase != $_POST['passPhrase']) exit;

require_once('../inc/db.inc.php');

// Prepare values to insert in database
$firstName = valueForDb($firstName);
$lastName = valueForDb($lastName);
$email = valueForDb($email);
$userName = valueForDb($userName);
$password = md5(valueForDb($password));

$query = "INSERT INTO Profiles (globalid, NickName, FirstName, LastName, Password, Email, LastReg, Status)
	VALUES ({$_POST['globalid']}, '$userName', '$firstName', '$lastName', '$password', '$email', NOW(), 'Active')";
$result = db_res($query);

if (mysql_affected_rows() < 1) exit;

$newUserID = mysql_insert_id();	

// Insert default friend
require_once('../inc/params.inc.php');
$id = intval($defaultfriendid);
$cond = ($id) ? "ID=$defaultfriendid" : "NickName='$defaultfriendid'";

// Check whether member with $defaultfriendid exists in database
$friendId = db_value("SELECT ID FROM Profiles WHERE $cond LIMIT 1");
if ($friendId)
{
	// add default friend to the friend list table
	db_res("INSERT INTO FriendList (ID, Profile, `Check`) VALUES ($newUserID, $friendId, 1)");
}

// Create user data file
require_once('../inc/profiles.inc.php');
if (!createUserDataFile($newUserID)) exit;

echo 'ok';

?>