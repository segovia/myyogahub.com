$(document).ready(function(){
  $("#eventRegisterForm").validate({
  		rules: {
		  	name: {
				remote: {
					url: "profiles/testnick.php",
					type: "post"
				}
			},
			email: {
				remote: {
					url: "profiles/testemail.php",
					type: "post"
				}
			},
			passwd1: {
				notequal: "name"
			},
  			own_question: {
  				required: "#ownQuestionRow:visible"
  			},
  			b_state2: {
  				required: "#b_state2:visible"
  			}
  		},
  		messages: {
  			name: {
  				remote: jQuery.format("{0} is already in use")
  			},
  			email: {
  				remote: jQuery.format("{0} is already in use")
  			}
  		},
  		onkeyup: false,
		errorElement: "div",
		errorClass: "invalidComment"
  });
});

$(document).ready(function(){
	$("#lastname, #email, #name, #passwd1").focus(showBottomHint);
});

$(document).ready(function(){
	$("#uname, #passwd1").blur(hideRightHint);
});