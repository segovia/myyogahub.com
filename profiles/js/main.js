function isdefined(variable)
{
    return (typeof(window[variable]) == "undefined")?  false: true;
}

if (isdefined('jQuery'))
{
	jQuery.validator.addMethod("alphanumeric", function(value, element){
		return !value.match(/[^\w\d]/);
		},
		"Alphanumeric characters only."
	);
	
	jQuery.validator.addMethod("validpass", function(value, element){
		return !value.match(/[^A-Za-z0-9]/);
		},
		"Letters and digits allowed only."
	);
	
	jQuery.validator.addMethod("notequal", function(value, element, params) {
		return (value.toLowerCase() != $("#" + params).val().toLowerCase());
		}, 
		"For security reasons, your password must be different from your username."
	);
	
	jQuery.validator.addMethod("phonenumber", function(value, element){
		var clearNum = value.replace(/[\s\-\.\(\)]/g, '');
		if (clearNum.length < 10) return false;
		return !value.match(/[^\d\s\-\.\(\)]/);
		},
		"Specify correct phone number at least with 10 digits."
	);	
	
	jQuery.validator.addMethod("faxnumber", function(value, element){
		return !value.match(/[^\d\s\-\.\(\)]/);
		},
		"Specify correct fax number."
	);		
	
	$(document).ready(function(){
		  $("#loginForm").validate({
				errorClass: "invalidComment",
				highlight: function(element, errorClass) {
				    $(element.form).find("label[for=" + element.id + "]").addClass(errorClass);
		  		}
		  })
	});
	
	function showBottomHint() {
		var bottomHintID = $(this).attr("id") + '_bottom_hint';
		var bottomHint = $("div[id='"+bottomHintID+"']");
		bottomHint.show();	
	}
	
	function hideRightHint() {
		$("#rightHint").hide();
	}
}

function selectValue(selectElementID, valueToSelect, defaultValue)
{
	var selectElement = document.getElementById(selectElementID);
	if (!selectElement || !valueToSelect) return false;
	
	var floatValue = parseFloat(valueToSelect);
	var valueToCompare = (isNaN(floatValue)) ? valueToSelect : floatValue;

	var selected = false;
	
	for (var i = 0; i < selectElement.options.length; i++)
	{
		if (selectElement.options[i].value == valueToCompare.toString())
		{
			selectElement.options[i].selected = 'selected';
			selected = true;
			break;
		}
	}
	
	if (defaultValue && !selected)
		selectElement.value = defaultValue;
}