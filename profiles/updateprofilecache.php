<?php

if (!isset($_POST['globalid'])) exit;

require_once('../inc/db.inc.php');
require_once('../inc/profiles.inc.php');

// Get userid by globalid
$userid = db_value("SELECT ID FROM `Profiles` WHERE globalid={$_POST['globalid']}");
if (!$userid) exit;

createUserDataFile($userid);

if (isset($_POST['defaultfriend']))
{
	// Insert default friend
	require_once('../inc/params.inc.php');
	$id = intval($defaultfriendid);
	$cond = ($id) ? "ID=$defaultfriendid" : "NickName='$defaultfriendid'";
	
	// Check whether member with $defaultfriendid exists in database
	$friendId = db_value("SELECT ID FROM Profiles WHERE $cond LIMIT 1");
	if ($friendId)
		db_res("INSERT INTO FriendList (ID, Profile, `Check`) VALUES ($userid, $friendId, 1)");
}

echo 'ok';

?>