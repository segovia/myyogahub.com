<?php

$K = 'asd;lfoweir2324sdfsdf';

if (!isset($_POST['globalid']) || !isset($_POST['passPhrase'])) exit;

function valueForDb($value)
{
	if (function_exists('mysql_real_escape_string'))
		return mysql_real_escape_string($value);
	elseif (function_exists('mysql_escape_string'))
		return mysql_escape_string($value);
	else
		return addslashes($value);
}

$passPhrase = md5($_POST['globalid'] . $K);
if ($passPhrase != $_POST['passPhrase']) exit;

require_once('../inc/db.inc.php');
require_once('../inc/profiles.inc.php');

// Get userid by globalid
$userid = db_value("SELECT ID FROM `Profiles` WHERE globalid={$_POST['globalid']}");
if (!$userid) exit;

// Prepare values to insert in database and perform update query
if (isset($_POST['firstname']) && $_POST['firstname'] != '') // update profile
{
	$firstname = valueForDb($_POST['firstname']);
	$lastname = valueForDb($_POST['lastname']);
	$sex = valueForDb($_POST['sex']);
	$interests = valueForDb($_POST['interests']);
	$headline = valueForDb($_POST['headline']);
	$description = valueForDb($_POST['description']);

	$query = "UPDATE `Profiles` SET FirstName = '$firstname', LastName = '$lastname', Sex = '$sex', 
			Tags = '$interests', Headline = '$headline', DescriptionMe = '$description'
		WHERE globalid={$_POST['globalid']}";
	$result = db_res($query);

	createUserDataFile($userid);

	if ($result) echo 'ok';
}
elseif (isset($_POST['email']) && $_POST['email'] != '') // update settings
{
	$email = valueForDb($_POST['email']);
	$password = valueForDb($_POST['password']);
	$city = valueForDb($_POST['city']);
	$country = valueForDb($_POST['country']);

	$oldPassQuery = "SELECT `Password` FROM `Profiles` WHERE globalid = {$_POST['globalid']}";
	$oldPass = db_value($oldPassQuery);

	if ($password != $oldPass)
	{		
		// Check whether email notification on password changed was specified by member
		$memberData = db_arr("SELECT `ID`, `PasswordChangedNotification` FROM `Profiles` 
			WHERE globalid = {$_POST['globalid']}");
		if ($memberData['PasswordChangedNotification'] == 'Yes')
		{
			// Send mail to member about the password was changed
			$message = getParam("t_PasswordChangedMail");
			$subject = getParam('t_PasswordChangedMail_subject');

			$aPlus = array();
			$aPlus['NewPassword'] = $_POST['passwordtxt'];
			
			require_once('../inc/utils.inc.php');
			
			sendMail($email, $subject, $message, $memberData['ID'], $aPlus);
		}
	}
	
	$query = "UPDATE `Profiles` SET Email = '$email', Password = '$password', City = '$city', Country = '$country'
		WHERE globalid={$_POST['globalid']}";
	$result = db_res($query);
	
	createUserDataFile($userid);	

	if ($result) echo 'ok';
}
elseif (isset($_POST['billadd_address']) && $_POST['billadd_address'] != '') // update billing address
{
	$city = valueForDb($_POST['billadd_city']);
	$country = valueForDb($_POST['billadd_country']);
	$state = valueForDb($_POST['billadd_state']);
	$zip = valueForDb($_POST['billadd_zip']);
	
	$query = "UPDATE `Profiles` SET City = '$city', Country = '$country', State = '$state', zip = '$zip'
		WHERE globalid={$_POST['globalid']}";
	$result = db_res($query);
	
	createUserDataFile($userid);	

	if ($result) echo 'ok';
}
elseif (isset($_POST['newpassword'])) // change password after remind
{
	$newPassword = md5($_POST['newpassword']);	
	$query = "UPDATE `Profiles` SET Password = '$newPassword'
		WHERE globalid={$_POST['globalid']}";
	$result = db_res($query);
	
	if ($result) echo 'ok';
}

?>