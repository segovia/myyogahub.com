<?php

$K = 'asd;lfoweir2324sdfsdf';

if (!isset($_POST['globalid']) ||
	!isset($_POST['firstname']) || $_POST['firstname'] == '' ||
	!isset($_POST['lastname']) || $_POST['lastname'] == '' ||
	!isset($_POST['email']) || $_POST['email'] == '' ||
	!isset($_POST['username']) || $_POST['username'] == '' ||
	!isset($_POST['password']) || $_POST['password'] == '' ||
	!isset($_POST['passPhrase']) || $_POST['passPhrase'] == '') exit;

function getStrippedPostValue($postName)
{
	return (get_magic_quotes_gpc()) ? stripslashes($_POST[$postName]) : $_POST[$postName];
}	

function valueForDb($value)
{
	if (function_exists('mysql_real_escape_string'))
		return mysql_real_escape_string($value);
	elseif (function_exists('mysql_escape_string'))
		return mysql_escape_string($value);
	else
		return addslashes($value);
}

// Strip post values to use in passPhrase
$firstName = getStrippedPostValue('firstname');
$lastName = getStrippedPostValue('lastname');
$email = getStrippedPostValue('email');
$userName = getStrippedPostValue('username');
$password = getStrippedPostValue('password');

$passPhrase = md5($_POST['globalid'] . $firstName . $lastName . $email . $K . strtoupper($userName) . $password);
if ($passPhrase != $_POST['passPhrase']) exit;

require_once('../inc/db.inc.php');

// Prepare values to insert in database
$firstName = valueForDb($firstName);
$lastName = valueForDb($lastName);
$email = valueForDb($email);
$userName = valueForDb($userName);
$password = md5(valueForDb($password));
$country = valueForDb($_POST['billadd_country']);
$city = valueForDb($_POST['billadd_city']);
$status = ($_POST['yhmember']) ? 'Active' : 'Suspended';
$zip = valueForDb($_POST['billadd_zip']);

$query = "INSERT INTO profiles (globalid, NickName, FirstName, LastName, Password, Country, City, 
		Email, LastReg, Status, zip)
	VALUES ({$_POST['globalid']}, '$userName', '$firstName', '$lastName', '$password', '$country', '$city', 
		'$email', NOW(), '$status', '$zip')";
$result = db_res($query);

if (mysql_affected_rows() < 1) exit;

$newUserID = mysql_insert_id();	

// Insert default friend
require_once('../inc/params.inc.php');
$id = intval($defaultfriendid);
$cond = ($id) ? "ID=$defaultfriendid" : "NickName='$defaultfriendid'";

// Check whether member with $defaultfriendid exists in database
$friendId = db_value("SELECT ID FROM Profiles WHERE $cond LIMIT 1");
if ($friendId)
{
	// add default friend to the friend list table
	db_res("INSERT INTO FriendList (ID, Profile, `Check`) VALUES ($newUserID, $friendId, 1)");
}

// Create user data file
require_once('../inc/profiles.inc.php');
if (!createUserDataFile($newUserID)) exit;

echo 'ok';

?>