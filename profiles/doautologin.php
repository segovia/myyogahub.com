<?php

if (!defined('YH_AUTOLOGIN_DEBUG'))
	define('YH_AUTOLOGIN_DEBUG', true);

if (!function_exists('yh_debug'))
{
	function yh_debug($msg)
	{
		if (!defined('YH_AUTOLOGIN_DEBUG') || !YH_AUTOLOGIN_DEBUG)
			return;
		
		$dbgFileName = (@$_SERVER['REMOTE_ADDR']) . '_autologin.log';
		
		@file_put_contents(dirname(__FILE__) . '/' . $dbgFileName,
			$msg . "\n", FILE_APPEND);
	}
}
	
function goToMainPage()
{
	yh_debug("Autologin failed, bye-bye...");
	header("Location: ../index.php?myh_rd=1");
	exit;
}

yh_debug('');
yh_debug('Start doautologin.php: ' . date('r'));

if (!isset($_GET['loginkey']) || !isset($_GET['returnurl'])) 
{
	yh_debug("Missing parameter:");
	yh_debug("loginkey = " . @$_GET['loginkey']);
	yh_debug("returnurl = " . @$_GET['returnurl']);
	goToMainPage();
}

require_once('superuserconfig.php');
yh_debug("superuserconfig.php loaded");

$remoteUrl = "{$su_config['url']}index.php/authmanager/getuserid_by_loginkey/{$_GET['loginkey']}";
yh_debug("Requesting with file_get_contents:");
yh_debug($remoteUrl);

$globalid = @file_get_contents($remoteUrl);
if (!intval($globalid))
{
	yh_debug("Failed, retrieved value: globalid = $globalid");
	goToMainPage();
}
yh_debug("Retrieved value: globalid = $globalid");

require_once('../inc/db.inc.php');

// Check whether user with globalid exists on myyogahub.com
yh_debug("Check whether user with globalid exists on myyogahub.com...");
$memberData = db_assoc_arr("SELECT ID, Password FROM Profiles WHERE globalid = $globalid");
if (!$memberData)
{
	yh_debug("Failed");
	goToMainPage();
}
yh_debug("OK, retrieved user values from database: ");
yh_debug(print_r($memberData, true));
	
$memberID = @$memberData['ID'];
$password = @$memberData['Password'];

if (!$memberID || !$password)
{
	yh_debug("Value missing");
	goToMainPage();
}

yh_debug("Logging in to MYH...");

// Clear cookies
setcookie("memberID", 0, time() - 3600, '/');
setcookie("memberPassword", '', time() - 3600, '/');

// Do login
setcookie("memberID", $memberID, 0, '/');
setcookie("memberPassword", $password, 0, '/');

$update_res = db_res("UPDATE Profiles SET LastLoggedIn = NOW() WHERE ID = $memberID");

yh_debug("OK! Redirecting to {$_GET['returnurl']}");
yh_debug("bye-bye...");
header("Location: {$_GET['returnurl']}");

?>