Thank you for choosing Dolphin Smart Community Builder from BoonEx.

This is a Dolphin 6.0.0000 
Released on 1st of September 2007.

Check http://www.boonex.com for version updates.

To install Dolphin, please refer to Install.txt. 

Latest installation manual and docs in different languages can be found 
online at http://www.boonex.net/dolphin/wiki/DolphinDocs .
