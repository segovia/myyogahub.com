<?

/***************************************************************************
*                            Dolphin Smart Community Builder
*                              -----------------
*     begin                : Mon Mar 23 2006
*     copyright            : (C) 2006 BoonEx Group
*     website              : http://www.boonex.com/
* This file is part of Dolphin - Smart Community Builder
*
* Dolphin is free software. This work is licensed under a Creative Commons Attribution 3.0 License. 
* http://creativecommons.org/licenses/by/3.0/
*
* Dolphin is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
* without even the implied warranty of  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the Creative Commons Attribution 3.0 License for more details. 
* You should have received a copy of the Creative Commons Attribution 3.0 License along with Dolphin, 
* see license.txt file; if not, write to marketing@boonex.com
***************************************************************************/

require_once( 'inc/header.inc.php' );
require_once( BX_DIRECTORY_PATH_INC . 'design.inc.php' );
require_once( BX_DIRECTORY_PATH_INC . 'profiles.inc.php' );

$_page['name_index'] = 44;

$relocateToReferer = true;
$pageContent = '';

// Prevent redirect to member.php in popup 
//$logged['member'] = member_auth(0, true);
if (!isset($_COOKIE['memberID']) || !isset($_COOKIE['memberPassword']))
	$pageContent = _t_err('You are not logged in');
else
{
	$sourceID = (int)$_COOKIE['memberID'];
	$targetID = (isset($_REQUEST['global']) && $_REQUEST['global'] == 1) ? 
		getMemberIDByGlobalID($_GET['ID']) : (int)$_GET['ID'];
}

$action = $_GET['action'];

$_ni = $_page['name_index'];

	switch ($action)
	{
		case 'block':
			$_page['header'] = _t( "_Block list" );
			if (!$pageContent)
				$pageContent = PageListBlock($sourceID, $targetID);
			$_page_cont[$_ni]['page_main_code'] = DesignBoxContent( _t('_Block list'), $pageContent, $oTemplConfig -> PageListPop_db_num );
		break;
		case 'unblock':
			$_page['header'] = _t( "_Block list" );
			if (!$pageContent)
				$pageContent = PageListUnblock($sourceID, $targetID);
			$_page_cont[$_ni]['page_main_code'] = DesignBoxContent( _t('_Block list'), $pageContent, $oTemplConfig -> PageListPop_db_num );
		break;
		case 'hot':
			$_page['header'] = _t('_Hot list');
			if (!$pageContent)
				$pageContent = PageListHot($sourceID, $targetID);
			$_page_cont[$_ni]['page_main_code'] = DesignBoxContent( _t('_Hot list'), $pageContent, $oTemplConfig -> PageListPop_db_num );
		break;
		case 'friend':
			$_page['header'] = _t('_Friend list');
			if (!$pageContent)
				$pageContent = PageListFriend($sourceID, $targetID);
			$_page_cont[$_ni]['page_main_code'] = DesignBoxContent(_t('_Friend list'), $pageContent, $oTemplConfig -> PageListPop_db_num );
		break;
		case 'spam':
			$_page['header'] = _t('_Spam report');
			if (!$pageContent)
				$pageContent = PageListSpam($sourceID, $targetID);
			$_page_cont[$_ni]['page_main_code'] = DesignBoxContent( _t('_Spam report'), $pageContent, $oTemplConfig -> PageListPop_db_num);
		break;
	}

PageCode();

function PageListBlock( $sourceID, $targetID )
{
	$ret = '';
	
	// Remove blocked member from friends list
	db_res("DELETE FROM `FriendList` WHERE `ID` = $sourceID AND `Profile` = $targetID OR 
		`ID` = $targetID AND `Profile` = $sourceID LIMIT 1");
	
	$query = "REPLACE INTO `BlockList` SET `ID` = '$sourceID', `Profile` = '$targetID';";
	if( db_res($query, 0) )
		$ret = _t_action('_User was added to block list');
	else
		$ret = _t_err('_Failed to apply changes');

	return $ret;
}

function PageListUnblock($sourceID, $targetID)
{
	$ret = '';
	
	// Remove blocked member from block list
	$query = "DELETE FROM `BlockList` WHERE `ID` = $sourceID AND `Profile` = $targetID LIMIT 1";
	if( db_res($query, 0) )
		$ret = _t_action('_User was deleted from block list');
	else
		$ret = _t_err('_Failed to apply changes');

	return $ret;	
}

function PageListHot($sourceID, $targetID)
{
	if ($sourceID == $targetID)
		return _t_action('Sorry, you cannot add yourself to your own hotlist.');
	
	$ret = '';

	$query = "REPLACE INTO `HotList` SET `ID` = '$sourceID', `Profile` = '$targetID';";
	if( db_res($query, 0) )
	{
		$ret = _t_action('_User was added to hot list');
	}
	else
	{
		$ret = _t_err('_Failed to apply changes');
	}

	// Save output message to session and redirect to referer	
	saveGuiMessage($ret);
	session_write_close();
	header("Location: {$_SERVER['HTTP_REFERER']}");
	
	return $ret;
}

function PageListFriend($sourceID, $targetID)
{
	if ($sourceID == $targetID)
		return _t_action('Sorry, you cannot add yourself to your own friends list.');
	
	$ret = '';
	$query = "SELECT * FROM `FriendList` WHERE (`ID` = '$sourceID' and `Profile` = '$targetID') or ( `ID` = '$targetID' and `Profile` = '$sourceID')";
	$temp = db_assoc_arr($query);

	if( $sourceID == $temp['ID'])
	{
		if ($temp['Check'] == 0)
			$ret = _t_action('_User was already invited to friend list');
		elseif ($temp['Check'] == 1)
			$ret = _t_action('_already_in_friend_list');
	}
	elseif( $targetID == $temp['ID'])
	{
		if ($temp['Check'] == 0)
		{
			$query = "UPDATE `FriendList` SET `Check` = '1' WHERE `ID` = '$targetID' AND `Profile` = '$sourceID';";
			if( db_res($query) )
			{
				$ret = _t_action('_User was added to friend list');
				updateRequestsInMemcache($sourceID);
			}
			else
				$ret = _t_err('_Failed to apply changes');
		}
		else 
			$ret = _t_action('_already_in_friend_list');
	}
	else
	{
		// Check whether email notification on new friend request was specified by target member
		$targetData = db_arr("SELECT `NewFriendRequestNotification`, `Email` FROM `Profiles` WHERE `ID`=$targetID");
		if ($targetData && ($targetData['NewFriendRequestNotification'] == 'Yes'))
		{
			// Send mail to source member about new friend request
			$message = getParam("t_NewFriendRequestMail");
			$subject = getParam('t_NewFriendRequestMail_subject');
			$subject = str_ireplace('{Friend}', ucwords(getNickName($sourceID)), $subject);
			
			$sender = getProfileInfo($sourceID);
			
			$hisStr = (strtolower($sender['Sex']) == 'male') ? 'his' : 'her';
			$message = str_replace('{his}', $hisStr, $message);
			
			$aPlus = array();
			$profileLink = getProfileLink($sourceID);
			$aPlus['ProfileReference'] = $sender ? "{$sender['NickName']} (<a href=\"$profileLink\">$profileLink</a>)" 
				: '<strong>'. _t("_Visitor") .'</strong>';

		    sendMail($targetData['Email'], $subject, $message, $targetID, $aPlus);
		}
		
		$query = "INSERT INTO `FriendList` SET `ID` = '$sourceID', `Profile` = '$targetID', `Check` = '0';";
		if (db_res($query))
		{
			$ret = _t_action('_User was invited to friend list');
			updateRequestsInMemcache($targetID);
		}
		else
			$ret = _t_err('_Failed to apply changes');
	}

	// Save output message to session and redirect to referer	
	saveGuiMessage($ret);
	session_write_close();
	header("Location: {$_SERVER['HTTP_REFERER']}");
	
	return $ret;
}

function PageListSpam($sourceID, $targetID)
{
	global $site;
	
	$reporterID = $sourceID;
	$spamerID = $targetID;

    $aReporter = getProfileInfo( $reporterID );// db_arr("SELECT `NickName` FROM `Profiles` WHERE `ID` = '$reporterID';", 0);
    $aSpamer   = getProfileInfo( $spamerID );//db_arr("SELECT `NickName` FROM `Profiles` WHERE `ID` = '$spamerID';", 0);

    $message = getParam( "t_SpamReport" );
	$subject = getParam('t_SpamReport_subject');


    $aPlus = array();
    $aPlus['reporterID'] = $reporterID;
    $aPlus['reporterNick'] = $aReporter['NickName'];

    $aPlus['spamerID'] = $spamerID;
    $aPlus['spamerNick'] = $aSpamer['NickName'];


	$mail_result = sendMail( getParam('main_email'), $subject, $message, '', $aPlus );

	if ( $mail_result )
	    $ret = _t_action('_Report about spam was sent');
	else
	    $ret = _t_err('_Report about spam failed to sent');

	return $ret;
}

?>