<?php
header("Content-type: plain/text"); 

if (strpos($_SERVER['HTTP_HOST'], 'belucky') !== false || 
	strpos($_SERVER['HTTP_HOST'], 'localhost') !== false ||
	strpos($_SERVER['HTTP_HOST'], 'megliosoft') !== false ||
	strpos($_SERVER['HTTP_HOST'], 'dev.') !== false)
{
echo "# Hide the whole DEV site
User-agent: *
Disallow: /";
}
else
{
echo "User-agent: * 
Disallow: /privacy.php
Disallow: /terms_of_use.php";
}

?>
