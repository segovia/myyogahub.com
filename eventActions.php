<?php



/***************************************************************************

*                            Dolphin Smart Community Builder

*                              -----------------

*     begin                : Mon Mar 23 2006

*     copyright            : (C) 2006 BoonEx Group

*     website              : http://www.boonex.com/

* This file is part of Dolphin - Smart Community Builder

*

* Dolphin is free software. This work is licensed under a Creative Commons Attribution 3.0 License. 

* http://creativecommons.org/licenses/by/3.0/

*

* Dolphin is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;

* without even the implied warranty of  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

* See the Creative Commons Attribution 3.0 License for more details. 

* You should have received a copy of the Creative Commons Attribution 3.0 License along with Dolphin, 

* see license.txt file; if not, write to marketing@boonex.com

***************************************************************************/



require_once('inc/header.inc.php');

require_once( BX_DIRECTORY_PATH_INC . 'design.inc.php' );

require_once( BX_DIRECTORY_PATH_INC . 'admin.inc.php' );

require_once( BX_DIRECTORY_PATH_INC . 'images.inc.php' );

require_once( BX_DIRECTORY_PATH_INC . 'sharing.inc.php' );



$_page['name_index']	= 44;

$_page['css_name']		= 'explanation.css';



$_page['extra_js'] = '';



if ( !( $logged['admin'] = member_auth( 1, false ) ) )

{

	if ( !( $logged['member'] = member_auth( 0, false ) ) )

	{

		if ( !( $logged['aff'] = member_auth( 2, false ) ) )

		{

			$logged['moderator'] = member_auth( 3, false );

		}

	}

}





$_page['header'] = _t( "Event Actions" );

$_page['header_text'] = _t("Event Actions");



$_ni = $_page['name_index'];



$member['ID'] = (int)$_COOKIE['memberID'];



if (isset($_POST['eventID']) && isset($_POST['send']) && isset($_POST['email']))

{

	$iEvent    = (int)$_POST['eventID'];

	$sEmail   = $_POST['email'];

	$sMessage = htmlspecialchars_adv($_POST['messageText']);

	$sCode .=  sendFileInfo($iEvent, $sEmail, $sMessage);

}



if (isset($_GET['eventID']))
	$sCode = displaySubmitForm($_GET['eventID']);


$_page_cont[$_ni]['page_main_code'] = DesignBoxContent("Notification", $sCode, 1);



PageCode();



function displaySubmitForm($iFile)
{
	global $member;
	global $site;

	if ($iFile)
	{
		$sAddr  = '<div>'._t("_Enter email(s)").':</div><div><input type="text" size="40" name="email"></div>';
		$sFromName = '<div>From Name:</div><div><input type="text" size="40" name="fromname"></div>';
		$sSites = '<div style="margin-top:10px; margin-bottom:10px;">'.getSitesArray($iFile,'Photo').'</div>';
	}

	$sCode  = '<div class="mediaInfo">';
	$sCode .= '<form name="submitAction" method="post" action="'.$_SERVER['PHP_SELF'].'">';
	$sCode .= '<input type="hidden" name="eventID" value="'.$iFile.'">';

	$sCode .= $sAddr.$sFromName.$sSites;

	$sCode .= '<div>'._t("_Message text").'</div>';
	$sCode .= '<div><textarea cols="30" rows="10" name="messageText"></textarea></div>';
	$sCode .= '<div><input type="submit" size="15" name="send" value="Send">';
	$sCode .= '<input type="reset" size="15" name="send" value="Reset"></div>';

	$sCode .= '</form>';
	$sCode .= '</div>';

	return $sCode;
}





function sendFileInfo($iFile, $sEmail, $sMessage)

{

	global $site;

	global $member;

	

	$aUser = getProfileInfo( $member['ID'] );

	$emailNotify = getParam('notifications_email');
	$emailNotifyTitle = getParam('notifications_email_title');	

	$sMailHeader		= "From: $emailNotifyTitle <$emailNotify>";

	$sMailParameters	= "-f$emailNotify";

	

	$sMailHeader = "MIME-Version: 1.0\r\n" . "Content-type: text/html; charset=UTF-8\r\n" . $sMailHeader;

	$fromName = (isset($_POST['fromname']) && $_POST['fromname'] != '') ? $_POST['fromname'] : $aUser['NickName'];
	
	$sMailSubject = "$fromName shared an Event with you";	

	$sMailBody    = "Hello,\n $fromName shared an event with you: <a href=\"{$site['url']}events.php?action=show_info&event_id=$iFile\">See it</a>\n <i>$sMessage</i>\n	Regards,<br/>$fromName";
	

	$aEmails = explode(",",$sEmail);

	foreach ($aEmails as $iKey => $sMail)

	{

		$sMail = trim($sMail);

		$iSendingResult = mail( $sMail, $sMailSubject, nl2br($sMailBody), $sMailHeader, $sMailParameters );

	}

	if ($iSendingResult)

	{

		$sCode = '<div class="mediaInfo">'._t("_File info was sent").'</div>';

	}

	return $sCode;

}



?>