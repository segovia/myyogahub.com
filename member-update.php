<?php

require_once( 'inc/header.inc.php' );
require_once( BX_DIRECTORY_PATH_INC . 'design.inc.php' );
require_once( BX_DIRECTORY_PATH_INC . 'profiles.inc.php' );
require_once( BX_DIRECTORY_PATH_INC . 'members.inc.php' );
require_once( BX_DIRECTORY_PATH_INC . 'utils.inc.php' );
require_once( BX_DIRECTORY_PATH_INC . 'sharing.inc.php' );
require_once( BX_DIRECTORY_PATH_CLASSES . 'BxDolClassifieds.php' );
require_once( BX_DIRECTORY_PATH_ROOT . "templates/tmpl_{$tmpl}/scripts/BxTemplProfileView.php" );

// --------------- page variables and login

if (isset($_COOKIE['memberID']))
{
	$oProfile = new BxTemplProfileView( $_COOKIE['memberID'] );
	$_page['extra_css'] = $oProfile -> genProfileCSS( $_COOKIE['memberID'] );
}

$_page['name_index'] = 6;
$_page['extra_css'] .= '<link href="templates/tmpl_uni/css/blogs.css" rel="stylesheet" type="text/css" />';
$_page['extra_css'] .= '<link href="templates/tmpl_uni/css/browse.css" rel="stylesheet" type="text/css" />';
$_page['css_name'] = 'member_update.css';

$_page['header'] = _t( "Member Update" );

if ( !( $_POST['ID'] && $_POST['Password'] ) && ( $_COOKIE['memberID'] && $_COOKIE['memberPassword'] ) )
{
    if ( !( $logged['member'] = member_auth( 0, false ) ) )
		login_form( _t( "_LOGIN_OBSOLETE" ) );
}

if (!$logged)
{
	header('Location: member.php');
	exit;
}

$member['ID'] = (int)$_COOKIE['memberID'];
$member['Password'] = $_COOKIE['memberPassword'];

$p_arr = getProfileInfo( $member['ID'] );

// Profile was filled already
if (trim($p_arr['DescriptionMe']) != '')
{
	header('Location: member.php');
	exit;	
}

$_ni = $_page['name_index'];
$_page_cont[$_ni]['content_column_1'] = getPageBlocks( 1 );
$_page_cont[$_ni]['content_column_2'] = getPageBlocks( 2 );

// this is dynamic page -  send headers to do not cache this page
send_headers_page_changed();

PageCode();


function getPageBlocks($col)
{
	global $site;

	$leftCol = '<div class="leftCol">' . PageCompActivate() . '</div>';
	
	$rightCol = '<div class="rightCol">'. 
		PageCompProfileInfo() . 
		PageCompQuickLinks() . 
		PageCompFriends() .
	'</div>
	<div class="rightHint" id="rightHint" style="display:none">
	<img src="'. $site['url'] . $site['images'] .'info.png" class="infoIcon"/>
	<span id="rightHintText"></span>
</div>';
	
	return ($col == 1) ? $leftCol : $rightCol;
}

function PageCompActivate()
{
	global $p_arr, $tmpl, $site, $su_config;
	
	$K = 'asd;lfoweir2324sdfsdf';
	
	$addProfilePhotoLink = "<div class='title_content'>
		<a class=\"title_content_link\" href=\"javascript:void(0)\"
		onclick=\"needCompleteMsg()\">Add Profile Photo</a></div>";
	
	if (!$p_arr['globalid'])
		return DesignBoxContent(_t('_ActivateCommunityProfile'), 
			'For global users only!', 1, $addProfilePhotoLink);
	
	$tal = new PHPTAL(BX_DIRECTORY_PATH_ROOT . "templates/tmpl_{$tmpl}/activate.html");
	$tal->globalid = $p_arr['globalid'];
	$tal->passPhrase = md5($p_arr['globalid'] . $K);
	$referer = (isset($_REQUEST['referer'])) ? urlencode($_REQUEST['referer']) : '';
	$tal->referer = $referer;

	if (isset($_POST['description']) && trim($_POST['description']) != '')
	{
		// Validation form fields on global site 
		// and update profile data on global site and other YH sites (except MYH)	
		$updateRes = do_post_request("{$su_config['url']}fill_medium_from_myh", $_POST);
		
		if ($updateRes == 'ok')
		{
			// Update profile data on myh
			$sex = process_db_input($_POST['sex']);
			$interests = process_db_input($_POST['interests']);
			$headline = process_db_input($_POST['headline']);
			$description = process_db_input($_POST['description']);
			$city = process_db_input($_POST['b_city']);
			$country = process_db_input($_POST['b_country']);
		
			$query = "UPDATE Profiles SET Sex = '$sex', 
					Tags = '$interests', Headline = '$headline', DescriptionMe = '$description',
					City = '$city', Country = '$country', Status = 'Active'
				WHERE globalid={$p_arr['globalid']} LIMIT 1";
			$result = db_res($query);
			if ($result)
			{
				createUserDataFile($p_arr['ID']);
				header("Location: activation_account.php?referer=$referer");
				exit;
			}
		}
		else
		{
			$tal->errorMsg = $updateRes;
			$tal->male = ($_POST['sex'] == 'male');
			$tal->b_city = $_POST['b_city'];
			$tal->b_country = $_POST['b_country'];
			$tal->interests = $_POST['interests'];
			$tal->headline = $_POST['headline'];
			$tal->description = $_POST['description'];
			$tal->timezone = $_POST['timezoneoffset'];
			if (isset($_POST['source']))
			{
				$tal->source = $_POST['source'];
				$tal->discounts = (isset($_POST['discounts']) && $_POST['discounts'] == 'on');
			}
		}
	}
	else
	{
		// Get profile fields from global site
		$profileData = unserialize(file_get_contents(
			"{$su_config['url']}get_profile_fields/{$p_arr['globalid']}/{$p_arr['Password']}"));
		
		if (is_array($profileData))
		{		
			$tal->male = ($profileData['sex'] != 'female');
			$tal->b_city = ($profileData['city'] != '') ? $profileData['city'] : $profileData['billadd_city'];
			$tal->b_country = ($profileData['country'] != '') ? $profileData['country'] : $profileData['billadd_country'];
			$tal->interests = $profileData['interests'];
			$tal->headline = $profileData['headline'];
			$tal->sec_question = $profileData['security_question'];
			$tal->sec_answer = ($profileData['security_answer'] == '') ? '' : '********';
			$tal->timezone = $profileData['timezone'];
			if ($profileData['source'] != '')
				$tal->source = $profileData['source'];
			$tal->discounts = ($profileData['discounts'] == 1);
		} 
		else
		{
			//$tal->errorMsg = $profileData;
			$tal->male = ($p_arr['Sex'] != 'female');
			$tal->b_city = $p_arr['City'];
			$tal->b_country = $p_arr['Country'];
			$tal->sec_question = '';
			$tal->timezone = '';
			$tal->discounts = true;			
		}
	}
	
	return DesignBoxContent(_t('_ActivateCommunityProfile'), PageCompActivateContent($tal), 1, $addProfilePhotoLink);
}

function PageCompProfileInfo()
{
	global $p_arr, $site;
	
	$thumb = '<div class="cls_res_thumb">
		<div class="marg_both">'. get_member_thumbnail($p_arr['ID'], 'left', false, 'relative', false) .
		'</div>
	</div>';
	
	$profileIconPath = getTemplateIcon('action_friends.gif');
	$sendIconPath = getTemplateIcon('action_send.gif');
	
	$miniActionsSect = <<<EOF
		<div class="miniActionCont">
			<img src="$profileIconPath" class="miniAction" alt="View Profile"/>
			<a class="miniAction" href="javascript:void(0)" onclick="needCompleteMsg()">View Profile</a></div>
		<div class="miniActionCont" >
			<img src="$sendIconPath" class="miniAction" alt="Send Message"/>
			<a class="miniAction" href="javascript:void(0)" onclick="needCompleteMsg()">Send Message</a></div>
EOF;

	$lastLogin = '<div class="lastLogin">Last login:<br/>' .
		((!$p_arr['LastLoggedIn'] || $p_arr['LastLoggedIn'] == "0000-00-00 00:00:00") ?
			_t('_never') : $p_arr['LastLoggedIn']) . '</div>';

	return DesignBoxContent($p_arr['NickName'], $thumb . $miniActionsSect . $lastLogin, 
		1, '', '', '', '', 'padding-left:0');
}

function PageCompQuickLinks()
{
	$quickLinkIcon = getTemplateIcon('small_folder9.png');
	$links = <<<EOF
		<div>
			<span class="quickLink">
				<img src="$quickLinkIcon" class="quickLinkImg" alt="Write Blog Post"/>
				<a href="javascript:void(0)" onclick="needCompleteMsg()">Write Blog Post</a>
			</span>
			<span class="quickLink">
				<img src="$quickLinkIcon" class="quickLinkImg" alt="Upload Photos"/>
				<a href="javascript:void(0)" onclick="needCompleteMsg()">Upload Photos</a>
			</span>
			<span class="quickLink">
				<img src="$quickLinkIcon" class="quickLinkImg" alt="Upload Videos"/>
				<a href="javascript:void(0)" onclick="needCompleteMsg()">Upload Videos</a>
			</span>
			<span class="quickLink">
				<img src="$quickLinkIcon" class="quickLinkImg" alt="Customize Profile"/>
				<a href="javascript:void(0)" onclick="needCompleteMsg()">Customize Profile</a>
			</span>									
		</div>
EOF;
	
	return DesignBoxContent(_t('_Quick Links'), $links, 1);	
}

function PageCompFriends()
{
	global $p_arr;
	
	$maxFriends = 4;
	$friends = getFriendList($p_arr['ID'], $maxFriends);
	$friendsNum = getFriendNumber($p_arr['ID']);
	$friendsNumMsg = ($friendsNum == 1) ? 'friend' : 'friends';
	
	$friendsNumLink = "<div class='title_content'>
		<a class=\"title_content_link\" href=\"javascript:void(0)\"
			onclick=\"needCompleteMsg()\">$friendsNum $friendsNumMsg</a></div>";
	
	return DesignBoxContent(_t('_Friends'), $friends, 1, $friendsNumLink, '', '', '', 'padding:10px 0');	
}

?>