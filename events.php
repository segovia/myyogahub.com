<?php

/***************************************************************************
*                            Dolphin Smart Community Builder
*                              -----------------
*     begin                : Mon Mar 23 2006
*     copyright            : (C) 2006 BoonEx Group
*     website              : http://www.boonex.com/
* This file is part of Dolphin - Smart Community Builder
*
* Dolphin is free software. This work is licensed under a Creative Commons Attribution 3.0 License.
* http://creativecommons.org/licenses/by/3.0/
*
* Dolphin is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
* without even the implied warranty of  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the Creative Commons Attribution 3.0 License for more details.
* You should have received a copy of the Creative Commons Attribution 3.0 License along with Dolphin,
* see license.txt file; if not, write to marketing@boonex.com
***************************************************************************/

require_once( 'inc/header.inc.php' );
require_once( BX_DIRECTORY_PATH_INC . 'design.inc.php' );
require_once( BX_DIRECTORY_PATH_INC . 'prof.inc.php' );
require_once( BX_DIRECTORY_PATH_INC . 'utils.inc.php' );
require_once( BX_DIRECTORY_PATH_INC . 'profiles.inc.php' );
require_once( BX_DIRECTORY_PATH_INC . 'sdating.inc.php' );
require_once( BX_DIRECTORY_PATH_INC . 'images.inc.php' );

require_once( BX_DIRECTORY_PATH_CLASSES . 'BxDolEvents.php' );


// --------------- page variables and login

$_page['name_index'] = 55;
$_page['css_name'] = 'sdating.css';
$_page['extra_css'] = $oTemplConfig -> sCalendarCss;
//$_page['extra_js'] = $oTemplConfig -> sTinyMceEditorCompactJS;
$_page['extra_js'] 	= $oTemplConfig -> sTinyMceEditorMiniJS . 
	'<script type="text/javascript">urlIconLoading = "'.getTemplateIcon('loading.gif').'";</script>';

if( $_GET['show_only'] )
{
	$eventID = $_GET['event_id'];
	switch( $_GET['show_only'] )
	{
		case 'sharePhotos':
			$sCaption = 'Event Photos';
			echo PageCompSharePhotosContent($sCaption, 0, $eventID, 'event');
		break;
		case 'shareVideos':
			$sCaption = 'Event Videos';
			echo PageCompShareVideosContent($sCaption, 0, $eventID, 'event');
		break;
	}
	exit;
}

// Check for immediate login on myh from global redirection
if (isset($_GET['logingid']) && isset($_GET['logintime']) && isset($_GET['loginkey']))
{
	$globalid =  superlogin($_GET['logingid'], $_GET['logintime'], $_GET['loginkey']);
	if ($globalid)
	{
		// Check whether user with globalid exists on myyogahub.com
		$memberData = db_assoc_arr("SELECT ID, Password FROM Profiles WHERE globalid = $globalid");
		if ($memberData)
		{
			$memberID = $memberData['ID'];
			$password = $memberData['Password'];
	
			// Clear cookies
			setcookie("memberID", 0, time() - 3600, '/');
			setcookie("memberPassword", '', time() - 3600, '/');
	
			// Do login
			setcookie("memberID", $memberID, 0, '/');
			setcookie("memberPassword", $password, 0, '/');
	
			// Update last loggedin
			db_res("UPDATE Profiles SET LastLoggedIn = NOW() WHERE ID = $memberID");
	
			// Save to session needTrackLeads parameter
			if (!session_id()) session_start();
			$_SESSION['needTrackLeads'] = true;
			
			// Redirect back to requested uri without immediate login params
			$loginGIDPos = strpos($_SERVER['REQUEST_URI'], '&logingid');
			$redirect = ($loginGIDPos) ? substr($_SERVER['REQUEST_URI'], 0, $loginGIDPos) : $_SERVER['REQUEST_URI'];
			
			header('Location: '.$redirect);
			exit;
		}
	}
}

//$logged['member'] = member_auth( 0, false );
check_logged();

$_page['header'] = _t("_sdating_h");
$_page['header_text'] = _t("_sdating_h");

$oEvents = new BxDolEvents();
if ($logged['admin'])
$oEvents->bAdminMode = TRUE;

$_ni = $_page['name_index'];
$_page_cont[$_ni]['page_main_code'] = PageCompPageMainCode();

// if SpeedDating disabled then don't show anything
if ( !$en_sdating ) {
	$_page['name_index'] = 0;
	$_page_cont[0]['page_main_code'] = '';
	PageCode();
	exit();
}

function PageCompPageMainCode() {
	global $site, $oEvents;
	
	$bEventCreating = (getParam('enable_event_creating') == 'on');
	$sRetHtml = '';
	$sRetHtml .= $oEvents -> PrintCommandForms();

	// put in main code appropriate values
	switch ( $_REQUEST['action'] ) {
		case 'edit_event':
			$iEventID = (int)($_POST['EditEventID']);
			$sRetHtml .= $oEvents->PageSDatingNewEventForm($iEventID);
			break;
		case 'event_save':
			$iEventID = (int)($_POST['EditedEventID']);
			//if (isset($_POST['event_save'])) {
			$aPostAdv = $oEvents -> FillPostEventArrByPostValues();
			$aErr = $oEvents -> CheckEventErrors( $aPostAdv );
			if( empty( $aErr ) ) {
				$add_res = $oEvents->SDAddEvent($iEventID);//like update
				$_REQUEST['event_id'] = $iEventID;
				
				// Redirect to edited event for correct referer on logout
				header("Location: {$_SERVER['PHP_SELF']}?action=show_info&event_id=$iEventID");
				//$sRetHtml .= $oEvents->PageSDatingShowInfo();
			} else {
				$sRetHtml .= $oEvents->PageSDatingNewEventForm($iEventID, $aErr);

			}
			//} else {
			//	$sRetHtml .= $oEvents->PageSDatingNewEventForm($iEventID);
			//}
			break;
		case 'delete_event':
			$sRetHtml .= $oEvents->PageSDatingDeleteEvent();
			$sRetHtml .= $oEvents->PageSDatingShowEvents();
			break;
		case 'show':
		case 'search_by_tag':	
			$sRetHtml .= '<div class="cls_info_left">'.$oEvents->PageSDatingFeaturedEvents().
				$oEvents->PageSDatingPopularTags().'</div>';
			$sRetHtml .= '<div class="cls_info">'.$oEvents->PageSDatingShowEvents().'</div>';
			break;
		case 'search':
			//$sRetHtml .= $oEvents->PageSDatingShowForm();
			$sRetHtml .= $oEvents->PageSDatingShowEvents();
			break;
		case 'show_info':	
			if (isset($_GET['event_id']))
			{
				// Check whether event has sef url
				$eventData = db_arr("SELECT ResponsibleID, urltitle FROM SDatingEvents WHERE ID = {$_GET['event_id']}");
				if ($eventData['urltitle'])
				{
					$ownerUsername = getNickName($eventData['ResponsibleID']);
					header("HTTP/1.1 301 Moved Permanently");
					header("Location: {$site['url']}$ownerUsername/event/{$eventData['urltitle']}.html");
				}
			}
			$sRetHtml .= $oEvents->PageSDatingShowInfo();
			break;
		case 'calendar':
			$sRetHtml .= $oEvents->PageSDatingCalendar();
			break;
		case 'show_part':
			$sRetHtml .= $oEvents->PageSDatingShowParticipants();
			break;
		case 'select_match':
			$sRetHtml .= $oEvents->PageSDatingSelectMatches();
			break;
			/*
		case 'search_by_tag':
			$sRetHtml .= $oEvents->ShowSearchResult();
			break;
			*/
		case 'new':
			if ($bEventCreating) {
				if (isset($_POST['event_save'])) {
					$aPostAdv = $oEvents -> FillPostEventArrByPostValues();
					$aErr = $oEvents -> CheckEventErrors( $aPostAdv );
					if( empty( $aErr ) ) {
						$add_res = $oEvents->SDAddEvent();
						
						// Redirect to added event for correct referer on logout
						header("Location: {$_SERVER['PHP_SELF']}?action=show_info&event_id={$_REQUEST['event_id']}");
						//$sRetHtml .= $oEvents->PageSDatingShowInfo();
					} else 
						$sRetHtml .= $oEvents->PageSDatingNewEventForm(-1, $aErr);
				} else
					$sRetHtml .= $oEvents->PageSDatingNewEventForm(-1);
			} else
			$sRetHtml .= '';
			break;
		case 'addcomment':
			$sRetHtml .= $oEvents->ActionAddComment();
			$iEventID = (int)($_POST['CommEventID']);
			$_REQUEST['event_id'] = $iEventID;
			$sRetHtml .= $oEvents->PageSDatingShowInfo();
			break;
		case 'editcomment':
			$sRetHtml .= $oEvents->ActionEditComment();
			$iEventID = (int)($_POST['EEventID']);
			$_REQUEST['event_id'] = $iEventID;
			$sRetHtml .= $oEvents->PageSDatingShowInfo();
			break;
		case 'delete_comment':
			$sRetHtml .= $oEvents->ActionDeleteComment();
			$iEventID = (int)($_POST['DEventID']);
			$_REQUEST['event_id'] = $iEventID;
			$sRetHtml .= $oEvents->PageSDatingShowInfo();
			break;
		default:
			$sRetHtml .= '';
	}
	return $sRetHtml;
}

PageCode();

?>