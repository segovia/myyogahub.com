<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="root">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>
	<xsl:choose>
		<xsl:when test="string-length(/root/page/posts/topic/title) &gt; 0">
			<xsl:value-of select="/root/page/posts/topic/title" /> :: Orca Forum
		</xsl:when>
		<xsl:when test="string-length(/root/page/topics/forum/title) &gt; 0">
			<xsl:value-of select="/root/page/topics/forum/title" /> :: Orca Forum
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="title" />
		</xsl:otherwise>
	</xsl:choose>
</title>	

<xsl:element name="base">
	<xsl:attribute name="href"><xsl:value-of select="base"/></xsl:attribute>
</xsl:element>

<link type="text/css" rel="stylesheet" href="{/root/urls/css}main.css" />
<link type="text/css" rel="stylesheet" href="{/root/urls/css}uni.css" />
<!--
<script language="javascript" type="text/javascript" src="js/util.js"></script>
<script language="javascript" type="text/javascript" src="js/BxError.js"></script>
<script language="javascript" type="text/javascript" src="js/BxXmlRequest.js"></script>
<script language="javascript" type="text/javascript" src="js/BxXslTransform.js"></script>
<script language="javascript" type="text/javascript" src="js/BxForum.js"></script>
<script language="javascript" type="text/javascript" src="js/BxEditor.js"></script>
<script language="javascript" type="text/javascript" src="js/BxHistory.js"></script>
<script language="javascript" type="text/javascript" src="js/BxLogin.js"></script>
<xsl:if test="1 = /root/logininfo/admin"><script language="javascript" type="text/javascript" src="js/BxAdmin.js"></script></xsl:if>
-->
<script language="javascript" type="text/javascript" src="js/loader.php"></script>
<script language="javascript" type="text/javascript" src="../../plugins/tiny_mce/tiny_mce_gzip.js"></script>
<script language="javascript" type="text/javascript" src="{/root/url_dolphin}inc/js/functions.js"></script>
</head>
<xsl:element name="body">
	<xsl:attribute name="onload">h = new BxHistory(); document.h = h; return h.init('h'); </xsl:attribute>

    <script type="text/javascript">
        tinyMCE_GZ.init({
        	plugins : 'table,advimage,advlink,emotions,iespell,insertdatetime,preview,zoom,searchreplace,print,contextmenu',
        	themes : 'advanced',
        	languages : 'en',
        	disk_cache : true,
        	debug : false
        });
    </script>



	<script language="javascript" type="text/javascript">

		function orcaSetupContent (id, body, doc) {	}

		tinyMCE.init({
            entity_encoding : "raw",
			mode : "exact",
			elements : "tinyEditor",
			theme : "advanced",
			gecko_spellcheck : true,
			content_css : "<xsl:value-of select="/root/urls/css" />blank.css",

			remove_linebreaks : true,

			setupcontent_callback : "orcaSetupContent",

			plugins : "table,advimage,advlink,emotions,iespell,insertdatetime,preview,zoom,searchreplace,print,contextmenu",
			theme_advanced_buttons1_add : "fontsizeselect,separator,forecolor,backcolor",
			theme_advanced_buttons2_add : "separator,insertdate,inserttime,preview,zoom",
			theme_advanced_buttons2_add_before: "cut,copy,paste,separator,search,replace,separator",
			theme_advanced_buttons3_add_before : "tablecontrols,separator",
			theme_advanced_buttons3_add : "emotions,iespell,flash,separator,print",
			theme_advanced_disable : "charmap",
			theme_advanced_toolbar_location : "top",
			theme_advanced_toolbar_align : "left",
			theme_advanced_path_location : "bottom",
			plugin_insertdate_dateFormat : "%Y-%m-%d",
			plugin_insertdate_timeFormat : "%H:%M:%S",
			extended_valid_elements : "a[name|href|target|title|onclick],img[class|src|border=0|alt|title|hspace|vspace|width|height|align|onmouseover|onmouseout|name],hr[class|width|size|noshade],font[face|size|color|style],span[class|align|style]"
			});


		var urlXsl = '<xsl:value-of select="/root/urls/xsl" />';
		var urlImg = '<xsl:value-of select="/root/urls/img" />';
        var defTitle = '<xsl:value-of select="/root/title" />';
        var isLoggedIn = '<xsl:value-of select="/root/logininfo/username" />'.length ? true : false;

        var xsl_mode = '<xsl:value-of select="/root/urls/xsl_mode" />';

		var f = new Forum ('<xsl:value-of select="base"/>', <xsl:value-of select="min_point"/>);
		document.f = f;
		var orca_login = new Login ('<xsl:value-of select="base"/>', f);
		document.orca_login = orca_login;
		<xsl:if test="1 = /root/logininfo/admin">
			var orca_admin = new Admin ('<xsl:value-of select="base"/>', f);
			document.orca_admin = orca_admin;
		</xsl:if>
	</script>


    <!-- RAY INTEGRATION [BEGIN] -->

    <xsl:if test="1 = /root/logininfo/ray_on">
    
    <div id="FloatDesc" style="position:absolute;display:none;z-index:100;"></div>

    <script src="{/root/url_dolphin}ray/modules/im/js/invite.js" type="text/javascript" language="javascript"></script>

    <div id="flcontainer" name="flcontainer" style="position:absolute;top:-9999px;left:0px;width:300px;height:200px;z-index:90;">

        <object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=8,0,0,0" width="300" height="200" id="ray_invite_object" align="middle">
            <param name="allowScriptAccess" value="always" />
            <param name="movie" value="{/root/url_dolphin}ray/modules/global/app/holder.swf" />
            <param name="quality" value="high" />
            <param name="base" value="{/root/url_dolphin}ray/modules/im/" />
            <param name="FlashVars" value="module=im&amp;app=invite&amp;id=3&amp;password=aac77443df77f44d22ffca1f5d2b87c0&amp;url={/root/url_dolphin}ray/XML.php" />
            <embed
                    id="ray_invite_embed"
                    name="ray_invite"
                    src="{/root/url_dolphin}ray/modules/global/app/holder.swf"
                    quality="high"
                    bgcolor="#ffffff"
                    width="300" height="200"
                    align="middle"
                    allowScriptAccess="always"
                    base="{/root/url_dolphin}ray/modules/im/"
                    FlashVars="module=im&amp;app=invite&amp;id={/root/logininfo/ray_id}&amp;password={/root/logininfo/ray_pwd}&amp;url={/root/url_dolphin}ray/XML.php"
                    pluginspage="http://www.macromedia.com/go/getflashplayer" />
        </object>

    </div>

    </xsl:if>

    <!-- RAY INTEGRATION [ END ] -->







<div class="main2">

    <!-- top -->
    <div style="padding:0px 8px 0px 11px">
        <div class="topestMenuWrapper">

			<div class="topestMenu">
                <a class="menu_item_link" href="{/root/url_dolphin}links.php">Links</a>
				<a class="menu_item_link" href="{/root/url_dolphin}news.php">News</a>
				<a class="menu_item_link" href="{/root/url_dolphin}contact.php">Contact Us</a>
				<a class="menu_item_link" href="{/root/url_dolphin}about_us.php">About us</a>
				<a class="menu_item_link" href="{/root/url_dolphin}privacy.php">Privacy Policy</a>

				<a class="menu_item_link" href="{/root/url_dolphin}terms_of_use.php">Terms</a>
				<a class="menu_item_link" href="{/root/url_dolphin}faq.php">FAQ</a>
				<a class="menu_item_link" href="{/root/url_dolphin}story.php">Add feedback</a>
				<a class="menu_item_link" href="{/root/url_dolphin}affiliates.php">Affiliates</a>
				<a class="menu_item_link" href="javascript:void(0);" onclick="addBookmark( '{/root/url_dolphin}', 'Dolphin6' );">Bookmark</a>

			</div>
		</div>
		
		<div class="topBlock">
			<xsl:copy-of select="url_logo" />
            <div class="topMemberBlock">

		<xsl:choose>
			<xsl:when test="string-length(logininfo/username) &gt; 0">

                <div class="thumbnail_block" style="float:right; ">
                    <a href="{/root/logininfo/profile_url}" onclick="{/root/logininfo/profile_onclick}"><img src="{/root/urls/img}sp.gif" style="width:45px;height:45px;background-image:url({/root/logininfo/avatar});" alt="" /></a>
                </div>
                <div class="hello_member">
                        Hello, <b><xsl:value-of select="logininfo/username" /></b>!
                </div>
                <div class="hello_actions">

                    <xsl:choose>
                            <xsl:when test="1 = /root/logininfo/admin">
                                <span><a href="{/root/url_dolphin}admin/index.php">Admin Panel</a></span>
                                <span><a href="{/root/url_dolphin}logout.php?action=admin_logout">Log Out</a></span>
                            </xsl:when>
                            <xsl:otherwise>
                                <span><a href="{/root/url_dolphin}member.php">My Account</a></span>
                                <span><a href="{/root/url_dolphin}mail.php?mode=inbox">My Mail</a></span>
                                <span><a href="{/root/url_dolphin}{logininfo/username}">My Profile</a></span>                                
                                <span><a href="{/root/url_dolphin}logout.php?action=member_logout">Log Out</a></span>
                            </xsl:otherwise>
                    </xsl:choose>

                </div>

		    </xsl:when>
            <xsl:otherwise>

                <div class="no_hello_actions">
    				<a href="{/root/url_dolphin}join_form.php">Join Now</a>
				    <a href="{/root/url_dolphin}member.php">Member Login</a>
                </div>
                
			</xsl:otherwise>
        </xsl:choose>                    

            </div>
		</div>
		
		<div class="topMenuWrapper">
			
				<div class="topMenu">
                    <table class="topMenuCont" cellpadding="0" cellspacing="0">
		<script language="javascript" type="text/javascript">
            currentTopItem = <xsl:value-of select="mainmenu/mm['1' = Active]/id" />; 
		</script>                        
                        <tr>
                            <xsl:for-each select="mainmenu/mm">
                                <td>
                                    <xsl:attribute name="class">topMenuItem<xsl:if test="'1' = Active">Active</xsl:if></xsl:attribute>
                                    <xsl:if test="'1' != Active">
                                        <xsl:attribute name="onmouseover">this.className = 'topMenuItemHover'; holdHiddenMenu = <xsl:value-of select="id" />; showHiddenMenu(<xsl:value-of select="id" />);</xsl:attribute>
                                        <xsl:attribute name="onmouseout">this.className = 'topMenuItem'; holdHiddenMenu = currentTopItem; hideHiddenMenu(<xsl:value-of select="id" />);</xsl:attribute>
                                    </xsl:if>
                                    <div class="topMenuItemCont">
                                        <a href="{/root/url_dolphin}{Link}" onclick="{Onclick}" target="{Target}" class="menu_item_link" ><xsl:value-of select="Name" /></a>
                                    </div>
                                </td>
                            </xsl:for-each>
                        </tr>
					</table>

                </div>

                <xsl:for-each select="allsubmenu/mm">
                <div class="hiddenMenu" id="hiddenMenu_{id}"
                    onmouseover="holdHiddenMenu = {id};"
                    onmouseout="holdHiddenMenu = currentTopItem; hideHiddenMenu({id})">
                    <xsl:attribute name="style">                                        
                        <xsl:if test="id = ../../mainmenu/mm['1' = Active]/id">display:block;</xsl:if>
                        <xsl:if test="id != ../../mainmenu/mm['1' = Active]/id">display:none;</xsl:if>
                    </xsl:attribute>
                    <div class="hiddenMenuBgCont">
                    <div class="hiddenMenuCont">
                        <div class="topPageHeader"><xsl:value-of select="Name" /></div>
                        <div class="topCustomMenu">
                            <xsl:for-each select="submenu/sm">
                                <a class="customMenuItem" href="{/root/url_dolphin}{Link}" onclick="{Onclick}" target="{Target}"><xsl:value-of select="Name" /></a> 
                            </xsl:for-each>
                        </div>
                    </div>
                </div>
                    <div class="clear_both"></div>
                </div>
                </xsl:for-each>

				</div>

    </div>

	<!-- end of top -->

    <div class="rightNavDiv">
        &#160;
                </div>


	<div id="content">	

				<div id="main">

						<xsl:if test="not(string-length(page/onload))">
						<xsl:apply-templates select="page" />
						</xsl:if>

				</div>
			

<!-- bottom -->
		<div class="bottomBlock">


			<div class="clear_both"></div>
			
			<div class="bottomImages">
				<img src="{/root/url_dolphin}templates/tmpl_uni/images/small_dol.png" alt="" title="" class="dolphinLogo" />
				<img src="{/root/url_dolphin}templates/tmpl_uni/images/small_orca.png" alt="" title="" class="dolphinLogo" />
				<img src="{/root/url_dolphin}templates/tmpl_uni/images/small_ray.png" alt="" title="" class="dolphinLogo" />
			</div>
			
			<div class="bottomTexts">

				<div class="bottomLinks">
				<span class="btmLinks"><a class="bottommenu" href="{/root/url_dolphin}about_us.php">About us</a></span>
				<span class="btmLinks"><a class="bottommenu" href="{/root/url_dolphin}contact.php">Contact Us</a></span>

				<span class="btmLinks"><a class="bottommenu" href="{/root/url_dolphin}privacy.php">Privacy Policy</a></span>
				<span class="btmLinks"><a class="bottommenu" href="{/root/url_dolphin}terms_of_use.php">Terms</a></span>
				<span class="btmLinks"><a class="bottommenu" href="{/root/url_dolphin}faq.php">FAQ</a></span>
				<span class="btmLinks"><a class="bottommenu" href="{/root/url_dolphin}affiliates.php">Affiliates</a></span>
				<span class="btmLinks"><a class="bottommenu" href="{/root/url_dolphin}links.php">Links</a></span>
				</div>
				
				<div class="bottomPowered">Powered by <a href="http://www.boonex.com/products/dolphin/">Dolphin Smart Community Builder</a> &#160; <a href="http://www.boonex.com/products/orca/">Orca Interactive Forum Script</a> &#160; <a href="http://www.boonex.com/products/ray/">Ray Community Widget Suite</a></div>
				
				<div class="bottomCopyright">Copyright &#169; 2007 Your Company here.</div>

                </div>				
			
			<div class="clear_both"></div>
		</div>
	<!-- end of bottom -->


	</div>

</div>

	<script language="javascript" type="text/javascript">
		correctPNG ('f_head');
	</script>

</xsl:element>
</html>
</xsl:template>

</xsl:stylesheet>
