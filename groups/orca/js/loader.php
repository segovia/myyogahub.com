<?php
/**
*                            Orca Interactive Forum Script
*                              ---------------
*     Started             : Mon Mar 23 2006
*     Copyright           : (C) 2007 BoonEx Group
*     Website             : http://www.boonex.com
* This file is part of Orca - Interactive Forum Script
* GPL
**/


chdir ('..');

require_once ('./inc/header.inc.php');

require_once ($gConf['dir']['classes'].'BxJsGzipLoader.php');

new BxJsGzipLoader ('d', $gConf['dir']['base'] . 'js/', '', $gConf['dir']['cache']);

?>
