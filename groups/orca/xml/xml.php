<?php
/**
*                            Orca Interactive Forum Script
*                              ---------------
*     Started             : Mon Mar 23 2006
*     Copyright           : (C) 2007 BoonEx Group
*     Website             : http://www.boonex.com
* This file is part of Orca - Interactive Forum Script
* GPL
**/


/**
 *
 * Add xml contents to whole xml output 
 * put xml content to $integration_xml variable
 *******************************************************************************/

if( !@eval( file_get_contents( BX_DIRECTORY_PATH_INC . 'menu_content.inc.php' ) ) )
{
	if( basename( dirname( $_SERVER['PHP_SELF'] ) ) != $admin_dir )
	{
		$aTopMenu = array();
		$aMenu    = array();
		echo '<b>Warning:</b> Please rebuild the menu from Admin Panel -> Builders -> Navigation Menu Builder';
	}
}


function getMainLogo()
{
    global $dir;
    global $site;

    $ret = '';

    foreach( array( 'gif', 'jpg', 'png' ) as $ext )
        if( file_exists( $dir['mediaImages']."logo.$ext" ) )
        {
            $ret .= '<a href="' . $site['url'] . '">';
                $ret .= "<img src=\"{$site['mediaImages']}logo.$ext\" class=\"mainLogo\" alt=\"logo\" />";
            $ret .= '</a>';
        }
    return $ret;
}

function getMenuLink ($s)
{
    $a = explode("|", $s);
    $s = str_replace('&amp;', '&', $a[0]);
    return str_replace('&', '&amp;', $s);
}

// check if user logged in

$iLoggedInId = 0;
if ($li && is_array($li) && isset($li['username']) && $li['username'] && $li['ray_id'])
    $iLoggedInId = $li['ray_id'];


$sNiddle = $iLoggedInId ? 'memb' : 'non';

// build main menu array

$aTopMenuOrca = array();
for (reset($aTopMenu) ; list ($k, $a) = each ($aTopMenu) ; )
{
    if ($a['Type'] != 'top') continue; 
    if (false === strstr($a['Visible'], $sNiddle)) continue;

    $a['Link'] = getMenuLink ($a['Link']);
    $a['Name'] = substr($a['Caption'], 1);
    $a['Active'] = 0;
    $a['id'] = $k;

    $aTopMenuOrca[$k] = $a;
}

$iActiveMenuId = 22;
$aTopMenuOrca[$iActiveMenuId]['Active'] = 1;

// build active submenu array

$aSubMenuOrca = array();
for (reset($aTopMenu) ; list ($k, $a) = each ($aTopMenu) ; )
{
    if ($a['Type'] != 'custom') continue;
    if ($a['Parent'] != $iActiveMenuId) continue;    
    if (false === strstr($a['Visible'], $sNiddle)) continue;

    $a['Link'] = getMenuLink ($a['Link']);
    $a['Name'] = substr($a['Caption'], 1);
    $a['Active'] = 0;

    $aSubMenuOrca[$k] = $a;
}


// build other submenus array 
$aAllSubMenuOrca = array ();
foreach ($aTopMenu as $k => $a)
{
    if ($a['Type'] != 'top') continue; 
    if (false === strstr($a['Visible'], $sNiddle)) continue;
    $aAllSubMenuOrca[$k] = array ('id' => $k, 'Name' => substr($a['Caption'], 1), 'submenu' => array());

    $aTmpSubMenu = array();
    for (reset($aTopMenu) ; list ($kk, $aa) = each ($aTopMenu) ; )
    {
        if ($aa['Type'] != 'custom') continue;
        if ($aa['Parent'] != $k) continue;    
        if (false === strstr($aa['Visible'], $sNiddle)) continue;

        $aa['Link'] = getMenuLink ($aa['Link']);
        $aa['Name'] = substr($aa['Caption'], 1);
        $aa['Active'] = 0;
        $aTmpSubMenu[] = $aa;
    }    

    $aAllSubMenuOrca[$k]['submenu'] = array2xml($aTmpSubMenu, 'sm');    
}

global $site;
$integration_xml  = '<url_logo>' . getMainLogo() . '</url_logo>';
$integration_xml .= '<url_dolphin>' . $site['url'] . '</url_dolphin>';
$integration_xml .= '<mainmenu>' . array2xml ($aTopMenuOrca, 'mm') . '</mainmenu>';
$integration_xml .= '<submenu>' . array2xml ($aSubMenuOrca, 'mm') . '</submenu>';
$integration_xml .= '<allsubmenu>' . array2xml ($aAllSubMenuOrca, 'mm') . '</allsubmenu>';
?>
