<?



/***************************************************************************

*                            Dolphin Smart Community Builder

*                              -----------------

*     begin                : Mon Mar 23 2006

*     copyright            : (C) 2006 BoonEx Group

*     website              : http://www.boonex.com/

* This file is part of Dolphin - Smart Community Builder

*

* Dolphin is free software. This work is licensed under a Creative Commons Attribution 3.0 License. 

* http://creativecommons.org/licenses/by/3.0/

*

* Dolphin is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;

* without even the implied warranty of  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

* See the Creative Commons Attribution 3.0 License for more details. 

* You should have received a copy of the Creative Commons Attribution 3.0 License along with Dolphin, 

* see license.txt file; if not, write to marketing@boonex.com

***************************************************************************/

if (!session_id()) session_start();

require_once( 'inc/header.inc.php' );

require_once( BX_DIRECTORY_PATH_INC . 'design.inc.php' );

require_once( BX_DIRECTORY_PATH_INC . 'profiles.inc.php' );

require_once( BX_DIRECTORY_PATH_INC . 'utils.inc.php' );



require_once( BX_DIRECTORY_PATH_CLASSES . 'BxDolBlogs.php' );

require_once( BX_DIRECTORY_PATH_ROOT . "templates/tmpl_{$tmpl}/scripts/BxTemplProfileView.php" );



// --------------- page variables and login

$ownerID = 0;
if (isset($_GET['sefNick']))
{
  $_GET['ownerID'] = getID($_GET['sefNick'], 0);
  $_REQUEST['ownerID'] = $_GET['ownerID'];
}
if (isset($_GET['ownerID']))
  $ownerID = $_GET['ownerID'];
	
if ($ownerID)
{
	$oProfile = new BxTemplProfileView($ownerID);
	$_page['extra_css'] = $oProfile -> genProfileCSS($ownerID);
}

$_page['name_index']	= 49;

$_page['css_name']		= 'blogs.css';



check_logged();



$oBlogs = new BxDolBlogs(FALSE);



$_ni = $_page['name_index'];
$_page_cont[$_ni]['gui_message'] = guiMessage();
$_page_cont[$_ni]['page_main_code'] = PageCompBlogs();

$_page['extra_js'] = $oTemplConfig -> sTinyMceEditorMiniJS;

$_page['header'] = $oBlogs->GetHeaderString();
$_page['header_text'] = $oBlogs->GetHeaderString();

function PageCompBlogs() {
  global $site;
	global $date_format;
	global $oBlogs;
	global $meta_keywords;
	global $meta_description;
	global $title;
	global $ownerID;
	
	$sRetHtml = '';
	$sRetHtml .= $oBlogs->GenCommandForms();

	switch ( $_REQUEST['action'] ) {
		//print functions
		case 'top_blogs':
			$sRetHtml .= $oBlogs->GenBlogLists('top');
			break;

		case 'show_member_blog':
			$sRetHtml .= $oBlogs->GenMemberBlog();
			break;

		case 'top_posts':
			$sRetHtml .= $oBlogs->GenPostLists('top');
			break;

		case 'new_post':
			$sRetHtml .= $oBlogs->AddNewPostForm();
			break;

		case 'show_member_post':

			// Is redirect from SEF url
			if (isset($_GET['sefNick']) && isset($_GET['sefTitle']) && $ownerID > 0)
			{
				$postID = db_value("SELECT `BlogPosts`.`PostID` FROM `BlogPosts`
					LEFT JOIN `BlogCategories` USING (`CategoryID`)
					WHERE `BlogCategories`.`OwnerID` = $ownerID
						AND `BlogPosts`.`urltitle` = '{$_GET['sefTitle']}'
					LIMIT 1");
				$sRetHtml .= $oBlogs->GenPostPage($postID);
			}
			else
				$sRetHtml .= $oBlogs->GenPostPage();
			break;

		case 'search_by_tag':
		case 'search':
			
			// Is redirect from SEF url
			if (isset($_GET['sefTag']) && isset($_GET['sefNick']) && $ownerID > 0)
				$sRetHtml .= $oBlogs->GenSearchResult($_GET['sefTag'], $ownerID);
			else
				$sRetHtml .= $oBlogs->GenSearchResult();
			break;

		//forms of editing
		case 'add_category':
			$sRetHtml .= $oBlogs->GenEditCategoryForm();
			break;

		case 'edit_category':
			$iCategoryID = (int)($_REQUEST['categoryID']);
			$sRetHtml .= $oBlogs->GenEditCategoryForm($iCategoryID);
			break;

		case 'edit_post':
			$iPostID = (int)($_POST['EditPostID']);
			$sRetHtml .= $oBlogs->AddNewPostForm($iPostID);
			break;

		//non safe functions
		case 'create_blog':
			$sRetHtml .= $oBlogs->ActionCreateBlog();
			break;

		case 'edit_blog':
			$sRetHtml .= $oBlogs->ActionEditBlog();
			$iBlogID = (int)($_POST['EditBlogID']);
			$iOwnerID = (int)($_REQUEST['EOwnerID']);
			$sRetHtml .= $oBlogs->GenMemberBlog($iOwnerID);
			break;

		case 'delete_blog':
			$sRetHtml .= $oBlogs->ActionDeleteBlogSQL();
			$sRetHtml .= $oBlogs->GenBlogLists('last');
			break;

		case 'addcategory':
			$sRetHtml .= $oBlogs->ActionUpdateCategory();
			$iOwnerID = (int)($_REQUEST['OwnerID']);
			$_REQUEST['category'] = mysql_insert_id();
			$sRetHtml .= $oBlogs->GenMemberBlog($iOwnerID);
			break;

		case 'editcategory':
			$sRetHtml .= $oBlogs->ActionUpdateCategory(TRUE);
			$iOwnerID = (int)($_REQUEST['OwnerID']);
			$_REQUEST['category'] = mysql_insert_id();
			$sRetHtml .= $oBlogs->GenMemberBlog($iOwnerID);
			break;

		case 'delete_category':
			$sRetHtml .= $oBlogs->ActionDeleteCategory();
			$iOwnerID = (int)($_REQUEST['OwnerID']);
			$sRetHtml .= $oBlogs->GenMemberBlog($iOwnerID);
			break;

		case 'add_post':
			$arrPostAdv = $oBlogs->GetPostArrByPostValues();
			$arrErr = $oBlogs->GetCheckErrors($arrPostAdv);
			if( empty( $arrErr ) ) {
				$iLastID = -1;
				$sRetHtml .= $oBlogs->ActionAddNewPost($iLastID);
				$_REQUEST['post_id'] = $iLastID;
				$sRetHtml .= $oBlogs->GenPostPage();
			} else
				$sRetHtml .= $oBlogs->AddNewPostForm(-1, $arrErr);
			break;

		case 'post_updated':
			$iPostID = (int)($_POST['EditedPostID']);
			$arrPostAdv = $oBlogs->GetPostArrByPostValues();
			$arrErr = $oBlogs -> GetCheckErrors($arrPostAdv);
			if( empty( $arrErr ) ) {
				$sRetHtml .= $oBlogs->ActionEditPost();
				$_REQUEST['post_id'] = $iPostID;
				$sRetHtml .= $oBlogs->GenPostPage();
			} else
				$sRetHtml .= $oBlogs->AddNewPostForm($iPostID, $arrErr);
			break;

		case 'delete_post':
			$sRetHtml .= $oBlogs->ActionDeletePost();
			$iPostID = (int)($_POST['DeletePostID']);
			$_REQUEST['ownerID'] = $oBlogs->aBlogConf['visitorID'];
			$sRetHtml .= $oBlogs->GenMemberBlog();
			break;

		case 'addcomment':
			$sRetHtml .= $oBlogs->ActionAddBlogComment();
			$iPostID = (int)($_POST['CommPostID']);
			$iOwnerID = (int)($_GET['ownerID']);
			$_REQUEST['post_id'] = $iPostID;
			$sRetHtml .= $oBlogs->GenPostPage();
			break;

		case 'editcomment':
			$sRetHtml .= $oBlogs->ActionEditComment();
			$iPostID = (int)($_POST['EPostID']);
			$iOwnerID = (int)($_GET['ownerID']);
			$_REQUEST['post_id'] = $iPostID;
			$sRetHtml .= $oBlogs->GenPostPage();
			break;

		case 'delete_comment':
			$sRetHtml .= $oBlogs->ActionDeleteComment();
			$iPostID = (int)($_POST['DPostID']);
			$iOwnerID = (int)($_GET['ownerID']);
			$_REQUEST['post_id'] = $iPostID;
			$sRetHtml .= $oBlogs->GenPostPage();
			break;

		default:
			$sRetHtml .= $oBlogs->GenBlogLists('last');
			break;
	}

	return $sRetHtml;

}



PageCode();



?>