<?php
/***************************************************************************
*                            Orca Interactive Forum Script
*                              ---------------
*     Started             : Fr Nov 10 2006
*     Copyright        : (C) 2007 BoonEx Group
*     Website             : http://www.boonex.com
* This file is part of Orca - Interactive Forum Script
*
* Orca is free software; you can redistribute it and/or modify it under 
* the terms of the GNU General Public License as published by the 
* Free Software Foundation; either version 2 of the 
* License, or any later version.      
*
* Orca is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
* without even the implied warranty of  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details. 
* You should have received a copy of the GNU General Public License along with Orca, 
* see license.txt file; if not, write to marketing@boonex.com
***************************************************************************/


// util functions  

/**
 * Output XML or make XSL transformation and output ready HTML
 * @param $code		XML code
 * @param $xsl		file name
 * @param $trans	make xsl transformation or not
 */
function transCheck ($xml, $xsl, $trans, $browser_transform = 0)
{
	if ($trans)
	{
		$now = gmdate('D, d M Y H:i:s') . ' GMT';
		header("Expires: $now");
		header("Last-Modified: $now");
		header("Cache-Control: no-cache, must-revalidate");
		header("Pragma: no-cache");

		$xslt = new BxXslTransform ($xml, $xsl, BXXSLTRANSFORM_SF);
		$xslt->setHeader ('Content-Type: text/html');
        $s = $xslt->process ();
        $s = str_replace('<'.'?xml version="1.0" encoding="UTF-8"?'.'>', '', $s);
        echo $s;
	}
	else
	{
		header ('Content-Type: application/xml');
        echo '<' . '?xml version="1.0" encoding="UTF-8"?' . '>';
        if ($browser_transform)
        {
        	global $gConf;
        	echo '<' . '?xml-stylesheet type="text/xml" href="'.str_replace($gConf['dir']['xsl'],$gConf['url']['xsl'],$xsl).'"?>';
        }
		echo $xml;
	}
}


/**
 * Convert array to XML format
 *
 * @param $arr	array with data
 * @param $tag	main tag <main tag>XML data</main tag>
 * @return XML presentation of data
 */
function array2xml($arr, $tag = false)
{
	$res = '';
	foreach($arr as $k=>$v)
	{
		if(is_array($v))
		{
			if(!is_numeric($k) && trim($k))//
				$res .= count($v) ? '<'.$k.'>'.array2xml($v).'</'.$k.'>' : '<'.$k.'/>';
			elseif($tag)
				$res .= '<'.$tag.'>'.array2xml($v).'</'.$tag.'>';
			else
				$res .= array2xml($v);
		}
		else
		{
			if(!is_numeric($k) && trim($k))//
				$res .= strlen(trim($v)) ? '<'.$k.'>'.$v.'</'.$k.'>' : '<'.$k.'/>';//'<'.$k.'>'.$v.'</'.$k.'>';
			elseif($tag)
				$res .= '<'.$tag.'>'.$v.'</'.$tag.'>';//trim($v) ? '<'.$tag.'>'.$v.'</'.$tag.'>' : '<'.$tag.'/>';
			else
			{
				echo 'Error: array without tag';	
				exit;
			}
		}
	}
	return  $res;
}


/**
 * check if magick quotes is disables
 */
function checkMagicQuotes ()
{
	if (0 == get_magic_quotes_gpc())
	{
		addSlashesArray ($_COOKIE);
		addSlashesArray ($_GET);
		addSlashesArray ($_POST);
	}
}

/**
 * add slashes to every value of array 
 */
function addSlashesArray (&$a)
{
	for ( reset ($a); list ($k, $v) = each ($a);  )	
	{
		if (is_array($v))
			addSlashesArray ($v);
		else
			$a[$k] = addslashes ($v);
	}
}


function prepare_to_db(&$s, $iAllowHTML = 1)
{
	if ($iAllowHTML)
		cleanPost($s);
}

	/**
	 * check html message, remove unknown tags, chech for xhtml errors
	 */
	function cleanPost (&$s)
	{

        function makeStyle ($s)
		{
			global $gConf;

            if ($s == ">") return '';

            $style = '';
			$ret = '';

            if (preg_match("/\bbold\b/", $s)) $style .= "font-weight:bold;";
            if (preg_match("/\bunderline\b/", $s)) $style .= "text-decoration:underline;";
            if (preg_match("/\bitalic\b/", $s)) $style .= "font-style:italic;";
            if (preg_match("/\bmargin-left[:\s]+([0-9a-z]+)/", $s, $m)) $style .= "margin-left:{$m[1]};";			
			
			if (preg_match("/\bhref=\\\\\\\\\"([0-9A-Za-z:@_\.\/?=&;-]+)/", $s, $m)) 
				$ret = " " . (preg_match ('#^' . $gConf['url']['base'] . '#', $m[1]) ? "" : "target=\"_blank\"") . " href=\"{$m[1]}\"";

			if (preg_match("/\bsrc=\\\\\\\\\"([0-9A-Za-z:@_\.\/?=&;-]+)/", $s, $m)) 
				$ret = " src=\"{$m[1]}\" /";

            if ($style) $ret .= " style=\"$style\"";

            return $ret;
        }


		$s = str_replace ("&nbsp;", "&#160;", $s);

//		$s = preg_replace("/(<\/?)(\w+)([^>]*>)/e", "'\\1'.strtoupper('\\2').makeStyle('\\3').'>'", $s);  // clean tags

//		$s = str_replace (array ("&apos;",'&quot;'), array("\'",'"'), $s);  

//		$s = str_replace (array("&nbsp;", "&lt;", "&gt;"), array("-=nbsp=-", "-=lt=-", "-=gt=-"), $s);

//		$s = str_replace ("&amp;", "&", $s);
//		$s = str_replace ("&", "&amp;", $s);

//		$s = str_replace (array ('-=nbsp=-','-=lt=-','-=gt=-','<BR>','<HR>','<IMG>', '<INPUT>'), array('&#160;','&lt;','&gt;','<BR />','<HR />','',''), $s);
		
		$s = strip_tags ($s, '<span><br><pre><ul><ol><li><div><p><strong><em><u><strike><blockquote><a><img><address><font><sup><sub><table><tbody><tr><td><hr><H1><H2><H3><H4><H5><H6>');
		/*
		if (((int)phpversion()) >= 5)		
		{
			$doc = new DOMDocument();
			if (!@$doc->loadXML('<neverhood>' . $s . '</neverhood>'))
				$s = strip_tags ($s);		
		}
		else 
		{
			if (!@domxml_open_mem('<neverhood>' . $s . '</neverhood>'))
				$s = strip_tags ($s);		
		}	
		 */
	}

    function encode_post_text (&$s, $wp = 0, $utf8_decode = 0)
    {
        global $gConf;

        $s = "<![CDATA[{$s}]]>";
/*        
        if (($wp || $gConf['xsl_mode'] == 'server') && $gConf['xsl_mode'] != 'client')
        {        	        
            $s = "<![CDATA[{$s}]]>";
        }
        else
        {                 

            if (!function_exists('spec_callback'))
            {
                function spec_callback ($m)
                {
                    global $gEnt;
                    switch ($m[1])
                    {
                    case '&quot;':
                    case '&lt;':
                    case '&gt;':
                    case '&amp;':
                        return $m[1];
                    }
                    $s = html_entity_decode ($m[1]);
                    if ($s == $m[1])
                    {
                        if (array_key_exists($m[1],$gEnt))
                            $s = $gEnt[$m[1]];
                        else
                            $s = ' ';
                    }
                    return $s;
                }
            }
            $s = utf8_encode(preg_replace_callback ('/(&[A-Za-z]+;)/', 'spec_callback', $s));
        }
*/        
        if ($utf8_decode) 
        {        	
        	$s = utf8_decode ($s);
        }
        
    }
    	
?>
