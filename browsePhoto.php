<?php

/***************************************************************************
*                            Dolphin Smart Community Builder
*                              -----------------
*     begin                : Mon Mar 23 2006
*     copyright            : (C) 2006 BoonEx Group
*     website              : http://www.boonex.com/
* This file is part of Dolphin - Smart Community Builder
*
* Dolphin is free software. This work is licensed under a Creative Commons Attribution 3.0 License. 
* http://creativecommons.org/licenses/by/3.0/
*
* Dolphin is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
* without even the implied warranty of  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the Creative Commons Attribution 3.0 License for more details. 
* You should have received a copy of the Creative Commons Attribution 3.0 License along with Dolphin, 
* see license.txt file; if not, write to marketing@boonex.com
***************************************************************************/

require_once('inc/header.inc.php');
require_once( BX_DIRECTORY_PATH_INC . 'design.inc.php' );
require_once( BX_DIRECTORY_PATH_INC . 'admin.inc.php' );
require_once( BX_DIRECTORY_PATH_INC . 'images.inc.php' );
require_once( BX_DIRECTORY_PATH_INC . 'sharing.inc.php' );
require_once( BX_DIRECTORY_PATH_ROOT . "templates/tmpl_{$tmpl}/scripts/BxTemplProfileView.php" );

$userID = 0;
if (isset($_GET['userID']))
	$userID = (int)$_GET['userID'];
elseif (isset($_GET['sefNick']))
	$userID = getID($_GET['sefNick'], 0);

if ($userID)
{
	$oProfile = new BxTemplProfileView($userID);
	$_page['extra_css'] = $oProfile -> genProfileCSS($userID);
}

$_page['name_index']	= 82;
$_page['css_name']		= 'viewPhoto.css';

$oVotingView = new BxTemplVotingView('gphoto', 0, 0);
$_page['extra_js'] 	= $oVotingView->getExtraJs();

if ( !( $logged['admin'] = member_auth( 1, false ) ) )
{
	if ( !( $logged['member'] = member_auth( 0, false ) ) )
	{
		if ( !( $logged['aff'] = member_auth( 2, false ) ) )
		{
			$logged['moderator'] = member_auth( 3, false );
		}
	}
}


$_page['header'] = _t( "_browsePhoto" );
$searchSect = <<<EOF
<div class="caption_item">
	<form name="PhotosSearchForm" action="{$_SERVER['PHP_SELF']}" method="get">
	<input type="text" name="search" size="23" value="{$_GET['search']}"/>
	<input type="submit" value="Search"/>
</form></div>
EOF;
$headerText = (isset($_GET['search'])) ? 'Search Results for "'.$_GET['search'].'"'
	: _t("_browsePhoto");
$_page['header_text'] = '<div style="float:left">'.$headerText.'</div>'.$searchSect;

$_ni = $_page['name_index'];

$member['ID'] = (int)$_COOKIE['memberID'];

$aWhere = array();
$aWhere[] = '1';

if ($userID)
	$aWhere[] = "`sharePhotoFiles`.`medProfId`=$userID";   
elseif (isset($_GET['eventID']))
{
	$iEventID = (int)$_GET['eventID'];
	$aWhere[] = "`sharePhotoFiles`.`AssocID`=$iEventID AND `sharePhotoFiles`.`AssocType`='event'";   
}

if (isset($_GET['tag']))
{
	$sTag = htmlspecialchars_adv($_GET['tag']);
	$aWhere[] = "`sharePhotoFiles`.`medTags` like '%$sTag%'";
}

// Search
if (isset($_GET['search']) && $keyword = trim($_GET['search']))
	$aWhere[] = "medTitle LIKE '%$keyword%' OR medDesc LIKE '%$keyword%' 
		OR medTags LIKE '%$keyword%'";

if (isset($_GET['action']))
{
	if ($_GET['action'] == 'edit')
	{
		define('MAXTITLELENGTH', 150);
		define('MAXTAGSLENGTH', 150);
		
		if (!isset($_GET['fileID']) || !($_GET['fileID'] > 0))
		{
			$_page_cont[$_ni]['page_main_code'] = 'Photo not specified!';
			PageCode();
			exit;
		}
		
		// Get photo info from database
		$query = "SELECT medTitle, medTags, medDesc, Permission FROM sharePhotoFiles WHERE medID={$_GET['fileID']} LIMIT 1";
		$fileInfo = fill_assoc_array(db_res($query));

		if (!is_array($fileInfo))
		{
			$_page_cont[$_ni]['page_main_code'] = 'Photo not found!';
			PageCode();
			exit;
		}
		$fileInfo = $fileInfo[0];
		
		$permPublic = (!$fileInfo['Permission']) ? ' checked' : '';
		$permFriends = ($fileInfo['Permission'] == 1) ? ' checked' : '';
		
		$uploadFileForm = '<form enctype="multipart/form-data" method="post" action="'.$site['url'].'uploadSharePhoto.php">
			<table><tr><td width="550px">
			
			<div class="uploadLine"><div class="uploadText">'._t("_Title").': </div>
				<input type="text" name="title" class="uploadForm" value="'.$fileInfo['medTitle'].'"
					 maxlength="' . MAXTITLELENGTH . '"/></div>
			<div class="uploadLine"><div class="uploadText">'._t("_Description").': </div>
				<textarea name="description" class="uploadForm"/>'.$fileInfo['medDesc'].'</textarea></div>
			<div class="uploadLine"><div class="uploadText">'._t("_Tags").': </div>
				<input type="text" name="tags" class="uploadForm" value="'.$fileInfo['medTags'].'"
					 maxlength="'. MAXTAGSLENGTH .'"/></div>
			<div style="float:left;"><div class="uploadText">'._t("_Select").': </div>
				<div style="float:left"><input type="file" name="uploadFile" size="43"/></div>
				<div style="clear:left; color:gray; font-size:11px; text-align:center; margin-bottom:10px;margin-left:150px">
					(leave empty to keep old photo)</div>	
			</div>
			
			</td><td style="vertical-align: top;">
			<b>Photo Permissions</b><div style="margin-top:5px">
				<input type="radio" name="photoPerm" value="0"'.$permPublic.'/>public<br/>
				<input type="radio" name="photoPerm" value="1"'.$permFriends.'/>friends only
				</div>
			</td></tr></table>
			
			<div style="text-align:center; clear:both"><input type="submit" name="upload" value="Save"/></div>
			<input type="hidden" name="action" value="edit"/>
			<input type="hidden" name="medID" value="'.$_GET['fileID'].'"/>
			<input type="hidden" name="medProfId" value="'.$member['ID'].'"/>				
			</form>';
				
		$_page_cont[$_ni]['page_main_code'] = $uploadFileForm;
		PageCode();
		exit;		
	}
	$sAct = htmlspecialchars_adv($_GET['action']);
	$sReferer = $_GET['referer'];
	$sAddon = defineBrowseAction($sAct,'Photo',$member['ID']);
}

$sqlWhere = "WHERE " . implode( ' AND ', $aWhere ).$sAddon." AND `Approved`= 'true'";

$iTotalNum = db_value("SELECT COUNT( * ) FROM `sharePhotoFiles` $sqlWhere");
if (!$iTotalNum)
{
	if ($userID > 0) // member photos
	{
		// Get profile owner nickname
		$ownerNick = (isset($_GET['sefNick'])) ? $_GET['sefNick'] : getNickName($userID);
	
		if ($member['ID'] == $userID) // my photos
			$_page_cont[$_ni]['page_main_code'] = "Sorry, $ownerNick it looks like you have not setup a photo gallery yet.<br/><br/>
				Would you like to <a href=\"uploadSharePhoto.php\">upload your first photo</a> to your photo gallery or 
				<a href=\"upload_media.php\">upload your profile photo avatar?</a>";
		else
			$_page_cont[$_ni]['page_main_code'] = "Sorry, it looks like $ownerNick has not setup a photo gallery yet.";
	}
	else // event photos
		$_page_cont[$_ni]['page_main_code'] = _t( '_Sorry, nothing found' );

	PageCode();
	exit;
}

$iPerPage = (int)$_GET['per_page'];
if( !$iPerPage )
	$iPerPage = 10;

$iTotalPages = ceil( $iTotalNum / $iPerPage );

$iCurPage = (int)$_GET['page'];

if( $iCurPage > $iTotalPages )
	$iCurPage = $iTotalPages;

if( $iCurPage < 1 )
	$iCurPage = 1;

$sLimitFrom = ( $iCurPage - 1 ) * $iPerPage;

$sqlOrderBy = 'ORDER BY `medDate` DESC';

if (isset($_GET['rate']))
{
	$oVotingView = new BxTemplVotingView ('gphoto', 0, 0);
	
	$aSql        = $oVotingView->getSqlParts('`sharePhotoFiles`', '`medID`');
	$sHow        = $_GET['rate'] == 'top' ? "DESC" : "ASC";
	$sqlOrderBy  = $oVotingView->isEnabled() ? "ORDER BY `voting_rate` $sHow, `voting_count` $sHow, `medDate` $sHow" : $sqlOrderBy ;
	$sqlFields   = $aSql['fields'];
	$sqlLJoin    = $aSql['join'];
}	
$sqlLimit = "LIMIT $sLimitFrom, $iPerPage";

$sQuery = "
	SELECT
		`sharePhotoFiles`.`medID`,
		`sharePhotoFiles`.`medProfId`,
		`sharePhotoFiles`.`medTitle`,
		UNIX_TIMESTAMP(`sharePhotoFiles`.`medDate`) as `medDate`,
		`sharePhotoFiles`.`medViews`,
		`sharePhotoFiles`.`medExt`,
		`sharePhotoFiles`.`Permission`,
		`sharePhotoFiles`.`urltitle`,
		`Profiles`.`NickName`, `Profiles`.`ID` as OwnerID,
		SDatingEvents.ResponsibleID as eventOwnerID
		$sqlFields
	FROM `sharePhotoFiles`
	LEFT JOIN `Profiles` ON
		`Profiles`.`ID` = `sharePhotoFiles`.`medProfId`
	LEFT JOIN SDatingEvents ON SDatingEvents.ID=sharePhotoFiles.AssocID
	$sqlLJoin
	$sqlWhere
	$sqlOrderBy
	$sqlLimit
	";

$rData = db_res($sQuery);

$_page_cont[$_ni]['page_main_code'] = PageCompPageMainCode();

PageCode();

function PageCompPageMainCode()
{
	global $site;
	global $rData;
	global $iTotalPages;
	global $iCurPage;
	global $iPerPage;
	global $member;
	global $userID;
	
	$sCode = '';
	if (mysql_num_rows($rData))
	{
		$photoTitleTmplOrig = "{$site['url']}viewPhoto.php?fileID=%photoID%";
		$photoTitleTmpl = (useSEF) ? "{$site['url']}%ownerNick%/photo/%urltitle%.html" : $photoTitleTmplOrig;
		while ($aData = mysql_fetch_array($rData))
		{
			if ($aData['Permission'] == 1 && !is_friends($member['ID'], $aData['OwnerID']) && !($member['ID']==$aData['OwnerID']))
				continue;
			
			$sImage = $site['sharingImages'].$aData['medID'].'_t.'.$aData['medExt'];
			$sProfLink = '<div>'._t("_By").': <a href="'.getProfileLink($aData['medProfId']).'">'.$aData['NickName'].'</a></div>';
			
			$oVotingView = new BxTemplVotingView ('gphoto', $aData['medID']);
		    if( $oVotingView->isEnabled())
	    	{
				$sRate = $oVotingView->getSmallVoting (0);
				$sShowRate = '<div class="galleryRate">'. $sRate . '</div>';
			}
			if ($aData['urltitle'] == '')
				$sHref = str_replace('%photoID%', $aData['medID'], $photoTitleTmplOrig);
			else
				$sHref = str_replace(array('%ownerNick%', '%photoID%', '%urltitle%'), 
					array($aData['NickName'], $aData['medID'], $aData['urltitle']), $photoTitleTmpl);
				
			$sImg  = '<div class="lastFilesPic" style="background-image: url(\''.$sImage.'\');">
					  <a href="'.$sHref.'"><img src="'.$site['images'] .'spacer.gif" width="110" height="110"></a></div>';

			$sPicTitle = strlen($aData['medTitle']) > 0 ? $aData['medTitle'] : _t("_Untitled");
			$isOwner = ($member['ID'] == $aData['medProfId']);
			$isEventOwner = ($member['ID'] == $aData['eventOwnerID']);
			$sEditLink = ($member['ID'] && ($isOwner || $isEventOwner)) ? '<div style="float:right; margin-bottom:5px">
				<a href="'.$_SERVER['PHP_SELF'].'?action=edit&fileID='.$aData['medID'].'">Edit</a></div>'  : "" ;
			$sDelLink = ($member['ID'] && ($isOwner || $isEventOwner)) ? '<div style="float:right; margin-bottom:20px">
				<a href="'.$_SERVER['PHP_SELF'].'?action=del&fileID='.$aData['medID'].'" 
				onClick="return confirm( \''._t("_confirmDeletePhoto").'\');">'._t("_Delete").'</a></div>'  : "" ;
			$sCode .= '<div class="browseUnit"><table><tr><td>';
				$sCode .= $sImg . '</td>';
				$sCode .= '<td style="vertical-align:top; width:250px"><div><a href="'.$sHref.'"><b>'.$sPicTitle.'</b></a></div>';
				$sCode .= $sProfLink;
				$sCode .= '<div>'._t("_Added").': <b>'.defineTimeInterval($aData['medDate']).'</b></div>';
				$sCode .= '<div>'._t("_Views").': <b>'.$aData['medViews'].'</b></div>';
				$sCode .= $sShowRate . '</td>';
				$sCode .= '<td style="vertical-align:bottom; width:45px">' . $sEditLink;
				$sCode .= $sDelLink . '</td>';
			$sCode .= '</tr></table></div>';	
		}
	}
	
	// generate pagination
	if( $iTotalPages > 1)
	{
		if ($userID)
			$sRequest = "userID=$userID";
					
		$aFields = array('tag', 'rate', 'search');
		foreach ($aFields as $field)
		{
			if (isset($_GET[$field]))
			{
				if ($sRequest != '') $sRequest .= "&amp;";
				$sRequest .= "{$field}=" . htmlentities( process_pass_data( $_GET[$field] ) );
			}
		}
		
		$sRequest = "{$_SERVER['PHP_SELF']}?$sRequest";
	
		$perPageHtml = '<div style="text-align: center; position: relative; 
			margin-top:15px; height:23px">'. _t('_Results per page').':
		<select name="per_page" onchange="window.location=\'' . $sRequest . '&amp;per_page=\' + this.value;">
			<option value="10"' . ( $iPerPage == 10 ? ' selected="selected"' : '' ) . '>10</option>
			<option value="20"' . ( $iPerPage == 20 ? ' selected="selected"' : '' ) . '>20</option>
			<option value="50"' . ( $iPerPage == 50 ? ' selected="selected"' : '' ) . '>50</option>
			<option value="100"' . ( $iPerPage == 100 ? ' selected="selected"' : '' ) . '>100</option>
		</select>
		</div>';
		
		$pagination = genPagination($iTotalPages, $iCurPage, ($sRequest . '&amp;page={page}&amp;per_page='.$iPerPage));
	}
	else
	{
		$perPageHtml = '';
		$pagination = '';
	}
	
	$res = <<<EOF
<div style="position: relative; float: left;">
	<center>$pagination</center>
	$sCode
	<div class="clear_both"></div>
	<center>{$perPageHtml}{$pagination}</center> 
</div>
EOF;
		
	return $res;
}

?>
