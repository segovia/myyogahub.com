<?php

require_once(BX_DIRECTORY_PATH_ROOT . 'uploadEventPhoto.php');    

class EventPhotos extends UploadEventPhoto
{
	function EventPhotos($iProfileID, $iEventID)
	{		
		parent::UploadEventPhoto($iProfileID, $iEventID);
	}


	function getMediaPage($iMediaID = 0)
    {
        global $votes_pic, $site, $tmpl;

		$ret = '';
		//$ret .= $this->getJSCode();

		if($this->iMediaCount > 0)
		{
			$iMediaID = ($iMediaID > 0) ? $iMediaID : $this->aMedia['0']['PrimPhoto'];
			$aCurPhoto = $this->getElementArrayByID($iMediaID);
			if(empty($aCurPhoto))
				$sPhotoUrl = $site['url']."templates/tmpl_{$tmpl}/images/icons/group_no_pic.gif";
			else
			{
				$ret .= '<div class="mediaTitle" id="sTitleDiv">';
					$ret .= stripcslashes(process_line_output($aCurPhoto['med_title']));
				$ret .= '</div>';
				$iPhotoID = $aCurPhoto['med_id'];
				$sPhotoUrl = $this->sMediaUrl . 'photo_' . $aCurPhoto['med_file'];
				$iPhotoRating = $this->getRating($aCurPhoto['med_rating_sum'], $aCurPhoto['med_rating_count']);
			}
		}
		else
			$sPhotoUrl = $site['url']."templates/tmpl_{$tmpl}/images/icons/group_no_pic.gif";

		$ret .= $this->getJSCode($aCurPhoto);
		$ret .= '<div class="photoBlock" id="photoKeeper">';
			$style = '
				width:' . $this->aMediaConfig['size']['photoWidth'] . 'px;' .
				'height:' . $this->aMediaConfig['size']['photoHeight'] . 'px;' .
				'background-repeat: no-repeat; background-image:url(\'' . $sPhotoUrl . '\');';
			$ret .= '<img src="' . getTemplateIcon('spacer.gif') . '" style="' . $style . '" class="photo" alt="" id="temPhotoID" />';
		$ret .= '</div>';

        $sJsArray = '';
        $iPhotosCount = 0;
        $sIconsList = $this->_getIconsList($sJsArray, $iPhotosCount);
        $oVotingView = new BxTemplVotingView ('media', (int)$aCurPhoto['med_id']);
        if($iPhotosCount && $votes_pic && $oVotingView->isEnabled())
            $ret .= '<div class="votingBlock">' . $oVotingView->getBigVoting () . '</div>';
		$ret .= '<div class="iconBlock">';
		$ret .= $sIconsList;
		$ret .= '</div>';
        $ret .= '<script type="text/javascript">' . $sJsArray . '</script>';

		return $ret;
	}

	function getJSCode($aCurPhoto)
	{
		global $site;

		$ret = '';
		$ret .= '<script type="text/javascript">
			if (window.attachEvent)
				window.attachEvent("onload", onloadPhotos);
			else
				window.addEventListener("load", onloadPhotos, false);

			function onloadPhotos()
            {
                hideScroll();
                if (window.oVotingmediasmall)
                {
                    oVotingmediasmall.onvote = function (fRate, iCount) 
                    {
                        oIcons[this._iObjId]["rate"] = fRate;
                        oIcons[this._iObjId]["count"] = iCount;
                    }                    
                }
                if (window.oVotingmediabig)
                {
                    oVotingmediabig.onvote = function (fRate, iCount) 
                    {
                        oPhotos[this._iObjId]["rate"] = fRate;
                        oPhotos[this._iObjId]["count"] = iCount;
                    }                    
                }
			}
			
			//hide scrollers if needed
			
			function hideScroll()
			{
				b = document.getElementById("iconBlock");
				s = document.getElementById("scrollCont");
				if(!b || !s)
					return false;
				if(b.parentNode.clientWidth >= b.clientWidth)
					s.style.display = "none";
				else
					s.style.display = "block";
			}

			function setImage()
			{
				var imgCode;
				var oOldImg = document.getElementById("temPhotoID");
				oOldImg.style.backgroundImage = "url(' . $this->sMediaUrl . 'photo_' . $aCurPhoto['med_file'] . ')";
				return false;
			}

			function setThumb()
			{
				var imgCode;
				var oOldImg = document.getElementById("temThumbID");
				var oLink = document.getElementById("temThumbLink");

				oOldImg.style.backgroundImage = "url(' . $this->sMediaUrl . 'photo_' . $aCurPhoto['med_file'] . ')";
				oLink.href = "' . $site['url'] . 'event_photos_gallery.php?eventID=' .  $this->iEventID . '&photoID=' . $aCurPhoto['med_id'] . '";
				return false;
			}

			function changePhoto(iMediaID)
			{			
                var oOldImg = document.getElementById("temPhotoID");
				oOldImg.style.backgroundImage = "url(' . $this->sMediaUrl . 'photo_"+oPhotos[iMediaID]["file"]+")";
                changeTitle(oPhotos[iMediaID]["title"]);

                if (oVotingmediabig)
                {
    				oVotingmediabig._iObjId = iMediaID;
	    			oVotingmediabig.setCount(oPhotos[iMediaID]["count"]);
		    		oVotingmediabig.setRate(oPhotos[iMediaID]["rate"]);
                }

				return false;
            }

			function changeThumb(iMediaID)
			{
				var oOldImg = document.getElementById("temThumbID");
                var oLink = document.getElementById("temThumbLink");
                var oTitle = document.getElementById("temPhotoTitle"); 

				oOldImg.style.backgroundImage = "url(' . $this->sMediaUrl . 'photo_"+oIcons[iMediaID]["file"]+")";
                oTitle.innerHTML = oIcons[iMediaID]["title"];

                if (oVotingmediasmall)
                {
    				oVotingmediasmall._iObjId = iMediaID;
	    			oVotingmediasmall.setCount(oIcons[iMediaID]["count"]);
		    		oVotingmediasmall.setRate(oIcons[iMediaID]["rate"]);
                }
				oLink.href = "' . $site['url'] . 'event_photos_gallery.php?eventID=' .  $this->iEventID . '&photoID=" + iMediaID;

				return false;
			}

			function changeTitle(sTitle)
			{
				var oTitlDiv = document.getElementById("sTitleDiv");
				oTitlDiv.innerHTML = stripSlashes(sTitle);
            }
		</script>';

		return $ret;
	}

	function _getIconsList(&$sJsArray, &$iCountPhotos)
	{
		global $site, $tmpl;
		
		$ret = '';
        $sJsArray = 'var oPhotos = {';
        $iCountPhotos = 0;
		for($i = 0; $i < $this->aMediaConfig['max']['eventphoto']; $i++)
		{
			$sIconSrc = $this->sMediaDir . 'icon_' . $this->aMedia[$i]['med_file'];
			if(extFileExists($sIconSrc))
            {
				$iPhotoRatingCount = $this->aMedia[$i]['voting_count'] ? $this->aMedia[$i]['voting_count'] : 0;
				$iPhotoRating = $this->aMedia[$i]['voting_rate'] ? $this->aMedia[$i]['voting_rate'] : 0; 
                $sIconUrl = $this->sMediaUrl . 'icon_' . $this->aMedia[$i]['med_file'];
                $atrib = "'{$this->aMedia[$i]['med_id']}'";
                $ret .= '<img src="' . getTemplateIcon('spacer.gif') . '" style="width:' . $this->aMediaConfig['size']['iconWidth'] . 'px; height:' . $this->aMediaConfig['size']['iconHeight'] . 'px; background-repeat: no-repeat; background-image:url(' . $sIconUrl . '); cursor:pointer;"  alt="" class="icons" onmouseover="this.className=\'iconsHover\'" onmouseout="this.className=\'icons\'" onclick="return changePhoto(' . $atrib . ');" />';
                $sJsArray .= "'{$this->aMedia[$i]['med_id']}' : {" . 
                    "'title' : '{$this->aMedia[$i]['med_title']}'," . 
                    "'file' : '{$this->aMedia[$i]['med_file']}'," . 
                    "'rate' : '{$iPhotoRating}'," . 
                    "'count' : '{$iPhotoRatingCount}'},\n";
                ++$iCountPhotos;
			}
			else
			{
				$sIconUrl = $site['url']."templates/tmpl_{$tmpl}/images/icons/group_no_pic.gif";
				$ret .= '<img src="' . getTemplateIcon('spacer.gif') . '" style="width:' . $this->aMediaConfig['size']['iconWidth'] . 'px; height:' . $this->aMediaConfig['size']['iconHeight'] . 'px; background-repeat: no-repeat; background-image:url(' . $sIconUrl . ');" alt="" class="icons" />';
			}
		}
		if($iCountPhotos)
			$sJsArray = substr($sJsArray,0,-2);
        $sJsArray .= '}';

		return $ret;
	}

	// for thumbs switching
	function _getIconsList2(&$sJsArray, &$iCountPhotos)
	{
		$ret = '';
        $sJsArray = 'var oIcons = {';
        $iCountPhotos = 0;

		for($i = 0; $i < $this->aMediaConfig['max']['eventphoto']; $i++)
		{
			$sIconSrc = $this->sMediaDir . 'icon_' . $this->aMedia[$i]['med_file'];
			if(extFileExists($sIconSrc))
            {
				$iPhotoRatingCount = $this->aMedia[$i]['voting_count'] ? $this->aMedia[$i]['voting_count'] : 0;
				$iPhotoRating = $this->aMedia[$i]['voting_rate'] ? $this->aMedia[$i]['voting_rate'] : 0; 
				$sIconUrl = $this->sMediaUrl . 'icon_' . $this->aMedia[$i]['med_file'];
                $atrib = "'{$this->aMedia[$i]['med_id']}'";
                $ret .= '<img src="' . getTemplateIcon('spacer.gif') . '" style="width:' . $this->aMediaConfig['size']['iconWidth'] . 'px; height:' . $this->aMediaConfig['size']['iconHeight'] . 'px; background-repeat: no-repeat; background-image:url(' . $sIconUrl . ');"  alt="" class="icons" onclick="return changeThumb(' . $atrib . ');" />';
                $sJsArray .= "'{$this->aMedia[$i]['med_id']}' : {" . 
                    "'title' : '{$this->aMedia[$i]['med_title']}'," . 
                    "'file' : '{$this->aMedia[$i]['med_file']}'," . 
                    "'rate' : '{$iPhotoRating}'," . 
                    "'count' : '{$iPhotoRatingCount}'},\n";
                ++$iCountPhotos;
			}
		}
		if($iCountPhotos)
			$sJsArray = substr($sJsArray,0,-2);
        $sJsArray .= '}';

		return $ret;
	}

	function getPrimaryPhotoArray()
	{
		$aPrimPhoto = $this->getElementArrayByID($this->aMedia['0']['PrimPhoto']);
		return $aPrimPhoto;
	}

	function getMediaBlock($iMediaID = 0)
	{
		global $dir, $site, $tmpl;
		global $votes_pic;

		$ret = '';

		if($this->iMediaCount > 0)
		{
			$iMediaID = ($iMediaID > 0) ? $iMediaID : $this->aMedia['0']['PrimPhoto'];
			$aCurPhoto = $this->getElementArrayByID($iMediaID);
			if(empty($aCurPhoto))
				$sPhotoUrl = $site['url']."templates/tmpl_{$tmpl}/images/icons/group_no_pic.gif";
			else
			{
				$iPhotoID = $aCurPhoto['med_id'];
				$sPhotoUrl = $this->sMediaUrl . 'photo_' . $aCurPhoto['med_file'];
			}
		}
		else
		{
			// Check is event PhotoFilename exists in sdating folder and copy to profile folder
			$photoFilename = db_value("SELECT PhotoFilename FROM SDatingEvents WHERE ID = ".$this->iEventID);
			if ($photoFilename != '' && file_exists($dir['sdatingImage'].$photoFilename))
			{
				$newPhotoFilename = str_replace(array('g_', '_1'), '', $photoFilename);
			
				copy($dir['sdatingImage'].$photoFilename,
					$dir['profileImage'] . $this->iProfileID . "/photo_$newPhotoFilename");
				copy($dir['sdatingImage']."thumb_$photoFilename", 
					$dir['profileImage'] . $this->iProfileID . "/thumb_$newPhotoFilename");
				copy($dir['sdatingImage']."icon_$photoFilename", 
					$dir['profileImage'] . $this->iProfileID . "/icon_$newPhotoFilename");
			
				$medTitle = 'main';
					
				// Insert record to media table
				$res = db_res("INSERT INTO `media` SET 
					med_prof_id=".$this->iEventID.", 
					med_type = 'eventphoto',
					med_file = '$newPhotoFilename',
					med_title = '$medTitle',
					med_status = 'active',
					med_date = NOW()");
				$insertedMediaID = mysql_insert_id();
					
				// Update PrimPhoto in events table
				$res = db_res("UPDATE SDatingEvents SET
						PrimPhoto = $insertedMediaID,
						Picture = '1'
					WHERE ID=".$this->iEventID);
				
				// Fill media array
				$lastMediaRow = db_arr("SELECT * FROM `media` WHERE med_id=$insertedMediaID LIMIT 1");
				$this->aMedia[0] = $lastMediaRow;
							
				$iPhotoID = $insertedMediaID;
				$sPhotoUrl = $this->sMediaUrl . 'photo_' . $newPhotoFilename;
			}
			else
				$sPhotoUrl = $site['url']."templates/tmpl_{$tmpl}/images/icons/group_no_pic.gif";
		}
		$ret .= $this->getJSCode($aCurPhoto);
		$ret .= '<div class="photoBlock" id="photoKeeper">';
			$style = 
				'width:'  . $this->aMediaConfig['size']['photoWidth'] . 'px;'.
				'height:' . $this->aMediaConfig['size']['photoHeight'] . 'px;' .
				'background-repeat: no-repeat; background-image:url(' . $sPhotoUrl . ');';
			$ret .= '<a href="' . $site['url'] . 'event_photos_gallery.php?eventID=' .  $this->iEventID . '" id="temThumbLink">';
				$ret .= '<img src="' . getTemplateIcon('spacer.gif') . '" style="' . $style . '" class="photo" alt="" id="temThumbID" />';
				//$ret .= '<img src="' . getTemplateIcon('spacer.gif') . '" style="' . $style . '" class="photo" alt="" onload="return setThumb();" id="temThumbID" />';
			$ret .= '</a>';
		$ret .= '</div>';
		$ret .= '<div id="temPhotoTitle" class="photo_title">' . stripslashes($aCurPhoto['med_title']) . '</div>';
        $sJsIconsArray = '';
        $iCountPhotos = 0;
        $sIcons = $this->_getIconsList2($sJsIconsArray, $iCountPhotos);
        $oVotingView = new BxTemplVotingView ('media', (int)$aCurPhoto['med_id']);
        if($iCountPhotos && $votes_pic && $oVotingView->isEnabled())
		    $ret .= $oVotingView->getSmallVoting();
		$ret .= '<div class="clear_both"></div>';		
		if(strlen($sIcons))
		{
			$ret .= '<div class="scrollIconContainer">';
				$ret .= '<div class="scrollCont" id="scrollCont">';
					$ret .= '<div class="scrollLeft"  onmouseover="moveScrollLeftAuto( \'iconBlock\', 1);" onmouseout="moveScrollLeftAuto( \'iconBlock\', 0);"><img src="' . getTemplateIcon('left_arrow.gif') . '" alt="left arrow"/></div>';
					$ret .= '<div class="scrollRight" onmouseover="moveScrollRightAuto(\'iconBlock\', 1);" onmouseout="moveScrollRightAuto(\'iconBlock\', 0);"><img src="' . getTemplateIcon('right_arrow.gif') . '" alt="right arrow"/></div>';
					$ret .= '<div class="clear_both"></div>';
				$ret .= '</div>';
				$ret .= '<div class="iconBlockCont">';
					$ret .= '<div id="iconBlock" class="iconBlock">';
						$ret .= $sIcons;
					$ret .= '</div>';
				$ret .= '</div>';
			$ret .= '</div>';
		}
        $ret .= '<script type="text/javascript">' . $sJsIconsArray . '</script>';

        return $ret;
	}
	
	function EventDetails()
	{
		global $prof, $site, $tmpl;
	
		$eventInfo = fill_assoc_array(db_res("SELECT SDatingEvents.*, Profiles.NickName, 
				(UNIX_TIMESTAMP() - UNIX_TIMESTAMP(`EventStart`)) AS `sec`, media.med_file
			FROM SDatingEvents
			LEFT JOIN Profiles ON Profiles.ID = SDatingEvents.ResponsibleID
			LEFT JOIN media ON media.med_id = SDatingEvents.PrimPhoto
			WHERE SDatingEvents.ID=".$this->iEventID." LIMIT 1"));
		
		if (is_array($eventInfo))
			$eventInfo = $eventInfo[0];
		else 
			return false;
		
		$date_format_php = getParam('php_date_format');
		$sDateTime = date( $date_format_php, strtotime($eventInfo['EventStart']));
	
		$primaryEventImgDir = $this->sMediaDir . "thumb_{$eventInfo['med_file']}";
		$primaryEventImgUrl = $this->sMediaUrl . "thumb_{$eventInfo['med_file']}";
		$noEventIconImg = $site['url']."templates/tmpl_{$tmpl}/images/icons/group_no_pic.gif";
		$eventImg = (file_exists($primaryEventImgDir)) ? $primaryEventImgUrl : $noEventIconImg;
		
		$ret .= "<table align=\"center\"><tr>";
		
		$ret .= "<td style=\"vertical-align:top\"><a href=\"{$site['url']}events.php?action=show_info&event_id=".
			$this->iEventID."\"><img style=\"margin-right:20px\" src=\"$eventImg\"/></a></td>";
			
		$ret .= "<td><table align=\"center\" style=\"background-color:#F8F8E7; border: 1px solid #F8DAA8; margin-bottom:10px\">
			<tr><td class=\"eventInfoItem\">Title:</td><td class=\"eventInfoItemValue\">{$eventInfo['Title']}</td><tr>
			<tr><td class=\"eventInfoItem\">Country:</td><td class=\"eventInfoItemValue\">".$prof['countries'][$eventInfo['Country']]."</td><tr>
			<tr><td class=\"eventInfoItem\">City:</td><td class=\"eventInfoItemValue\">{$eventInfo['City']}</td><tr>
			<tr><td class=\"eventInfoItem\">Place:</td><td class=\"eventInfoItemValue\">{$eventInfo['Place']}</td><tr>
			<tr><td class=\"eventInfoItem\">Date:</td><td class=\"eventInfoItemValue\">$sDateTime ("._format_when($eventInfo['sec']).")</td><tr>
			<tr><td class=\"eventInfoItem\">Posted by:</td><td class=\"eventInfoItemValue\">
			<a href=\"{$site['url']}{$eventInfo['NickName']}\">{$eventInfo['NickName']}</a></td><tr></table></td>";
		
		$ret .= "</tr></table>";
		
		return $ret;
	}
}