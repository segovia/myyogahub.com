<?php

require_once( 'inc/header.inc.php' );
require_once( BX_DIRECTORY_PATH_INC . 'design.inc.php' );
require_once( BX_DIRECTORY_PATH_INC . 'profiles.inc.php' );
require_once( BX_DIRECTORY_PATH_INC . 'utils.inc.php' );
require_once( BX_DIRECTORY_PATH_ROOT . 'eventPhotos.php' );
require_once( BX_DIRECTORY_PATH_ROOT . "templates/tmpl_{$tmpl}/scripts/BxTemplVotingView.php" );

// --------------- page variables and login

$img_num = $pic_num;

$_page['name_index'] 	= 62;
$_page['css_name']		= 'upload_media.css';


if ( !( $logged['admin'] = member_auth( 1, false ) ) )
{
	if ( !( $logged['member'] = member_auth( 0, false ) ) )
	{
		if ( !( $logged['aff'] = member_auth( 2, false )) )
		{
			$logged['moderator'] = member_auth( 3, false );
		}
	}
}


$_page['header'] = _t( "_EventPhotos" );
//$_page['header_text'] = _t( "_PIC_GALLERY_H1" );

$oVotingView = new BxTemplVotingView('media', 0, 0);
$_page['extra_js'] 	= $oVotingView->getExtraJs();

// --------------- GET/POST actions


$_ni = $_page['name_index'];
$_page_cont[$_ni]['page_main_code'] = getPageMainCode();

// --------------- [END] page components

PageCode();

// --------------- page components functions


function getPageMainCode()
{
	global $_page;
	
	$aPhotoConf = array();
	$aPhotoConf['eventID'] = (int)$_REQUEST['eventID'];
	$aPhotoConf['photoID'] = (int)$_REQUEST['photoID'];
	$aPhotoConf['visitorID'] = (int)$_COOKIE['memberID'];
	
	// Get event title and owner id
	$eventInfo = db_arr("SELECT ResponsibleID, Title FROM SDatingEvents WHERE ID = {$aPhotoConf['eventID']} LIMIT 1");
	
	$aPhotoConf['isOwner'] = ($aPhotoConf['visitorID'] == $eventInfo['ResponsibleID']) ? true : false;
	if( $aPhotoConf['isOwner'] )
	{
		header("Location:upload_media.php?show=eventphoto&eventID=" . $aPhotoConf['eventID']);
		exit;
	}
	
	$_page['header_text'] = "Event: {$eventInfo['Title']}";
	
	$check_res = checkAction( $aPhotoConf['visitorID'], ACTION_ID_VIEW_PHOTOS );
	if ( $check_res[CHECK_ACTION_RESULT] != CHECK_ACTION_RESULT_ALLOWED )
	{
		$ret = '
			<table width="100%" cellpadding="4" cellspacing="4" border="0">
				<tr>
					<td align="center">' . $check_res[CHECK_ACTION_MESSAGE] . '</td>
				</tr>
			</table>';
		return $ret;
	}

	$oPhotos = new EventPhotos($aPhotoConf['visitorID'], $aPhotoConf['eventID']);
	$oPhotos -> getActiveMediaArray();

	$ret = '';
	if( $_REQUEST['voteSubmit'] && $_REQUEST['photoID'] )
	{
		$oPhotos -> setVoting();
		header('Location:' . $_SERVER['PHP_SELF'] . '?eventID=' . $oPhotos->iEventID . '&photoID=' . $_REQUEST['photoID'] );
	}

	if( !$aPhotoConf['isOwner'] )
	{
		$ret .= $oPhotos->EventDetails();
		$ret .= '<div class="clear_both"></div>';
	}

	if($_REQUEST['photoID'] > 0)
	{
		$iPhotoID = $_REQUEST['photoID'];
		$ret .= $oPhotos->getMediaPage($iPhotoID);
	}
	else 
		$ret .= $oPhotos -> getMediaPage();

	return $ret;
}
?>
