<?

/***************************************************************************

*                            Dolphin Smart Community Builder

*                              -----------------

*     begin                : Mon Mar 23 2006

*     copyright            : (C) 2006 BoonEx Group

*     website              : http://www.boonex.com/

* This file is part of Dolphin - Smart Community Builder

*

* Dolphin is free software. This work is licensed under a Creative Commons Attribution 3.0 License. 

* http://creativecommons.org/licenses/by/3.0/

*

* Dolphin is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;

* without even the implied warranty of  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

* See the Creative Commons Attribution 3.0 License for more details. 

* You should have received a copy of the Creative Commons Attribution 3.0 License along with Dolphin, 

* see license.txt file; if not, write to marketing@boonex.com

***************************************************************************/

require_once( 'header.inc.php' );
require_once( BX_DIRECTORY_PATH_INC . 'db.inc.php' );
require_once( BX_DIRECTORY_PATH_INC . 'prof.inc.php' );
require_once( BX_DIRECTORY_PATH_INC . 'newsfeed.inc.php' );
require_once( BX_DIRECTORY_PATH_ROOT . "templates/tmpl_{$tmpl}/scripts/BxTemplVotingView.php" );
require_once( BX_DIRECTORY_PATH_ROOT . "phptal/PHPTAL.php" );

function defineTimeInterval($iTime)

{

	$iTime = time() - $iTime;

	if ( $iTime < 60 )

	  $sCode = "$iTime "._t("_seconds ago");

	else

	{

	  $iTime = round( $iTime / 60 ); // minutes

	  if ( $iTime < 60 )

	    $sCode = "$iTime "._t("_minutes ago");

	  else

	  {

	    $iTime = round( $iTime / 60 ); //hours

	    if ( $iTime < 24 )

			$sCode = "$iTime "._t("_hours ago");

	    else

	    {

		    $iTime = round( $iTime / 24 ); //days

		    $sCode = "$iTime "._t("_days ago");

	    }

	  }

	}

	

	return $sCode;

}





function deleteMedia($iFile, $sType, $sExt = '')
{
	global $dir;
	global $logged;
	global $sReferer;

	$sType = ($sType == 'Video') ? 'Movie' : $sType ;

	// delete voting

    require_once( BX_DIRECTORY_PATH_CLASSES . 'BxDolVoting.php' ); 


	if( $logged['admin'] )
	{
	}
	elseif( $logged['member'] )
	{
	  $iMemberID = (int)$_COOKIE['memberID'];

	  if ($sType == 'Photo')
	  	$sQuery = "SELECT sharePhotoFiles.*, SDatingEvents.ResponsibleID as eventOwnerID, SDatingEvents.ID as eventID
	  				FROM sharePhotoFiles 
	  				LEFT JOIN SDatingEvents ON SDatingEvents.ID=sharePhotoFiles.AssocID
	  				WHERE `medID`=$iFile LIMIT 1";
	  else
	  	$sQuery = "SELECT RayMovieFiles.`ID` as `medID`,
	  					  RayMovieFiles.`Title` as `medTitle`,
	  					  RayMovieFiles.`Description` as `medDesc`,
	  					  RayMovieFiles.`Date` as `medDate`,
	  					  RayMovieFiles.`Owner` as `medProfId`,
	  					  SDatingEvents.ResponsibleID as eventOwnerID,
	  					  SDatingEvents.ID as eventID
	  			   	FROM RayMovieFiles 
	  				LEFT JOIN SDatingEvents ON SDatingEvents.ID=RayMovieFiles.AssocID	  			   	
	  			   	WHERE RayMovieFiles.`ID`=$iFile";

	  $aFile = db_arr( $sQuery );

	  if( !$aFile )	return false;

	  if($iMemberID != $aFile['medProfId'] && $iMemberID != $aFile['eventOwnerID']) return false;
	}
	else
	  return false;

	$aFName = array();

	switch ($sType)
	{
		case 'Music':
		  	$sTableName = "`Ray{$sType}Files`";
			$sModPath = 'ray/modules/music/files/';
            $aFName[] = $iFile.'.mp3';
            $oVoting = new BxDolVoting ('gmusic', 0, 0);
			break;
		case 'Photo':
			$sExt = db_value("SELECT medExt FROM sharePhotoFiles WHERE medID=$iFile LIMIT 1");
			$sTableName = "`share{$sType}Files`";
			$sModPath = 'media/images/sharingImages/';
			$aFName[] = "$iFile.$sExt";
			$aFName[] = $iFile.'_t.'.$sExt;
            $aFName[] = $iFile.'_m.'.$sExt;
            $oVoting = new BxDolVoting ('gphoto', 0, 0);
			break;
		case 'Movie':
			$sTableName = "`Ray{$sType}Files`";
			$sModPath = 'ray/modules/movie/files/';
			$aFName[] = $iFile.'.jpg';
			$aFName[] = $iFile.'_small.jpg';
			$aFName[] = $iFile.'.flv';
            $aFName[] = $iFile.'.mpg';
            $oVoting = new BxDolVoting ('gvideo', 0, 0);
			break;
	}

	foreach($aFName as $iK => $sVal)
	{
		$sFilePath = BX_DIRECTORY_PATH_ROOT.$sModPath.$sVal;
		@unlink($sFilePath);
    }    

	$sCond = ($sType == 'Photo') ? " `medID`=$iFile" : "`ID`=$iFile" ;

    db_res("DELETE FROM $sTableName WHERE $sCond");
    
    // Clear this media and comments assigned to it from newsfeedtrack table
    $newsFeedTypeFilter = ($sType == 'Photo') ? '(type=3 OR type=4)' : '(type=5 OR type=6)';
    db_res("DELETE FROM NewsFeedTrack WHERE mediaid=$iFile AND $newsFeedTypeFilter");

    $oVoting->deleteVotings ($iFile);

    if ($aFile['eventOwnerID'] > 0)
    	header("Location: {$site['url']}events.php?action=show_info&event_id={$aFile['eventID']}");
	elseif ($sReferer != '')
		header('Location:' . $sReferer);
	else
    	header('Location:' . $_SERVER["HTTP_REFERER"]);
}



function deleteProfileGalleries($iUser)

{

	$aType = array(

	'Photo'=>array('medProfId','sharePhotoFiles'),

	'Video'=>array('Owner','RayMovieFiles'),

	'Music'=>array('Owner','RayMusicFiles')

	);

	

	foreach ($aType as $sKey=>$sVal)

	{

		$sqlQuery = "SELECT * FROM `{$sVal[1]}` WHERE `{$sVal[0]}`='$iUser'";

		$rFiles = db_res($sqlQuery);

		

		if (mysql_num_rows($rFiles))

		{

			while ($aFile = mysql_fetch_array($rFiles))

			{

				$iID = isset($aFile['medID']) ? $aFile['medID'] : $aFile['ID'] ;

				$sExt = isset($aFile['medExt']) ? $aFile['medExt'] : '';

				deleteMedia($iID, $sKey, $sExt);

			}

		}	

	}

}



function approveMedia($iFile, $sType)

{

	$sId = 'ID';

	switch ($sType)

	{

		case 'Photo':

			$sTableName = '`sharePhotoFiles`';

			$sId = 'med'.$sId;

			break;

		case 'Music':

			$sTableName = '`RayMusicFiles`';

			break;

		case 'Video':

			$sTableName = '`RayMovieFiles`';

			break;

	}

	$sqlQuery = "UPDATE $sTableName SET `Approved` = 'true' WHERE `$sId`='$iFile'";

	db_res($sqlQuery);

	header('Location:' . $_SERVER["HTTP_REFERER"]);

}



function addMediaComment($iFile, $iUser, $sText, $sType)

{

	$sQuery = "INSERT INTO `share".$sType."Comments` (`medID`, `commDate`, `profileID`, `commText`) 

	VALUES('$iFile', NOW(), '$iUser','$sText')";

	

	db_res($sQuery);

}

// Tracks photo and video comments
function trackMediaComment($iFile, $iUser, $sText, $sType, $commentId=0)
{
	// Get user nickname
	$sUserNickQuery = "SELECT NickName
		FROM `Profiles` 
		WHERE ID = $iUser LIMIT 1";
	$userNick = db_value($sUserNickQuery);
	if (!$userNick) return;
	
	if ($sType == 'Photo')
	{
		$newsFeedType = 4;
		
		// Get file extension
		$imageExt = db_value("SELECT medExt FROM `sharePhotoFiles` WHERE medID = $iFile LIMIT 1");
		$imagePath = "{$iFile}_t.$imageExt";
		
		// Get owner and title of media file
		$sOwnerQuery = "SELECT `sharePhotoFiles`.medProfId as ownerid, 
				`sharePhotoFiles`.medTitle as title, `Profiles`.NickName as ownernick
			FROM `sharePhotoFiles` 
			LEFT JOIN `Profiles` ON `Profiles`.ID = `sharePhotoFiles`.medProfId
			WHERE `sharePhotoFiles`.medID = $iFile LIMIT 1";
		$owner = db_assoc_arr($sOwnerQuery);
		if (!$owner) return;
		
		$title = trim($owner['title']);
		$title = ($title == '') ? 'Untitled' : process_db_input(TrimNewsFeedContent($title));
	}
	elseif ($sType == 'Video')
	{
		$newsFeedType = 6;
		$imagePath = "{$iFile}_small.jpg";
		
		// Get owner and title of media file
		$sOwnerQuery = "SELECT `RayMovieFiles`.Owner as ownerid,
				`RayMovieFiles`.Title as title, `Profiles`.NickName as ownernick
			FROM `RayMovieFiles` 
			LEFT JOIN `Profiles` ON `Profiles`.ID = `RayMovieFiles`.Owner
			WHERE `RayMovieFiles`.ID = $iFile LIMIT 1";
		$owner = db_assoc_arr($sOwnerQuery);
		if (!$owner) return;

		$title = trim($owner['title']);
		$title = ($title == '') ? 'Untitled' : process_db_input(TrimNewsFeedContent($title));
	}
		
	$time = time();
	$comment = TrimNewsFeedContent($sText);
	
	// Update news-feed track table
	$sQuery = "INSERT INTO `NewsFeedTrack` 
		(`type`, `date`, `actorid`, `actornick`, `targetid`, `targetnick`, `mediaid`, `path`, `content`, `content2`, `commentid`) 
		VALUES ($newsFeedType, $time, $iUser, '$userNick', {$owner['ownerid']}, '{$owner['ownernick']}', 
			$iFile, '$imagePath', '$title', '$comment', $commentId)";
	db_res($sQuery);
}

function commentNavigation($iNumber, $iDivis, $iCurr, $profileID=false)
{
	global $site;
	global $aFile;

	$iPages = ceil($iNumber / $iDivis);
	
	$sCode = '<div id="commentNavigation">';
	
	$commPagePos = strpos($_SERVER['REQUEST_URI'], 'commPage');
	if ($profileID)
		$baseUrl = "{$site['url']}profile.php?ID=$profileID";
	else
		$baseUrl = ($commPagePos !== false) ? substr($_SERVER['REQUEST_URI'], 0, $commPagePos - 1) : $_SERVER['REQUEST_URI'];
	$commPageUrl = (strpos($baseUrl, '&') !== false) ?
		"$baseUrl&amp;commPage=" : "$baseUrl?commPage=";

	for ($i = 1; $i < $iPages + 1; $i++)
	{
		$sCapt = $i == 1 ? _t("_Page").': ' : '' ;
		$sCode .= '<div class="commentNavUnit">'.$sCapt;
		$sLink =  ($i != $iCurr) ? "<a href=\"{$commPageUrl}$i\">$i</a>" : $iCurr;
		$sCode .= $sLink.'</div>';
	}
	
	if ($iPages > 1)
	{
		$nextPage = $iCurr + 1;
		$prevPage = $iCurr - 1;

		if ($iCurr == 1)
			$sCode .= "<a href=\"{$commPageUrl}$nextPage\" style=\"float:right; margin:5px; padding-left:5px\">Next</a>";
		elseif ($iCurr == $iPages)
			$sCode .= "<a href=\"{$commPageUrl}$prevPage\" style=\"float:right; margin:5px\">Prev</a>";
		else 
		{
			$sCode .= "<a href=\"{$commPageUrl}$nextPage\" style=\"float:right; margin:5px; padding-left:5px\">Next</a>";
			$sCode .= "<a href=\"{$commPageUrl}$prevPage\" style=\"float:right; margin:5px\">Prev</a>";
		}
	}
	$sCode .= '<div class="clear_both"></div>';
	$sCode .= '</div>';

	return $sCode;
}



function getTagLinks($sTagList, $sType)

{

	global $site;

	

	if (strlen($sTagList))

	{

		$aTags = explode(' ', $sTagList);

		foreach ($aTags as $iKey => $sVal)

		{

			$sVal   = trim($sVal,',');

			$sCode .= '<a href="'.$site['URL'].'browse'.$sType.'.php?tag='.$sVal.'">'.$sVal.'</a> ';

		}

	}

	

	return $sCode;

}



function defineBrowseAction($sAct, $sType, $iUser = 0)

{

	global $member;

	

	$sqlQuery = '';

	switch ($sAct)

	{

		case 'fav':

			$sUserCond = $iUser != 0 ? " AND `userID`=$iUser" : "" ;

			$sqlQuery = "SELECT `medID` FROM `share".$sType."Favorites` WHERE 1 ".$sUserCond;

			$sType = $sType == 'Video' ? 'Movie' : $sType;



			if ($sType == 'Photo')

			{

				$sRes = " AND `share{$sType}Files`.`medID` IN(";

			}

			else

			{

				$sRes = " AND `Ray{$sType}Files`.`ID` IN(" ;

			}

			$rList = db_res($sqlQuery);

			

			while ($aList = mysql_fetch_row($rList))

			{

				$sParam .= $aList[0] . ',';

			}

			if (strlen($sParam) > 0)

			{

				$sRes = $sRes.trim($sParam,',').')';

			}

			else

			{

				$sRes =' AND 0';

			}

			break;

		case 'del':

			if (isset($_GET['fileID']))
			{
				$iFile = (int)$_GET['fileID'];
				deleteMedia($iFile, $sType);
			}
			$sRes = '';
			break;	
	}

	return $sRes;

}



function getSitesArray($iFile, $sType)

{

	global $site;

	

	$sLink = $site['url'].'viewMusic.php?fileID='.$iFile;

	$aSites = array(

		array(

		'image'=>'digg.png',

		'link'=>'http://digg.com/submit?phase=2&url='.$sLink

		),

		array(

		'image'=>'delicious.png',

		'link' =>'http://del.icio.us/post?url='.$sLink

		),

		array(

		'image'=>'blinklist.png',

		'link' =>'http://www.blinklist.com/index.php?Action=Blink/addblink.php&amp;Url='.$sLink

		),

		array(

		'image'=>'furl.png',

		'link' =>'http://www.furl.net/storeIt.jsp?u='.$sLink

		),

		array(

		'image'=>'netscape.gif',

		'link' =>'http://www.netscape.com/submit/?U='.$sLink

		),

		array(

		'image'=>'newsvine.png',

		'link' =>'http://www.newsvine.com/_tools/seed&save?u='.$sLink

		),

		array(

		'image'=>'reddit.png',

		'link' =>'http://reddit.com/submit?url='.$sLink

		),

		array(

		'image'=>'shadows.png',

		'link' =>'http://www.shadows.com/features/tcr.htm?url='.$sLink

		),

		array(

		'image'=>'slashdot.png',

		'link' =>'http://slashdot.org/bookmark.pl?url='.$sLink

		),

		array(

		'image'=>'sphere.png',

		'link' =>'http://www.sphere.com/search?q=sphereit:'.$sLink

		),

		array(

		'image'=>'stumbleupon.png',

		'link' =>'http://www.stumbleupon.com/url/http'.$sLink

		),

		array(

		'image'=>'technorati.png',

		'link' =>'http://technorati.com/faves?add='.$sLink

		)

	);

	$sLink = '<a href="{Link}"><div class="shareLink" style="background-image:url(\'{Image}\')"></div></a>';



	foreach ($aSites as $iKey =>$sVal)

	{

		$sLinkCur = str_replace('{Image}', getTemplateIcon($sVal['image']),$sLink);

		$sLinkCur = str_replace('{Link}', $sVal['link'],$sLinkCur);

		$sCode   .= $sLinkCur;

	}

	$sCode .= '<div class="clear_both"></div>';

	

	return $sCode;

}

function PageCompSharePhotosContent($sCaption, $iID = 0, $assocID = 0, $assocType = '')

{
	global $site;

	//$max_num	= (int)getParam("top_photos_max_num");
	$max_num = ($iID) ? (int)getParam('photos_number_on_profile_page') : (int)getParam('photos_number_on_homepage');
	$mode		= process_db_input( getParam("top_photos_mode") );
	
	$mode = $_GET['shPhotosMode'];
	if( $mode != 'rand' && $mode != 'top' && $mode != 'last')
		//$mode = 'last';
		$mode = 'rand';
	
	$sqlSelect = "SELECT `medID`,
						 `medExt`,
						 `medTitle`";
	$sqlFrom  = " FROM `sharePhotoFiles`";
	if ($assocID != 0 && $assocType != '')
		$sqlWhere = " WHERE `AssocID`=$assocID AND `AssocType`='$assocType'";
	elseif ($iID != 0)
		$sqlWhere = " WHERE `Approved`='true' AND `medProfId`='$iID'";
	
	$menu = '<div class="dbTopMenu">';
	 foreach( array( 'top', 'rand' ) as $myMode )
	 {
	  switch ( $myMode )
	  {
	   case 'last':
	    if( $mode == $myMode )
	     $sqlOrder = "
	  		ORDER BY `medDate` DESC";
	     $modeTitle = _t('_Latest');
	   break;
	   case 'rand':
	    if( $mode == $myMode )
	     $sqlOrder = "
	  		ORDER BY RAND()";
	     $modeTitle = _t('_Random');
	   break;
	   case 'top':
	    if( $mode == $myMode )
	    {
			$oVotingView = new BxTemplVotingView ('gphoto', 0, 0);
			$aSql        = $oVotingView->getSqlParts('`sharePhotoFiles`', '`medID`');
			$sHow        = "DESC";
			$sqlOrderBy  = $oVotingView->isEnabled() ? "ORDER BY `voting_rate` $sHow, `voting_count` $sHow, `medDate` $sHow" : $sqlOrderBy ;
			$sqlFields   = $aSql['fields'];
			$sqlLJoin    = $aSql['join'];
	
		    $sqlSelect  .= $sqlFields;
		    $sqlFrom    .= $sqlLJoin;
		    $sqlOrder    = $sqlOrderBy;
	    }
	    $modeTitle = _t('_Top');
	   break;
	  }
	  	 if( $myMode == $mode )
	   		$menu .= "<div class=\"active\">$modeTitle</div>";	
	  else
	  {
	  	if( basename( $_SERVER['PHP_SELF'] ) == 'rewrite_name.php' || basename( $_SERVER['PHP_SELF'] ) == 'profile.php' )
	  		$menu .= "<div class=\"notActive\"><a href=\"profile.php?ID={$iID}&amp;shPhotosMode=$myMode\" 
	  			class=\"top_members_menu\" onclick=\"getHtmlData( 'show_sharePhotos', this.href+'&amp;show_only=sharePhotos'); 
	  			return false;\">$modeTitle</a></div>";
	  	else
	   		$menu .= "<div class=\"notActive\"><a href=\"{$_SERVER['PHP_SELF']}?action=show_info&amp;event_id=$assocID&amp;shPhotosMode=$myMode\" 
	   			class=\"top_members_menu\" onclick=\"getHtmlData('show_sharePhotos', this.href+'&amp;show_only=sharePhotos'); return false;\">$modeTitle</a></div>";
	  }
	 }
	 $menu .= '</div>';
	 
	$aNum = db_arr( "SELECT COUNT(`sharePhotoFiles`.`medID`) $sqlFrom $sqlWhere" );
	$num = (int)$aNum[0];
	$photosStr = ($num > 1) ? ' Photos' : ' Photo';
	
	if ($assocID != 0 && $assocType == 'event')
		$menu .= '<div class="caption_item">
			<a href="'.$site['url'].'browsePhoto.php?eventID='.$assocID.'">'.$num.$photosStr.'</a></div>';	
	elseif ($iID)
		$menu .= '<div class="caption_item">
			<a href="'.$site['url'].'browsePhoto.php?userID='.$iID.'">'.$num.$photosStr.'</a></div>';
	
	$ret = '';
	if( $num )
	{
		$pages = ceil( $num / $max_num );
		$page = (int)$_GET['page_p'];
		
		if( $page < 1 or $mode == 'rand' )
			$page = 1;
		if( $page > $pages )
			$page = $pages;
		
		$sqlLimitFrom = ( $page - 1 ) * $max_num;
		$sqlLimit = "
		LIMIT $sqlLimitFrom, $max_num";
		
		if ($max_num < 1)
			$sqlLimit = " LIMIT 8";
			

	 $rData = db_res($sqlSelect.$sqlFrom.$sqlWhere.$sqlOrder.$sqlLimit);
	 
	 $ret .= '<div class="clear_both"></div>';
	 $iCounter = 0;
	 $photos_per_row = ($iID || $assocType == 'event') ? 3 : 4; // 4 photos per row - for home page
	 $sAddon = '';
	 $ret .= "<table>";	 
	 
	 while ($aData = mysql_fetch_array($rData))
	 {
	 	$sImage = $site['sharingImages'].$aData['medID'].'_t.'.$aData['medExt'];
		$oVotingView = new BxTemplVotingView ('gphoto', $aData['medID']);
	    if( $oVotingView->isEnabled())
	   	{
			$sRate = $oVotingView->getSmallVoting (0);
			$sShowRate = '<div class="galleryRate">'. $sRate . '</div>';
		}
		$sHref = $site['url'].'viewPhoto.php?fileID='.$aData['medID'];
		/*
		$sImg  = '<div class="lastFilesPic" style="background-image: url(\''.$sImage.'\');">
				  <a href="'.$sHref.'"><img src="'.$site['images'] .'spacer.gif" width="110" height="110"></a></div><div class="clear_both"></div>';
		if( ($iCounter % 3) != 0 )
			$ret .= '<div class="sharePhotosContent_1">';
		else
		{
			$ret .= '<div class="sharePhotosContent_2">';
			$sAddon = '<div class="clear_both"></div>';
		}
		$sTitle = strlen($aData['medTitle']) > 0 ? $aData['medTitle'] : _t("_Untitled");
		$ret .= $sImg.'<div><a href="'.$sHref.'">'.$sTitle.'</a></div>'.$sShowRate.'</div>';
		$ret .= $sAddon;
		$sAddon = '';
		*/

		if ($iCounter % $photos_per_row == 0)
			$ret .= "<tr style=\"vertical-align:top\">";
			
		$sTitle = strlen($aData['medTitle']) > 0 ? $aData['medTitle'] : _t("_Untitled");
				
		$ret .= '<td>
				 <div style="background-position: center; background-repeat: no-repeat; border:1px solid #CCCCCC; width:100px; height:110px; background-image: url(\''.$sImage.'\');">
				 <a href="'.$sHref.'"><img src="'.$site['images'] .'spacer.gif" width="110" height="110" alt="photo"/></a>				
				 </div>
				 ' . $sImg . '<div class="photoInfoTitle"><a href="'.$sHref.'">'.$sTitle.'</a></div>'.$sShowRate.'
				 </td>';
		
		if (($iCounter + 1) % $photos_per_row == 0)
			$ret .= "</tr>";
		
		
		$iCounter++;
	 }
	 $ret .= "</table>";	 
	 $ret .= '<div class="clear_both"></div>';

	 if ($pages > 1 && !$iID) // show 'Back/Next' links only on the homepage
	 {
	 	$ret .= '<div class="dbBottomMenu">';

		if( $page > 1 ) {
			$prevPage = $page - 1;
			$ret .= "
				<a href=\"{$_SERVER['PHP_SELF']}?shPhotosMode=$mode&amp;page_p=$prevPage\" class=\"backMembers\" 
					onclick=\"getHtmlData( 'show_sharePhotos', this.href+'&amp;show_only=sharePhotos'); return false;\">"._t('_Back')."</a>";
		}

		if( $page < $pages ) {
			$nextPage = $page + 1;
			$ret .= "
				<a href=\"{$_SERVER['PHP_SELF']}?shPhotosMode=$mode&amp;page_p=$nextPage\" class=\"moreMembers\"
					onclick=\"getHtmlData( 'show_sharePhotos', this.href+'&amp;show_only=sharePhotos'); return false;\">"._t('_Next')."</a>";
		}

		$ret .= '<div class="clear_both"></div></div>';
	 }
	 
	 $ret = DesignBoxContent( _t( $sCaption ), $ret, 1, $menu, '', 'float:left');
	 
	}

 return $ret;
}

function PageCompShareVideosContent($sCaption, $iID = 0, $assocID = 0, $assocType = '')
{
	global $site;
	
	$max_num	= (int)getParam("top_photos_max_num");
	$max_num	= 3;
	$mode		= process_db_input( getParam("top_photos_mode") );
	
	$mode = $_GET['shVideosMode'];
	if( $mode != 'rand' && $mode != 'top' && $mode != 'last')
		$mode = 'rand';
	
	$sqlSelect = "SELECT `ID` as `medID`,
						 `Title` as `medTitle`,
						 `Date` as `medDate`
						 ";
	$sqlFrom  = " FROM `RayMovieFiles`";
	if ($assocID != 0 && $assocType != '')
		$sqlWhere = " WHERE `AssocID`=$assocID AND `AssocType`='$assocType'";
	elseif ($iID != 0)
		$sqlWhere = " WHERE `Approved`='true' AND `Owner`='$iID'";
	
	$menu = '<div class="dbTopMenu">';
	 foreach( array( 'top', 'rand' ) as $myMode )
	 {
	  switch ( $myMode )
	  {
	   case 'last':
	    if( $mode == $myMode )
	     $sqlOrder = "
	  		ORDER BY `medDate` DESC";
	     $modeTitle = _t('_Latest');
	   break;
	   case 'rand':
	    if( $mode == $myMode )
	     $sqlOrder = "
	  		ORDER BY RAND()";
	     $modeTitle = _t('_Random');
	   break;
	   case 'top':
	    if( $mode == $myMode )
	    {
			$oVotingView = new BxTemplVotingView ('gvideo', 0, 0);
			$aSql        = $oVotingView->getSqlParts('`RayMovieFiles`', '`ID`');
			$sHow        = "DESC";
			$sqlOrderBy  = $oVotingView->isEnabled() ? "ORDER BY `voting_rate` $sHow, `voting_count` $sHow, `medDate` $sHow" : $sqlOrderBy ;
			$sqlFields   = $aSql['fields'];
			$sqlLJoin    = $aSql['join'];
	
		    $sqlSelect  .= $sqlFields;
		    $sqlFrom    .= $sqlLJoin;
		    $sqlOrder    = $sqlOrderBy;
	    }
	    $modeTitle = _t('_Top');
	   break;
	  }
	  if( $myMode == $mode )
	   		$menu .= "<div class=\"active\">$modeTitle</div>";
	  else
	  {
	  	if( basename( $_SERVER['PHP_SELF'] ) == 'rewrite_name.php' || basename( $_SERVER['PHP_SELF'] ) == 'profile.php' )
	  		$menu .= "<div class=\"notActive\"><a href=\"profile.php?ID={$iID}&amp;shVideosMode=$myMode\" class=\"top_members_menu\" onclick=\"getHtmlData( 'show_shareVideos', this.href+'&amp;show_only=shareVideos'); return false;\">$modeTitle</a></div>";
	  	else
	   		$menu .= "<div class=\"notActive\"><a href=\"{$_SERVER['PHP_SELF']}?action=show_info&amp;event_id=$assocID&amp;shVideosMode=$myMode\" class=\"top_members_menu\" onclick=\"getHtmlData( 'show_shareVideos', this.href+'&amp;show_only=shareVideos'); return false;\">$modeTitle</a></div>";
	  }
	 }
	 $menu .= '</div>';
	 
	$aNum = db_arr( "SELECT COUNT(`RayMovieFiles`.`ID`) $sqlFrom $sqlWhere" );
	$num = (int)$aNum[0];
	$videosStr = ($num > 1) ? ' Videos' : ' Video';

	if ($assocID != 0 && $assocType == 'event')
		$menu .= '<div class="caption_item">
			<a href="'.$site['url'].'browseVideo.php?eventID='.$assocID.'">'.$num.$videosStr.'</a></div>';	
	elseif ($iID)
		$menu .= '<div class="caption_item">
			<a href="'.$site['url'].'browseVideo.php?userID='.$iID.'">'.$num.$videosStr.'</a></div>';
	
	$ret = '';
	if( $num )
	{
		$pages = ceil( $num / $max_num );
		$page = (int)$_GET['page_p'];
		
		if( $page < 1 or $mode == 'rand' )
			$page = 1;
		if( $page > $pages )
			$page = $pages;
		
		$sqlLimitFrom = ( $page - 1 ) * $max_num;
		$sqlLimit = "
		LIMIT $sqlLimitFrom, $max_num";

	 $rData = db_res($sqlSelect.$sqlFrom.$sqlWhere.$sqlOrder.$sqlLimit);
	 
	 $ret .= '<div class="clear_both"></div>';
	 
	 $iCounter = 1;
	 $sAddon = '';
	 $ret .= '<div class="clear_both"></div>';
	 $ret .= "<table>";
	 while ($aData = mysql_fetch_array($rData))
	 {
	 	$sHref = $site['url'].'viewVideo.php?fileID='.$aData['medID'];
	 	$sTitleLength = strlen($aData['medTitle']);
	 	if ($sTitleLength > 0)
	 	{
	 		if ($sTitleLength > 30)
	 			$sVidTitle = substr($aData['medTitle'], 0, 27) . '...';
	 		else 
	 			$sVidTitle = $aData['medTitle'];
	 	}
	 	else
	 		$sVidTitle = _t("_Untitled");
	 	
	 	$imagePath = "ray/modules/movie/files/{$aData['medID']}_small.jpg";
		if (file_exists($imagePath))
			$sImg = '<a href="'.$sHref.'"><img src="'.$site['url'].$imagePath.'" 
				width="112px" height="80px" alt="video"/></a>';
		else 
			$sImg = '<div style="border: 1px solid #EDEDED">
	 			<table cellpadding="0" cellspacing="0" style="width:110px; height:80px; background-color:white">
				<tr><td style="text-align:center">
				<a class="thumbUnavailable" href="'.$sHref.'">Thumbnail Unavailable</a></td></tr></table></div>';
			
	 	$oVotingView = new BxTemplVotingView ('gvideo', $aData['medID']);
	    if( $oVotingView->isEnabled())
	   	{
			$sRate = $oVotingView->getSmallVoting (0);
			$sShowRate = '<div class="galleryRate">'. $sRate . '</div>';
		}
	
	if (strstr($_SERVER['PHP_SELF'],"index.php"))
		{
		$ret .= '<tr style="vertical-align:top">';			
		$ret .= '<td>'.$sImg.'<br/><div style="width:100px; overflow:hidden"><a href="'.$sHref.'">'.$sVidTitle.'</a></div>';
		$ret .= $sShowRate.'</td>';		
		$ret .= "</tr>";
		}
	else 
		{
		if ($iCounter % 3 == 1)
			$ret .= '<tr style="vertical-align:top">';
			
		$brStr = (file_exists($imagePath)) ? '<br/>' : '';
		$ret .= '<td style="width:112px">'.$sImg. $brStr .'<div class="videoInfoTitle">
			<a href="'.$sHref.'">'.$sVidTitle.'</a></div>';
		$ret .= $sShowRate.'</td>';
		
		if ($iCounter % 3 == 0)
			$ret .= "</tr>";
				
		}
	 $sAddon = '';
	 $iCounter++;
			
	 }
	$ret .= "</table>";
	
	//$ret .= '<div class="clear_both"></div>';
	$ret = DesignBoxContent( _t( $sCaption ), $ret, 1, $menu, '', 'float:left' );
	} 

	return $ret;
}


function PageCompFriendsContent( $sCaption, $iID = 0 )
{
	global $site;

	$iFriendNums = getFriendNumber( $iID );
	
	$mode = $_GET['profilesMode'];

	if( $mode != 'rand' && $mode != 'top')
	{
		$mode = 'rand';
		$sqlOrder = ($iFriendNums > 12) ? 'ORDER BY `Profiles`.`Picture` DESC'
			: 'ORDER BY RAND()';
	}

	$menu = '<div class="dbTopMenu">';

	foreach( array( 'top', 'rand' ) as $myMode )
	 {
	  switch ( $myMode )
	  {
	   case 'rand':
	   	if( $mode == $myMode && $sqlOrder == '')
	   		$sqlOrder = 'ORDER BY RAND()';
	     $modeTitle = _t('_Random');
	   break;
	   
	   case 'top':
	    if( $mode == $myMode )
	    {

			$oVotingView = new BxTemplVotingView ('profile', 0, 0);
			$aSql        = $oVotingView->getSqlParts('`Profiles`', '`ID`');
			$sHow        = "DESC";
			$sqlOrderBy  = $oVotingView->isEnabled() ? "ORDER BY `voting_rate` $sHow, `voting_count` $sHow" : $sqlOrderBy ;
			$sqlFields   = $aSql['fields'];
			$sqlLJoin    = $aSql['join'];

		    $sqlSelect  .= $sqlFields;
		    $sqlFrom    .= $sqlLJoin;
		    $sqlOrder    = $sqlOrderBy;
	    }
	    $modeTitle = _t('_Top');
	   break;
	  }
	  
	  if( $myMode == $mode )
	   		$menu .= "<div class=\"active\">$modeTitle</div>";
	  else
	  {
	  	if( basename( $_SERVER['PHP_SELF'] ) == 'rewrite_name.php' || basename( $_SERVER['PHP_SELF'] ) == 'profile.php' )
	  		$menu .= "<div class=\"notActive\"><a href=\"profile.php?ID=$iID&profilesMode=$myMode\" 
	  			class=\"top_members_menu\" onclick=\"getHtmlData( 'show_friends', this.href+'&amp;show_only=friends'); return false;\">$modeTitle</a></div>";
	  	else
	   		$menu .= "<div class=\"notActive\"><a href=\"{$_SERVER['PHP_SELF']}?profilesMode=$myMode\" 
	   			class=\"top_members_menu\" onclick=\"getHtmlData( 'show_friends', this.href+'&amp;show_only=friends'); return false;\">$modeTitle</a></div>";
	  }
	 }

	 $menu .= '</div>';

	$sFriendList = ShowFriendList( $iID, $sqlFields, $sqlLJoin, $sqlOrder );
	
	if( $sFriendList )
	{
		ob_start();
		
		?>
			<div class="clear_both"></div>
			<?= $sFriendList ?>
			<div class="clear_both"></div>
		<?
		
		$ret = ob_get_clean();
		
		$sFriendInfo = '<div class="caption_item"><a href="'.$site['url'].
			'viewFriends.php?iUser='.$iID.'">'.$iFriendNums.' '._t("_Friends").'</a></div>';
		
		return DesignBoxContent( _t( $sCaption ), $ret, 1, $menu . $sFriendInfo);
	}
	return '';
}


function PageCompShareMusicContent($sCaption, $iID = 0)

{

	global $site;

	

	$max_num	= (int)getParam("top_photos_max_num");

	$mode		= process_db_input( getParam("top_photos_mode") );

	

	$mode = $_GET['shMusicMode'];

	if( $mode != 'rand' && $mode != 'top' && $mode != 'last')

		$mode = 'last';

	

	$sqlSelect = "SELECT `ID` as `medID`,

						 `Title` as `medTitle`,

						 `Date` as `medDate`

						 ";

	$sqlFrom  = " FROM `RayMusicFiles`";

	if ($iID != 0)

	{

		$sqlWhere = " WHERE `Approved`='true' AND `Owner`='$iID'";

	}	

	

	$menu = '<div class="dbTopMenu">';

	 foreach( array( 'last', 'top', 'rand' ) as $myMode )

	 {

	  switch ( $myMode )

	  {

	   case 'last':

	    if( $mode == $myMode )

	     $sqlOrder = "

	  		ORDER BY `medDate` DESC";

	     $modeTitle = _t('_Latest');

	   break;

	   case 'rand':

	    if( $mode == $myMode )

	     $sqlOrder = "

	  		ORDER BY RAND()";

	     $modeTitle = _t('_Random');

	   break;

	   case 'top':

	    if( $mode == $myMode )

	    {

			$oVotingView = new BxTemplVotingView ('gmusic', 0, 0);

			$aSql        = $oVotingView->getSqlParts('`RayMusicFiles`', '`ID`');

			$sHow        = "DESC";

			$sqlOrderBy  = $oVotingView->isEnabled() ? "ORDER BY `voting_rate` $sHow, `voting_count` $sHow, `medDate` $sHow" : $sqlOrderBy ;

			$sqlFields   = $aSql['fields'];

			$sqlLJoin    = $aSql['join'];

	

		    $sqlSelect  .= $sqlFields;

		    $sqlFrom    .= $sqlLJoin;

		    $sqlOrder    = $sqlOrderBy;

	    }

	    $modeTitle = _t('_Top');

	   break;

	  }

		

	  //if( $_SERVER['PHP_SELF'] == 'rewrite_name.php' )

	  

	  if( $myMode == $mode )

	   		$menu .= "<div class=\"active\">$modeTitle</div>";

	  else

	  {

	  	if( basename( $_SERVER['PHP_SELF'] ) == 'rewrite_name.php' || basename( $_SERVER['PHP_SELF'] ) == 'profile.php' )

	  		$menu .= "<div class=\"notActive\"><a href=\"profile.php?ID={$iID}&shMusicMode=$myMode\" class=\"top_members_menu\" onclick=\"getHtmlData( 'show_shareMusic', this.href+'&amp;show_only=shareMusic'); return false;\">$modeTitle</a></div>";

	  	else

	   		$menu .= "<div class=\"notActive\"><a href=\"{$_SERVER['PHP_SELF']}?shMusicMode=$myMode\" class=\"top_members_menu\" onclick=\"getHtmlData( 'show_shareMusic', this.href+'&amp;show_only=shareMusic'); return false;\">$modeTitle</a></div>";

	  }

	   	

	 }

	 $menu .= '</div>';

	 

	$aNum = db_arr( "SELECT COUNT(`RayMusicFiles`.`ID`) $sqlFrom $sqlWhere" );

	$num = (int)$aNum[0];

	

	$ret = '';

	if( $num )

	{

		$pages = ceil( $num / $max_num );

		$page = (int)$_GET['page_p'];

		

		if( $page < 1 or $mode == 'rand' )

			$page = 1;

		if( $page > $pages )

			$page = $pages;

		

		$sqlLimitFrom = ( $page - 1 ) * $max_num;

		$sqlLimit = "

		LIMIT $sqlLimitFrom, $max_num";

	 

	 $rData = db_res($sqlSelect.$sqlFrom.$sqlWhere.$sqlOrder.$sqlLimit);

	 

	 $sAddon = '';

	 $iCounter = 1;

	 $ret .= '<div class="clear_both"></div>';

	 while ($aData = mysql_fetch_array($rData))

	 {

	 	$sHref = $site['url'].'viewMusic.php?fileID='.$aData['medID'];

	 	$sVidTitle = strlen($aData['medTitle']) > 0 ? $aData['medTitle'] : _t("_Untitled");

		$sImg = '<a href="'.$sHref.'"><img src="'.$site['images'].'music.png"></a>';

		

	 	$oVotingView = new BxTemplVotingView ('gmusic', $aData['medID']);

	    if( $oVotingView->isEnabled())

	   	{

			$sRate = $oVotingView->getSmallVoting (0);

			$sShowRate = '<div class="galleryRate">'. $sRate . '</div>';

		}

		

		if( ($iCounter % 3) != 0 )

			$ret .= '<div class="shareMusicContent_1">';

		else

		{

			$ret .= '<div class="shareMusicContent_2">';

			$sAddon = '<div class="clear_both"></div>';			

		}

		$ret .= '<div class="lastMusicPic">'.$sImg.'</div>';

		$ret .= '<div><a href="'.$sHref.'">'.$sVidTitle.'</a></div>';

		$ret .= $sShowRate.'</div>';

		$ret .= $sAddon;

		$sAddon = '';

		$iCounter++;

	 }

	 

	 $ret .= '<div class="clear_both"></div>';

	 $ret = DesignBoxContent( _t( $sCaption ), $ret, 1, $menu );

	} 



 return $ret;

}

function PageCompOtherSitesLinkContent($savedLinks)
{
	global $tmpl;
	
	$imagespath = "templates/tmpl_$tmpl/images/icons/";
	
	// Prepare mylinks array to show in the table on main page
	$mylinks = array();
	for ($lr = 0; $lr <= 1; $lr++)
	{
		foreach ($savedLinks as $savedLink)
		{
			if ((($lr == 0) xor !$savedLink['isleft']))
			{
				$title = $savedLink['title'];
				$url = $savedLink['url'];
				$image = $savedLink['imagename'];
				$target = ($savedLink['innewwindow']) ? '_blank' : '';
				
				$mylinks[$lr][] = array('title' => $title, 'url' => $url, 'image' => $image, 'target' => $target);
			}
		}
	}
	
	// Show mylinks
	$ret = '<table style="width:100%">';
	
	$maxrows = max(count($mylinks[0]), count($mylinks[1]));
	for ($i = 0; $i < $maxrows; $i++)
	{
		$ret .= '<tr>';
		if (is_array($mylinks[0]) && array_key_exists($i, $mylinks[0]))
			$ret .= "<td><a href=\"{$mylinks[0][$i]['url']}\" target=\"{$mylinks[0][$i]['target']}\">
					<img src=\"$imagespath{$mylinks[0][$i]['image']}\" style=\"vertical-align:middle\"/></a>
				<a href=\"{$mylinks[0][$i]['url']}\" target=\"{$mylinks[0][$i]['target']}\">{$mylinks[0][$i]['title']}</a></td>";
		else 
			$ret .= '<td/>';
		if (is_array($mylinks[1]) && array_key_exists($i, $mylinks[1]))
			$ret .= "<td><a href=\"{$mylinks[1][$i]['url']}\" target=\"{$mylinks[1][$i]['target']}\">
					<img src=\"$imagespath{$mylinks[1][$i]['image']}\" style=\"vertical-align:middle\"/></a>
				<a href=\"{$mylinks[1][$i]['url']}\" target=\"{$mylinks[1][$i]['target']}\">{$mylinks[1][$i]['title']}</a></td>";
		else 
			$ret .= '<td/>';
		$ret .= '</tr>';
	}

	$ret .= '</table>';

	return $ret;
}

// Returns news entries for specified member.
// Limit result entries to $maxNews if specified
function PrintNewsFeed($memberID = 0, $isFriendFeed = true, $maxNews = null)
{
	global $site, $member, $tmpl;
	
	$iPerPage = 30;

	if ($maxNews > 0)
		$sLimit =  " LIMIT $maxNews";
	elseif (isset($_GET['offset']) && $_GET['offset'] > 0)
		$sLimit = ' LIMIT '. (int)$_GET['offset'] .','.$iPerPage;
	else 
		$sLimit = ' LIMIT 0,'.$iPerPage;
		
	if ($memberID == 0)
		$memberID = $member['ID'];		
		
	if ($isFriendFeed)
	{
		$friends = getfriends($memberID);
		if (count($friends) == 0) return 'There is currently no news to display.';
		
		$friendSID = '(';
		foreach ($friends as $friend)
		{
			if (strlen($friendSID) > 1)
				$friendSID .= ',';
			$friendSID .= $friend['Profile'];
		}
		$friendSID .= ')';
		$whereQuery = "actorid IN $friendSID OR targetid IN $friendSID";
	}
	else
		$whereQuery = "(actorid = $memberID OR targetid = $memberID)
			AND type NOT IN (SELECT typeid FROM NewsFeedOff WHERE ownerid=$memberID)";
	
	$query = "SELECT * FROM NewsFeedTrack 
		LEFT JOIN NewsFeedTypes ON NewsFeedTypes.id = NewsFeedTrack.type
		WHERE $whereQuery
		ORDER BY `date` DESC
		$sLimit";		
	$news = fill_assoc_array(db_res($query));
	
	if (!$news) return 'There is currently no news to display.';
	
	$newsFeedImagesFolder = 'media/images/newsfeed/';
	$profileImagesFolder = 'media/images/profile/';
	$photosFolder = 'media/images/sharingImages/';
	$videosFolder = 'ray/modules/movie/files/';
	
	foreach ($news as &$newsItem)
	{		
		if ($newsItem['type'] != 1)
			$newsIcon = '<img src="' . $newsFeedImagesFolder . $newsItem['image'] . '" class="newsIcon"/>';
		else // hide news icon on new friend connections
			$newsIcon = '';
			
		$date = '<span class="newsDate">' . gmdate("j M H:i", $newsItem['date']) . '</span>';
	
		// Prepare title template
		if (strpos($newsItem['title_template'], '{his}') !== false)
		{
			$hisStr = 'his';
			$targetSexQuery = "SELECT Sex FROM Profiles
				WHERE ID={$newsItem['targetid']} LIMIT 1";
			$targetSex = db_value($targetSexQuery);
			if ($targetSex == 'female')
				$hisStr = 'her';
			$newsItem['title_template'] = str_replace('{his}', $hisStr, $newsItem['title_template']);
		}
		$newsItem['title_template'] = str_replace('{actornick}', 
			"<a href=\"{$newsItem['actornick']}\">{$newsItem['actornick']}</a>", $newsItem['title_template']);
		$newsItem['title_template'] = str_replace('{targetnick}', 
			"<a href=\"{$newsItem['targetnick']}\">{$newsItem['targetnick']}</a>", $newsItem['title_template']);
		$newsItem['title_template'] = $newsIcon . $date . $newsItem['title_template'];
		
		// Prepare body template
		$newsItem['body_template'] = str_replace('{content2}', $newsItem['content2'], $newsItem['body_template']);
		if ($newsItem['type'] == 3 || $newsItem['type'] == 4) // photo or photo comment
		{
			$ownerID = ($newsItem['type'] == 3) ? $newsItem['actorid'] : $newsItem['targetid'];
			$photoLink = "viewPhoto.php?fileID={$newsItem['mediaid']}";
			if (useSEF)
			{
				$urltitle = db_value("SELECT urltitle FROM sharePhotoFiles WHERE medID = {$newsItem['mediaid']}");
				$ownerNick = getNickName($ownerID);
				if ($urltitle != '' && $ownerNick != '')
					$photoLink = "$ownerNick/photo/$urltitle.html";
			}
			$photoPath = $photosFolder . $newsItem['path'];
			$newsItem['title_template'] = str_replace('{photolink}', 
				"<a href=\"$photoLink\">{$newsItem['content']}</a>", $newsItem['title_template']);
			$newsItem['body_template'] = str_replace('{photo}', 
				"<a href=\"$photoLink\"><img class=\"newsPhoto\" src=\"$photoPath\" alt=\"news photo\"/></a>", $newsItem['body_template']);
		}
		elseif ($newsItem['type'] == 5 || $newsItem['type'] == 6) // video or video comment
		{
			$ownerID = ($newsItem['type'] == 5) ? $newsItem['actorid'] : $newsItem['targetid'];
			$videoLink = "viewVideo.php?fileID={$newsItem['mediaid']}";
			if (useSEF)
			{
				$urltitle = db_value("SELECT urltitle FROM RayMovieFiles WHERE ID = {$newsItem['mediaid']}");
				$ownerNick = getNickName($ownerID);
				if ($urltitle != '' && $ownerNick != '')
					$videoLink = "$ownerNick/video/$urltitle.html";
			}
			$videoPath = $videosFolder . $newsItem['path'];
			$newsItem['title_template'] = str_replace('{videolink}', 
				"<a href=\"$videoLink\">{$newsItem['content']}</a>", $newsItem['title_template']);
			$newsItem['body_template'] = str_replace('{video}', 
				"<a href=\"$videoLink\"><img class=\"newsPhoto\" src=\"$videoPath\"/></a>", $newsItem['body_template']);
		}	
		elseif ($newsItem['type'] == 7 || $newsItem['type'] == 8) // blog post or blog post comment
		{
			$ownerID = ($newsItem['type'] == 7) ? $newsItem['actorid'] : $newsItem['targetid'];
			$blogPostLink = "blogs.php?action=show_member_post&owner_id=$ownerID&post_id={$newsItem['mediaid']}";
			if (useSEF)
			{
				$urltitle = db_value("SELECT urltitle FROM BlogPosts WHERE PostID = {$newsItem['mediaid']}");
				$ownerNick = getNickName($ownerID);
				if ($urltitle != '' && $ownerNick != '')
					$blogPostLink = "$ownerNick/$urltitle.html";
			}
			$newsItem['title_template'] = str_replace('{blogpostlink}', 
				"<a href=\"$blogPostLink\">{$newsItem['content']}</a>", $newsItem['title_template']);
		}	
		elseif (in_array($newsItem['type'], array(9, 10, 11))) // event, join event or event comment
		{
			$ownerID = ($newsItem['type'] == 9) ? $newsItem['actorid'] : $newsItem['targetid'];
			$eventLink = "events.php?action=show_info&event_id={$newsItem['mediaid']}";
			if (useSEF)
			{
				$urltitle = db_value("SELECT urltitle FROM SDatingEvents WHERE ID = {$newsItem['mediaid']}");
				$ownerNick = getNickName($ownerID);
				if ($urltitle != '' && $ownerNick != '')
					$eventLink = "$ownerNick/event/$urltitle.html";
			}
			$newsItem['title_template'] = str_replace('{eventlink}', 
				"<a href=\"$eventLink\">{$newsItem['content']}</a>", $newsItem['title_template']);
		}
		elseif ($newsItem['type'] == 13) // review on syh
		{
			$newsItem['title_template'] = str_replace('{productlink}', 
				"<a href=\"{$newsItem['content2']}#tabbedcontentrounded\">{$newsItem['content']}</a>", $newsItem['title_template']);
			$newsItem['body_template'] = str_replace('{productimg}', 
				"<a href=\"{$newsItem['content2']}\"><img class=\"newsPhoto\" src=\"{$newsItem['path']}\"/></a>", $newsItem['body_template']);
		}
		// yh blog or yh team-blog or yh article post comment
		elseif ($newsItem['type'] == 14 || $newsItem['type'] == 15 || $newsItem['type'] == 16)
			$newsItem['title_template'] = str_replace('{blogpostlink}', 
				"<a href=\"{$newsItem['path']}\">{$newsItem['content']}</a>", $newsItem['title_template']);
		
		// Prepare template
		if (strpos($newsItem['template'], '{actorphoto}') !== false)
		{
			$photoFriendId = ($newsItem['actorid'] == $memberID) ? $newsItem['targetid'] : $newsItem['actorid'];
			
			$friendPhotoQuery = "SELECT med_file FROM media
				LEFT JOIN Profiles ON Profiles.ID=med_prof_id
				WHERE med_prof_id=$photoFriendId AND med_id=Profiles.PrimPhoto
				LIMIT 1";
			$friendPhoto= db_value($friendPhotoQuery);
			if (!$friendPhoto)
				$friendPhoto = "templates/tmpl_{$tmpl}/images/icons/man_medium.gif";
			else
				$friendPhoto = "$profileImagesFolder{$photoFriendId}/thumb_$friendPhoto";
			
			$newsItem['template'] = str_replace('{actorphoto}', 
				"<a href=\"profile.php?ID=$photoFriendId\"><img class=\"newsMemberPhoto\" src=\"$friendPhoto\"/></a>", 
				$newsItem['template']);
		}
		
		$newsItem['template'] = str_replace('{title_template}', $newsItem['title_template'], $newsItem['template']);
		$newsItem['template'] = str_replace('{body_template}', $newsItem['body_template'], $newsItem['template']);	
	}
	
	$tal = new PHPTAL(BX_DIRECTORY_PATH_ROOT . "templates/tmpl_{$tmpl}/newsfeed.html");

	$tal->news = $news;
	
	if ($isFriendFeed)
	{
		// Prepare pagination
		$iNums = db_value("SELECT COUNT(*) FROM NewsFeedTrack 
			LEFT JOIN NewsFeedTypes ON NewsFeedTypes.id = NewsFeedTrack.type
			WHERE $whereQuery");
		if ($iNums > $iPerPage)
		{
			require_once(BX_DIRECTORY_PATH_INC . 'pagination.inc.php');
			
			$paginationConfig = array(
				'base_url' => $_SERVER['REQUEST_URI'],
				'query_string_segment' => 'offset',
				'total_rows' => $iNums,
				'per_page' => $iPerPage,
				'num_links' => 3
			);
			$pagination = new Pagination($paginationConfig);
			$tal->pagination = 'Page: ' . $pagination->create_links();
		}
	}
	
	return $tal->execute();
}

function PageCompNewsFeedContent()
{
	return PrintNewsFeed();
}

function PageCompActivateContent($tal)
{
	global $tmpl;
	
	$tal->jsFiles = array('jquery-1.4.1.min', 'jquery-ui.min', 
		'jquery.validate.min', 'jquery.metadata.pack', 'activate');
	$tal->cssFiles = array('global', 'registerlight', 'registermedium');
	$tal->templatePath = "templates/tmpl_{$tmpl}/";
	$tal->imagesBase = "templates/tmpl_{$tmpl}/images/";
	
	return $tal->execute();
}

?>