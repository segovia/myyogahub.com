// Returns videoservice name if url matches correspond regex in embed array
// or false - otherwise 
function videoSiteInUrl(url)
{
	if (url.length == 0)
		return false;
	
	for (var i = 0; i < embed.length; i++)
		if (url.match(embed[i]['regexUrl']) != null || 
			(embed[i]['regexUrlFeed'] && url.match(embed[i]['regexUrlFeed']) != null))
			return embed[i]['name'];
	
	return false;
}

// Returns videoservice name if embed source matches correspond regex in embed array
// or false - otherwise 
function videoSiteInEmbedSource(embedSource)
{
	if (embedSource.length == 0)
		return false;
	
	for (var i = 0; i < embed.length; i++)
		if (embedSource.match(embed[i]['regexCorrectEmbed']) != null)
			return embed[i]['name'];
	
	return false;
}

function SetErrorMessage(videoservice, errorMsgElement)
{
	var submitButton = document.forms.uploadVideoForm.upload;
	var title = document.forms.uploadVideoForm.title.value;
	if (videoservice)
	{
		errorMsgElement.style.color = 'green';
		errorMsgElement.innerHTML = videoservice;	
		submitButton.disabled = (title == '');
	}
	else
	{
		errorMsgElement.style.color = 'red';
		errorMsgElement.innerHTML = 'Not Supported';
		submitButton.disabled = true;
	}	
}

function CheckSubmitEnabled(editMode)
{
	var submitButton = document.forms.uploadVideoForm.upload;
	var url = document.forms.uploadVideoForm.sharingUrl.value;
	var embedSource = document.forms.uploadVideoForm.embedSource.value;
	var urlErrorMsgElement = document.getElementById('urlErrorMsg');
	var embedErrorMsgElement = document.getElementById('embedErrorMsg');
	var shareTypeList = document.forms.uploadVideoForm.shareType;
	var shareType;
	var videoservice;
	
	for (var i = 0; i < shareTypeList.length; i++)
		if (shareTypeList[i].checked)
			shareType = shareTypeList[i].value;

	if (shareType == 'shareTypeUrl')
	{
		if (url == '')
		{
			urlErrorMsgElement.innerHTML = '';
			submitButton.disabled = (!editMode);
		}
		else
		{
			videoservice = videoSiteInUrl(url);
			SetErrorMessage(videoservice, urlErrorMsgElement);
		}
	}
	else if (shareType == 'shareTypeEmbed')
	{
		if (embedSource == '')
		{
			embedErrorMsgElement.innerHTML = '';
			submitButton.disabled = (!editMode);
		}
		else
		{
			videoservice = videoSiteInEmbedSource(embedSource);
			SetErrorMessage(videoservice, embedErrorMsgElement);
		}
	}
}

function ToggleShareType(selectedType, editMode)
{
	var url = document.forms.uploadVideoForm.sharingUrl;
	var embedSource = document.forms.uploadVideoForm.embedSource;
	var urlErrorMsgElement = document.getElementById('urlErrorMsg');
	var embedErrorMsgElement = document.getElementById('embedErrorMsg');
	
	if (selectedType == null) // initialize url and embed inputs
	{
		document.forms.uploadVideoForm.shareType[0].checked = true;
		document.forms.uploadVideoForm.shareType[1].checked = false;
		url.disabled = false;
		embedSource.disabled = true;
		url.value = '';
		embedSource.value = '';
		urlErrorMsgElement.innerHTML = '';
		embedErrorMsgElement.innerHTML = '';
	}
	
	if (selectedType == 'shareTypeEmbed')
	{
		url.value = '';
		urlErrorMsgElement.innerHTML = '';
		url.disabled = true;
		embedSource.disabled = false;
	}
	else if (selectedType == 'shareTypeUrl')
	{
		embedSource.value = '';
		embedErrorMsgElement.innerHTML = '';
		embedSource.disabled = true;
		url.disabled = false;
	}
	CheckSubmitEnabled(editMode);
}

function UpdateMaxDescChars(maxChars)
{
	var desc = document.forms.uploadVideoForm.description.value;
 	var counter = document.getElementById("descCounter").getElementsByTagName('span')[0];
 	if (desc.length > maxChars)
 	{
 		document.forms.uploadVideoForm.description.value = desc.substr(0, maxChars);
 		counter.innerHTML = 0;
 		return false;
 	}
 	else
 	{
 		var leftChars = maxChars - desc.length;
 		counter.innerHTML = leftChars;
 	}
}

function showVideoService(videoservice, msgElementId)
{
	var msgElement = document.getElementById(msgElementId);
	if (!msgElement) return;
	if (videoservice)
	{
		msgElement.style.color = 'green';
		msgElement.innerHTML = videoservice;	
	}
	else
	{
		msgElement.style.color = 'red';
		msgElement.innerHTML = 'Not Supported';
	}		
}

function checkSyndVideoService()
{
	var syndVideoUrl = $('input[name="syndVideoUrl"]').val();
	if (syndVideoUrl.trim())
		showVideoService(videoSiteInUrl(syndVideoUrl), 'syndVideoService');
	else
		document.getElementById('syndVideoService').innerHTML = '';
}