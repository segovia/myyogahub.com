<?



/***************************************************************************

*                            Dolphin Smart Community Builder

*                              -----------------

*     begin                : Mon Mar 23 2006

*     copyright            : (C) 2006 BoonEx Group

*     website              : http://www.boonex.com/

* This file is part of Dolphin - Smart Community Builder

*

* Dolphin is free software. This work is licensed under a Creative Commons Attribution 3.0 License. 

* http://creativecommons.org/licenses/by/3.0/

*

* Dolphin is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;

* without even the implied warranty of  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

* See the Creative Commons Attribution 3.0 License for more details. 

* You should have received a copy of the Creative Commons Attribution 3.0 License along with Dolphin, 

* see license.txt file; if not, write to marketing@boonex.com

***************************************************************************/



require_once("header.inc.php");

$statesUS = array('AL' => 'Alabama', 'AK' => 'Alaska', 'AZ' => 'Arizona', 'AR' => 'Arkansas', 'CA' => 'California', 'CO' => 'Colorado', 'CT' => 'Connecticut', 'DE' => 'Delaware', 'DC' => 'District of Columbia', 'FL' => 'Florida', 'GA' => 'Georgia', 'GU' => 'Guam', 'HI' => 'Hawaii', 'ID' => 'Idaho', 'IL' => 'Illinois', 'IN' => 'Indiana', 'IA' => 'Iowa', 'KS' => 'Kansas', 'KY' => 'Kentucky', 'LA' => 'Louisiana', 'ME' => 'Maine', 'MD' => 'Maryland', 'MA' => 'Massachusetts', 'MI' => 'Michigan', 'MN' => 'Minnesota', 'MS' => 'Mississippi', 'MO' => 'Missouri', 'MT' => 'Montana', 'NE' => 'Nebraska', 'NV' => 'Nevada', 'NH' => 'New Hampshire', 'NJ' => 'New Jersey', 'NM' => 'New Mexico', 'NY' => 'New York', 'NC' => 'North Carolina', 'ND' => 'North Dakota', 'OH' => 'Ohio', 'OK' => 'Oklahoma', 'OR' => 'Oregon', 'PA' => 'Pennsylvania', 'PR' => 'Puerto Rico', 'RI' => 'Rhode Island', 'SC' => 'South Carolina', 'SD' => 'South Dakota', 'TN' => 'Tennessee', 'TX' => 'Texas', 'UT' => 'Utah', 'VT' => 'Vermont', 'VI' => 'Virgin Islands', 'VA' => 'Virginia', 'WA' => 'Washington', 'WV' => 'West Virginia', 'WI' => 'Wisconsin', 'WY' => 'Wyoming');
$statesCA = array('AB' => 'Alberta', 'BC' => 'British Columbia', 'MB' => 'Manitoba', 'NB' => 'New Brunswick', 'NF' => 'Newfoundland/Labrador', 'NT' => 'Northwest Territories', 'NS' => 'Nova Scotia', 'NU' => 'Nunavut', 'ON' => 'Ontario', 'PE' => 'Prince Edward Island', 'QC' => 'Quebec', 'SK' => 'Saskatchewan', 'YT' => 'Yukon');

/*

 * Common functions

 */

function PrintErrorPageCode( $errorText )

{

	ob_start();



?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>

	<head>

		<title>Error</title>



		<style type="text/css">

			div.error_container

			{

				margin-top: 2px;

				margin-left: auto;

				margin-right: auto;

				width: 400px;

			}

			div.error_head

			{

				font-weight: bold;

				font-family: Arial, Helvetica, sans-serif;

				font-size: 11px;

				padding: 2px;

				margin-left: auto;

				margin-right: auto;

				margin-bottom: 2px;

				text-align: left;

				color: white;

				background-color: red;

				width: 100%;

				border: 1px solid red;

			}

			div.error_body

			{

				font-weight: normal;

				font-family: Arial, Helvetica, sans-serif;

				font-size: 11px;

				padding: 2px;

				margin-left: auto;

				margin-right: auto;

				text-align: left;

				color: black;

				background-color: white;

				width: 100%;

				border: 1px solid red;

			}

		</style>



	</head>

	<body>



		<div class="error_container">

			<div class="error_head">Error</div>

			<div class="error_body"><?= $errorText ?></div>

		</div>



	</body>

</html>

<?



	$content = ob_get_contents();

	ob_end_clean();



	return $content;

}



/*

 * Print error page with message

 */

function PrintErrorPage( $errorText )

{

	echo PrintErrorPageCode( $errorText );

}



/*

 * function for work with profile

 */

function is_friends($id1, $id2)
{
	$cnt = db_arr("SELECT COUNT(`Check`) AS cnt FROM `FriendList` 
		WHERE (ID = $id1 AND Profile = $id2 OR ID = $id2 AND Profile = $id1) AND `Check` = 1");
	return $cnt['cnt'] > 0;
}

function is_hotlist_friends($id1, $id2)
{
	$cnt = db_arr("SELECT COUNT(`ID`) AS cnt FROM `HotList` WHERE `ID`='{$id1}' AND `Profile`='{$id2}'");

	return $cnt['cnt'] > 0;	
}



/*

 * functions for limiting maximal word length

 */

function strmaxwordlen($input, $len = 100)

{

/*

	$l = 0;

	$output = "";

	$input_len = strlen($input);

	$word_break_array = array(" ", "\r", "\n", "\t");

	for ($i = 0; $i < $input_len; $i++)

	{

		$char = $input{$i};

		if ( !in_array($char, $word_break_array) )

			$l++;

		else

			$l = 0;

		if ($l == $len)

		{

			$l = 0;

			$output .= " ";

		}

		$output .= $char;

    }

*/

	$output = wordwrap($input, $len, " ", true);



	return $output;

}



/*

 * functions for limiting maximal text length

 */

function strmaxtextlen($input, $len = 60)

{

	if ( strlen($input) > $len )

		return substr($input, 0, $len - 4) . "...";

	else

		return $input;

}



function html2txt($content, $tags = "")

{

	while($content != strip_tags($content, $tags))

	{

		$content = strip_tags($content, $tags);

	}



	return $content;

}



function html_encode($text)

{

     $searcharray =  array(

    "'([-_\w\d.]+@[-_\w\d.]+)'",

    "'((?:(?!://).{3}|^.{0,2}))(www\.[-\d\w\.\/]+)'",

    "'(http[s]?:\/\/[-_~\w\d\.\/]+)'");



    $replacearray = array(

    "<a href=\"mailto:\\1\">\\1</a>",

    "\\1http://\\2",

    "<a href=\"\\1\" target=_blank>\\1</a>");



   return preg_replace($searcharray, $replacearray, stripslashes($text));

}



/*

 * functions for input data into database

 */

function process_db_input( $text, $strip_tags = 0, $force_addslashes = 0 )

{

	if ( $strip_tags )

		$text = strip_tags($text);



	if ( !get_magic_quotes_gpc() || $force_addslashes )

		return addslashes($text);

	else

		return $text;

}



/*

 * function for processing pass data

 *

 * This function cleans the GET/POST/COOKIE data if magic_quotes_gpc() is on

 * for data which should be outputed immediately after submit

 */

function process_pass_data( $text, $strip_tags = 0 )

{

	if ( $strip_tags )

		$text = strip_tags($text);



	if ( !get_magic_quotes_gpc() )

		return $text;

	else

		return stripslashes($text);

}



/*

 * function for output data from database into html

 */

function htmlspecialchars_adv( $string )

{

	$patterns = array( "/(?!&#\d{2,};)&/m", "/>/m", "/</m", "/\"/m", "/'/m" );

	$replaces = array( "&amp;", "&gt;", "&lt;", "&quot;", "&#039;" );

	return preg_replace( $patterns, $replaces, $string );

}



function process_text_output( $text, $maxwordlen = 100 )

{

	return nl2br( htmlspecialchars_adv( strmaxwordlen( $text, $maxwordlen ) ) );

}



function process_textarea_output( $text, $maxwordlen = 100 )

{

	return htmlspecialchars_adv( strmaxwordlen( $text, $maxwordlen ) );

}



function process_text_withlinks_output( $text, $maxwordlen = 100 )

{

	return nl2br( html_encode( htmlspecialchars_adv( strmaxwordlen( $text, $maxwordlen ) ) ) );

}



function process_line_output( $text, $maxwordlen = 100 )

{

	return htmlspecialchars_adv( strmaxwordlen( $text, $maxwordlen ) );

}



function process_html_output( $text, $maxwordlen = 100 )
{
	$text = str_replace("\n", "<br/>", $text);
	return strmaxwordlen( $text, $maxwordlen );
}


/*

 * functions for work with arrays

 */

function array_stick($base_arr, $add_arr)

{

	if(is_array($base_arr) && is_array($add_arr))

	{

		foreach($add_arr as $key => $value)

		{

			$base_arr[$key] = array_merge($base_arr[$key], $value);

		}

	}

	else

	{

		print_err("Input arguments are not arrays");

	}

	return $base_arr;

}



/**

*	Used to construct sturctured arrays in GET or POST data. Supports multidimensional arrays.

*

*	@param array	$Values	Specifies values and values names, that should be submitted. Can be multidimensional.

*

*	@return string	HTML code, which contains <input type="hidden"...> tags with names and values, specified in $Values array.

*/

function ConstructHiddenValues($Values)

{

	/**

	*	Recursive function, processes multidimensional arrays

	*

	*	@param string $Name	Full name of array, including all subarrays' names

	*

	*	@param array $Value	Array of values, can be multidimensional

	*

	*	@return string	Properly consctructed <input type="hidden"...> tags

	*/

	function ConstructHiddenSubValues($Name, $Value)

	{

		if (is_array($Value))

		{

			$Result = "";

			foreach ($Value as $KeyName => $SubValue)

			{

				$Result .= ConstructHiddenSubValues("{$Name}[{$KeyName}]", $SubValue);

			}

		}

		else

			// Exit recurse

			$Result = "<input type=\"hidden\" name=\"".htmlspecialchars($Name)."\" value=\"".htmlspecialchars($Value)."\" />\n";



		return $Result;

	}

	/* End of ConstructHiddenSubValues function */





	$Result = '';

	if (is_array($Values))

	{

		foreach ($Values as $KeyName => $Value)

		{

			$Result .= ConstructHiddenSubValues($KeyName, $Value);

		}

	}



	return $Result;

}



/**

*	Returns HTML/javascript code, which redirects to another URL with passing specified data (through specified method)

*

*	@param string	$ActionURL	destination URL

*

*	@param array	$Params	Parameters to be passed (through GET or POST)

*

*	@param string	$Method	Submit mode. Only two values are valid: 'get' and 'post'

*

*	@return mixed	Correspondent HTML/javascript code or false, if input data is wrong

*/

function RedirectCode($ActionURL, $Params = NULL, $Method = "get", $Title = 'Redirect')

{

	if ((strcasecmp(trim($Method), "get") && strcasecmp(trim($Method), "post")) || (trim($ActionURL) == ""))

		return false;



	ob_start();



?>

<html>

	<head>

		<title><?= $Title ?></title>

	</head>

	<body>

		<form name="RedirectForm" action="<?= htmlspecialchars($ActionURL) ?>" method="<?= $Method ?>">



<?= ConstructHiddenValues($Params) ?>



		</form>

		<script type="text/javascript">

			<!--

			document.forms['RedirectForm'].submit();

			-->

		</script>

	</body>

</html>

<?



	$Result = ob_get_contents();

	ob_end_clean();



	return $Result;

}



/**

*	Redirects browser to another URL, passing parameters through POST or GET

*	Actually just prints code, returned by RedirectCode (see RedirectCode)

*/

function Redirect($ActionURL, $Params = NULL, $Method = "get", $Title = 'Redirect')

{

	$RedirectCodeValue = RedirectCode($ActionURL, $Params, $Method, $Title);

	if ($RedirectCodeValue !== false)

		echo $RedirectCodeValue;

}



function ErrorHandler($errno, $errstr, $errfile, $errline)

{

    switch ($errno)

    {

    case FATAL:

            echo "<b>FATAL</b> [$errno] $errstr<br>\n";

        echo "  Fatal error in line ".$errline." of file ".$errfile;

        echo ", PHP ".PHP_VERSION." (".PHP_OS.")<br>\n";

        echo "Aborting...<br>\n";

        exit(1);

    break;

    case ERROR:

         echo "<b>ERROR</b> [$errno] $errstr<br>\n";

    break;

    case WARNING:

    //    echo "<b></b> [$errno] $errstr<br>\n";

    break;

    default:

    break;

    }



}



function isRWAccessible($filename)

{



    clearstatcache();

    $perms = fileperms($filename);

    return ( $perms & 0x0004 && $perms & 0x0002 ) ? true : false;



}



/**

 * Send email function

 *

 * @param string $sRecipientEmail		- Email where email should be send

 * @param string $sMailSubject			- subject of the message

 * @param string $sMailBody				- Body of the message

 * @param integer $iRecipientID			- ID of recipient profile

 * @param array $aPlus					- Array of additional information

 *

 *

 * @return boolean 						- trie if message was send

 * 										- false if not

 */

function sendMail( $sRecipientEmail, $sMailSubject, $sMailBody, $iRecipientID = '', $aPlus = '', $sNNBuyer = '', $sEMLBuyer = '' )
{
	global $site;
	global $isActivation;

	if( $iRecipientID )
		$aRecipientInfo = getProfileInfo( $iRecipientID );

	if ($aPlus['isComposeMessage'] == 1)
	{
		$emailNotify = 'NoReply@YogaHub.com';
		$emailNotifyTitle = 'MyYogaHub.com';
	}
	else 
	{
		$emailNotify = getParam('notifications_email');
		$emailNotifyTitle = getParam('notifications_email_title');
	}
	
	if (defined('replace_from_email'))
		$emailNotify = replace_from_email;

	$sMailHeader		= "From: $emailNotifyTitle <$emailNotify>";
	$sMailParameters	= "-f$emailNotifyTitle";
	$sMailSubject	= str_replace( "<SiteName>", $site['title'], $sMailSubject );

	//$sMailBody		= str_replace( "<SiteName>", $site['title'], $sMailBody );
	$sMailBody		= str_replace( "<SiteName>", $emailNotifyTitle, $sMailBody );
	$sMailBody		= str_replace( "<Domain>", $site['url'], $sMailBody );
	$sMailBody		= str_replace( "<recipientID>", $aRecipientInfo['ID'], $sMailBody );
	$sMailBody		= str_replace( "<RealName>", ( ( 1 < strlen($aRecipientInfo['RealName'] ) ? $aRecipientInfo['RealName'] : $aRecipientInfo['NickName'] )), $sMailBody );
	$sMailBody		= str_replace( "<NickName>", $aRecipientInfo['NickName'], $sMailBody );
	$sMailBody		= str_replace( "<Email>", $aRecipientInfo['Email'], $sMailBody );

	if ($sNNBuyer!='')
		$sMailBody		= str_replace( "<NickNameB>", $sNNBuyer, $sMailBody );

	if ($sEMLBuyer!='')
		$sMailBody		= str_replace( "<EmailB>", $sEMLBuyer, $sMailBody );

	$sMailBody = str_replace( "<Password>", $aRecipientInfo['Password'], $sMailBody );

	if( is_array($aPlus) )
		foreach ( $aPlus as $key => $value )
			$sMailBody = str_replace( '<' . $key . '>', $value, $sMailBody );

	if( 'HTML' == $aRecipientInfo['EmailFlag'] || $isActivation || !$iRecipientID)
	{
		$sMailHeader = "MIME-Version: 1.0\r\n" . "Content-type: text/html; charset=UTF-8\r\n" . $sMailHeader;
		$iSendingResult = mail( $sRecipientEmail, $sMailSubject, $sMailBody, $sMailHeader, $sMailParameters );
	}
	else
		$iSendingResult = mail( $sRecipientEmail, $sMailSubject, html2txt($sMailBody), $sMailHeader, $sMailParameters );

	return $iSendingResult;

}



/*

 * Getting Array with Templates Names

*/



function get_templates_array()

{

	global $dir;



	$path = $dir['root'].'templates/';

	$templ_choices = array();

	$handle = opendir( $path );



	while ( false !== ($filename = readdir($handle)) )

	{

		if ( is_dir($path.$filename) && substr($filename, 0, 5) == 'tmpl_' )

		{

			$sTemplName = '';

			@include( $path.$filename.'/scripts/BxTemplName.php' );

			if( $sTemplName )

				$templ_choices[substr($filename, 5)] = $sTemplName;

		}

	}

	closedir( $handle );

    return $templ_choices;

}



/*

 * The Function Show a Line with Templates Names

*/



function templates_select_txt()

{

	global $dir;



	$templ_choices = get_templates_array();

	$current_template = ( strlen( $_GET['skin'] ) ) ? $_GET['skin'] : $_COOKIE['skin'];





	foreach ($templ_choices as $tmpl_key => $tmpl_value)

	{

		if ($current_template == $tmpl_key)

		{

			$ReturnResult .= $tmpl_value;

			$ReturnResult .= ' | ';

		}

		else

		{

			foreach ($_GET as $param_key => $param_value)

			{



				if ( 'skin' != $param_key ) $sGetTransfer .= "&{$param_key}={$param_value}";



			}



			$ReturnResult .= '<a href="' . $_SERVER['PHP_SELF'] . '?skin='. $tmpl_key . $sGetTransfer . '">' . $tmpl_value . '</a>';

			$ReturnResult .= ' | ';



		}

	}



	return $ReturnResult;





}



/**

 * callback function for including template files

 */

function getTemplateIncludedFile( $aFile )

{

	global $tmpl;



	// read include file



	$sFile = BX_DIRECTORY_PATH_ROOT . 'templates/tmpl_' . $tmpl . '/'. $aFile['1'];



	if( file_exists ($sFile) && is_file( $sFile ) )

	{

		$fp = fopen ($sFile, "r");

		if ($fp)

		{

			$s = fread ($fp, filesize ($sFile));

			fclose ($fp);

			return $s;

		}

	}



	return "<b>error reading {$aFile[1]}</b>";

}



function getTemplateBaseFile( $aFile )

{

	global $dir;

	global $tmpl;



	// read include file

	$sFile = BX_DIRECTORY_PATH_ROOT . 'templates/base/' . $aFile['1'];



	if (file_exists ($sFile) && is_file( $sFile ))

	{

		$fp = fopen ($sFile, "r");

		if ($fp)

		{

			$s = fread ($fp, filesize ($sFile));

			fclose ($fp);

			return $s;

		}

	}



	return "<b>error reading base {$aFile[1]}</b>";

}



function extFileExists( $sFileSrc )

{



	if( file_exists( $sFileSrc ) && is_file( $sFileSrc ) )

	{

		$ret = true;

	}

	else

	{

		$ret = false;

	}



	return $ret;

}



function extDirExists( $sDirSrc )

{



	if( file_exists( $sDirSrc ) && is_dir( $sDirSrc ) )

	{

		$ret = true;

	}

	else

	{

		$ret = false;

	}



	return $ret;

}



function getVisitorIP()

{

	$ip = '';

	if( ( isset( $_SERVER['HTTP_X_FORWARDED_FOR'] ) ) && ( !empty( $_SERVER['HTTP_X_FORWARDED_FOR'] ) ) )

	{

		$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];

	}

	elseif( ( isset( $_SERVER['HTTP_CLIENT_IP'])) && (!empty($_SERVER['HTTP_CLIENT_IP'] ) ) )

	{

		$ip = explode(".",$_SERVER['HTTP_CLIENT_IP']);

		$ip = $ip[3].".".$ip[2].".".$ip[1].".".$ip[0];

	}

	elseif((!isset( $_SERVER['HTTP_X_FORWARDED_FOR'])) || (empty($_SERVER['HTTP_X_FORWARDED_FOR'])))

	{

		if ((!isset( $_SERVER['HTTP_CLIENT_IP'])) && (empty($_SERVER['HTTP_CLIENT_IP'])))

		{

			$ip = $_SERVER['REMOTE_ADDR'];

		}

	}

	else

	{

		$ip = "0.0.0.0";

	}



	return $ip;

}



function genFlag( $country )

{

	global $site;

	$country = strtolower( $country );

	

	return "<img src=\"{$site['flags']}{$country}.gif\" />";

}



// print debug information ( e.g. arrays )

function echoDbg( $what, $desc = '' )

{

	if ( $desc )

		echo "<b>$desc:</b> ";

	echo "<pre>";

		print_r( $what );

	echo "</pre>\n";

}



function clear_xss($val)

{

	global $dir;

	require_once( "{$dir['plugins']}safehtml/safehtml.php" );

	$safehtml =& new safehtml();

	$res = $safehtml->parse($val);

	return $res;

}



function _format_when ($iSec) {

	$s = '';



	if ($iSec>0) {

		if ($iSec < 3600) {

			$i = round($iSec/60);

			if (0 == $i || 1 == $i) $s .= '1 Minute Ago';

			else $s .= $i . ' Minutes Ago';

		}

		else if ($iSec < 86400) {

			$i = round($iSec/60/60);

			if (0 == $i || 1 == $i) $s .= '1 Hour Ago';

			else $s .= $i . ' Hours Ago';

		}

		else {

			$i = round($iSec/60/60/24);

			if (0 == $i || 1 == $i) $s .= '1 Day Ago';

			else $s .= $i . ' Days Ago';

		}

	}else {

		if ($iSec > -3600) {

			$iSec = -$iSec;

			$i = round($iSec/60);

			if (0 == $i || 1 == $i) $s .= 'In 1 Minute';

			else $s .= 'In ' . $i . ' Minutes';

		}

		else if ($iSec > -86400) {

			$iSec = -$iSec;

			$i = round($iSec/60/60);

			if (0 == $i || 1 == $i) $s .= 'In 1 Hour';

			else $s .= 'In ' . $i . ' Hours';

		}

		elseif ($iSec < -86400) {

			$iSec = -$iSec;

			$i = round($iSec/60/60/24);

			if (0 == $i || 1 == $i) $s .= 'In 1 Day';

			else $s .= 'In ' . $i . ' Days';

		}

	}

	return $s;

}



function execSqlFile( $filename )

{

    if ( !$f = fopen ( $filename, "r" ) )

    	return false;



	$s_sql = "";

    while ( $s = fgets ( $f, 10240) )

    {

		$s = trim( $s );

		if( !strlen( $s ) ) continue;

        if ( substr( $s, 0, 1 ) == '#'  ) continue; //pass comments

        if ( substr( $s, 0, 2 ) == '--' ) continue;



		$s_sql .= $s;

		

        if ( substr( $s, -1 ) != ';' ) continue;



        db_res( $s_sql );

		$s_sql = "";

    }



    fclose($f);

	return true;

}


function jsAddSlashes($str) {
    $pattern = array(
        "/\\\\/"  , "/\n/"    , "/\r/"    , "/\"/"    ,
        "/\'/"    , "/&/"     , "/</"     , "/>/"
    );
    $replace = array(
        "\\\\\\\\", "\\n"     , "\\r"     , "\\\""    ,
        "\\'"     , "\\x26"   , "\\x3C"   , "\\x3E"
    );
    return preg_replace($pattern, $replace, $str);
}

function jsNewLinesClear($str)
{
    $pattern = array("/\n/", "/\r/");
    return preg_replace($pattern, '', $str);	
}

// Replace hex characters to readable presentation in $hexString
function ReplaceHexChars($hexString)
{
	$normString = $hexString;
	preg_match_all('/\\\x([0-9a-f])([0-9a-f])/is', $hexString, $hexmatches, PREG_SET_ORDER);

	for ($i = 0; $i < count($hexmatches); $i++)
    {
		$normString = str_replace("\x{$hexmatches[$i][1]}{$hexmatches[$i][2]}", 
			chr(hexdec("{$hexmatches[$i][1]}{$hexmatches[$i][2]}")), $normString);
    }
    return $normString;
}

// Replace all HTML entities from html_translation_table in $str to space char
function RemoveHTMLEntities($str)
{
	$str = str_replace("&amp;", "&", $str);
	$str = str_replace("&#039;", "'", $str);
	$charsArr = array_flip(get_html_translation_table(HTML_ENTITIES));
	
	foreach ($charsArr as &$char)
		$char = ' ';

	return strtr($str, $charsArr);
}

// Replace breaklines to spaces
function BreakLinesToSpaces($str)
{
	return str_replace(array("\r\n", "\n", "\r", "<br/>", "<br />"), " ", $str);
}

function SeparateKeywords($keywordsStr, $sep = ', ')
{
	$keywordsStr = str_replace(',', ' ', $keywordsStr);
	return preg_replace("/\s+/s", $sep, trim($keywordsStr));
}

/**
 * Gets user's friend's IDs
 * @param $sId - user ID
 * @return $aUsers - friends array
 */
function getFriends($sId)
{
	$friendsArr = array();
	$friendsArr = fill_assoc_array(db_res("SELECT `Profile` FROM `FriendList` 
		WHERE `ID` = '" . $sId . "' 
		UNION SELECT `ID` FROM `FriendList` 
		WHERE `Profile` = '" . $sId . "'"));
	return $friendsArr;
}

function getFriendList($id, $limit = 0)
{
	global $site;
	global $max_thumb_width;
	global $max_thumb_height;

	$id = (int)$id;
	$limitFilter = ($limit > 0) ? " LIMIT $limit" : '';
	$friend_list_query = "SELECT `Profiles`.* FROM `FriendList`
		LEFT JOIN `Profiles` ON (`Profiles`.`ID` = `FriendList`.`Profile` AND `FriendList`.`ID` = '$id' OR `Profiles`.`ID` = `FriendList`.`ID` AND `FriendList`.`Profile` = '$id')
		WHERE (`FriendList`.`Profile` = '$id' OR `FriendList`.`ID` = '$id') AND `FriendList`.`Check` = '1' 
		ORDER BY `Profiles`.`Picture` DESC
		$limitFilter";

	$friend_list_res = db_res("$friend_list_query");
	
	$iCounter = 0;
	while ( $friend_list_arr = mysql_fetch_assoc( $friend_list_res ) )
	{
		$iCounter ++;
		$sKey = '1';
		if( $iCounter == 3 )
		{
			$sKey = '1';
		}

		$ret .= '<div class="friends_thumb_'.$sKey.'">' . get_member_thumbnail($friend_list_arr['ID'], left) . '<div class="browse_nick"><a href="' . getProfileLink($friend_list_arr['ID']) . '">' . $friend_list_arr['NickName'] . '</a></div><div class="clear_both"></div></div>';
		
		if( $iCounter == 3)
			$iCounter = 0;
	}

	return $ret;
}

/**
 * Return SEF url from title with maximum $numWords words separated by $separator.
 * @param $title string
 * @param $numWords int
 * @param $separator string
 * @return string
 */
function getSEFUrl($title, $numWords = 2, $separator = 'dash')
{
	$title = preg_replace('#-#i', ' ', strip_tags($title));
	$words = array_slice(str_word_count($title, 1), 0, $numWords);
	$wordsStr = implode(' ', $words);

	if ($separator == 'dash')
	{
		$search		= '_';
		$replace	= '-';
	}
	else
	{
		$search		= '-';
		$replace	= '_';
	}

	$trans = array(
		'&\#\d+?;'			=> '',
		'&\S+?;'			=> '',
		'\s+'				=> $replace,
		'[^a-z0-9\-\._]'	=> '',
		$replace.'+'		=> $replace,
		$replace.'$'		=> $replace,
		'^'.$replace		=> $replace
	);

	foreach ($trans as $key => $val)
		$wordsStr = preg_replace("#".$key."#i", $val, $wordsStr);
			
	return trim(stripslashes($wordsStr));
}

/**
 * Generate unique SEF url for $ownerID in the $table.
 * If $sefUrl exists already for $ownerID in $table then '_2' suffix will be added
 * @param $sefUrl string
 * @param $ownerID int
 * @param $ownerDBFieldName string
 * @param $table string
 * @return string
 */
function uniqueSEFUrl($sefUrl, $ownerID, $ownerDBFieldName, $table, $urlTitleDBFieldName='urltitle')
{
	$urlTitlesRows = fill_assoc_array(db_res("SELECT $urlTitleDBFieldName FROM $table 
		WHERE $ownerDBFieldName = '$ownerID' AND $urlTitleDBFieldName LIKE '$sefUrl%'"));
	$urlTitles = array();
	$maxUrlTitleVer = 1;
	foreach ($urlTitlesRows as $urlTitleRow)
	{
		$urlTitles[] = $urlTitleRow[$urlTitleDBFieldName];
		$underscorePos = strpos($urlTitleRow[$urlTitleDBFieldName], '_');
		if ($underscorePos !== false && $underscorePos < strlen($urlTitleRow[$urlTitleDBFieldName]) - 1)
		{
			// Extract url title version
			$urlTitleVer = intval(substr($urlTitleRow[$urlTitleDBFieldName], $underscorePos + 1));
			if ($urlTitleVer > $maxUrlTitleVer)
				$maxUrlTitleVer = $urlTitleVer;
		}
	}
	if (in_array($sefUrl, $urlTitles))
		$sefUrl .= '_' . ($maxUrlTitleVer + 1);
	
	return $sefUrl;
}

// Performs post request to $url with specified $data array.
function do_post_request($url, $data, $optional_headers = null)
{
	 $params = array('http' => array(
	              'method' => 'POST',
	              'content' => http_build_query($data)
	           ));
	 if ($optional_headers !== null) {
	    $params['http']['header'] = $optional_headers;
	 }
	 $ctx = stream_context_create($params);
	 $fp = @fopen($url, 'rb', false, $ctx);
	 if (!$fp) {
	 	$error = error_get_last();
	    throw new Exception("Error {$error['type']}: {$error['message']} in {$error['file']} at {$error['line']}");
	 }
	 $response = @stream_get_contents($fp);
	 if ($response === false) {
	 	$error = error_get_last();
	    throw new Exception("Error {$error['type']}: {$error['message']} in {$error['file']} at {$error['line']}");
	 }
	 return $response;
}

// Redirect to Account page description is empty
function checkCommunityPermissions($iMemberID)
{
	global $site;

	$profileData = getProfileInfo($iMemberID);
	if ($profileData['DescriptionMe'] == '')
	{
		$refer = urlencode($_SERVER['REQUEST_URI']);
		header("Location: {$site['url']}member-update.php?referer=$refer");
		exit;
	}
}

// Track login/logout in global db
function logLoginActivity($globalid, $action, $box = 'none', $success = 1)
{
	global $su_config;

	$yh1Link = new mysqli($su_config['yh1db_host'], $su_config['yh1db_user'], $su_config['yh1db_pass'], $su_config['yh1db_db']);
	if (!mysqli_connect_errno())
	{
		$date = time();
		$yh1Link->query("CALL YH_TRACK_LOGIN_ACTIVITY($globalid, $date, '$action', 'MyYogaHub', '$box', $success)");
	}
	$yh1Link->close();
}

function ImgIndexByFileName($fileName)
{
	if (preg_match('/(?P<ind>\d+)\./', $fileName, $matches))
		return $matches['ind'];
	else 
		return '';
}

function getLinksMacro($linksArr, $maxRows, $linksImageName = 'small_folder', $linksMinImage = 1, $linksMaxImage = 11)
{
	global $tmpl;
	
	try {
		// Create a new template engine
		$tal = new PHPTAL( BX_DIRECTORY_PATH_ROOT . "templates/tmpl_{$tmpl}/mylinks.html");
	}
	catch (Exception $e) {
		echo $e->getMessage();
	}
	
	$linksCount = count($linksArr);
	if ($linksCount < 5)
		$visibleLinksCount = 4;
	else 
		$visibleLinksCount = ($linksCount % 2 != 0) ? $linksCount + 1 : $linksCount;
	
	if ($visibleLinksCount / 2 < $maxRows)
		$linkAddMore = 
		'<div id="linkAddMore" style="float:left; clear:both; width:100%; 
			margin-bottom:20px; text-align:center">
			<a href="javascript:void(0);" onclick="AddMoreLinks();">Add More Links</a>
		</div>';
	else 
		$linkAddMore = '';
			
	$links = array();
	$selectedImages = array();
	for ($lr = 0; $lr <= 1; $lr++)
	{
		$strlr = ($lr == 0) ? 'left' : 'right';
		
		$startj = 0;
		for ($i = 0; $i < $maxRows; $i++)
		{
			$innewwin = true;
			$savedLinkFound = false;
			for ($j = $startj; $j < $linksCount; $j++)
			{
				if ((($lr == 0) xor !$linksArr[$j]['isleft']))
				{
					$pos = $linksArr[$j]['position'];
					$title = $linksArr[$j]['title'];
					$url = $linksArr[$j]['url'];
	
					if (!$linksArr[$j]['innewwindow']) $innewwin = false;
					
					$selectedImageIndex = ImgIndexByFileName($linksArr[$j]['imagename']);
					$selectedImages["{$strlr}_{$i}_{$selectedImageIndex}"] = 'selected';
					
					$startj = $j + 1;
					$savedLinkFound = true;
					break;
				}
			}
			if (!$savedLinkFound)
				list($pos, $title, $url) = array('', '', '');
			$ind = ($lr == 0) ? $i * 2 : $i * 2 + 1;
			$display = ($ind < $visibleLinksCount) ? 'block' : 'none';
			$links[$strlr][] = array('ind' => $ind, 'pos' => $pos, 'title' => $title, 
				'url' => $url, 'side' => $strlr, 'innewwin' => $innewwin, 'display' => $display);
		}
	}
	
	
	$images = array();
	for ($i = 1; $i <= $linksMaxImage; $i++)
		$images[] = $i;
		
	$tal->myleftlinks = $links['left'];
	$tal->myrightlinks = $links['right'];
	$tal->imagespath = "templates/tmpl_$tmpl/images/icons/";
	$tal->imagename = $linksImageName;
	$tal->images = $images;
	$tal->selectedImages = $selectedImages;
	$tal->linkAddMore = $linkAddMore;
	$tal->linksCount = $visibleLinksCount;
	$tal->maxRows = $maxRows;
	$tal->linksMinImage = $linksMinImage;
	$tal->linksMaxImage = $linksMaxImage;
	$tal->tmpl = $tmpl;
	
	try {
		return $tal->execute();
	}
	catch (Exception $ex) {
		echo $ex->getMessage();
	}	
}

// Returns all links from POST
function getPostedLinks($maxRows, $imageName)
{
	$myLinks = array();
	for ($rl = 0; $rl <= 1; $rl++)
	{
		$strrl = ($rl == 0) ? 'right' : 'left';
		for ($i = 0; $i < $maxRows; $i++)
		{
			$mylinkUrl = $_POST["mylink_{$strrl}_url_$i"];
			if ((isset($_POST["mylink_{$strrl}_title_$i"]) && trim($_POST["mylink_{$strrl}_title_$i"]) != '') ||
				($mylinkUrl != '' && $mylinkUrl != 'http://' && $mylinkUrl != 'https://'))
			{
				$innewwin = (isset($_POST["mylink_{$strrl}_innewwin_$i"])) ? 1 : 0;
				$myLinks[] = array(
					'pos' => $_POST["mylink_{$strrl}_pos_$i"], 
					'isleft' => $rl,
					'title' => $_POST["mylink_{$strrl}_title_$i"], 
					'url' => $mylinkUrl,
					'image' => $imageName . $_POST["mylink_{$strrl}_image_$i"] . '.png',
					'innewwin' => $innewwin
				);
			}
		}
	}
	
	return $myLinks;
}

// Save $links to specified db table.
// $targetID - entity's ID to which these links corresponds (e.g. memberID, eventID)
function saveLinks($links, $targetID, $tableName, $targetFieldName)
{
	if (!count($links)) return false;
	
	$values = '';
	foreach ($links as $link)
	{
		if ($link['title'] == '' && $link['url'] == 'http://') continue;
		
		$pos = ($link['pos'] == '') ? 0 : $link['pos'];
		if (strpos($link['url'], 'http://') === 0 || strpos($link['url'], 'https://') === 0)
			$url = $link['url'];
		else 
			$url = 'http://' . $link['url'];
		if (strlen($values) > 0) $values .= ', ';
		$values .= "($targetID, {$link['isleft']}, $pos, '".mysql_real_escape_string($link['title'])."', 
			'".mysql_real_escape_string($url)."', '{$link['image']}', {$link['innewwin']})";
	}
	
	$query = "INSERT INTO $tableName ($targetFieldName, isleft, position, title, url, imagename, innewwindow) VALUES $values";
	
	return db_res($query);
}

function superlogin($logingid, $logintime, $loginkey)
{
	if (!$logingid || !$logintime || !$loginkey) return false;
	
	if (time() - 10 * 60 > $logintime) return false;
	
	return ($loginkey == md5($logingid . $logintime . $logingid . substr($logintime, 2))) ? $logingid : false;
}

function saveGuiMessage($msg)
{
	if (!$msg) return false;
	
	if (!session_id()) session_start();		
	$_SESSION['guiMessage'] = $msg;
}

function guiMessage()
{
	if (isset($_SESSION['guiMessage']))
	{
		$ret = $_SESSION['guiMessage'];
		unset($_SESSION['guiMessage']);
	}
	else
		$ret = '';
		
	return $ret;
}

// Return corresponding state name for specified stateCode
// or $stateCode if not found
function getStateName($stateCode)
{
	global $statesUS, $statesCA;
	
	foreach ($statesUS as $code => $name)
		if ($code == $stateCode) return $name;
		
	foreach ($statesCA as $code => $name)
		if ($code == $stateCode) return $name;

	return $stateCode;
}

function updateRequestsInMemcache($memberID)
{
	if (!$memberID) return false;
	
	require_once(BX_DIRECTORY_PATH_CLASSES . 'class.yhInteractor.php');
	$yhInteractor = new yhInteractor();	
	$globalid = db_value("SELECT globalid FROM Profiles WHERE ID = $memberID");
	$newRequestsNumber = $yhInteractor->getRequests($globalid, false);
	$yhInteractor->setRequestsNumber($globalid, $newRequestsNumber);	
}

function updateAvatarsInMemcache($memberID)
{
	if (!$memberID) return false;
	
	require_once(BX_DIRECTORY_PATH_CLASSES . 'class.yhInteractor.php');
	$yhInteractor = new yhInteractor();	
	$globalid = db_value("SELECT globalid FROM Profiles WHERE ID = $memberID");
	
	$newSmallAvatar = $yhInteractor->getSmallAvatar($globalid, false);
	$newMiddleAvatar = $yhInteractor->getMiddleAvatar($globalid, false);
}

function updateFeaturedEventsInMemcache()
{
	require_once(BX_DIRECTORY_PATH_CLASSES . 'class.yhInteractor.php');
	$yhInteractor = new yhInteractor();	
	$yhInteractor->getFeaturedEvents(false);
}

function clearYHCache()
{
	global $su_config;

	do_post_request("{$su_config['main_host']}yogahub.com/admin/home_page.php", array('clear_cache' => 1));
}

function getMemberIDByGlobalID($globalid)
{
	if (!$globalid) return 0;
	return db_value("SELECT ID FROM Profiles WHERE globalid = $globalid");
}

?>