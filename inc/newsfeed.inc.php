<?php

define('MAX_COMMENT_LENGTH_TRACK', 50);

function TrimNewsFeedContent($sText)
{
	//$sText = strip_tags(htmlspecialchars_decode(RemoveHTMLEntities($sText), ENT_QUOTES));
	$sText = strip_tags($sText);
	return (strlen($sText) > MAX_COMMENT_LENGTH_TRACK) ? substr($sText, 0, 47) . '...' : $sText;
}

function TrackNewAction($newsFeedType, $fileId, $actorId, $sText='', $sText2='', 
	$path='', $targetId=0, $targetNick='', $needSlashes = false, $commentId=0)
{
	// Get actor nickname
	$actorNickQuery = "SELECT NickName
		FROM `Profiles` 
		WHERE ID = $actorId LIMIT 1";
	$actorNick = db_value($actorNickQuery);
	if (!$actorNick) return false;

	// Get target nickname
	if ($newsFeedType == 1 || $newsFeedType == 10 || $newsFeedType == 11)
	{
		$targetNickQuery = "SELECT NickName
		FROM `Profiles` 
		WHERE ID = $targetId LIMIT 1";
		$targetNick = db_value($targetNickQuery);
		if (!$targetNick) return false;
	}
	
	$time = time();	
	
	$sText = strip_tags($sText);
	$sText2 = strip_tags($sText2);
	
	if ($sText != '') $sText = TrimNewsFeedContent($sText);
	if ($sText2 != '') $sText2 = TrimNewsFeedContent($sText2);
	$magic_quotes = get_magic_quotes_gpc();

	if (!$magic_quotes && $needSlashes)
	{
		$sText = addslashes($sText);
		$sText2 = addslashes($sText2);
	}
	
	// Update news-feed track table
	$sQuery = "INSERT INTO `NewsFeedTrack` 
		(`type`, `date`, `actorid`, `actornick`, `targetid`, `targetnick`, `mediaid`, `path`, `content`, `content2`, `commentid`) 
		VALUES ($newsFeedType, $time, $actorId, '$actorNick', $targetId, '$targetNick', $fileId, '$path', '$sText', '$sText2', $commentId)";
	return db_res($sQuery);		
}

?>