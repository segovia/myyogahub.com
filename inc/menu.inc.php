<?

/***************************************************************************
*                            Dolphin Smart Community Builder
*                              -----------------
*     begin                : Mon Mar 23 2006
*     copyright            : (C) 2006 BoonEx Group
*     website              : http://www.boonex.com/
* This file is part of Dolphin - Smart Community Builder
*
* Dolphin is free software. This work is licensed under a Creative Commons Attribution 3.0 License. 
* http://creativecommons.org/licenses/by/3.0/
*
* Dolphin is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
* without even the implied warranty of  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the Creative Commons Attribution 3.0 License for more details. 
* You should have received a copy of the Creative Commons Attribution 3.0 License along with Dolphin, 
* see license.txt file; if not, write to marketing@boonex.com
***************************************************************************/

require_once( 'header.inc.php' );
require_once( BX_DIRECTORY_PATH_INC . 'admin.inc.php' );
require_once( BX_DIRECTORY_PATH_INC . 'db.inc.php' );
require_once( BX_DIRECTORY_PATH_INC . 'prof.inc.php' );
require_once( BX_DIRECTORY_PATH_INC . 'banners.inc.php' );
require_once( BX_DIRECTORY_PATH_INC . 'membership_levels.inc.php' );
require_once( BX_DIRECTORY_PATH_INC . 'params.inc.php' );
require_once( BX_DIRECTORY_PATH_INC . 'languages.inc.php');
require_once( BX_DIRECTORY_PATH_INC . 'utils.inc.php');
require_once( BX_DIRECTORY_PATH_ROOT . "templates/tmpl_{$tmpl}/scripts/functions.php" );
require_once( BX_DIRECTORY_PATH_ROOT . "templates/tmpl_{$tmpl}/scripts/BxTemplMenu.php" );

$aMenuInfo = array();

if( !@eval( file_get_contents( BX_DIRECTORY_PATH_INC . 'menu_content.inc.php' ) ) )
{
	if( basename( dirname( $_SERVER['PHP_SELF'] ) ) != $admin_dir )
	{
		///$aTopMenu = array();
		//$aMenu    = array();
		//echo '<b>Warning:</b> Please rebuild the menu from Admin Panel -> Builders -> Navigation Menu Builder';
	}
}

if (isset($_REQUEST['memberID']) && $_REQUEST['memberID'] != "") 
{
	$aTopMenu[] = array(
		'Type'      => "custom",
		'Caption'   => "",
		'Link'      => "upload_media.php?show=photo",
		'Visible'   => "memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => "",
		'Strict'    => "0",
		'Parent'    => "9"
	);	
			
	$aTopMenu[] = array(
		'Type'      => "custom",
		'Caption'   => "",
		'Link'      => "upload_media.php?show=eventphoto",
		'Visible'   => "memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => "",
		'Strict'    => "0",
		'Parent'    => "51"
	);	
}

function MemberMenuDesign ( $iUserId = 0 )
{
	global $site;
	global $aMenu;
	global $tmpl;


	global $oTemplConfig;
	$oTemplMenu = new BxTemplMenu( $oTemplConfig );

	$aMenuIcon = array(
        '1'=>'_member_panel.gif',
        '2'=>'_profile_edit.gif',
        '3'=>'_profile_customize.gif',
        '4'=>'_communicator.gif',
        '5'=>'_chat.gif',
        '6'=>'_forum.gif',
        '7'=>'_membership.jpg',
        '8'=>'_cart.gif',
        '9'=>'_feedback.jpg',
        '10'=>'_gallery.gif',
        '11'=>'_blog.gif',
        '12'=>'_gallery.gif',
        '13'=>'_polls.gif',
        '14'=>'_polls.gif',
        '15'=>'_speed_dating.gif',
        '16'=>'_logout.gif',
        '17'=>'_join.gif',
        '18'=>'_login.gif',
        '19'=>'_profile_photos.gif',
        '20'=>'_forum.gif',
        '21'=>'_chat.gif',
        '22'=>'_groups.gif',
    );

	$sMenuLink = "";
	$sPath = "";
	$sVisible = $iUserId == 0 ? "non": "memb";

	$ret .= '<div class="menu_item_block">';
	foreach ($aMenu as $iVal => $aValue)
	{
		$sIconName = $tmpl == 'dol' ? $aMenuIcon[$iVal] : '';
		if ($aValue['MenuGroup'] != 0) continue;
		$sMenuLink = $iUserId != 0 ? add_id($aValue['Link'], $iUserId) : $aValue['Link'];
		if ( strpos($sMenuLink,'http://') === FALSE )
		{
			$sPath =  "" ;
		}
		else
		{
			$sPath = $sMenuLink;
			$sMenuLink = "";
		}
		$jFunc = strlen($aValue['Onclick']) > 0 ? $aValue['Onclick'] : "";
		
		$memberPass = getPassword( $iUserId );
		$jFunc = str_replace( '{*}',          $iVal,        $jFunc);
		$jFunc = str_replace( '{URL}',        $site['url'], $jFunc);
		$jFunc = str_replace( '{memberID}',   $iUserId,     $jFunc);
		$jFunc = str_replace( '{memberPass}', $memberPass,  $jFunc);
		if (check_condition(str_replace('\$','$',$aValue['Check'])) == TRUE && strrpos($aValue['Visible'],$sVisible) !== FALSE)
		{
			$ret .= $oTemplMenu -> getMenuItem( _t($aValue['Caption']), $sMenuLink, $sPath, $aValue['Target'], $jFunc, $sIconName );
			$ret .= ('group' == $aValue['MenuType']) ? '<div id="submenu_' . $iVal . '" style="display: none;" class="member_submenu"></div>' : '';
		}
	}
	$ret .= "</div>";
	return $ret;
}


function getMenuInfo()
{
	global $logged;
	global $aMenuInfo;
	global $p_arr;
	global $aTopMenu;
	
	if( $logged['member'] )
	{
		$aMenuInfo['memberID']   = (int)$_COOKIE['memberID'];
		$aMenuInfo['memberNick'] = getNickName( $aMenuInfo['memberID'] );
		$aMenuInfo['memberLink'] = getProfileLink( $aMenuInfo['memberID'] );
		$aMenuInfo['visible']    = 'memb';
	}
	elseif ($logged['admin'])
	{
		$aMenuInfo['memberID']   = (int)$_REQUEST['memberID'];
		$aMenuInfo['memberNick'] = getNickName( $aMenuInfo['memberID'] );
		$aMenuInfo['memberLink'] = getProfileLink( $aMenuInfo['memberID'] );
		$aMenuInfo['visible']    = 'memb';
	}
	else
	{
		$aMenuInfo['memberID'] = 0;
		$aMenuInfo['memberNick'] = '';
		$aMenuInfo['memberLink'] = '';
		$aMenuInfo['visible']  = 'non';
	}
	
	/**********************
	 * SEF URL REPLACEMENT
	 *********************/
	
	// Photos page
	if (preg_match('#([^/]+)/photos.?$#i', $_SERVER['REQUEST_URI'], $matches)
		|| preg_match('#browsePhoto\.php\?userID=#i', $_SERVER['REQUEST_URI']))
	{
		if ($matches)
		{
			$profileID = getID($matches[1]);
			$profileNick = $matches[1];
		} 
		else
		{
			$profileID = $_GET['userID'];
			$profileNick = getNickName($profileID);
		}	
		$aMenuInfo['profileID'] = $profileID;
		$aMenuInfo['profileNick'] = $profileNick;
		$aMenuInfo['profileLink'] = getProfileLink($profileID);	
		$aMenuInfo['currentCustom'] = 65;
		$aMenuInfo['currentTop'] = $aTopMenu[65]['Parent'];
		$isMenuSet = true;				
	}
	// Photo page
	elseif (preg_match('#([^/]+)/photo/([^/]+)\.html$#i', $_SERVER['REQUEST_URI']))
	{
		$aMenuInfo['currentTop'] = 41;
		$isMenuSet = true;			
	}
	// Videos page
	elseif (preg_match('#([^/]+)/videos.?$#i', $_SERVER['REQUEST_URI'], $matches)
		|| preg_match('#browseVideo\.php\?userID=#i', $_SERVER['REQUEST_URI']))
	{
		if ($matches)
		{
			$profileID = getID($matches[1]);
			$profileNick = $matches[1];
		} 
		else
		{
			$profileID = $_GET['userID'];
			$profileNick = getNickName($profileID);
		}		
		$aMenuInfo['profileID'] = $profileID;
		$aMenuInfo['profileNick'] = $profileNick;
		$aMenuInfo['profileLink'] = getProfileLink($profileID);	
		$aMenuInfo['currentCustom'] = 61;
		$aMenuInfo['currentTop'] = $aTopMenu[61]['Parent'];
		$isMenuSet = true;	
	}
	// Video page
	elseif (preg_match('#([^/]+)/video/([^/]+)\.html$#i', $_SERVER['REQUEST_URI']))
	{
		$aMenuInfo['currentTop'] = 28;
		$isMenuSet = true;			
	}
	// Events page
	elseif (preg_match('#(([^/]+)/)?events.?(upcoming|past|tagKey=(.*)|keyword=(.*))?$#i', $_SERVER['REQUEST_URI'], $matches)
		|| preg_match('#events\.php\?(.*?)&ID=#i', $_SERVER['REQUEST_URI']))
	{
		if ($matches)
		{
			$profileID = getID($matches[2]);
			$profileNick = $matches[2];
		} 
		else
		{
			$profileID = $_GET['ID'];
			$profileNick = getNickName($profileID);
		}		
		if ($profileID)
		{
			$aMenuInfo['profileID'] = $profileID;
			$aMenuInfo['profileNick'] = $profileNick;
			$aMenuInfo['profileLink'] = getProfileLink($profileID);	
			$aMenuInfo['currentCustom'] = 94;
			$aMenuInfo['currentTop'] = $aTopMenu[94]['Parent'];
		}
		else
			$aMenuInfo['currentTop'] = 51;	
		$isMenuSet = true;	
	}
	// Event page
	elseif (preg_match('#([^/]+)/event/([^/]+)\.html$#i', $_SERVER['REQUEST_URI'])
		|| preg_match('#([^/]+)/events.php\?action=show_info#i', $_SERVER['REQUEST_URI']))
	{
		$aMenuInfo['currentTop'] = 51;
		$isMenuSet = true;			
	}	
	// Blog page or search by blog category
	elseif (preg_match('#([^/]+)/blog.?$#i', $_SERVER['REQUEST_URI'], $matches)
		|| preg_match('#([^/]+)/blog/(.+)$#i', $_SERVER['REQUEST_URI'], $matches))
	{
		if (array_key_exists(1, $matches))
		{
			$blogOwnerID = getID($matches[1]);				
			if ($blogOwnerID > 0)
			{
				$sRequestUriFile = "blogs.php?action=show_member_blog&amp;ownerID=$blogOwnerID";
				$aMenuInfo['profileID'] = $blogOwnerID;
				$aMenuInfo['profileNick'] = $matches[1];
				$aMenuInfo['profileLink'] = getProfileLink($blogOwnerID);
				$aMenuInfo['currentCustom'] = 66; // My Blog page
				$aMenuInfo['currentTop'] = $aTopMenu[66]['Parent'];
				$isMenuSet = true;			
			}
		}
	}
	// Post page
	elseif (preg_match('#([^/]+)/([^/]+)\.html$#i', $_SERVER['REQUEST_URI'], $matches))
	{
		if (array_key_exists(1, $matches) && array_key_exists(2, $matches)) // url contains nickname and post title
		{
			$blogOwnerID = getID($matches[1]);
			if ($blogOwnerID > 0)
			{
				$postID = db_value("SELECT `BlogPosts`.`PostID` FROM `BlogPosts`
					LEFT JOIN `BlogCategories` USING (`CategoryID`)
					WHERE `BlogCategories`.`OwnerID` = $blogOwnerID
						AND `BlogPosts`.`urltitle` LIKE '{$matches[2]}'");
				if ($postID > 0)
					$sRequestUriFile = "blogs.php?action=show_member_post&amp;ownerID=$blogOwnerID&amp;post_id=$postID";			
				$aMenuInfo['profileID'] = $blogOwnerID;
				$aMenuInfo['profileNick'] = $matches[1];
				$aMenuInfo['profileLink'] = getProfileLink($blogOwnerID);
				$aMenuInfo['currentCustom'] = 66; // My Blog page
				$aMenuInfo['currentTop'] = $aTopMenu[66]['Parent'];
				$isMenuSet = true;					
			}
		}
	}
	// Original blog page or post page
	elseif (preg_match('#action=show_member_blog#i', $_SERVER['REQUEST_URI'])
		|| preg_match('#action=show_member_post#i', $_SERVER['REQUEST_URI']))
	{
		$aMenuInfo['profileID'] = $_GET['ownerID'];
		$aMenuInfo['profileNick'] = getNickName($_GET['ownerID']);
		$aMenuInfo['profileLink'] = getProfileLink($_GET['ownerID']);
		$aMenuInfo['currentCustom'] = 66; // My Blog page
		$aMenuInfo['currentTop'] = $aTopMenu[66]['Parent'];
		$isMenuSet = true;	
	}
	// New blog post
	elseif (preg_match('#([^/]+)/new-blog-post.?$#i', $_SERVER['REQUEST_URI']))
		$sRequestUriFile = "blogs.php?action=new_post";
	// Add blog category
	elseif (preg_match('#([^/]+)/add-blog-category.?$#i', $_SERVER['REQUEST_URI']))
		$sRequestUriFile = "blogs.php?action=add_category";		
	// Top blogs
	elseif (preg_match('#top-blogs.?$#i', $_SERVER['REQUEST_URI'])
		|| preg_match('#action=top_blogs$#i', $_SERVER['REQUEST_URI']))
	{
		$aMenuInfo['currentCustom'] = 46;
		$aMenuInfo['currentTop'] = $aTopMenu[46]['Parent'];
		$isMenuSet = true;
	}	
	// Top posts
	elseif (preg_match('#top-posts.?$#i', $_SERVER['REQUEST_URI'])
		|| preg_match('#action=top_posts$#i', $_SERVER['REQUEST_URI']))
	{
		$aMenuInfo['currentCustom'] = 79;
		$aMenuInfo['currentTop'] = $aTopMenu[79]['Parent'];
		$isMenuSet = true;
	}				
	// Search blog post by tag
	elseif (preg_match('#([^/]+)/blog/tagKey=(.+)$#i', $_SERVER['REQUEST_URI'], $matches))
	{
		if (array_key_exists(1, $matches) && array_key_exists(2, $matches)) // url contains nickname and tagKey
		{
			$blogOwnerID = getID($matches[1]);
			$sefTagKey = $matches[2];
			$sRequestUriFile = "blogs.php?action=search_by_tag&amp;tagKey=$sefTagKey&amp;ownerID=$blogOwnerID";
			$aMenuInfo['profileID'] = $blogOwnerID;
			$aMenuInfo['profileNick'] = $matches[1];
			$aMenuInfo['profileLink'] = getProfileLink($blogOwnerID);
		}
	}
	// Without SEF
	else
	{
		$selfFile = basename( $_SERVER['PHP_SELF'] );

		//get viewing profile ID
		if( $p_arr and $p_arr['ID'])
		{
			$aMenuInfo['profileID']   = (int)$p_arr['ID'];
			$aMenuInfo['profileNick'] = $p_arr['NickName'];
			$aMenuInfo['profileLink'] = getProfileLink( $aMenuInfo['profileID'] );
		}
		elseif( $selfFile == 'browseVideo.php' or $selfFile == 'browsePhoto.php' or $selfFile == 'browseMusic.php' )
		{
			$aMenuInfo['profileID']   = (int)$_GET['userID'];
			$aMenuInfo['profileNick'] = getNickName( $aMenuInfo['profileID'] );
			$aMenuInfo['profileLink'] = getProfileLink( $aMenuInfo['profileID'] );
		}
		elseif( $selfFile == 'guestbook.php' )
		{
			$aMenuInfo['profileID']   = $_REQUEST['owner'] ? (int)$_REQUEST['owner'] : $aMenuInfo['profileID'];
			$aMenuInfo['profileNick'] = getNickName( $aMenuInfo['profileID'] );
			$aMenuInfo['profileLink'] = getProfileLink( $aMenuInfo['profileID'] );
		}
		elseif( $selfFile == 'blogs.php' )
		{
			$aMenuInfo['profileID']   = $_GET['ownerID'] ? (int)$_GET['ownerID'] : $aMenuInfo['profileID'];
			$aMenuInfo['profileNick'] = getNickName( $aMenuInfo['profileID'] );
			$aMenuInfo['profileLink'] = getProfileLink( $aMenuInfo['profileID'] );
		}
		elseif( $selfFile == 'viewFriends.php' )
		{
			$aMenuInfo['profileID']   = (int)$_GET['iUser'];
			$aMenuInfo['profileNick'] = getNickName( $aMenuInfo['profileID'] );
			$aMenuInfo['profileLink'] = getProfileLink( $aMenuInfo['profileID'] );
		}
		elseif($selfFile == 'profile_customize.php' || $selfFile == 'upload_media.php')
		{
			$aMenuInfo['profileID']   = $aMenuInfo['memberID'];
			$aMenuInfo['profileNick'] = $aMenuInfo['memberNick'];
			$aMenuInfo['profileLink'] = $aMenuInfo['memberLink'];
		}
		elseif( $selfFile == 'events.php' )
		{
			$aMenuInfo['profileID']   = (int)$_GET['ID'];
			$aMenuInfo['profileNick'] = getNickName( $aMenuInfo['profileID'] );
			$aMenuInfo['profileLink'] = getProfileLink( $aMenuInfo['profileID'] );
		}
		else
		{
			$aMenuInfo['profileID']   = 0;
			$aMenuInfo['profileNick'] = '';
			$aMenuInfo['profileLink'] = '';
		}
	}

	// Replace menu link for SEF mode
	if (useSEF)
	{
		if (!$aMenuInfo['profileNick'] && 
			preg_match('#([^/]+)/(blog|photo|video|([^/]+\.html))#i', $_SERVER['REQUEST_URI'], $matches))
		{
			$profileID = getID($matches[1]);	
			$aMenuInfo['profileID'] = $profileID;
			$aMenuInfo['profileNick'] = $matches[1];
			$aMenuInfo['profileLink'] = getProfileLink($profileID);
		}

		if ($aMenuInfo['profileNick'])
			$nick = $aMenuInfo['profileNick'];
		elseif ($p_arr && $p_arr['NickName'])
			$nick = $p_arr['NickName'];
		else
			$nick = $aMenuInfo['memberNick'];
						
		$myProfileBlogLinks = explode('|', $aTopMenu[66]['Link']);
		$myProfileBlogLinks[0] = "$nick/blog";
		$aTopMenu[66]['Link'] = implode('|', $myProfileBlogLinks);
		$aTopMenu[47]['Link'] = "$nick/blog";
		$aTopMenu[46]['Link'] = 'top-blogs';
		$aTopMenu[79]['Link'] = 'top-posts';
		$aTopMenu[65]['Link'] = "$nick/photos";
		$aTopMenu[69]['Link'] = "{$aMenuInfo['memberNick']}/photos";
		$aTopMenu[61]['Link'] = "$nick/videos";
		$aTopMenu[68]['Link'] = "{$aMenuInfo['memberNick']}/videos";
		$aTopMenu[94]['Link'] = "$nick/events";
		$aTopMenu[54]['Link'] = "{$aMenuInfo['memberNick']}/events";
		$aTopMenu[51]['Link'] = "events";
		$aTopMenu[52]['Link'] = "events";
	}
	
	if ($isMenuSet) return;
	
	// detect current menu
	$aMenuInfo['currentCustom'] = 0;
	$aMenuInfo['currentTop'] = 0;
		
	if (!$sRequestUriFile)
		$sRequestUriFile = htmlspecialchars_adv( basename( $_SERVER['REQUEST_URI'] ) );
		
	$lastLink = ''; // last link for current custom item
	
	// Replace link of Friends submenu if on own profile
	if ($logged['member'] && $aMenuInfo['memberID'] == $aMenuInfo['profileID'])
		$aTopMenu[82]['Link'] = 'contacts.php';
	
	foreach( $aTopMenu as $iItemID => $aItem )
	{	
		if( $aItem['Type'] == 'top' and $aMenuInfo['currentTop'] and $aMenuInfo['currentTop'] != $iItemID )
			break;

		$aItemUris = explode( '|', $aItem['Link'] );
		foreach( $aItemUris as $sItemUri )
		{
			if( $aMenuInfo['memberID'] )
			{
				$sItemUri = str_replace( "{memberID}",    $aMenuInfo['memberID'],    $sItemUri );
				$sItemUri = str_replace( "{memberNick}",  $aMenuInfo['memberNick'],  $sItemUri );
				$sItemUri = str_replace( "{memberLink}",  $aMenuInfo['memberLink'],  $sItemUri );
			}
			
			if( $aMenuInfo['profileID'] )
			{
				$sItemUri = str_replace( "{profileID}",   $aMenuInfo['profileID'],   $sItemUri );
				$sItemUri = str_replace( "{profileNick}", $aMenuInfo['profileNick'], $sItemUri );
				$sItemUri = str_replace( "{profileLink}", $aMenuInfo['profileLink'], $sItemUri );
			}
			if (strpos($sItemUri, 'blogs.php?action=search_by_tag') !== false)
			{
				$tagKey = (isset($_REQUEST['tagKey'])) ? $_REQUEST['tagKey'] : $sefTagKey;
				$sItemUri = str_replace('{tagKey}', $tagKey, $sItemUri);
			}

			$macroArray = array("/{memberID}/", "/{profileID}/");
			$compareItemUri = preg_replace($macroArray, '', $sItemUri); // compare without {macro}
			if (strcasecmp($sItemUri, $sRequestUriFile) == 0)
			{
				if( $aItem['Type'] == 'custom' )
				{
					$aMenuInfo['currentCustom'] = $iItemID;
					$aMenuInfo['currentTop']    = (int)$aItem['Parent'];
					return;
				}
				else //top or system
				{
					if( $aMenuInfo['currentTop'] and $aMenuInfo['currentTop'] != $iItemID )
						break;
					else
						$aMenuInfo['currentTop'] = $iItemID;
				}
			}
			elseif (substr($sRequestUriFile, 0, strlen($compareItemUri)) == $compareItemUri && !(int)$aItem['Strict'])
			{
				if( $aItem['Type'] == 'custom' )
				{
					// Save current custom item with longer link.
					// e.g. blogs.php?action=top_blogs but no blogs.php
					if (strlen($compareItemUri) > $lastLink)
					{
						$lastLink = $compareItemUri;
						$aMenuInfo['currentCustom'] = $iItemID;
						$aMenuInfo['currentTop']    = (int)$aItem['Parent'];
					}
				}
				else //top or system
				{
					if( $aMenuInfo['currentTop'] and $aMenuInfo['currentTop'] != $iItemID )
						break;
					else
						$aMenuInfo['currentTop'] = $iItemID;
				}
			}			
		}
	}
	
	//echoDbg( $aMenuInfo );
}



function TopMenuDesign()
{
	global $aTopMenu;
	global $aMenuInfo;
	global $oTemplConfig;

	if( !$aMenuInfo )
		getMenuInfo();
	
	$oTemplMenu = new BxTemplMenu( $oTemplConfig );
	$ret = '';
	
	foreach( $aTopMenu as $iItemID => $aItem )
	{
		if( $aItem['Type'] != 'top' )
			continue;
		
		if( strpos( $aItem['Visible'], $aMenuInfo['visible'] ) === false )
			continue;
		
		if( strlen( $aItem['Check'] ) )
		{
			$sCheck = $aItem['Check'];
			$sCheck = str_replace( '\$', '$', $sCheck );
			
			$func = create_function('', $sCheck );
			if( !$func() )
				continue;
		}
		
		//generate
		list( $aItem['Link'] ) = explode( '|', $aItem['Link'] );
		
		$aItem['Link'] = str_replace( "{memberID}",    $aMenuInfo['memberID'],    $aItem['Link'] );
		$aItem['Link'] = str_replace( "{memberNick}",  $aMenuInfo['memberNick'],  $aItem['Link'] );
		
		$aItem['Link'] = str_replace( "{profileID}",   $aMenuInfo['profileID'],   $aItem['Link'] );
		$aItem['Link'] = str_replace( "{profileNick}", $aMenuInfo['profileNick'], $aItem['Link'] );
		
		$aItem['Onclick'] = str_replace( "{memberID}",    $aMenuInfo['memberID'],    $aItem['Onclick'] );
		$aItem['Onclick'] = str_replace( "{memberNick}",  $aMenuInfo['memberNick'],  $aItem['Onclick'] );
		$aItem['Onclick'] = str_replace( "{memberPass}",  getPassword( $aMenuInfo['memberID'] ),  $aItem['Onclick'] );
		
		$aItem['Onclick'] = str_replace( "{profileID}",   $aMenuInfo['profileID'],   $aItem['Onclick'] );
		$aItem['Onclick'] = str_replace( "{profileNick}", $aMenuInfo['profileNick'], $aItem['Onclick'] );

		$ret .= $oTemplMenu -> getTopMenuItem( _t( $aItem['Caption'] ), $aItem['Link'], $aItem['Target'], $aItem['Onclick'], ( $iItemID == $aMenuInfo['currentTop'] ), $iItemID );		
	}

	return $ret;
}

function CustomMenuDesign( $parent = 0 )
{
	global $aTopMenu;
	global $aMenuInfo;
	global $oTemplConfig;
	global $logged;

	if( !$aMenuInfo )
		getMenuInfo();
	
	if( !$parent )
		$parent = $aMenuInfo['currentTop'];
	
	$oTemplMenu = new BxTemplMenu( $oTemplConfig );
	$ret = '';

	if ($logged['admin'])
	{
		$aMenuInfo['memberID'] = $aMenuInfo['profileID'];
		$aMenuInfo['memberNick'] = $aMenuInfo['profileNick'];
		$aMenuInfo['memberLink'] = $aMenuInfo['profileLink'];
	}
	
	// Check is default user (can not create events)
	$ID = ($aMenuInfo['profileID'] > 0) ? $aMenuInfo['profileID'] : $aMenuInfo['memberID'];
	$defaultUser = (!$ID || db_value("SELECT `SiteEvents` FROM `Profiles` WHERE `ID`=$ID") == 0);
	
	foreach( $aTopMenu as $iItemID => $aItem )
	{
		if( $aItem['Type'] != 'custom' )
			continue;
	
		if( $aItem['Parent'] != $parent )
			continue;
	
		if(!$logged['admin'] && strpos( $aItem['Visible'], $aMenuInfo['visible'] ) === false )
			continue;
					
		if( strlen( $aItem['Check'] ) )
		{
			$sCheck = $aItem['Check'];
			$sCheck = str_replace( '\$', '$', $sCheck );
			
			$func = create_function('', $sCheck );
			
			if( !$func() )
				continue;
		}

		if ($aMenuInfo['memberID'] != $aMenuInfo['profileID'])
		{
			if ($aItem['Caption'] == '_Edit Profile' || $aItem['Caption'] == '_Customize Profile')
				continue; // hide "Edit Profile" and "Customize" link when viewing another persons profile (except admin)
		}
		if ($aItem['Caption'] == '_View Profile' && $aMenuInfo['memberID'] == $aMenuInfo['profileID'] &&
			!$logged['admin']) // replace "View Profile" to "My Profile" when viewing self profile 
			$aItem['Caption'] = '_My Profile';
		
		// Hide "Add Event" menu if not this permission
		if ($aItem['Caption'] == 'Add Event')
		{
			$memberID = (int)$_COOKIE['memberID'];
			if ($memberID > 0)
			{
				$createEventPerm = db_value("SELECT `SiteEvents` FROM `Profiles`
					WHERE `ID`=$memberID");
				if (!$createEventPerm) continue;
			}
			else continue;
		}
		
		// Hide "Events", "Calendar" menu for default users
		if (($aItem['Caption'] == '_My Events' || $aItem['Caption'] == '_My Calendar'
			|| $aItem['Caption'] == 'Events' || $aItem['Caption'] == 'Calendar') 
			&& $defaultUser) continue;
		
		// Replace 

		//generate
		list( $aItem['Link'] ) = explode( '|', $aItem['Link'] );
		
		$aItem['Link'] = str_replace( "{memberID}",    $aMenuInfo['memberID'],    $aItem['Link'] );
		$aItem['Link'] = str_replace( "{memberNick}",  $aMenuInfo['memberNick'],  $aItem['Link'] );
		$aItem['Link'] = str_replace( "{memberLink}",  $aMenuInfo['memberLink'],  $aItem['Link'] );
		
		$aItem['Link'] = str_replace( "{profileID}",   $aMenuInfo['profileID'],   $aItem['Link'] );
		$aItem['Link'] = str_replace( "{profileNick}", $aMenuInfo['profileNick'], $aItem['Link'] );
		$aItem['Link'] = str_replace( "{profileLink}", $aMenuInfo['profileLink'], $aItem['Link'] );
		
		$aItem['Onclick'] = str_replace( "{memberID}",    $aMenuInfo['memberID'],    $aItem['Onclick'] );
		$aItem['Onclick'] = str_replace( "{memberNick}",  $aMenuInfo['memberNick'],  $aItem['Onclick'] );
		$aItem['Onclick'] = str_replace( "{memberPass}",  getPassword( $aMenuInfo['memberID'] ),  $aItem['Onclick'] );
		
		$aItem['Onclick'] = str_replace( "{profileID}",   $aMenuInfo['profileID'],   $aItem['Onclick'] );
		$aItem['Onclick'] = str_replace( "{profileNick}", $aMenuInfo['profileNick'], $aItem['Onclick'] );
		
		if ($aItem['Caption'] == '_Customize Profile' && $logged['admin'])
			$aItem['Link'] .= "?memberID={$aMenuInfo['profileID']}";
		
		$ret .= $oTemplMenu -> getCustomMenuItem( _t( $aItem['Caption'] ), $aItem['Link'], $aItem['Target'], $aItem['Onclick'], ( $iItemID == $aMenuInfo['currentCustom'] ) );
	}
	
	return $ret;
}

function getAllMenus()
{
	global $aTopMenu;
	global $aMenuInfo;
	global $oTemplConfig;
	
	if( !$aMenuInfo )
		getMenuInfo();

	$oTemplMenu = new BxTemplMenu( $oTemplConfig );
	$ret = '';
	$aTTopMenu = $aTopMenu;
	foreach( $aTTopMenu as $iTItemID => $aTItem )
	{
		if( $aTItem['Type'] != 'top' && $aTItem['Type'] !='system')
			continue;
		
		if( strpos( $aTItem['Visible'], $aMenuInfo['visible'] ) === false )
			continue;
		
		if( strlen( $aTItem['Check'] ) )
		{
			$sCheck = $aTItem['Check'];
			$sCheck = str_replace( '\$', '$', $sCheck );
			
			$func = create_function('', $sCheck );
			if( !$func() )
				continue;
		}
		
		if( $aMenuInfo['currentTop'] == $iTItemID && basename( $_SERVER['PHP_SELF'] ) != 'index.php' )
			$display = 'block';
		else
			$display = 'none';

		$sCaption = _t( $aTItem['Caption'] );

		$sCaption = str_replace( "{memberNick}",  $aMenuInfo['memberNick'],  $sCaption );
		$sCaption = str_replace( "{profileNick}", $aMenuInfo['profileNick'], $sCaption );
		
		//generate
		$ret .= '
			<div class="hiddenMenu" style="margin-top:15px; display:' . $display . ';" id="hiddenMenu_' . $iTItemID . '"
			  onmouseover="holdHiddenMenu = ' . $iTItemID . ';"
			  onmouseout="holdHiddenMenu = currentTopItem; hideHiddenMenu( ' . $iTItemID . ' )">
				<div class="hiddenMenuBgCont">
					<div class="hiddenMenuCont">
						<table>
						<tr>
						<td><div class="topPageHeader" style="margin-right:20px;">' . $sCaption . '</div></td>
						<td>' .
						$oTemplMenu -> getCustomMenu( $iTItemID ) . '
						</td>
						</tr>
						</table>
					</div>
				</div>
				<div class="clear_both"></div>
			</div>';
	}
	
	return $ret;
}


//-----Replacement {ID} on current id in menu-link (array of menu items, users id)
function add_id ( $sMenuLink = "", $iId = 0 )
{
	if ( strpos($sMenuLink, "{ID}") !== FALSE && strpos($sMenuLink, "=") !== FALSE)
	{
		$sMenuLink = str_replace("{ID}", $iId, $sMenuLink);
	}
	return $sMenuLink;
}



//-----Check condition of menu item (function from `Check` field of `MemberMenu`)
function check_condition ( $sCon = "" )
{
	if ( strlen($sCon) > 0 )
	{
		$func = create_function('', $sCon);
		return $func();
	}
	else
	{
		return TRUE;
	}
}


function compileMenus()
{
	global $dir;
	
	$fMenu = @fopen($dir['inc']. "/menu_content.inc.php", "w");
	if( !$fMenu )
		return false;
	
	//write member menu
	fwrite( $fMenu, "\$aMenu = array(\n" );
	$aFields = array('Name','Caption','Link','MenuOrder','MenuType','MenuGroup','Visible','Target','Onclick','Check');
		
	$sQuery = "
		SELECT
			`ID`,
			`" . implode('`,
			`', $aFields ) . "`
		FROM `MemberMenu`
		ORDER BY `MenuOrder`
		";
	
	$rMenu = db_res( $sQuery );
	while( $aMenuItem = mysql_fetch_assoc( $rMenu ) )
	{
		fwrite( $fMenu, "\t" . str_pad( $aMenuItem['ID'], 2 ) . " => array(\n" );
		foreach( $aFields as $sKey => $sField )
		{
			$sCont = $aMenuItem[$sField];
			
			if( $sField == 'Link' )
				$sCont = htmlspecialchars_adv( $sCont );
			
			$sCont = str_replace( '\\', '\\\\', $sCont );
			$sCont = str_replace( '"', '\\"', $sCont );
			$sCont = str_replace( '$', '\\$', $sCont );
			
			fwrite( $fMenu, "\t\t" . str_pad( "'$sField'", 11 ) . " => \"$sCont\"" );
			
			if( $sKey < ( count( $aFields ) - 1 ) )
				fwrite( $fMenu, "," );
			
			fwrite( $fMenu, "\n" );
		}
		fwrite( $fMenu, "\t),\n" );
	}
	fwrite( $fMenu, ");\n\n" );
	
	fwriteCompileTopMenu($fMenu);
	
	fwrite( $fMenu, "return true;\n" );
	fclose( $fMenu );
}

function fwriteCompileTopMenu($fMenu)
{
	fwrite( $fMenu, "\$aTopMenu = array(\n" );
	$aFields = array('Type','Caption','Link','Visible','Target','Onclick','Check','Strict','Parent');
		
	$sQuery = "
		SELECT
			`ID`,
			`" . implode('`,
			`', $aFields ) . "`
		FROM `TopMenu`
		WHERE `Active` = 1 AND ( `Type` = 'system' OR `Type` = 'top' )
		ORDER BY `Type`,`Order`
		";
	
	$rMenu = db_res( $sQuery );
	while( $aMenuItem = mysql_fetch_assoc( $rMenu ) )
	{
		fwrite( $fMenu, "\t" . str_pad( $aMenuItem['ID'], 2 ) . " => array(\n" );
		foreach( $aFields as $sKey => $sField )
		{
			$sCont = $aMenuItem[$sField];
			
			if( $sField == 'Link' )
				$sCont = htmlspecialchars_adv( $sCont );
			
			$sCont = str_replace( '\\', '\\\\', $sCont );
			$sCont = str_replace( '"', '\\"', $sCont );
			$sCont = str_replace( '$', '\\$', $sCont );
			
			fwrite( $fMenu, "\t\t" . str_pad( "'$sField'", 11 ) . " => \"$sCont\"" );
			
			if( $sKey < ( count( $aFields ) - 1 ) )
				fwrite( $fMenu, "," );
			
			fwrite( $fMenu, "\n" );
		}
		fwrite( $fMenu, "\t),\n" );
		
		
		// write it's children
		$sQuery = "
			SELECT
				`ID`,
				`" . implode('`,
				`', $aFields ) . "`
			FROM `TopMenu`
			WHERE `Active` = 1 AND `Type` = 'custom' AND `Parent` = {$aMenuItem['ID']}
			ORDER BY `Order`
			";
		
		$rCMenu = db_res( $sQuery );
		while( $aMenuItem = mysql_fetch_assoc( $rCMenu ) )
		{
			fwrite( $fMenu, "\t" . str_pad( $aMenuItem['ID'], 2 ) . " => array(\n" );
			foreach( $aFields as $sKey => $sField )
			{
				$sCont = $aMenuItem[$sField];
				
				if( $sField == 'Link' )
					$sCont = htmlspecialchars_adv( $sCont );
				
				$sCont = str_replace( '\\', '\\\\', $sCont );
				$sCont = str_replace( '"', '\\"', $sCont );
				$sCont = str_replace( '$', '\\$', $sCont );
				
				fwrite( $fMenu, "\t\t" . str_pad( "'$sField'", 11 ) . " => \"$sCont\"" );
				
				if( $sKey < ( count( $aFields ) - 1 ) )
					fwrite( $fMenu, "," );
				
				fwrite( $fMenu, "\n" );
			}
			fwrite( $fMenu, "\t),\n" );
		}
		
		
	}
	fwrite( $fMenu, ");\n\n" );
}


function getTopPageHead()
{
	global $aMenuInfo;
	global $aTopMenu;
	
	if( !$aMenuInfo )
		getMenuInfo();
	
	if( $aTopMenu[ $aMenuInfo['currentTop'] ]['Caption'] == '{profileNick}' )
		return $aMenuInfo['profileNick'];
	elseif( $aTopMenu[ $aMenuInfo['currentTop'] ]['Caption'] == '{memberNick}' )
		return $aMenuInfo['memberNick'];
	else
		return _t( $aTopMenu[ $aMenuInfo['currentTop'] ]['Caption'] );
}
	

?>
