<?php

/**
 * Pagination Class.
 * Always works with GET urls
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	Pagination
 * @author		ExpressionEngine Dev Team
 * @link		http://codeigniter.com/user_guide/libraries/pagination.html
 */
class Pagination {

	var $base_url			= ''; // The page we are linking to
	var $total_rows  		= ''; // Total number of items (database results)
	var $per_page	 		= 10; // Max number of items you want shown per page
	var $num_links			=  2; // Number of "digit" links to show before/after the currently viewed page
	var $cur_page	 		=  0; // The current page being viewed
	var $first_link   		= '&lsaquo; First';
	var $next_link			= '&gt;';
	var $prev_link			= '&lt;';
	var $last_link			= 'Last &rsaquo;';
	var $uri_segment		= 3;
	var $full_tag_open		= '';
	var $full_tag_close		= '';
	var $first_tag_open		= '';
	var $first_tag_close	= '&nbsp;';
	var $last_tag_open		= '&nbsp;';
	var $last_tag_close		= '';
	var $cur_tag_open		= '&nbsp;<strong>';
	var $cur_tag_close		= '</strong>';
	var $next_tag_open		= '&nbsp;';
	var $next_tag_close		= '&nbsp;';
	var $prev_tag_open		= '&nbsp;';
	var $prev_tag_close		= '';
	var $num_tag_open		= '&nbsp;';
	var $num_tag_close		= '';
	var $query_string_segment = 'per_page';

	/**
	 * Constructor
	 *
	 * @access	public
	 * @param	array	initialization parameters
	 */
	function Pagination($params = array())
	{
		if (is_array($params) && count($params) > 0)
			$this->initialize($params);
	}

	/**
	 * Initialize Preferences
	 *
	 * @access	public
	 * @param	array	initialization parameters
	 * @return	void
	 */
	function initialize($params = array())
	{
		if (count($params) > 0)
			foreach ($params as $key => $val)
				if (isset($this->$key))
					$this->$key = $val;
	}

	/**
	 * Generate the pagination links
	 *
	 * @access	public
	 * @return	string
	 */
	function create_links()
	{
		// If our item count or per-page total is zero there is no need to continue.
		if ($this->total_rows == 0 OR $this->per_page == 0)
			return '';

		// Calculate the total number of pages
		$num_pages = ceil($this->total_rows / $this->per_page);

		// Is there only one page? Hm... nothing more to do here then.
		if ($num_pages == 1)
			return '';

		// Determine the current page number.
		$getSegm = @$_GET[$this->query_string_segment];
		if ($getSegm)
			$this->cur_page = (int) $getSegm;

		$this->num_links = (int)$this->num_links;

		if ($this->num_links < 1)
			$this->num_links = 3;

		if ( ! is_numeric($this->cur_page))
			$this->cur_page = 0;

		// Is the page number beyond the result range?
		// If so we show the last page
		if ($this->cur_page > $this->total_rows)
			$this->cur_page = ($num_pages - 1) * $this->per_page;

		$uri_page_number = $this->cur_page;
		$this->cur_page = floor(($this->cur_page/$this->per_page) + 1);

		// Calculate the start and end numbers. These determine
		// which number to start and end the digit links with
		$start = (($this->cur_page - $this->num_links) > 0) ? $this->cur_page - ($this->num_links - 1) : 1;
		$end   = (($this->cur_page + $this->num_links) < $num_pages) ? $this->cur_page + $this->num_links : $num_pages;

		// Remove existing query_string_segment from base_url if any
		$this->base_url = preg_replace("/&?{$this->query_string_segment}=\d+/i", '', $this->base_url);
		
		// Add a per_page query string
		$baseUrlQuery = @parse_url($this->base_url, PHP_URL_QUERY);
		if ($baseUrlQuery)
			$paramPrefix = '&';
		elseif (substr($this->base_url, -1) != '?')
    	$paramPrefix = '?';
		else
    	$paramPrefix = '';
		$this->base_url = rtrim($this->base_url) . $paramPrefix . $this->query_string_segment . '=';

  	// And here we go...
		$output = '';

		// Render the "First" link
		if  ($this->cur_page > ($this->num_links + 1))
			$output .= $this->first_tag_open.'<a href="'.$this->base_url.'0">'.$this->first_link.'</a>'.$this->first_tag_close;

		// Render the "previous" link
		if  ($this->cur_page != 1)
		{
			$i = $uri_page_number - $this->per_page;
			$output .= $this->prev_tag_open.'<a href="'.$this->base_url.$i.'">'.$this->prev_link.'</a>'.$this->prev_tag_close;
		}

		// Write the digit links
		for ($loop = $start -1; $loop <= $end; $loop++)
		{
			$i = ($loop * $this->per_page) - $this->per_page;

			if ($i >= 0)
			{
				if ($this->cur_page == $loop)
					$output .= $this->cur_tag_open.$loop.$this->cur_tag_close; // Current page
				else
					$output .= $this->num_tag_open.'<a href="'.$this->base_url.$i.'">'.$loop.'</a>'.$this->num_tag_close;
			}
		}

		// Render the "next" link
		if ($this->cur_page < $num_pages)
			$output .= $this->next_tag_open.'<a href="'.$this->base_url.($this->cur_page * $this->per_page).'">'
				. $this->next_link . '</a>' . $this->next_tag_close;

		// Render the "Last" link
		if (($this->cur_page + $this->num_links) < $num_pages)
		{
			$i = (($num_pages * $this->per_page) - $this->per_page);
			$output .= $this->last_tag_open.'<a href="'.$this->base_url.$i.'">'.$this->last_link.'</a>'.$this->last_tag_close;
		}

		// Kill double slashes.  Note: Sometimes we can end up with a double slash
		// in the penultimate link so we'll kill all double slashes.
		$output = preg_replace("#([^:])//+#", "\\1/", $output);

		// Add the wrapper HTML if exists
		$output = $this->full_tag_open . $output . $this->full_tag_close;

		return $output;
	}
}