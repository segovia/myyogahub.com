$aMenu = array(
	1  => array(
		'Name'      => "My_Account",
		'Caption'   => "_My Account",
		'Link'      => "member.php",
		'MenuOrder' => "1",
		'MenuType'  => "link",
		'MenuGroup' => "0",
		'Visible'   => "memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => ""
	),
	2  => array(
		'Name'      => "My_Profile",
		'Caption'   => "_My Profile",
		'Link'      => "profile_edit.php?ID={ID}|profile.php?ID={ID}|profile_customize.php?ID={ID}",
		'MenuOrder' => "2",
		'MenuType'  => "link",
		'MenuGroup' => "0",
		'Visible'   => "memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => ""
	),
	3  => array(
		'Name'      => "My_Mail",
		'Caption'   => "_My Mail",
		'Link'      => "mail.php?mode=inbox|mail.php|compose.php|messages_inbox.php|messages_outbox.php|contacts.php?show=block",
		'MenuOrder' => "3",
		'MenuType'  => "link",
		'MenuGroup' => "0",
		'Visible'   => "memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => ""
	),
	4  => array(
		'Name'      => "My_Photos",
		'Caption'   => "_My Photos",
		'Link'      => "upload_media.php?show=photos",
		'MenuOrder' => "4",
		'MenuType'  => "link",
		'MenuGroup' => "0",
		'Visible'   => "memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => ""
	),
	5  => array(
		'Name'      => "My_Videos",
		'Caption'   => "_My Videos",
		'Link'      => "browseVideo.php?userID={ID}",
		'MenuOrder' => "5",
		'MenuType'  => "link",
		'MenuGroup' => "0",
		'Visible'   => "memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => ""
	),
	6  => array(
		'Name'      => "My_Audio",
		'Caption'   => "_My Audio",
		'Link'      => "upload_media.php?show=audio",
		'MenuOrder' => "6",
		'MenuType'  => "link",
		'MenuGroup' => "0",
		'Visible'   => "memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => ""
	),
	7  => array(
		'Name'      => "My_Groups",
		'Caption'   => "_My Groups",
		'Link'      => "groups.php|group_create.php",
		'MenuOrder' => "7",
		'MenuType'  => "link",
		'MenuGroup' => "0",
		'Visible'   => "memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => ""
	),
	8  => array(
		'Name'      => "My_Events",
		'Caption'   => "_My Events",
		'Link'      => "events.php?action=show&amp;show_events=my|events.php?action=new",
		'MenuOrder' => "8",
		'MenuType'  => "link",
		'MenuGroup' => "0",
		'Visible'   => "memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => ""
	),
	9  => array(
		'Name'      => "My_Blog",
		'Caption'   => "_My Blog",
		'Link'      => "blog.php?owner={ID}",
		'MenuOrder' => "9",
		'MenuType'  => "link",
		'MenuGroup' => "0",
		'Visible'   => "memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => ""
	),
	10 => array(
		'Name'      => "My_Polls",
		'Caption'   => "_My Polls",
		'Link'      => "profile_poll.php",
		'MenuOrder' => "10",
		'MenuType'  => "link",
		'MenuGroup' => "0",
		'Visible'   => "memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => ""
	),
	11 => array(
		'Name'      => "My_Albums",
		'Caption'   => "_My Albums",
		'Link'      => "gallery.php?owner={ID}",
		'MenuOrder' => "11",
		'MenuType'  => "link",
		'MenuGroup' => "0",
		'Visible'   => "memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => ""
	),
	12 => array(
		'Name'      => "My_Guestbook",
		'Caption'   => "_My Guestbook",
		'Link'      => "guestbook.php?owner={ID}",
		'MenuOrder' => "12",
		'MenuType'  => "link",
		'MenuGroup' => "0",
		'Visible'   => "memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => ""
	),
	13 => array(
		'Name'      => "My_Greets",
		'Caption'   => "_My Greets",
		'Link'      => "contacts.php?show=greet&amp;list=i|contacts.php?show=greet&amp;list=me",
		'MenuOrder' => "13",
		'MenuType'  => "link",
		'MenuGroup' => "0",
		'Visible'   => "memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => ""
	),
	14 => array(
		'Name'      => "My_Faves",
		'Caption'   => "_My Faves",
		'Link'      => "contacts.php?show=hot&amp;list=i|contacts.php?show=hot&amp;list=me",
		'MenuOrder' => "14",
		'MenuType'  => "link",
		'MenuGroup' => "0",
		'Visible'   => "memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => ""
	),
	15 => array(
		'Name'      => "My_Friends",
		'Caption'   => "_My Friends",
		'Link'      => "contacts.php?show=friends|contacts.php?show=friends_inv&amp;list=i|contacts.php?show=friends_inv&amp;list=me",
		'MenuOrder' => "15",
		'MenuType'  => "link",
		'MenuGroup' => "0",
		'Visible'   => "memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => ""
	),
	16 => array(
		'Name'      => "My_Views",
		'Caption'   => "_My Views",
		'Link'      => "contacts.php?show=view&amp;list=i|contacts.php?show=view&amp;list=me",
		'MenuOrder' => "16",
		'MenuType'  => "link",
		'MenuGroup' => "0",
		'Visible'   => "memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => ""
	),
	19 => array(
		'Name'      => "My_Classifieds",
		'Caption'   => "_My Classifieds",
		'Link'      => "classifiedsmy.php?MyAds=1|classifieds.php?Browse=2|classifiedsmy.php?PostAd=1",
		'MenuOrder' => "19",
		'MenuType'  => "link",
		'MenuGroup' => "0",
		'Visible'   => "memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => ""
	),
);







$aTopMenu = array(
	1  => array(
		'Type'      => "system",
		'Caption'   => "_My Account",
		'Link'      => "member.php",
		'Visible'   => "memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => "",
		'Strict'    => "0",
		'Parent'    => "0"
	),
	2  => array(
		'Type'      => "custom",
		'Caption'   => "_Account Home",
		'Link'      => "member.php|member-update.php",
		'Visible'   => "memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => "",
		'Strict'    => "0",
		'Parent'    => "1"
	),
	15 => array(
		'Type'      => "custom",
		'Caption'   => "_My Membership",
		'Link'      => "membership.php",
		'Visible'   => "memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => "return ( getParam('free_mode') != 'on' );",
		'Strict'    => "0",
		'Parent'    => "1"
	),
	16 => array(
		'Type'      => "custom",
		'Caption'   => "_My Settings",
		'Link'      => "profile_edit.php?ID={memberID}",
		'Visible'   => "memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => "",
		'Strict'    => "0",
		'Parent'    => "1"
	),
	/*
	83 => array(
		'Type'      => "custom",
		'Caption'   => "_My Contacts",
		'Link'      => "contacts.php",
		'Visible'   => "memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => "",
		'Strict'    => "0",
		'Parent'    => "1"
	),
	*/
	3  => array(
		'Type'      => "system",
		'Caption'   => "_My Mail",
		'Link'      => "mail.php",
		'Visible'   => "memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => "",
		'Strict'    => "0",
		'Parent'    => "0"
	),
	17 => array(
		'Type'      => "custom",
		'Caption'   => "_Inbox",
		'Link'      => "mail.php?mode=inbox",
		'Visible'   => "memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => "",
		'Strict'    => "0",
		'Parent'    => "3"
	),
	12 => array(
		'Type'      => "custom",
		'Caption'   => "_Write",
		'Link'      => "compose.php",
		'Visible'   => "memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => "",
		'Strict'    => "0",
		'Parent'    => "3"
	),
	14 => array(
		'Type'      => "custom",
		'Caption'   => "_Sent",
		'Link'      => "mail.php?mode=outbox",
		'Visible'   => "memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => "",
		'Strict'    => "0",
		'Parent'    => "3"
	),
	13 => array(
		'Type'      => "custom",
		'Caption'   => "Blocked",
		'Link'      => "contacts.php?show=block&amp;list=i",
		'Visible'   => "memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => "",
		'Strict'    => "0",
		'Parent'    => "3"
	),
	18 => array(
		'Type'      => "custom",
		'Caption'   => "Blocking",
		'Link'      => "contacts.php?show=block&amp;list=me",
		'Visible'   => "memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => "",
		'Strict'    => "0",
		'Parent'    => "3"
	),
	
	9  => array(
		'Type'      => "system",
		'Caption'   => "{profileNick}",
		'Link'      => "{profileNick}|profile_edit.php?ID={profileID}",
		'Visible'   => "non,memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => "",
		'Strict'    => "0",
		'Parent'    => "0"
	),
	60 => array(
		'Type'      => "custom",
		'Caption'   => "_View Profile",
		'Link'      => "{profileLink}|{profileNick}|profile.php?ID={profileID}",
		'Visible'   => "non,memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => "",
		'Strict'    => "0",
		'Parent'    => "9"
	),
	20 => array(
		'Type'      => "custom",
		'Caption'   => "_Edit Profile",
		'Link'      => "profile_edit.php?ID={memberID}",
		'Visible'   => "memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => "",
		'Strict'    => "0",
		'Parent'    => "9"
	),	
	65 => array(
		'Type'      => "custom",
		'Caption'   => "_Photos Gallery",
		'Link'      => "browsePhoto.php?userID={profileID}",
		'Visible'   => "non,memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => "",
		'Strict'    => "0",
		'Parent'    => "9"
	),
	61 => array(
		'Type'      => "custom",
		'Caption'   => "_Profile Video",
		'Link'      => "browseVideo.php?userID={profileID}",
		'Visible'   => "non,memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => "",
		'Strict'    => "0",
		'Parent'    => "9"
	),
	66 => array(
		'Type'      => "custom",
		'Caption'   => "_Blog",
		'Link'      => "blogs.php?action=show_member_blog&amp;ownerID={profileID}|blogs.php?action=show_member_post&amp;ownerID={profileID}|blogs.php?action=search_by_tag&amp;tagKey={tagKey}&amp;ownerID={profileID}|blogs.php?action=edit_blog&amp;ownerID={profileID}|blogs.php?action=edit_post&amp;ownerID={profileID}|blogs.php?action=edit_category&amp;ownerID={profileID}|blogs.php?action=delete_category&amp;ownerID={profileID}|blogs.php?action=delete_post&amp;ownerID={profileID}",
		'Visible'   => "non,memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => "",
		'Strict'    => "0",
		'Parent'    => "9"
	),
	82 => array(
		'Type'      => "custom",
		'Caption'   => "_Member Friends",
		'Link'      => "viewFriends.php?iUser={profileID}",
		'Visible'   => "non,memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => "",
		'Strict'    => "0",
		'Parent'    => "9"
	),
	94 => array(
		'Type'      => "custom",
		'Caption'   => "Events",
		'Link'      => "events.php?show_events=upcoming&amp;action=show&amp;ID={profileID}",
		'Visible'   => "non,memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => "",
		'Strict'    => "0",
		'Parent'    => "9"
	),	
	93 => array(
		'Type'      => "custom",
		'Caption'   => "Calendar",
		'Link'      => "events.php?action=calendar&amp;ID={profileID}",
		'Visible'   => "non,memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => "",
		'Strict'    => "0",
		'Parent'    => "9"
	),
	/*
	67 => array(
		'Type'      => "custom",
		'Caption'   => "_Guestbook",
		'Link'      => "guestbook.php?owner={profileID}",
		'Visible'   => "non,memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => "",
		'Strict'    => "0",
		'Parent'    => "9"
	),
	*/
	74 => array(
		'Type'      => "custom",
		'Caption'   => "_Customize Profile",
		'Link'      => "javascript:void(0);",
		'Visible'   => "memb",
		'Target'    => "",
		'Onclick'   => "window.location.href='profile_customize.php'; if (navigator.appName != 'Netscape') window.event.returnValue=false;",
		'Check'     => "",
		'Strict'    => "0",
		'Parent'    => "9"
	),	
	4 => array(
		'Type'      => "top",
		'Caption'   => "My Profile",
		'Link'      => "{memberNick}|change_status.php",
		'Visible'   => "non,memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => "",
		'Strict'    => "0",
		'Parent'    => "0"
	),
	/*
	5  => array(
		'Type'      => "top",
		'Caption'   => "YogaHub",
		'Link'      => "index.php",
		'Visible'   => "non,memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => "",
		'Strict'    => "0",
		'Parent'    => "0"
	),
	1005  => array(
		'Type'      => "custom",
		'Caption'   => "YogaHub.com",
		'Link'      => "http://www.yogahub.com",
		'Visible'   => "non,memb",
		'Target'    => "_new",
		'Onclick'   => "",
		'Check'     => "",
		'Strict'    => "0",
		'Parent'    => "5"
	),
	1006  => array(
		'Type'      => "custom",
		'Caption'   => "YogaHub.org",
		'Link'      => "http://www.yogahub.org",
		'Visible'   => "non,memb",
		'Target'    => "_new",
		'Onclick'   => "",
		'Check'     => "",
		'Strict'    => "0",
		'Parent'    => "5"
	),
	1007  => array(
		'Type'      => "custom",
		'Caption'   => "ShopYogaHub.com",
		'Link'      => "http://www.shopyogahub.com",
		'Visible'   => "non,memb",
		'Target'    => "_new",
		'Onclick'   => "",
		'Check'     => "",
		'Strict'    => "0",
		'Parent'    => "5"
	),
	1008  => array(
		'Type'      => "custom",
		'Caption'   => "YogaHub.TV",
		'Link'      => "http://www.yogahub.tv",
		'Visible'   => "non,memb",
		'Target'    => "_new",
		'Onclick'   => "",
		'Check'     => "",
		'Strict'    => "0",
		'Parent'    => "5"
	),
	1009  => array(
		'Type'      => "custom",
		'Caption'   => "YogaPostCards.com",
		'Link'      => "http://www.yogapostcards.com",
		'Visible'   => "non,memb",
		'Target'    => "_new",
		'Onclick'   => "",
		'Check'     => "",
		'Strict'    => "0",
		'Parent'    => "5"
	),
	*/
	6  => array(
		'Type'      => "top",
		'Caption'   => "_Members",
		'Link'      => "browse.php|search_result.php",
		'Visible'   => "non,memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => "",
		'Strict'    => "0",
		'Parent'    => "0"
	),
	7  => array(
		'Type'      => "custom",
		'Caption'   => "_All Members",
		'Link'      => "browse.php",
		'Visible'   => "non,memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => "",
		'Strict'    => "0",
		'Parent'    => "6"
	),
	25 => array(
		'Type'      => "custom",
		'Caption'   => "_Online",
		'Link'      => "search_result.php?online_only=1",
		'Visible'   => "non,memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => "",
		'Strict'    => "0",
		'Parent'    => "6"
	),
	8  => array(
		'Type'      => "custom",
		'Caption'   => "_Search",
		'Link'      => "search.php",
		'Visible'   => "non,memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => "",
		'Strict'    => "0",
		'Parent'    => "6"
	),
	81 => array(
		'Type'      => "custom",
		'Caption'   => "_My Friends",
		'Link'      => "viewFriends.php?iUser={memberID}",
		'Visible'   => "memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => "",
		'Strict'    => "0",
		'Parent'    => "6"
	),
	44 => array(
		'Type'      => "top",
		'Caption'   => "_Blogs",
		'Link'      => "blogs.php",
		'Visible'   => "non,memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => "",
		'Strict'    => "0",
		'Parent'    => "0"
	),
	1001 => array(
		'Type'      => "custom",
		'Caption'   => "YogaHub Blog",
		'Link'      => "http://www.yogahub.org",
		'Visible'   => "non,memb",
		'Target'    => "_new",
		'Onclick'   => "",
		'Check'     => "",
		'Strict'    => "0",
		'Parent'    => "44"
	),
	86 => array(
		'Type'      => "custom",
		'Caption'   => "_Team Blog",
		'Link'      => "http://www.yogahub.com/team-blog/",
		'Visible'   => "non,memb",
		'Target'    => "_new",
		'Onclick'   => "",
		'Check'     => "",
		'Strict'    => "0",
		'Parent'    => "44"
	),	
	45 => array(
		'Type'      => "custom",
		'Caption'   => "_All Blogs",
		'Link'      => "blogs.php",
		'Visible'   => "non,memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => "",
		'Strict'    => "0",
		'Parent'    => "44"
	),	
	46 => array(
		'Type'      => "custom",
		'Caption'   => "_Top Blogs",
		'Link'      => "blogs.php?action=top_blogs",
		'Visible'   => "non,memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => "",
		'Strict'    => "0",
		'Parent'    => "44"
	),
	79 => array(
		'Type'      => "custom",
		'Caption'   => "_Top Posts",
		'Link'      => "blogs.php?action=top_posts",
		'Visible'   => "non,memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => "",
		'Strict'    => "0",
		'Parent'    => "44"
	),
	47 => array(
		'Type'      => "custom",
		'Caption'   => "_My Blog",
		'Link'      => "blogs.php?action=show_member_blog&amp;ownerID={memberID}",
		'Visible'   => "memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => "",
		'Strict'    => "0",
		'Parent'    => "44"
	),
	/*
	49 => array(
		'Type'      => "custom",
		'Caption'   => "_Add Category",
		'Link'      => "blogs.php?action=add_category&amp;ownerID={memberID}",
		'Visible'   => "memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => "",
		'Strict'    => "0",
		'Parent'    => "44"
	),
	50 => array(
		'Type'      => "custom",
		'Caption'   => "_Add Post",
		'Link'      => "blogs.php?action=new_post",
		'Visible'   => "memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => "",
		'Strict'    => "0",
		'Parent'    => "44"
	),
	*/
	41 => array(
		'Type'      => "top",
		'Caption'   => "_Photos",
		'Link'      => "browsePhoto.php|viewPhoto.php",
		'Visible'   => "non,memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => "",
		'Strict'    => "0",
		'Parent'    => "0"
	),
	42 => array(
		'Type'      => "custom",
		'Caption'   => "_All Photos",
		'Link'      => "browsePhoto.php|viewPhoto.php",
		'Visible'   => "non,memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => "",
		'Strict'    => "1",
		'Parent'    => "41"
	),
	43 => array(
		'Type'      => "custom",
		'Caption'   => "_Upload Photos",
		'Link'      => "uploadSharePhoto.php",
		'Visible'   => "memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => "",
		'Strict'    => "0",
		'Parent'    => "41"
	),
	69 => array(
		'Type'      => "custom",
		'Caption'   => "_My Photos",
		'Link'      => "browsePhoto.php?userID={memberID}",
		'Visible'   => "memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => "",
		'Strict'    => "0",
		'Parent'    => "41"
	),
	71 => array(
		'Type'      => "custom",
		'Caption'   => "_My Favorite Photos",
		'Link'      => "browsePhoto.php?action=fav",
		'Visible'   => "memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => "",
		'Strict'    => "0",
		'Parent'    => "41"
	),
	28 => array(
		'Type'      => "top",
		'Caption'   => "_Videos",
		'Link'      => "browseVideo.php|viewVideo.php",
		'Visible'   => "non,memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => "",
		'Strict'    => "0",
		'Parent'    => "0"
	),
	1002 => array(
		'Type'      => "custom",
		'Caption'   => "YogaHub.TV",
		'Link'      => "http://www.yogahub.tv",
		'Visible'   => "non,memb",
		'Target'    => "_new",
		'Onclick'   => "",
		'Check'     => "",
		'Strict'    => "0",
		'Parent'    => "28"
	),
	1003 => array(
		'Type'      => "custom",
		'Caption'   => "YogaTube.TV",
		'Link'      => "http://www.yogatube.tv",
		'Visible'   => "non,memb",
		'Target'    => "_new",
		'Onclick'   => "",
		'Check'     => "",
		'Strict'    => "0",
		'Parent'    => "28"
	),
	29 => array(
		'Type'      => "custom",
		'Caption'   => "_All Videos",
		'Link'      => "browseVideo.php|viewVideo.php",
		'Visible'   => "non,memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => "",
		'Strict'    => "1",
		'Parent'    => "28"
	),
	75 => array(
		'Type'      => "custom",
		'Caption'   => "_Top Video",
		'Link'      => "browseVideo.php?rate=top",
		'Visible'   => "non,memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => "",
		'Strict'    => "0",
		'Parent'    => "28"
	),
	30 => array(
		'Type'      => "custom",
		'Caption'   => "_Upload Video",
		'Link'      => "uploadShareVideo.php",
		'Visible'   => "memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => "",
		'Strict'    => "0",
		'Parent'    => "28"
	),
	68 => array(
		'Type'      => "custom",
		'Caption'   => "My Videos",
		'Link'      => "browseVideo.php?userID={memberID}",
		'Visible'   => "memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => "",
		'Strict'    => "0",
		'Parent'    => "28"
	),
	72 => array(
		'Type'      => "custom",
		'Caption'   => "My Favorites",
		'Link'      => "browseVideo.php?action=fav",
		'Visible'   => "memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => "",
		'Strict'    => "0",
		'Parent'    => "28"
	),
	51 => array(
		'Type'      => "top",
		'Caption'   => "_Events",
		'Link'      => "events.php?show_events=upcoming&amp;action=show|events.php",
		'Visible'   => "non,memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => "",
		'Strict'    => "0",
		'Parent'    => "0"
	),
	1004 => array(
		'Type'      => "custom",
		'Caption'   => "YogaHub Calendar",
		'Link'      => "http://www.yogahub.com/yoga-calendar.html",
		'Visible'   => "non,memb",
		'Target'    => "_new",
		'Onclick'   => "",
		'Check'     => "",
		'Strict'    => "0",
		'Parent'    => "51"
	),
	52 => array(
		'Type'      => "custom",
		'Caption'   => "List Events",
		'Link'      => "events.php?show_events=upcoming&amp;action=show",
		'Visible'   => "non,memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => "",
		'Strict'    => "0",
		'Parent'    => "51"
	),
	53 => array(
		'Type'      => "custom",
		'Caption'   => "Community Calendar",
		'Link'      => "events.php?action=calendar",
		'Visible'   => "non,memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => "",
		'Strict'    => "0",
		'Parent'    => "51"
	),
	87 => array(
		'Type'      => "system",
		'Caption'   => "_contact list",
		'Link'      => "contacts.php",
		'Visible'   => "memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => "",
		'Strict'    => "0",
		'Parent'    => "0"	
	),
	88 => array(
		'Type'      => "custom",
		'Caption'   => "_My Friends",
		'Link'      => "contacts.php?show=friends|contacts.php",
		'Visible'   => "memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => "",
		'Strict'    => "0",
		'Parent'    => "87"	
	),
	89 => array(
		'Type'      => "custom",
		'Caption'   => "_Hotlist",
		'Link'      => "contacts.php?show=hot",
		'Visible'   => "memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => "",
		'Strict'    => "0",
		'Parent'    => "87"	
	),
	90 => array(
		'Type'      => "custom",
		'Caption'   => "_Friend Requests",
		'Link'      => "contacts.php?show=friends_inv|contacts.php?show=friends_inv&amp;list=me",
		'Visible'   => "memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => "",
		'Strict'    => "0",
		'Parent'    => "87"	
	),
	91 => array(
		'Type'      => "custom",
		'Caption'   => "_Blocked",
		'Link'      => "contacts.php?show=block",
		'Visible'   => "memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => "",
		'Strict'    => "0",
		'Parent'    => "87"	
	),
	92 => array(
		'Type'      => "custom",
		'Caption'   => "_Viewed",
		'Link'      => "contacts.php?show=view",
		'Visible'   => "memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => "",
		'Strict'    => "0",
		'Parent'    => "87"	
	),
	/*
	10099 => array(
		'Type'      => "custom",
		'Caption'   => "Contact List",
		'Link'      => "javascript:void(0);",
		'Visible'   => "memb",
		'Target'    => "",
		'Onclick'   => "window.location.href='contacts.php';window.event.returnValue=false;",
		'Check'     => "",
		'Strict'    => "0",
		'Parent'    => "1"
	),*/
	55 => array(
		'Type'      => "custom",
		'Caption'   => "Add Event",
		'Link'      => "events.php?action=new",
		'Visible'   => "memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => "",
		'Strict'    => "0",
		'Parent'    => "51"	
	),
	54 => array(
		'Type'      => "custom",
		'Caption'   => "_My Events",
		'Link'      => "events.php?show_events=upcoming&amp;action=show&amp;ID={memberID}|events.php?show_events=past&amp;action=show&amp;ID={memberID}",
		'Visible'   => "memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => "",
		'Strict'    => "0",
		'Parent'    => "51"		
	),
	95 => array(
		'Type'      => "custom",
		'Caption'   => "_My Calendar",
		'Link'      => "events.php?action=calendar&amp;ID={memberID}",
		'Visible'   => "memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => "",
		'Strict'    => "0",
		'Parent'    => "51"		
	),
	96 => array(
		'Type'      => "system",
		'Caption'   => "_Customize",
		'Link'      => "profile_customize.php",
		'Visible'   => "memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => "",
		'Strict'    => "0",
		'Parent'    => "0"	
	),
	97 => array(
		'Type'      => "custom",
		'Caption'   => "Background",
		'Link'      => "profile_customize.php?show=bg|profile_customize.php",
		'Visible'   => "memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => "",
		'Strict'    => "0",
		'Parent'    => "96"	
	),
	98 => array(
		'Type'      => "custom",
		'Caption'   => "News-Feed",
		'Link'      => "profile_customize.php?show=newsfeed",
		'Visible'   => "memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => "",
		'Strict'    => "0",
		'Parent'    => "96"	
	),	
	99 => array(
		'Type'      => "custom",
		'Caption'   => "Blog",
		'Link'      => "profile_customize.php?show=blog",
		'Visible'   => "memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => "",
		'Strict'    => "0",
		'Parent'    => "96"	
	),	
	100 => array(
		'Type'      => "custom",
		'Caption'   => "Links",
		'Link'      => "profile_customize.php?show=links",
		'Visible'   => "memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => "",
		'Strict'    => "0",
		'Parent'    => "96"	
	),
	102 => array(
		'Type'      => "custom",
		'Caption'   => "Videos",
		'Link'      => "profile_customize.php?show=videos",
		'Visible'   => "memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => "",
		'Strict'    => "0",
		'Parent'    => "96"	
	),	
	101 => array(
		'Type'      => "top",
		'Caption'   => "_Forum",
		'Link'      => "http://www.yogahub.com/MYHforum.php",
		'Visible'   => "non,memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => "",
		'Strict'    => "0",
		'Parent'    => "0"
	)	
/*
	78 => array(
		'Type'      => "custom",
		'Caption'   => "_Search",
		'Link'      => "events.php?action=search",
		'Visible'   => "non,memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => "",
		'Strict'    => "0",
		'Parent'    => "51"
	)
*/
);








/*

,
	56 => array(
		'Type'      => "top",
		'Caption'   => "_Polls",
		'Link'      => "polls.php",
		'Visible'   => "non,memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => "",
		'Strict'    => "0",
		'Parent'    => "0"
	),
	57 => array(
		'Type'      => "custom",
		'Caption'   => "_All Polls",
		'Link'      => "polls.php",
		'Visible'   => "non,memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => "",
		'Strict'    => "0",
		'Parent'    => "56"
	),
	58 => array(
		'Type'      => "custom",
		'Caption'   => "_My Polls",
		'Link'      => "profile_poll.php",
		'Visible'   => "memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => "",
		'Strict'    => "0",
		'Parent'    => "56"
	),
	59 => array(
		'Type'      => "top",
		'Caption'   => "_Articles",
		'Link'      => "articles.php",
		'Visible'   => "non,memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => "",
		'Strict'    => "0",
		'Parent'    => "0"
	),
	38 => array(
		'Type'      => "top",
		'Caption'   => "_Music",
		'Link'      => "browseMusic.php|viewMusic.php",
		'Visible'   => "non,memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => "",
		'Strict'    => "0",
		'Parent'    => "0"
	),
	39 => array(
		'Type'      => "custom",
		'Caption'   => "_All Music",
		'Link'      => "browseMusic.php|viewMusic.php",
		'Visible'   => "non,memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => "",
		'Strict'    => "1",
		'Parent'    => "38"
	),
	40 => array(
		'Type'      => "custom",
		'Caption'   => "_Upload Music",
		'Link'      => "uploadShareMusic.php",
		'Visible'   => "memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => "",
		'Strict'    => "0",
		'Parent'    => "38"
	),
	70 => array(
		'Type'      => "custom",
		'Caption'   => "_My Music",
		'Link'      => "browseMusic.php?userID={memberID}",
		'Visible'   => "memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => "",
		'Strict'    => "0",
		'Parent'    => "38"
	),
	73 => array(
		'Type'      => "custom",
		'Caption'   => "_My Favorite Music",
		'Link'      => "browseMusic.php?action=fav",
		'Visible'   => "memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => "",
		'Strict'    => "0",
		'Parent'    => "38"
	),
	77 => array(
		'Type'      => "custom",
		'Caption'   => "_Top Music",
		'Link'      => "browseMusic.php?rate=top",
		'Visible'   => "non,memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => "",
		'Strict'    => "0",
		'Parent'    => "38"
	),
	22 => array(
		'Type'      => "top",
		'Caption'   => "_Groups",
		'Link'      => "groups_home.php",
		'Visible'   => "non,memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => "",
		'Strict'    => "0",
		'Parent'    => "0"
	),
	23 => array(
		'Type'      => "custom",
		'Caption'   => "_All Groups",
		'Link'      => "groups_home.php",
		'Visible'   => "non,memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => "",
		'Strict'    => "0",
		'Parent'    => "22"
	),
	24 => array(
		'Type'      => "custom",
		'Caption'   => "_Search",
		'Link'      => "groups_browse.php",
		'Visible'   => "non,memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => "",
		'Strict'    => "0",
		'Parent'    => "22"
	),
	26 => array(
		'Type'      => "custom",
		'Caption'   => "_My Groups",
		'Link'      => "groups.php",
		'Visible'   => "memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => "",
		'Strict'    => "0",
		'Parent'    => "22"
	),
	27 => array(
		'Type'      => "custom",
		'Caption'   => "_Create Group",
		'Link'      => "group_create.php",
		'Visible'   => "memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => "",
		'Strict'    => "0",
		'Parent'    => "22"
	),
	31 => array(
		'Type'      => "top",
		'Caption'   => "_Classifieds",
		'Link'      => "classifieds.php?Browse=1|classifieds.php|classifiedsmy.php",
		'Visible'   => "non,memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => "",
		'Strict'    => "0",
		'Parent'    => "0"
	),
	34 => array(
		'Type'      => "custom",
		'Caption'   => "_All Classifieds",
		'Link'      => "classifieds.php?Browse=1",
		'Visible'   => "non,memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => "",
		'Strict'    => "0",
		'Parent'    => "31"
	),
	35 => array(
		'Type'      => "custom",
		'Caption'   => "_Search",
		'Link'      => "classifieds.php?SearchForm=1",
		'Visible'   => "non,memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => "",
		'Strict'    => "0",
		'Parent'    => "31"
	),
	36 => array(
		'Type'      => "custom",
		'Caption'   => "_My Classifieds",
		'Link'      => "classifiedsmy.php?MyAds=1",
		'Visible'   => "memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => "",
		'Strict'    => "0",
		'Parent'    => "31"
	),
	37 => array(
		'Type'      => "custom",
		'Caption'   => "_Add Classified",
		'Link'      => "classifiedsmy.php?PostAd=1",
		'Visible'   => "memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => "",
		'Strict'    => "0",
		'Parent'    => "31"
	),
	48 => array(
		'Type'      => "top",
		'Caption'   => "_Forums",
		'Link'      => "orca/",
		'Visible'   => "non,memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => "",
		'Strict'    => "0",
		'Parent'    => "0"
	),
	33 => array(
		'Type'      => "top",
		'Caption'   => "_Boards",
		'Link'      => "board.php",
		'Visible'   => "non,memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => "",
		'Strict'    => "0",
		'Parent'    => "0"
	),
	32 => array(
		'Type'      => "top",
		'Caption'   => "_Chat",
		'Link'      => "chat.php",
		'Visible'   => "non,memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => "",
		'Strict'    => "0",
		'Parent'    => "0"
	),
	62 => array(
		'Type'      => "custom",
		'Caption'   => "_Music Gallery",
		'Link'      => "browseMusic.php?userID={profileID}",
		'Visible'   => "non,memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => "",
		'Strict'    => "0",
		'Parent'    => "9"
	),
	76 => array(
		'Type'      => "custom",
		'Caption'   => "_Top Photos",
		'Link'      => "browsePhoto.php?rate=top",
		'Visible'   => "non,memb",
		'Target'    => "",
		'Onclick'   => "",
		'Check'     => "",
		'Strict'    => "0",
		'Parent'    => "41"
	),
		
*/


return true;
