<?php

/**
 * Trim each value in array
 * @param $array array
 * @return array
 */
function array_trim_values($array)
{
	foreach ($array as $key => $value)
		$array[$key] = trim($value);
	return $array;		
}

class KeywordsExtractor
{
	
var $stopWords;
var $minCharsInKeyword;
var $minWordsInKeyword = 1;
var $maxWordsInKeyword = 4;
	
function KeywordsExtractor($stopWordsFile, $minCharsInKeyword = 2)
{
	require_once('PorterStemmer.php');
	
	if (file_exists($stopWordsFile))
	{    	
	    $stopWordsStr  = file_get_contents($stopWordsFile);
		$this->stopWords = array_trim_values(mb_split('[ \n]+', mb_strtolower($stopWordsStr, 'utf-8')));
	}	
	$this->minCharsInKeyword = $minCharsInKeyword;
}

/**
 * Remove HTML tags, including invisible text such as style and
 * script code, and embedded objects.  Add line breaks around
 * block-level tags to prevent word joining after tag removal.
 */
function strip_html_tags( $text )
{
    $text = preg_replace(
        array(
          // Remove invisible content
            '@<head[^>]*?>.*?</head>@siu',
            '@<style[^>]*?>.*?</style>@siu',
            '@<script[^>]*?.*?</script>@siu',
            '@<object[^>]*?.*?</object>@siu',
            '@<embed[^>]*?.*?</embed>@siu',
            '@<applet[^>]*?.*?</applet>@siu',
            '@<noframes[^>]*?.*?</noframes>@siu',
            '@<noscript[^>]*?.*?</noscript>@siu',
            '@<noembed[^>]*?.*?</noembed>@siu',
          // Add line breaks before and after blocks
            '@</?((address)|(blockquote)|(center)|(del))@iu',
            '@</?((div)|(h[1-9])|(ins)|(isindex)|(p)|(pre))@iu',
            '@</?((dir)|(dl)|(dt)|(dd)|(li)|(menu)|(ol)|(ul))@iu',
            '@</?((table)|(th)|(td)|(caption))@iu',
            '@</?((form)|(button)|(fieldset)|(legend)|(input))@iu',
            '@</?((label)|(select)|(optgroup)|(option)|(textarea))@iu',
            '@</?((frameset)|(frame)|(iframe))@iu',
        ),
        array(
            ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
            "\n\$0", "\n\$0", "\n\$0", "\n\$0", "\n\$0", "\n\$0",
            "\n\$0", "\n\$0",
        ),
        $text );
    return strip_tags( $text );
}

/**
 * Strip punctuation from text.
 */
function strip_punctuation( $text )
{
    $urlbrackets    = '\[\]\(\)';
    $urlspacebefore = ':;\'_\*%@&?!' . $urlbrackets;
    $urlspaceafter  = '\.,:;\'\-_\*@&\/\\\\\?!#' . $urlbrackets;
    $urlall         = '\.,:;\'\-_\*%@&\/\\\\\?!#' . $urlbrackets;
 
    $specialquotes  = '\'"\*<>';
 
    $fullstop       = '\x{002E}\x{FE52}\x{FF0E}';
    $comma          = '\x{002C}\x{FE50}\x{FF0C}';
    $arabsep        = '\x{066B}\x{066C}';
    $numseparators  = $fullstop . $comma . $arabsep;
 
    $numbersign     = '\x{0023}\x{FE5F}\x{FF03}';
    $percent        = '\x{066A}\x{0025}\x{066A}\x{FE6A}\x{FF05}\x{2030}\x{2031}';
    $prime          = '\x{2032}\x{2033}\x{2034}\x{2057}';
    $nummodifiers   = $numbersign . $percent . $prime;
 
    return preg_replace(
        array(
        // Remove separator, control, formatting, surrogate,
        // open/close quotes.
            '/[\p{Z}\p{Cc}\p{Cf}\p{Cs}\p{Pi}\p{Pf}]/u',
        // Remove other punctuation except special cases
            '/\p{Po}(?<![' . $specialquotes .
                $numseparators . $urlall . $nummodifiers . '])/u',
        // Remove non-URL open/close brackets, except URL brackets.
            '/[\p{Ps}\p{Pe}](?<![' . $urlbrackets . '])/u',
        // Remove special quotes, dashes, connectors, number
        // separators, and URL characters followed by a space
            '/[' . $specialquotes . $numseparators . $urlspaceafter .
                '\p{Pd}\p{Pc}]+((?= )|$)/u',
        // Remove special quotes, connectors, and URL characters
        // preceded by a space
            '/((?<= )|^)[' . $specialquotes . $urlspacebefore . '\p{Pc}]+/u',
        // Remove dashes preceded by a space, but not followed by a number
            '/((?<= )|^)\p{Pd}+(?![\p{N}\p{Sc}])/u',
        // Remove consecutive spaces
            '/ +/',
        ),
        ' ',
        $text );
}
	
/**
 * Strip symbols from text.
 */
function strip_symbols( $text )
{
    $plus   = '\+\x{FE62}\x{FF0B}\x{208A}\x{207A}';
    $minus  = '\x{2012}\x{208B}\x{207B}';
 
    $units  = '\\x{00B0}\x{2103}\x{2109}\\x{23CD}';
    $units .= '\\x{32CC}-\\x{32CE}';
    $units .= '\\x{3300}-\\x{3357}';
    $units .= '\\x{3371}-\\x{33DF}';
    $units .= '\\x{33FF}';
 
    $ideo   = '\\x{2E80}-\\x{2EF3}';
    $ideo  .= '\\x{2F00}-\\x{2FD5}';
    $ideo  .= '\\x{2FF0}-\\x{2FFB}';
    $ideo  .= '\\x{3037}-\\x{303F}';
    $ideo  .= '\\x{3190}-\\x{319F}';
    $ideo  .= '\\x{31C0}-\\x{31CF}';
    $ideo  .= '\\x{32C0}-\\x{32CB}';
    $ideo  .= '\\x{3358}-\\x{3370}';
    $ideo  .= '\\x{33E0}-\\x{33FE}';
    $ideo  .= '\\x{A490}-\\x{A4C6}';
 
    return preg_replace(
        array(
        // Remove modifier and private use symbols.
            '/[\p{Sk}\p{Co}]/u',
        // Remove mathematics symbols except + - = ~ and fraction slash
            '/\p{Sm}(?<![' . $plus . $minus . '=~\x{2044}])/u',
        // Remove + - if space before, no number or currency after
            '/((?<= )|^)[' . $plus . $minus . ']+((?![\p{N}\p{Sc}])|$)/u',
        // Remove = if space before
            '/((?<= )|^)=+/u',
        // Remove + - = ~ if space after
            '/[' . $plus . $minus . '=~]+((?= )|$)/u',
        // Remove other symbols except units and ideograph parts
            '/\p{So}(?<![' . $units . $ideo . '])/u',
        // Remove consecutive white space
            '/ +/',
        ),
        ' ',
        $text );
}

/**
 * Remove html tags, punctuation, symbols and stopwords from $content
 * @param $content string
 * @return array
 */
function remove_html_stop_words($content)
{
	// Prepare content
	$content = $this->strip_html_tags($content);
	$content = html_entity_decode($content, ENT_QUOTES, "utf-8");
	$content = $this->strip_punctuation($content);
	$content = strtolower($this->strip_symbols($content));
	
	// Split content to words
	mb_regex_encoding("utf-8");
	
	$words = mb_split(' +', $content);

	// Remove stop words before stemming
	if ($this->stopWords)
	    $words = array_values(array_diff($words, $this->stopWords));

	return $words;
}

/**
 * Stem each word in the word list.
 * Return array (if $returnArray = true; string otherwise) 
 * of stemmed words with length >= minCharsInKeyword
 * @param $words array
 * @param $returnArray boolean
 * @return mixed
 */
function stemWords($words, $returnArray = true)
{
	$stemmedLimitedWords = array();
	foreach ($words as $word)
	{
		$stemWord = PorterStemmer::Stem($word);
		if (strlen($stemWord) >= $this->minCharsInKeyword)
    		$stemmedLimitedWords[] = $stemWord;
	}
	
	return ($returnArray) ? $stemmedLimitedWords : implode(' ', $stemmedLimitedWords);
}

/**
 * Return array of keywords which contain of $numWordsInKeyword words
 * and sorted by count usage
 * @param $words array
 * @param $numWordsInKeyword int
 * @return array
 */
function extract_keywords($words, $numWordsInKeyword = 1)
{
	if ($numWordsInKeyword < $this->minWordsInKeyword) $numWordsInKeyword = $this->minWordsInKeyword;
	if ($numWordsInKeyword > $this->maxWordsInKeyword) $numWordsInKeyword = $this->maxWordsInKeyword;
		
	if ($numWordsInKeyword == 1)
	{
		$keywords = $this->stemWords($words);
		
		// Remove stop words after stemming
		if ($this->stopWords)
		    $keywords = array_values(array_diff($keywords, $this->stopWords));
	}
	else
	{
		// Create array of keywords with $numWordsInKeyword words
		$multiKeywords = array();
		$lastMultiKwdIndex = count($words) - $numWordsInKeyword;

		for ($i = 0; $i <= $lastMultiKwdIndex; $i++)
		{
			$multiKeywordStr = '';
			$lastWordInd = $i + $numWordsInKeyword - 1;
			for ($j = $i; $j <= $lastWordInd; $j++)
			{
				// Check for minimum chars and no words duplicates in keyword
				if (strlen($words[$j]) < $this->minCharsInKeyword ||
					$j < $lastWordInd - 1 && $words[$j] == $words[$j + 1])
					continue 2;
				if (strlen($multiKeywordStr)) $multiKeywordStr .= ' ';
				$multiKeywordStr .= $words[$j];
			}
			$multiKeywords[] = $multiKeywordStr;
		}
		$keywords = $multiKeywords;
	}
	
	// Sort keywords by count of keyword usage
	$keywordsByUsage = array_count_values($keywords);
	arsort($keywordsByUsage, SORT_NUMERIC);
	
	return array_keys($keywordsByUsage);
}

/**
 * Return true if $value contains in values of $array
 * @param $array array
 * @param $value string
 * @return bool
 */
function intersected_by($array, $value)
{
	if (count($array) == 0 || trim($value) == '') return true;
	foreach ($array as $arrayValue)
		if (strpos($arrayValue, $value) !== false)
			return true;
	return false;
}

/**
 * Extract keywords from title and content
 * @param $title string
 * @param $content string
 * @param $maxKeywords int (maximum keywords to extraction)
 * @return array of keywords
 */
function extract($title, $content, $maxKeywords)
{
	$titleWords = $this->remove_html_stop_words($title);
	$contentWords = $this->remove_html_stop_words($content);
	$contentWordsStr = implode(' ', $contentWords);
	
	// Stem each word in prepared content (without html, symbols, stopwords)
	$contentStem = $this->stemWords($contentWords, false);
	
	$titleKeywords = $this->extract_keywords($titleWords, 1);
	$titleDblKeywords = $this->extract_keywords($titleWords, 2);
	
	// Get title keywords which was found in stemmed content
	$titleKeywordsInContent = array();
	foreach ($titleKeywords as $titleKeyword)
		if (strpos($contentStem, $titleKeyword) !== false)
			$titleKeywordsInContent[] = $titleKeyword;

	// Get title double keywords which was found in content
	$titleDblKeywordsInContent = array();
	foreach ($titleDblKeywords as $titleDblKeyword)
		if (strpos($contentWordsStr, $titleDblKeyword) !== false)
			$titleDblKeywordsInContent[] = $titleDblKeyword;	
			
	// Add $titleKeywordsInContent to $singleKeywords array if they not contain in $titleDblKeywordsInContent
	if (count($titleDblKeywordsInContent) == 0)
		$singleKeywords = $titleKeywordsInContent;
	else
	{
		$singleKeywords = array();
		foreach ($titleKeywordsInContent as $titleKeywordInContent)
			if (!$this->intersected_by($titleDblKeywordsInContent, $titleKeywordInContent))
				$singleKeywords[] = $titleKeywordInContent;
	}
	
	// Merge single and double keywords from title
	$keywords = array_merge($singleKeywords, $titleDblKeywordsInContent);

	if (count($keywords) < $maxKeywords)
	{
		$contentKeywords = $this->extract_keywords($contentWords, 1);
		foreach ($contentKeywords as $contentKeyword)
			if (!$this->intersected_by($keywords, $contentKeyword))
			{
				$keywords[] = $contentKeyword;
				break;
			}
	}
	if (count($keywords) < $maxKeywords)
	{	
		$contentDblKeywords = $this->extract_keywords($contentWords, 2);
		foreach ($contentDblKeywords as $contentDblKeyword)
			if (!$this->intersected_by($keywords, $contentDblKeyword))
			{
				$keywords[] = $contentDblKeyword;
				break;
			}	
	}

	return $keywords;
}
}

?>