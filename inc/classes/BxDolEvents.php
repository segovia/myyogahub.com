<?

//require_once( BX_DIRECTORY_PATH_INC . 'prof.inc.php' );
//require_once( BX_DIRECTORY_PATH_INC . 'sdating.inc.php' );

require_once( BX_DIRECTORY_PATH_INC . 'header.inc.php' );
require_once( BX_DIRECTORY_PATH_INC . 'design.inc.php' );
require_once( BX_DIRECTORY_PATH_INC . 'profiles.inc.php' );
require_once( BX_DIRECTORY_PATH_INC . 'utils.inc.php' );
require_once( BX_DIRECTORY_PATH_INC . 'newsfeed.inc.php' );
require_once( BX_DIRECTORY_PATH_INC . 'tags.inc.php' );
require_once(BX_DIRECTORY_PATH_CLASSES . 'BxDolComments.php' );
require_once( BX_DIRECTORY_PATH_ROOT . 'eventPhotos.php' );

/*

* class for Events

*/

class BxDolEvents {

	//variables



	//div element with spacer image

	//var $sRowSpacerDiv;



	//count of events per page

	var $iEventsPerPage; // count of events to show per page



	//var $iNewEvPicWidth = 200;

	//var $iNewEvPicHeight = 150;



	//max sizes of pictures for resizing during upload

	var $iIconSize;

	var $iThumbSize;

	var $iImgSize;



	//admin mode, can All actions

	var $bAdminMode;



	//path to image pic_not_avail.gif

	var $sPicNotAvail;

	//path to spacer image

	var $sSpacerPath;



	var $iLastInsertedID = -1;



	/**

	 * constructor

	 */

	function BxDolEvents($bAdmMode = FALSE) {

		global $site;



		$this->sSpacerPath = $site['images'].'spacer.gif';

		$this->sPicNotAvail = 'images/icons/group_no_pic.gif';

		$this->bAdminMode = $bAdmMode;



		// $this->sRowSpacerDiv = <<<EOF

		// <table cellspacing="0" cellpadding="0" height="10" border="0">

		// <td><img src="{$this->sSpacerPath}"></td>

		// </table>

		// EOF;

		$this->iEventsPerPage = 10;



		$this->iIconSize = 45;

		$this->iThumbSize = 110;

		$this->iImgSize = 340;

	}



	/**

	 * Compose Array of posted data before validating (post ad or edit)

	 *

	 * @return Array of posted variables

	 */

	function FillPostEventArrByPostValues() {

		$sEventTitle			= process_db_input( $_POST['event_title'] );

		$sEventDesc				= process_db_input( $_POST['event_desc'] );

		$sEventStatusMessage	= process_db_input( $_POST['event_statusmsg'] );

		$sTags					= process_db_input( $_POST['event_tags'] );



		$sEventCountry	= process_db_input( $_POST['event_country'] );

		$sEventCity		= process_db_input( $_POST['event_city'] );

		$sEventPlace	= process_db_input( $_POST['event_place'] );



		$sEventStart		= strtotime( $_POST['event_start'] );

		$sEventEnd			= strtotime( $_POST['event_end'] );

		$sEventSaleStart	= strtotime( $_POST['event_sale_start'] );

		$sEventSaleEnd		= strtotime( $_POST['event_sale_end'] );



		$iEventCountF = (int)$_POST['event_count_female'];

		$iEventCountM = (int)$_POST['event_count_male'];



		$aElements = array('Title' => $sEventTitle, 'Description' => $sEventDesc, 'Status message' => $sEventStatusMessage,

		'City' => $sEventCity, 'Country' => $sEventCountry, 'Place' => $sEventPlace, 'Event start' => $sEventStart,

		'Event end' => $sEventEnd, 'Ticket Sale Start' => $sEventSaleStart, 'Ticket Sale End' => $sEventSaleEnd,

		'Female Ticket Count' => $iEventCountF, 'Male Ticket Count' => $iEventCountM,

		'Tags' => $sTags);



		return $aElements;

	}



	/**

	 * Generate common forms and includes js

	 *

	 * @return HTML presentation of data

	 */

	function PrintCommandForms() {

		$sRetHtml = <<<EOF

<script src="{$sJSPath}inc/js/dynamic_core.js.php" type="text/javascript"></script>

<form action="{$_SERVER['PHP_SELF']}" method="post" name="command_edit_event">

	<input type="hidden" name="action" value="edit_event" />

	<input type="hidden" name="EditEventID" id="EditEventID" value="" />

</form>

<form action="{$_SERVER['PHP_SELF']}" method="post" name="command_delete_event">

	<input type="hidden" name="action" value="delete_event" />

	<input type="hidden" name="DeleteEventID" id="DeleteEventID" value="" />

</form>

EOF;

		return $sRetHtml;

	}



	function CheckLogged() {

		global $logged;

		if( !$logged['member'] && !$logged['admin'] ) {

			member_auth(0);

		}

	}



	function DeleteProfileEvents($iProfileID) {

		if ($this->bAdminMode==true) {

			$vDelSQL = db_res("SELECT `ID` FROM `SDatingEvents` WHERE `ResponsibleID` = {$iProfileID}");

			while( $aEnent = mysql_fetch_assoc($vDelSQL) ) {

				$this->PageSDatingDeleteEvent($aEnent['ID']);

			}

		}

	}

	function PageSDatingDeleteEvent($iDelEventID = -1) {
		$this->CheckLogged();
		global $dir, $memcacher;

		$sEventID = ($iDelEventID>0) ? $iDelEventID : process_db_input( (int)$_POST['DeleteEventID'] );
		$eventData = db_arr("SELECT `ResponsibleID`, `Featured` FROM `SDatingEvents` WHERE `ID` = {$sEventID} LIMIT 1");

		//�������� �� ���������� ��������
		if ($eventData['ResponsibleID']==(int)$_COOKIE['memberID'] || $this->bAdminMode) {
			$vPosts = db_res( "DELETE FROM `SDatingParticipants` WHERE `IDEvent`={$sEventID}" );

			// Delete RSS Image of event
			if (file_exists("{$dir['sdatingImage']}rss/$sEventID.jpg"))
				unlink("{$dir['sdatingImage']}rss/$sEventID.jpg");
			
			//TODO delete pictures of event
			$aPicSQL = db_arr("SELECT `PhotoFilename` FROM `SDatingEvents` WHERE `ID` = {$sEventID}");
			$sMediaFileName = $aPicSQL['PhotoFilename'];
			if ($sMediaFileName != '') {
				if (unlink ( $dir['sdatingImage'] . $sMediaFileName ) == FALSE)
					$sRetHtml .= MsgBox(_t('_FAILED_TO_DELETE_PIC', $sMediaFileName));
				if (unlink ( $dir['sdatingImage'] . 'thumb_'.$sMediaFileName ) == FALSE)
					$sRetHtml .= MsgBox(_t('_FAILED_TO_DELETE_PIC', $sMediaFileName));
				if (unlink ( $dir['sdatingImage'] . 'icon_'.$sMediaFileName ) == FALSE)
					$sRetHtml .= MsgBox(_t('_FAILED_TO_DELETE_PIC', $sMediaFileName));
					
				$aMediaPicSQL = db_arr("SELECT `SDatingEvents`.*, `media`.med_file 
					FROM `SDatingEvents` 
					LEFT JOIN `media` ON `media`.med_id = `SDatingEvents`.PrimPhoto
					WHERE `SDatingEvents`.ID = $sEventID LIMIT 1");
				$sMediaFileName2 = $aMediaPicSQL['med_file'];
					
				// Delete from profile folder
				if (unlink ("{$dir['profileImage']}{$eventData['ResponsibleID']}/photo_$sMediaFileName2") == FALSE)
					$sRetHtml .= MsgBox(_t('_FAILED_TO_DELETE_PIC', $sMediaFileName2));
				if (unlink ("{$dir['profileImage']}{$eventData['ResponsibleID']}/thumb_$sMediaFileName2") == FALSE)
					$sRetHtml .= MsgBox(_t('_FAILED_TO_DELETE_PIC', $sMediaFileName2));
				if (unlink ("{$dir['profileImage']}{$eventData['ResponsibleID']}/icon_$sMediaFileName2") == FALSE)
					$sRetHtml .= MsgBox(_t('_FAILED_TO_DELETE_PIC', $sMediaFileName2));
			}

			$sQuery = "DELETE FROM `SDatingEvents` WHERE `SDatingEvents`.`ID` = {$sEventID} LIMIT 1";
			db_res( $sQuery );
			
			// Delete all photos associated with this event
			$sQuery = "DELETE FROM `media` WHERE `med_prof_id` = $sEventID";
			db_res( $sQuery );
			
			// Clear all records associated with this event from newsfeedtrack table
			db_res("DELETE FROM NewsFeedTrack WHERE mediaid=$sEventID AND type IN (9,10,11)");
			
			// Update featured events in memcache
			if ($eventData['Featured'])
			{
				$memcache = $memcacher->obj();
				$memcache->set('eventsLastUpdatedTime', time());
				updateFeaturedEventsInMemcache();
				clearYHCache();
			}
			
			return MsgBox(_t('_event_deleted'));
		} 
		elseif ($aOwner['ResponsibleID']!=(int)$_COOKIE['memberID'])
			return MsgBox(_t('_Hacker String'));
		else
			return MsgBox(_t('_event_delete_failed'));
	}

	/**

	 * Compose Array of errors during filling (validating)

	 *

	 * @param $aElements	Input Array with data

	 * @return Array with errors

	 */

	function CheckEventErrors( $aElements ) {

		$aErr = array();

		foreach( $aElements as $sFieldName => $sFieldValue ) {

			switch( $sFieldName ) {

				// case 'Status message':

				case 'Title':

				case 'Country':

					if( !strlen($sFieldValue) )

					$aErr[ $sFieldName ] = "{$sFieldName} is required";

					break;

				case 'Description':

					if( strlen($sFieldValue) < 50 )

					$aErr[ $sFieldName ] = "{$sFieldName} must be 50 symbols at least";

					break;

				case 'City':

				case 'Place':

					if( !strlen($sFieldValue) && $this->bAdminMode==TRUE )

					$aErr[ $sFieldName ] = "{$sFieldName} is required";

					break;

				case 'Event start':
					if(!strlen(trim($sFieldValue)))
						$aErr[ $sFieldName ] = "{$sFieldName} is required";
					break;
					
				case 'Event end':
					if(!strlen(trim($sFieldValue)))
						$aErr[ $sFieldName ] = "{$sFieldName} is required";
					elseif ($aElements['Event end'] < $aElements['Event start'])
						$aErr[ $sFieldName ] = "Event end can not be less than event start";
					break;

				case 'Ticket Sale Start':
				case 'Ticket Sale End':

					if( $sFieldValue == -1 && $this->bAdminMode==TRUE )

					$aErr[ $sFieldName ] = "{$sFieldName} is required";

					break;



				case 'Female Ticket Count':

				case 'Male Ticket Count':

					if( $sFieldValue < 1 && $this->bAdminMode==TRUE )

					$aErr[ $sFieldName ] = "{$sFieldName} must be positive";

					break;

			}

		}

		return $aErr;

	}

	/**
	 * function for New/Edit event
	 * @return Text Result
	 */
	function SDAddEvent( $iEventID = -1 ) {//for update event

		global $logged, $dir, $site, $memcacher;

		if( !$logged['member'] && !$logged['admin'] )
			member_auth(0);		
			
		// collect information about current member or event owner if admin mode
		if ($logged['member'])
			$aMember['ID'] = (int)$_COOKIE['memberID'];
		elseif ($logged['admin'])
		{ 
			if (!$iEventID) return SDATING_ERROR_MEMBER_PROCESS;
			$aMember['ID'] = db_value("SELECT ResponsibleID FROM SDatingEvents WHERE `ID` = {$iEventID} LIMIT 1");
		}
		if (!$aMember['ID']) return SDATING_ERROR_MEMBER_PROCESS;
		$aMemberData = getProfileInfo( $aMember['ID'] ); //db_arr( "SELECT `ID`, `RealName`, `Email`, `Phone` FROM `Profiles` WHERE `ID` = {$aMember['ID']}" );

		// common
		$sEventTitle          = process_db_input( $_POST['event_title'] );
		$sEventDesc           = $this->process_html_db_input($_POST['event_desc']);
		$sEventStatusMessage  = process_db_input( $_POST['event_statusmsg'] );

		// event place
		$sEventCountry = process_db_input( $_POST['event_country'] );
		$sEventCity    = process_db_input( $_POST['event_city'] );
		$EventPlace    = process_db_input( $_POST['event_place'] );
		$sTags    = process_db_input( $_POST['event_tags'] );
		$aTags = explodeTags($sTags);
		$sTags = implode(",", $aTags);

		//$sPictureName = $_FILES['event_photo']['name'];
		//$sThumbName   = getThumbNameByPictureName( $sPictureName, true );
		//$sIconName	  = "icon_" . $sPictureName;
	
		$sFileName = ($iEventID > 0) ? db_value("SELECT `PhotoFilename` FROM `SDatingEvents` WHERE `ID`={$iEventID}") : '';

		// Check image action
		$uploaded = false;
		if (isset($_POST['imageAction']))
		{
			if ($_POST['imageAction'] == 'clear')
			{
				$bigImageName = $dir['sdatingImage'] . $sFileName;
				$thumbImageName = $dir['sdatingImage'] . 'thumb_'. $sFileName;	
				$iconImageName = $dir['sdatingImage'] . 'icon_'. $sFileName;
				if (@unlink($bigImageName) && @unlink($thumbImageName) && @unlink($iconImageName))
					$sFileName = '';
			}
			elseif ($_POST['imageAction'] == 'update')
				$uploaded = true;
		}
		else
			$uploaded = true;
		
		if ($_FILES['event_photo']['tmp_name'])
			$aScan = getimagesize($_FILES['event_photo']['tmp_name']);
		else
			$aScan = array();

		//$sFileNameExt = '';
		//if ( ( 1 == $aScan[2] || 2 == $aScan[2] || 3 == $aScan[2] || 6 == $aScan[2] )
		
		if ($uploaded && in_array($aScan[2], array(1,2,3,6)) && 0 < strlen( $_FILES['event_photo']['name'])
		/* && move_uploaded_file( $_FILES['event_photo']['tmp_name'], $dir['tmp'] . $sPictureName ) */ ) {
		
			$sCurrentTime = time();
			if ($iEventID == -1)
				$sBaseName = 'g_'. $sCurrentTime .'_1';
			else {
				if ($sFileName!="") {
					if (ereg ("([a-z0-9_]+)\.", $sFileName, $aRegs))
						$sBaseName = $aRegs[1];
				} 
				else
					$sBaseName = ($sFileName!="") ? $sFileName : 'g_'. $sCurrentTime .'_1';
			}
			$sExt = moveUploadedImage( $_FILES, 'event_photo', $dir['tmp'] . $sBaseName, '', false );
			$sBaseName .= $sExt;
			$sPictureName = $sBaseName;
			$sThumbName   = 'thumb_'.$sBaseName;
			$sIconName	  = 'icon_'.$sBaseName;

			// resize for thumbnail
			$vRes = imageResize( $dir['tmp'] . $sBaseName, $dir['sdatingImage'] . $sThumbName, $this->iThumbSize, $this->iThumbSize );
			if ( $vRes != IMAGE_ERROR_SUCCESS )
				return SDATING_ERROR_PHOTO_PROCESS;
			$vRes = imageResize( $dir['tmp'] . $sBaseName, $dir['sdatingImage'] . $sPictureName, $this->iImgSize, $this->iImgSize );
			if ( $vRes != IMAGE_ERROR_SUCCESS )
				return SDATING_ERROR_PHOTO_PROCESS;
			$vRes = imageResize( $dir['tmp'] . $sBaseName, $dir['sdatingImage'] . $sIconName, $this->iIconSize, $this->iIconSize );
			if ( $vRes != IMAGE_ERROR_SUCCESS )
				return SDATING_ERROR_PHOTO_PROCESS;
				
			// Copy event photo, thumbnail and icon to the profile folder
			$sBaseNameMedia = $sCurrentTime . $sExt;
			$sPictureNameMedia = 'photo_'.$sBaseNameMedia;
			$sThumbNameMedia = 'thumb_'.$sBaseNameMedia;
			$sIconNameMedia	= 'icon_'.$sBaseNameMedia;

			copy($dir['sdatingImage'] . $sPictureName, "{$dir['profileImage']}{$aMember['ID']}/$sPictureNameMedia");			
			copy($dir['sdatingImage'] . $sThumbName, "{$dir['profileImage']}{$aMember['ID']}/$sThumbNameMedia");
			copy($dir['sdatingImage'] . $sIconName, "{$dir['profileImage']}{$aMember['ID']}/$sIconNameMedia");

			unlink( $dir['tmp'] . $sBaseName );
			chmod( $dir['sdatingImage'] . $sPictureName, 0644 );
			chmod( $dir['sdatingImage'] . $sThumbName, 0644 );
			chmod( $dir['sdatingImage'] . $sIconName, 0644 );
			$sEventPhotoFilename = process_db_input( $sPictureName );
		}
		else 
			$sEventPhotoFilename = ($uploaded) ? '' : $sFileName;

		$sPictureSQL = '';
		if ($iEventID>0)
			$sPictureSQL = "`PhotoFilename` = '{$sEventPhotoFilename}',";

		// event date
		$sEventStart = strtotime( $_POST['event_start'] );
		$sEventEnd = strtotime( $_POST['event_end'] );
		$sEventEndVal = "FROM_UNIXTIME($sEventEnd)";
		if ( $sEventStart == -1 )
			return SDATING_ERROR_WRONG_DATE_FORMAT;

		$siteLevelSQL = '';
		$siteEventsCreator = db_value("SELECT `SiteEvents` FROM `Profiles` WHERE `ID`={$aMember['ID']} LIMIT 1");
		if ($this->bAdminMode || $siteEventsCreator)
		{
			$siteLevel = (isset($_POST['siteLevel'])) ? '1' : '0';
			$siteLevelSQL = ", `EventLevel`='$siteLevel'";
		}
			
		if ($this->bAdminMode) {
			$sEventEnd = strtotime( $_POST['event_end'] );
			//if ( $sEventEnd == -1 )
			//	return SDATING_ERROR_WRONG_DATE_FORMAT;
			$sEventSaleStart = strtotime( $_POST['event_sale_start'] );
			//if ( $sEventSaleStart == -1 )
			//	return SDATING_ERROR_WRONG_DATE_FORMAT;
			$sEventSaleEnd = strtotime( $_POST['event_sale_end'] );
			//if ( $sEventSaleEnd == -1 )
			//	return SDATING_ERROR_WRONG_DATE_FORMAT;
			//if ( $sEventEnd < $sEventStart || $sEventSaleEnd < $sEventSaleStart || $sEventStart < $sEventSaleStart )
			//	return SDATING_ERROR_WRONG_DATE_FORMAT;
			$sEventSaleStartVal = "FROM_UNIXTIME( {$sEventSaleStart} )";
			$sEventSaleEndVal = "FROM_UNIXTIME( {$sEventSaleEnd} )";
			$sFeatured = (isset($_POST['featured'])) ? '1' : '0';
			$sFeaturedSQL = ", `Featured` = '$sFeatured'";
			$exclusive = (isset($_POST['exclusive'])) ? 1 : 0;
			$sExclusiveSQL = ", `exclusive` = $exclusive";
		} else {
			$sEventSaleStartVal = 'NOW()';
			$sEventSaleEndVal = 'NOW()';
			$sFeaturedSQL = '';
			$sExclusiveSQL = '';
		}
		
		// event responsible
		$sEventRespId    = ($this->bAdminMode && $iEventID == -1) ? 0 : process_db_input( $aMemberData['ID'], 0, 1 );
		$sEventRespName  = ($this->bAdminMode && $iEventID == -1) ? _t('Admin') : process_db_input( $aMemberData['RealName'], 0, 1 );
		$sEventRespEmail = ($this->bAdminMode && $iEventID == -1) ? getParam('main_email') : process_db_input( $aMemberData['Email'], 0, 1 );
		$sEventRespPhone = ($this->bAdminMode && $iEventID == -1) ? '666' : process_db_input( $aMemberData['Phone'], 0, 1 );
		
		$iEventAgeLowerFilter = (int)getParam( 'search_start_age' );
		$iEventAgeUpperFilter = (int)getParam( 'search_end_age' );

		$sEventMembershipFilter = "\'all\'";

		$iEventCountF = (int)$_POST['event_count_female'];
		$iEventCountM = (int)$_POST['event_count_male'];
		
		$readPerm = $_POST['readPerm'];
		$joinPerm = $_POST['joinPerm'];

		$dEventPriceF = '0.00';
		$dEventPriceM = '0.00';
		$dEventPriceC = '0.00';

		// choose options
		$iEventChoosePeriod = 5;

		// allow to view participants
		$iEventAllowView = '1';

		$urltitle = uniqueSEFUrl(getSEFUrl($sEventTitle, sefEventWords), $sEventRespId, 'ResponsibleID', 'SDatingEvents');
		
		if ($iEventID==-1) {
			$vRes = db_res( "INSERT INTO `SDatingEvents` SET
						`Title` = '{$sEventTitle}',
						`Description` = '{$sEventDesc}',
						`Status` = 'Active',
						`StatusMessage` = '{$sEventStatusMessage}',
						`Country` = '{$sEventCountry}',
						`City` = '{$sEventCity}',
						`Place` = '{$EventPlace}',
						`PhotoFilename` = '{$sEventPhotoFilename}',
						`EventStart` = FROM_UNIXTIME( {$sEventStart} ),
						`EventEnd` = {$sEventEndVal},
						/*`TicketSaleStart` = {$sEventSaleStartVal},
						`TicketSaleStart` = FROM_UNIXTIME( {$sEventSaleStart} ),
						`TicketSaleEnd` = {$sEventSaleEndVal},
						`TicketSaleEnd` = FROM_UNIXTIME( {$sEventSaleEnd} ),*/
						`ResponsibleID` = '{$sEventRespId}',
						`ResponsibleName` = '{$sEventRespName}',
						`ResponsibleEmail` = '{$sEventRespEmail}',
						`ResponsiblePhone` = '{$sEventRespPhone}',
						`EventSexFilter` = 'female,male',
						`EventAgeLowerFilter` = {$iEventAgeLowerFilter},
						`EventAgeUpperFilter` = {$iEventAgeUpperFilter},
						`EventMembershipFilter` = '{$sEventMembershipFilter}',
						`TicketCountFemale` = {$iEventCountF},
						`TicketCountMale` = {$iEventCountM},
						`TicketPriceFemale` = {$dEventPriceF},
						`TicketPriceMale` = {$dEventPriceM},
						`ChoosePeriod` = {$iEventChoosePeriod},
						`AllowViewParticipants` = {$iEventAllowView},
						`Tags` = '{$sTags}'
						$sFeaturedSQL
						$sExclusiveSQL
						$siteLevelSQL,
						`ReadPermission` = '$readPerm',
						`JoinPermission` = '$joinPerm',
						`urltitle` = '$urltitle'
						" );
			$iLastID = mysql_insert_id();
			$_REQUEST['event_id'] = $iLastID;
			
			// Insert event owner in participants table
			$iParticipantUID = $aMemberData['NickName'] . $iLastID . rand(100, 999);
			$vRes = db_res( "INSERT INTO `SDatingParticipants` SET `IDEvent` = $iLastID, 
				`IDMember` = $sEventRespId, `ParticipantUID` = '{$iParticipantUID}'", 0 );
			
			if ($sEventPhotoFilename != '')
			{
				// Insert record to the `media` table
				$vRes = db_res( "INSERT INTO `media` SET
							`med_prof_id` = $iLastID,
							`med_type` = 'eventphoto',
							`med_file` = '$sBaseNameMedia',
							`med_title` = '$sEventTitle',
							`med_status` = 'active',
							`med_date` = NOW()");
				$iLastMediaID = mysql_insert_id();
				
				// Set inserted media id as `PrimPhoto` field in events table
				$vRes = db_res("UPDATE `SDatingEvents` SET 
							`PrimPhoto` = $iLastMediaID, 
							`Picture` = '1'
							WHERE `ID`=$iLastID");
			}
			
			// Set RSS image for gloabl event if uploaded
			if ($siteLevel == '1' && $_FILES['rssimage']['type'] == 'image/jpeg')
			{
				$sExt = moveUploadedImage($_FILES, 'rssimage', "{$dir['sdatingImage']}rss/$iLastID", '', true);
				if ($sExt == '.jpg')
					db_res("UPDATE SDatingEvents SET RSSImage = 1 WHERE ID = $iLastID");
			}
			
			if ($iLastID>0) {
				$this->iLastInsertedID = $iLastID;
				reparseObjTags( 'event', $iLastID );
				TrackNewAction(9, $iLastID, $sEventRespId, $sEventTitle);
			}
		} else {
			$vRes = db_res( "UPDATE `SDatingEvents` SET
						`Title` = '{$sEventTitle}',
						`Description` = '{$sEventDesc}',
						`Status` = 'Active',
						`StatusMessage` = '{$sEventStatusMessage}',
						`Country` = '{$sEventCountry}',
						`City` = '{$sEventCity}',
						`Place` = '{$EventPlace}',
						{$sPictureSQL}
						`EventStart` = FROM_UNIXTIME( {$sEventStart} ),
						`EventEnd` = {$sEventEndVal},
						/*`TicketSaleStart` = {$sEventSaleStartVal},
						`TicketSaleStart` = FROM_UNIXTIME( {$sEventSaleStart} ),
						`TicketSaleEnd` = {$sEventSaleEndVal},
						`TicketSaleEnd` = FROM_UNIXTIME( {$sEventSaleEnd} ),*/
						`ResponsibleID` = '{$sEventRespId}',
						`ResponsibleName` = '{$sEventRespName}',
						`ResponsibleEmail` = '{$sEventRespEmail}',
						`ResponsiblePhone` = '{$sEventRespPhone}',
						`EventSexFilter` = 'female,male',
						`EventAgeLowerFilter` = {$iEventAgeLowerFilter},
						`EventAgeUpperFilter` = {$iEventAgeUpperFilter},
						`EventMembershipFilter` = '{$sEventMembershipFilter}',
						`TicketCountFemale` = {$iEventCountF},
						`TicketCountMale` = {$iEventCountM},
						`TicketPriceFemale` = {$dEventPriceF},
						`TicketPriceMale` = {$dEventPriceM},
						`ChoosePeriod` = {$iEventChoosePeriod},
						`AllowViewParticipants` = {$iEventAllowView},
						`Tags` = '{$sTags}'
						$sFeaturedSQL
						$sExclusiveSQL
						$siteLevelSQL,
						`ReadPermission` = '$readPerm',
						`JoinPermission` = '$joinPerm'
						WHERE `ID` = {$iEventID}
						");
			if ($sEventPhotoFilename != '')
			{
				// Insert record to the `media` table
				$vRes = db_res( "INSERT INTO `media` SET
							`med_prof_id` = $iEventID,
							`med_type` = 'eventphoto',
							`med_file` = '$sBaseNameMedia',
							`med_title` = '$sEventTitle',
							`med_status` = 'active',
							`med_date` = NOW()");
				$iLastMediaID = mysql_insert_id();
				
				// Set inserted media id as `PrimPhoto` field in events table
				$vRes = db_res("UPDATE `SDatingEvents` SET 
							`PrimPhoto` = $iLastMediaID, 
							`Picture` = '1'
							WHERE `ID`=$iEventID");
			}
			
			// Set RSS image for gloabl event if uploaded
			if ($siteLevel == '1' && $_FILES['rssimage']['type'] == 'image/jpeg' &&
				!isset($_POST['rssImageAction']))
			{
				$sExt = moveUploadedImage($_FILES, 'rssimage', "{$dir['sdatingImage']}rss/$iEventID", '', true);
				if ($sExt == '.jpg')
					db_res("UPDATE SDatingEvents SET RSSImage = 1 WHERE ID = $iEventID");
			}			
			
			// Check RSS Image action
			if (isset($_POST['rssImageAction']))
			{
				if ($_POST['rssImageAction'] == 'clear')
				{
					unlink("{$dir['sdatingImage']}rss/$iEventID.jpg");
					db_res("UPDATE `SDatingEvents` SET `RSSImage` = 0 WHERE `ID`=$iEventID");
				}
				elseif ($_POST['rssImageAction'] == 'update' && $siteLevel == '1' &&
						$_FILES['rssimage']['type'] == 'image/jpeg')
					moveUploadedImage($_FILES, 'rssimage', "{$dir['sdatingImage']}rss/$iEventID", '', true);
			}
			
			reparseObjTags( 'event', $iEventID );
		}
		
		if ($this->bAdminMode) // only admin can change featured event
		{
			$memcache = $memcacher->obj();
			$memcache->set('eventsLastUpdatedTime', time());
			updateFeaturedEventsInMemcache();
			clearYHCache();
		}
		
		return SDATING_ERROR_SUCCESS;
	}

	/**

	 * page show event list function

	 * @return HTML presentation of data

	 */

	function PageSDatingShowEvents() {
		global $site;
		global $dir;
		global $prof;
		global $oTemplConfig;
		//global $date_format;
		global $tmpl;
		// if( !$logged['member'] && !$logged['admin'] ) {
		// member_auth(0);
		// }
		$sEditC = _t('_Edit');
		$sDeleteC = _t('_Delete');
		$sSureC = _t("_Are you sure");
		$sSureDeleteEvent = _t('_confirmDeleteEvent');
		$sTagsC = _t('_Tags');
		$showEventsInColumns = 1;

		// collect information about current member
		$aMember['ID'] = (int)$_COOKIE['memberID'];
		$aMemberData = getProfileInfo( $aMember['ID'] );
		$sMemberSex = $aMemberData['Sex'];
		$aMembership = getMemberMembershipInfo( $aMember['ID'] );
		$bShowFrom = (int)$_REQUEST['from'];
		
		//$sDateWhereCheck = ($this->bAdminMode) ? "AND NOW() < DATE_ADD(`SDatingEvents`.`EventEnd`, INTERVAL `SDatingEvents`.`ChoosePeriod` DAY)" : '';
		$sDateWhereCheck = ($_REQUEST['show_events'] == 'past') ? '' : " AND `Featured`='0'";
		if (isset($_REQUEST['ID']))
		{
			$eventOwnerID = $_REQUEST['ID'];
			$eventOwnerNick = getNickName($eventOwnerID);
		}
		elseif (isset($_REQUEST['sefNick']))
		{
			$eventOwnerID = getID($_REQUEST['sefNick']);
			$eventOwnerNick = $_REQUEST['sefNick'];
		}
		if ($eventOwnerID)
		{
			// Check "Read friends only" permission
			$bOwner = ($aMember['ID'] == $eventOwnerID) ? true : false;
			$bFriend = is_friends($aMember['ID'], $eventOwnerID);
			if (!$bOwner && !$bFriend && !$this->bAdminMode)
				$sDateWhereCheck .= " AND `ReadPermission`='public'"; // show public events only
		}
		else
			$sDateWhereCheck .= " AND `EventLevel`='1'";
		$sCommonSelectSQL = "`SDatingEvents`.`ID`, `Title`, `Description`, `StatusMessage`, `Country`, `City`, `Place`, `PhotoFilename`, `Tags`, ";
		$sCommonSelectSQL  .= "`EventStart`, `EventEnd`, `urltitle`, ";
		$sCommonSelectSQL  .= "(UNIX_TIMESTAMP() - UNIX_TIMESTAMP(`EventStart`)) AS `sec`, `ResponsibleID`, ";
		$sCommonSelectSQL  .= "(NOW() > `EventEnd` AND NOW() < DATE_ADD(`EventEnd`, INTERVAL `ChoosePeriod` DAY)) AS `ChooseActive`, ";
		$sCommonSelectSQL  .= "(NOW() > `EventStart`) AS `EventBegan`, ";
		$sCommonSelectSQL  .= "(NOW() < `EventEnd`) AS `EventNotFinished`, (UNIX_TIMESTAMP() - UNIX_TIMESTAMP(`EventEnd`)) AS `secAgo`, ";
		$sCommonSelectSQL  .= "`AllowViewParticipants`, (`SDatingParticipants`.`ID` IS NOT NULL) AS `IsParticipant`, ";
		$sCommonSelectSQL  .= "`media`.`med_file` ";
		
		$sLeftJoinAddonSQL = "LEFT JOIN `SDatingParticipants` ON `SDatingParticipants`.`IDEvent` = `SDatingEvents`.`ID` AND `SDatingParticipants`.`IDMember` = {$aMember['ID']} ";
		$sLeftJoinAddonSQL .= "LEFT JOIN `media` ON `media`.`med_id` = `SDatingEvents`.`PrimPhoto`";
		/*$sLimitSQL = "LIMIT {$bShowFrom}, {$this->iEventsPerPage}";*/

		$sStatusActiveSQL = "`SDatingEvents`.`Status` = 'Active'";

		$sOnlyMemberEventsCheck = ($eventOwnerID > 0) ? " AND `ResponsibleID` = $eventOwnerID" : '';
		if (isset($_REQUEST['show_events_keywords']) && trim($_REQUEST['show_events_keywords']) != '')
		{
			$keywords = process_db_input(trim($_REQUEST['show_events_keywords']));	
			$sWithKeywords = " AND (`SDatingEvents`.`Title` LIKE '%$keywords%' 
				OR `SDatingEvents`.`Description` LIKE '%$keywords%'
				OR `SDatingEvents`.`Tags` LIKE '%$keywords%'
				OR `SDatingEvents`.`Country` LIKE '%$keywords%'
				OR `SDatingEvents`.`City` LIKE '%$keywords%'
				OR `SDatingEvents`.`Place` LIKE '%$keywords%') ";
		}
		elseif (isset($_REQUEST['tagKey']) && trim($_REQUEST['tagKey']) != '')
			$sWithKeywords = " AND (`SDatingEvents`.`Tags` LIKE '%".process_db_input($_REQUEST['tagKey'])."%')";
		else
			$sWithKeywords = '';
		$sOrderBySQL = "ORDER BY `EventStart`";

		// build SQL query for event listing
		$sShowQuery = '';
		$upcomingFilter = ((!isset($_REQUEST['show_events_keywords']) || trim($_REQUEST['show_events_keywords']) == '') &&
			(!isset($_REQUEST['tagKey']) || trim($_REQUEST['tagKey']) == '')) ?
			' AND `EventEnd` > NOW() ' : '';
		switch ( $_REQUEST['show_events'] ) {
			case 'country':
				
				// queries for showing 'by country'
				$sShowQuery = "
					SELECT
						{$sCommonSelectSQL }
					FROM `SDatingEvents`
					{$sLeftJoinAddonSQL}
					WHERE
						{$sStatusActiveSQL}
						{$sDateWhereCheck}
						{$sOnlyMemberEventsCheck}
						{$sWithKeywords}
						AND `SDatingEvents`.`Country` = '". process_db_input($_REQUEST['show_events_country']) ."'
					{$sOrderBySQL} {$sLimitSQL}
				";
				break;
			case 'my':

				// queries for showing my events
				$sShowQuery = "
					SELECT
						{$sCommonSelectSQL }
					FROM `SDatingEvents`
					{$sLeftJoinAddonSQL}
					WHERE
						`ResponsibleID` = {$aMember['ID']}
						{$sWithKeywords}
					{$sOrderBySQL} {$sLimitSQL}
				";
				break;
			case 'past':
				$sShowQuery = "
					SELECT
						{$sCommonSelectSQL }
					FROM `SDatingEvents`
					{$sLeftJoinAddonSQL}
					WHERE
						{$sStatusActiveSQL}
						{$sDateWhereCheck}
						{$sOnlyMemberEventsCheck}
						{$sWithKeywords}
						AND `EventEnd` < NOW() 
					ORDER BY `SDatingEvents`.`EventEnd` DESC
						{$sLimitSQL}
				";
				break;
			case 'all':

				// queries for showing all available events
				$sShowQuery = "
					SELECT
						{$sCommonSelectSQL }
					FROM `SDatingEvents`
					{$sLeftJoinAddonSQL}
					WHERE
						{$sStatusActiveSQL}
						{$sDateWhereCheck}
						{$sOnlyMemberEventsCheck}
						{$sWithKeywords}
					{$sOrderBySQL} {$sLimitSQL}
				";
				break;
			case 'upcoming':
			default:
				$sShowQuery = "
					SELECT
						{$sCommonSelectSQL }
					FROM `SDatingEvents`
					{$sLeftJoinAddonSQL}
					WHERE
						{$sStatusActiveSQL}
						{$sDateWhereCheck}
						{$sOnlyMemberEventsCheck}
						{$sWithKeywords}
						{$upcomingFilter}
					{$sOrderBySQL} {$sLimitSQL}
				";
		}

		////////////////////////////
		
		$iTotalNum = mysql_num_rows(db_res($sShowQuery));

		$iPerPage = (int)$_GET['per_page'];

		if( !$iPerPage )
			$iPerPage = $this->iEventsPerPage;

		$iTotalPages = ceil( $iTotalNum / $iPerPage );

		$iCurPage = (int)$_GET['page'];

		if( $iCurPage > $iTotalPages )
			$iCurPage = $iTotalPages;

		if( $iCurPage < 1 )
			$iCurPage = 1;

		$sLimitFrom = ( $iCurPage - 1 ) * $iPerPage;

		$sqlLimit = "LIMIT $sLimitFrom, $iPerPage";

		$sShowQuery .= $sqlLimit;



		// process database queries

		$vShowRes = db_res( $sShowQuery );

		////////////////////////////

		$iShowNum = mysql_num_rows( $vShowRes );

		// $aTotalNum = db_arr( $sCountSQL );

		// $iTotalNum = (int)$aTotalNum[0];



		// $sRetHtml = <<<EOF

		// <script language="JavaScript" type="text/javascript">

		// function navigationSubmit( fromParam )

		// {

		// document.forms['showEventsForm'].elements['from'].value = fromParam;

		// document.forms['showEventsForm'].submit();

		// }

		// </script>

		// EOF;



		if ( $iShowNum == 0 ) {
			$sRetHtml .= '<center>'. _t('_No events available') . '</center>';
		}
		else {

			// print navigation pages and store content for further use

			/*if ( $iTotalNum > $this->iEventsPerPage ) {

			$sNavContent = _t('_Pages'). ':&nbsp;';

			// if it's not first page then show 'Prev' link

			if ( $bShowFrom >= $this->iEventsPerPage ) {

			$iPrevFrom = (0 < ($bShowFrom - $this->iEventsPerPage)) ? ($bShowFrom - $this->iEventsPerPage) : 0;

			$sNavContent .= "<a href=\"javascript:void(null);\" onClick=\"javascript: navigationSubmit({$iPrevFrom}); return false;\">". _t('_Prev') ."</a>&nbsp;";

			}

			// show list of pages

			$iCurrentEvent = 0;

			$iCurrentPage = 1;

			while ( $iCurrentEvent < $iTotalNum ) {

			if ( $iCurrentEvent == $bShowFrom )

			$sNavContent .= "[{$iCurrentPage}]&nbsp;";

			else

			$sNavContent .= "<a href=\"javascript:void(null);\" onClick=\"javascript: navigationSubmit({$iCurrentEvent}); return false;\">{$iCurrentPage}</a>&nbsp;";

			$iCurrentEvent += $this->iEventsPerPage;

			$iCurrentPage++;

			}

			// if it's not last page then show 'Next' link

			if ( $iTotalNum > $bShowFrom + $this->iEventsPerPage ) {

			$iNextFrom = $bShowFrom + $this->iEventsPerPage;

			$sNavContent .= "<a href=\"javascript:void(null);\" onClick=\"javascript: navigationSubmit({$iNextFrom}); return false;\">". _t('_Next') ."</a>";

			}

			$sRetHtml .= DesignBoxContent( _t('_Pages'), '<center>'. $sNavContent .'</center>', $oTemplConfig -> PageSDatingShowEvents_db_num );

			$sRetHtml .= $this->sRowSpacerDiv;

			}*/



			$sSpacerName = $site['url'].$this -> sSpacerPath;

			$counter = 0;
			$split = false;

			// Prepare events array to sorting
			$eventsArr = fill_assoc_array($vShowRes);
			usort($eventsArr, 'EventsCompare');

			$eventHrefTmplOrig = "{$site['url']}events.php?action=show_info&event_id=%eventID%";
			$eventHrefTmpl = (useSEF) ? "{$site['url']}%ownerNick%/event/%urltitle%.html" : $eventHrefTmplOrig;
			
			$tagHrefTmpl = (useSEF) ? "{$site['url']}events/tagKey=%tag%" 
				: "{$site['url']}events.php?action=search_by_tag&amp;tagKey=%tag%";
			
			// Print list of events
			//while ( $aResSQL = mysql_fetch_assoc($vShowRes) ) {
			while (list($ind, $event) = each($eventsArr)) {
				
				if ($event['med_file'] != '')
				{
					$dirEventImage = "{$dir['profileImage']}{$event['ResponsibleID']}/thumb_{$event['med_file']}";
					$urlEventImage = "{$site['profileImage']}{$event['ResponsibleID']}/thumb_{$event['med_file']}";
					$eventImageExists = true;
				}
				elseif ($event['PhotoFilename'] != '')
				{
					$dirEventImage = "{$dir['sdatingImage']}thumb_{$event['PhotoFilename']}";
					$urlEventImage = "{$site['sdatingImage']}thumb_{$event['PhotoFilename']}";
					$eventImageExists = true;
				}	
				else 
					$eventImageExists = false;

				if ($event['urltitle'] == '')
					$sHref = str_replace('%eventID%', $event['ID'], $eventHrefTmplOrig);
				else
				{
					$ownerNick = getNickName($event['ResponsibleID']);
					$sHref = str_replace(array('%ownerNick%', '%eventID%', '%urltitle%'), 
						array($ownerNick, $event['ID'], $event['urltitle']), $eventHrefTmpl);
				}
					
				$sShowInfoC = _t('_Show info');

				$sParticipantsC = _t('_Participants');

				//$sParticipantsYLC = _t('_Participants you liked');

				$sStatusMessageC = _t('_Status message');

				$sStartDateC = _t('_StartDate');

				$sPlaceC = _t('_Place');

				$sDescriptionC = _t('_Description');

				$sTitleC = _t('_Title');

				$sActionsC = _t('_Actions');

				$sListOfParticipantsC = _t('_List').' '._t('_of').' '._t('_Participants');

				$sPostedByC = _t('_Posted by');

				$aPostedBy = $this->GetProfileData($event['ResponsibleID']);

				$sPostedBy = $aPostedBy['NickName'];


/*
				$sViewParticipantsVal = <<<EOF

<a href="{$_SERVER['PHP_SELF']}?action=show_part&amp;event_id={$event['ID']}">

	{$sListOfParticipantsC}

</a>

EOF;

				$sViewParticipants = ($event['AllowViewParticipants'] == '1') ? $sViewParticipantsVal : '';
*/


				//$sMatchesPart = "&nbsp;|&nbsp;<a href=\"{$_SERVER['PHP_SELF']}?action=select_match&amp;event_id={$event['ID']}\">". $sParticipantsYLC ."</a>";

				//$sIsParticipants = ($event['ChooseActive'] && $event['IsParticipant']) ? $sMatchesPart : '';

				//if ($this->bAdminMode==FALSE) $sIsParticipants = $sMatchesPart;

				$sStatusMessage = process_line_output($event['StatusMessage']);

				$date_format_php = getParam('php_date_format');

				$sDateTime = date( $date_format_php, strtotime( $event['EventStart'] ) );

				//$secAgo = ($event['EventEnd'] < $event['EventStart']) ? $event['sec'] : $event['secAgo'];
				$eventStart = ($event['EventNotFinished'] || !$event['EventBegan']) 
					? '('._format_when($event['sec']).')'
					: '(<span style="color:red">Ended: '._format_when($event['secAgo']).'</span>)';
				
				if (!$event['EventBegan'])
					$eventStart = '('._format_when($event['sec']).')';
				elseif (!$event['EventNotFinished'])
					$eventStart = '(<span style="color:red">'._format_when($event['secAgo']).'</span>)';
				else 
				{
					$eventEnd = date( $date_format_php, strtotime($event['EventEnd'] ) );			
					$eventStart = " to $eventEnd";
				}
					
				$sEventsStart = "$sDateTime $eventStart";

				//$sCountry = _t('__'. $prof['countries'][$event['Country']]);

				$sCountry = ($event['Country']!='') ? _t('__'. $prof['countries'][$event['Country']]) : '';

				//$sCity = process_line_output($event['City']);

				$sCity = ($event['City']!='') ? ', '.process_line_output($event['City']) : '';

				//$sPlace = process_line_output($event['Place']);

				$sPlace = ($event['Place']!='') ? ', '.process_line_output($event['Place']) : '';

				$sDescription = $event['Description'];



				$sTagsCommas = $event['Tags'];

				$aTags = split(',', $sTagsCommas);

				$sTagsHrefs = '';

				
				foreach( $aTags as $sTagKey ) {

					$tagHref = str_replace('%tag%', $sTagKey, $tagHrefTmpl);
					$sTagsHrefs .= <<<EOF

<a href="$tagHref">{$sTagKey}</a>&nbsp;

EOF;

}
/*
$sTags = <<<EOF

<div class="cls_res_info_p">

	<!--<span style="vertical-align:middle;">

		<img src="{$site['icons']}tag_small.png" class="marg_icon" alt="" />

	</span>-->{$sTagsC}:&nbsp;{$sTagsHrefs}

</div>

EOF;
*/


// print "<pre>";

// print_r($event);

// print "</pre><br />";

//$sActions = $this->GetEventActions($event['ID'], $event['ResponsibleID']);

$sImgEl = $this->GetEventPicture($event['ID'], $event['PhotoFilename'], 'thumb', true);

if ($showEventsInColumns)
{
	if (isset($_REQUEST['tagKey']) && $_REQUEST['tagKey'] != '')
	{
		$aTags = split(',', $event['Tags']);
		if (!in_array($_REQUEST['tagKey'],$aTags))
			continue;
	}

	if (($counter > ceil($iShowNum / $showEventsInColumns) - 1) && !$split)
	{
		$sMainContent .= '</td><td valign="top" width="50%">';
		$split = true;
	}
	//$sDescription = RemoveHTMLEntities(process_line_output(html2txt(BreakLinesToSpaces(trim($sDescription)))));
	
	$sMainContent .= <<<EOF
		<table>
			<tr>
			<td valign="top">$sImgEl</td>
			<td valign="top" style="color:#666666">
				<div class="oneline"><a href="$sHref" style="margin-left:10px; font-weight:bold; text-decoration:none">
					{$event['Title']}</a></div>
				<!--<div class="oneline">$sTags</div>-->
				<div><div class="cls_res_info_p">
					<div class="clr3">$sEventsStart</div></div></div>
				<!--
				<div class="oneline"><div class="cls_res_info_p">
					$sPostedByC: <a href="{$site['url']}$sPostedBy"><div class="clr3">{$sPostedBy}</div></a></div></div>
				-->
				<div class="oneline"><div class="cls_res_info_p">
					<div class="clr3">{$sCountry}{$sCity}{$sPlace}</div></div></div>
				<!--<div class="cls_res_info_p">$sViewParticipants</div>-->
			</td>
			</tr>
		</table>
		<br/>
EOF;
	$counter++;
}
else 
{
$sMainContent .= <<<EOF
<div class="cls_result_row">
	<div class="clear_both"></div>
	<!-- <div  class="thumbnail_block" style="float:left;">
		<a href="{$_SERVER['PHP_SELF']}?action=show_info&amp;event_id={$event['ID']}">
			{$sImgEL}
		</a>
	</div> -->
	{$sImgEl}
	<div class="cls_res_info_nowidth" {$sDataStyleWidth}>
		<div class="cls_res_info_p">
			<a class="actions" href="{$_SERVER['PHP_SELF']}?action=show_info&amp;event_id={$event['ID']}">{$event['Title']}</a>
		</div>
		{$sTags}
		<!-- <div class="cls_res_info_p">
			{$sStatusMessageC}: <div class="clr3">{$sStatusMessage}</div>
		</div> -->
		<div class="cls_res_info_p">
			{$sStartDateC}: <div class="clr3">{$sEventsStart}</div>
		</div>
		<div class="cls_res_info_p">
			{$sPostedByC}: <a href="{$site['url']}{$sPostedBy}"><div class="clr3">{$sPostedBy}</div></a>
		</div>
		<div class="cls_res_info_p">
			{$sPlaceC}: <div class="clr3">{$sCountry}{$sCity}{$sPlace}</div>
		</div>
		<div class="cls_res_info_p">
			{$sDescriptionC}: <div class="clr3">{$sDescription}</div>
		</div>
		<div class="cls_res_info_p">
			{$sViewParticipants}
		</div>
		{$sActions}
	</div>
	<div class="clear_both"></div>
</div>
EOF;
}

//$sRetHtml .= $this->sRowSpacerDiv;

			}



			///////////////////////////

			if ($iTotalNum > 10)
			{
				$action = (isset($_GET['action'])) ? $_GET['action'] : 'show';
				$sRequest = "{$_SERVER['PHP_SELF']}?action=$action";
	
				if (isset($_GET['show_events']))
					$sRequest .= "&amp;show_events={$_GET['show_events']}";
					
				if (isset($_GET['ID']))
					$sRequest .= "&amp;ID={$_GET['ID']}";
	
				if (isset($_GET['show_events_keywords']))
					$sRequest .= "&amp;show_events_keywords=".
						htmlentities( process_pass_data($_GET['show_events_keywords']));
				
				$pagination = '<div style="text-align: center; position: relative; margin-top:15px; height:23px">'._t("_Results per page").':
						<select name="per_page" onchange="window.location=\'' . $sRequest . '&amp;per_page=\' + this.value;">
							<option value="10"' . ( $iPerPage == 10 ? ' selected="selected"' : '' ) . '>10</option>
							<option value="20"' . ( $iPerPage == 20 ? ' selected="selected"' : '' ) . '>20</option>
							<option value="50"' . ( $iPerPage == 50 ? ' selected="selected"' : '' ) . '>50</option>
							<option value="100"' . ( $iPerPage == 100 ? ' selected="selected"' : '' ) . '>100</option>
						</select></div>';
			
				if( $iTotalPages > 1)
					$pagination .= genPagination($iTotalPages, $iCurPage, 
						($sRequest . '&amp;page={page}&amp;per_page='.$iPerPage));
			}
				
			///////////////////////////



			// $sNavContent already built

			/*if ( $iTotalNum > $this->iEventsPerPage ) {

			$sRetHtml .= DesignBoxContent( _t('_Pages'), '<center>'. $sNavContent .'</center>', $oTemplConfig -> PageSDatingShowEvents_db_num );

			$sRetHtml .= $this->sRowSpacerDiv;

			}*/

		}

	$sRetHtml .= $sMainContent;		

	if ($showEventsInColumns)
	{
		$paramID = ($eventOwnerID) ? "&ID=$eventOwnerID" : '';
		if (useSEF)
		{
			if ($paramID)
			{
				$upcomingEventsLink = "$eventOwnerNick/events/upcoming";
				$pastEventsLink = "$eventOwnerNick/events/past";
			}
			else
			{
				$upcomingEventsLink = "events/upcoming";
				$pastEventsLink = "events/past";
			}
		}
		else
		{
			$upcomingEventsLink = "events.php?show_events=upcoming&action=show{$paramID}";
			$pastEventsLink = "events.php?show_events=past&action=show{$paramID}";
		}
		if ($_REQUEST['show_events'] == 'past')
		{
			if ($eventOwnerID)
			{
				if ($eventOwnerID == $aMember['ID'])
					$eventsHeader = 'My Past Events';
				else
					$eventsHeader = "$eventOwnerNick's Events";
			}
			else 
				$eventsHeader = 'Past Events';
			$viewEventsCaption = 'Upcoming Events';
			$viewEventsURL = $upcomingEventsLink;
		}
		elseif ((isset($_REQUEST['show_events_keywords']) && trim($_REQUEST['show_events_keywords']) != '') ||
			(isset($_REQUEST['tagKey']) && trim($_REQUEST['tagKey']) != ''))
		{
			$eventsHeader = "Search Results ($iTotalNum)";
			$viewEventsCaption = 'Upcoming Events';
			$viewEventsURL = $upcomingEventsLink;
		}
		else 
		{
			if (isset($eventOwnerID))
			{
				if ($eventOwnerID == $aMember['ID'])
					$eventsHeader = 'My Upcoming Events';
				else
					$eventsHeader = "$eventOwnerNick's Events";
			}
			else 
				$eventsHeader = 'Upcoming Events';
			$viewEventsCaption = 'Past Events';
			$viewEventsURL = $pastEventsLink;
		}

		$pastEventsLink = "<div class=\"caption_item\"><a href=\"$viewEventsURL\">$viewEventsCaption</a></div>";
		$sRetHtml = "<table width='100%'><tr><td valign=\"top\" width=\"50%\">$sRetHtml</td></tr></table>$pagination";
		return DesignBoxContent($eventsHeader, $sRetHtml, 1, $pastEventsLink);
	}
	else
		return $this->DecorateAsTable(_t('_All Events'), $sRetHtml.$pagination);
	}


function GetEventActions($eventID, $ownerID)
{
	$sActions = '';
	$sEditC = _t('_Edit');
	$sDeleteC = _t('_Delete');
	$sSureDeleteEvent = _t('_confirmDeleteEvent');

	if ($ownerID==(int)$_COOKIE['memberID'] || $this->bAdminMode) {
		$sActions = <<<EOF
		<div class="cls_res_info_p">
			<a href="{$_SERVER['PHP_SELF']}" onclick="UpdateField('EditEventID','$eventID');
				document.forms.command_edit_event.submit();return false;" style="text-transform:none;">{$sEditC}</a>&nbsp;
			<a href="{$_SERVER['PHP_SELF']}" onclick="if (confirm('{$sSureDeleteEvent}')) {UpdateField('DeleteEventID','$eventID');
				document.forms.command_delete_event.submit(); } return false;" style="text-transform:none;">{$sDeleteC}</a>
		</div>
EOF;
	}	

	return $sActions;
}
	
	
	function PageSDatingFeaturedEvents()
	{
		global $dir;
		global $site;		
		global $tmpl;
		global $prof;
		
		$sStartDateC = _t('_StartDate');
		$sPostedByC = _t('_Posted by');
		$sPlaceC = _t('_Place');
		$sListOfParticipantsC = _t('_List').' '._t('_of').' '._t('_Participants');
		
		$sSpacerName = $site['url'].$this -> sSpacerPath;
	
		$maxFeaturedEvents = getParam('num_featured_events');
		
		$vShowRes = db_res("SELECT `SDatingEvents`.*,
			(UNIX_TIMESTAMP() - UNIX_TIMESTAMP(`EventStart`)) AS `sec`,
			(NOW() > `EventStart`) AS `EventBegan`,
			(NOW() < `EventEnd`) AS `EventNotFinished`, 
			(UNIX_TIMESTAMP() - UNIX_TIMESTAMP(`EventEnd`)) AS `secAgo`,
			`media`.`med_file`
		FROM `SDatingEvents` 
		LEFT JOIN `media` ON `media`.`med_id` = `SDatingEvents`.`PrimPhoto`
		WHERE `Featured` = '1' AND `EventLevel`='1' AND (NOW() < `EventEnd`)
		ORDER BY `EventStart`
		LIMIT $maxFeaturedEvents
		");

		while ( $aResSQL = mysql_fetch_assoc($vShowRes) ) {		

			if ($aResSQL['med_file'] != '')
			{
				$dirEventImage = "{$dir['profileImage']}{$aResSQL['ResponsibleID']}/thumb_{$aResSQL['med_file']}";
				$urlEventImage = "{$site['profileImage']}{$aResSQL['ResponsibleID']}/thumb_{$aResSQL['med_file']}";
				$eventImageExists = true;
			}
			elseif ($aResSQL['PhotoFilename'] != '')
			{
				$dirEventImage = "{$dir['sdatingImage']}thumb_{$aResSQL['PhotoFilename']}";
				$urlEventImage = "{$site['sdatingImage']}thumb_{$aResSQL['PhotoFilename']}";
				$eventImageExists = true;
			}	
			else 
				$eventImageExists = false;

			$sImgEL = ($eventImageExists && file_exists($dirEventImage))
				? "<img class=\"photo1\" style=\"width:{$this->iThumbSize}px;height:{$this->iThumbSize}px;
					background-image:url($urlEventImage); background-repeat: no-repeat; background-position:center top;\"  src=\"{$sSpacerName}\" alt=\"{$aResSQL['Title']}\" />"
				: "<img class=\"photo1\" style=\"width:{$this->iThumbSize}px;height:{$this->iThumbSize}px;
					background-image:url({$site['url']}templates/tmpl_{$tmpl}/{$this->sPicNotAvail});\" src=\"{$sSpacerName}\" />";
			
			$eventLinkTmpl = ($aResSQL['urltitle'] && useSEF) ?	
				"<a href=\"{$site['url']}".getNickName($aResSQL['ResponsibleID']).
					"/event/{$aResSQL['urltitle']}.html\" %1%>%0%</a>"
				: "<a href=\"events.php?action=show_info&amp;event_id={$aResSQL['ID']}\" %1%>%0%</a>";
			$eventImgLink = str_replace(array('%0%', '%1%'), array($sImgEL, ''), $eventLinkTmpl);
			$eventTitleLink = str_replace(array('%0%', '%1%'), array($aResSQL['Title'],
				'style="margin-left:10px; font-weight:bold; text-decoration:none"'), $eventLinkTmpl);
				
			$date_format_php = getParam('php_date_format');
			$sDateTime = date( $date_format_php, strtotime( $aResSQL['EventStart'] ) );
			//$secAgo = ($aResSQL['EventEnd'] < $aResSQL['EventStart']) ? $aResSQL['sec'] : $aResSQL['secAgo'];
			$eventStart = ($aResSQL['EventNotFinished'] || !$aResSQL['EventBegan']) 
				? '<div>('._format_when($aResSQL['sec']).')</div>'
				: '<div>(<span style="color:red">Ended: '._format_when($aResSQL['secAgo']).'</span>)</div>';
			
			$sEventsStart = "$sDateTime $eventStart";
			
			$aPostedBy = $this->GetProfileData($aResSQL['ResponsibleID']);
			$sPostedBy = $aPostedBy['NickName'];
			
			$sCountry = ($aResSQL['Country']!='') ? _t('__'. $prof['countries'][$aResSQL['Country']]) : '';
			$sCity = ($aResSQL['City']!='') ? ', '.process_line_output($aResSQL['City']) : '';
			$sPlace = ($aResSQL['Place']!='') ? ', '.process_line_output($aResSQL['Place']) : '';
			
			$sViewParticipantsVal = <<<EOF
<a href="{$_SERVER['PHP_SELF']}?action=show_part&amp;event_id={$aResSQL['ID']}">
	{$sListOfParticipantsC}
</a>
EOF;
			$sViewParticipants = ($aResSQL['AllowViewParticipants'] == '1') ? $sViewParticipantsVal : '';
			
			$sActions = $this->GetEventActions($aResSQL['ID'], $aResSQL['ResponsibleID']);
			
			$sMainContent .= <<<EOF
			<table>
				<tr>
				<td valign="top">$eventImgLink</td>
				<td valign="top" style="color:#666666">
					<div class="oneline">$eventTitleLink</div>
					<div><div class="cls_res_info_p">
						$sStartDateC: <div class="clr3">$sEventsStart</div></div></div>
					<!--	
					<div class="oneline"><div class="cls_res_info_p">
						$sPostedByC: <a href="{$site['url']}$sPostedBy"><div class="clr3">{$sPostedBy}</div></a></div></div>
					-->
					<div class="oneline"><div class="cls_res_info_p">
						$sPlaceC: <div class="clr3">{$sCountry}{$sCity}{$sPlace}</div></div></div>
					<div class="cls_res_info_p">$sViewParticipants</div>
					$sActions
				</td>
				</tr>
			</table>
EOF;

		}
	
		$sRetHtml = '<table width="100%"><tr><td valign="top" width="50%">'.
			$sMainContent . '</td></tr></table>';
		
		return DesignBoxContent(_t('_featured events'), $sRetHtml, 1);
	}
	
	function PageSDatingPopularTags()
	{
		global $oTemplConfig;
		global $site;

		$hrefTmpl  = (useSEF) ? 'events/keyword={tag}'
			: 'events.php?action=show&amp;show_events_keywords={tag}';

		$vTags = db_res("SELECT `Tags` FROM `SDatingEvents` WHERE `EventLevel`='1' LIMIT 50");
	
		if( !mysql_num_rows( $vTags ) )
			$ret = _t_err('_No tags found here');
		else
		{

		$aEventsTags = array();
	
		while ($aEventTags = mysql_fetch_assoc($vTags) ) {
			$sTagsCommas = trim($aEventTags['Tags']);
			$aTags = explode(',', $sTagsCommas);
			foreach( $aTags as $sTagKey ) {
				if ($sTagKey!='') {
					if( isset($aEventsTags[$sTagKey]) )
						$aEventsTags[$sTagKey]++;
					else
						$aEventsTags[$sTagKey] = 1;
				}
			}
		}	
		
		ksort( $aEventsTags );

		$iMinFontSize = $oTemplConfig -> iTagsMinFontSize;
		$iMaxFontSize = $oTemplConfig -> iTagsMaxFontSize;
		
		$iFontDiff = $iMaxFontSize - $iMinFontSize;

		$iMinRating = min($aEventsTags);
		$iMaxRating = max($aEventsTags);

		$iRatingDiff = $iMaxRating - $iMinRating;
		$iRatingDiff = ($iRatingDiff == 0) ? 1 : $iRatingDiff;

		$ret = '<div class="tags_wrapper">';

		foreach( $aEventsTags as $sTag => $iCount )
		{
			$iTagSize = $iMinFontSize + round( $iFontDiff * ( ( $iCount - $iMinRating ) / $iRatingDiff ) );
			$href = str_replace('{tag}', urlencode($sTag), $hrefTmpl);
			$lineHeight = ($iTagSize > 20) ? "line-height:{$iTagSize}px" : '';
			$tagStyle = "style=\"font-size:{$iTagSize}px; $lineHeight\"";

			$ret .= '<span class="one_tag" '. $tagStyle .'>
				<a href="'.$href.'" title="' . _t( '_Count' ) . ':' . $iCount . '">' . htmlspecialchars_adv( $sTag ) .'</a>
			</span>';
		}

		$ret .= '</div>';
		}
		
		$searchSect = '<div class="caption_item">
			<form name="EventSearchForm" action="'.$_SERVER['PHP_SELF'].'?action=show" method="post">
				<input type="text" name="show_events_keywords" size="23" value="'.$_POST['show_events_keywords'].'"/>
				<input type="submit" value="Search"/>
			</form></div>';

		return '<div id="popular_tags">'.DesignBoxContent ( _t('_popularTags'), $ret, 1, $searchSect).'</div>';
	}	
	

	/**

	 * Compose result into Wrapper class

	 *

	 * @param $sCaption	caption of Box

	 * @param $sValue	inner text of box

	 * @return HTML presentation of data

	 */

	function DecorateAsTable($sCaption, $sValue) {

		$sValueF = <<<EOF

<div class="cls_result_wrapper">

	{$sValue}

</div>

EOF;

		$sDecTbl = DesignBoxContent ( _t($sCaption), $sValueF, 1 );

		return $sDecTbl;

	}

	function showBlockPhoto($profileID, $eventID, $sCaption)
	{
		$oPhotos = new EventPhotos($profileID, $eventID);
		$oPhotos->getActiveMediaArray();
		$iPhotoID = (int)$_REQUEST['photoID'];
		return $oPhotos->getMediaBlock( $iPhotoID );
	}

	/**
	 * page show information about specified event
	 * @return HTML presentation of data
	 */
	function PageSDatingShowInfo() {
		global $site, $dir, $tmpl, $prof, $doll, $su_config, $oTemplConfig, $logged, $meta_keywords, $meta_description, $title;

		$maxTitleLen = 40;
		
		// collect information about current member
		if( $logged['member'] ) {
			$aMember['ID'] = (int)$_COOKIE['memberID'];
			$aMemberData = getProfileInfo( $aMember['ID'] );
		}
		else
			$aMember['ID'] = 0;

		$sNoPhotoC = _t('_No photo');
		$sChangeC = _t('_Change');
		$sCanBuyTicketC = _t('_You can buy the ticket');
		$sBuyTicketC = _t('_Buy ticket');
		$sCountryC = _t('_Country');
		$sCityC = _t('_City');
		$sPlaceC = _t('_Place');
		$sEventStartC = _t('_Event start');
		$sStartDateC = _t('_StartDate');
		$sEndDateC = _t('_EndDate');
		$sEventEndC = _t('_Event end');
		$sTicketSaleStartC = _t('_Ticket sale start');
		$sTicketSaleEndC = _t('_Ticket sale end');
		$sResponsiblePersonC = _t('_Responsible person');
		$sPostedByC = _t('_Posted by');
		$sTicketsLeftC = _t('_Tickets left');
		$sTicketPriceC = _t('_Ticket price');
		$sDescriptionC = _t('_Description');
		$sSaleStatusC = _t('_Sale status');
		$sEventC = _t('_Event');
		$sEditC = _t('_Edit');
		$sDeleteC = _t('_Delete');
		$sSureC = _t("_Are you sure");
		$sSureDeleteEvent = _t('_confirmDeleteEvent');
		$sPictureC = _t('_Picture');
		$sStatusC = _t('_Status');
		$sPhoneC = _t('_Phone');
		$sEmailC = _t('_E-mail');
		$sActionsC = _t('_Actions');
		$sJoinC = _t('_Join');
		$sUnjoinC = _t('_Unjoin');
		$sParticipantsC = _t('_Participants');
		$sListOfParticipantsC = _t('_List').' '._t('_of').' '._t('_Participants');
		$sTagsC = _t('_Tags');

		if (isset($_GET['sefTitle']) && isset($_GET['sefNick']))
		{
			$ownerID = getID($_GET['sefNick'], 0);
			if ($ownerID)
				$iEventID = db_value("SELECT ID FROM SDatingEvents
					WHERE ResponsibleID = $ownerID AND urltitle = '{$_GET['sefTitle']}'
					LIMIT 1");
			else
				return _t_err('Event Not Found');
		}
		else
			$iEventID = (int)$_REQUEST['event_id'];
		
		if (!$iEventID)
			return _t_err('Event Not Found');

		if ($this->iLastInsertedID > 0) {
			$iEventID = $this->iLastInsertedID;
			$this->iLastInsertedID = -1;
		}
		$sQuery =  "
			SELECT
				`SDatingEvents`.`ID` AS `EventIDN`,
				`Title`, `Description`,	`PhotoFilename`, `StatusMessage`, `Country`, `City`,
				`Place`, `Tags`, `EventStart`, `EventEnd`,
				DATE_FORMAT(`EventEnd`, '{$date_format}' ) AS EventEndFormat,
				(NOW() > `EventStart`) AS `EventBegan`,
				(NOW() < `EventEnd`) AS `EventNotFinished`,
				DATE_FORMAT(`TicketSaleStart`, '{$date_format}' ) AS TicketSaleStart,
				DATE_FORMAT(`TicketSaleEnd`, '{$date_format}' ) AS TicketSaleEnd,
				(NOW() > `TicketSaleStart`) AS `SaleBegan`,
				(NOW() < `TicketSaleEnd`) AS `SaleNotFinished`,
				(UNIX_TIMESTAMP() - UNIX_TIMESTAMP(`EventStart`)) AS `sec`,
				(UNIX_TIMESTAMP() - UNIX_TIMESTAMP(`EventEnd`)) AS `secAgo`,
				`ResponsibleID`, `ResponsibleName`,	`ResponsibleEmail`, `ResponsiblePhone`,
				`TicketPriceFemale`, `TicketPriceMale`, `TicketCountFemale`, `TicketCountMale`,
				`ReadPermission`, `JoinPermission`,	`urltitle`,
				(NOW() > `EventEnd` AND NOW() < DATE_ADD(`EventEnd`, INTERVAL `ChoosePeriod` DAY)) AS `ChooseActive`,
				(`SDatingParticipants`.`ID` IS NOT NULL) AS `IsParticipant`
			FROM `SDatingEvents`
			LEFT JOIN `SDatingParticipants` ON
				`SDatingParticipants`.`IDEvent` = `SDatingEvents`.`ID`
				AND `SDatingParticipants`.`IDMember` = {$aMember['ID']}
			WHERE
				`SDatingEvents`.`ID` = {$iEventID}
				AND `SDatingEvents`.`Status` = 'Active'
			";

		$aEventData = db_arr( $sQuery );

		if ( !is_array($aEventData) || count($aEventData) == 0 )
			return DesignBoxContent( '', '<center>'. _t('_Event is unavailable') .'</center>', $oTemplConfig -> PageSDatingShowInfo_db_num );
		
		$bFriend = is_friends($aMember['ID'], $aEventData['ResponsibleID']);
		$bOwner = ($aMember['ID'] == $aEventData['ResponsibleID']) ? true : false;	
			
		// Check read permission
		if ($aEventData['ReadPermission'] == 'friends' && !$bFriend && !$bOwner && !$this->bAdminMode)
			return MsgBox('Event Locked For Friends Only', 16, 2, 
				"<img src=\"{$site['icons']}lock.gif\" style=\"height:40px; margin-right:5px; vertical-align:middle\"/>");

		$sQuery = "
			SELECT COUNT(*)
			FROM `SDatingParticipants`
			LEFT JOIN `Profiles` ON
				`SDatingParticipants`.`IDMember` = `Profiles`.`ID`
			WHERE
				`SDatingParticipants`.`IDEvent` = {$iEventID}
				AND `Profiles`.`Sex` = '{$sMemberSex}'";

		$aPartNum = db_arr( $sQuery );
		$iPartNum = (int)$aPartNum[0];
		$iTicketsLeft = ( $aMemberData['Sex'] == 'male' ? $aEventData['TicketCountMale'] - $iPartNum : $aEventData['TicketCountFemale'] - $iPartNum );
		$iTicketPrice = (float)( $aMemberData['Sex'] == 'male' ? $aEventData['TicketPriceMale'] : $aEventData['TicketPriceFemale'] );

		// change participant UID
		$sErrorMessage = '';
		if ( isset($_POST['change_uid']) && $_POST['change_uid'] == 'on' )
		{
			// check if this UID doesn't exist for this event
			$sNewUid = process_db_input($_POST['participant_uid']);
			$aExistUid = db_arr("SELECT `ID` FROM `SDatingParticipants`
				WHERE `IDEvent` = {$iEventID}
				AND `IDMember` <> {$aMember['ID']}
				AND LOWER(`ParticipantUID`) = LOWER('$sNewUid')" );

			if ( !$aExistUid['ID'] )
			{
				$vRes = db_res("UPDATE `SDatingParticipants`
					SET `ParticipantUID` = '$sNewUid'
					WHERE `IDEvent` = $iEventID
					AND `IDMember` = {$aMember['ID']}" );
				if ( !$vRes )
					$sErrorMessage = _t('_Cant change participant UID');
			} else
				$sErrorMessage = _t('_UID already exists');
		}

		// if ticket is free then buy it here without any checkouts
		if ( isset($_POST['purchase_ticket']) && $_POST['purchase_ticket'] == 'on' && !$aEventData['IsParticipant'] and $logged['member'] ) {
			if ( $aEventData['SaleBegan'] && $aEventData['SaleNotFinished'] && $iTicketsLeft > 0 && $iTicketPrice <= 0.0 ) {

				// insert into participants table
				$iParticipantUID = $aMemberData['NickName'] . $iEventID . rand(100, 999);
				$vRes = db_res( "INSERT INTO `SDatingParticipants` SET `IDEvent` = {$iEventID}, `IDMember` = {$aMember['ID']}, `ParticipantUID` = '{$iParticipantUID}'", 0 );

				if ( !$vRes )
					$sErrorMessage = _t('Error: Participant subscription error');
				else {
					$sSubject = getParam( 't_SDatingCongratulation_subject' );
					$sMessage = getParam( 't_SDatingCongratulation' );
					TrackNewAction(10, $iEventID, $aMember['ID'], $aEventData['Title'], '', '', $aEventData['ResponsibleID']);
					$aPlus = array();
					$aPlus['NameSDating'] = $aEventData['Title'];
					$aPlus['PlaceSDating'] = $aEventData['Place'];
					$aPlus['WhenStarSDating'] = $aEventData['EventStart'];
					$aPlus['PersonalUID'] = $iParticipantUID;
					$aPlus['LinkSDatingEvent'] = $site['url'] . 'events.php?action=show_info&event_id=' . $iEventID;

					$vMailRes = sendMail( $aMemberData['Email'], $sSubject, $sMessage, $aMember['ID'], $aPlus );

					$_POST['result'] = ($vMailRes) ? 1 : 3;
				}
			} else
				$_POST['result'] = -1;
		}
		elseif ( isset($_REQUEST['join_event']) && $_REQUEST['join_event'] == 'on' && $logged['member'] ) 
		{
			// Check join permission
			if ($aEventData['JoinPermission'] == 'friends' && !$bFriend && !$bOwner && !$this->bAdminMode)
				return MsgBox('Event joining locked for friends only', 16, 2, 
					"<img src=\"{$site['icons']}lock.gif\" style=\"height:40px; margin-right:5px; vertical-align:middle\"/>");
			
			// Insert into participants table
			$iParticipantUID = $aMemberData['NickName'] . $iEventID . rand(100, 999);

			$vRes = db_res( "INSERT INTO `SDatingParticipants` SET `IDEvent` = {$iEventID}, `IDMember` = {$aMember['ID']}, `ParticipantUID` = '{$iParticipantUID}'", 0 );

			if ( !$vRes ) {
				$sErrorMessage = _t('Error: Participant subscription error');
				$sRetHtml .= _t_err("_Sorry, you're already joined");
			} 
			else 
			{
				$sSubject = getParam( 't_SDatingCongratulation_subject' );
				$sMessage = getParam( 't_SDatingCongratulation' );

				TrackNewAction(10, $iEventID, $aMember['ID'], $aEventData['Title'], 
					'', '', $aEventData['ResponsibleID'], '', true);

				$aPlus = array();
				$aPlus['NameSDating'] = $aEventData['Title'];
				$aPlus['PlaceSDating'] = $aEventData['Place'];
				$aPlus['WhenStarSDating'] = $aEventData['EventStart'];
				$aPlus['PersonalUID'] = $iParticipantUID;
				$aPlus['LinkSDatingEvent'] = $site['url'] . 'events.php?action=show_info&event_id=' . $iEventID;

				$vMailRes = sendMail( $aMemberData['Email'], $sSubject, $sMessage, $aMember['ID'], $aPlus );

				$sRetHtml .= _t_action("You've successfully registered for this event.");

				$_POST['result'] = ($vMailRes) ? 1 : 3;
			}
		} 
		elseif ( isset($_POST['join_event']) && $_POST['join_event'] == 'on' 
			&& $logged['member']==false && !$bOwner)
				$this->CheckLogged();
		elseif (isset($_POST['unjoin_event']) && $_POST['unjoin_event'] == 'on' 
			&& $logged['member'] && !$bOwner)
		{
			// Delete member from event participants table
			$vRes = db_res("DELETE FROM `SDatingParticipants` WHERE `IDEvent`={$iEventID} AND `IDMember`={$aMember['ID']}");
			if ($vRes)
				$sRetHtml .= _t_action("_You have successfully unjoined this Event");
				
			// Clear record associated with event joining from newsfeedtrack table
			db_res("DELETE FROM NewsFeedTrack WHERE type=10 AND mediaid=$iEventID AND actorid={$aMember['ID']}");
		}
		elseif (isset($_GET['err']))
			$sRetHtml .= _t_err(urldecode($_GET['err']));

		$aMemberPart = db_arr( "SELECT `ID`, `ParticipantUID` FROM `SDatingParticipants`
			WHERE `IDEvent` = $iEventID
			AND `IDMember` = {$aMember['ID']}" );
		$sErrElems = '';
		if ( isset($_POST['result']) ) {
			$sResult = '';
			switch ( $_POST['result'] ) {
				case '-1':   $sResult = _t('_RESULT-1'); break;
				case '0':    $sResult = _t('_RESULT0'); break;
				case '1':    $sResult = _t('_RESULT1_THANK', $aEventData['Title'] , $aEventData['EventStart']); break;
				case '3':    $sResult = _t('_RESULT_SDATING_MAIL_NOT_SENT'); break;
				case '1000': $sResult = _t('_RESULT1000'); break;
			}
			//$sErrElems .= '<div class="err">'.$sResult.'</div>';
		}
		$sPicElement = '';
		$sSpacerName = $site['url'].$this -> sSpacerPath;
		if ( strlen(trim($aEventData['PhotoFilename'])) && file_exists($dir['sdatingImage'] . $aEventData['PhotoFilename']) ) {
			$sPicName = $site['sdatingImage'].$aEventData['PhotoFilename'];
			$sPicElement .= "<img class=\"photo\" style=\"width:{$this -> iImgSize}px;height:{$this -> iImgSize}px;background-image:url({$sPicName});\" src=\"{$sSpacerName}\" border=\"0\" />";
			$sPicElement1 .= '
				<img src="'.$site['sdatingImage'].$aEventData['PhotoFilename'].'" border="0" alt="'._t('_SDating photo alt', $aEventData['Title']).'" />';
		} else {
			$sPicNaName = "{$site['url']}templates/tmpl_{$tmpl}/{$this -> sPicNotAvail}";
			$sPicElement .= "<img class=\"photo\" style=\"width:{$this -> iImgSize}px;height:{$this -> iImgSize}px;background-image:url({$sPicNaName});\" src=\"{$sSpacerName}\" border=\"0\" />";
			$sPicElement1 .= '
				<div align="center" class="text" style="width: 200px; height: 150px; vertical-align: middle; line-height: 150px; border: 1px solid silver;">'.$sNoPhotoC.'</div>';
		}

		/*$sInnerData = '';
		if ( $aMemberPart['ID'] ) {
		if ( $aEventData['EventBegan'] && $aEventData['EventNotFinished'] ) {
		$sInnerData .= _t('_Event started');
		}
		elseif ( $aEventData['ChooseActive'] ) {
		$sInnerData .= _t('_Event finished') .". <a href=\"{$_SERVER['PHP_SELF']}?action=select_match&amp;event_id={$iEventID}\">". _t('_Choose participants you liked') ."</a>";
		} else {
		if ( strlen($sErrorMessage) )
		$sInnerData .= "<div align=\"center\" class=\"err\" style=\"width: 100%;\">{$sErrorMessage}</div>\n";
		$sInnerData .= _t('_You are participant of event').'<br />';
		$sParticipantUID = htmlspecialchars($aMemberPart['ParticipantUID']);
		$sInnerData .= <<<EOF
		<center>
		<form id="changeUIDForm" action="{$_SERVER['PHP_SELF']}?action=show_info&event_id={$iEventID}" method="post" style="margin: 2px;">
		<input type="hidden" name="change_uid" value="on" />
		<input type="text" class="no" name="participant_uid" value="{$sParticipantUID}" size="20" maxlength="30" style="vertical-align: middle;" />&nbsp;
		<input type="submit" class="no" value="{$sChangeC}" style="width: 80px; vertical-align: middle" />
		</form>
		</center>
		EOF;
		}
		}
		elseif ( $aEventData['SaleBegan'] and $aEventData['SaleNotFinished'] and $logged['member'] ) {
		if ( $iTicketsLeft > 0 ) {
		if ( $iTicketPrice > 0.0 ) {
		$sInnerData .= <<<EOF
		{$sCanBuyTicketC}<br />
		<center>
		<form id="buyTicketForm" action="{$site['url']}checkout.php" method="post" style="margin: 2px;">
		<input type="hidden" name="action" value="collect" />
		<input type="hidden" name="checkout_action" value="speeddating" />
		<input type="hidden" name="data" value="{$iEventID}" />
		<input type="hidden" name="amount" value="{$iTicketPrice}" />
		<input type="submit" class="no" value="{$sBuyTicketC}" style="width: 100px; vertical-align: middle;" />
		</form>
		</center>
		EOF;
		}
		else {
		$sInnerData .= <<<EOF
		{$sCanBuyTicketC}<br />
		<center>
		<form id="buyTicketForm" action="{$_SERVER['PHP_SELF']}?action=show_info&event_id={$iEventID}" method="post" style="margin: 2px;">
		<input type="hidden" name="purchase_ticket" value="on" />
		<input type="submit" class="no" value="{$sBuyTicketC}" style="width: 100px; vertical-align: middle;" />
		</form>
		</center>
		EOF;
		}
		} else {
		$sInnerData .=  _t('_No tickets left');
		}
		} elseif ( $aEventData['SaleBegan'] ) {
		$sInnerData .=  _t('_Sale finished');
		} else {
		$sInnerData .=  _t('_Sale not started yet');
		}*/

		$sStatusMessage = process_line_output($aEventData['StatusMessage']);
		$sCountryPic = "<a href=\"search_result.php?country={$aEventData['Country']}\">" . 
			_t('__'. $prof['countries'][$aEventData['Country']]) . '</a>';
		// If city saved with state then separate them (link only city)
		list($city, $state) = explode(',', $aEventData['City']);
		$sCity = '<a href="search_result.php?city=' . urlencode(trim($city)) . '">' . 
			process_line_output($city) . '</a>';
		if ($state)
			$sCity .= ",$state";
		$sPlace = process_line_output($aEventData['Place']);
		$sResponsiblePerson = process_line_output($aEventData['ResponsibleName']);
		$aPostedBy = $this->GetProfileData($aEventData['ResponsibleID']);
		$sPostedBy = $aPostedBy['NickName'];
		$sPhone = process_line_output($aEventData['ResponsiblePhone']);
		$sEmail = process_line_output($aEventData['ResponsibleEmail']);
		$sTicketPrice = ($iTicketPrice > 0.0) ? $doll . $iTicketPrice : _t('_free');
		$sDescription = /*process_text_withlinks_output*/ $aEventData['Description'];
		$sTitle = process_line_output($aEventData['Title']);
		if (strlen($sTitle) > $maxTitleLen)
			$sTitle = trim(substr($sTitle, 0, $maxTitleLen - 3)) . '...';
		$sTagsVals = '';
		$sTagsCommas = $aEventData['Tags'];
		$aTags = split(',', $sTagsCommas);
		foreach( $aTags as $sTagKey ) {
			if( isset($aTagsPost[$sTagKey] ) )
				$aTagsPost[$sTagKey]++;
			else
				$aTagsPost[$sTagKey] = 1;
		}

		$tagHrefTmpl = (useSEF) ? "{$site['url']}events/tagKey=%tag%" 
			: "{$site['url']}events.php?action=search_by_tag&amp;tagKey=%tag%";

		foreach( $aTagsPost as $varKey => $varValue ) {
			$tagHref = str_replace('%tag%', $varKey, $tagHrefTmpl);
			$sTagsVals .= <<<EOF
<div style="float:left; padding-bottom:5px; padding-left:10px; position:relative">
<!--<span><img src="{$site['icons']}tag.png" class="marg_icon" alt="" /></span>-->
<a class="actions" href="$tagHref" style="text-transform:capitalize" >{$varKey}</a>
</div>

EOF;

}

// Update meta data
$eventNumTagsAsKeywords = (int)getParam('eventNumberTagsAsKeywords');
$meta_keywords = implode(', ', array_slice($aTags, 0, $eventNumTagsAsKeywords));

// Save first $wordsCount of event description to the meta description
$wordsCount = (int)getParam('eventDescMaxLength');
$meta_description = RemoveHTMLEntities(process_line_output(html2txt(BreakLinesToSpaces($aEventData['Description']))));
preg_match_all("/\w+/", $meta_description, $matches, PREG_OFFSET_CAPTURE);
if (count($matches[0]) > $wordsCount)
{
	$lastWordOffset = $matches[0][$wordsCount][1];
	$meta_description = substr($meta_description, 0, $lastWordOffset);
}
else 
	$meta_description = $meta_description;

$title = $aEventData['Title'];

$sActions = '';

if ($aEventData['ResponsibleID']==(int)$_COOKIE['memberID'] || $this->bAdminMode) {
	$sActions = <<<EOF
<div class="padds">
	<img src="{$site['icons']}action_edit.png" alt="{$sEditC}" title="{$sEditC}" class="marg_icon" />
	<a class="actions" href="{$_SERVER['PHP_SELF']}" onclick="UpdateField('EditEventID','{$aEventData['EventIDN']}');document.forms.command_edit_event.submit();return false;" style="text-transform:none;">{$sEditC}</a>&nbsp;
</div>
<div class="padds">
	<img src="{$site['icons']}delete.png" alt="{$sDeleteC}" title="{$sDeleteC}" class="marg_icon" />
	<a class="actions" href="{$_SERVER['PHP_SELF']}" onclick="if (confirm('{$sSureDeleteEvent}')) {UpdateField('DeleteEventID','{$aEventData['EventIDN']}');document.forms.command_delete_event.submit(); } return false;" style="text-transform:none;">{$sDeleteC}</a>
</div>

EOF;

}

$visitorID = (int)$_COOKIE['memberID'];
$isOwner = $visitorID == $aEventData['ResponsibleID'];
$isParticipant = db_value("SELECT COUNT(*) FROM SDatingParticipants WHERE IDEvent=$iEventID AND IDMember=$visitorID LIMIT 1");

if ($isOwner || $isParticipant) {
	$sActions .= <<<EOF
<div class="padds">
	<img src="{$site['icons']}action_add.png" alt="Add photo" title="Add photo" class="marg_icon" />
	<a class="actions" href="{$site['url']}uploadSharePhoto.php?eventID={$aEventData['EventIDN']}" style="text-transform:none;">Add Photo</a>
</div>
<div class="padds">
	<img src="{$site['icons']}action_add.png" alt="Add video" title="Add video" class="marg_icon" />
	<a class="actions" href="{$site['url']}uploadShareVideo.php?eventID={$aEventData['EventIDN']}" style="text-transform:none;">Add Video</a>
</div>

EOF;
}

if ($isOwner) {
	$sActions .= <<<EOF
<div class="padds">
	<img src="{$site['icons']}action_add.png" alt="Add Links" title="Add Links" class="marg_icon" />
	<a class="actions" href="{$site['url']}addEventLinks.php?eventID={$aEventData['EventIDN']}" style="text-transform:none;">Add Links</a>
</div>
<div class="padds">
	<img src="{$site['icons']}action_share.gif" alt="Send Broadcast" title="Send Broadcast" class="marg_icon" />
	<a class="actions" href="{$site['url']}compose.php?eventID={$aEventData['EventIDN']}" style="text-transform:none;">Send Broadcast</a>
</div>
EOF;
}

$sActions .= <<<EOF
<div class="padds">
	<img src="{$site['icons']}action_share.gif" alt="Share" title="Share" class="marg_icon" />
	<a class="actions" href="javascript:void(0);" onclick="javascript: window.open('eventActions.php?eventID={$aEventData['EventIDN']}', 'event', 'width=500, height=380, menubar=no,status=no,resizable=yes,scrollbars=yes,toolbar=no,location=no' );" style="text-transform:none;">Share</a>
</div>
EOF;

$participantsCount = db_value("SELECT COUNT(`ParticipantUID`)
	FROM `SDatingParticipants`
	WHERE `IDEvent` = {$iEventID}");

if (!$participantsCount)
{
	// Insert event owner as participant by default
	$iParticipantUID = getNickName($aEventData['ResponsibleID']) . $iEventID . rand(100, 999);
	$vRes = db_res("INSERT INTO `SDatingParticipants` SET `IDEvent` = $iEventID, 
		`IDMember` = {$aEventData['ResponsibleID']}, `ParticipantUID` = '{$iParticipantUID}'", 0 );
}

$vPartProfilesRes = db_res( "
			SELECT `Profiles`.*, `SDatingParticipants`.`ParticipantUID` AS `UID` FROM `SDatingParticipants`
			LEFT JOIN `Profiles` ON `SDatingParticipants`.`IDMember` = `Profiles`.`ID`
			WHERE `SDatingParticipants`.`IDEvent` = {$iEventID}
			ORDER BY `Profiles`.`Picture` DESC, RAND() LIMIT 18" );

$iCounter = 0;
$imagesPerRow = 6;
$sParticipants = '<table>';

while ( $aPartProfiles = mysql_fetch_assoc($vPartProfilesRes) ) 
{
	if ($iCounter % $imagesPerRow == 0)
	{
		$sParticipants .= '<tr style="vertical-align:top">';
		$paddingLeft = '0px';
	}
	else 
		$paddingLeft = '5px';
	
	$iUserIsOnline = get_user_online_status($aPartProfiles['ID']);
	// Profiles' thumbnails with links
	/*
	$sCont = get_member_thumbnail($aPartProfiles['ID'], 'left', false, 'relative' );
	$sParticipants .= '<td><div style="float:left; position:relative; padding-bottom:10px; padding-left:'.$paddingLeft.'">'.$sCont. 
		'<center><div class="browse_nick"><a href="'.getProfileLink($aPartProfiles['ID']).'">'.
		$aPartProfiles['NickName'].'</a></div></center></div></td>';
		*/
	// Profiles' icons only
	$sCont = get_member_icon($aPartProfiles['ID'], 'left', false);
	$sParticipants .= "<td><div style=\"float:left; padding-bottom:10px; padding-left:$paddingLeft\">$sCont</div></td>";
			
	$iCounter++;
	
	if ($iCounter % $imagesPerRow == 0)
		$sParticipants .= '</tr>';	
}

$sParticipants .= '</table>';

$sAdminTicketsPart = '';$sAdminTicketsPart2='';
$sStatusSect = '';
if ($aEventData['ResponsibleID']==0) {
	$sAdminTicketsPart = <<<EOF
<div class="cls_res_info">
	{$sEventEndC}: <div class="clr3">{$aEventData['EventEndFormat']}</div>
</div>
<div class="cls_res_info">
	{$sTicketSaleStartC}: <div class="clr3">{$aEventData['TicketSaleStart']}</div>
</div>
<div class="cls_res_info">
	{$sTicketSaleEndC}: <div class="clr3">{$aEventData['TicketSaleEnd']}</div>
</div>
EOF;

	$sAdminTicketsPart2 = <<<EOF
<div class="cls_res_info">
	{$sTicketsLeftC}: <div class="clr3">{$iTicketsLeft}</div>
</div>
<div class="cls_res_info">
	{$sTicketPriceC}: <div class="clr3">{$sTicketPrice}</div>
</div>
<tr class="panel">
	<td colspan="2" align="center" class="profile_header"><b>{$sSaleStatusC}</b></td>
</tr>
<!-- <tr>
	<td colspan="2" align="left" class="profile_td_2">
		{$sInnerData}
	</td>
</tr> -->
EOF;

$sStatusSectCont = '<div class="cls_res_info"><div class="clr3">'.$sStatusMessage.'</div></div>';
$sStatusSect = DesignBoxContent($sStatusC, $sStatusSectCont, 1);
}

$sPicElement = $this->showBlockPhoto($aEventData['ResponsibleID'], $iEventID, $sEventC);
$sImageSectCont = '<div class="photoBlock">'.$sPicElement.'</div>';
$sImageSect = DesignBoxContent($sTitle, $sImageSectCont, 1);
			
if (!$aEventData['EventBegan'] || $aEventData['EventNotFinished'])
{
	$joinPerm = true;

	// Check join permission	
	if ($aEventData['JoinPermission'] == 'friends' && !$bFriend && !$bOwner && !$this->bAdminMode)
		$joinPerm = false;
	
	if ($joinPerm && !$bOwner)
	{
		$aSelfPart = db_arr("SELECT `ID` FROM `SDatingParticipants`
			WHERE `IDEvent` = $iEventID	AND `IDMember` = {$aMember['ID']}");
		
		$joinUnjoinAction = ($aSelfPart) ? 
			"<div class=\"padds\">
				<img src=\"{$site['icons']}action_evjoin.png\" alt=\"$sUnjoinC\" title=\"$sUnjoinC\" class=\"marg_icon\"/>
					<a class=\"actions\" href=\"{$_SERVER['PHP_SELF']}\" onclick=\"document.forms.UnjoinEventForm.submit(); return false;\" >
						$sUnjoinC
					</a>
			</div>"
			: "<div class=\"padds\">
				<img src=\"{$site['icons']}action_evjoin.png\" alt=\"$sJoinC\" title=\"$sJoinC\" class=\"marg_icon\"/>
					<a class=\"actions\" href=\"{$_SERVER['PHP_SELF']}\" onclick=\"document.forms.JoinEventForm.submit(); return false;\" >
						$sJoinC
					</a>
			</div>";
	}
}
else 
	$joinUnjoinAction = '';

// Show actions section for logged in members only or admin
// and Event Registration section otherwise
if ($logged['member'] || $this->bAdminMode) 
	$sActionsSect = DesignBoxContent($sActionsC, "<div id=\"actionList\">$joinUnjoinAction	{$sActions}</div>", 1);
else
{
	// Prepare relocate (clear from additional parameters)
	$requestURI = urldecode($_SERVER['REQUEST_URI']);
	$joinPos = strpos($requestURI, '&join_event');
	if ($joinPos)
		$relocate = substr($requestURI, 0, $joinPos);
	else
	{
		$errPos = strpos($requestURI, '&err');
		if ($errPos)
			$relocate = substr($requestURI, 0, $errPos);
		else
			$relocate = $requestURI;
	}
	
	$relocate = "http://{$_SERVER['HTTP_HOST']}$relocate"; // save current host to referer because on login will be redirect to YH1 host
	$displayLogForm = (isset($_GET['err'])) ? 'none' : 'block';
	$eventRegCont = <<<EOF
		<div id="eventLoginFormCont" style="display:$displayLogForm">
			<form action="member.php" method="post" name="eventLoginForm" id="eventLoginForm">
				<input type="hidden" name="relocate" value="$relocate"/>
				<input type="hidden" name="event_id" value="$iEventID"/>
				<input type="text" onfocus="document.eventLoginForm.ID.value='';" value="Username" 
					style="width: 80px;" class="login_form_input" id="ID" name="ID"/>
				<input type="password" style="width: 80px;" onfocus="document.eventLoginForm.Password.value='';" 
					value="Password" class="login_form_input" maxlength="8" name="Password" id="Password"/><br/><br/>
				<input type="checkbox" name="eventAttend" checked="1"/> Yes, I want to confirm my attendance<br/><br/>
				<input type="submit" value="Log in"/>
				<div style="font-size:11px; margin-top:10px"><b>Not a YogaHub member yet?</b> <a href="javascript:void(0)" 
					onclick="$('#eventLoginFormCont').hide(); $('#eventRegisterFormCont').show();"/>Click here to register.</a></div>
			</form>
		</div>
EOF;
	$eventRegCont .= $this->_eventRegisterForm($relocate);
	$sActionsSect = DesignBoxContent('Event Registration', $eventRegCont, 1);
}


// Prepare comment pagination
$iDivis = 5;
$iCurr  = 1;

if (!isset($_GET['commPage']))
	$sLimit =  ' LIMIT 0,'.$iDivis;
elseif ($_GET['commPage'] > 0)
{
	$iCurr = (int)$_GET['commPage'];
	$sLimit =  ' LIMIT '.($iCurr - 1)*$iDivis.','.$iDivis;
}
else 
	$sLimit = '';			

$iNums = db_value("SELECT COUNT(`CommentID`) FROM `SDatingEventComments` WHERE `EventID` = $iEventID");
$sNav = ($iNums > $iDivis && $sLimit != '') ? commentNavigation($iNums, $iDivis, $iCurr) : '';

if ($sNav != '')
{
	$commPagePos = strpos($_SERVER['REQUEST_URI'], '&commPage');	
	$baseUrl = ($commPagePos !== false) ? substr($_SERVER['REQUEST_URI'], 0, $commPagePos) : $_SERVER['REQUEST_URI'];
	$sNav .= "<a href=\"$baseUrl&commPage=0\" style=\"float:left; clear:left; 
		margin-bottom:15px; width:140px; padding-left:10px\">View all $iNums comments</a>";
}

$oComments = new BxDolComments(3);
$sCommentsSect = $oComments->PrintCommentSection($iEventID, '', $sLimit, $sNav);

//$secAgo = ($aEventData['EventEnd'] < $aEventData['EventStart']) ? $aEventData['sec'] : $aEventData['secAgo'];
if ($aEventData['EventNotFinished'] || !$aEventData['EventBegan'])
{
	$sEventsStart = '('. _format_when($aEventData['sec']) . ')';
	$sEventsEnd = '';
}
else 
{
	$sEventsStart = '';	
	$sEventsEnd = '(<span style="color:red">Ended: '._format_when($aEventData['secAgo']).'</span>)';
}

$date_format_php = getParam('php_date_format');

$sDateTime = date( $date_format_php, strtotime( $aEventData['EventStart'] ) );
$sEndDateTime = date( $date_format_php, strtotime( $aEventData['EventEnd'] ) );

$sSubjectSectCont = <<<EOF
		<div class="cls_res_info">
			{$sCountryC}: <div class="clr3">{$sCountryPic}</div>
		</div>
		<div class="cls_res_info">
			{$sCityC}: <div class="clr3">{$sCity}</div>
		</div>
		<div class="cls_res_info">
			{$sPlaceC}: <div class="clr3">{$sPlace}</div>
		</div>
		<div class="cls_res_info">
			{$sStartDateC}: <div class="clr3">{$sDateTime} {$sEventsStart}</div>
		</div>
		<div class="cls_res_info">
			{$sEndDateC}: <div class="clr3">{$sEndDateTime} {$sEventsEnd}</div>
		</div>		
		{$sAdminTicketsPart}
		<div class="cls_res_info">
			{$sPostedByC}: <a href="{$site['url']}{$sPostedBy}"><div class="clr3">{$sPostedBy}</div></a>
		</div>
		<!-- <div class="cls_res_info">
			{$sPhoneC}: <div class="clr3">{$sPhoneC}</div>
		</div>
		<div class="cls_res_info">
			{$sEmailC}: <div class="clr3">{$sEmail}</div>
		</div> -->
		{$sAdminTicketsPart2}
EOF;
$sSubjectSect = DesignBoxContent('Details', $sSubjectSectCont, 1);

$sDescriptionSectCont = '<div class="cls_res_info"><div class="clr3">'.$sDescription.'</div></div>';
$sDescriptionSect = DesignBoxContent($sDescriptionC, $sDescriptionSectCont, 1);

$participantsNum = db_value("SELECT COUNT(ID) FROM SDatingParticipants WHERE IDEvent = $iEventID");
$sparticipantsLink = '<div class="caption_item"><a href="'.$_SERVER['PHP_SELF'].'?action=show_part&amp;event_id='.$iEventID.'">'.$participantsNum.' Attending</a></div>';
$sParticipantsSect = DesignBoxContent($sParticipantsC, $sParticipants, 1, $sparticipantsLink, '', '', '', 'float:left; position:relative; width:100%');

// Event links section
$query = "SELECT * FROM eventlinks WHERE eventid = $iEventID AND TRIM(title) != '' AND TRIM(url) != '' ORDER BY position";
$eventLinks = fill_assoc_array(db_res($query));
if (count($eventLinks))
{
	$linksBoxTitle = trim(db_value("SELECT LinksBoxTitle FROM SDatingEvents	WHERE ID = $iEventID"));
	if (!$linksBoxTitle)
		$linksBoxTitle = 'Event Links';
	$eventLinksSect = DesignBoxContent($linksBoxTitle, PageCompOtherSitesLinkContent($eventLinks), 1, '', '', 'float:left;');
}
else
	$eventLinksSect = '';
	
// Tags section
$sTagsSect = DesignBoxContent($sTagsC, $sTagsVals, 1, '', '', 'float:left;');

$sEventPhotosSect = '<div id="show_sharePhotos">'. PageCompSharePhotosContent('Event photos', 0, $iEventID, 'event') . '</div>';
$sEventVideosSect = '<div id="show_shareVideos">'. PageCompShareVideosContent('Event videos', 0, $iEventID, 'event') . '</div>';

$sRetHtml .= <<<EOF
<form id="JoinEventForm" action="{$_SERVER['PHP_SELF']}?action=show_info&amp;event_id={$iEventID}" method="post">
	<input type="hidden" name="join_event" value="on" />
</form>
<form id="UnjoinEventForm" action="{$_SERVER['PHP_SELF']}?action=show_info&amp;event_id={$iEventID}" method="post">
	<input type="hidden" name="unjoin_event" value="on" />
</form>
<!--{$sBreadCrumbs}-->
{$sErrElems}
<div>
	<div class="clear_both"></div>
	<div class="cls_info_left">
		{$sImageSect}
		{$sActionsSect}
		{$sCommentsSect}
	</div>
	<div class="cls_info">
		{$sSubjectSect}
		{$sDescriptionSect}
		{$sStatusSect}
		{$sParticipantsSect}
		{$eventLinksSect}
		{$sTagsSect}
		{$sEventPhotosSect}
		{$sEventVideosSect}
	</div>
	<div class="clear_both"></div>
</div>
<div class="clear_both"></div>
EOF;

if (isset($_GET['commPage']))
	$sRetHtml .= "<script>if (window.addEventListener) window.addEventListener('load', ShowComments, false);
		else if (document.addEventListener) document.addEventListener('load', ShowComments, false);
		else if (window.attachEvent) window.attachEvent('onload',ShowComments); 
		
		function ShowComments()
		{
			location.hash = '#comments_section';
		}
		</script>";	

	if (!session_id()) session_start();	
	if (array_key_exists('needTrackLeads', $_SESSION))
	{
		$sRetHtml .= '<iframe src="'.$su_config['main_host'].'yogahub.com/track.php?result=lead" width="1" height="1" frameborder="0"></iframe>';
		unset($_SESSION['needTrackLeads']);
	}

	return $sRetHtml;
}

	function _eventRegisterForm($relocate)
	{
		global $su_config;
			
		$scriptsPath = "profiles/js/";
		if (isset($_GET['uname']))
			$uname = $_GET['uname'];
		elseif (isset($_GET['name']))
			$uname = $_GET['name'];
		else
			$uname = '';
		$email = (isset($_GET['email'])) ? $_GET['email'] : '';
		$firstname = (isset($_GET['firstname'])) ? $_GET['firstname'] : '';
		$lastname = (isset($_GET['lastname'])) ? $_GET['lastname'] : '';
		$displayRegForm = (isset($_GET['err'])) ? 'block' : 'none';
		$res = <<<EOF
<script src="{$scriptsPath}jquery-1.4.1.min.js"></script>
<script src="{$scriptsPath}jquery-ui.min.js"></script>
<script src="{$scriptsPath}jquery.validate.min.js"></script>
<script src="{$scriptsPath}jquery.metadata.pack.js"></script>	
<script src="{$scriptsPath}main.js"></script>
<script src="{$scriptsPath}eventregister.js"></script>
<div id="eventRegisterFormCont" style="display:$displayRegForm"> 
<!--	
<form name="eventRegisterForm" id="eventRegisterForm" method="post" action="{$su_config['url']}registerlight">
<input type="hidden" name="eventURL" value="$relocate"/> 
	
<table cellspacing="2" cellpadding="4" style="margin-left:-10px">
	<tr class="fieldRow">
		<td class="fieldCaption">First Name</td>
		<td><input type="text" class="fieldInput required" minlength="2" maxlength="30"
			name="firstname" id="firstname" value="$firstname"/></td>
	</tr>
	<tr class="fieldRow">
		<td class="fieldCaption">Last Name</td>
		<td><input type="text" class="fieldInput required" minlength="2" maxlength="30" 
			name="lastname" id="lastname" value="$lastname"/>
			<div class="bottomHint" id="lastname_bottom_hint">
				First and Last Name are used for internal records only 
				and not	displayed publicly.</div>
		</td>
	</tr>	
	<tr class="fieldRow">
		<td class="fieldCaption">Primary Email</td>
		<td><input type="text" class="fieldInput required email" maxlength="50"	
			name="email" id="email" value="$email"/>
			<div class="bottomHint" id="email_bottom_hint" style="display:none">
				Used for account verification to complete your registration or 
				if you should ever need to reset your password in the future.</div>
		</td>
	</tr>
	<tr class="fieldRow">
		<td class="fieldCaption">Username</td>
		<td><input type="text" class="fieldInput required alphanumeric" 
			minlength="2" maxlength="12" name="uname" id="uname" value="$uname"/>
			<div class="bottomHint" id="uname_bottom_hint" style="display:none">
				Your ID lets you sign into all our YogaHub sites and is 
				how the public identifies you.</div>
		</td>
	</tr>	
	<tr class="fieldRow">
		<td class="fieldCaption">Password</td>
		<td><input type="password" class="fieldInput required validpass" 
			minlength="3" maxlength="8"	name="passwd1" id="passwd1"/>
			<div class="bottomHint" id="passwd1_bottom_hint" style="display:none">
				Passwords are case sensitive, must be between 3 to 8 characters and 
				cannot contain spaces.</div>	
		</td>
	</tr>
	<tr class="fieldRow">
		<td class="fieldCaption">Retype</td>
		<td><input type="password" class="fieldInput required" equalTo="#passwd1" 
			minlength="3" maxlength="8"	name="passwd2" id="passwd2"/></td>
	</tr>	
	<tr>
		<td colspan="2">		
			<div class="submitCont">
				<input type="hidden" name="submitName" id="submitName" value="step2"/>
				<input class="submit" type="submit" value="Register For Event"/>
				<input type="button" value="Cancel" 
					onclick="$('#eventRegisterFormCont').hide(); $('#eventLoginFormCont').show();"/>
			</div>
		</td>
	</tr>	
</table>	
		
</form>
-->	

<form name="eventRegisterForm" id="eventRegisterForm" method="post" 
	action="http://www.aweber.com/scripts/addlead.pl">
<input type="hidden" name="event_url" value="$relocate"/> 

<input type="hidden" name="meta_web_form_id" value="505303953"/>
<input type="hidden" name="unit" value="01106_yh-member"/>
<input type="hidden" name="meta_adtracking" value="Event Registration"/>
<input type="hidden" name="redirect" value="{$su_config['url']}registerlight"/>
<input type="hidden" name="meta_message" value="1"/>
<input type="hidden" name="meta_required" value="name,email"/>
<input type="hidden" name="meta_forward_vars" value="1"/>	

<table cellspacing="2" cellpadding="4" style="margin-left:-10px">
	<tr class="fieldRow">
		<td class="fieldCaption">First Name</td>
		<td><input type="text" class="fieldInput required" minlength="2" maxlength="30"
			name="firstname" id="firstname" value="$firstname"/></td>
	</tr>
	<tr class="fieldRow">
		<td class="fieldCaption">Last Name</td>
		<td><input type="text" class="fieldInput required" minlength="2" maxlength="30" 
			name="lastname" id="lastname" value="$lastname"/>
			<div class="bottomHint" id="lastname_bottom_hint">
				First and Last Name are used for internal records only 
				and not	displayed publicly.</div>
		</td>
	</tr>	
	<tr class="fieldRow">
		<td class="fieldCaption">Primary Email</td>
		<td><input type="text" class="fieldInput required email" maxlength="50"	
			name="email" id="email" value="$email"/>
			<div class="bottomHint" id="email_bottom_hint" style="display:none">
				Used for account verification to complete your registration or 
				if you should ever need to reset your password in the future.</div>
		</td>
	</tr>
	<tr class="fieldRow">
		<td class="fieldCaption">Username</td>
		<td><input type="text" class="fieldInput required alphanumeric" 
			minlength="2" maxlength="12" name="name" id="name" value="$uname"/>
			<div class="bottomHint" id="name_bottom_hint" style="display:none">
				Your ID lets you sign into all our YogaHub sites and is 
				how the public identifies you.</div>
		</td>
	</tr>	
	<tr class="fieldRow">
		<td class="fieldCaption">Password</td>
		<td><input type="password" class="fieldInput required validpass" 
			minlength="3" maxlength="8"	name="passwd1" id="passwd1"/>
			<div class="bottomHint" id="passwd1_bottom_hint" style="display:none">
				Passwords are case sensitive, must be between 3 to 8 characters and 
				cannot contain spaces.</div>	
		</td>
	</tr>
	<tr class="fieldRow">
		<td class="fieldCaption">Retype</td>
		<td><input type="password" class="fieldInput required" equalTo="#passwd1" 
			minlength="3" maxlength="8"	name="passwd2" id="passwd2"/></td>
	</tr>	
	<tr>
		<td colspan="2">		
			<div class="submitCont">
				<input type="hidden" name="submitName" id="submitName" value="step2"/>
				<input class="submit" type="submit" value="Register For Event"/>
				<input type="button" value="Cancel" 
					onclick="$('#eventRegisterFormCont').hide(); $('#eventLoginFormCont').show();"/>
			</div>
		</td>
	</tr>	
</table>	
		
</form>
</div>
EOF;
		return $res;
	}


	/**

	 * page show participants function

	 * @return HTML presentation of data

	 */

	function PageSDatingShowParticipants() {

		global $site;

		global $oTemplConfig;


		$sRetHtml = '';

		$sEventParticipantsC = _t('_Event participants');

		$sListOfParticipantsC = _t('_List').' '._t('_of').' '._t('_Participants');



		// collect information about current member

		if( $logged['member'] ) {

			$aMember['ID'] = (int)$_COOKIE['memberID'];

			$aMemberData = getProfileInfo( $aMember['ID'] );

		} else

		$aMember['ID'] = 0;



		$aMembership = getMemberMembershipInfo( $aMember['ID'] );



		$iEventID = (int)$_REQUEST['event_id'];



		$sQuery = "

			SELECT `ID`, `Title`,

				(NOW() > `EventEnd` AND NOW() < DATE_ADD(`EventEnd`, INTERVAL `ChoosePeriod` DAY)) AS `ChooseActive`

			FROM `SDatingEvents`

			WHERE

				`ID` = {$iEventID} AND `Status` = 'Active' AND `AllowViewParticipants` = 1";

		$aEventData = db_arr( $sQuery );

		if ( !$aEventData['ID'] )

		return DesignBoxContent( '', '<center>'. _t('_Event is unavailable') .'</center>', $oTemplConfig -> PageSDatingShowParticipants_db_num );


		// list of participants

		$aSelfPart = db_arr( "SELECT `ID` FROM `SDatingParticipants`

									WHERE `IDEvent` = $iEventID

									AND `IDMember` = {$aMember['ID']}" );

		$iPartPage = (isset($_REQUEST['part_page'])) ? (int)$_REQUEST['part_page'] : 1;

		$iPartPPerPage = (isset($_REQUEST['part_p_per_page'])) ? (int)$_REQUEST['part_p_per_page'] : 30;

		$iLimitFirst = (int)($iPartPage - 1) * $iPartPPerPage;

		$vPartProfilesRes = db_res( "SELECT `Profiles`.*, `SDatingParticipants`.`ParticipantUID` AS `UID`

										FROM `SDatingParticipants`

										INNER JOIN `Profiles` ON `SDatingParticipants`.`IDMember` = `Profiles`.`ID`

										WHERE `SDatingParticipants`.`IDEvent` = $iEventID

										ORDER BY `Profiles`.`Picture` DESC, `Profiles`.`NickName`

										LIMIT $iLimitFirst, $iPartPPerPage" );

		$aTotal = db_arr( "SELECT COUNT(*) FROM `SDatingParticipants`

									WHERE `SDatingParticipants`.`IDEvent` = $iEventID" );



		$iPartProfilesTotal = (int)$aTotal[0];

		$iPagesNum = ceil( $iPartProfilesTotal / $iPartPPerPage );

		$sPartGetUrl = "{$_SERVER['PHP_SELF']}?action=show_part&amp;event_id={$iEventID}". (isset($_REQUEST['part_p_per_page']) ? '&amp;part_p_per_page='. (int)$_REQUEST['part_p_per_page'] : '');

		$sRetHtml .= '<div class="text" style="font-size:16px; font-weight:bold; margin-bottom:15px">'.process_line_output($aEventData['Title']).'</div>';

		if ( $iPartProfilesTotal == 0 ) {

			$sRetHtml .= _t('_There are no participants for this event');

		} else {

			if ( $iPagesNum > 1 ) {

				$sRetHtml .= '<div class="text">'._t('_Pages').':&nbsp;';



				for ( $i = 1; $i <= $iPagesNum; $i++ ) {

					if ( $i == $iPartPage )

					$sRetHtml .= "[{$i}]&nbsp;";

					else

					$sRetHtml .= "<a href=\"{$sPartGetUrl}&amp;part_page={$i}\">{$i}</a>&nbsp;";

				}

				$sRetHtml .= '</div><br />';

			}



			$sProfilesTrs = '';

			while ( $part_profiles_arr = mysql_fetch_assoc($vPartProfilesRes) ) {

				$iUserIsOnline = get_user_online_status($part_profiles_arr[ID]);

				$sCont = get_member_thumbnail($part_profiles_arr['ID'], 'none' );

				//$sProfilesTrs .= DesignBoxContentBorder( process_line_output( strmaxtextlen( $part_profiles_arr['NickName'], 11 ) ), $sCont );
				$sProfilesTrs .= '<div class="member_thumb">'.$sCont. '<center><div class="browse_nick">
					<a href="'.getProfileLink($part_profiles_arr['ID']).'">'.$part_profiles_arr['NickName'].'</a></div></center></div>';
			}

			$sNicknameC = _t('_Nickname');

			$sDateOfBirthC = _t('_DateOfBirth');

			$sSexC = _t('_Sex');

			$sEventUIDC = _t('_Event UID');



			$sChooseParts = '';

			// show 'choose participants' link only during choose period and if member is participant of this event

			// if ( $this->bAdminMode==FALSE || ($aEventData['ChooseActive'] && $aSelfPart['ID'] )) {

			// $sChooseParts = '<div class="text" align="center"><a href="'.$_SERVER['PHP_SELF'].'?action=select_match&amp;event_id='.$iEventID.'">'._t('_Choose participants you liked').'</a></div><br />';

			// }



			$sPagesHref = '';

			if ( $iPagesNum > 1 ) {

				$sPagesHref .= '<div class="text">'._t('_Pages').':&nbsp;';

				for ( $i = 1; $i <= $iPagesNum; $i++ ) {

					if ( $i == $iPartPage )

					$sPagesHref .= "[{$i}]&nbsp;";

					else

					$sPagesHref .= "<a href=\"{$sPartGetUrl}&amp;part_page={$i}\">{$i}</a>&nbsp;";

				}

				$sPagesHref .= '</div><br />';

			}



			$sRetHtml .= $sProfilesTrs . $sPagesHref;

		}

		$eventLink = '<div class="caption_item"><a href="'.$_SERVER['PHP_SELF'].
			'?action=show_info&amp;event_id='.$iEventID.'">Return to event</a></div>';

		return DesignBoxContent( $sListOfParticipantsC . " ($iPartProfilesTotal)", $sRetHtml, 
			$oTemplConfig -> PageSDatingShowParticipants_db_num, $eventLink);
	}


	/**

	 * page show filer form function

	 * @return HTML presentation of data

	 */

	function PageSDatingShowForm() {

		global $prof;

		global $oTemplConfig;

		global $enable_event_creating;

		global $logged;



		$sShowCalendarC = _t('_Show calendar');

		$sAddNewEventC = _t('_Add new event');

		$sShowAllEventsC = _t('_Show all events');

		$sShowEventsByCountryC = _t('_Show events by country');



		// collect information about current member

		if( $logged['member'] ) {

			$aMember['ID'] = (int)$_COOKIE['memberID'];

			$aMemberData = getProfileInfo( $aMember['ID'] ); //db_arr( "SELECT `Country` FROM `Profiles` WHERE `ID` = {$aMember['ID']}" );

		}



		$aShow = array();

		$sCountryDisabled = 'disabled="disabled"';



		if ( $_REQUEST['show_events'] == 'country' ) {

			$aShow['country'] = process_pass_data($_REQUEST['show_events_country']);

			$sCountryDisabled = '';

		}



		$sBlockForCalendarAndEventDiv = '';

		if( $oTemplConfig -> customize['events']['showTopButtons'] ) {

			$sBlockForCalendarAndEventDiv .= <<<EOF

<div align="center" class="blockForCalendarAndEvent">

	<a href="{$_SERVER['PHP_SELF']}?action=calendar">{$sShowCalendarC}</a>

EOF;

			if( $enable_event_creating and $logged['member'] ) {

				$sBlockForCalendarAndEventDiv .= "| <a href=\"{$_SERVER['PHP_SELF']}?action=new\">{$sAddNewEventC}</a>";

			}

			$sBlockForCalendarAndEventDiv .= '</div>';

		}



		$sShowEventsChk = ($_REQUEST['show_events'] == 'all') ? 'checked="checked"' : '';

		$sCountryChk = ($_REQUEST['show_events'] == 'country') ? 'checked="checked"' : '';



		$sCountryOptions = '';

		$sSelCountry = isset($aShow['country']) ? $aShow['country'] : $aMemberData['Country'];

		foreach ( $prof['countries'] as $key => $value ) {

			$sCountrySelected = ( $sSelCountry == $key ) ? 'selected="selected"' : '';

			$sCountryOptions .= "<option value=\"{$key}\" ". $sCountrySelected ." >". _t('__'.$value) ."</option>";

		}



		$sRetHtml = <<<EOF

<center>

	<script language="JavaScript" type="text/javascript">

	<!--

		function updateShowControls()

		{

			document.getElementById('show_events_select_id').disabled = !(document.getElementById('show_events_country_id').checked);

		}

	-->

	</script>

	{$sBlockForCalendarAndEventDiv}

	<form id="showEventsForm" action="{$_SERVER['PHP_SELF']}" method="get">
		<table cellpadding="4" cellspacing="0" border="0" class="text" width="490">
			<tr>
				<td align="left" colspan="2" class="text">
					<input type="radio" name="show_events" id="show_events_all_id" value="all" style="vertical-align: middle;" 
						onClick="javascript: updateShowControls();" {$sShowEventsChk} />
					&nbsp;<label for="show_events_all_id">{$sShowAllEventsC}</label>
				</td>
			</tr>
			<tr>
				<td align="left" width="200" class="text">
					<input type="radio" name="show_events" id="show_events_country_id" value="country" 
						style="vertical-align: middle;" onClick="javascript: updateShowControls();"  {$sCountryChk} />
					&nbsp;<label for="show_events_country_id">{$sShowEventsByCountryC}</label>
				</td>
				<td align="left" class="text">
					<select class="no" name="show_events_country" id="show_events_select_id" {$sCountryDisabled} >{$sCountryOptions}</select>
				</td>
			</tr>
			<tr>
				<td align="left" width="200" class="text">
					Show events by keywords 
					
				</td>
				<td align="left" class="text">
					<input type="text" name="show_events_keywords" value="" style="width:100%"/>
				</td>
			</tr>
		</table>
		<br />
		<input type="submit" class="no" value="Show" />
		<input type="hidden" name="action" value="show" />
		<input type="hidden" name="from" value="0" />
	</form>

</center>

EOF;



		return DesignBoxContent( _t('_Select events to show'), $sRetHtml, $oTemplConfig -> PageSDatingShowForm_db_num );

	}



	/**

	 * page show filer form function

	 * @return HTML presentation of data

	 */

	function PageSDatingCalendar() {

		global $dir;

		global $site;

		global $sdatingThumbWidth;

		global $sdatingThumbHeight;

		global $prof;

		global $oTemplConfig;

		global $all_countries_events;


		$iPicSize = $this->iIconSize + 15;



		// collect information about current member

		$aMember['ID'] = (int)$_COOKIE['memberID'];

		$aMemberData = getProfileInfo( $aMember['ID'] );

		$sMemberSex = $aMemberData['Sex'];

		$aMembership = getMemberMembershipInfo( $aMember['ID'] );



		// now year, month and day

		list($iNowYear, $iNowMonth, $iNowDay) = explode( '-', date('Y-m-d') );

		// current year, month, month name, day, days in month

		if ( isset($_REQUEST['month']) ) {

			list($iCurMonth, $iCurYear) = explode( '-', $_REQUEST['month'] );

			$iCurMonth = (int)$iCurMonth;

			$iCurYear = (int)$iCurYear;

		}

		else {

			list($iCurMonth, $iCurYear) = explode( '-', date('n-Y') );

		}

		list($sCurMonthName, $iCurDaysInMonth) = explode( '-', date('F-t', mktime( 0, 0, 0, $iCurMonth, $iNowDay, $iCurYear )) );

		// previous month year, month

		$iPrevYear = $iCurYear;

		$iPrevMonth = $iCurMonth - 1;

		if ( $iPrevMonth <= 0 ) {

			$iPrevMonth = 12;

			$iPrevYear--;

		}

		// next month year, month

		$iNextYear = $iCurYear;

		$iNextMonth = $iCurMonth + 1;

		if ( $iNextMonth > 12 ) {

			$iNextMonth = 1;

			$iNextYear++;

		}

		// days in previous month

		$iPrevDaysInMonth = (int)date( 't', mktime( 0, 0, 0, $iPrevMonth, $iNowDay, $iPrevYear ) );

		// days-of-week of first day in current month

		$iFirstDayDow = (int)date( 'w', mktime( 0, 0, 0, $iCurMonth, 1, $iCurYear ) );

		// from which day of previous month calendar starts

		$iPrevShowFrom = $iPrevDaysInMonth - $iFirstDayDow + 1;



		// select events array

		$aCalendarEvents = array();

		$sCountryFilter = 'all';

		if ($all_countries_events)
		$sRCalendarCountry = 'all';
		else
		{
			$sRCalendarCountry = isset($_REQUEST['calendar_country']) ? $_REQUEST['calendar_country'] : $aMemberData['Country'];
			$sRCalendarCountry = ($sRCalendarCountry=='') ? 'all' : $sRCalendarCountry ;
		}

		if ( $sRCalendarCountry == 'all' ) {

			$sCountryFilter = '';

		}

		else {

			$sCountryFilter = 'AND `Country` = \''. process_db_input($sRCalendarCountry) . '\'';

		}

		$memberFilter = (isset($_REQUEST['ID']) && (int)$_REQUEST['ID'] > 0) ? " AND `ResponsibleID`={$_REQUEST['ID']}" : '';
		$leverFilter = ($memberFilter == '') ? " AND `EventLevel`='1'" : '';

		//old WHERE data`s

		/*

		AND FIND_IN_SET('{$sMemberSex}', `EventSexFilter`)

		AND ( TO_DAYS('{$aMemberData['DateOfBirth']}')

		BETWEEN TO_DAYS(DATE_SUB(NOW(), INTERVAL `EventAgeUpperFilter` YEAR))

		AND TO_DAYS(DATE_SUB(NOW(), INTERVAL `EventAgeLowerFilter` YEAR)) )

		AND ( INSTR(`EventMembershipFilter`, '\'all\'') OR INSTR(`EventMembershipFilter`, '\'{$aMembership['ID']}\'') )

		*/

		$sRequest = "SELECT `ID`, `Title`, `PhotoFilename`, `ResponsibleID`,
			DAYOFMONTH(`EventStart`) AS `EventDay`, MONTH(`EventStart`) AS `EventMonth`, `media`.`med_file`
			FROM `SDatingEvents`
			LEFT JOIN `media` ON `med_id`=`PrimPhoto`
			WHERE ( MONTH(`EventStart`) = {$iCurMonth} AND YEAR(`EventStart`) = {$iCurYear} OR
				MONTH( DATE_ADD(`EventStart`, INTERVAL 1 MONTH) ) = {$iCurMonth} AND YEAR( DATE_ADD(`EventStart`, INTERVAL 1 MONTH) ) = {$iCurYear} OR
				MONTH( DATE_SUB(`EventStart`, INTERVAL 1 MONTH) ) = {$iCurMonth} AND YEAR( DATE_SUB(`EventStart`, INTERVAL 1 MONTH) ) = {$iCurYear} )
				{$sCountryFilter}
				$memberFilter
				$leverFilter
				AND `Status` = 'Active'
			";

		$vEventsRes = db_res( $sRequest );

		while ( $aEventData = mysql_fetch_assoc($vEventsRes) ) {
			$aCalendarEvents["{$aEventData['EventDay']}-{$aEventData['EventMonth']}"][$aEventData['ID']]['Title'] = $aEventData['Title'];
			$eventPhoto = ($aEventData['PhotoFilename']) ? $aEventData['PhotoFilename'] : $aEventData['med_file'];
			$aCalendarEvents["{$aEventData['EventDay']}-{$aEventData['EventMonth']}"][$aEventData['ID']]['PhotoFilename'] = $eventPhoto;
			$aCalendarEvents["{$aEventData['EventDay']}-{$aEventData['EventMonth']}"][$aEventData['ID']]['ResponsibleID'] = $aEventData['ResponsibleID'];
		}



		// make calendar grid

		$bPreviousMonth = ($iFirstDayDow > 0 ? true : false);

		$bNextMonth = false;

		$iCurrentDay = ($bPreviousMonth) ? $iPrevShowFrom : 1;

		for ($i = 0; $i < 6; $i++) {

			for ($j = 0; $j < 7; $j++) {

				$aCalendarGrid[$i][$j]['day'] = $iCurrentDay;

				$aCalendarGrid[$i][$j]['month'] = ($bPreviousMonth ? $iPrevMonth : ($bNextMonth ? $iNextMonth : $iCurMonth));

				$aCalendarGrid[$i][$j]['current'] = (!$bPreviousMonth && !$bNextMonth);

				$aCalendarGrid[$i][$j]['today'] = ($iNowYear == $iCurYear && $iNowMonth == $iCurMonth && $iNowDay == $iCurrentDay && !$bPreviousMonth && !$bNextMonth);

				// make day increment

				$iCurrentDay++;

				if ( $bPreviousMonth && $iCurrentDay > $iPrevDaysInMonth ) {

					$bPreviousMonth = false;

					$iCurrentDay = 1;

				}

				if ( !$bPreviousMonth && !$bNextMonth && $iCurrentDay > $iCurDaysInMonth ) {

					$bNextMonth = true;

					$iCurrentDay = 1;

				}

			}

		}



		$sShowEventsByCountryC = _t('_Show events by country');

		$sAllC = _t('_All');

		$sPrevC = _t('_Prev');

		$sNextC = _t('_Next');

		$sSundaySC = _t('_Sunday_short');

		$sMondaySC = _t('_Monday_short');

		$sTuesdaySC = _t('_Tuesday_short');

		$sWednesdaySC = _t('_Wednesday_short');

		$sThursdaySC = _t('_Thursday_short');

		$sFridaySC = _t('_Friday_short');

		$sSaturdaySC = _t('_Saturday_short');

		$sNoPhotoC = _t('_No photo');

		$sCalendarC = _t('_Calendar');



		$sCalendarOptions = '';

		$sCalSel = ( $sRCalendarCountry == 'all' ) ? 'selected="selected"' : '';

		$sCalendarOptions .= '<option value="all" '. $sCalSel ." >{$sAllC}</option>";

		foreach ( $prof['countries'] as $key => $value ) {

			$sCalKeySel = ( $sRCalendarCountry == "{$key}" ) ? 'selected="selected"' : '';

			$sCuontryVal = _t('__'.$value);

			//$sCalendarOptions .= '<option value="{$key}" '. $sCalKeySel ." >{$sCuontryVal}</option>";

			$sCalendarOptions .= "<option value=\"{$key}\" {$sCalKeySel} >{$sCuontryVal}</option>";

		}

		$memberIDParam = (isset($_REQUEST['ID'])) ? "&amp;ID={$_REQUEST['ID']}" : '';

		$sCalendarCountry = (isset($_REQUEST['calendar_country'])) ? '&amp;calendar_country='. process_pass_data($_REQUEST['calendar_country']) : '';

		$sCalendarPrevHref = $_SERVER['PHP_SELF']."?action=calendar$memberIDParam&amp;month={$iPrevMonth}-{$iPrevYear}".$sCalendarCountry;

		$sCurMonYear = _t('_'.$sCurMonthName) .', '. $iCurYear;

		$sCalendarNextHref = $_SERVER['PHP_SELF']."?action=calendar$memberIDParam&amp;month={$iNextMonth}-{$iNextYear}".$sCalendarCountry;



		$sCalTableTrs = '';

		for ($i = 0; $i < 6; $i++) {

			$sCalTableTrs .= '<tr>';

			for ($j = 0; $j < 7; $j++) {

				if ( $aCalendarGrid[$i][$j]['today'] )

				$sCellClass = 'calendar_today';

				elseif ( $aCalendarGrid[$i][$j]['current'] )

				$sCellClass = 'calendar_current';

				else

				$sCellClass = 'calendar_non_current';



				//$sCalTableTrs .= '<td align="center" width="'.$this->iThumbSize.'" height="'.$this->iThumbSize.'" class="'.$sCellClass.'">'.$aCalendarGrid[$i][$j]['day'];

				$sCalTableTrs .= <<<EOF

<td style="width:100px;height:100px;" class="{$sCellClass}">{$aCalendarGrid[$i][$j]['day']}

EOF;



				$vDayMonthValue = $aCalendarGrid[$i][$j]['day'] .'-'.  $aCalendarGrid[$i][$j]['month'];

				if ( isset($aCalendarEvents[$vDayMonthValue]) && is_array($aCalendarEvents[$vDayMonthValue]) ) {

					foreach ( $aCalendarEvents[$vDayMonthValue] as $eventID => $eventArr ) {
						
						$sEventThumbname = getThumbNameByPictureName($eventArr['PhotoFilename'], true);
						
						if (strpos($eventArr['PhotoFilename'], 'g_') === false) // media filename
						{
							$imgSrc = "{$site['profileImage']}{$eventArr['ResponsibleID']}/icon_{$eventArr['PhotoFilename']}";
							$imgFile = "{$dir['profileImage']}{$eventArr['ResponsibleID']}/icon_{$eventArr['PhotoFilename']}";
						}
						else 
						{
							$imgSrc = "{$site['sdatingImage']}icon_{$eventArr['PhotoFilename']}";
							$imgFile = "{$dir['sdatingImage']}icon_{$eventArr['PhotoFilename']}";
						}

						if (file_exists($imgFile)) {

							$sCalTableTrs .= <<<EOF

<div>

<a href="{$_SERVER['PHP_SELF']}?action=show_info&amp;event_id={$eventID}">

	<img src="$imgSrc" border="0" alt="{$eventArr['Title']}" title="{$eventArr['Title']}" style="margin: 2px;" />

</a>

</div>

EOF;

}

else {

	global $tmpl;

	$sSpacerName = $site['url'].$this -> sSpacerPath;

	$sNaname = $site['url'].'templates/tmpl_'.$tmpl.'/'.$this -> sPicNotAvail;

	$sCalTableTrs .= <<<EOF

<!-- <div align="center" class="small" title="{$eventArr['Title']}" style="width: {$sdatingThumbWidth}px; height: {$sdatingThumbHeight}px; vertical-align: middle; line-height: {$sdatingThumbHeight}px; border: 1px solid silver; background-color: #FFFFFF; font-weight: normal; margin: 2px; font-size: 80%; cursor: pointer;"> -->

<div>

	<a href="{$_SERVER['PHP_SELF']}?action=show_info&amp;event_id={$eventID}">

		<img src="{$sSpacerName}" style="width:64px;height:64px; background-image: url({$sNaname});" class="photo1"/>

		<!--<nobr>{$sNoPhotoC}</nobr>-->

	</a>

</div>

EOF;

}
					}
				}

				$sCalTableTrs .= '</td>';
			}
			$sCalTableTrs .= '</tr>';
		}


		$sRetHtml = <<<EOF

<br />

<div align="center" style="margin-bottom: 10px;">

	<form id="calendarCountryForm" action="{$_SERVER['PHP_SELF']}" method="get" style="margin: 0px;">

		<input type="hidden" name="action" value="calendar" />

		<input type="hidden" name="month" value="{$iCurMonth}-{$iCurYear}" />

		{$sShowEventsByCountryC}&nbsp;

		<select class="no" name="calendar_country" onchange="javascript: document.forms['calendarCountryForm'].submit();" style="vertical-align: middle;">{$sCalendarOptions}</select>

	</form>



	<table cellpadding="1" cellspacing="1" border="0" width="100%" class="text" style="text-align:center;margin-top:10px;">

		<tr>

			<td class="calendar_current" style="padding: 3px;">

				<a href="{$sCalendarPrevHref}">{$sPrevC}</a>

			</td>

			<td colspan="5" class="calendar_current">{$sCurMonYear}</td>

			<td class="calendar_current" style="padding: 3px;">

				<a href="{$sCalendarNextHref}">{$sNextC}</a>

			</td>

		</tr>

		<tr>

			<td style="width:{$iPicSize}px;" class="calendar_non_current">{$sSundaySC}</td>

			<td style="width:{$iPicSize}px;" class="calendar_non_current">{$sMondaySC}</td>

			<td style="width:{$iPicSize}px;" class="calendar_non_current">{$sTuesdaySC}</td>

			<td style="width:{$iPicSize}px;" class="calendar_non_current">{$sWednesdaySC}</td>

			<td style="width:{$iPicSize}px;" class="calendar_non_current">{$sThursdaySC}</td>

			<td style="width:{$iPicSize}px;" class="calendar_non_current">{$sFridaySC}</td>

			<td style="width:{$iPicSize}px;" class="calendar_non_current">{$sSaturdaySC}</td>

		</tr>

	{$sCalTableTrs}

	</table>

</div>

<br />

EOF;

		if (isset($_REQUEST['ID']))
			$ownerPrefix = ($aMember['ID'] == $_REQUEST['ID']) ? 'My'
				: ucwords(getNickName($_REQUEST['ID']))."'s";
		else 
			$ownerPrefix = '';

		return DesignBoxContent("$ownerPrefix $sCalendarC", $sRetHtml, $oTemplConfig -> PageSDatingCalendar_db_num );

	}



	function GenerateZapatecCode($sEl1, $sEl2) {

		$sRes = <<<EOF

Zapatec.Calendar.setup({

	firstDay          : 1,

	weekNumbers       : true,

	showOthers        : true,

	showsTime         : true,

	timeFormat        : "24",

	step              : 2,

	range             : [1900.01, 2099.12],

	electric          : false,

	singleClick       : true,

	inputField        : "{$sEl1}",

	button            : "{$sEl2}",

	ifFormat          : "%Y-%m-%d %H:%M:%S",

	daFormat          : "%Y/%m/%d",

	align             : "Br"

});

EOF;

		return $sRes;

	}



	/**

	 * page show add new event form function

	 * @return HTML presentation of data

	 */

	function PageSDatingNewEventForm($iEventID=-1, $arrErr = NULL) {
		$this->CheckLogged();

		global $site;
		global $prof;
		global $new_result_text;
		global $oTemplConfig;
		global $date_format;
		global $logged;
		global $dir;

		// collect information about current member
		$aMember['ID'] = (int)$_COOKIE['memberID'];
		$aMemberData = getProfileInfo( $aMember['ID'] ); //db_arr( "SELECT `Country` FROM `Profiles` WHERE `ID` = {$aMember['ID']}" );
		$sMemberCountry = ($this->bAdminMode) ? getParam( 'default_country' ) : $aMemberData['Country'];

		$sPleaseFillAllFieldsC = _t('_Please fill up all fields');
		$sTitleC = _t('_Title');
		$sDescriptionC = _t('_Description');
		$sStatusMessageC = _t('_Status message');
		$sCountryC = _t('_Country');
		$sCityC = _t('_City');
		$sPlaceC = _t('_Place');
		$sVenuePhotoC = _t('_Venue photo');
		$sEventStartC = _t('_Event start');
		$sEventEndC = _t('_Event end');
		$sTicketSaleStartC = _t('_Ticket sale start');
		$sTicketSaleEndC = _t('_Ticket sale end');
		$sFemaleTicketCountC = _t('_Female ticket count');
		$sMaleTicketCountC = _t('_Male ticket count');
		//$sSaveChangesC = _t('_Save Changes');
		$sPostEventC = _t('_Post Event');
		$sAddNewEventC = _t('_Add new event');
		$sTagsC = _t('_Tags');
		$sCommitC = _t('_Apply Changes');
		$sMarkAsFeatured = _t('_Mark as Featured');
		$sPublicC = _t('_public');
		$sFriendsOnlyC = _t('_friends only');

		$createEventPermission = db_value("SELECT `SiteEvents` FROM `Profiles` WHERE `ID`={$aMember['ID']} LIMIT 1");
		
		if ($iEventID>0) {
			$sEventSQL = "SELECT * FROM `SDatingEvents` WHERE `ID` = {$iEventID} LIMIT 1";
			$aEvent = db_arr( $sEventSQL );
			$sEventTitle = $aEvent['Title'];
			$sEventTags = $aEvent['Tags'];
			/*
			$sEventPhoto = $aEvent['PhotoFilename'];
			
			if ($sEventPhoto != '') 
			{
				$sSpacerName = $site['url'].$this -> sSpacerPath;
				$sEventPictureTag = '<div class="marg_both_left"><img alt="" style="width: 110px; height: 110px; background-image: url('.$site['url'] . 'media/images/sdating/thumb_' . $sEventPhoto.');" src="'.$sSpacerName.'"/></div>';
			}
			*/
			$sEventPictureTag = $this->GetEventPicture($iEventID);
			$noEventPicture = (strpos($sEventPictureTag, $this->sPicNotAvail) !== false);
					
			$sEventDesc = $aEvent['Description'];
			$sEventStatusMsg = $aEvent['StatusMessage'];
			$sSelectedCountry = $aEvent['Country'];
			$sSitySrc = $site['flags'].strtolower($sSelectedCountry);
			$sEventCity = $aEvent['City'];
			$sEventPlace = $aEvent['Place'];
			$sEventEnd = $aEvent['EventEnd'];
			$sEventStart = $aEvent['EventStart'];
			$sFemaleTicketCount = $aEvent['TicketCountFemale'];
			$sMaleTicketCount = $aEvent['TicketCountMale'];
			//$sEventTags = $aEvent['Tags'];
			//$sPostPicture = $aEvent['PostPhoto'];
			//if ($sPostImage != '')
			//	$sPostPictureTag = '<img src="'.$site['blogImage'].'big_'.$sPostImage.'" style="position:static;" />';
			$sEditedIdElement = '<input type="hidden" name="EditedEventID" id="EditedEventID" value="'.$iEventID.'" />';
			$sPostEventC = $sCommitC;
			$sPostAction = 'event_updated';
			$sAddNewEventC = _t('_Edit event');
			$sANEventAction = "event_save";
			//$sJSIns = "UpdateField('EditEventID','{$iEventID}');";
			//$sEditIdStr = '<input type="hidden" name="EditedEventID" value="'.$iEventID.'">';

		} else {
			if (!$createEventPermission)
				return MsgBox("You can't create events", 16, 2, 
					"<img src=\"{$site['icons']}lock.gif\" 
					style=\"height:40px; margin-right:5px; vertical-align:middle\"/>");

			$noEventPicture = true;
			$sEventTitle = isset($_POST['event_title']) ? htmlspecialchars( process_pass_data($_POST['event_title']) ) : '';
			$sEventTags = isset($_POST['event_tags']) ? htmlspecialchars( process_pass_data($_POST['event_tags']) ) : '';
			$sEventDesc = isset($_POST['event_desc']) ? htmlspecialchars( process_pass_data($_POST['event_desc']) ) : '';
			$sEventStatusMsg = isset($_POST['event_statusmsg']) ? htmlspecialchars( process_pass_data($_POST['event_statusmsg']) ) : '';
			$sSelectedCountry = isset($_POST['event_country']) ? $_POST['event_country'] : $sMemberCountry;
			$sSitySrc = $site['flags'].strtolower($sSelectedCountry);
			$sEventCity = isset($_POST['event_city']) ? htmlspecialchars( process_pass_data($_POST['event_city']) ) : '';
			$sEventPlace = isset($_POST['event_place']) ? htmlspecialchars( process_pass_data($_POST['event_place']) ) : '';
			$sEventStart = isset($_POST['event_start']) ? htmlspecialchars( process_pass_data($_POST['event_start']) ) : '';
			$sEventEnd = isset($_POST['event_end']) ? htmlspecialchars( process_pass_data($_POST['event_end']) ) : '';
			//$sEventEnd = isset($_POST['event_end']) ? htmlspecialchars( process_pass_data($_POST['event_end']) ) : strftime( str_replace('%i','%M', $date_format) );
			$sFemaleTicketCount = isset($_POST['event_count_female']) ? htmlspecialchars( process_pass_data($_POST['event_count_female']) ) : '';
			$sMaleTicketCount = isset($_POST['event_count_male']) ? htmlspecialchars( process_pass_data($_POST['event_count_male']) ) : '';
			$sPostAction = 'event_save';
			$sANEventAction = "new";
		}

		$sCountriesOptions = '';

		foreach ( $prof['countries'] as $key => $value ) {
			$sSC = ($sSelectedCountry == $key) ? 'selected="selected"' : '';
			$sVal = _t('__'.$value);
			$sCountriesOptions .= "<option value=\"{$key}\" {$sSC}>{$sVal}</option>";
		}

		$sNewResText = ( strlen($new_result_text) ) ? '<div class="err" style="margin: 10px;"><div>'.$new_result_text.'</div></div>' : '';

		$sJSCode = <<<EOF
<script type="text/javascript">
<!--
	function AutoSelectFlag() {
		var vElem = document.getElementById('event_country_id');
		changeFlag(vElem.value);
	}

	function changeFlag(flagISO)
	{
		flagImage = document.getElementById('flagImageId');
		flagImage.src = '{$site['flags']}' + flagISO.toLowerCase() + '.gif';
	}
	
	function CheckJoinOption(publicOption)
	{
		var joinOption = document.getElementById('newEventForm').joinPerm;
		var dis = (publicOption == 'friends');
		for (var i = 0; i < joinOption.length; i++)
			joinOption[i].disabled = dis;
	}
	
	// Set exclusive checkbox disabled depending on featured
	function checkExclusive()
	{
		var featured = document.getElementById('newEventForm').featured.checked;
		document.getElementById('newEventForm').exclusive.disabled = !featured;
	}
//-->
</script>
EOF;

		$sZapatecCalendar = '';
		//if ( $_REQUEST['action'] == 'new' ) {
		$sAdminCalendars = '';

		if ($this->bAdminMode) {
			$sAdminCalendars = $this->GenerateZapatecCode('event_end_id','end_choose_id') .
			$this->GenerateZapatecCode('event_sale_start_id','sale_start_choose_id') .
			$this->GenerateZapatecCode('event_sale_end_id','sale_end_choose_id');
		}

		$sZapatecCalendar = $this->GenerateZapatecCode('event_start_id','start_choose_id') . 
			$this->GenerateZapatecCode('event_end_id','end_choose_id') . $sAdminCalendars;

		//}
		// PHP validating fields
		$sTstyle = ($arrErr['Title'] ? 'block' : 'none');
		$sDstyle = ($arrErr['Description'] ? 'block' : 'none');
		$sSMstyle = ($arrErr['Status message'] ? 'block' : 'none');
		$sCstyle = ($arrErr['City'] ? 'block' : 'none');
		$sPstyle = ($arrErr['Place'] ? 'block' : 'none');
		$sESstyle = ($arrErr['Event start'] ? 'block' : 'none');
		$sEEstyle = ($arrErr['Event end'] ? 'block' : 'none');
		$sTSSstyle = ($arrErr['Ticket Sale Start'] ? 'block' : 'none');
		$sTSEstyle = ($arrErr['Ticket Sale End'] ? 'block' : 'none');
		$sFTCstyle = ($arrErr['Female Ticket Count'] ? 'block' : 'none');
		$sMTCstyle = ($arrErr['Male Ticket Count'] ? 'block' : 'none');

		// Error messages
		$sTmsg = ($arrErr['Title'] ? _t( '_'.$arrErr['Title'] ) : '' );
		$sDmsg = ($arrErr['Description'] ? _t( '_'.$arrErr['Description'] ) : '' );
		$sSMmsg = ($arrErr['Status message'] ? _t( '_'.$arrErr['Status message'] ) : '' );
		$sCmsg = ($arrErr['City'] ? _t( '_'.$arrErr['City'] ) : '' );
		$sPmsg = ($arrErr['Place'] ? _t( '_'.$arrErr['Place'] ) : '' );
		$sESmsg = ($arrErr['Event start'] ? _t( '_'.$arrErr['Event start'] ) : '' );
		$sEEmsg = ($arrErr['Event end'] ? _t( '_'.$arrErr['Event end'] ) : '' );
		$sTSSmsg = ($arrErr['Ticket Sale Start'] ? _t( '_'.$arrErr['Ticket Sale Start'] ) : '' );
		$sTSEmsg = ($arrErr['Ticket Sale End'] ? _t( '_'.$arrErr['Ticket Sale End'] ) : '' );
		$sFTCmsg = ($arrErr['Female Ticket Count'] ? _t( '_'.$arrErr['Female Ticket Count'] ) : '' );
		$sMTCmsg = ($arrErr['Male Ticket Count'] ? _t( '_'.$arrErr['Male Ticket Count'] ) : '' );

		$sAdminSalesPart='';
		$sStatusMess='';

		if ($this->bAdminMode) {
			$sStatusMess = <<<EOF
<tr class="vc">
	<td class="form_label">{$sStatusMessageC}:</td>
	<td class="form_value">
		<div class="edit_error" style="display:{$sSMstyle}">
			{$sSMmsg}
		</div>
		<input class="form_input" type="text" name="event_statusmsg" id="event_statusmsg_id" value="{$sEventStatusMsg}" />
	</td>
</tr>
EOF;
			$featuredEventChecked = ($aEvent['Featured'] == '1') ? ' checked' : '';
			$exclusiveEventChecked = ($aEvent['exclusive']) ? ' checked' : '';

			$sAdminSalesPart = <<<EOF
<tr class="vc">
	<td class="form_label">{$sTicketSaleStartC}:</td>
	<td class="form_value">
		<div class="edit_error" style="display:{$sTSSstyle}">
			{$sTSSmsg}
		</div>
		<input type="text" class="form_input_date" name="event_sale_start" id="event_sale_start_id" value="" />
		<input type="button" id="sale_start_choose_id" value="Choose" />
		<input type="button" id="sale_start_clear_id" onClick="document.getElementById('event_sale_start_id').value = ''; " value="Clear" />
	</td>
</tr>
<tr class="vc">
	<td class="form_label">{$sTicketSaleEndC}:</td>
	<td class="form_value">
		<div class="edit_error" style="display:{$sTSEstyle}">
			{$sTSEmsg}
		</div>
		<input type="text" class="form_input_date" name="event_sale_end" id="event_sale_end_id" value="" />
		<input type="button" id="sale_end_choose_id" value="Choose" />
		<input type="button" id="sale_end_clear_id" onClick="document.getElementById('event_sale_end_id').value = ''; " value="Clear" />
	</td>
</tr>
<tr class="vc">
	<td class="form_label">{$sFemaleTicketCountC}:</td>
	<td class="form_value">
		<div class="edit_error" style="display:{$sFTCstyle}">
			{$sFTCmsg}
		</div>
		<input type="text" class="form_input_count" name="event_count_female" id="event_count_female_id" value="{$sFemaleTicketCount}" />
	</td>
</tr>
<tr class="vc">
	<td class="form_label">{$sMaleTicketCountC}:</td>
	<td class="form_value">
		<div class="edit_error" style="display:{$sMTCstyle}">
			{$sMTCmsg}
		</div>
		<input type="text" class="form_input_count" name="event_count_male" id="event_count_male_id" value="{$sMaleTicketCount}" />
	</td>
</tr>
<tr class="vc">
	<td class="form_label" style="vertical-align:middle">{$sMarkAsFeatured}:</td>
	<td class="form_value">
		<input type="checkbox" name="featured" $featuredEventChecked onclick="checkExclusive();"/>
		<span style="vertical-align:middle; margin-left:50px">YH Exclusive Event: 
			<input type="checkbox" name="exclusive" $exclusiveEventChecked/></span>
	</td>
</tr>
EOF;

}

$siteEventsCreator = ($createEventPermission == 2);
$isSiteLevel = db_value("SELECT `EventLevel` FROM `SDatingEvents` WHERE `ID`=$iEventID LIMIT 1");
$siteLevelChecked = ($isSiteLevel == '1') ? 'checked' : '';
$withRSSImage = ($iEventID > 0) ?
	db_value("SELECT `RSSImage` FROM `SDatingEvents` WHERE `ID`=$iEventID LIMIT 1") : 0;
if ($this->bAdminMode || $siteEventsCreator)
{
	if ($withRSSImage)
		$rssImageOptions = <<<EOF
	<tr class="vc">
		<td class="form_label"</td>
		<td class="form_value">
			<div class="imageOptions">
				<input type="radio" checked="1" value="keep" name="rssImageAction"/> Keep image
				<input type="radio" value="update" name="rssImageAction"/> Update image
				<input type="radio" value="clear" name="rssImageAction"/> Clear image
			</div>
		</td>
	</tr>	
EOF;
	else
		$rssImageOptions = '';
	
	$SiteLevelSect = <<<EOF
<tr class="vc">
	<td class="form_label" style="vertical-align:middle">Mark as Global:</td>
	<td class="form_value">
		<div style="float:left; margin-top:16px">
			<input type="checkbox" name="siteLevel" $siteLevelChecked/>
		</div>
		<div style="padding-left:10px; float:left">
			<div style="font-size:10px; font-weight:bold; margin-bottom:5px">
				Upload YH Home Page Thumbnail
			</div>
			<input type="file" name="rssimage" accept="image/jpeg"/><br/>
			<span style="font-size:10px">(Only .jpg)</span>
		</div>
	</td>
</tr>	
$rssImageOptions
EOF;
}

$showAdvancedTextEditor = true;
if ($showAdvancedTextEditor)
	$textEditorCode = 
		'<script src="inc/rte/js/richtext.js" type="text/javascript" language="javascript"></script>
		<script src="inc/rte/js/config-event.js" type="text/javascript" language="javascript"></script>
		<script>
			initRTE("'. jsAddSlashes($sEventDesc) .'");
		</script>';
else 
	$textEditorCode = "<textarea class='classfiedsTextArea' name='event_desc' id='event_desc_id' 
		style='width:500px;'>{$sEventDesc}</textarea>";

if ($noEventPicture)
	$imageSection = '<td class="form_value"><input type="file" class="form_file" name="event_photo" id="event_photo_id" /></td>';
else 
/*
	$imageSection = '<td><table style="border:1px #4682B4 solid">
				<tr style="vertical-align:top">
				<td><input type="radio" name="imageAction" value="keep" checked/>Keep image<br/><br/>'.$sEventPictureTag.'</td>
				<td><input type="radio" name="imageAction" value="update"/>Update image<br/><br/>
					<div class="margin_bottom_10"><input type="file" name="event_photo" id="event_photo_id"></div>
				</td>
				<td><input type="radio" name="imageAction" value="clear"/>Clear image</td></tr>
				</table></td>';
				*/
	$imageSection = "<td><table><tr><td>$sEventPictureTag</td>
		<td style=\"vertical-align:top\"><a href=\"{$site['url']}event_photos_gallery.php?eventID=$iEventID\">more photo(s)</a></td></tr></table></td>";
	
if ($iEventID > 0 && $aEvent['ReadPermission'] == 'friends')
{
	$sCheckedReadPermP = '';
	$sCheckedReadPermF = 'checked';
	$readPerm = 'friends';
}
else 
{
	$sCheckedReadPermP = 'checked';
	$sCheckedReadPermF = '';	
}

if ($iEventID > 0 && $aEvent['JoinPermission'] == 'friends')
{
	$sCheckedJoinPermP = '';
	$sCheckedJoinPermF = 'checked';
}
else 
{
	$sCheckedJoinPermP = 'checked';
	$sCheckedJoinPermF = '';	
}
	
$sRetHtml = <<<EOF

{$sJSCode}

{$sNewResText}

<form id="newEventForm" name="newEventForm" action="{$_SERVER['PHP_SELF']}" method="post" enctype="multipart/form-data">

<table class="addEventForm">

	<tr class="vc">

		<td class="form_label">{$sTitleC}:</td>

		<td class="form_value">

			<div class="edit_error" style="display:{$sTstyle}">

				{$sTmsg}

			</div>

			<input class="form_input" type="text" name="event_title" id="event_title_id" value="{$sEventTitle}" />

		</td>

	</tr>

	<tr class="vc">

		<td class="form_label">{$sTagsC}:</td>

		<td class="form_value">

			<input class="form_input" type="text" name="event_tags" value="{$sEventTags}" />

		</td>

	</tr>

	<tr class="vc">

		<td class="form_label">{$sDescriptionC}:</td>

		<td class="form_value">

			<div class="edit_error" style="display:{$sDstyle}">

				{$sDmsg}

			</div>

			{$textEditorCode}

		</td>

	</tr>

	{$sStatusMess}

	<tr class="vc">

		<td class="form_label">{$sCountryC}:</td>

		<td class="form_value">

			<select class="form_select" name="event_country" id="event_country_id" onchange="javascript: changeFlag(this.value);">

				{$sCountriesOptions}

			</select>

			<img id="flagImageId" src="{$sSitySrc}.gif" alt="flag" />

		</td>

	</tr>

	<tr class="vc">

		<td class="form_label">{$sCityC}:</td>

		<td align="left">

			<div class="edit_error" style="display:{$sCstyle}">

				{$sCmsg}

			</div>

			<input type="text" class="form_input" name="event_city" id="event_city_id" value="{$sEventCity}" />

		</td>

	</tr>

	<tr class="vc">

		<td class="form_label">{$sPlaceC}:</td>

		<td class="form_value">

			<div class="edit_error" style="display:{$sPstyle}">

				{$sPmsg}

			</div>

			<input type="text" class="form_input" name="event_place" id="event_place_id" value="{$sEventPlace}" />

		</td>

	</tr>

	<tr class="vc">

		<td class="form_label">{$sVenuePhotoC}:</td>
		
		{$imageSection}

	</tr>

	<tr class="vc">

		<td class="form_label">{$sEventStartC}:</td>

		<td class="form_value">

			<div class="edit_error" style="display:{$sESstyle}">

				{$sESmsg}

			</div>

			<input type="text" class="form_input_date" name="event_start" id="event_start_id" value="{$sEventStart}" />

			<input type="button" id="start_choose_id" value="Choose" />

			<input type="button" id="start_clear_id" onClick="document.getElementById('event_start_id').value = ''; " value="Clear" />

		</td>

	</tr>
	
	<tr class="vc">

	<td class="form_label">{$sEventEndC}:</td>

	<td class="form_value">

		<div class="edit_error" style="display:{$sEEstyle}">

			{$sEEmsg}

		</div>

		<input type="text" class="form_input_date" name="event_end" id="event_end_id" value="{$sEventEnd}" />

		<input type="button" id="end_choose_id" value="Choose" />

		<input type="button" id="end_clear_id" onClick="document.getElementById('event_end_id').value = ''; " value="Clear" />

	</td>

	</tr>
	
	<tr class="vc">
		<td colspan="2">
		<div style="margin:15px 0px 15px 42px">
			<div style="float:left; position:relative; width:50%">
				<div class="permTitle">Event Visible to:</div>
				<div class="permValues">
					<input type="radio" {$sCheckedReadPermP} name="readPerm" 
						value="public" onclick="CheckJoinOption(this.value)"/> $sPublicC<br />
					<input type="radio" {$sCheckedReadPermF} name="readPerm" 
						value="friends" onclick="CheckJoinOption(this.value)"/> $sFriendsOnlyC
				</div>
			</div>
			<div style="float:left; position:relative; padding-left:30px; border-left:1px solid #CCCCCC">
				<div class="permTitle">Who Can Join Event:</div>
				<div class="permValues">
					<input type="radio" {$sCheckedJoinPermP} name="joinPerm" value="public"/> $sPublicC<br />
					<input type="radio" {$sCheckedJoinPermF} name="joinPerm" value="friends"/>	$sFriendsOnlyC
				</div>
			</div>
			<div class="clear_both"></div>
		</div>
		</td>	
	</tr>
	
	{$sAdminSalesPart}
	
	{$SiteLevelSect}

	<tr class="vc">

		<td class="form_colspan" colspan="2">

			<input type="hidden" name="action" value="{$sANEventAction}" />

			{$sEditedIdElement}

			<input type="submit" class="form_submit" name="{$sPostAction}" value="{$sPostEventC}"

			  style="width: 100px; vertical-align: middle; margin-top:10px" />

		</td>

	</tr>

</table>

</form>

<!-- Loading Calendar JavaScript files -->

    <script type="text/javascript" src="{$site['plugins']}calendar/calendar_src/utils.js"></script>

    <script type="text/javascript" src="{$site['plugins']}calendar/calendar_src/calendar.js"></script>

    <script type="text/javascript" src="{$site['plugins']}calendar/calendar_src/calendar-setup.js"></script>



<!-- Loading language definition file -->

    <script type="text/javascript" src="{$site['plugins']}calendar/calendar_lang/calendar-en.js"></script>



<script type="text/javascript">
	var readPerm = '{$readPerm}';
	CheckJoinOption(readPerm);
//<![CDATA[

	{$sZapatecCalendar}

//]]>

document.onload=AutoSelectFlag();

</script>

EOF;

if ($this->bAdminMode)
	$sRetHtml .= '<script type="text/javascript">checkExclusive();</script>';

return DesignBoxContent( $sAddNewEventC, $sRetHtml, $oTemplConfig -> PageSDatingNewEvent_db_num );

	}



	function ShowSearchResult() {

		$sRetHtml = '';

		$sSearchedTag = process_db_input( $_REQUEST['tagKey'] );

		//$iMemberID = $_REQUEST['ownerID'];

		global $site;

		global $prof;

		global $tmpl;

		global $dir;



		$date_format_php = getParam('php_date_format');

		$sTagsC = _t('_Tags');

		$sTagC = _t('_Tag');

		$sShowInfoC = _t('_Show info');

		$sParticipantsC = _t('_Participants');

		$sStatusMessageC = _t('_Status message');

		$sStartDateC = _t('_StartDate');

		$sPlaceC = _t('_Place');

		$sDescriptionC = _t('_Description');

		$sTitleC = _t('_Title');

		$sActionsC = _t('_Actions');

		$sListOfParticipantsC = _t('_List').' '._t('_of').' '._t('_Participants');



		$sSpacerName = $site['url'].$this -> sSpacerPath;



		//$sCategoryAddon = ($iCategoryID>0) ? "AND `BlogPosts`.`CategoryID` = {$iCategoryID}" : '';

		$sBlogPosts = '';

		$sEventSQL = "SELECT * FROM `SDatingEvents`";

		$vBlogPosts = db_res( $sEventSQL );

		$tagHrefTmpl = (useSEF) ? "{$site['url']}events/tagKey=%tag%" 
			: "{$site['url']}events.php?action=search_by_tag&amp;tagKey=%tag%";
		
		while ( $aResSQL = mysql_fetch_assoc($vBlogPosts) ) {

			$sDateTime = date( $date_format_php, strtotime( $aResSQL['EventStart'] ) );



			$sCountry = ($aResSQL['Country']!='') ? _t('__'. $prof['countries'][$aResSQL['Country']]) : '';

			$sCity = ($aResSQL['City']!='') ? ', '.process_line_output($aResSQL['City']) : '';

			$sPlace = ($aResSQL['Place']!='') ? ', '.process_line_output($aResSQL['Place']) : '';

			$sDescription = /*process_text_withlinks_output*/($aResSQL['Description']);



			$sImgEL = ( strlen(trim($aResSQL['PhotoFilename'])) && file_exists($dir['sdatingImage'] . $aResSQL['PhotoFilename']) )

			? "<img class=\"photo1\" style=\"width:{$this->iThumbSize}px;height:{$this->iThumbSize}px;background-image:url({$site['sdatingImage']}thumb_{$aResSQL['PhotoFilename']});\" src=\"{$sSpacerName}\" />"

			: "<img class=\"photo1\" style=\"width:{$this->iThumbSize}px;height:{$this->iThumbSize}px;background-image:url({$site['url']}templates/tmpl_{$tmpl}/{$this->sPicNotAvail});\" src=\"{$sSpacerName}\" />";



			$sTagsCommas = $aResSQL['Tags'];

			$aTags = split(',', $sTagsCommas);



			$sTagsHrefs = '';

			foreach( $aTags as $sTagKey ) {
				$tagHref = str_replace('%tag%', $sTagKey, $tagHrefTmpl);
				$sTagsHrefs .= <<<EOF

<a href="$tagHref" >{$sTagKey}</a>&nbsp;

EOF;

}

// $sActions = '';

// if ($this->aBlogConf['visitorID']==$aBlogsRes['OwnerID'] || $this->bAdminMode==TRUE) {

// $sJSPostText = addslashes($aResSQL['PostText']);

///// <a href="{$_SERVER['PHP_SELF']}" onclick="UpdateField('EditBlogPostID','{$aResSQL['PostID']}');UpdateField('PostText','{$sJSPostText}');document.getElementById('edit_blog_post_div').style.display = 'block';return false;" style="text-transform:none;">{$sEditC}</a>&nbsp;



// $sActions = <<<EOF

// <div class="fr">

// <a href="{$_SERVER['PHP_SELF']}" onclick="UpdateField('EditPostID','{$aResSQL['PostID']}');document.forms.command_edit_post.submit();return false;" style="text-transform:none;">{$sEditC}</a>&nbsp;

// <a href="{$_SERVER['PHP_SELF']}" onclick="if (confirm('{$sSureC}')) {UpdateField('DeletePostID','{$aResSQL['PostID']}');document.forms.command_delete_post.submit(); } return false;" style="text-transform:none;">{$sDeleteC}</a>

// </div>

// EOF;

// }



if (in_array($sSearchedTag,$aTags)) {

	$sBlogPosts .= <<<EOF

<div class="cls_result_row">

	<div  class="thumbnail_block" style="float:left;">

		<a href="{$_SERVER['PHP_SELF']}?action=show_info&amp;event_id={$aResSQL['ID']}">

			{$sImgEL}

		</a>

	</div>

	<div class="cls_res_info_nowidth" {$sDataStyleWidth}>

		<div class="cls_res_info_p">

			<a class="actions" href="{$_SERVER['PHP_SELF']}?action=show_info&amp;event_id={$aResSQL['ID']}">{$aResSQL['Title']}</a>

		</div>

		<div class="cls_res_info_p">

			<!-- <span style="vertical-align:middle;">

				<img src="{$site['icons']}tag_small.png" class="marg_icon" alt="" />

			</span>-->{$sTagsC}:&nbsp;{$sTagsHrefs}

		</div>

		<!-- <div class="cls_res_info_p">

			{$sStatusMessageC}: <div class="clr3">{$sStatusMessage}</div>

		</div> -->

		<div class="cls_res_info_p">

			{$sStartDateC}: <div class="clr3">{$sDateTime}</div>

		</div>

		<div class="cls_res_info_p">

			{$sPlaceC}: <div class="clr3">{$sCountry}{$sCity}{$sPlace}</div>

		</div>

		<div class="cls_res_info_p">

			{$sDescriptionC}: <div class="clr3">{$sDescription}</div>

		</div>

		<div class="cls_res_info_p">

			{$sViewParticipants}

		</div>

		{$sActions}

	</div>

	<div class="clear_both"></div>

</div>

EOF;

}

		}



		return $this->DecorateAsTable($sTagC.' - '.$sSearchedTag, $sBlogPosts);

	}



	/**

	 * SQL Get all Profiles data by Profile Id

	 *

	  * @param $iProfileId

	 * @return SQL data

	 */

	function GetProfileData($iProfileId) {

		return getProfileInfo( $iProfileId );

	}

	function GetEventPicture($iEventID, $sEventPicName='DOLPHIN', $imgType='icon', $isAllEventsPage=false) {
		
		global $dir, $site, $tmpl;

		$sSpacerName = $site['url'].$this -> sSpacerPath;

		$sRequest = "SELECT `PhotoFilename`, `Title`, `ResponsibleID`, `media`.med_file, urltitle
			FROM `SDatingEvents` 
			LEFT JOIN `media` ON `media`.med_id = `SDatingEvents`.PrimPhoto
			WHERE `ID` = {$iEventID} LIMIT 1";
		$aResPic = db_arr($sRequest);

		if ($aResPic['med_file'] != '')
		{
			$sAlt = $aResPic['med_file'];		
			$dirEventImage = "{$dir['profileImage']}{$aResPic['ResponsibleID']}/{$imgType}_{$aResPic['med_file']}";
			$urlEventImage = "{$site['profileImage']}{$aResPic['ResponsibleID']}/{$imgType}_{$aResPic['med_file']}";
			$eventImageExists = true;
		}
		elseif ($aResPic['PhotoFilename'] != '')
		{
			$sAlt = $aResPic['PhotoFilename'];
			$dirEventImage = "{$dir['sdatingImage']}{$imgType}_{$aResPic['PhotoFilename']}";
			$urlEventImage = "{$site['sdatingImage']}{$imgType}_{$aResPic['PhotoFilename']}";
			$eventImageExists = true;
		}
		else 
			$eventImageExists = false;

		if ($isAllEventsPage)
		{
			$width = 59;
			$height = 45;
		}
		else 
		{
			$width = $this->iIconSize;
			$height = $this->iIconSize;
		}
			
		$sEventPicName = ($eventImageExists && file_exists($dirEventImage))
			//? "<img  style=\"width:{$width}px; height:{$height}px; background-position:center;
				//background-image:url($urlEventImage);\" src=\"{$sSpacerName}\" alt=\"{$sAlt}\"/>"
			? "<img  style=\"width:{$width}px;\" src=\"$urlEventImage\" alt=\"{$sAlt}\"/>"
			: "<img  style=\"width:{$width}px; height:{$height}px;
				background-image:url({$site['url']}templates/tmpl_{$tmpl}/{$this->sPicNotAvail});\" src=\"{$sSpacerName}\" alt=\"{$sAlt}\"/>";

		if ($aResPic['urltitle'] && useSEF)	
			$sEventPic = "<a href=\"{$site['url']}".getNickName($aResPic['ResponsibleID']).
				"/event/{$aResPic['urltitle']}.html\">{$sEventPicName}</a>";
		else		
			$sEventPic = "<a href=\"events.php?action=show_info&amp;event_id={$iEventID}\">{$sEventPicName}</a>";
		return $sEventPic;
	}

	
	function GetGroupPicture($iGroupID) {

		global $dir;

		global $site;

		global $tmpl;

		$sSpacerName = $site['url'].$this -> sSpacerPath;

		$sRequest = "SELECT `thumb` FROM `Groups` WHERE `ID` = {$iGroupID} LIMIT 1";

		$aResPic = db_arr($sRequest);

		$iGroupPicID = (int)$aResPic['thumb'];

		$sRequest = "SELECT * FROM `GroupsGallery` WHERE `ID` = {$iGroupPicID}";

		$aResPicName = db_arr($sRequest);

		$sPicName = $aResPicName['groupID'].'_'.$aResPicName['ID'].'_'.$aResPicName['seed'].'_.'.$aResPicName['ext'];

		$this->iThumbSize = 45;

		$sEventPicName = $sPicName;

		//$sTypePic = "icon_";



		$sEventPicName = ( strlen(trim($sEventPicName)) && file_exists('groups/gallery/' . $sEventPicName) )

		? "<img class=\"photo1\" style=\"width:{$this->iThumbSize}px;height:{$this->iThumbSize}px;background-image:url(groups/gallery/{$sTypePic}{$sEventPicName});\" src=\"{$sSpacerName}\" />"

		: "<img class=\"photo1\" style=\"width:{$this->iThumbSize}px;height:{$this->iThumbSize}px;background-image:url({$site['url']}templates/tmpl_{$tmpl}/{$this->sPicNotAvail});\" src=\"{$sSpacerName}\" />";

		$sEventPic = <<<EOF

<div  class="thumbnail_block" style="float:left;">

	<a href="group.php?ID={$iGroupID}">

		{$sEventPicName}

	</a>

</div>

EOF;

		return $sEventPic;

	}



	function process_html_db_input( $sText ) {

		return addslashes( clear_xss( trim( process_pass_data( $sText ))));

	}
	
	/**
	 * Adding a Comment to Event
	 *
	 * @return MsgBox result
	 */
	function ActionAddComment() {
		$this->CheckLogged();

		$eventID = (int)$_POST['CommEventID'];
		$senderID = (int)$_COOKIE['memberID'];
		$commentText = process_db_input(clear_xss(trim(process_pass_data($_POST['commentText']))));
		$replyTo = (int)$_POST['replyTo'];
		$ip = getVisitorIP(); // ( getenv('HTTP_CLIENT_IP') ? getenv('HTTP_CLIENT_IP') : getenv('REMOTE_ADDR') );
		
		if ( !$ip ) {
			$ret = _t_err("_sorry, i can not define you ip adress. IT'S TIME TO COME OUT !");
			return $ret;
		}

		if( 0 >= $senderID ) {
			return _t_err('_im_textLogin');
		}

		if( 0 >= $eventID ) return '';

		$last_count = db_arr( "SELECT COUNT( * ) AS `last_count` FROM `SDatingEventComments` WHERE `IP` = '{$ip}' AND (UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(`Date`) < 1*60)" );
		if ( $last_count['last_count'] != 0 ) {
			$ret = _t("_You have to wait for PERIOD minutes before you can write another message!", 1);
			return MsgBox($ret);
		}

		$addCommentQuery = "
			INSERT INTO `SDatingEventComments`
			SET
				`EventID` = $eventID,
				`SenderID` = $senderID,
				`CommentText` = '$commentText',
				`ReplyTo` = $replyTo,
				`IP` = '$ip',
				`Date` = NOW()
		";

		if( db_res( $addCommentQuery ) ) {
			$ret = _t('_comment_added_successfully');
			$this->TrackComment($eventID, $senderID, $commentText, mysql_insert_id());
		}
		else
			$ret = _t('_failed_to_add_comment');

		return MsgBox($ret);
	}
	
	function TrackComment($eventID, $senderID, $commentText, $commentId)
	{
		// Get event title and owner
		$eventDataQuery = "SELECT Title, ResponsibleID FROM SDatingEvents WHERE ID=$eventID LIMIT 1";
		$eventData = db_arr($eventDataQuery);
		$eventTitle = ($eventData['Title'] == '') ? 'Untitled' : process_db_input(trim($eventData['Title']));
	
		$owner = $eventData['ResponsibleID'];
		if (!$owner) return;
		
		TrackNewAction(11, $eventID, $senderID, $eventTitle, $commentText, '', $owner, '', false, $commentId);
	}
	
	/**
	 * Editing a Comment to Event
	 *
	 * @return MsgBox result
	 */
	function ActionEditComment(){
		$iCommId = (int)$_REQUEST['EditCommentID'];
		$sCheckSQL = "SELECT `SenderID`,`EventID` FROM `SDatingEventComments` WHERE `CommentID`={$iCommId}";
		$aEventData = db_arr($sCheckSQL);
		$iSenderID = $aEventData['SenderID'];
		$iEventID = $aEventData['EventID'];
		$sCheckPostSQL = "SELECT `ResponsibleID`
							FROM `SDatingEvents`
							WHERE `ID` = $iEventID LIMIT 1
						";
		$iPostOwnerID = db_value($sCheckPostSQL);
		$visitorID = (int)$_COOKIE['memberID'];
		if (( $visitorID == $iPostOwnerID || $visitorID == $iSenderID) && $iCommId > 0) {
			$sMessage = addslashes( clear_xss( process_pass_data( $_POST['commentText'] ) ) );
			$query = "UPDATE `SDatingEventComments` SET `CommentText` = '{$sMessage}' 
				WHERE `CommentID` = {$iCommId} LIMIT 1 ;";
			$sqlRes = db_res( $query );
		} 
		elseif($visitorID != $iSenderID || $visitorID != $iPostOwnerID) {
			return MsgBox(_t('_Hacker String'));
		} else {
			return MsgBox(_t('_Error Occured'));
		}
	}
	
	/**
	 * Deleting a Comment to Event
	 *
	 * @return MsgBox result
	 */
	function ActionDeleteComment(){
		$iCommId = (int)$_REQUEST['DeleteCommentID'];
		$sCheckSQL = "SELECT `SenderID`,`EventID` FROM `SDatingEventComments` WHERE `CommentID`={$iCommId}";
		$aEventData = db_arr($sCheckSQL);
		$iSenderID = $aEventData['SenderID'];
		$iEventID = $aEventData['EventID'];
		$sCheckPostSQL = "SELECT `ResponsibleID`
							FROM `SDatingEvents`
							WHERE `ID` = $iEventID LIMIT 1
						";
		$iPostOwnerID = db_value($sCheckPostSQL);
		$visitorID = (int)$_COOKIE['memberID'];
		if (( $visitorID == $iPostOwnerID || $visitorID == $iSenderID) && $iCommId > 0)
		{
			$query = "DELETE FROM `SDatingEventComments` WHERE `CommentID` = {$iCommId} LIMIT 1";
			$sqlRes = db_res( $query );
			
			// Delete from NewsFeed table
			db_res("DELETE FROM `NewsFeedTrack` WHERE `type`=11 AND `commentid`=$iCommId LIMIT 1");
		} 
		elseif($visitorID != $iSenderID || $visitorID != $iPostOwnerID)
			return MsgBox(_t('_Hacker String'));
		else
			return MsgBox(_t('_Error Occured'));
	}

}

function EventsCompare($evRow1, $evRow2)
{
	if ($evRow1['EventNotFinished'] && $evRow2['EventNotFinished'])
	{
		if ($evRow1['sec'] == $evRow2['sec'])
			return 0;
		else 
			return ($evRow1['sec'] > $evRow2['sec']) ? -1 : 1;
	}
	elseif (!$evRow1['EventNotFinished'] && !$evRow2['EventNotFinished'])
	{
		if ($evRow1['secAgo'] == $evRow2['secAgo'])
			return 0;
		else 
			return ($evRow1['secAgo'] > $evRow2['secAgo']) ? 1 : -1;		
	}
	else
		return ($evRow1['EventNotFinished']) ? -1 : 1;
}