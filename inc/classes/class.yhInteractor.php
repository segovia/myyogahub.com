<?php

class yhInteractor
{
	private $K = 'asd;lfoweir2324sdfsdf';
	private $cacheInboxUnreadLifetime = 10800; // 3 hours
	private $cacheInboxUnreadPrefix = 'myh-inbox-';
	private $cacheRequestsLifetime = 10800; // 3 hours
	private $cacheRequestsPrefix = 'myh-requests-';
	private $cacheSmallAvatarLifetime = 10800; // 3 hours
	private $cacheSmallAvatarPrefix = 'myh-smallavatar-';
	private $cacheMiddleAvatarLifetime = 10800; // 3 hours
	private $cacheMiddleAvatarPrefix = 'myh-middleavatar-';
	private $cacheFeaturedEventsLifetime = 43200; // 12 hours
	private $cacheFeaturedEventsName = 'myh-featuredevents';
	
	function getInboxUnread($globalid, $useCache = true)
	{
		global $site, $memcacher;

		// Using Memcache to load Inbox from MYH
		$memcache = $memcacher->obj();
	
		// Record in Memcache for this globalid
		if ($useCache)
		{
			$cachedInboxNumber = $memcache->get($this->cacheInboxUnreadPrefix . $globalid);
			if (is_numeric($cachedInboxNumber))
				return $cachedInboxNumber;
		}

		$getInboxScriptPath = "{$site['url']}profiles/get-unread-inbox.php";
		$hash = md5(strtoupper($this->K) . $globalid . $this->K);
		$inboxNumber = file_get_contents("$getInboxScriptPath?globalid=$globalid&hash=$hash");
		
		// Save inbox number to Memcache
		$memcache->set($this->cacheInboxUnreadPrefix . $globalid, $inboxNumber,	0, $this->cacheInboxUnreadLifetime);
		
		return (int)$inboxNumber;
	}
	
	function setInboxNumber($globalid, $newInboxNumber)
	{
		global $memcacher;
		
		$memcache = $memcacher->obj();
		$memcache->set($this->cacheInboxUnreadPrefix . $globalid, $newInboxNumber, 0, $this->cacheInboxUnreadLifetime);
	}
	
	function getRequests($globalid, $useCache = true)
	{
		global $site, $memcacher;

		$memcache = $memcacher->obj();
	
		if ($useCache)
		{
			$cachedRequestsNumber = $memcache->get($this->cacheRequestsPrefix . $globalid);
			if (is_numeric($cachedRequestsNumber))
				return $cachedRequestsNumber;
		}

		$getRequestsScriptPath = "{$site['url']}profiles/get-requests.php";
		$hash = md5(strtoupper($this->K) . $globalid . $this->K);
		$requestsNumber = file_get_contents("$getRequestsScriptPath?globalid=$globalid&hash=$hash");
		
		$memcache->set($this->cacheRequestsPrefix . $globalid, $requestsNumber,	0, $this->cacheRequestsLifetime);
		
		return (int)$requestsNumber;
	}	
	
	function setRequestsNumber($globalid, $newRequestsNumber)
	{
		global $memcacher;
		
		$memcache = $memcacher->obj();
		$memcache->set($this->cacheRequestsPrefix . $globalid, $newRequestsNumber, 0, $this->cacheRequestsLifetime);
	}
	
	function getSmallAvatar($globalid, $useCache = true)
	{
		global $site, $memcacher;

		$memcache = $memcacher->obj();
	
		if ($useCache)
		{
			$cachedSmallAvatar = $memcache->get($this->cacheSmallAvatarPrefix . $globalid);
			if ($cachedSmallAvatar)
				return $cachedSmallAvatar;
		}

		$getSmallAvatarScriptPath = "{$site['url']}profiles/get-small-avatar.php";
		$hash = md5(strtoupper($this->K) . $globalid . $this->K);
		$smallAvatar = file_get_contents("$getSmallAvatarScriptPath?globalid=$globalid&hash=$hash");
		
		$memcache->set($this->cacheSmallAvatarPrefix . $globalid, $smallAvatar,	0, $this->cacheSmallAvatarLifetime);
		
		return $smallAvatar;
	}	
	
	function setSmallAvatar($globalid, $newSmallAvatar)
	{
		global $memcacher;
		
		$memcache = $memcacher->obj();
		$memcache->set($this->cacheSmallAvatarPrefix . $globalid, $newSmallAvatar, 0, $this->cacheSmallAvatarLifetime);
	}
	
	function getMiddleAvatar($globalid, $useCache = true)
	{
		global $site, $memcacher;

		$memcache = $memcacher->obj();
	
		if ($useCache)
		{
			$cachedMiddleAvatar = $memcache->get($this->cacheMiddleAvatarPrefix . $globalid);
			if ($cachedMiddleAvatar)
				return $cachedMiddleAvatar;
		}

		$getMiddleAvatarScriptPath = "{$site['url']}profiles/get-middle-avatar.php";
		$hash = md5(strtoupper($this->K) . $globalid . $this->K);
		$middleAvatar = file_get_contents("$getMiddleAvatarScriptPath?globalid=$globalid&hash=$hash");
		
		$memcache->set($this->cacheMiddleAvatarPrefix . $globalid, $middleAvatar, 0, $this->cacheMiddleAvatarLifetime);
		
		return $middleAvatar;
	}	
	
	function setMiddleAvatar($globalid, $newMiddleAvatar)
	{
		global $memcacher;
		
		$memcache = $memcacher->obj();
		$memcache->set($this->cacheMiddleAvatarPrefix . $globalid, $newMiddleAvatar, 0, $this->cacheMiddleAvatarLifetime);
	}	
	
	function getFeaturedEvents($useCache = true)
	{
		global $site, $memcacher;
		
		$maxEventsToSave = 4;
		
		$memcache = $memcacher->obj();
		if ($useCache)
		{
			$cachedFeaturedEvents = $memcache->get($this->cacheFeaturedEventsName);	
			if ($cachedFeaturedEvents)
				return $cachedFeaturedEvents;
		}

		$featuredEvents = array();
		$featuredEventsFeedUrl = "{$site['url']}eventfeeds.php?type=rss&events=featured";		
			
		// Parse featured events feed into array
		include_once('./simplepie/simplepie.inc');
		include_once('./simplepie/idn/idna_convert.class.php');	
		
		$feed = new SimplePie();
		$feed->set_feed_url($featuredEventsFeedUrl);
		$feed->enable_cache(false);	
		$feed->init();		
		$feed->handle_content_type();
		$items = $feed->get_items();
		
		$i = 0;
		foreach($items as $item)
		{
			$featuredEvent = array();
			$featuredEvent['title'] = $item->get_title();
			$featuredEvent['link'] = $item->get_link();
			$exclusiveEvent = ($item->data['child']['']['comments'][0]['data'] == 'Exclusive');
			$imgLink = $item->get_enclosure();
			if ($imgLink && (!array_key_exists('eventImage', $featuredEvents) || $exclusiveEvent))
			{
				$featuredEvents['eventImage'] = $imgLink->get_link();
				$featuredEvents['eventImageLink'] = $featuredEvent['link'];
			}
			$featuredEvents[] = $featuredEvent;
			$i++;
			if ($i == $maxEventsToSave) break;
		}
		$featuredEvents = serialize($featuredEvents);
	
		$memcache->set($this->cacheFeaturedEventsName, $featuredEvents, 0, $this->cacheFeaturedEventsLifetime);
		
		return $featuredEvents;		
	}		
}

?>