<?

require_once(BX_DIRECTORY_PATH_INC . 'header.inc.php' );
require_once(BX_DIRECTORY_PATH_INC . 'admin.inc.php');
require_once(BX_DIRECTORY_PATH_INC . 'design.inc.php' );
require_once(BX_DIRECTORY_PATH_INC . 'profiles.inc.php' );
require_once(BX_DIRECTORY_PATH_INC . 'utils.inc.php' );
require_once(BX_DIRECTORY_PATH_INC . 'tags.inc.php' );
require_once(BX_DIRECTORY_PATH_INC . 'newsfeed.inc.php' );
require_once(BX_DIRECTORY_PATH_CLASSES . 'BxDolComments.php' );


/*
 * class for Events
 */
class BxDolBlogs {
	//variables

	//max sizes of pictures for resizing during upload
	var $iIconSize = 45;

	var $iThumbSize = 110;
	
	var $iBigThumbSize = 340;

	var $iImgSize = 800;

	//admin mode, can All actions
	var $bAdminMode;

	//path to image pic_not_avail.gif
	var $sPicNotAvail;
	//path to spacer image
	var $sSpacerPath = 'templates/base/images/icons/spacer.gif';

	var $aBlogConf = array();

	var $iLastPostedPostID = -1;

	var $iPerPageElements = 10;
	
	var $tags = array();

	/**
	 * constructor
	 */
	function BxDolBlogs($bAdmMode = FALSE) {
		
		global $logged;		

		$this->bAdminMode = $logged['admin'];

		$this->aBlogConf['visitorID']   = (int)$_COOKIE['memberID'];
		$this->aBlogConf['ownerID']     = isset($_REQUEST['ownerID']) ? (int)$_REQUEST['ownerID'] : $this->aBlogConf['visitorID'];
		$this->aBlogConf['isOwner']     = ( $this->aBlogConf['ownerID'] == $this->aBlogConf['visitorID'] ) ? true : false;
		$this->aBlogConf['editAllowed'] = ( $this->aBlogConf['isOwner'] );// || $logged['admin'] ) ? true : false; // deprecated to admin edit/delete blogs
		$this->aBlogConf['period']      = 1; // time period before user can add another record (in minutes)

		if( !$this->aBlogConf['isOwner'] )
			$this->aBlogConf['isFriend'] = is_friends( $this->aBlogConf['visitorID'], $this->aBlogConf['ownerID'] );

		$this->aBlogConf['loggedMember']             = $logged['member'];
		$this->aBlogConf['commentMaxLenght']         = (int)getParam('blogCommentMaxLenght');
		$this->aBlogConf['categoryCaptionMaxLenght'] = (int)getParam('blogCategoryCaptionMaxLenght');
		//$this->aBlogConf['categoryDescMaxLenght']    = (int)getParam('blogCategoryDescMaxLenght');
		$this->aBlogConf['blogCaptionMaxLenght']     = (int)getParam('blogCaptionMaxLenght');
		$this->aBlogConf['blogTextMaxLenght']        = (int)getParam('blogTextMaxLenght');
		$this->aBlogConf['breadCrampDivider']        = getParam('breadCrampDivider');
		$this->aBlogConf['blogDescMaxLength']        = (int)getParam('blogDescMaxLength');
		$this->aBlogConf['blogNumberTagsAsKeywords'] = (int)getParam('blogNumberTagsAsKeywords');
		$this->aBlogConf['blogNameMaxLength']        = (int)getParam('blogNameMaxLength');
		$this->aBlogConf['blogPostDescMaxLength']    = (int)getParam('blogPostDescMaxLength');
	}

	function CheckLogged() {
		global $logged;
		if( !$logged['member'] && !$logged['admin'] ) {
			member_auth(0);
		}
	}
	
	/**

	 * Return string for Header, depends at POST params

	 *

	 * @return Textpresentation of data

	 */

	function GetHeaderString() {

		switch ( $_REQUEST['action'] ) {
			//print functions
			case 'top_blogs':
				$sCaption = _t('_Top Blogs');
				break;
			case 'show_member_blog':
				$sCaption = _t('_my_blog');
				break;
			case 'top_posts':
				$sCaption = _t('_Top Posts');
				break;
			case 'new_post':

				$sCaption = _t('_Add Post');

				break;
			case 'show_member_post':
				$sCaption = _t('_Post');
				break;
			case 'search_by_tag':

				$sCaption = _t('_Search result');

				break;
			default:
				$sCaption = _t('_Blogs');
				break;
		}
		return $sCaption;
	}

	/**
	 * Generate common forms and includes js
	 *
	 * @return HTML presentation of data
	 */
	function GenCommandForms() {
		//unnecessary
		/*<form action="{$_SERVER['PHP_SELF']}" method="post" name="command_activate_post">
			<input type="hidden" name="ActivateAdvertisementID" id="ActivateAdvertisementID" value=""/>
		</form>
		<!-- <form action="{$_SERVER['PHP_SELF']}" method="post" name="command_edit_blog">
			<input type="hidden" name="action" id="action" value="edit_blog" />
			<input type="hidden" name="EditBlogID" id="EditBlogID" value=""/>
			<input type="hidden" name="Description" id="Description" value=""/>
		</form> -->
		if ($this -> bAdminMode) {*/

		$sJSPath = ($this -> bAdminMode) ? "../" : "";
		$ownerID = $this->aBlogConf['visitorID'];

		$sRetHtml = <<<EOF
		<script src="{$sJSPath}inc/js/dynamic_core.js.php" type="text/javascript"></script>

		<form action="{$_SERVER['PHP_SELF']}?action=delete_post&amp;ownerID=$ownerID" method="post" name="command_delete_post">
			<input type="hidden" name="action" value="delete_post" />
			<input type="hidden" name="DOwnerID" id="DOwnerID" value=""/>
			<input type="hidden" name="DeletePostID" id="DeletePostID" value=""/>
		</form>
		<form action="{$_SERVER['PHP_SELF']}?action=delete_category&amp;ownerID=$ownerID" method="post" name="command_delete_category">
			<input type="hidden" name="action" value="delete_category" />
			<input type="hidden" name="OwnerID" value="$ownerID" />
			<input type="hidden" name="DeleteCategoryID" id="DeleteCategoryID" value=""/>
		</form>
		<form action="{$_SERVER['PHP_SELF']}?action=edit_post&amp;ownerID=$ownerID" method="post" name="command_edit_post">
			<input type="hidden" name="action" value="edit_post" />
			<input type="hidden" name="OwnerID" value="$ownerID" />
			<input type="hidden" name="EditPostID" id="EditPostID" value=""/>
		</form>
		<form action="{$_SERVER['PHP_SELF']}" method="post" name="command_delete_blog">
			<input type="hidden" name="action" id="action" value="delete_blog" />
			<input type="hidden" name="DeleteBlogID" id="DeleteBlogID" value="" />
		</form>
EOF;
		return $sRetHtml;
	}

	/**
	 * Generate List of Blogs
	 *
	 * @param $sType - tyle of list ('top', 'last')
	 * @return HTML presentation of data
	 */
	function GenBlogLists($sType = '') {
		$sDescriptionC = _t('_Description');
		$sPostsC = _t('_Posts');
		$sNoBlogsC = _t('_Sorry, nothing found');
		$sAllBlogsC = _t('_All Blogs');
		$sTopBlogsC = _t('_Top Blogs');

		$sBlogsSQL = "
			SELECT `Blogs`. * , `Profiles`.`Nickname` 
			FROM `Blogs` 
			INNER JOIN `Profiles` ON `Blogs`.`OwnerID` = `Profiles`.`ID`
		";

		//////////////////pagination addition//////////////////////////
		$iTotalNum = db_value( "SELECT COUNT(*) FROM `Blogs` INNER JOIN `Profiles` ON `Blogs`.`OwnerID` = `Profiles`.`ID`" );
		if( !$iTotalNum ) {
			return MsgBox($sNoBlogsC);
		}

		$iPerPage = (int)$_GET['per_page'];
		if( !$iPerPage )
			$iPerPage = $this->iPerPageElements;
		$iTotalPages = ceil( $iTotalNum / $iPerPage );

		$iCurPage = (int)$_GET['page'];

		if( $iCurPage > $iTotalPages )
			$iCurPage = $iTotalPages;

		if( $iCurPage < 1 )
			$iCurPage = 1;

		$sLimitFrom = ( $iCurPage - 1 ) * $iPerPage;
		$sqlLimit = "LIMIT {$sLimitFrom}, {$iPerPage}";
		////////////////////////////

		$sCaption = $sAllBlogsC;

		if ($sType == 'top') {
			$sBlogsSQL = "
				SELECT `Blogs`.`ID` , `Blogs`.`OwnerID` , `Blogs`.`Description` , `Profiles`.`Nickname` , MAX( `PostDate` ) AS 'MPD', COUNT( `BlogPosts`.`PostID` ) AS 'PostCount'

				FROM `Blogs` 

				INNER JOIN `BlogCategories` ON `BlogCategories`.`OwnerID` = `Blogs`.`OwnerID` 

				INNER JOIN `Profiles` ON `Profiles`.`ID` = `Blogs`.`OwnerID` 

				INNER JOIN `BlogPosts` ON `BlogPosts`.`CategoryID` = `BlogCategories`.`CategoryID`
				GROUP BY `Blogs`.`ID` 
				ORDER BY `PostCount` DESC
				{$sqlLimit}
			";
			$sCaption = $sTopBlogsC;
		} elseif ($sType == 'last') {
			$sBlogsSQL = "
				SELECT `Blogs`.`ID` , `Blogs`.`OwnerID` , `Blogs`.`Description` , `Profiles`.`Nickname` , MAX( `PostDate` ) AS 'MPD', COUNT( `BlogPosts`.`PostID` ) AS 'PostCount'

				FROM `Blogs` 

				INNER JOIN `BlogCategories` ON `BlogCategories`.`OwnerID` = `Blogs`.`OwnerID` 

				INNER JOIN `Profiles` ON `Profiles`.`ID` = `Blogs`.`OwnerID` 

				INNER JOIN `BlogPosts` ON `BlogPosts`.`CategoryID` = `BlogCategories`.`CategoryID`
				GROUP BY `Blogs`.`ID`
				ORDER BY `MPD` DESC
				{$sqlLimit}
			";
		}

		// process database queries
		$vBlogsRes = db_res( $sBlogsSQL );
		if (mysql_affected_rows()==0) {
			return MsgBox($sNoBlogsC);
		}
		
		$count = mysql_affected_rows();
		$counter = 0;
		$split = false;
		$sRetHtml .= "<table style=\"width:100%\"><tr><td valign=top width='50%'>";
			
		while ( $aBlogsRes = mysql_fetch_assoc($vBlogsRes) ) {
			if ($aBlogsRes['PostCount'] == 0 && $sType == 'top') //in Top blogs skip blogs with 0 comments
				continue;

			$sCont = get_member_thumbnail($aBlogsRes['OwnerID'], 'left' );

		if (($counter > (($count-1) / 2)) && (!$split))
			{
			$sRetHtml .= "</td><td valign=top width='50%'>";
			$split = true;
			}
			
		if (strlen(strip_tags($aBlogsRes['Description'])) > 100)
			$aBlogsRes['Description'] = substr(strip_tags($aBlogsRes['Description']),0,100) . "...";

		$blogLink = (useSEF) ? "{$site['url']}{$aBlogsRes['Nickname']}/blog" :
				"{$site['url']}blogs.php?action=show_member_blog&ownerID={$aBlogsRes['OwnerID']}";
			
			$sRetHtml .= <<<EOF
	
	<table>
	<tr>
	<td valign='top' style="width:120px">{$sCont}</td>
	<td valign='top' style="width:230px">
	<a href="$blogLink">{$aBlogsRes['Nickname']} Blog</a>
	<BR>{$aBlogsRes['PostCount']} {$sPostsC}
	<BR>{$sDescriptionC}: <div class="clr3">{$aBlogsRes['Description']}</div>
	</td>
	</tr>
	</table>
	
<BR>
EOF;
		$counter ++;
		}
		
		$sRetHtml .= "</tr></table>";

		/////////pagination addition//////////////////
		if( $iTotalPages > 1) {
			$sRequest = $_SERVER['PHP_SELF'] . '?';
			$aFields = array( 'action' );

			foreach( $aFields as $vField )
				if( isset( $_GET[$vField] ) )
					$sRequest .= "&amp;{$vField}=" . htmlentities( process_pass_data( $_GET[$vField] ) );

			$sPagination = '<div style="text-align: center; position: relative; margin-top:15px; height:23px">'._t("_Results per page").':
					<select name="per_page" onchange="window.location=\'' . $sRequest . '&amp;per_page=\' + this.value;">
						<option value="10"' . ( $iPerPage == 10 ? ' selected="selected"' : '' ) . '>10</option>
						<option value="20"' . ( $iPerPage == 20 ? ' selected="selected"' : '' ) . '>20</option>
						<option value="50"' . ( $iPerPage == 50 ? ' selected="selected"' : '' ) . '>50</option>
						<option value="100"' . ( $iPerPage == 100 ? ' selected="selected"' : '' ) . '>100</option>
					</select></div>' .
				genPagination( $iTotalPages, $iCurPage, ( $sRequest . '&amp;page={page}&amp;per_page='.$iPerPage ) );
		} else
			$sPagination = '';
		///////////////////////////

		$searchSect = <<<EOF
<div class="caption_item">
	<form name="BlogsSearchForm" action="blogs.php" method="get">
	<input type="hidden" name="action" value="search"/>
	<input type="text" name="keyword" size="23" value="{$_REQUEST['keyword']}"/>
	<input type="submit" value="Search"/>
</form></div>
EOF;
			
		return $this->_sectionWithSearchBox($sCaption, $sRetHtml.$sPagination);
	}

	/**
	 * Generate List of Posts
	 *
	 * @param $sType - tyle of list ('top', 'last'), but now realized only Top posts
	 * @return HTML presentation of data
	 */
	function GenPostLists($sType = '') {
		global $site;

		$sEditC = _t('_Edit');
		$sDeleteC = _t('_Delete');
		$sSureC = _t("_Are you sure");
		$sPostCommentC = _t('_Post Comment');
		$sDescriptionC = _t('_Description');
		$sAddCommentC = _t('_Add comment');
		$sNewPostC = _t('_New Post');
		$sTagsC = _t('_Tags');
		$sPostsC = _t('_Posts');
		$sCaption = _t('_Top Posts');

		$sDateFormatPhp = getParam('php_date_format');

		$postsPerPage = 10;
		$curPage = (isset($_GET['page']) && (int)$_GET['page'] > 0) ? (int)$_GET['page'] : 1;
		$offset = ($curPage > 1) ? ($curPage - 1) * $postsPerPage : 0;			
		
		$sTopPostSQL = "
			SELECT `BlogPosts`.*, COUNT(`CommentID`) AS `CountComments`, `BlogCategories`.`OwnerID`,`BlogCategories`.`CategoryName`
			FROM `BlogPosts`
			LEFT JOIN `BlogPostComments` ON `BlogPosts`.`PostID`=`BlogPostComments`.`PostID`
			LEFT JOIN `BlogCategories` ON `BlogCategories`.`CategoryID`=`BlogPosts`.`CategoryID`
			GROUP BY `BlogPostComments`.`PostID`
			ORDER BY `CountComments` DESC, `PostDate` DESC
			LIMIT $offset, $postsPerPage
		";
		$vTopPostSQL = db_res($sTopPostSQL);

		while ( $aResSQL = mysql_fetch_assoc($vTopPostSQL) ) {
			if ($aResSQL['CountComments'] > 0) {
				$bFriend = is_friends( $this->aBlogConf['visitorID'], $aResSQL['OwnerID'] );
				$bOwner = ($this->aBlogConf['visitorID']==$aResSQL['OwnerID']) ? true : false;
				$sBlogPosts .= $this->GenPostString($aResSQL, 2);
			}
		}
		
		$totalTopPosts = db_value("
			SELECT COUNT(*)
			FROM
			(
				SELECT COUNT(bpc.`CommentID`) AS `CountComments`
				FROM `BlogPosts` as bp
				LEFT JOIN `BlogPostComments` as bpc USING(PostID)
				GROUP BY bpc.`PostID`
			) as c
			WHERE CountComments > 0
		");
	
		// Calculate total pages
		$totalPages = ceil($totalTopPosts / $postsPerPage);
		
		// Show pagination
		if ($totalPages > 1)
			$sBlogPosts .= $this->_pagination($totalPages, $curPage, 
				stripos($_SERVER['REQUEST_URI'], 'action=top_posts') === false);
	
		return $this->_sectionWithSearchBox($sCaption, $sBlogPosts);
	}

	/**
	 * Generate Post Block by Array of data
	 *
	 * @param $aResSQL - Post Array Data
	 * @param $iView - type of view(1 is short view, 2 is full view, 3 is post view(short)):
	 * 1 - posts view on blog page;
	 * 2 - posts view on top posts page;
	 * 3 - posts view on post page; 
	 * @return HTML presentation of data
	 */
	function GenPostString($aResSQL, $iView=1) {
		global $site;
		global $logged;

		$sEditC = _t('_Edit');
		$sDeleteC = _t('_Delete');
		$sSureC = _t("_Are you sure");
		$sConfirmDeleteBlogPost = _t("_confirmDeleteBlogPost");
		$sPostCommentC = _t('_Post Comment');
		$sDescriptionC = _t('_Description');
		$sAddCommentC = _t('_Add comment');
		$sNewPostC = _t('_New Post');
		$sTagsC = _t('_Tags');
		$sPostsC = _t('_Posts');

		$sDateFormatPhp = getParam('php_date_format');
		$sDateTime = date( $sDateFormatPhp, strtotime( $aResSQL['PostDate'] ) );

		$aProfileRes = $this -> GetProfileData($aResSQL['OwnerID']);

		$sTagsHrefs = '';
		if ($iView == 3) // show tags only for single post (action=show_member_post)
		{
			$sTagsCommas = $aResSQL['Tags'];
			$aTags = split(',', $sTagsCommas);
			foreach( $aTags as $sTagKey ) {
				$tagLink = (useSEF) ? "{$site['url']}{$aProfileRes['NickName']}/blog/tagKey=$sTagKey"
					: "{$_SERVER['PHP_SELF']}?action=search_by_tag&amp;tagKey={$sTagKey}&amp;ownerID={$aResSQL['OwnerID']}";
				$sTagsHrefs .= <<<EOF
<span style="float:left; margin-right:5px"><a href="$tagLink" >{$sTagKey}</a></span>
EOF;
			}
		}
		$sActions = '';
		if ($this->aBlogConf['visitorID']==$aResSQL['OwnerID'] || $this->bAdminMode==TRUE) {
			$sJSPostText = addslashes($aResSQL['PostText']);

			$sActions = <<<EOF
<div class="fr_small">
	<a href="{$_SERVER['PHP_SELF']}" onclick="javascript: UpdateField('EditPostID','{$aResSQL['PostID']}');document.forms.command_edit_post.submit();return false;" style="text-transform:none;">{$sEditC}</a>&nbsp;
	<a href="{$_SERVER['PHP_SELF']}" onclick="javascript: if (confirm('{$sConfirmDeleteBlogPost}')) {UpdateField('DeletePostID','{$aResSQL['PostID']}');UpdateField('DOwnerID','{$aResSQL['OwnerID']}');document.forms.command_delete_post.submit(); } return false;" style="text-transform:none;">{$sDeleteC}</a>
</div>
EOF;
		}

		$sAuthor = '';
		if ($iView==2) {
			$sAuthorC = _t( '_By Author', $aProfileRes['NickName'] );
			$sAuthor = $sAuthorC; //"{$sAuthorC}<a class=\"\" style=\"font-weight:bold;\" href=\"".getProfileLink($aResSQL['OwnerID'])."\">{$aProfileRes['NickName']}</a>&nbsp;&nbsp;" ;
		}
		
		$sFriendStyle = "";
		$bFriend = is_friends( $this->aBlogConf['visitorID'], $aResSQL['OwnerID'] );
		$bHotlistFriend = is_hotlist_friends($aResSQL['OwnerID'], $this->aBlogConf['visitorID']);
		$bOwner = ($this->aBlogConf['visitorID']==$aResSQL['OwnerID']) ? true : false;
		
		// Check blog post read permission
		if ($aResSQL['PostReadPermission'] == 'friends')
		{
			if (!$bFriend && !$bOwner && !$this->bAdminMode )
			{
				$sFriendStyle="1";
				$sPostText = MsgBox('Locked for friends only', 16, 2,
					"<img src=\"{$site['icons']}lock.gif\" style=\"height:40px; margin-right:5px; vertical-align:middle\"/>");
			}
		}
		elseif ($aResSQL['PostReadPermission'] == 'favorites')
		{
			if (!$bHotlistFriend && !$bOwner && !$this->bAdminMode )
				$sPostText = MsgBox('Locked by invitation only', 16, 2,
					"<img src=\"{$site['icons']}lock.gif\" style=\"height:40px; margin-right:5px; vertical-align:middle\"/>");
		}
		elseif ($aResSQL['PostReadPermission'] == 'members')
		{
			if (!$logged['member'] && !$this->bAdminMode )
				$sPostText = MsgBox(_t('_private_blog'), 16, 2,
					"<img src=\"{$site['icons']}lock.gif\" style=\"height:40px; margin-right:5px; vertical-align:middle\"/>");
		}
		
		if (!$sPostText)
		{
			if ($iView == 3)
				$sPostText = $aResSQL['PostText'];
			else
			{
				$sPostText = $aResSQL['PostText'];
				/*
				$maxCharsInShortDesc = 500;
				$sPostText = strip_tags($aResSQL['PostText']);
				if (strlen($sPostText) > $maxCharsInShortDesc)
					$sPostText = substr($sPostText, 0, $maxCharsInShortDesc) . '...';
					*/
			}
			if ($iView==2) $sActions='';
			if ($iView == 3)
				$sTags = <<<EOF
<div class="fr_small_gray_centered" style="margin-bottom:5px; float:left; clear:left; width:100%">
	<span style="float:left; margin-right:5px"><span style="vertical-align:middle;"><img src="{$site['icons']}tag_small.png" class="marg_icon" alt="{$sTagsC}" /></span>{$sTagsC}: </span>{$sTagsHrefs}
</div>
EOF;
		}
	
		$sPostCaptionHref = (useSEF && $aResSQL['urltitle'] != '') ?
			"<a class=\"actions\" href=\"{$site['url']}{$aProfileRes['NickName']}/{$aResSQL['urltitle']}.html\">{$aResSQL['PostCaption']}</a>"
			: "<a class=\"actions\" href=\"{$site['url']}blogs.php?action=show_member_post&ownerID={$aResSQL['OwnerID']}&post_id={$aResSQL['PostID']}\">{$aResSQL['PostCaption']}</a>";
			
		$sPostPhoto = '';
		if ($iView==3) {
			$sPostCaptionHref = '<div class="actions">'.$aResSQL['PostCaption'].'</div>';
		}
		if (($iView==1 || $iView==3) && $sFriendStyle != '1') {
		//if ($iView==3) {
			if ( $aResSQL['PostPhoto'] ) {
				$sSpacerName = $site['url'].$this -> sSpacerPath;
				$sPostPhoto = <<<EOF
<div class="marg_both_left">
	<img alt="{$aResSQL['PostPhoto']}" style="width: {$this->iThumbSize}px; background-image: url({$site['blogImage']}big_{$aResSQL['PostPhoto']});" src="{$sSpacerName}"/>
</div>
EOF;
			}
		}

		$folderIcon = (empty($aResSQL['CategoryPhoto'])) ? 'folder_small.png' : "small_{$aResSQL['CategoryPhoto']}";
		
		// Check is visitor in blocklist of blog owner
		$visitorID = (int)$_COOKIE['memberID'];
		$isBlocked = db_value("SELECT ID FROM BlockList WHERE ID=".$aResSQL['OwnerID']." AND Profile=$visitorID LIMIT 1");		
		
		/*
		// Check is visitor have Unconfirmed status
		$status = db_value("SELECT `Status` FROM `Profiles` WHERE ID=$visitorID LIMIT 1");
		$isUnconfirmed = ($status == 'Unconfirmed');*/
		
		//$addCommentLink = (!$isBlocked && !$isUnconfirmed) ?
		$addCommentLink = (!$isBlocked) ?
			"<a href=\"{$_SERVER['PHP_SELF']}?action=show_member_post&amp;ownerID={$aResSQL['OwnerID']}&amp;post_id={$aResSQL['PostID']}&amp;add_comment=1\">$sPostCommentC</a>" : '';
		
		$postBottomStyle = ($iView != 3) ? 'border-bottom:1px solid #CCCCCC; margin-bottom:10px;' : '';
		$countComments = (array_key_exists('CountComments', $aResSQL)) ? $aResSQL['CountComments']
			: db_value("SELECT COUNT(*) FROM `BlogPostComments` WHERE `PostID`={$aResSQL['PostID']}");
		$commentsSect = ($iView != 3) ? 
"<div class=\"fr_small_gray_centered\" style=\"clear:left; padding-bottom:10px\">
	<span style=\"vertical-align:middle\">
		<img src=\"{$site['icons']}add_comment.gif\" class=\"marg_icon\" alt=\"{$sAddCommentC}\" title=\"{$sAddCommentC}\" />
	</span><span class=\"margined\">$countComments comments</span>$addCommentLink
</div>" : '';

		$categoryLink = (useSEF && $aResSQL['caturltitle'] != '') ? 
			"{$site['url']}{$aProfileRes['NickName']}/blog/{$aResSQL['caturltitle']}"
			: "{$_SERVER['PHP_SELF']}?action=show_member_blog&amp;ownerID={$aResSQL['OwnerID']}&amp;category={$aResSQL['CategoryID']}";
		$sBlogPosts .= <<<EOF
<div style="float:left; $postBottomStyle position:relative; width:100%">
<div style="float:left; clear:left; width:100%">
	<div class="cls_res_thumb">
		{$sPostCaptionHref}
	</div>
	{$sActions}
	<div class="clear_both"></div>
</div>
<div class="fr_small_gray_centered">
	{$sAuthor}
	<span class="margined">{$sDateTime}</span>
	<span style="vertical-align:middle;">
		<img src="{$site['icons']}$folderIcon" class="marg_icon" style="width:16px;height:12px" alt="{$aResSQL['CategoryName']}"/>
	</span>
	<a href="$categoryLink">{$aResSQL['CategoryName']}</a>
</div>
<div class="blog_text" style="overflow:hidden">
	{$sPostPhoto}
	{$sPostText}
</div>
{$sTags}
{$commentsSect}
</div>
EOF;

		return $sBlogPosts;
	}

	/**
	 * Generate User Right Part for Blogs
	 *
	 * @param $aBlogsRes - Blog Array Data
	 * @param $iView - type of view(1 is short view, 2 is full view, 3 is post view(short))
	 * @return HTML presentation of data
	 */
	function GenMemberDescrAndCat($aBlogsRes, $iCategID = -1) {
		global $site;
		global $logged;
		global $tmpl;
		global $php_date_format;

		$sEditBlogC = _t('_Edit');
		$sEditBlogPopupC = _t('_edit_blog');
		$sBlogSettingsC = _t('_blog_settings');
		$sDeleteBlogC = _t('_Delete');
		$sDeleteBlogPopupC = _t('_delete_blog');
		$sConfirmDeleteBlog = _t('_confirmDeleteBlog');
		$sSureC = _t("_Are you sure");
		$sConfirmDeleteCategory = _t('_confirmDeleteCategory');
		$sApplyChangesC = _t('_Apply Changes');
		$sDescriptionC = _t('_Description');
		$sCategoriesC = _t('_Categories');
		$sCategoryC = _t('_Category');
		$sTagsC = _t('_Tags');
		$sEditCategoryC = _t('_edit_category');
		$sAddCategoryC = _t('_add_category');
		$sDeleteCategoryC = _t('_delete_category');
		$sCharactersLeftC = _t('_characters_left');
		
		$php_date_format = getParam('php_date_format');
	
		$maxBlogDescLength = 5000;

		$sNewC = ucfirst(_t('_new'));

		$iMemberID = $aBlogsRes['OwnerID'];
		//$sCont = get_member_icon($aBlogsRes['OwnerID'], 'left' );
		$sCont = get_member_thumbnail($aBlogsRes['OwnerID'], 'left');

		//tested, right
		$sCountPostSQL = "
			SELECT COUNT(*)
			FROM `BlogPosts`
			LEFT JOIN `BlogCategories` ON `BlogCategories`.`CategoryID` = `BlogPosts`.`CategoryID`
			WHERE `BlogCategories`.`OwnerId` = {$iMemberID}
		";

		$aCountPost = db_arr( $sCountPostSQL );
		$iCountPost = (int)$aCountPost[0];

		//tested, right
		$sCountCommentsSQL = "
			SELECT COUNT( * ) 
			FROM `BlogPostComments` 
			LEFT JOIN `BlogPosts` ON `BlogPosts`.`PostID` = `BlogPostComments`.`PostID` 
			LEFT JOIN `BlogCategories` ON `BlogCategories`.`CategoryID` = `BlogPosts`.`CategoryID` 
			WHERE `BlogCategories`.`OwnerId` = {$iMemberID}
		";

		$aCountComments = db_arr( $sCountCommentsSQL );
		$iCountComments = (int)$aCountComments[0];

		$sCategories = '';
		$sCategoriesSQL = "
			SELECT * 
			FROM `BlogCategories`
			WHERE `OwnerId` = {$iMemberID}
		";
		$vCategories = db_res( $sCategoriesSQL );
		$aTagsPost = array();
		$sTagsVals = '';

		if ($iCategID  > 0)
			$sPostsSQL = "SELECT `Tags`,`PostReadPermission`,`BlogCategories`.`OwnerID` FROM `BlogPosts`
				LEFT JOIN `BlogCategories` ON `BlogCategories`.`CategoryID` = `BlogPosts`.`CategoryID`
				WHERE `BlogCategories`.`CategoryID` = {$iCategID}";
		else
			$sPostsSQL = "SELECT `Tags`,`PostReadPermission`,`BlogCategories`.`OwnerID` FROM `BlogPosts`
				LEFT JOIN `BlogCategories` ON `BlogCategories`.`CategoryID` = `BlogPosts`.`CategoryID`
				WHERE `BlogCategories`.`OwnerID` = {$aBlogsRes['OwnerID']}";
				
		$vTags = db_res( $sPostsSQL );
		$aTagsPost = array();
		while ( $aPost = mysql_fetch_assoc($vTags) ) {
			$bFriend = is_friends( $this->aBlogConf['visitorID'], $aPost['OwnerID'] );
			$bOwner = ($this->aBlogConf['visitorID']==$aPost['OwnerID']) ? true : false;
			if( 'friends' == $aPost['PostReadPermission'] && !$bFriend && !$bOwner && !$this->bAdminMode ) {
			} else {
				$sTagsCommas = trim($aPost['Tags']);
				$aTags = explode(',', $sTagsCommas);
				foreach( $aTags as $sTagKey ) {
					if ($sTagKey!='') {
						if( isset($aTagsPost[$sTagKey]) )
							$aTagsPost[$sTagKey]++;
						else
							$aTagsPost[$sTagKey] = 1;
					}
				}
			}
		}
		$this->tags = $aTagsPost;

		$counter = 0;
		foreach( $aTagsPost as $varKey => $varValue ) 
			{
			$sTagImg = '<img src="'.$site['icons'].'tag.png" class="static_icon" alt="'.$varValue.'" /></span>';
			$tagLink = (useSEF) ? "{$site['url']}{$aBlogsRes['OwnerNick']}/blog/tagKey=$varKey"
				: "{$_SERVER['PHP_SELF']}?action=search_by_tag&amp;tagKey=$varKey&amp;ownerID=$iMemberID";
			$sTagName = '<a class="actions" href="'.$tagLink.'" style="text-transform:capitalize;" >'.$varKey.'</a>&nbsp;';
			$sTagsImgName = $this->GenCenteredActionsBlock($sTagImg, $sTagName);
			/*$sTagsVals .= <<<EOF
<div style="vertical-align:middle;height:25px;margin-bottom:5px;">
	{$sTagsImgName}
</div>
EOF;*/
			
			if ($counter < 25)
				$sTagsVals .= '<div style="vertical-align:middle;height:25px;margin-bottom:5px; margin-right:5px;float:left;">'.$sTagName.'</div>';
			$counter ++;
			}
		
		
		while ( $aCategories = mysql_fetch_assoc($vCategories) ) {
			$sCountPostCatSQL = "
				SELECT COUNT(*)
				FROM `BlogPosts`
				LEFT JOIN `BlogCategories` ON `BlogCategories`.`CategoryID` = `BlogPosts`.`CategoryID`
				WHERE `BlogCategories`.`CategoryID` = {$aCategories['CategoryID']}
			";

			$aCountCatPost = db_arr( $sCountPostCatSQL );
			$iCountCatPost = (int)$aCountCatPost[0];

			$sAbilEditDelCateg = '';
			if ($this->aBlogConf['visitorID']==$aBlogsRes['OwnerID'] || $this->bAdminMode==TRUE) {
				$sDelAbilOnly = '';
				if ($aCategories['CategoryType']==1) {
					$sDelAbilOnly = <<<EOF
<a href="{$_SERVER['PHP_SELF']}" onclick="javascript: if (confirm('{$sConfirmDeleteCategory}')) {UpdateField('DeleteCategoryID','{$aCategories['CategoryID']}');document.forms.command_delete_category.submit(); } return false;">
	<span style="vertical-align:middle;"><img src="{$site['icons']}description_delete.png" style="border:0 solid;position:static;" alt="{$sDeleteCategoryC}" title="{$sDeleteCategoryC}" /></span>
</a>
EOF;
				}
				$sAbilEditDelCateg = <<<EOF
<a href="{$_SERVER['PHP_SELF']}?action=edit_category&amp;ownerID={$iMemberID}&amp;categoryID={$aCategories['CategoryID']}" style="text-transform:none;">
	<span style="vertical-align:middle;"><img src="{$site['icons']}description_edit.png" style="border:0 solid;position:static;" alt="{$sEditCategoryC}" title="{$sEditCategoryC}" /></span>
</a>
EOF;
				$sAbilEditDelCateg .= $sDelAbilOnly;
			}
			$sCatPic = ($aCategories['CategoryPhoto'])?$site['icons'].'small_'.$aCategories['CategoryPhoto']:"{$site['icons']}small_folder11.png";

			$sCatName = $aCategories['CategoryName'];
			$sSpacerName = $site['url'].$this->sSpacerPath;
			$sDefaultLabel = ($aCategories['CategoryType'] == 5 && $this->aBlogConf['visitorID']==$aBlogsRes['OwnerID']) ? 
				'<span style="font-size:10px;"> *default</span>' : '';

			if ($this->aBlogConf['visitorID']==$aBlogsRes['OwnerID'])
				$catNameMaxWidth = ($sDefaultLabel) ? '97px' : '115px';
			else 
				$catNameMaxWidth = '150px';
				
			$categoryLink = (useSEF && $aCategories['caturltitle'] != '') ? 
				"{$site['url']}{$aBlogsRes['OwnerNick']}/blog/{$aCategories['caturltitle']}"
				: "{$_SERVER['PHP_SELF']}?action=show_member_blog&amp;ownerID={$iMemberID}&amp;category={$aCategories['CategoryID']}";
							
			$sCategories .= <<<EOF
<table width="100%" cellpadding="0" cellspacing="0" style="margin-bottom:5px">
<tr style="vertical-align:top"><td style="width:25px; padding-right:2px">
	<span style="vertical-align:middle;">
		<img class="icons" src="{$sSpacerName}" style="border:0 solid;width: 25px; height: 20px; background-image:url({$sCatPic});" alt="" />
	</span>
</td><td>
		<span class="blog_category" style="width:$catNameMaxWidth;">
			<a class="actions" href="$categoryLink">{$sCatName}</a>
		</span>
		&nbsp;({$iCountCatPost})
</td><td style="text-align:right">
	$sDefaultLabel
	{$sAbilEditDelCateg}
</td></tr>
</table>
EOF;
		}

		if ($this->aBlogConf['visitorID']==$aBlogsRes['OwnerID'] || $this->bAdminMode==TRUE) {
			$sDescrAct = trim(htmlspecialchars($aBlogsRes['Description']));
			$charsRemaining = $maxBlogDescLength - strlen($aBlogsRes['Description']);
			if ($charsRemaining < 0)
				$charsRemaining = 0;
			$sTitleAct = htmlspecialchars($aBlogsRes['Title']);
			$cancelAction = <<<EOF
	<span id="cancel_action" style="display:none; padding-right:10px">
		<a href="{$_SERVER['PHP_SELF']}" style="text-transform:none"
			onclick="javascript:document.getElementById('cancel_action').style.display = 'none';
				document.getElementById('edit_action').style.display = 'inline';
				document.getElementById('settings_action').style.display = 'inline';
				document.getElementById('edited_blog_div').style.display = 'none'; 
				document.getElementById('SmallDesc').style.display = 'block'; 
				return false">Cancel</a>
	</span>
EOF;
			$settingsCancelAction = <<<EOF
	<div class="caption_item">		
		<a href="{$_SERVER['PHP_SELF']}" style="text-transform:none"
			onclick="javascript:document.getElementById('blog_settings').style.display = 'none';
				document.getElementById('description_sect').style.display = 'block';
				return false">Cancel</a>
	</div>
EOF;
			$editAction = <<<EOF
	<span id="edit_action">
	<span style="vertical-align:middle">
		<a href="{$_SERVER['PHP_SELF']}" onclick="javascript:  
			UpdateField('EditBlogID','{$aBlogsRes['ID']}'); UpdateField('EOwnerID','{$iMemberID}'); 
			document.getElementById('edited_blog_div').style.display = 'block'; 
			document.getElementById('SmallDesc').style.display = 'none'; 
			document.getElementById('edit_action').style.display = 'none';
			document.getElementById('settings_action').style.display = 'none';
			document.getElementById('cancel_action').style.display = 'block';
			return false;">
			<img src="{$site['icons']}description_edit.png" class="marg_icon" alt="{$sEditBlogPopupC}" title="{$sEditBlogPopupC}" />
		</a>
	</span>
	<a href="{$_SERVER['PHP_SELF']}" onclick="javascript:  
			UpdateField('EditBlogID','{$aBlogsRes['ID']}'); UpdateField('EOwnerID','{$iMemberID}'); 
			document.getElementById('edited_blog_div').style.display = 'block'; 
			document.getElementById('SmallDesc').style.display = 'none'; 
			document.getElementById('edit_action').style.display = 'none';
			document.getElementById('settings_action').style.display = 'none';
			document.getElementById('cancel_action').style.display = 'block';
			return false;"
			style="text-transform:none">{$sEditBlogC}
	</a>
	</span>
EOF;
			$settingsAction = <<<EOF
	<span id="settings_action">
	<span style="vertical-align:middle">
		<a href="{$_SERVER['PHP_SELF']}" onclick="javascript:
			UpdateField('EditBlogID','{$aBlogsRes['ID']}'); UpdateField('EOwnerID','{$iMemberID}'); 
			document.getElementById('description_sect').style.display = 'none';
			document.getElementById('blog_settings').style.display = 'block';
			return false;">
			<img src="{$site['icons']}description_edit.png" class="marg_icon" alt="{$sBlogSettingsC}" title="{$sBlogSettingsC}" />
		</a>
	</span>
	<a href="{$_SERVER['PHP_SELF']}" style="text-transform:none" onclick="javascript:
			UpdateField('EditBlogID','{$aBlogsRes['ID']}'); UpdateField('EOwnerID','{$iMemberID}'); 
			document.getElementById('description_sect').style.display = 'none';
			document.getElementById('blog_settings').style.display = 'block';
			return false;">Settings</a>
	</span>
EOF;
			$blogActions= <<<EOF
<div class="caption_item" style="padding-right:0px">
	$editAction
	$settingsAction
	$cancelAction
</div>
EOF;
/*
			$delAction = <<<EOF
<div class="caption_item">	
	<span style="vertical-align:middle;">
		<a href="{$_SERVER['PHP_SELF']}" onclick="javascript: if (confirm('{$sConfirmDeleteBlog}')) 
			{UpdateField('DeleteBlogID','{$aBlogsRes['ID']}');document.forms.command_delete_blog.submit(); } return false;">
			<img src="{$site['icons']}icon_delete.png" class="marg_icon" alt="{$sDeleteBlogPopupC}" title="{$sDeleteBlogPopupC}"/>
		</a>
	</span>
	<a href="{$_SERVER['PHP_SELF']}" onclick="javascript: if (confirm('{$sConfirmDeleteBlog}')) 
		{UpdateField('DeleteBlogID','{$aBlogsRes['ID']}');document.forms.command_delete_blog.submit(); } return false;" 
		style="text-transform:none;">{$sDeleteBlogC}</a>			
</div>
EOF;
*/
		}

		$sProfLink = getProfileLink($aBlogsRes['OwnerID']);
		$notLocked = ($aBlogsRes['Locked'] == 0) ? 'checked' : '';
		$lockedFriendsOnly = ($aBlogsRes['Locked'] == 1) ? 'checked' : '';
		$lockedHotlistFriendsOnly = ($aBlogsRes['Locked'] == 2) ? 'checked' : '';
		$lockedMembersOnly = ($aBlogsRes['Locked'] == 3) ? 'checked' : '';
		$lockedPrivateOnly = ($aBlogsRes['Locked'] == 4) ? 'checked' : '';
		$syndicateURL = $aBlogsRes['SyndicateUrl'];
		
		$blogSettingsTable = <<<EOF
		<div style="font-weight:bold">Permissions</div>
		<table style="margin-top:5px; font-size:11px">
		<tr style="vertical-align:top">
			<td><input type="radio" name="lockoption" value="0" $notLocked /></td>
			<td>Public - Visible to Everyone</td></tr>
		<tr style="vertical-align:top">
			<td><input type="radio" name="lockoption" value="3" $lockedMembersOnly /></td>
			<td>Visible to Members Only</td></tr>			
		<tr style="vertical-align:top">
			<td><input type="radio" name="lockoption" value="1" $lockedFriendsOnly /></td>
			<td>Visible to Friends Only</td></tr>
		<tr style="vertical-align:top">
			<td><input type="radio" name="lockoption" value="2" $lockedHotlistFriendsOnly /></td>
			<td>Visible to Hot-List Only</td></tr>	
		<tr style="vertical-align:top">
			<td><input type="radio" name="lockoption" value="4" $lockedPrivateOnly /></td>
			<td>Lock Blog - Visible to Me Only</td></tr>
		</table>
		<br/>
		<div style="font-weight:bold">Syndicate an existing blog</div>
		<div style="font-size:11px; padding-top:5px">URL (RSS 2.0, Atom):
			<input type="text" name="syndicateURL" style="width:95%" maxlength="255" value="$syndicateURL"/></div>
		<div style="color:grey; font-size:10px; padding-top:5px">
			If specified, new messages will be fetched from feed and added to your blog automatically. 
			Keep empty if you prefer to post to blog manually.</div>

		<div style="text-align:center; margin-top:5px">
			<input type="submit" name="updateSettings" value="Update Settings" onclick="return CheckRSSURL();"/>
		</div>
		<div style="font-weight:bold; margin-top:10px; margin-bottom:5px">Delete My Blog</div>
		<input type="checkbox" name="delete_blog" id="delete_blog"
			onclick="document.forms.EditBlogForm.delete_blog_button.disabled = !this.checked;"/> 
		<span style="font-size:11px; width:200px">Yes, delete my entire blog.</span>
		<div style="text-align:center; margin-top:5px">
			<input type="button" name="delete_blog_button" value="Delete My Blog" disabled="1"
				onclick="javascript: if (confirm('{$sConfirmDeleteBlog}')) 
				{UpdateField('DeleteBlogID','{$aBlogsRes['ID']}');document.forms.command_delete_blog.submit(); } return false;"/>
		</div>		
		
		<script>
		
		// Save old syndicateURL
		var oldSyndicateURL = '$syndicateURL';
		
		var xmlhttp;
		
		function CheckRSSURL()
		{
			var syndicateURL = document.EditBlogForm.syndicateURL.value;
			if (syndicateURL == '' || syndicateURL == oldSyndicateURL) return true;
			
			return loadXMLDoc(syndicateURL, 'simplepie/rss_validate.php');
		}		
		</script>
EOF;

		$blogSettingsBar = '<div id="blog_settings" style="display:none">' .
			DesignBoxContent('Blog Settings', $blogSettingsTable, 1, $settingsCancelAction) . '</div>';
		
		$profileIconPath = getTemplateIcon('action_friends.gif');
		$sendIconPath = getTemplateIcon('action_send.gif');
		$favIconPath = getTemplateIcon('action_fave.gif');
		$friendIconPath = getTemplateIcon('action_friends.gif');
		
		$oTemplConfig = new BxTemplConfig($site);
		$popupWindowWidth = $oTemplConfig->popUpWindowWidth;
		$popupWindowHeight = $oTemplConfig->popUpWindowHeight;
		
		$favHref = "list_pop.php?action=hot&amp;ID={$aBlogsRes['OwnerID']}";
		$friendHref = "list_pop.php?action=friend&amp;ID={$aBlogsRes['OwnerID']}";
		
		if ($aBlogsRes['OwnerID'] != (int)$_COOKIE['memberID']) // show mini actions if not my blog
		{
			$miniActionsSect = <<<EOF
				<div class="blog_infoItem">
					<img src="$profileIconPath" class="miniAction" alt="View Profile"/>
					<a class="miniAction" href="profile.php?ID={$aBlogsRes['OwnerID']}">View Profile</a></div>
				<div class="blog_infoItem" >
					<img src="$sendIconPath" class="miniAction" alt="Send Message"/>
					<a class="miniAction" href="compose.php?ID={$aBlogsRes['OwnerID']}">Send Message</a></div>
EOF;
			if ($logged['member'])
				$miniActionsSect .= <<<EOF
				<div class="blog_infoItem">
					<img src="$favIconPath" class="miniAction" alt="Add To Favorites"/>
					<a class="miniAction" href="$favHref">Add To Favorites</a></div>
				<div class="blog_infoItem">
					<img src="$friendIconPath" class="miniAction" alt="Add To Friends"/>
					<a class="miniAction" href="$friendHref">Add To Friends</a></div>		
EOF;
		}
		else
			$miniActionsSect = '';
			
		$oVotingView = new BxTemplVotingView ('blog', $aBlogsRes['ID']);
		$votingSect = $oVotingView->getSmallVoting();
		
		$views = db_value("SELECT `Views` FROM `Blogs` WHERE `ID` = {$aBlogsRes['ID']} LIMIT 1");
			
		$sDescriptionContent = <<<EOF
<div class="cls_res_thumb">
	<div class="marg_both">
		{$sCont}
	</div>
</div>
<div style="float:left; position:relative">
	<!--<div style="float:left; width:90px; height:15px; overflow:hidden"><a class="actions" href="{$sProfLink}">{$aBlogsRes['Nickname']}</a></div>-->
	<div class="blog_infoItem">Posts: {$iCountPost}</div>
	<div class="blog_infoItem">Comments: {$iCountComments}</div>
	$miniActionsSect
</div>
	<div style="float:left; clear:both; width:100%">
{$votingSect}
<div style="float:left; font-weight:bold; color:#666666; margin:10px 0px 10px 5px;">/ {$views} views</div>

	</div>
<div class="cls_res_info_p22" id="SmallDesc" style="text-align:left;clear:left; float:left; width:100%; position:relative">
	<div style="float:left; clear:left; font-weight:bold; margin-bottom:5px; width:100%">{$aBlogsRes['Title']}</div>
	{$aBlogsRes['Description']}
</div>
EOF;

		$editedBlogDiv = <<<EOF
<div id="edited_blog_div" style="display: none; position:relative; float:left" class="boxContent">
	<div style="margin-bottom:5px">Title: 
		<input type="text" name="Title" id="Title" value="{$sTitleAct}" style="width:80%" maxlength="25"/></div>
	Description:
	<textarea name="Description" id="blog_desc" rows="4" cols="10" class="classfiedsTextArea1" 
		style="width:210px;height:85px;margin-top:5px" 
		onKeyUp="return charCounter('blog_desc', $maxBlogDescLength, 'desc_counter')">{$sDescrAct}</textarea>
	<div style="text-align:right; margin-top:3px">
		<span id="desc_counter" name="desc_counter" style="font-weight:bold">$charsRemaining</span> $sCharactersLeftC 
	</div>
	<div style="text-align:center; margin-top:10px"><input type="submit" value="{$sApplyChangesC}"/></div>
</div>
EOF;

		$descSectStyle = ($this->aBlogConf['visitorID']==$aBlogsRes['OwnerID']) ? 'padding-left:3px' : '';
		$sDescriptionSect = '<div id="description_sect">'.
			DesignBoxContent ( _t($sDescriptionC), $sDescriptionContent, 1 , 
				$blogActions, '', '', $descSectStyle, 'padding:0px 0px 0px 1px') . '</div>';
				
		$sDescriptionSect .= <<<EOF
		<form action="{$_SERVER['PHP_SELF']}" method="post" name="EditBlogForm">
			<input type="hidden" name="action" id="action" value="edit_blog" />
			<input type="hidden" name="EditBlogID" id="EditBlogID" value=""/>
			<input type="hidden" name="EOwnerID" id="EOwnerID" value=""/>
EOF;
		$sDescriptionSect .= $editedBlogDiv;
		$sDescriptionSect .= $blogSettingsBar;
		$sDescriptionSect .= "</form>";

		$sCategoriesActions = '';
		if ($this->aBlogConf['visitorID']==$aBlogsRes['OwnerID'] /*|| $this->bAdminMode==TRUE*/) {
			$addCategoryLink = (useSEF) ? "{$site['url']}{$aBlogsRes['OwnerNick']}/add-blog-category"
				: "{$_SERVER['PHP_SELF']}?action=add_category&amp;ownerID={$iMemberID}";
			$sCategoriesActions = <<<EOF
<div class="caption_item">
	<span style="vertical-align:middle;">
		<a href="$addCategoryLink" style="text-transform:none;">
			<img src="{$site['icons']}categ_add.png" class="marg_icon" alt="{$sAddCategoryC}" title="{$sAddCategoryC}"/>
		</a>
	</span>
	<a href="$addCategoryLink" style="text-transform:none;">New Category</a>
</div>
EOF;
		}
		
		/***********************
		// RSS Blog Feed
		***********************/
		
		if (db_value("SELECT `StudioFeatures` FROM `Profiles` WHERE `ID`=$iMemberID"))
		{
			$RSSSettings = db_arr("SELECT `ShowRSSOnBlog`, `RSSBlogFeedURL`, `RSSTitle`, 
				`RSSFeedHomepage`, `RSSCategoryIcon`, `RSSPostsOnBlog`, `RSSLinesInSummaryOnBlog`
				FROM `ProfilesSettings` WHERE `IDMember`=$iMemberID");
			if ($RSSSettings['ShowRSSOnBlog'])
			{
				$RSSPostsCount = ($RSSSettings['RSSPostsOnBlog'] > 0) ? 
					$RSSSettings['RSSPostsOnBlog'] : 3;
				$RSSInNewWindow = ' target="_blank"';
				$maxCharsInDesc = $RSSSettings['RSSLinesInSummaryOnBlog'] * 30;

				$tal = new PHPTAL(BX_DIRECTORY_PATH_ROOT . "templates/tmpl_{$tmpl}/rssfeedonblog.html");
				$tal->rssFetcher = new rssFetcher($RSSSettings['RSSBlogFeedURL'], 
					$RSSPostsCount, $RSSInNewWindow, $maxCharsInDesc);			
				$tal->inNewWindow = $RSSInNewWindow;
				$tal->settingsID = md5($RSSSettings['RSSBlogFeedURL'] . 
					$RSSSettings['RSSFeedHomepage'] . $RSSSettings['RSSCategoryIcon'] . 
					$RSSSettings['RSSPostsOnBlog'] . $RSSSettings['RSSLinesInSummaryOnBlog']);
				$categoryIcon = ($RSSSettings['RSSCategoryIcon'] != '') ? $RSSSettings['RSSCategoryIcon']
					: "small_folder1.png";
				$tal->categoryIcon = "{$site['icons']}$categoryIcon";
						
				try {
				     $ret = $tal->execute();
				}
				catch (Exception $e) {
				     $ret = $e->getMessage();
				}
				
				$RSSTitle = (trim($RSSSettings['RSSTitle']) != '') ? $RSSSettings['RSSTitle'] : 'RSS Blog Feed';
				if (strlen($RSSTitle) > 15)
					$RSSTitle = trim(substr($RSSTitle, 0, 14)) . '...';
				$title_extra = '<div class="caption_item" style="padding-right:10px">
					<a href="'.$RSSSettings['RSSFeedHomepage'].'"'.$RSSInNewWindow.'>View Blog</a></div>';
				
				$RSSFeedSect = DesignBoxContent($RSSTitle, $ret, 1, $title_extra);
			}
		}		
		
		/***********************
		// My Links
		***********************/		
		
		$myLinksSettings = db_arr("SELECT `MyLinksBoxTitle`, `ShowMyLinksOnBlog` 
			FROM `ProfilesSettings` WHERE `IDMember`=$iMemberID");
		if ($myLinksSettings['ShowMyLinksOnBlog'])
		{
			$myLinksTitle = $myLinksSettings['MyLinksBoxTitle'];
			if (!$myLinksTitle) $myLinksTitle = _t('_Link to my other sites default');
			
			// Get saved mylinks for this member from database
			$query = "
				SELECT * FROM MyLinks 
				WHERE owner = $iMemberID
					AND TRIM(title) != '' 
					AND TRIM(url) != '' 
				ORDER BY isleft DESC, position";
			$savedLinks = fill_assoc_array(db_res($query));
			foreach ($savedLinks as $savedLink)
			{
				$target = ($savedLink['innewwindow']) ? '_blank' : '';
				$myLinks .= <<<EOF
				<div>
					<a href="{$savedLink['url']}" target="$target">
						<img src="{$site['icons']}{$savedLink['imagename']}" 
							style="vertical-align:middle"></a>
					<a href="{$savedLink['url']}" target="$target">
						{$savedLink['title']}</a>
				</div>
EOF;
			}
		
			if ($myLinks != '')
				$myLinksSect = DesignBoxContent($myLinksTitle, $myLinks, 1);
		}
		
		$sCategoriesSect = DesignBoxContent ( _t($sCategoriesC), $sCategories, 1 , $sCategoriesActions);
		$sTagsSect = DesignBoxContent ( _t($sTagsC), $sTagsVals, 1, '', '', '', '', 'float:left');
		
		$setDescCounter = '<script>document.getElementById("desc_counter").value = UnivStrLength(document.forms.EditBlogForm.Description.value);</script>';
		
		return $sDescriptionSect . $sCategoriesSect . $sTagsSect . 
			$RSSFeedSect . $myLinksSect . $setDescCounter;
	}

	/**
	 * Generate User`s Blog Page
	 *
	 * @param $iUserID - Blog owner ID
	 * @return HTML presentation of data
	 */
	function GenMemberBlog($iUserID = -1) {
		global $site;
		global $meta_keywords;
		global $meta_description;
		global $title;
		global $pageHeaderExtraContent;
		global $logged;
		global $iMemberID;
			
		$sRetHtml = '';
		$visitorID = $this->aBlogConf['visitorID'];
		$iMemberID = (int)$_REQUEST['ownerID'];
		if ($iUserID > 0)
			$iMemberID = $iUserID;
	
		$iCategoryID = (int)$_REQUEST['category'];
		if (!$iCategoryID && isset($_GET['sefCat']))
		{
			// Try to get category from SEF url
			$iCategoryID = db_value("SELECT `CategoryID` FROM `BlogCategories` 
				WHERE `OwnerID` = $iMemberID AND `caturltitle` = '{$_GET['sefCat']}'
				LIMIT 1");
		}

		$sEditC = _t('_Edit');
		$sDeleteC = _t('_Delete');
		$sSureC = _t("_Are you sure");
		$sPostCommentC = _t('_Post Comment');
		$sDescriptionC = _t('_Description');
		$sAddCommentC = _t('_Add comment');
		$sNewPostC = _t('_New Post');
		$sTagsC = _t('_Tags');
		$sPostsC = _t('_Posts');

		// Get owner info
		$sBlogsSQL = "
			SELECT `Blogs`. * , `Profiles`.`Nickname` as `OwnerNick`
			FROM `Blogs` 
			LEFT JOIN `Profiles` ON `Blogs`.`OwnerID` = `Profiles`.`ID`
			WHERE `Blogs`.`OwnerID` = {$iMemberID}
			LIMIT 1
		";
		$aBlogsRes = db_arr( $sBlogsSQL );
		if (mysql_affected_rows()==0)
		{				
			checkCommunityPermissions($visitorID);	
			return $this->GenCreateBlogForm();
		}
			
		$pageHeaderExtraContent = array();
		if (useSEF)
		{
			$pageHeaderExtraContent[0] = '<link href="'.$site['url'].$aBlogsRes['OwnerNick'].
				'blog/rss" rel="alternate" type="application/rss+xml" title="RSS"/>';
			$pageHeaderExtraContent[1] = '<link href="'.$site['url'].$aBlogsRes['OwnerNick'].
				'blog/atom" rel="alternate" type="application/atom+xml" title="Atom"/>';
		}
		else
		{
			$pageHeaderExtraContent[0] = '<link href="blogfeeds.php?type=rss&owner='.
				$iMemberID . '" rel="alternate" type="application/rss+xml" title="RSS" />';
			$pageHeaderExtraContent[1] = '<link href="blogfeeds.php?type=atom&owner='.
				$iMemberID . '" rel="alternate" type="application/atom+xml" title="Atom" />';
		}			
		
		$bOwner = ($visitorID == $aBlogsRes['OwnerID']);
		$bFriend = is_friends($visitorID, $aBlogsRes['OwnerID']);
		
		// Increase Views number of this Blog if visitor not owner
		if (!$bOwner)
			db_res("UPDATE `Blogs` SET `Views` = `Views` + 1 WHERE `ID` = {$aBlogsRes['ID']} LIMIT 1");
		
		if ($aBlogsRes['Locked'] == 1)
		{
			if (!$bFriend && !$bOwner && !$this->bAdminMode )
				return MsgBox(_t('_private_blog_for_friends'), 16, 2, 
					"<img src=\"{$site['icons']}lock.gif\" style=\"height:40px; margin-right:5px; vertical-align:middle\"/>");
		}
		elseif ($aBlogsRes['Locked'] == 2)
		{
			$bHotlistFriend = is_hotlist_friends($aBlogsRes['OwnerID'], $visitorID);	
			if (!$bHotlistFriend && !$bOwner && !$this->bAdminMode )
				return MsgBox(_t('_private_blog_by_invitation'), 16, 2, 
					"<img src=\"{$site['icons']}lock.gif\" style=\"height:40px; margin-right:5px; vertical-align:middle\"/>");
		}
		elseif ($aBlogsRes['Locked'] == 3)
		{
			if (!$logged['member'] && !$this->bAdminMode )
				return MsgBox(_t('_private_blog'), 16, 2, 
					"<img src=\"{$site['icons']}lock.gif\" style=\"height:40px; margin-right:5px; vertical-align:middle\"/>");
		}	
		elseif ($aBlogsRes['Locked'] == 4)
		{
			if (!$bOwner && !$this->bAdminMode)
				return MsgBox('Blog Locked', 16, 2, 
					"<img src=\"{$site['icons']}lock.gif\" style=\"height:40px; margin-right:5px; vertical-align:middle\"/>");
		}
		
		// Update syndicated blog posts
		if (trim($aBlogsRes['SyndicateUrl']) != '')
			$this->SyndicateBlogFeed($aBlogsRes['SyndicateUrl'], $aBlogsRes['ID'], $aBlogsRes['OwnerID']); 
				
		$title = (trim($aBlogsRes['Title']) != '') ? $aBlogsRes['Title'] : $aBlogsRes['Nickname'].' Blog';
		//$meta_description = $aBlogsRes['Description'];

		$sDateFormatPhp = getParam('php_date_format');
		
		$postsPerPage = 10;
		$curPage = (isset($_GET['page']) && (int)$_GET['page'] > 0) ? (int)$_GET['page'] : 1;
		$offset = ($curPage > 1) ? ($curPage - 1) * $postsPerPage : 0;	
		
		$sCategoryAddon = ($iCategoryID>0) ? "AND `BlogPosts`.`CategoryID` = {$iCategoryID}" : '';
		$sBlogPosts = '';

		$sBlogPostsSQL = "
			SELECT `BlogPosts`.*, COUNT(`CommentID`) AS `CountComments`, `BlogCategories`.*
			FROM `BlogPosts`
			LEFT JOIN `BlogPostComments`
			ON `BlogPosts`.`PostID`=`BlogPostComments`.`PostID`
			LEFT JOIN `BlogCategories`
			ON `BlogCategories`.`CategoryID`=`BlogPosts`.`CategoryID`
			WHERE `BlogCategories`.`OwnerId` = {$iMemberID}
			{$sCategoryAddon}
			GROUP BY `BlogPosts`.`PostID`
			ORDER BY `PostDate` DESC, `CountComments` DESC
			LIMIT $offset, $postsPerPage
		";

		$vBlogPosts = db_res( $sBlogPostsSQL );
		$sCurCategory = '';
		if ($iCategoryID>0) {
			$sBlogCategSQL = "
				SELECT `BlogCategories`.`CategoryName`
				FROM `BlogCategories`
				WHERE `BlogCategories`.`CategoryID` = {$iCategoryID}
				LIMIT 1
			";
			$aBlogCateg = db_arr($sBlogCategSQL);
			$sCurCategory = $aBlogCateg['CategoryName'];
		}
		
		while ( $aResSQL = mysql_fetch_assoc($vBlogPosts) ) 
				$sBlogPosts .= $this->GenPostString($aResSQL);
		
		// Get total posts number of this member
		$totalPosts = db_value("SELECT COUNT(*) FROM `BlogPosts`
			LEFT JOIN `BlogCategories` ON `BlogCategories`.`CategoryID`=`BlogPosts`.`CategoryID`
			WHERE `BlogCategories`.`OwnerId` = {$iMemberID} $sCategoryAddon");

		// Calculate total pages
		$totalPages = ceil($totalPosts / $postsPerPage);
		
		// Show pagination
		if ($totalPages > 1)
			$sBlogPosts .= $this->_pagination($totalPages, $curPage, 
				stripos($_SERVER['REQUEST_URI'], '&ownerID') === false);
		
		// Save first $wordsCount of first post to the meta description
		$wordsCount = $this->aBlogConf['blogDescMaxLength'];

		if (!mysql_num_rows($vBlogPosts)) 
			$meta_description = '';
		else
		{
			mysql_data_seek($vBlogPosts, mysql_num_rows($vBlogPosts) - 1);
			$lastRow = mysql_fetch_assoc($vBlogPosts);
			$meta_description = BreakLinesToSpaces($lastRow['PostText']);
			preg_match_all("/\w+/", $meta_description, $matches, PREG_OFFSET_CAPTURE);
			if (count($matches[0]) > $wordsCount)
			{
				$lastWordOffset = $matches[0][$wordsCount][1];
				$meta_description = substr($meta_description, 0, $lastWordOffset);
			}
			else 
				$meta_description = $meta_description;
		}

		$sNewPost = '';
		if ($bOwner /*|| $this->bAdminMode==TRUE*/ ) {
			$newPostLink = (useSEF) ? "{$site['url']}{$aBlogsRes['OwnerNick']}/new-blog-post"
				: "{$_SERVER['PHP_SELF']}?action=new_post";
			$sNewPost = <<<EOF
<div class="caption_item">
	<span style="vertical-align:middle;">
		<a href="$newPostLink">
			<img src="{$site['icons']}post_new.png" class="marg_icon" alt="{$sNewPostC}" title="{$sNewPostC}"/>
		</a>
	</span>
	<a href="$newPostLink" style="text-transform:none;">{$sNewPostC}</a>
</div>
EOF;
		}

		//<span style="vertical-align:middle;"><img src="{$site['icons']}rss.png" style="position:static;" alt="" /></span>
		$sCurCategory = ($sCurCategory!='')?' - '.$sCurCategory:'';
		if (useSEF)
		{
			$rssLink = "{$site['url']}{$aBlogsRes['OwnerNick']}/blog/rss";
			$atomLink = "{$site['url']}{$aBlogsRes['OwnerNick']}/blog/atom";
		}
		else
		{
			$rssLink = "{$site['url']}blogfeeds.php?type=rss&amp;owner=$iMemberID";
			$atomLink = "{$site['url']}blogfeeds.php?type=atom&amp;owner=$iMemberID";
		}
		$feedsLinks = "<div class=\"feeds_links\">
			<a href=\"$rssLink\"><img src=\"{$site['icons']}rss2.gif\" style=\"vertical-align:middle;\"/></a>
			<a href=\"$rssLink\">RSS</a> | <a href=\"$atomLink\">Atom</a>
			</div>";

		//$sPostsSect = DesignBoxContent ( _t($sPostsC), $sBlogPosts, 1, $sNewPost, $feedsLinks);
		$sPostsSect = DesignBoxContent ( $title, $sBlogPosts, 1, $sNewPost.$feedsLinks);
		$sRightSect = $this->GenMemberDescrAndCat($aBlogsRes,$iCategoryID);
		
		$sRetHtml = <<<EOF
<div>
	<div class="clear_both"></div>
	<div class="cls_info_left">
		{$sPostsSect}
	</div>
	<div class="cls_info">
		{$sRightSect}
	</div>
	<div class="clear_both"></div>
</div>
<div class="clear_both"></div>
EOF;
		$meta_keywords = implode(', ', array_slice(array_keys($this->tags), 0, $this->aBlogConf['blogNumberTagsAsKeywords']));
		
		return $sRetHtml;
	}

	/**
	 * SQL: Updating post by POSTed data
	 *
	 * @return MsgBox of result
	 */
	function ActionEditPost() {
		global $dir;

		$this->CheckLogged();

		$sSuccUpdPost = _t('_SUCC_UPD_POST');
		$sFailUpdPost = _t('_FAIL_UPD_ADV');

		$iCategoryID = process_db_input( (int)$_POST['categoryID'] );
		$sPostCaption = addslashes(clear_xss(process_pass_data($_POST['caption'] )));
		//$sPostText = addslashes(clear_xss(process_pass_data($_POST['blogText'] )));
		$sPostText = process_db_input($_POST['blogText']);
		$readPerm = process_db_input( $_POST['readPerm'] );
		if (!isset($_POST['commentPerm']) || $_POST['commentPerm'] == '')
			$commentPerm = $readPerm;
		else
			$commentPerm = process_db_input( $_POST['commentPerm'] );
		$sTagsPerm = process_db_input( $_POST['tags'] );
		$aTags = explodeTags($sTagsPerm);
		$sTagsPerm = implode(",", $aTags);
		$iPostID = (int)($_POST['EditedPostID']);

		$sCheckPostSQL = "SELECT `BlogCategories`.`OwnerID`
							FROM `BlogPosts`
							LEFT JOIN `BlogCategories` ON `BlogCategories`.`CategoryID`=`BlogPosts`.`CategoryID`
							WHERE `PostID`={$iPostID}
						";
		$PostID = db_arr($sCheckPostSQL);
		$iPostOwnerID = $PostID['OwnerID'];
		if (($this->aBlogConf['visitorID'] == $iPostOwnerID || $this->bAdminMode) && $iPostID > 0) 
		{
			// Check for correct read/comment permissions
			if ($readPerm == 'members' && $commentPerm == 'public')
				return MsgBox('If read level is "Members only", then commenting can be granted only to "Members only", "Friends only" or "Favorites only"');
			elseif ($readPerm == 'friends' && $commentPerm != 'friends')
				return MsgBox('If read level is "Friends only", commenting can be only granted to "Friends only"');
			elseif ($readPerm == 'favorites' && $commentPerm != 'favorites')
				return MsgBox('If read level is "Favorites only", commenting can be only granted to "Favorites only"');
			
			$sPhotosSQL = "SELECT `PostPhoto` FROM `BlogPosts` WHERE `PostID` = {$iPostID} LIMIT 1";
			$aFiles = db_arr($sPhotosSQL);
			$sFileName = $aFiles['PostPhoto'];

			// Check image action
			$uploaded = false;
			if (isset($_POST['imageAction']))
			{
				if ($_POST['imageAction'] == 'clear')
				{
					$bigImageName = $dir['blogImage'] . 'big_'. $sFileName;
					$smallImageName = $dir['blogImage'] . 'small_'. $sFileName;		
					if (@unlink($bigImageName) && @unlink($smallImageName))
						$sFileName = '';
				}
				elseif ($_POST['imageAction'] == 'update')
					$uploaded = true;
			}
			else
				$uploaded = true;
				
			if ($uploaded && 0 < $_FILES['BlogPic']['size'] && 0 < strlen( $_FILES['BlogPic']['name'] ) ) {
				$sFileNameExt = '';
				$sFileName = 'blog_' . $iPostID;

				$sExt = moveUploadedImage( $_FILES, 'BlogPic', $dir['blogImage'] . $sFileName, '', false );
				if( strlen( $sExt ) && !(int)$sExt ) {
					$sFileNameExt = $sFileName.$sExt; 
					imageResize( $dir['blogImage'] . $sFileNameExt, $dir['blogImage'] . 'small_' . $sFileNameExt, $this->iIconSize / 2, $this->iIconSize / 2);
					imageResize( $dir['blogImage'] . $sFileNameExt, $dir['blogImage'] . 'big_' . $sFileNameExt, $this->iThumbSize, $this->iThumbSize);
					@unlink( $dir['blogImage'] . $sFileNameExt );
				}
			}

			$sAutoApprovalVal = (getParam('blogAutoApproval')=='on') ? "approval" : "disapproval";
			$fileName = ($uploaded) ? $sFileNameExt : $sFileName;
			$sQuery = "
				UPDATE `BlogPosts` SET
				`CategoryID`={$iCategoryID},
				`PostCaption`='{$sPostCaption}',
				`PostText`='{$sPostText}',
				`PostCommentPermission`='{$commentPerm}',
				`PostReadPermission`='{$readPerm}',
				`Tags`='{$sTagsPerm}',
				`PostStatus`='{$sAutoApprovalVal}',
				`PostPhoto`='{$fileName}'
				WHERE `PostID`={$iPostID}
			";
			$vSqlRes = db_res( $sQuery );
			//$sRet = (mysql_affected_rows()>0) ? _t($sSuccUpdPost) : _t($sFailUpdPost);
			$sRet = '';
			if (!$vSqlRes)
				$sRet = _t($sFailUpdPost);
			elseif (mysql_affected_rows() > 0) 
				$sRet = _t($sSuccUpdPost);
			reparseObjTags( 'blog', $iPostID );
			if ($sRet != '')
				return MsgBox($sRet);
			else 
				return '';
		} elseif($this->aBlogConf['visitorID'] != $iPostOwnerID) {
			return MsgBox(_t('_Hacker String'));
		} else {
			return MsgBox(_t('_Error Occured'));
		}
	}

	/**
	 * SQL: Delete post by POSTed data
	 *
	 * @return MsgBox of result
	 */
	function ActionDeletePost() {
		$this->CheckLogged();

		$iPostID = (int)($_POST['DeletePostID']);

		$sCheckPostSQL = "SELECT `BlogCategories`.`OwnerID`
							FROM `BlogPosts`
							LEFT JOIN `BlogCategories` ON `BlogCategories`.`CategoryID`=`BlogPosts`.`CategoryID`
							WHERE `PostID`={$iPostID}
						";
		$PostID = db_arr($sCheckPostSQL);
		$iPostOwnerID = $PostID['OwnerID'];
		if (($this->aBlogConf['visitorID'] == $iPostOwnerID || $this->bAdminMode) && $iPostID > 0) {
			db_res( "DELETE FROM `BlogPostComments` WHERE `PostID` = {$iPostID}" );
			$sQuery = "DELETE FROM `BlogPosts` WHERE `BlogPosts`.`PostID` = {$iPostID} LIMIT 1";
			$vSqlRes = db_res( $sQuery );
			$sRet = (mysql_affected_rows()>0) ? _t('_post_successfully_deleted') : _t('_failed_to_delete_post');
			
			// Clear this post and comments assigned to it from newsfeedtrack table
			db_res("DELETE FROM NewsFeedTrack WHERE mediaid=$iPostID AND (type=7 OR type=8)");
			
			reparseObjTags( 'blog', $iPostID );
			return MsgBox($sRet);
		} elseif($this->aBlogConf['visitorID'] != $iPostOwnerID) {
			return MsgBox(_t('_Hacker String'));
		} else {
			return MsgBox(_t('_Error Occured'));
		}
	}

	/**
	 * Generate User`s Blog Post Page
	 *
	 * @return HTML presentation of data
	 */
	function GenPostPage($iPostID = 0) {
		global $site;
		global $aBreadCramp;
		global $meta_keywords;
		global $meta_description;
		global $title;
		global $logged;

		if (!$iPostID)
			$iPostID = (int)$_REQUEST['post_id'];
		if ($this->iLastPostedPostID>0) {
			$iPostID = $this->iLastPostedPostID;
			$this->iLastPostedPostID = -1;
		}

		$sCategoryC = _t( '_Category' );
		$sPostC = _t( '_Post' );
		$sEditC = _t('_Edit');
		$sDeleteC = _t('_Delete');
		$sCommentsC = _t('_comments');

		$sRetHtml = '';
		
		$visitorID = $this->aBlogConf['visitorID'];
		if (isset($_POST['subscribe']))
		{
			if ($_POST['subscribe'])
				db_res("INSERT INTO BlogPostCommentsNotifications
					(PostID, UserID) VALUES ($iPostID, $visitorID)
					ON DUPLICATE KEY UPDATE PostID = $iPostID, UserID = $visitorID");
			else
				db_res("DELETE FROM BlogPostCommentsNotifications
					WHERE PostID = $iPostID AND UserID = $visitorID");
		}

		$sPostedBySQL = "
			SELECT `BlogCategories`.`OwnerID` FROM `BlogCategories`
			LEFT JOIN `BlogPosts` ON `BlogCategories`.`CategoryID`=`BlogPosts`.`CategoryID`
			WHERE `BlogPosts`.`PostID` = {$iPostID}
			LIMIT 1
		";
		$aPostBy = db_arr($sPostedBySQL);

		if (mysql_affected_rows()==0) {
			return MsgBox(_t('_No such blog post'));
		}

		$sBlogsSQL = "
			SELECT `Blogs`. * , `Profiles`.`Nickname` as OwnerNick
			FROM `Blogs` 
			LEFT JOIN `Profiles` ON `Blogs`.`OwnerID` = `Profiles`.`ID`
			WHERE `Blogs`.`OwnerID` = {$aPostBy['OwnerID']}
			LIMIT 1
		";
		$aBlogInfo = db_arr($sBlogsSQL);
	
		$bOwner = ($this->aBlogConf['visitorID']==$aBlogInfo['OwnerID']) ? true : false;
		$bFriend = is_friends( $this->aBlogConf['visitorID'], $aBlogInfo['OwnerID'] );
		$bHotlistFriend = is_hotlist_friends($aBlogInfo['OwnerID'], $this->aBlogConf['visitorID']);
		
		// Increase Views number of this Blog if visitor not owner and not comment manipulation
		if (!$bOwner && !isset($_POST['CommPostID']) && !isset($_POST['EPostID']) && !isset($_POST['DPostID']))
			db_res("UPDATE `Blogs` SET `Views` = `Views` + 1 WHERE `ID` = {$aBlogInfo['ID']} LIMIT 1");
		
		// Check blog permissions
		if ($aBlogInfo['Locked'] == 1)
		{
			if (!$bFriend && !$bOwner && !$this->bAdminMode )
				return MsgBox('Locked for friends only', 16, 2,
					"<img src=\"{$site['icons']}lock.gif\" style=\"height:40px; margin-right:5px; vertical-align:middle\"/>");
		}
		elseif ($aBlogInfo['Locked'] == 2)
		{
			if (!$bHotlistFriend && !$bOwner && !$this->bAdminMode )
				return MsgBox('Locked by invitation only', 16, 2,
					"<img src=\"{$site['icons']}lock.gif\" style=\"height:40px; margin-right:5px; vertical-align:middle\"/>");
		}
		elseif ($aBlogInfo['Locked'] == 3)
		{
			if (!$logged['member'] && !$this->bAdminMode )
				return MsgBox(_t('_private_blog'), 16, 2,
					"<img src=\"{$site['icons']}lock.gif\" style=\"height:40px; margin-right:5px; vertical-align:middle\"/>");
		}
		
		$sBlogPostSQL = "
				SELECT `BlogPosts`.*, COUNT(`CommentID`) AS `CountComments`, `BlogCategories`.*
				FROM `BlogPosts`
				LEFT JOIN `BlogPostComments`
				ON `BlogPosts`.`PostID`=`BlogPostComments`.`PostID`
				LEFT JOIN `BlogCategories`
				ON `BlogCategories`.`CategoryID`=`BlogPosts`.`CategoryID`
				WHERE `BlogPosts`.`PostID` = {$iPostID}
				GROUP BY `BlogPostComments`.`PostID`
				ORDER BY `CountComments` DESC, `PostDate` DESC
		";
		$aBlogPost = db_arr( $sBlogPostSQL );

		// Check blog post read permission
		if ($aBlogPost['PostReadPermission'] == 'friends')
		{
			if (!$bFriend && !$bOwner && !$this->bAdminMode )
				return MsgBox('Locked for friends only', 16, 2,
					"<img src=\"{$site['icons']}lock.gif\" style=\"height:40px; margin-right:5px; vertical-align:middle\"/>");
		}
		elseif ($aBlogPost['PostReadPermission'] == 'favorites')
		{
			if (!$bHotlistFriend && !$bOwner && !$this->bAdminMode )
				return MsgBox('Locked by invitation only', 16, 2,
					"<img src=\"{$site['icons']}lock.gif\" style=\"height:40px; margin-right:5px; vertical-align:middle\"/>");
		}
		elseif ($aBlogPost['PostReadPermission'] == 'members')
		{
			if (!$logged['member'] && !$this->bAdminMode )
				return MsgBox(_t('_private_blog'), 16, 2,
					"<img src=\"{$site['icons']}lock.gif\" style=\"height:40px; margin-right:5px; vertical-align:middle\"/>");
		}
		
		$meta_keywords = str_replace(',', ', ', $aBlogPost['Tags']);
		$title = $aBlogPost['PostCaption'];
		
		// save first $wordsCount of post text to the meta description
		$wordsCount = $this->aBlogConf['blogPostDescMaxLength'];
		$meta_description = BreakLinesToSpaces($aBlogPost['PostText']);
		preg_match_all("/\w+/", $meta_description, $matches, PREG_OFFSET_CAPTURE);
		if (count($matches[0]) > $wordsCount)
		{
			$lastWordOffset = $matches[0][$wordsCount][1];
			$meta_description = substr($meta_description, 0, $lastWordOffset);
		}
		else 
			$meta_description = $meta_description;
		
		if( 'friends' == $aBlogPost['PostReadPermission'] && !$bFriend && !$bOwner && !$this->bAdminMode ) {
			$sFriendsC .= MsgBox('Locked for friends only', 16, 2,
				"<img src=\"{$site['icons']}lock.gif\" style=\"height:40px; margin-right:5px; vertical-align:middle\"/>");
			$sRightSection = $this->GenMemberDescrAndCat($aBlogInfo);
			$sRetHtml .= <<<EOF
<div>
	<div class="clear_both"></div>
	<div class="cls_info_left">
		<div>
			<div class="disignBoxFirst">
				<div class="boxFirstHeader">{$sPostC}</div>
				<div class="boxContent">
					{$sFriendsC}
				</div>
			</div>
		</div>
		<div class="clear_both"></div>
	</div>
	<div class="cls_info">
		{$sRightSection}
	</div>
	<div class="clear_both"></div>
</div>
<div class="clear_both"></div>
EOF;
		} else {

			// Prepare comment pagination
			$iDivis = 5;
			$iCurr  = 1;
			
			if (!isset($_GET['commPage']))
				$sLimit =  ' LIMIT 0,'.$iDivis;
			elseif ($_GET['commPage'] > 0)
			{
				$iCurr = (int)$_GET['commPage'];
				$sLimit =  ' LIMIT '.($iCurr - 1)*$iDivis.','.$iDivis;
			}
			else 
				$sLimit = '';			

			$iNums = db_value("SELECT COUNT(`CommentID`) FROM `BlogPostComments` WHERE `PostID` = $iPostID");
			$sNav = ($iNums > $iDivis && $sLimit != '') ? commentNavigation($iNums, $iDivis, $iCurr) : '';
		
			if ($sNav != '')
			{
				$commPagePos = strpos($_SERVER['REQUEST_URI'], 'commPage');	
				$baseUrl = ($commPagePos !== false) ? substr($_SERVER['REQUEST_URI'], 0, $commPagePos - 1) : $_SERVER['REQUEST_URI'];
				$allCommentsHref = (strpos($baseUrl, '&') !== false) ? 
					"$baseUrl&amp;commPage=0" : "$baseUrl?commPage=0";
				$sNav .= "<a href=\"$allCommentsHref\" style=\"float:left; clear:left; 
					margin-bottom:15px; width:140px; padding-left:10px\">View all $iNums comments</a>";
			}
			
			$oComments = new BxDolComments(2);
			$sPostComm = $oComments->PrintCommentSection($iPostID, "$iNums $sCommentsC", $sLimit, $sNav);
			$sPostString = $this->GenPostString($aBlogPost,3);
			
			$blogLink = (useSEF) ? "{$site['url']}{$aBlogInfo['OwnerNick']}/blog" :
				"{$site['url']}blogs.php?action=show_member_blog&ownerID={$aBlogInfo['OwnerID']}";
			$returnToBlogLink = <<<EOF
<div class="caption_item">
	<span style="vertical-align:middle;">
		<a href="$blogLink">
			<img src="{$site['icons']}ico_bluearrow.gif" class="marg_icon" alt="Return to Blog" title="Return to Blog"/>
		</a>
	</span>
	<a href="$blogLink" style="text-transform:none;">Return to Blog</a>
</div>
EOF;
			$postSection = DesignBoxContent($sPostC, $sPostString, 1, $returnToBlogLink);
			$sRightSection = $this->GenMemberDescrAndCat($aBlogInfo);

			$sRetHtml = <<<EOF
<div>
	<div class="clear_both"></div>
	<div class="cls_info_left">
		{$postSection}
		<div class="clear_both"></div>
		<div id="comments">
			{$sPostComm}
		</div>
	</div>
	<div class="cls_info">
		{$sRightSection}
	</div>
	<div class="clear_both"></div>
</div>
<div class="clear_both"></div>
EOF;
		}

$sRetHtml .= <<<EOF
<script>
function AttachLoadHandler(functionName)
{
	if (window.addEventListener) 
		window.addEventListener('load', functionName, false);
	else if (document.addEventListener) 
		document.addEventListener('load', functionName, false);
	else if (window.attachEvent) 
		window.attachEvent('onload', functionName);
}


function ShowPostCommentForm()
{
	document.getElementById('answer_form_to_0').style.display = 'block';
	document.getElementById('add_comment_label').style.display = 'none';
	location.hash = '#answer_form_to_0';
}

function ShowComments()
{
	location.hash = '#comments';
}

if (location.hash == '#answer_form_to_0')
	 AttachLoadHandler(ShowPostCommentForm);
	 
</script>		
EOF;
		
	if (isset($_GET['add_comment']))
		$sRetHtml .= "<script>AttachLoadHandler(ShowPostCommentForm);</script>";		
		
	if (isset($_GET['commPage']))
		$sRetHtml .= "<script>AttachLoadHandler(ShowComments);</script>"; 
	
		return $sRetHtml;
	}

	/**
	 * Generate Form for NewPost/EditPost
	 *
	 * @param $iPostID - Post ID
	 * @param $arrErr - Array for PHP validating
	 * @return HTML presentation of data
	 */
	function AddNewPostForm($iPostID=-1, $arrErr = NULL) {
		global $site;

		$this->CheckLogged();

		$sPostCaptionC = _t('_Post') . ' ' . _t('_Caption');
		$sCharactersLeftC = _t('_characters_left');
		$sPostTextC = _t('_Post') . ' ' . _t('_Text');
		$sPleaseSelectC = _t('_please_select');
		$sAssociatedImageC = 'Upload image to blog post:';
		$sPostCommentPerC = _t('_post_comment_per');
		$sPublicC = _t('_Public');
		$sMembersOnlyC = _t('_Members only');
		$sFriendsOnlyC = _t('_Friends only');
		$sFavoritesOnlyC = _t('_Favorites only');
		$sPostReadPerC = _t('_post_read_per');
		$sAddBlogC = _t('_Add Post');
		$sCommitC = _t('_Apply Changes');
		$sTagsC = _t('_Tags');
		$sNewPostC = _t('_New Post');
		$sEditPostC = _t('_Edit Post');

		$sBlogsSQL = "
			SELECT `Blogs`. *
			FROM `Blogs` 
			WHERE `Blogs`.`OwnerID` = {$this->aBlogConf['visitorID']}
			LIMIT 1
		";
		$aBlogsRes = db_arr( $sBlogsSQL );		
		if (mysql_affected_rows()==0) {
			checkCommunityPermissions($this->aBlogConf['visitorID']);
			return $this->GenCreateBlogForm();
		}

		$sRetHtml = '';

		$sCATIDstyle = ($arrErr['CategoryID'] ? 'block' : 'none');
		$sCPTstyle = ($arrErr['Caption'] ? 'block' : 'none');
		$sPTstyle = ($arrErr['PostText'] ? 'block' : 'none');
		$sCPstyle = ($arrErr['CommentPerm'] ? 'block' : 'none');
		$sRPstyle = ($arrErr['ReadPerm'] ? 'block' : 'none');

		$sCATIDmsg = $arrErr['CategoryID'] ? $arrErr['CategoryID'] : '';
		$sCPTmsg = $arrErr['Caption'] ? $arrErr['Caption'] : '';
		$sPTmsg = $arrErr['PostText'] ? $arrErr['PostText'] : '';
		$sCPmsg = $arrErr['CommentPerm'] ? $arrErr['CommentPerm'] : '';
		$sRPmsg = $arrErr['ReadPerm'] ? $arrErr['ReadPerm'] : '';

		$sPostCaption = '';
		$sPostText = '';
		$sPostImage = '';
		$sPostTags = '';
		$sCheckedReadPostPermP = 'checked ';
		$sCheckedReadPostPermM = '';
		$sCheckedReadPostPermF = '';
		$sCheckedReadPostPermFv = '';
		$sCheckedCommPermP = 'checked ';
		$sCheckedCommPermM = '';
		$sCheckedCommPermF = '';
		$sCheckedCommPermFv = '';
		
		$sPostPicture = '';
		$sPostPictureTag = '';
		$sPostAction = 'add_post';
		$iSavedCategoryID = -1;

		if ($iPostID>0) {
			$sBlogPostsSQL = "SELECT * FROM `BlogPosts` WHERE `PostID` = {$iPostID} LIMIT 1";
			$aBlogPost = db_arr( $sBlogPostsSQL );
			$sPostCaption = $aBlogPost['PostCaption'];
			$sPostText = $aBlogPost['PostText'];
			$sPostImage = $aBlogPost['PostPhoto'];
			$sPostTags = $aBlogPost['Tags'];
			$sPostPicture = $aBlogPost['PostPhoto'];
			$sSpacerName = $site['url'].$this -> sSpacerPath;
			if ($sPostImage != '') 
				$sPostPictureTag = '<div class="postImage"><img alt="" 
					style="width: 110px; 
					background-image: url('.$site['blogImage'].'big_'.$sPostImage.');
					background-repeat:no-repeat" src="'.$sSpacerName.'"/></div>';
			$iSavedCategoryID = $aBlogPost['CategoryID'];
			$sAddBlogC = $sCommitC;
			$sPostAction = 'post_updated';
			$sEditIdStr = '<input type="hidden" name="EditedPostID" value="'.$iPostID.'">';
			$sCheckedReadPostPerm = $aBlogPost['PostReadPermission'];
			$sCheckedCommPerm = $aBlogPost['PostCommentPermission'];
		} else {
			$iSavedCategoryID = process_db_input( (int)$_POST['categoryID'] );

			$sPostCaption = process_db_input( $_POST['caption'] );
			$sPostText = process_db_input( $_POST['blogText'] );
			$sPostImage = '';
			$sPostTags = process_db_input( $_POST['tags'] );
			$sCheckedReadPostPerm = (isset($_POST['readPerm'])) ? process_db_input($_POST['readPerm']) : '';
			$sCheckedCommPerm = (isset($_POST['commentPerm'])) ? process_db_input($_POST['commentPerm']) : '';
		}
		
		// Init read post permission
		if ($sCheckedReadPostPerm=='members')
			$sCheckedReadPostPermM = 'checked ';
		elseif ($sCheckedReadPostPerm=='friends')
			$sCheckedReadPostPermF = 'checked ';
		elseif ($sCheckedReadPostPerm=='favorites')
			$sCheckedReadPostPermFv = 'checked ';
		else
			$sCheckedReadPostPermP = 'checked ';
			
		// Init comment post permission
		if ($sCheckedCommPerm=='members')
			$sCheckedCommPermM = 'checked ';
		elseif ($sCheckedCommPerm=='friends')
			$sCheckedCommPermF = 'checked ';
		elseif ($sCheckedCommPerm=='favorites')
			$sCheckedCommPermFv = 'checked ';
		else
			$sCheckedCommPermP = 'checked ';

		$iOwner = $this->aBlogConf['visitorID'];
		if ($iPostID>0)
			$iOwner = db_value("SELECT `OwnerID` FROM `BlogCategories`
								INNER JOIN `BlogPosts` ON `BlogPosts`.`CategoryID` = `BlogCategories`.`CategoryID` 
								WHERE `BlogPosts`.`PostID` = {$iPostID}");

		//$sCategories = '';
		$sCategoriesSQL = "
			SELECT * 
			FROM `BlogCategories`
			WHERE `OwnerId` = {$iOwner}
			ORDER BY CategoryType DESC
		";

		$vCategories = db_res( $sCategoriesSQL );
		$sCategOptions = '';

		while ( $aCategories = mysql_fetch_assoc($vCategories) ) {
			if ($iSavedCategoryID>0 && $iSavedCategoryID==$aCategories['CategoryID'] )
			{
				$sSelected = ' selected="selected"';
				$categoryIcon = "small_{$aCategories['CategoryPhoto']}";
			}
			else
				$sSelected = '';
			$sCategOptions .= '<option value="'.$aCategories['CategoryID'].'"'.$sSelected.'>'.process_line_output(strmaxtextlen($aCategories['CategoryName'])).'</option>'."\n";
		}
		/*
		if (!$categoryIcon)	$categoryIcon = 'folder.png';
		$sCategPicture = '<img src="'.$site['icons'].$categoryIcon.'" style="position:static;" alt="'.$sPleaseSelectC.'" />';
		*/
		$sCategPicture = 'Category:';
		$sCategSelect = '<select name="categoryID" id="categoryID" >'.$sCategOptions.'</select>';
		$sCategPictSpans = $this->GenCenteredActionsBlock($sCategPicture, $sCategSelect);

		$showAdvancedTextEditor = true;
		if ($showAdvancedTextEditor)
			$textEditorCode = 
				'<script src="inc/rte/js/richtext-blog.js" type="text/javascript" language="javascript"></script>
				<script src="inc/rte/js/config-blog.js" type="text/javascript" language="javascript"></script>
				<script>
					initRTE("'. jsAddSlashes($sPostText) .'");
				</script>';
		else 
			$textEditorCode = "<textarea name='blogText' rows=20 cols=60 class='classfiedsTextArea'
				style='width:630px;' id='desc'>{$sPostText}</textarea>";
		
		$imageSection = <<<EOF
			<div style="float:left">
				<div class="uploadImageCaption">$sAssociatedImageC</div>
				<div class="imageInput"><input type="file" name="BlogPic"></div>
				<div class="imageHint">(Only .jpg, .gif and .png - Max width 500 pixels)</div>
			</div>
			<div class="imageTip">
				<img src="{$site['icons']}ico_photo.gif" style="float:left"/>
				<span class="imageTipText">
					<b>Image Tip:</b> Images are automatically left aligned 
					to the top of your blog post. To add more or control their
					alignment, use the image icon (<img src="{$site['icons']}ico_img.gif" 
					style="vertical-align:top"/>) on the editor toolbar.
				</span>
			</div>			
EOF;
		if ($sPostImage != '')
			$imageSection .= <<<EOF
				<div class="imageOptions">
					<input type="radio" name="imageAction" value="keep" checked=""/>Keep image
					<input type="radio" name="imageAction" value="update"/>Update image
					<input type="radio" name="imageAction" value="clear"/>Clear image
				</div>
				$sPostPictureTag
EOF;
		
$WordWarning = '<div class="WordWarning">
	<img src="'.$site['icons'].'ico_MS-Word.gif" 
		style="vertical-align:top; float:left; margin-right:10px"/>
	<span style="color:red; font-weight:bold">ATTENTION:</span><br/>
		We do NOT recommend you paste text directly into the box from Microsoft Word. 
		For best results, instead you should paste it into Notepad and then from Notepad you can paste it into the box.
</div>';

$YouTubeWarning = '<div class="YouTubeWarning">
	<img src="'.$site['icons'].'ico_YouTube.gif" 
		style="vertical-align:top; float:left; margin-right:10px"/>
	<span style="font-weight:bold">Video Tip:</span><br/>
		You can include videos directly from YouTube and other video sites into your 
		blog posts by pasting their &lt;embed&gt; code into the \'code\' view of this window.
	<img src="'.$site['icons'].'code-view.gif" style="margin-top:10px"/>
</div>';
			
		$sRetHtml .= <<<EOF
<div class="categoryBlock">
	<form action="{$_SERVER['PHP_SELF']}?action=edit_post&amp;ownerID=$iOwner" 
		id="addPostForm" enctype="multipart/form-data" method="post">
		<div class="margin_bottom_10">
			{$sPostCaptionC} ( <span id="captCounter">{$this->aBlogConf['blogCaptionMaxLenght']}</span> {$sCharactersLeftC} )
		</div>
		<div class="margin_bottom_10">
			<div class="edit_error" style="display:{$sCPTstyle}">
				{$sCPTmsg}
			</div>
			<input type="text" size="70" name="caption" id="caption" class="categoryCaption1" value="{$sPostCaption}" onkeydown="return charCounter('caption', '{$this->aBlogConf['categoryCaptionMaxLenght']}', 'captCounter');" />
		</div>
		<div class="margin_bottom_10">
			{$sTagsC} (related keywords about your blog post)
		</div>
		<div class="margin_bottom_10">
			<input type="text" size="70" name="tags" id="tags" value="{$sPostTags}" />
		</div>
		<div style="margin-bottom:10px;">
			<div class="edit_error" style="display:{$sCATIDstyle}">
				{$sCATIDmsg}
			</div>
			{$sCategPictSpans}
		</div>
			
		<div class="margin_bottom_10">{$sPostTextC}</div>
		<div class='blogTextAreaKeeper' style="float:left; clear:left;width:565px; overflow:hidden">
			<div class='edit_error' style='display:{$sPTstyle}'>{$sPTmsg}</div>
			{$textEditorCode}
		</div>
		
		$WordWarning
		$YouTubeWarning
		
		<div class="clear_both"></div>

		<div class="assocImageBlock">{$imageSection}</div>
		
		<script>
			function CheckCommentPerm(readPerm)
			{
				var commentOption = document.getElementById('addPostForm').commentPerm;
				var valueToSelect = '';
				var valueToDisable = '';
				var dis = false;
				if (readPerm == 'members')
					valueToDisable = 'public';
				else if (readPerm == 'friends')
				{
					valueToSelect = 'friends';
					dis = true;
				}
				else if (readPerm == 'favorites')
				{
					valueToSelect = 'favorites';
					dis = true;
				}	
				var selectNext = false;							
				for (var i = 0; i < commentOption.length; i++)
				{
					if (commentOption[i].value == valueToSelect || selectNext)
					{
						commentOption[i].checked = 'checked';
						if (selectNext) 
							selectNext = false;
					}
					if (commentOption[i].value == valueToDisable)
					{
						commentOption[i].disabled = true;
						if (commentOption[i].checked)
							selectNext = true;
					}
					else
						commentOption[i].disabled = dis;
				}
			}
		</script>
		
		<div class="permissions">
			<div style="float:left; position:relative; width:50%">
				<div class="margined_left">{$sPostReadPerC}:</div>
				<div class="margined_left">
					<div class="edit_error" style="display:{$sRPstyle}">
						{$sRPmsg}
					</div>
					<input type="radio" {$sCheckedReadPostPermP} name="readPerm" 
						value="public" onclick="CheckCommentPerm(this.value)"/>
					{$sPublicC}<br />
					<input type="radio" {$sCheckedReadPostPermM} name="readPerm" 
						value="members" onclick="CheckCommentPerm(this.value)"/>
					{$sMembersOnlyC}<br />
					<input type="radio" {$sCheckedReadPostPermF} name="readPerm" 
						value="friends" onclick="CheckCommentPerm(this.value)"/>
					{$sFriendsOnlyC}<br />
					<input type="radio" {$sCheckedReadPostPermFv} name="readPerm" 
						value="favorites" onclick="CheckCommentPerm(this.value)"/>
					{$sFavoritesOnlyC}
				</div>			
			</div>
			<div style="float:left; position:relative; padding-left:30px; border-left:1px solid #CCCCCC">
				<div class="margined_left">{$sPostCommentPerC}:</div>
				<div class="margined_left">
					<div class="edit_error" style="display:{$sCPstyle}">
						{$sCPmsg}
					</div>
					<input type="radio" {$sCheckedCommPermP} name="commentPerm" value="public" checked="checked" />
					{$sPublicC}<br />
					<input type="radio" {$sCheckedCommPermM} name="commentPerm" value="members" />
					{$sMembersOnlyC}<br />
					<input type="radio" {$sCheckedCommPermF} name="commentPerm" value="friends" />
					{$sFriendsOnlyC}<br />
					<input type="radio" {$sCheckedCommPermFv} name="commentPerm" value="favorites" />
					{$sFavoritesOnlyC}
				</div>
			</div>
			<div class="clear_both"></div>
		</div>
			
		<div class="submit">
			<input type="submit" value="{$sAddBlogC}" />
			<input type="hidden" name="action" value="{$sPostAction}" />
			<input type="hidden" name="show" value="blogList" />
			{$sEditIdStr}
		</div>
	</form>
</div>

		<script>
			var readPerm = '$sCheckedReadPostPerm';
			CheckCommentPerm(readPerm);
		</script>
EOF;
		$formCaption = ($iPostID > 0) ? $sEditPostC : $sNewPostC;
		return DesignBoxContent ($formCaption, $sRetHtml, 1);
	}

	/**
	 * Compose Array of posted data before validating (add/delete a post)
	 *
	 * @return Array
	 */
	function GetPostArrByPostValues() {
		$iCategoryID = process_db_input( (int)$_POST['categoryID'] );
		$sPostCaption = process_db_input( $_POST['caption'] );
		$sPostText = process_db_input( $_POST['blogText'] );
		$commentPerm = process_db_input( $_POST['commentPerm'] );
		$readPerm = process_db_input( $_POST['readPerm'] );
		$sTags = process_db_input( $_POST['tags'] );

		$arr = array('CategoryID' => $iCategoryID, 'Caption' => $sPostCaption, 'PostText' => $sPostText,
			'CommentPerm' => $commentPerm, 'ReadPerm' => $readPerm, 'Tags' => $sTags);
		return $arr;
	}

	/**
	 * Compose Array of errors during filling (validating)
	 *
	 * @param $arrAdv	Input Array with data
	 * @return Array with errors
	 */
	function GetCheckErrors( $arrAdv ) {
		$arrErr = array();
		foreach( $arrAdv as $sFieldName => $sFieldValue ) {
			switch( $sFieldName ) {
				case 'CategoryID':
					if( $sFieldValue < 1)
						$arrErr[ $sFieldName ] = "{$sFieldName} is required";
				break;
				case 'Caption':
					if( !strlen($sFieldValue) )
						$arrErr[ $sFieldName ] = "{$sFieldName} is required";
				break;
				case 'PostText':
					if( strlen($sFieldValue) < 50 )
						$arrErr[ $sFieldName ] = "Blog posts must contain a minimum of 50 characters.";
				break;
				case 'ReadPerm':
					if( !strlen($sFieldValue) )
						$arrErr[ $sFieldName ] = "{$sFieldName} is required";
				break;
			}
		}
		return $arrErr;
	}

	/**
	 * Adding a New Post SQL
	 *
	 * @param $iLastID - returning Last Inserted ID (SQL) (just try)
	 * @return HTML presentation of data
	 */
	function ActionAddNewPost(&$iLastID) {
		global $dir;

		$this->CheckLogged();

		$iCategoryID = process_db_input( (int)$_POST['categoryID'] );

		$sCheckPostSQL = "SELECT `OwnerID`
							FROM `BlogCategories`
							WHERE `CategoryID`={$iCategoryID}
						";
		$aCategoryOwner = db_arr($sCheckPostSQL);
		$iCategoryOwnerID = $aCategoryOwner['OwnerID'];
		if ($this->aBlogConf['visitorID'] == $iCategoryOwnerID && $iCategoryID > 0) {
			$sPostCaption = process_db_input( $_POST['caption'] );
			$sPostText = process_db_input( $_POST['blogText'] );
			$commentPerm = process_db_input( $_POST['commentPerm'] );
			$readPerm = process_db_input( $_POST['readPerm'] );
			$sTagsPerm = process_db_input( $_POST['tags'] );
			$aTags = explodeTags($sTagsPerm);
			$sTagsPerm = implode(",", $aTags);
			$urltitle = process_db_input(GetSEFUrl($sPostCaption, sefPostWords));
			
			// Check for existing urltitle in blog posts of this member
			$urlTitlesRows = fill_assoc_array(db_res("SELECT `urltitle` FROM BlogPosts 
				LEFT JOIN BlogCategories USING (CategoryID) 
				WHERE BlogCategories.OwnerID = '$iCategoryOwnerID' AND `urltitle` LIKE '$urltitle%'"));
			$urlTitles = array();
			$maxUrlTitleVer = 1;
			foreach ($urlTitlesRows as $urlTitleRow)
			{
				$urlTitles[] = $urlTitleRow['urltitle'];
				$underscorePos = strpos($urlTitleRow['urltitle'], '_');
				if ($underscorePos !== false && $underscorePos < strlen($urlTitleRow['urltitle']) - 1)
				{
					// Extract url title version
					$urlTitleVer = intval(substr($urlTitleRow['urltitle'], $underscorePos + 1));
					if ($urlTitleVer > $maxUrlTitleVer)
						$maxUrlTitleVer = $urlTitleVer;
				}
			}
			if (in_array($urltitle, $urlTitles))
				$urltitle .= '_' . ($maxUrlTitleVer + 1);
			
			$queryActionAdd = " INSERT INTO ";

			//$queryImgAdd = " `blogPhoto` = '$blogImage', ";

			$sAutoApprovalVal = (getParam('blogAutoApproval')=='on') ? "approval" : "disapproval";
			$addQuery = "
				{$queryActionAdd} `BlogPosts`
				SET
					`CategoryID` = '{$iCategoryID}',
					`PostCaption` = '{$sPostCaption}',
					`PostText` = '{$sPostText}',
					/*$queryImgAdd*/
					`PostReadPermission` = '{$readPerm}',
					`PostCommentPermission` = '{$commentPerm}',
					`PostStatus` = '{$sAutoApprovalVal}',
					`Tags` = '{$sTagsPerm}',
					`PostDate` = NOW(),
					`urltitle` = '$urltitle'
			";

			$sRet = _t('_failed_to_add_post');
			if( db_res( $addQuery ) ) {
				$iLastId = mysql_insert_id();
				$this->iLastPostedPostID = $iLastId;
				if ( 0 < $_FILES['BlogPic']['size'] && 0 < strlen( $_FILES['BlogPic']['name'] ) && 0 < $iLastId ) {
					$sFileName = 'blog_' . $iLastId;
					$sExt = moveUploadedImage( $_FILES, 'BlogPic', $dir['blogImage'] . $sFileName, '', false );
					if( strlen( $sExt ) && !(int)$sExt ) {
						imageResize( $dir['blogImage'] . $sFileName.$sExt, $dir['blogImage'] . 'small_' . $sFileName.$sExt, $this->iIconSize / 2, $this->iIconSize / 2);
						imageResize( $dir['blogImage'] . $sFileName.$sExt, $dir['blogImage'] . 'big_' . $sFileName.$sExt, $this->iThumbSize, $this->iThumbSize);

						$query = "UPDATE `BlogPosts` SET `PostPhoto` = '" . $sFileName . $sExt . "' WHERE `PostID` = '$iLastId'";
						db_res( $query );
						@unlink( $dir['blogImage'] . $sFileName . $sExt );
					}
				}
				if ($iLastId>0) {
					$sRet = _t('_post_successfully_added');
					reparseObjTags( 'blog', $iLastId );
					
					TrackNewAction(7, $iLastId, $iCategoryOwnerID, $sPostCaption);
				}
			}
			return MsgBox($sRet);
		} elseif($this->aBlogConf['visitorID'] != $iCategoryOwnerID) {
			return MsgBox(_t('_Hacker String'));
		} else {
			return MsgBox(_t('_Error Occured'));
		}
	}

	/**
	 * Generate a Form to Editing/Adding of Category of Blog
	 *
	 * @param $categoryID - category ID
	 * @return HTML presentation of data
	 */
	function GenEditCategoryForm( $iCategoryID = '' ) {
		global $aBlogConfig;
		global $aBreadCramp;
		global $site;

		$this->CheckLogged();

		$sBlogsSQL = "
			SELECT `Blogs`. *
			FROM `Blogs` 
			WHERE `Blogs`.`OwnerID` = {$this->aBlogConf['visitorID']}
			LIMIT 1
		";
		$aBlogsRes = db_arr( $sBlogsSQL );
		if (mysql_affected_rows()==0) {
			checkCommunityPermissions($this->aBlogConf['visitorID']);
			return $this->GenCreateBlogForm();
		}

		$sRetHtml = '';
		if( !$this->aBlogConf['editAllowed'] && !$this->bAdminMode) {
			$ret .= _t_err( '_you_have_no_permiss_to_edit' );
			$sRetHtml = $ret;
		} else {
			if( $_REQUEST['action'] == 'edit_category' ) {
				$sCategorySQL = "
					SELECT * 
					FROM `BlogCategories`
					WHERE `CategoryID` = {$iCategoryID}
					LIMIT 1
				";
				$aCategory = db_arr( $sCategorySQL );
				$categCaption = $aCategory['CategoryName'];
				$categImg = ($aCategory['CategoryPhoto']) ? $aCategory['CategoryPhoto'] : 'folder11.png';
			} else {
				$categCaption = '';
				$categDesc = '';
				$categImg = 'folder11.png';
			}

			$sCategoryCaptionC = _t('_category_caption');
			$sPleaseFillFieldsC = _t('_please_fill_next_fields_first');

			$sRetHtml .= <<<EOF
<script type="text/javascript">
	function checkForm() {
		var el;
		var hasErr = false;
		var fild = "";

		el = document.getElementById("caption");
		if( el.value.length < 3 ) {
			el.style.backgroundColor = "pink";
			el.style.border = "1px solid silver";
			hasErr = true;
			fild += "{$sCategoryCaptionC}";
		} else {
			el.style.backgroundColor = "#fff";
		}

		if (hasErr) {
			alert( "{$sPleaseFillFieldsC}!" + fild )
			return false;
		} else {
			return true;
		}
		return false;
	}
</script>
EOF;

			$sCategoryCaptionC = _t('_category_caption');
			$sCharactersLeftC = _t('_characters_left');
			//$sCategoryDescriptionC = _t('_category_description');
			$sAssociatedImageC = _t('_associated_image');
			$sApplyChangesC = _t('apply changes');
			$sAddCategoryC = _t('_add_category');
			$sEditCategoryC = _t('_edit_category');

/*		$sBlogPhoto = '';
			if ( $categImg ) {
				$sBlogPhoto = <<<EOF
<div class="blogPhoto">
		<img src="{$site['blogImage']}big_{$categImg}" alt="" />
</div>
EOF;
			}*/

			$sEditCategory = '';
			if( 'edit_category' == $_REQUEST['action']  ) {
				$sEditCategory = <<<EOF
<input type="submit" value="{$sApplyChangesC}" />
<input type="hidden" name="action" value="editcategory" />
<input type="hidden" name="categoryID" value="{$iCategoryID}" />
<input type="hidden" name="categoryPhoto" value="{$categImg}" />
EOF;
			} else {
				$sEditCategory = <<<EOF
<input type="submit" value="{$sAddCategoryC}" />
<input type="hidden" name="action" value="addcategory" />
EOF;
			}

			//$iMemberID = (int)process_db_input( $_REQUEST['ownerID']);

			$sCategImg = '<img id="catImg" src="'.$site['icons'].'small_'.$categImg.'" style="position:static;" />';
			$sCategInput = '<input type="" name="categoryCaption" id="caption" value="'.$categCaption.'" class="categoryCaption1" onkeydown="return charCounter(\'caption\', 20, \'captCounter\');" />
				<span id="captCounter" style="font-weight:bold">20</span> '.$sCharactersLeftC;
			$sCategInputImg = $this->GenCenteredActionsBlock($sCategImg, $sCategInput);
			$ownerID = $this->aBlogConf['visitorID'];

			$sRetHtml .= <<<EOF
<div>
	<form name="editCategoryForm" action="{$_SERVER['PHP_SELF']}?action=edit_category&amp;ownerID=$ownerID" enctype="multipart/form-data" method="post" onsubmit="return checkForm();">
		<div class="margin_bottom_10">
			{$sCategoryCaptionC}
		</div>
		<div class="margin_bottom_10">
			{$sCategInputImg}
		</div>
		<div class="assocImageBlock">
			
			<div style="margin-bottom:5px;">
			{$sAssociatedImageC}
			</div>

			<div class="margin_bottom_10">
			
				<input type="radio" name="CategPic2" value="folder11.png" onclick="SetCategImg(this.value)">
					<img src="templates/tmpl_uni/images/icons/small_folder11.png">
				<input type="radio" name="CategPic2" value="folder1.png" onclick="SetCategImg(this.value)">
					<img src="templates/tmpl_uni/images/icons/small_folder1.png">
				<input type="radio" name="CategPic2" value="folder2.png" onclick="SetCategImg(this.value)">
					<img src="templates/tmpl_uni/images/icons/small_folder2.png">
				<input type="radio" name="CategPic2" value="folder3.png" onclick="SetCategImg(this.value)">
					<img src="templates/tmpl_uni/images/icons/small_folder3.png">
				<input type="radio" name="CategPic2" value="folder4.png" onclick="SetCategImg(this.value)">
					<img src="templates/tmpl_uni/images/icons/small_folder4.png">
				<input type="radio" name="CategPic2" value="folder5.png" onclick="SetCategImg(this.value)">
					<img src="templates/tmpl_uni/images/icons/small_folder5.png">
				<input type="radio" name="CategPic2" value="folder6.png" onclick="SetCategImg(this.value)">
					<img src="templates/tmpl_uni/images/icons/small_folder6.png">
				<input type="radio" name="CategPic2" value="folder7.png" onclick="SetCategImg(this.value)">
					<img src="templates/tmpl_uni/images/icons/small_folder7.png">
				<input type="radio" name="CategPic2" value="folder8.png" onclick="SetCategImg(this.value)">
					<img src="templates/tmpl_uni/images/icons/small_folder8.png">
				<input type="radio" name="CategPic2" value="folder9.png" onclick="SetCategImg(this.value)">
					<img src="templates/tmpl_uni/images/icons/small_folder9.png">
				<input type="radio" name="CategPic2" value="folder10.png" onclick="SetCategImg(this.value)">
					<img src="templates/tmpl_uni/images/icons/small_folder10.png">
				
				<input type="hidden" name="CategPic" />
			</div>
			<div class="clear_both"></div>
		</div>
		{$sEditCategory}
		<input type="hidden" name="ownerID" value="{$ownerID}" />
	</form>
</div>
<script>
	function SetCategImg(picture)
	{
		document.getElementById('catImg').src='{$site['icons']}small_' + picture;
	}
	function CheckCategImg(picture)
	{
		var inputs = document.forms.editCategoryForm.CategPic2;
		for (var i = 0; i < inputs.length; i++)
		{
			if (inputs[i].value == picture)
			{
				inputs[i].checked = true;
				break;
			}
		}
	}
	CheckCategImg('$categImg');
</script>
EOF;
		}
		$caption = ($iCategoryID == '') ? $sAddCategoryC : $sEditCategoryC;
		return DesignBoxContent ($caption, $sRetHtml, 1);
		//return $sRetHtml;
	}

	/**
	 * Update (Adding or Editing) a Category
	 *
	 * @param $bEditMode - Update (Editing) mode
	 * @return MsgBox result
	 */
	function ActionUpdateCategory($bEditMode=FALSE) {
		global $aBlogConfig;
		global $dir;

		$this->CheckLogged();

		$ownerID = (int)process_db_input( $_REQUEST['ownerID']);
		$iCategoryID = process_db_input( (int)$_POST['categoryID'] );

		$sCheckPostSQL = "SELECT `BlogCategories`.`OwnerID`
							FROM `BlogCategories`
							WHERE `BlogCategories`.`CategoryID`={$iCategoryID}
						";
		$aCategoryOwner = db_arr($sCheckPostSQL);
		$iCategoryOwnerID = $aCategoryOwner['OwnerID'];

		if ((($this->aBlogConf['visitorID'] == $iCategoryOwnerID || $this->bAdminMode==TRUE) && $iCategoryID > 0 && $bEditMode==TRUE) || ($bEditMode==FALSE && $iCategoryID==0 && $ownerID==$this->aBlogConf['visitorID'])) {
			$ret = '';

			$categoryCaption = process_db_input( $_POST['categoryCaption']);
			$categoryPhoto = process_db_input( $_POST['categoryPhoto']);

			if ($bEditMode==TRUE) {
				$addQuery = "
					UPDATE `BlogCategories` SET
					`CategoryName` = '{$categoryCaption}',
					`CategoryPhoto` = '{$categoryPhoto}',
					`Date` = NOW( ) WHERE `CategoryID` = '{$iCategoryID}'
				";
				$update = true;
			} 
			else 
			{			
				$catUrlTitle = GetSEFUrl($categoryCaption, sefBlogCategoryWords);
				
				// Check whether category with same params exists
				if (!db_value("SELECT `CategoryID` FROM `BlogCategories` 
					WHERE `OwnerID` = '{$ownerID}' AND `CategoryName` = '{$categoryCaption}' 
						AND `CategoryType` = '1' LIMIT 1"))
					$addQuery = "
						INSERT INTO `BlogCategories` SET
						`OwnerID` = '{$ownerID}',
						`CategoryName` = '{$categoryCaption}',
						`CategoryPhoto` = '{$categoryPhoto}',
						`Date` = NOW(),
						`caturltitle` = '$catUrlTitle'
					";
				else 
					$addQuery = '';
			}

			if ($addQuery == '')
				$ret .= 'Category with this title exists already';
			elseif (db_res($addQuery))
				{
				if (isset($_POST['CategPic2']))
					{
					if (isset($update))
						{
							
						$query = "UPDATE `BlogCategories` SET `categoryPhoto` = '" . $_POST['CategPic2'] . "' WHERE `categoryID` = '{$iCategoryID}'";
						db_res( $query );		
						}
					else 
						{
						$iLastId = mysql_insert_id();
						$query = "UPDATE `BlogCategories` SET `categoryPhoto` = '" . $_POST['CategPic2'] . "' WHERE `categoryID` = '$iLastId'";
						//print "<LI>$query";
						db_res( $query );		
						}
					//print $query;
					}
				else 
					{
					if ( 0 < $_FILES['CategPic']['size'] && 0 < strlen( $_FILES['CategPic']['name'] ) && 0 < ( $iLastId = mysql_insert_id() ) ) {
						$sFileName = 'category_' . $iLastId;
						$sExt = moveUploadedImage( $_FILES, 'CategPic', $dir['blogImage'] . $sFileName, '', false );
						if ( strlen( $sExt ) && !(int)$sExt ) {
							imageResize( $dir['blogImage'] . $sFileName . $sExt, $dir['blogImage'] . 'small_' . $sFileName . $sExt, 25, 25, false );
							imageResize( $dir['blogImage'] . $sFileName . $sExt, $dir['blogImage'] . 'big_' . $sFileName . $sExt, 150, 150, false );
	
							$query = "UPDATE `BlogCategories` SET `categoryPhoto` = '" . $sFileName . $sExt . "' WHERE `categoryID` = '$iLastId'";
							db_res( $query );
	
							@unlink( $dir['blogImage'] . $sFileName . $sExt );
						}
					}
				}

				$ret .= _t( '_category_successfully_added' );
			} else {
				$ret .= _t( '_failed_to_add_category' );
			}
			return MsgBox($ret);
		} elseif($this->aBlogConf['visitorID'] != $iCategoryOwnerID) {
			return MsgBox(_t('_Hacker String'));
		} else {
			return MsgBox(_t('_Error Occured'));
		}
	}

	/**
	 * Deleting a Category
	 *
	 * @return MsgBox result
	 */
	function ActionDeleteCategory() {
		$this->CheckLogged();

		$iCategID = process_db_input( (int)$_POST['DeleteCategoryID'] );

		$aCatType = db_arr("SELECT `CategoryType`,`OwnerID` FROM `BlogCategories` WHERE `CategoryID` = {$iCategID} LIMIT 1");
		if ($aCatType['CategoryType'] == 1 && $aCatType['OwnerID']==$this->aBlogConf['visitorID']) {
			$vPosts = db_res( "SELECT `PostID` FROM `BlogPosts` WHERE `CategoryID` = {$iCategID}" );
			/*
			while( $aBlog = mysql_fetch_assoc( $vPosts ) ) {
				$iPostID = $aBlog['PostID'];
				db_res( "DELETE FROM `BlogPostComments` WHERE `PostID` = {$iPostID}" );
			}
			db_res( "DELETE FROM `BlogPosts` WHERE `CategoryID` = {$iCategID}" );
			*/
			
			// Get default member category
			$defCatId = db_value("SELECT `CategoryID` FROM `BlogCategories` 
				WHERE `OwnerID`={$aCatType['OwnerID']} AND `CategoryType`=5 LIMIT 1");
			
			// Set to all posts in this category value of a category by default
			db_res( "UPDATE `BlogPosts` SET `CategoryID`=$defCatId 
				WHERE `CategoryID` = {$iCategID}" );
			
			// Delete category
			db_res( "DELETE FROM `BlogCategories` WHERE `BlogCategories`.`CategoryID` = {$iCategID} LIMIT 1" );
			return MsgBox(_t('_category_deleted'));
		} elseif ($aCatType['OwnerID']!=$this->aBlogConf['visitorID']) {
			return MsgBox(_t('_Hacker String'));
		} else {
			return MsgBox(_t('_category_delete_failed'));
		}
	}

	/**
	 * Generate a Block of searching result by Tag (GET is tagKey) or by keyword
	 * 
	 * @return HTML presentation of data
	 */
	function GenSearchResult($tagKey = '', $ownerID = 0)
	{
		global $site, $logged;

		$sRetHtml = '';
		$sWhereAddon = '';
		if ($tagKey)
			$sSearchedTag = process_db_input($tagKey);
		elseif (isset($_REQUEST['keyword']))
		{
			$sSearchedTag = process_db_input($_REQUEST['keyword']);
			$sWhereAddon .= "AND (PostCaption LIKE '%$sSearchedTag%' 
				OR PostText LIKE '%$sSearchedTag%' OR Tags LIKE '%$sSearchedTag%')";
		}
		else
			$sSearchedTag = process_db_input($_REQUEST['tagKey']);
		
		$iMemberID = ($ownerID) ? $ownerID : (int)$_REQUEST['ownerID'];
		$sDateFormatPhp = getParam('php_date_format');
		$sTagsC = _t('_Tags');
		$sEditC = _t('_Edit');
		$sDeleteC = _t('_Delete');

		$postsPerPage = 10;
		$offset = (int)$_GET['offset'];
		
		$sBlogPosts = '';
		if ($iMemberID > 0)
			$sWhereAddon .= "AND `BlogCategories`.`OwnerID` = {$iMemberID}";
		$sBlogPostsCountSQL = "
			SELECT COUNT(*)
			FROM `BlogPosts` 
			LEFT JOIN `BlogCategories` USING(CategoryID) 
			WHERE OwnerID > 0 {$sWhereAddon}
		";				
		$totalPosts = db_value($sBlogPostsCountSQL);
			
		$sBlogPostsSQL = "
			SELECT `BlogPosts`.* , `BlogCategories`.*
			FROM `BlogPosts` 
			LEFT JOIN `BlogCategories` USING(CategoryID) 
			WHERE OwnerID > 0 {$sWhereAddon}
			LIMIT $offset, $postsPerPage
		";	
		$vBlogPosts = db_res( $sBlogPostsSQL );
		while ( $aResSQL = mysql_fetch_assoc($vBlogPosts) ) 
		{
			$bFriend = is_friends( $this->aBlogConf['visitorID'], $aResSQL['OwnerID'] );
			$bOwner = ($this->aBlogConf['visitorID']==$aResSQL['OwnerID']) ? true : false;
			if ('friends' == $aResSQL['PostReadPermission'] && 
				!$bFriend && !$bOwner && !$this->bAdminMode) continue;
				
			$sTagsCommas = $aResSQL['Tags'];
			$aTags = split(',', $sTagsCommas);
			
			if (in_array($sSearchedTag, $aTags) || isset($_REQUEST['keyword']))
				$sBlogPosts .= $this->GenPostString($aResSQL);
		}
		
		// Show pagination
		if ($totalPosts > $postsPerPage)
		{
			require_once(BX_DIRECTORY_PATH_INC . 'pagination.inc.php');
			
			$paginationConfig = array(
				'base_url' => $_SERVER['REQUEST_URI'],
				'query_string_segment' => 'offset',
				'total_rows' => $totalPosts,
				'per_page' => $postsPerPage,
				'num_links' => 3
			);
			$pagination = new Pagination($paginationConfig);
			$paginationStr = $pagination->create_links();
			$paginationTop = "<div class=\"navigation navigationTop\">Page: $paginationStr</div>";
			$paginationBottom = "<div class=\"navigation navigationBottom\">Page: $paginationStr</div>";
			
			$sBlogPosts = $paginationTop . $sBlogPosts . $paginationBottom;
		}
		
		if (isset($_REQUEST['keyword']))
		{
			$output = ($totalPosts) ? $sBlogPosts : MsgBox(_t('_Sorry, nothing found'));
			return $this->_sectionWithSearchBox("Search Results for \"$sSearchedTag\"", $output);
		}
		
		if (empty($sBlogPosts))
			return MsgBox(_t('_Sorry, nothing found'));
			
		$sContentSect = DesignBoxContent ($sTagsC.' - '.$sSearchedTag, $sBlogPosts, 1);
		$sRightSect='';
		if ($iMemberID>0 && $a = $this->GetProfileData($iMemberID)) {
			$sBlogsSQL = "
				SELECT `Blogs`. * , `Profiles`.`Nickname` as OwnerNick
				FROM `Blogs` 
				INNER JOIN `Profiles` ON `Blogs`.`OwnerID` = `Profiles`.`ID`
				WHERE `Blogs`.`OwnerID` = {$iMemberID}
				LIMIT 1
			";

			$aBlogsRes = db_arr( $sBlogsSQL );
			$sRightSect = $this->GenMemberDescrAndCat($aBlogsRes);

			$sWidthClass = ($iMemberID>0) ? 'cls_info_left' : 'cls_res_thumb' ;
			$sRetHtml = <<<EOF
<div>
	<div class="clear_both"></div>
	<div class="{$sWidthClass}">
		{$sContentSect}
	</div>
	<div class="cls_info">
		{$sRightSect}
	</div>
	<div class="clear_both"></div>
</div>
<div class="clear_both"></div>
EOF;
		} 
		else
			$sRetHtml = MsgBox(_t('_Profile Not found Ex'));

		return $sRetHtml;
	}

	/**
	 * Generate a Form to Create Blog
	 *
	 * @return HTML presentation of data
	 */
	function GenCreateBlogForm()
	{
		global $iMemberID;		

		$this->CheckLogged();
		
		$visitorID = $this->aBlogConf['visitorID'];
		if (!$iMemberID)
			$iMemberID = $visitorID;
			
		if (!$iMemberID) return '';

		$sRetHtml = '';
		//$sActionsC = _t('_Actions');
		$sActionsC = 'Create My Blog';
		$sPleaseCreateBlogC = _t('_Please, Create a Blog');
		$sNoBlogC = _t('_No blogs available');
		$sCreateMyBlogC = _t('_Create My Blog');
		$sCreateBlogC = _t('_Create Blog');
		$sMyBlogC = _t('_My Blog');
		$sNewBlogDescC = _t('_Write a description for your Blog.');

		//$sRetHtml .= MsgBox($sNoBlogC);
		
		$styleCreateBlogForm = (isset($_REQUEST['create_blog'])) ? 'style="display:block"' 
			: 'style="display:none; margin-top:30px"';
		
		// Get blog owner nickname
		$blogOwnerNick = db_value("SELECT NickName FROM Profiles 
			LEFT JOIN Blogs ON Profiles.ID = Blogs.OwnerID
			WHERE Profiles.ID = $iMemberID
			LIMIT 1");

		if ($this->aBlogConf['isOwner']) {
			if (!isset($_REQUEST['create_blog']))		
				$sRetHtml .= "Sorry, $blogOwnerNick it looks like you have not setup a blog yet.<br/><br/>
					Would you like to <a class=\"actions\" onclick=\"javascript: document.getElementById('CreateBlogFormDiv').style.display = 'block';return false;\" 
					href=\"{$_SERVER['PHP_SELF']}\">setup and create your blog now?</a>";

			$sCreateBlogContent = <<<EOF
	<form action="{$_SERVER['PHP_SELF']}" method="post" name="CreateBlogForm">
		<table width="100%">
			<tr><td style="vertical-align:top; text-align:right; width:100px">Blog Title:</td>
				<td><input type="text" id="title" name="Title" maxlength="60" style="width:170px" onkeyup="return charCounter('title', 60, 'titleRemain');"/>
				<span style="font-size:11px;padding-left:5px"><span id="titleRemain" style="font-weight:bold">60</span> Characters Remaining</span>
				<div style="font-size:11px; font-style:italic; margin-top:3px; margin-bottom:10px">Give your blog a name or title.</div></td></tr>
			<tr><td style="vertical-align:top; text-align:right; width:80px">Description:</td>
				<td><textarea name="Description" id="blogDesc" rows="3" class="classfiedsTextArea1" 
					style="width:100%;" onkeyup="return charCounter('blogDesc', 500, 'descRemain');"></textarea>
				<div style="float:left; font-size:11px; font-style:italic; margin-top:3px; margin-bottom:10px; width:400px">
					Write a short description to give others an idea of what your blog is about or just write something about yourself. 
					This will be displayed in the upper right hand corner of your blog.</div>
				<span style="font-size:11px; float:right; margin-top:3px"><span id="descRemain" style="font-weight:bold">500</span> Characters Remaining</span></td>
			</tr>
			<tr><td style="vertical-align:top; text-align:right">URL:<br/>
					<span style="font-size:10px">(RSS 2.0, Atom)</span></td>
				<td><input type="text" id="syndicateURL" name="syndicateURL" maxlength="255" style="width:400px" onkeyup="return charCounter('syndicateURL', 255, 'syndicateURLRemain');"/>
				<span style="font-size:11px;padding-left:5px"><span id="syndicateURLRemain" style="font-weight:bold">255</span> Characters Remaining</span>
				<div style="font-size:11px; font-style:italic; margin-top:3px; margin-bottom:10px">If specified, new messages will be fetched from feed and added to your blog automatically. 
					<br/>Keep empty if you prefer to post to blog manually.</div></td></tr>
		</table>
		<input type="hidden" name="action" id="action" value="create_blog" />
		<div style="text-align:right"><input type="submit" value="{$sCreateBlogC}" onclick="return CheckRSSURL();"/></div>
	</form>
	
	<script>		
		var xmlhttp;
		
		function CheckRSSURL()
		{
			var syndicateURL = document.CreateBlogForm.syndicateURL.value;
			if (syndicateURL.trim() == '') return true;
			
			return loadXMLDoc(syndicateURL, 'simplepie/rss_validate.php');
		}		
	</script>
EOF;

			$sRetHtml .= "<div id=\"CreateBlogFormDiv\" $styleCreateBlogForm>".
				DesignBoxContent ( $sActionsC, $sCreateBlogContent, 1). '</div>';
			if (isset($_REQUEST['create_blog']))
				return $sRetHtml;
		}
		else 
		{
			$sMyBlogC = "$blogOwnerNick's Blog";
			$sRetHtml .= "Sorry, it looks like $blogOwnerNick has not setup a blog yet.";
		}

		return DesignBoxContent($sMyBlogC, $sRetHtml, 1);
	}

	function GenCenteredActionsBlock($sPicElement, $sHrefElement) {

		$sResElement = <<<EOF

<span style="vertical-align: middle;margin-right:5px;">{$sPicElement}</span>

<span>{$sHrefElement}</span>

EOF;

		return $sResElement;

	}

	/**
	 * Creating a Blog
	 *
	 * @return MsgBox result
	 */
	function ActionCreateBlog() {
		$this->CheckLogged();

		$sErrorC = _t('_Error Occured');
		$iOwnID = $this->aBlogConf['visitorID'];

		// Check whether blog was created already
		if (db_value("SELECT COUNT(`ID`) FROM `Blogs` WHERE `OwnerID`=$iOwnID LIMIT 1"))
			return $this->GenMemberBlog($iOwnID);
		
		$sDescription = addslashes(clear_xss(process_pass_data($_POST['Description'] )));
		$sTitle = addslashes(clear_xss(process_pass_data($_POST['Title'] )));
		$syndicateURL = trim(process_db_input($_POST['syndicateURL']));
		$titleForSEF = ($sTitle != '') ? $sTitle : ucfirst(getNickName($iOwnID) . ' Blog');
		$urltitle = process_db_input(GetSEFUrl($titleForSEF, sefBlogWords));
		$sRequest = "INSERT INTO `Blogs` SET `OwnerID` = '{$iOwnID}', `Title` = '{$sTitle}', `Description` = '{$sDescription}', 
			`Other` = 'nothing', `SyndicateURL` = '$syndicateURL', `urltitle` = '$urltitle'";
		db_res($sRequest);
		$blogID = mysql_insert_id();

		if (mysql_affected_rows()==1) {
			$sAddQuery = "INSERT INTO `BlogCategories` SET
				`OwnerID` = {$iOwnID}, `CategoryName` = 'Uncategorized', `CategoryType` = '5',
				`CategoryPhoto` = '', `Date` = NOW()";
			db_res($sAddQuery);
			
			// Syndicate created blog
			if ($syndicateURL != '')
				$this->SyndicateBlogFeed($syndicateURL, $blogID, $iOwnID);
			
			return $this->GenMemberBlog($iOwnID);
		} else {
			return MsgBox($sErrorC);
		}
	}

	/**
	 * Adding a Comment to Post
	 *
	 * @return MsgBox result
	 */
	function ActionAddBlogComment() {
		global $site, $logged;				
		
		//$this->CheckLogged();

		if (!$logged['member'] && !$logged['admin'])
		{
			// Captcha verification
			require_once(BX_DIRECTORY_PATH_ROOT . 'recaptcha/config.php');
			require_once(BX_DIRECTORY_PATH_ROOT . 'recaptcha/recaptchalib.php');

			 $recaptchaResp = recaptcha_check_answer (RECAPTCHA_PRIVATE_KEY,
                                        $_SERVER["REMOTE_ADDR"],
                                        $_POST["recaptcha_challenge_field"],
                                        $_POST["recaptcha_response_field"]);

			$verified = $recaptchaResp->is_valid;
			
			if (!$verified)
			{
				// Save post data to database by sessionid
				if (!session_id())
					session_start();
				$name = process_db_input($_POST['name']);
				$email = process_db_input($_POST['email']);
				$website = process_db_input($_POST['website']);
				$commentText = process_db_input($_POST['commentText']);
				$postData = serialize(array('name' => $name, 'email' => $email,
					'website' => $website, 'commentText' => $commentText));
				
				db_res("INSERT INTO session SET sessionid = '".session_id()."', 
					name = 'blogCommentFields', data = '$postData'
					ON DUPLICATE KEY UPDATE data = '$postData'");

				header("Location: {$_SERVER['REQUEST_URI']}#answer_form_to_0");
				exit;
			}
		}
		
		$blogID = (int)$_POST['CommPostID'];		
		$senderID = $this->aBlogConf['visitorID'];
		$commentText = process_db_input(clear_xss(trim(process_pass_data($_POST['commentText']))));
		$replyTo = (int)$_POST['replyTo'];
		$ip = getVisitorIP(); // ( getenv('HTTP_CLIENT_IP') ? getenv('HTTP_CLIENT_IP') : getenv('REMOTE_ADDR') );

		if ( !$ip ) {
			$ret = _t_err("_sorry, i can not define you ip adress. IT'S TIME TO COME OUT !");
			return $ret;
		}
		/*
		if( 0 >= $senderID )
			return _t_err('_im_textLogin');
		*/
		if( 0 >= $blogID )
			return '';

		$last_count = db_arr( "SELECT COUNT( * ) AS `last_count` FROM `BlogPostComments` WHERE `IP` = '{$ip}' AND (UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(`Date`) < 1*60)" );
		if ( $last_count['last_count'] != 0 ) {
			$ret = _t("_You have to wait for PERIOD minutes before you can write another message!", 1);
			return MsgBox($ret);
		}

		if ($blogID)
		{
			// Get sender link
			if ($senderID == 0)
			{
				$name = ucwords(process_db_input($_POST['name']));
				$nick = ($name == '') ? '<strong>'. _t("_Visitor") .'</strong>'	: $name;
				$email = process_db_input($_POST['email']);
				$website = process_db_input($_POST['website']);
				if ($website != '' && strpos($website, 'http') === false)
					$website = "http://$website";
				$commentator = ($website == '') ? $name	: "<a href='$website'>$name</a>";
			}
			else 
			{
				$nick = ucwords(getNickName($senderID));
				if ($nick == '')
					$nick = '<strong>'. _t("_Visitor") .'</strong>';
				$profileLink = getProfileLink($senderID);	
				$commentator = "$nick <a href=\"$profileLink\">($profileLink)</a>";		
			}
		
			// Get comment recipient id
			$recipientID = db_value("SELECT `OwnerID` FROM `BlogCategories` 
				LEFT JOIN `BlogPosts` ON `BlogPosts`.`CategoryID` = `BlogCategories`.`CategoryID` 
				WHERE `BlogPosts`.`PostID`=$blogID");
			if ($recipientID && $recipientID != $senderID)
			{
				// Check whether email notification on new blog post comment was specified by recipient
				$recipientData = db_arr("SELECT `NewBlogPostCommentNotification`, `Email`, `NickName` FROM `Profiles` WHERE `ID`=$recipientID");
				if ($recipientData && ($recipientData['NewBlogPostCommentNotification'] == 'Yes'))
				{
					// Get blog post link
					if (useSEF && $urltitle = db_value("SELECT urltitle FROM BlogPosts WHERE PostID = $blogID"))
						$blogPostLink = "{$site['url']}{$recipientData['NickName']}/$urltitle.html";
					else
						$blogPostLink = "{$site['url']}blogs.php?action=show_member_post&ownerID={$recipientID}&post_id=$blogID";
					
					// Send mail to owner of this post about new comment
					$message = getParam("t_NewBlogPostCommentMail");
					$subject = getParam('t_NewBlogPostCommentMail_subject');
					$subject = str_ireplace('{Friend}', "$nick", $subject);

					$aPlus = array();
					$aPlus['ProfileReference'] = $commentator;
					$aPlus['BlogPostLink'] = $blogPostLink;
	
				    sendMail($recipientData['Email'], $subject, $message, $recipientID, $aPlus);
				}
			}
			
			$notify = 0; // save notification to BlogPostCommentsNotifications table only
			$addCommentQuery = "
				INSERT INTO `BlogPostComments`
				SET
					`PostID` = '$blogID',
					`SenderID` = '$senderID',
					`CommentText` = '$commentText',
					`ReplyTo` = '$replyTo',
					`IP` = '$ip',
					`Date` = NOW(),
					`Notify` = $notify
			";
			if ($senderID == 0)
				$addCommentQuery .= ", `Name`='$name', `Email`='$email', `Website`='$website'";
	
			if( db_res( $addCommentQuery ) ) 
			{
				$ret = _t('_comment_added_successfully');
				$this->TrackComment($blogID, $senderID, $commentText, mysql_insert_id());
				
				// Send mail to subscribed members
				$recipients = fill_assoc_array(db_res(
					"SELECT p.Email, p.FirstName as Name
					FROM Profiles as p
					LEFT JOIN BlogPostCommentsNotifications as bpcn 
						ON bpcn.UserID = p.ID 
					WHERE bpcn.PostID = $blogID AND bpcn.UserID != $senderID"));
				$subject = getParam("t_FollowBlogPostComment_subject");
				$message = getParam("t_FollowBlogPostComment");
				
				$urltitle = db_value("SELECT `urltitle` FROM `BlogPosts` WHERE `PostID` = $blogID LIMIT 1");
				if (useSEF && $urltitle != '')
				{
					$ownerNick = getNickName($recipientID);
					$aPlusURL = "{$site['url']}$ownerNick/$urltitle.html";
				}
				
				if (!isset($aPlus))	$aPlus = array();
				$aPlus['Commentator'] = $commentator;
				$aPlus['URL'] = ($aPlusURL) ? $aPlusURL 
					: "{$site['url']}blogs.php?action=show_member_post&ownerID=$recipientID&post_id=$blogID";
			
				foreach ($recipients as $recipient)	
				{
					$aPlus['Name'] = $recipient['Name'];
					sendMail($recipient['Email'], $subject, $message, '', $aPlus);
				}
				
				if (isset($_POST['notify']) && $_POST['notify'] == 'on' && $senderID > 0)
					db_res("INSERT INTO BlogPostCommentsNotifications
						(PostID, UserID) VALUES ($blogID, $senderID)
						ON DUPLICATE KEY UPDATE PostID = $blogID, UserID = $senderID");
			}
			else
				$ret = _t('_failed_to_add_comment');
		}
			
		return MsgBox($ret);
	}
	
	function TrackComment($postID, $senderID, $commentText, $commentId)
	{
		// Get post title
		$postTitleQuery = "SELECT PostCaption FROM BlogPosts WHERE PostID=$postID LIMIT 1";
		$postTitle = db_value($postTitleQuery);
		$postTitle = ($postTitle == '') ? 'Untitled' : process_db_input(trim($postTitle));
	
		// Get blog post owner
		$sOwnerQuery = "SELECT OwnerID as ownerid, `Profiles`.NickName as ownernick
			FROM `BlogCategories` 
			LEFT JOIN `BlogPosts` ON `BlogPosts`.CategoryID = `BlogCategories`.CategoryID
			LEFT JOIN `Profiles` ON `Profiles`.ID = `BlogCategories`.OwnerID
			WHERE `BlogPosts`.PostID = $postID LIMIT 1";
		$owner = db_assoc_arr($sOwnerQuery);
		if (!$owner) return;
		
		TrackNewAction(8, $postID, $senderID, $postTitle, $commentText, '', 
			$owner['ownerid'], $owner['ownernick'], false, $commentId);
	}

	/**
	 * Editing a Comment to Post
	 *
	 * @return MsgBox result
	 */
	function ActionEditComment(){
		$iCommId = (int)$_REQUEST['EditCommentID'];
		$sCheckSQL = "SELECT `SenderID`,`PostID` FROM `BlogPostComments` WHERE `CommentID`={$iCommId}";
		$aSenderID = db_arr($sCheckSQL);
		$iSenderID = $aSenderID['SenderID'];

		$iPostID = $aSenderID['PostID'];
		$sCheckPostSQL = "SELECT `BlogCategories`.`OwnerID`
							FROM `BlogPosts`
							LEFT JOIN `BlogCategories` ON `BlogCategories`.`CategoryID`=`BlogPosts`.`CategoryID`
							WHERE `PostID`={$iPostID}
						";
		$PostID = db_arr($sCheckPostSQL);
		$iPostOwnerID = $PostID['OwnerID'];
		if (( $this->aBlogConf['visitorID'] == $iPostOwnerID || $this->aBlogConf['visitorID'] == $iSenderID) && $iCommId > 0) {
			$sMessage = addslashes( clear_xss( process_pass_data( $_POST['commentText'] ) ) );

			$query = "UPDATE `BlogPostComments` SET `CommentText` = '{$sMessage}' WHERE `BlogPostComments`.`CommentID` = {$iCommId} LIMIT 1 ;";

			$sqlRes = db_res( $query );
		} elseif($this->aBlogConf['visitorID'] != $iSenderID || $this->aBlogConf['visitorID'] != $iPostOwnerID) {
			return MsgBox(_t('_Hacker String'));
		} else {
			return MsgBox(_t('_Error Occured'));
		}
	}

	/**
	 * Deleting a Comment to Post
	 *
	 * @return MsgBox result
	 */
	function ActionDeleteComment(){
		$iCommId = (int)$_REQUEST['DeleteCommentID'];
		$sCheckSQL = "SELECT `SenderID`,`PostID` FROM `BlogPostComments` WHERE `CommentID`={$iCommId}";
		$aSenderID = db_arr($sCheckSQL);
		$iSenderID = $aSenderID['SenderID'];

		$iPostID = $aSenderID['PostID'];
		$sCheckPostSQL = "SELECT `BlogCategories`.`OwnerID`
							FROM `BlogPosts`
							LEFT JOIN `BlogCategories` ON `BlogCategories`.`CategoryID`=`BlogPosts`.`CategoryID`
							WHERE `PostID`={$iPostID}
						";
		$PostID = db_arr($sCheckPostSQL);
		$iPostOwnerID = $PostID['OwnerID'];
		if (( $this->aBlogConf['visitorID'] == $iPostOwnerID || 
			$this->aBlogConf['visitorID'] == $iSenderID) && $iCommId > 0)
		{
			$query = "DELETE FROM `BlogPostComments` WHERE `BlogPostComments`.`CommentID` = {$iCommId} LIMIT 1";
			$sqlRes = db_res( $query );	
			
			// Delete from NewsFeed table
			db_res("DELETE FROM `NewsFeedTrack` WHERE `type`=8 AND `commentid`=$iCommId LIMIT 1");
		} 
		elseif($this->aBlogConf['visitorID'] != $iSenderID || $this->aBlogConf['visitorID'] != $iPostOwnerID)
			return MsgBox(_t('_Hacker String'));
		else
			return MsgBox(_t('_Error Occured'));
	}

	/**
	 * SQL Get all Profiles data by Profile Id
	 *
	  * @param $iProfileId
	 * @return SQL data
	 */
	function GetProfileData($iProfileId) {
		return getProfileInfo( $iProfileId );
	}

	/**
	 * Editing a Description of Blog
	 *
	 * @return MsgBox result
	 */
	function ActionEditBlog() {
		$this->CheckLogged();

		$iBlogID = (int)($_POST['EditBlogID']);

		$sCheckPostSQL = "SELECT `OwnerID`
							FROM `Blogs`
							WHERE `ID`={$iBlogID}
						";
		$aBlogOwner = db_arr($sCheckPostSQL);
		$iBlogOwner = $aBlogOwner['OwnerID'];

		if (($this->aBlogConf['visitorID'] == $iBlogOwner || $this->bAdminMode) && $iBlogID > 0) {
			$sDescription = process_db_input($_REQUEST['Description']);
			$sTitle = process_db_input($_REQUEST['Title']);
			$syndicateUrl = process_db_input($_REQUEST['syndicateURL']);
			$sQuery = "UPDATE `Blogs` SET
				`Title` = '{$sTitle}', `Description` = '{$sDescription}', `Locked` = {$_REQUEST['lockoption']},
				`SyndicateUrl` = '{$syndicateUrl}'
				WHERE `Blogs`.`ID` = {$iBlogID} LIMIT 1";
			db_res($sQuery);
			
			if ($syndicateUrl != '')
				$this->SyndicateBlogFeed($syndicateUrl, $iBlogID, $iBlogOwner);
				
		} elseif($this->aBlogConf['visitorID'] != $iBlogOwner) {
			return MsgBox(_t('_Hacker String'));
		} else {
			return MsgBox(_t('_Error Occured'));
		}
	}

	/**
	 * Deleting a Full Blog
	 *
	 * @return MsgBox result
	 */
	function ActionDeleteBlogSQL() {
		$this->CheckLogged();
		global $dir;

		$iBlogID = (int)$_REQUEST['DeleteBlogID'];

		$sCheckPostSQL = "SELECT `OwnerID`
							FROM `Blogs`
							WHERE `ID`={$iBlogID}
						";
		$aBlogOwner = db_arr($sCheckPostSQL);
		$iBlogOwner = $aBlogOwner['OwnerID'];
		if (($this->aBlogConf['visitorID'] == $iBlogOwner || $this->bAdminMode) && $iBlogID > 0) {
			//Clean blogs
			$blogPostsArray = array();
			$vBlogCategs = db_res( "SELECT `CategoryID`,`CategoryPhoto` FROM `BlogCategories` LEFT JOIN `Blogs` ON `Blogs`.`OwnerID` = `BlogCategories`.`OwnerID` WHERE `Blogs`.`ID` = {$iBlogID} " );
			while( $aBlogCateg = mysql_fetch_assoc( $vBlogCategs ) ) {
				$iCategID = $aBlogCateg['CategoryID'];
				$vPosts = db_res( "SELECT `PostID`,`PostPhoto` FROM `BlogPosts` WHERE `CategoryID` = {$iCategID}" );
				while( $aBlog = mysql_fetch_assoc( $vPosts ) ) {
					$iPostID = $aBlog['PostID'];
					db_res( "DELETE FROM `BlogPostComments` WHERE `PostID` = {$iPostID}" );
					$sFilePathPost = 'big_'.$aBlog['PostPhoto'];
					if ($sFilePathPost!='' && file_exists($dir['blogImage'].$sFilePathPost) && is_file($dir['blogImage'].$sFilePathPost))
						unlink( $dir['blogImage'] . $sFilePathPost );
					$sFilePathPost = 'small_'.$aBlog['PostPhoto'];
					if ($sFilePathPost!='' && file_exists($dir['blogImage'].$sFilePathPost) && is_file($dir['blogImage'].$sFilePathPost))
						unlink( $dir['blogImage'] . $sFilePathPost );
				}
				$blogPostsArray[] = fill_assoc_array(db_res("SELECT PostID FROM BlogPosts WHERE `CategoryID` = {$iCategID}"));
				db_res( "DELETE FROM `BlogPosts` WHERE `CategoryID` = {$iCategID}" );
				db_res( "DELETE FROM `BlogCategories` WHERE `CategoryID` = {$iCategID}" );

				$sFilePath = 'big_'.$aBlogCateg['CategoryPhoto'];
				if ($sFilePath!='' && file_exists($dir['blogImage'].$sFilePath) && is_file($dir['blogImage'].$sFilePath))
					unlink( $dir['blogImage'] . $sFilePath );
				$sFilePath = 'small_'.$aBlogCateg['CategoryPhoto'];
				if ($sFilePath!='' && file_exists($dir['blogImage'].$sFilePath) && is_file($dir['blogImage'].$sFilePath))
					unlink( $dir['blogImage'] . $sFilePath );
			}
			$blogPostsStr = '';
			foreach ($blogPostsArray as $blogPostCat)
				foreach ($blogPostCat as $blogPost)
				{
					if (!strlen($blogPostsStr))
						$blogPostsStr .= '(';
					else
						$blogPostsStr .= ', ';
					$blogPostsStr .= $blogPost['PostID'];
				}
			
			if (strlen($blogPostsStr))
			{
				$blogPostsStr .= ')';
				
				// Clear all posts from this blog and comments assigned to them from newsfeedtrack table
				db_res("DELETE FROM NewsFeedTrack WHERE mediaid IN $blogPostsStr AND (type=7 OR type=8)");
			}
			db_res( "DELETE FROM `Blogs` WHERE `ID` = {$iBlogID}" );
		} elseif($this->aBlogConf['visitorID'] != $iBlogOwner) {
			return MsgBox(_t('_Hacker String'));
		} else {
			return MsgBox(_t('_Error Occured'));
		}
	}

	/* common features function
	*/
	function process_html_db_input( $sText ) {

		return addslashes( clear_xss( trim( process_pass_data( $sText ))));
	}
		
	function SyndicateBlogFeed($syndicateURL, $blogID, $ownerID)
	{	
		global $dir;
		
		$PersonalBlogFetcherInterval = 15 * 60; // 15 min 
		$maxKeywords = 6;
		
		$blogDataArr = fill_assoc_array(db_res("SELECT * FROM `Blogs` WHERE ID = $blogID"));
		$blogData = $blogDataArr[0];
				
		if (trim($syndicateURL) == $blogData['SyndicateUrl'])
		{		
			// Check for last fetching time
			if ($blogData['SyndicateLastRan'] && 
				time() - $blogData['SyndicateLastRan'] < $PersonalBlogFetcherInterval)
				return true;
		}

		// Parse rss feed
		include_once('simplepie/simplepie.inc');
		include_once('simplepie/idn/idna_convert.class.php');	
		
		$feed = new SimplePie();
		$feed->set_feed_url($syndicateURL);
		$feed->enable_cache(false);	
		$feed->init();		
		$feed->handle_content_type();

		$items = $feed->get_items();	
		$itemsCount = $feed->get_item_quantity();
		if ($items && $itemsCount > 0)
		{
			// Sort items by date (first element must be the oldest)
			uasort($items, 'compareFeedItems');
			
			$itemsKeys = array_keys($items);
					
			// Search new posts by hash to syndicate with existing blog
			$indexSaveFrom = 0;
			if ($blogData['SyndicateLastRan'] && trim($blogData['SyndicateHash']) != '')
				for ($i = 0; $i < $itemsCount; $i++)
				{
					$curPost = $items[$itemsKeys[$i]];
					$curHash = md5($curPost->get_title() . $curPost->get_link());													
					if ($curHash == $blogData['SyndicateHash'])
					{					
						$indexSaveFrom = $i + 1;
						break;
					}
				}

			if ($indexSaveFrom < $itemsCount)
			{
				/*********************************
				// Syndicate blog posts addition
				**********************************/
				
				require_once('inc/keywords/keywords_extractor.php');
				$keywordsExtractor = new KeywordsExtractor("{$dir['inc']}/keywords/stopwords.txt");
				
				for ($i = $indexSaveFrom; $i < $itemsCount; $i++)
				{
					$syndicateCurPost = $items[$itemsKeys[$i]];
					$caption = process_db_input($syndicateCurPost->get_title());
					
					// Get category ID
					$curCategory = $syndicateCurPost->get_category()->term;			
					if ($curCategory)
					{
						$category = process_db_input(trim($curCategory));
						$categoryID = db_value("SELECT `CategoryID` FROM `BlogCategories` 
							WHERE `OwnerID` = $ownerID AND `CategoryName` LIKE '$category' AND `CategoryType` = 1");
						if (!$categoryID) // add new category of blog owner
						{
							$caturltitle = uniqueSEFUrl(getSEFUrl($category, sefBlogCategoryWords), 
								$ownerID, 'OwnerID', 'BlogCategories', 'caturltitle');
							db_res("INSERT INTO `BlogCategories` SET `OwnerID` = '$ownerID', 
								`CategoryName` = '$category', `CategoryType` = 1, `Date` = NOW(), `caturltitle` = '$caturltitle'");
							$categoryID = mysql_insert_id();
						}
					}
					else // add post to Default blog category
						$categoryID = db_value("SELECT `CategoryID` FROM `BlogCategories` 
							WHERE `OwnerID` = $ownerID AND `CategoryType` = 5");
					
					$text = process_db_input($syndicateCurPost->get_content());
					$date = process_db_input($syndicateCurPost->get_date('Y-m-d H:i:s'));
					$tags = process_db_input(implode(',',$keywordsExtractor->extract($caption, $text, $maxKeywords)));
					$urltitle = process_db_input(GetSEFUrl($caption, sefPostWords));
					
					if ($values) $values .= ', ';
					$values .= "($categoryID, '$caption', '$text', '$date', 'approval', '$tags', '$urltitle')";
				}

				$syndicateBlogPostsQuery = "INSERT INTO `BlogPosts` 
					(CategoryID, PostCaption, PostText, PostDate, PostStatus, Tags, urltitle) 
					VALUES $values";
				db_res($syndicateBlogPostsQuery);
	
				// Update syndicate blog hash (hash of the last syndicated post)
				$syndicateLastPost = $items[$itemsKeys[$itemsCount - 1]];
				$lastHash = md5($syndicateLastPost->get_title() . $syndicateLastPost->get_link());
				db_res("UPDATE `Blogs` SET `SyndicateHash` = '$lastHash' WHERE ID = $blogID");
			}
		}
		
		// Update last ran time
		db_res("UPDATE `Blogs` SET `SyndicateLastRan` = UNIX_TIMESTAMP() WHERE ID = $blogID");
	}
	
	function _pagination($totalPages, $curPage, $useSEF)
	{
			if ($useSEF)
			{
				$pattern = '/\/\d+/i';
				$mainLink = preg_replace($pattern, '', $_SERVER['REQUEST_URI']);
				if (strripos($mainLink, '/') == strlen($mainLink) - 1)
					$mainLink = substr($mainLink, 0, -1);
			}	
			else
			{
				$pattern = '/&page=(\d+)/i';
				$mainLink = preg_replace($pattern, '', $_SERVER['REQUEST_URI']);
			}
			
			$res .= '<div class="pagination">';
			for ($i = 1; $i <= $totalPages; $i++)
				if ($i != $curPage)
					$res .= ($useSEF) ? "<a href=\"$mainLink/$i\">$i</a> &nbsp;" 
						: "<a href=\"$mainLink&page=$i\">$i</a> &nbsp;";
				else
					$res .= "$i &nbsp;";
			$res .= '</div>';
			
			return $res;
	}
	
	function _sectionWithSearchBox($sectionTitle, $sectionCont)
	{
			$searchBox = <<<EOF
<div class="caption_item">
	<form name="BlogsSearchForm" action="blogs.php" method="get">
	<input type="hidden" name="action" value="search"/>
	<input type="text" name="keyword" size="23" value="{$_REQUEST['keyword']}"/>
	<input type="submit" value="Search"/>
</form></div>
EOF;
		return DesignBoxContent($sectionTitle, $sectionCont, 1, $searchBox);
	}
}

// Return 1 if $item1 is newer than $item2
function compareFeedItems($item1, $item2)
{
	return (strtotime($item1->get_date()) < strtotime($item2->get_date())) ? -1 : 1;
}