<?php



require_once( BX_DIRECTORY_PATH_CLASSES . 'BxDolDb.php' );


class BxDolVotingQuery extends BxDolDb

{

	var $_aSystem; // current voting system



	function BxDolVotingQuery(&$aSystem)

	{

		$this->_aSystem = &$aSystem;

		parent::BxDolDb();

	}



	function  getVote ($iId)

    {

		$sPre = $this->_aSystem['row_prefix'];

		$sTable = $this->_aSystem['table_rating'];



		return $this->getRow("SELECT `{$sPre}rating_count` as `count`, (`{$sPre}rating_sum` / `{$sPre}rating_count`) AS `rate` FROM {$sTable} WHERE `{$sPre}id` = '$iId' LIMIT 1");

	}
	
	/**
	 * Get member id by voting item id
	 *
	 * @param int $iId voting item id
	 * @return int member id
	 * 
	 * @author Anton Andriyevskyy
	 */
	function  getVoteMemberId ($iId)
    {
    	// Specifies how to find member id for given voting item id by rating table name;
    	// memberid_itself = true if $iId is member id already,
    	// lookup_table - table where correspondence between member id and voting item id takes place,
    	// voting_itemid_field,
    	// memberid_field (database field can be int or varchar)
    	$memberRelation = array(
    		'profile_rating' => array(
    			'memberid_itself' => true,
    			'lookup_table' => '',
    			'voting_itemid_field' => '',
    			'memberid_field' => ''
    		),
    		'media_rating' => array(
    			'memberid_itself' => false,
    			'lookup_table' => 'media',
    			'voting_itemid_field' => 'med_id',
    			'memberid_field' => 'med_prof_id'
    		),
    		'gphoto_rating' => array(
    			'memberid_itself' => false,
    			'lookup_table' => 'sharephotofiles',
    			'voting_itemid_field' => 'medID',
    			'memberid_field' => 'medProfId'
    		),
    		'gmusic_rating' => array(
    			'memberid_itself' => false,
    			'lookup_table' => 'raymp3files',
    			'voting_itemid_field' => 'ID',
    			'memberid_field' => 'Owner'
    		),
    		'gvideo_rating' => array(
    			'memberid_itself' => false,
    			'lookup_table' => 'raymoviefiles',
    			'voting_itemid_field' => 'ID',
    			'memberid_field' => 'Owner'
    		),
    	);
    	
		$sTable = $this->_aSystem['table_rating'];
		$relation = $memberRelation[$sTable];
		if ($relation['memberid_itself'])
			return $iId;
		else 
		{
			$memberid = $this->getOne(
				"SELECT `{$relation['memberid_field']}` as `memberid` 
				FROM {$relation['lookup_table']} 
				WHERE `{$relation['voting_itemid_field']}` = '$iId' LIMIT 1");	
			return intval($memberid);
		}
	} 



	function  putVote ($iId, $sIp, $iRate)

	{

		$sPre = $this->_aSystem['row_prefix'];



		$sTable = $this->_aSystem['table_rating'];



		if ($this->getOne("SELECT `{$sPre}id` FROM $sTable WHERE `{$sPre}id` = '$iId' LIMIT 1"))

		{

			$ret = $this->query ("UPDATE {$sTable} 	SET `{$sPre}rating_count` = `{$sPre}rating_count` + 1, `{$sPre}rating_sum` = `{$sPre}rating_sum` + '$iRate' WHERE `{$sPre}id` = '$iId'");

		}

		else

		{

			$ret = $this->query ("INSERT INTO {$sTable} SET `{$sPre}id` = '$iId', `{$sPre}rating_count` = '1', `{$sPre}rating_sum` = '$iRate'");



		}

		if (!$ret) return $ret;



		$sTable = $this->_aSystem['table_track'];

		return $this->query ("INSERT INTO {$sTable} SET `{$sPre}id` = '$iId', `{$sPre}ip` = '$sIp', `{$sPre}date` = NOW()");

	}



	function isDublicateVote ($iId, $sIp)

	{

		$sPre = $this->_aSystem['row_prefix'];

		$sTable = $this->_aSystem['table_track'];

		$iSec = $this->_aSystem['is_duplicate'];

	

		return $this->getOne ("SELECT `{$sPre}id` FROM {$sTable} WHERE `{$sPre}ip` = '$sIp' AND `{$sPre}id` = '$iId' AND UNIX_TIMESTAMP() - UNIX_TIMESTAMP(`{$sPre}date`) < $iSec");

		

    }



    function getSqlParts ($sMailTable, $sMailField)

    {

        if ($sMailTable) 

            $sMailTable .= '.';



        if ($sMailField)

            $sMailField = $sMailTable.$sMailField;



		$sPre = $this->_aSystem['row_prefix'];

        $sTable = $this->_aSystem['table_rating'];



        return array (

            'fields' => ",$sTable.`{$sPre}rating_count` as `voting_count`, ($sTable.`{$sPre}rating_sum` / $sTable.`{$sPre}rating_count`) AS `voting_rate` ",

            //'fields' => ",34 as `voting_count`, 2.5 AS `voting_rate` ",

            'join' => " LEFT JOIN $sTable ON ({$sTable}.`{$sPre}id` = $sMailField) "

        );

    }



    function deleteVotings ($iId)    

    {

        $sPre = $this->_aSystem['row_prefix'];        



        $sTable = $this->_aSystem['table_track'];        

        $this->query ("DELETE FROM {$sTable} WHERE `{$sPre}id` = '$iId'");



        $sTable = $this->_aSystem['table_rating'];        

        return $this->query ("DELETE FROM {$sTable} WHERE `{$sPre}id` = '$iId'");

    }



    function getTopVotedItem ($iDays, $sJoinTable = '', $sJoinField = '', $sWhere = '')

    {

        $sPre = $this->_aSystem['row_prefix'];

        $sTable = $this->_aSystem['table_track'];



        $sJoin = $sJoinTable && $sJoinField ? " INNER JOIN $sJoinTable ON ({$sJoinTable}.{$sJoinField} = $sTable.`{$sPre}id`) " : '';



        return $this->getOne ("SELECT $sTable.`{$sPre}id`, COUNT($sTable.`{$sPre}id`) AS `voting_count` FROM {$sTable} $sJoin WHERE TO_DAYS(NOW()) - TO_DAYS($sTable.`{$sPre}date`) <= $iDays $sWhere GROUP BY $sTable.`{$sPre}id` HAVING `voting_count` > 2 ORDER BY `voting_count` DESC LIMIT 1");

    }

}



