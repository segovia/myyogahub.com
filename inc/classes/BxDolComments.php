<?

require_once(BX_DIRECTORY_PATH_INC . 'header.inc.php' );
require_once(BX_DIRECTORY_PATH_INC . 'admin.inc.php');
require_once(BX_DIRECTORY_PATH_INC . 'design.inc.php' );
require_once(BX_DIRECTORY_PATH_INC . 'profiles.inc.php' );
require_once(BX_DIRECTORY_PATH_INC . 'utils.inc.php' );
require_once(BX_DIRECTORY_PATH_INC . 'tags.inc.php' );

require_once(BX_DIRECTORY_PATH_ROOT . 'recaptcha/config.php');
require_once(BX_DIRECTORY_PATH_ROOT . 'recaptcha/recaptchalib.php');

/*
 * class for Events
 */
class BxDolComments {



	//comments type (1-Classifieds, 2-Blogs, 3-Events)

	var $iCType;



	//name of table with comments

	var $sTableCommentsName;



	//admin mode, can All actions
	var $bAdminMode;



	//friends ability

	var $bFriendsMode;

	var $sCurrBrowsedFile;



	var $iVisitorID;
	//var $iBigThumbSize = 110;



	//constructor

	function BxDolComments( $iCType, $sCurrBrowsedFile='' ) {

		$this->iCType = $iCType;

		$this->iVisitorID = (int)$_COOKIE['memberID'];
		$this->sCurrBrowsedFile = ($sCurrBrowsedFile=='') ? $_SERVER['PHP_SELF'] : $sCurrBrowsedFile;

	}

	function SubscribedToNotifications($memberID, $postID)
	{
		return db_value("SELECT COUNT(*) 
			FROM BlogPostCommentsNotifications
			WHERE PostID = $postID AND UserID = $memberID"); 
	}
	


	/**
	 * Generate Comments Section
	 *
	 * @param $iElementID - Post ID
	 * @param $iOwnerID - Owner ID
	 * @return HTML presentation of data
	 */
	function PrintCommentSection($iElementID, $sCommentLbl='', $sLimit, $sNav) {
		global $prof;
		global $site;
		global $logged;

		$sRetHtml = '';

		$sCommentsC = ($sCommentLbl=='') ? _t('_comments') : $sCommentLbl;
		$sSbjN = _t('_Subject');
		$sPostedByC = _t('_Posted by');
		$sDateC = _t('_Date');
		$sLocationC = _t('_Location');
		$sAdminLocalAreaC = _t('_AdminArea');
		$sAdminC = _t('_Admin');
		$sSureC = _t("_Are you sure");
		$sPostCommentC = _t('_Post Comment');
		$sLeaveCommentC = _t('_LeaveComment');
		$sAddCommentC = _t('_Add comment');
		$sEditC = _t('_Edit');
		$sDeleteC = _t('_Delete');
		$sCommentFriendsOnlyC = _t('_commenting_this_blog_allowed_only_for_friends');

		$sPostDataSQL = $this->SelectionObjectSQL($iElementID);
		$aPostData = db_arr($sPostDataSQL);

		$iOwnerID = $aPostData['OwnerID'];

		//$bFriend = is_friends( $this->iVisitorID, $aPostData['OwnerID'] );
		//$bOwner = ($this->iVisitorID==$aPostData['OwnerID']) ? true : false;
		$bCanPostComment = $this->GetPostNewCommentPermission($aPostData);

		$sQuery = $this->SelectionCommentsObjectSQL($iElementID, $sLimit);	
		$vSqlRes = db_res ($sQuery);		
		$sCommsHtml = '';
		$showActionForms = false;
		
		// Show Subscribe/Unsubscribe to comments notifications 
		// on this blog post for loggedin member 
		if ($this->iCType==2 && $logged['member'])
		{
			$notificationForm = <<<EOF
			<form name="notificationForm" method="post" action="{$_SERVER['REQUEST_URI']}">
			<input type="hidden" name="subscribe" value=""/>
			</form>
EOF;
		
			if ($this->SubscribedToNotifications($this->iVisitorID, $iElementID))
				$subscribeMsg = 'You are subscribed to follow comments via email. 
					<a href="javascript:void(0)" 
					onclick="document.forms.notificationForm.subscribe.value=0;
					document.forms.notificationForm.submit();">Unsubscribe</a>';
			else
			{
				$subscribeMsg = '<a href="javascript:void(0)" 
					onclick="document.forms.notificationForm.subscribe.value=1;
					document.forms.notificationForm.submit();">Subscribe</a> 
					to follow comments via email';
				$notifyInput = 
					'<div style="margin:5px 0">
						<input type="checkbox" name="notify" checked="1"/>
							Notify me of followup comments via e-mail
					</div>';
			}
		}
		
		while( $aSqlResStr = mysql_fetch_assoc($vSqlRes) ) {

			$aCommentData = $this->FillCommentsData($aSqlResStr);
			$aProfileInfo = getProfileInfo($aCommentData['ProfID']);
			if ($aCommentData['ProfID']==0)
			{
				$name = trim($aCommentData['Name']);	
				if ($name == '')
					$sPostedBy = $sAdminC;
				else
				{
					$website = trim($aCommentData['Website']);
					if ($website == '')
						$sPostedBy = $name;
					else 
					{
						if (strpos($website, 'http') === false)
							$website = "http://$website";
						$sPostedBy = "<a href='$website' rel='nofollow' target='_blank'>$name</a>";
					}
				}
			}
			else 
				$sPostedBy = '<a href="'.getProfileLink($aCommentData['ProfID']).'">'.
					$aProfileInfo['NickName'].'</a>';
			$sCountryName = ($aProfileInfo['Country']=="")?$sAdminLocalAreaC:_t('__'.$prof['countries'][ $aProfileInfo['Country'] ] );
			$sCountryPic = ($aProfileInfo['Country']=='')?'':' <img alt="'.$aProfileInfo['Country'].'" src="'.($site['flags'].strtolower($aProfileInfo['Country'])).'.gif"/>';
			$sUserIcon = get_member_icon($aCommentData['ProfID'], 'left', true);
			$aCommentData['Text'] = ( $aCommentData['Text']);

			//$sTimeAgo = _format_when($aCommentData['Time']);
			$sTime = gmdate("j M H:i", $aCommentData['Date']);
			
			if ($this->iCType == 2)
				$aCommentData['Text'] = nl2br($aCommentData['Text']);
			$sMessageBR = jsNewLinesClear(addslashes(htmlspecialchars($aCommentData['Text'])));

			$sFullPermissions = $this->GetElementFullPermission($aPostData, $aCommentData);

			$sAdminActions = '';
			if ($sFullPermissions) {
				$showActionForms = true;
			
				if ($this->iCType==1) {
					$editAction = ($aCommentData['ProfID']==$this->iVisitorID) ?
						"<a href=\"{$this->sCurrBrowsedFile}\" onclick=\"javascript: UpdateField('EditCommentID',{$aCommentData['ID']});UpdateField('EAdvID',{$iElementID}); UpdateFieldStyle('answer_form_to_1','block'); return false;\">{$sEditC}</a> |"
						: '';
					$sAdminActions = <<<EOF
<div class="adminActions">
	$editAction
	<a href="{$_SERVER['PHP_SELF']}?ShowAdvertisementID={$aCommentData['ID']}" onclick="javascript: UpdateField('DeleteCommentID',{$aCommentData['ID']});UpdateField('DAdvID',{$iElementID});document.forms.command_delete_comment.submit(); return false;">{$sDeleteC}</a>
</div>
EOF;
				}
				if ($this->iCType==2) {
					$editAction = ($aCommentData['ProfID']==$this->iVisitorID) ?
						"<a href=\"{$this->sCurrBrowsedFile}\" onclick=\"javascript: UpdateField('EditCommentID',{$aCommentData['ID']});UpdateField('EPostID',{$iElementID});UpdateFieldStyle('answer_form_to_1','block');UpdateFieldStyle('add_comment_label','none');UpdateFieldTiny('commenttext_to_1','{$sMessageBR}'); return false;\">{$sEditC}</a> |"
						: '';
					$sAdminActions = <<<EOF
<div class="adminActions">
	$editAction
	<a href="{$this->sCurrBrowsedFile}" onclick="javascript: UpdateField('DeleteCommentID',{$aCommentData['ID']});UpdateField('DPostID',{$iElementID});document.forms.command_delete_comment.submit(); return false;">{$sDeleteC}</a>
</div>
EOF;
				}
				if ($this->iCType==3) {
					$editAction = ($aCommentData['ProfID']==$this->iVisitorID) ?
						"<a href=\"{$this->sCurrBrowsedFile}\" onclick=\"javascript: UpdateField('EditCommentID',{$aCommentData['ID']});UpdateField('EEventID',{$iElementID});UpdateFieldStyle('answer_form_to_1','block');UpdateFieldStyle('add_comment_label','none');UpdateFieldTiny('commenttext_to_1','{$sMessageBR}'); return false;\">{$sEditC}</a> |"
						: '';
					$sAdminActions = <<<EOF
<div class="adminActions">
	$editAction
	<a href="{$this->sCurrBrowsedFile}" onclick="javascript: UpdateField('DeleteCommentID',{$aCommentData['ID']});UpdateField('DEventID',{$iElementID});document.forms.command_delete_comment.submit(); return false;">{$sDeleteC}</a>
</div>
EOF;
				}				
			}
			
$sCommsHtml .= <<<EOF
<div class="commentUnit">
	<table><tr style="vertical-align:top">
		<td><div class="userPic">$sUserIcon</div></td>
		<td>
			<div class="commentMain">
				<div class="commentInfo">
					$sPostedBy
				</div>
				<br/>
				<div class="newsDate" style="float:left; margin-top:3px;">$sTime</div>
				<div class="commentText">{$aCommentData['Text']}</div>
			</div>
		</td></tr>
	<tr><td colspan="2">
	$sAdminActions
	</td></tr></table>
	<div class="clear_both"></div>
</div>
EOF;

		} // end while
		
		$sPostNewComm = '';
		if ($bCanPostComment) {
			if ($this->iCType==1) {
				$sPostNewComm = <<<EOF
<form action="{$this->sCurrBrowsedFile}" method="post" name="post_comment_adv_form">
	<input type="hidden" name="CommAdvertisementID" value="{$iElementID}" />
	<textarea name="message" id="postNewComm" rows="5" cols="30" class="classfiedsTextArea"></textarea>
	<input id="postCommentAdv" name="postCommentAdv" type="submit" value="{$sPostCommentC}"/>
</form>
EOF;
			}
			if ($this->iCType==2) {
				$commentText = '';

				if (!$logged['member'])
				{
					$name = '';
					$email = '';
					$website = '';
					$captchaErrorDisplay = 'none';
			
					// Try to get prefilled values from session table
					if (!session_id())
						session_start();	
											
					$prefilledData = db_value("SELECT data FROM session 
						WHERE sessionid='".session_id()."' AND name='blogCommentFields'");
										
					if ($prefilledData)
					{
						$prefilledDataArr = unserialize($prefilledData);
						if (is_array($prefilledDataArr))
						{
							$name = $prefilledDataArr['name'];
							$email = $prefilledDataArr['email'];
							$website = $prefilledDataArr['website'];
							$commentText = $prefilledDataArr['commentText'];
							$captchaErrorDisplay = 'block';
						}
						
						// Clear prefilled values
						db_res("DELETE FROM session WHERE sessionid = '".session_id()."'
							AND name = 'blogCommentFields'");
					}
						
					$nonMemberFields = 
						'<div class="margin_bottom_10">
							<input type="text" name="name" maxlength="25"
								onkeyup="CheckSubmitEnabled()" value="'.$name.'"/> 
							<span class="fieldComment">Name (required)</span></div>
						<div>
							<input type="text" name="email" maxlength="60"
								onkeyup="CheckSubmitEnabled()" onblur="javascript:
								var emailErrorMsg = document.getElementById(\'emailErrorMsg\');
								if (EmailCorrect(this.value))
									emailErrorMsg.style.display = \'none\';
								else
									emailErrorMsg.style.display = \'block\';" value="'.$email.'"/> 
							<span class="fieldComment">Mail (will not be published) (required)</span></div>
						<div id="emailErrorMsg" style="display:none; color:red">Incorrect Email</div>
						<div style="margin:10px 0">
							<input type="text" name="website" maxlength="100" value="'.$website.'"/> 
							<span class="fieldComment">Website</span></div>';


					$recaptchaImage = recaptcha_get_html(RECAPTCHA_PUBLIC_KEY /*, $error*/);

					$captcha = '<div>
						<script type="text/javascript">
							var RecaptchaOptions = {
								theme : \'white\'
							};
 						</script>'
						.$recaptchaImage.'</div>
						<div id="captchaError" style="display:'.$captchaErrorDisplay.'; 
							color:red; padding:5px 0">
							The verification code is incorrect, please try again.</div>';
					$nonMemberScript = <<<EOF
<script src="inc/js/functions.js"></script>
<script>
	function EmailCorrect(email)
	{
		var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
		return reg.test(email);
	}

	function CheckSubmitEnabled()
	{
		var submitButton = document.getElementById('postCommentPost');
		var form = document.forms.post_comment_adv_form;
		var name = form.name.value;
		var email = form.email.value;
		submitButton.disabled = (name.trim() == '' || email.trim() == '');
	}
	CheckSubmitEnabled();
</script>
EOF;
				}

				$sPostNewComm = <<<EOF
<div style="display:none; margin-left:8px;" class="addcomment_textarea" id="answer_form_to_0">
	<form action="{$_SERVER['REQUEST_URI']}" 
		method="post" name="post_comment_adv_form">
		$nonMemberFields
		<!--<textarea name="commentText" class="comment_textarea" id="commenttext_to_0"></textarea>-->
		<textarea name="commentText" id="commenttext_to_0">$commentText</textarea>
		$notifyInput
		$captcha
		<div class="addcomment_submit" style="text-align:center;padding-top:5px">
			<input type="hidden" name="action" value="addcomment" />
			<input type="hidden" name="CommPostID" value="{$iElementID}" />
			<input type="hidden" name="ownerID" value="{$this->aBlogConf['ownerID']}" />
			<input type="submit" id="postCommentPost" name="postCommentPost" value="Post" onclick="return CheckAllowSubmit();"/>
			<input type="button" value="Cancel" onClick="
				javascript: document.getElementById('answer_form_to_0').style.display = 'none';
				document.getElementById('add_comment_label').style.display = 'block'" />
		</div>
	</form>
</div>
<script>
function CheckAllowSubmit()
{
	var email = document.forms.post_comment_adv_form.email;
	if (email && !EmailCorrect(email.value))
	{
		document.getElementById('emailErrorMsg').style.display = 'block';
		return false;
	}
	
	var isEmptyComment = (document.forms.post_comment_adv_form.commenttext_to_0.value.replace(/ /g, '').replace(/\\r\\n/g, '').length == 0);
	if (isEmptyComment)
	{ 
		alert('You have not posted any comments');
		return false;
	}
	/*
	if (navigator.appName == 'Microsoft Internet Explorer')
		isEmptyComment = (document.getElementById('mce_editor_0').contentWindow.document.body.innerText.replace(/ /g, '').replace(/\\r\\n/g, '').length == 0);
	else
		isEmptyComment = (document.getElementById('mce_editor_0').contentDocument.body.innerHTML.replace(/<.*?>/g, '').replace(/ /g, '').replace(/&amp;nbsp;/g, '').replace(/&nbsp;/g, '').length == 0);
	*/	
}
</script>
$nonMemberScript
EOF;
			}
			if ($this->iCType==3) {
				$sPostNewComm = <<<EOF
<div style="display:none; margin-left:8px;" class="addcomment_textarea" id="answer_form_to_0">
	<form method="post" action="{$_SERVER['PHP_SELF']}?fileID={$aFile['medID']}">
		<textarea name="commentText" class="comment_textarea" id="commenttext_to_0"></textarea>
			<div class="addcomment_submit" style="text-align:center;">
				<input type="hidden" name="action" value="addcomment" />
				<input type="hidden" name="CommEventID" value="{$iElementID}" />
				<input type="submit" name="commentAdd" value="Post" />
				<input type="button" value="Cancel" onClick="
					javascript: document.getElementById('answer_form_to_0').style.display = 'none';
					document.getElementById('add_comment_label').style.display = 'block'" />
			</div>
	</form>
</div>

EOF;
			}			
		}

		if ($showActionForms) {
			if ($this->iCType==1) {
				$sAdminFormActions = <<<EOF
<form action="{$this->sCurrBrowsedFile}" method="post" name="command_edit_comment_adv_form">
	<input type="hidden" name="EditCommentID" id="EditCommentID" value=""/>
	<input type="hidden" name="EAdvID" id="EAdvID" value=""/>
	<textarea name="message" id="postEditComm" rows="20" cols="60" class="classfiedsTextArea" style="width:{$this->iBigThumbSize}px;"></textarea>
	<input type="submit" value="{$sPostCommentC}"/>
</form>
EOF;
			}
			if ($this->iCType==2) {
				$sAdminFormActions = <<<EOF
<form action="{$_SERVER['REQUEST_URI']}" method="post" name="command_edit_comment_post_form">
	<input type="hidden" name="action" value="editcomment" />
	<input type="hidden" name="EditCommentID" id="EditCommentID" value=""/>
	<input type="hidden" name="EPostID" id="EPostID" value=""/>
	<input type="hidden" name="ownerID" value="{$this->aBlogConf['ownerID']}" />
	<textarea name="commentText" id="commenttext_to_1" rows="10" cols="60" class="classfiedsTextArea" style="width:{$this->iBigThumbSize}px;"></textarea>
	<br/>
	<input type="submit" value="{$sPostCommentC}" style="margin-top:5px"/>
</form>
<form action="{$_SERVER['REQUEST_URI']}" method="post" name="command_delete_comment">
	<input type="hidden" name="DeleteCommentID" id="DeleteCommentID" value=""/>
	<input type="hidden" name="DPostID" id="DPostID" value=""/>
	<input type="hidden" name="action" id="action" value="delete_comment" />
	<input type="hidden" name="ownerID" value="{$this->aBlogConf['ownerID']}" />
</form>
EOF;
			}
			if ($this->iCType==3) {
				$sAdminFormActions = <<<EOF
<form action="{$this->sCurrBrowsedFile}?action=show_info&amp;event_id={$iElementID}" method="post" name="command_edit_comment_post_form">
	<input type="hidden" name="action" value="editcomment" />
	<input type="hidden" name="EditCommentID" id="EditCommentID" value=""/>
	<input type="hidden" name="EEventID" id="EEventID" value=""/>
	<input type="hidden" name="ownerID" value="{$this->aBlogConf['ownerID']}" />
	<textarea name="commentText" id="commenttext_to_1" rows="10" cols="60" class="comment_textarea" style="width:{$this->iBigThumbSize}px;"></textarea>
	<input type="submit" value="{$sPostCommentC}" style="margin-top:5px"/>
</form>
<form action="{$this->sCurrBrowsedFile}?action=show_info&amp;event_id={$iElementID}" method="post" name="command_delete_comment">
	<input type="hidden" name="DeleteCommentID" id="DeleteCommentID" value=""/>
	<input type="hidden" name="DEventID" id="DEventID" value=""/>
	<input type="hidden" name="action" id="action" value="delete_comment" />
	<input type="hidden" name="ownerID" value="{$this->aBlogConf['ownerID']}" />
</form>
EOF;
			}			
		}

		$sCommentActions = '';
		if (!$bCanPostComment)
		{
			$noPermissionsMsg = '';
			if ($aPostData['PostCommentPermission'] == 'members')
				$noPermissionsMsg = 'Commenting in this blog allowed for YogaHub community members only';
			elseif ($aPostData['PostCommentPermission'] == 'friends')
				$noPermissionsMsg = $sCommentFriendsOnlyC;
			elseif ($aPostData['PostCommentPermission'] == 'favorites')
				$noPermissionsMsg = 'Commenting in this blog allowed for favorites only';
			if ($noPermissionsMsg) {
				$sImgFriend = <<<EOF
<img src="{$site['icons']}lock.gif" alt="{$noPermissionsMsg}" title="{$noPermissionsMsg}" 
	style="height:40px; margin-right:5px; vertical-align:middle" />
EOF;
				$sCommentActions = '<div style="float:left; width:490px">'. MsgBox($noPermissionsMsg, 16, 2, $sImgFriend). '</div>';
			} 
		}
		else {
			$sCommentActions = <<<EOF
<div id="add_comment_label">
	<img src="{$site['icons']}add_comment.gif" alt="{$sPostCommentC}" title="{$sPostCommentC}" class="marg_icon" />
	<a class="actions" onclick="document.getElementById('answer_form_to_0').style.display = 'block'; document.getElementById('add_comment_label').style.display = 'none'; return false;" href="{$this->sCurrBrowsedFile}">{$sPostCommentC}</a>
</div>
EOF;
		}

		$sCommentsContent = <<<EOF

<div id="comments_section">
	$notificationForm
	<div style="clear:both; margin-bottom:10px">$subscribeMsg</div>
	{$sCommsHtml}
	{$sNav}
	{$sCommentActions}
	{$sPostNewComm}
	<div id="answer_form_to_1" style="display:none; clear:left;">
		{$sAdminFormActions}
	</div>
</div>
EOF;

		//$show_hide = $this -> genShowHideItem( 'comments_section' );
			
		$leaveCommentLink = ($bCanPostComment) ? "<div class='title_content'>
			<a href='". $_SERVER['REQUEST_URI'] ."#answer_form_to_0' class='title_content_link' id='leave_comment_link'
				onclick=\"document.getElementById('answer_form_to_0').style.display = 'block'; 
				document.getElementById('add_comment_label').style.display = 'none';\">
				"._t('_Post Comment').'</a>' : '';

		return DesignBoxContent ( $sCommentsC, $sCommentsContent, 1, $show_hide, $leaveCommentLink);;
	}

	function GetPostNewCommentPermission($aPostData)
	{
		global $logged;

		// Check is visitor in blocklist of photo owner
		$visitorID = (int)$_COOKIE['memberID'];
		$isBlocked = db_value("SELECT ID FROM BlockList WHERE ID=".$aPostData['OwnerID']." AND Profile=$visitorID LIMIT 1");		
		
		/*
		// Check is visitor have Unconfirmed status
		$status = db_value("SELECT `Status` FROM `Profiles` WHERE ID=$visitorID LIMIT 1");
		$isUnconfirmed = ($status == 'Unconfirmed');
		*/
		
		//if ($isBlocked || $isUnconfirmed)
		if ($isBlocked)
			return false;

		if ($this->iCType == 2) // blog post comment
		{
			$bFriend = is_friends( $this->iVisitorID, $aPostData['OwnerID'] );
			$bHotlistFriend = is_hotlist_friends($aPostData['OwnerID'], $this->iVisitorID);
			$bOwner = ($this->iVisitorID==$aPostData['OwnerID']) ? true : false;
			
			if ($aPostData['PostCommentPermission'] == 'members')
				return ($logged['member'] || $this->bAdminMode);
			elseif ($aPostData['PostCommentPermission'] == 'friends')
				return ($bFriend || $bOwner || $this->bAdminMode);
			elseif ($aPostData['PostCommentPermission'] == 'favorites')
				return ($bHotlistFriend || $bOwner || $this->bAdminMode);
			else
				return true;
		}
		else
			return true;
	}

	function GetElementFullPermission($aPostData, $aCommentData){
		if (!$this->iVisitorID) return false;
		
		$bFullAccess = true;

		//$bFriend = is_friends( $this->iVisitorID, $aPostData['OwnerID'] );
		$bOwner = ($this->iVisitorID == $aPostData['OwnerID']);

		switch ($this->iCType) {

			case 2:
			case 3:

				if ($bOwner || $this->bAdminMode || $aCommentData['ProfID']==$this->iVisitorID) {

					$bFullAccess = true;

				} else {
					$bFullAccess = false;
				}

				break;

		}

		return $bFullAccess;
	}



	/**
	 * Fill Array by comments data
	 *
	 * @param $aSqlResStr - comment data
	 * @return SQL request
	 */

	function FillCommentsData($aSqlResStr) {

		$aCommentData = array();

		switch ($this->iCType) {
			case 1:

				$aCommentData['ProfID'] = $aSqlResStr['IDProfile'];

				$aCommentData['Text'] = $aSqlResStr['Message'];

				$aCommentData['ID'] = $aSqlResStr['ID'];

				$aCommentData['Time'] = $aSqlResStr['sec'];

				$aCommentData['Date'] = $aSqlResStr['DateTime'];

				break;

			case 2:
			case 3:
				
				$aCommentData['ProfID'] = $aSqlResStr['SenderID'];

				$aCommentData['Text'] = $aSqlResStr['CommentText'];

				$aCommentData['ID'] = $aSqlResStr['CommentID'];

				$aCommentData['Time'] = $aSqlResStr['sec'];

				$aCommentData['Date'] = $aSqlResStr['DateTime'];

				break;

		}
		if ($this->iCType == 2)
		{
			$aCommentData['Name'] = $aSqlResStr['Name'];
			$aCommentData['Email'] = $aSqlResStr['Email'];
			$aCommentData['Website'] = $aSqlResStr['Website'];
			$aCommentData['Notify'] = $aSqlResStr['Notify'];
		}

		return $aCommentData;

	}



	function genShowHideItem( $wrapperID, $default = '' ) {

		$sHideC = _t( '_Hide' );

		$sShowC = _t( '_Show' );

		if( !$default )

			$default = $sHideC;



		return <<<EOF

<div class="caption_item">

	<a href="{$this->sCurrBrowsedFile}"

	  onclick="javascript: el = document.getElementById('{$wrapperID}');

				if(el.style.display == 'none') {el.style.display = 'block'; this.innerHTML='{$sHideC}'; }

				else {el.style.display = 'none'; this.innerHTML = '{$sShowC}';} return false;">{$default}</a>

</div>

EOF;

	}



	/**
	 * SAFE SQL - Get Main data about Element: owner, category, 
	 *
	 * @param $iElementID - Post ID
	 * @return SQL request
	 */

	function SelectionObjectSQL($iElementID){

		$sPostDataSQL = '';

		switch ($this->iCType) {
			case 1:

				$sPostDataSQL = "SELECT `ClassifiedsAdvertisements`.`IDProfile`
				FROM `ClassifiedsAdvertisements`
				/* INNER JOIN `ClassifiedsSubs` ON `ClassifiedsSubs`.`ID` = `ClassifiedsAdvertisements`.`IDClassifiedsSubs`
				INNER JOIN `Classifieds` ON `Classifieds`.`ID` = `ClassifiedsSubs`.`IDClassified` */
				WHERE `ClassifiedsAdvertisements`.`ID` = {$iElementID} LIMIT 1";

				break;

			case 2:

				$sPostDataSQL = "SELECT `BlogPosts`.`PostCommentPermission`, `BlogCategories`.`OwnerID`
				FROM `BlogPosts`
				INNER JOIN `BlogCategories` ON `BlogCategories`.`CategoryID` = `BlogPosts`.`CategoryID`
				WHERE `BlogPosts`.`PostID` = {$iElementID} LIMIT 1";

				break;
				
			case 3:

				$sPostDataSQL = "SELECT ResponsibleID as OwnerID
				FROM `SDatingEvents`
				WHERE `ID` = {$iElementID} LIMIT 1";

				break;

		}

		return $sPostDataSQL;

	}



	/**
	 * SAFE SQL - Get Comments data of Element:
	 *
	 * @param $iElementID - Post ID
	 * @return SQL request
	 */

	function SelectionCommentsObjectSQL($iElementID, $sLimit){

		$sPostDataSQL = '';

		switch ($this->iCType) {
			case 1:

				$sQuery = "
					SELECT `ClsAdvComments`.*, (UNIX_TIMESTAMP() - UNIX_TIMESTAMP(`ClsAdvComments`.`DateTime`)) AS `sec`,
						UNIX_TIMESTAMP(`ClsAdvComments`.`DateTime`) as `DateTime`
					FROM `ClsAdvComments`
					WHERE `ClsAdvComments`.`IDAdv` = {$iElementID}
					ORDER BY 'sec' DESC
				";

				break;

			case 2:

				$sQuery = "
					SELECT `BlogPostComments`.*, (UNIX_TIMESTAMP() - UNIX_TIMESTAMP(`BlogPostComments`.`Date`)) AS `sec`,
						UNIX_TIMESTAMP(`BlogPostComments`.`Date`) as `DateTime`
					FROM `BlogPostComments`
					WHERE `BlogPostComments`.`PostID` = {$iElementID}
					ORDER BY `Date` DESC
					$sLimit
				";

				break;
				
			case 3:

				$sQuery = "
					SELECT `SDatingEventComments`.*, (UNIX_TIMESTAMP() - UNIX_TIMESTAMP(`SDatingEventComments`.`Date`)) AS `sec`,
						UNIX_TIMESTAMP(`SDatingEventComments`.`Date`) as `DateTime`
					FROM `SDatingEventComments`
					WHERE `SDatingEventComments`.`EventID` = {$iElementID}
					ORDER BY `Date` DESC
					$sLimit
				";

				break;				

		}

		return $sQuery;

	}



}
