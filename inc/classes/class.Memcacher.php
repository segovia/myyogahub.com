<?php

class Memcacher
{
	private $memcache_obj = null;
	private $host;
	private $port = 11211;
	
	function Memcacher()
	{
		if (strpos($_SERVER['HTTP_HOST'], 'belucky') !== false || 
			strpos($_SERVER['HTTP_HOST'], 'localhost') !== false ||
			strpos($_SERVER['HTTP_HOST'], 'megliosoft') !== false)
			$this->host = 'megliosoft.org';
		elseif (strpos($_SERVER['HTTP_HOST'], 'dev.') !== false)
			$this->host = '192.168.1.2';
		else
			$this->host = '192.168.1.1';
	}
	
	public function obj()
	{
		if (is_null($this->memcache_obj))
		{
			$this->memcache_obj = new Memcache;
			$this->memcache_obj->connect($this->host, $this->port);
		}
		return $this->memcache_obj;
	}
}

?>