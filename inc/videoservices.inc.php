<?php
/*******************************

* name: the name of videoservice;
* url: link to the main page of videoservice;
* regexUrl: regular expression for JavaScript test match videoservice in the url;
* regexCorrectEmbed: regular expression for JavaScript test match videoservice in the embed source;
* regexImageUrl (optional): regular expression for extraction link to the video thumbnail from page source;
* embedHTML: HTML template for embeding video to the site;

* getCodeAction[method]: 
	RegexURL - extract embed code from url;
	RegexPage - extract embed code from page source;
* getCodeAction[regex]: regular expression for embed code extraction;

* regexEmbedCode (optional): regular expression for extract embed code from embed source;
* regexEmbedUrl (optional): regular expression for extract url to this video from embed source;
Note: regexEmbedCode or regexEmbedUrl must be specified!

* urlByCode (optional): URL template for the link to this video

*******************************/

$embed = array();
 
$embed['acceptabletv'] = array(
	'name' => 'AcceptableTv',
	'url' => 'www.acceptable.tv',
	'regexUrl' => 'acceptable.tv/videos',
	'regexCorrectEmbed' => 'flash.revver.com/player',
	'embedHTML' => '<script src="http://flash.revver.com/player/1.0/player.js?mediaId:{embedCode};height:{height};width:{width};pngLogo:http%3A//acceptable.tv/images/structure/check.png" type="text/javascript"></script>',
	'getCodeAction' => array(
		'method' => 'RegexPage', 
		'regex' => '/&lt;script.*?mediaId:(?P<embedCode>.*?);/i'),
	'regexEmbedCode' => '@player.js\?mediaId:(?P<embedCode>\d+);.*?acceptable.tv@is'
	// url not available
	);

$embed['break'] = array(
	'name' => 'Break',
	'url' => 'www.break.com',
	'regexUrl' => 'break.com',
	'regexCorrectEmbed' => '<object.*?embed.break.com',
	'regexImageUrl' => '/rel="image_src.*?href="(?P<imageUrl>.*?)"/is',
	'embedHTML' => '<embed src="http://embed.break.com/{embedCode}" type="application/x-shockwave-flash" width="{width}" height="{height}"></embed>',
	'getCodeAction' => array(
		'method' => 'RegexPage', 
		'regex' => '/&lt;embed\ssrc=.*?break\.com\/(?P<embedCode>.*?)&/i'),
	'regexEmbedUrl' => '@break.com.*?</embed>.*?<a href="(?P<embedUrl>.*?)"@is',
	'regexUrlFeed' => 'rss.break.com'
	);

$embed['currenttv'] = array(
	'name' => 'CurrentTv',
	'url' => 'www.current.tv',
	'regexUrl' => 'current.com',
	'regexCorrectEmbed' => 'value="http://current.com',
	'regexImageUrl' => "/'imageUrl',\s'(?P<imageUrl>.*?)'/i",
	'embedHTML' => '<embed src="http://current.com/e/{embedCode}" type="application/x-shockwave-flash" width="{width}" height="{height}" wmode="transparent" allowfullscreen="true"></embed>',
	'getCodeAction' => array(
		'method' => 'RegexURL', 
		'regex' => '@^http://current.com/.*/(?P<embedCode>.*?)_@i'),
	'regexEmbedCode' => '@embed.*?current.com[^\d]*?(?P<embedCode>\d+)"@is',
	'regexUrlFeed' => 'feeds.current.com'
	);
	
$embed['ehow'] = array(
	'name' => 'eHow',
	'url' => 'www.ehow.com',
	'regexUrl' => 'ehow.com/video_',
	'regexCorrectEmbed' => 'ehow.com/flash',
	'embedHTML' => '<embed  id="mediaPlayerContainer" width="{width}" height="{height}" align="TL" flashvars="id={embedCode}&partnerId=3&pwidth={width}&pheight={height}" scale="noscale" allowfullscreen="true" wmode="window" menu="false" loop="false" autostart="false" allowscriptaccesst="sameDomain" quality="high" bgcolor="#000000" name="mediaPlayerContainer" style="" name="mediaPlayerContainer" src="http://www.ehow.com/flash/player.swf" type="application/x-shockwave-flash"></embed>',
	'getCodeAction' => array(
		'method' => 'RegexPage', 
		'regex' => '/flashvars=&quot;id=(?P<embedCode>.*?)&/i'),
	'regexEmbedCode' => '@flashvars.*?id=(?P<embedCode>.*?)&.*?ehow.com@is'
	// rss not found
	);	

$embed['flowgo'] = array(
	'name' => 'Flowgo',
	'url' => 'www.flowgo.com',
	'regexUrl' => 'flowgo.com',
	'regexCorrectEmbed' => '<embed.*?flowgo.com',
	'embedHTML' => '<script src="http://www.flowgo.com/js/embed.js?{embedCode}"></script>',
	'getCodeAction' => array(
		'method' => 'RegexURL', 
		'regex' => '@^http://www.flowgo.com/funny/(?P<embedCode>.*)@i'),
	'regexEmbedUrl' => '@<embed.*?demand_page_url=(?P<embedUrl>.*?)&.*?flowgo.com@is'
	// rss not found
	);

$embed['googlevideo'] = array(
	'name' => 'Google Video',
	'url' => 'videos.google.com',
	'regexUrl' => 'video.google.com',
	'regexCorrectEmbed' => 'video.google.com/googleplayer',
	'regexImageUrl' => "/(image|thumbnail):\s'(?P<imageUrl>.*?)'/is",
	'embedHTML' => '<embed style="width:{width}px; height:{height}px;" id="VideoPlayback" type="application/x-shockwave-flash" src="http://video.google.com/googleplayer.swf?docId={embedCode}&hl=en" flashvars=""></embed>',
	'getCodeAction' => array(
		'method' => 'RegexURL', 
		'regex' => '@http://video.google.com/videoplay\?docid=(?P<embedCode>[-\d]+)@i'),
	'regexEmbedCode' => '@googleplayer\.swf\?docid=(?P<embedCode>[-\d]+)@is',
	'urlByCode' => 'http://video.google.com/videoplay?docid={embedCode}',
	'regexUrlFeed' => 'video.google.com/videofeed'
	);
	
$embed['ifilm'] = array(
	'name' => 'iFilm',
	'url' => 'www.ifilm.com',
	'regexUrl' => 'spike.com/video',
	'regexCorrectEmbed' => '<embed.*?spike.com',
	'regexImageUrl' => '/rel="videothumbnail"\shref="(?P<imageUrl>.*?)"/is',
	'embedHTML' => '<embed width="{width}" height="{height}" src="http://www.spike.com/efp" quality="high" bgcolor="000000" name="efp" align="middle" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" flashvars="flvbaseclip={embedCode}&"></embed>',
	'getCodeAction' => array(
		'method' => 'RegexURL', 
		'regex' => '@^http://www.spike.com/video/.*?(?P<embedCode>\d+)@i'),
	'regexEmbedUrl' => '@spike.com.*?</embed>.*?<a href=\'(?P<embedUrl>.*?)\'@is'
	// rss not found
	);	
	
$embed['metacafe'] = array(
	'name' => 'MetaCafe',
	'url' => 'www.metacafe.com',
	'regexUrl' => 'metacafe.com',
	'regexCorrectEmbed' => '<embed.*?metacafe.com',
	'regexImageUrl' => '/rel="image_src.*?href="(?P<imageUrl>.*?)"/is',
	'embedHTML' => '<embed src="http://www.metacafe.com/fplayer/{embedCode}.swf" width="{width}" height="{height}" wmode="transparent" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash"></embed>',
	'getCodeAction' => array(
		'method' => 'RegexPage', 
		'regex' => '/&lt;embed.*?fplayer\/(?P<embedCode>.*?).swf/i'),
	'regexEmbedUrl' => '@metacafe.com.*?</embed>.*?<a href="(?P<embedUrl>.*?)"@is',
	'regexUrlFeed' => 'metacafe.com/channels'
	);
	
$embed['viddler'] = array(
	'name' => 'Viddler.com',
	'url' => 'www.viddler.com',
	'regexUrl' => 'viddler.com',
	'regexCorrectEmbed' => '<embed.*?viddler.com',
	'regexImageUrl' => '/rel="image_src.*?href="(?P<imageUrl>.*?)"/is',
	'embedHTML' => '<embed src="http://www.viddler.com/player/{embedCode}/" width="{width}" height="{height}" type="application/x-shockwave-flash" allowScriptAccess="always" allowFullScreen="true" name="viddler" ></embed>',
	'getCodeAction' => array(
		'method' => 'RegexPage', 
		'regex' => '@video_src.*?player/(?P<embedCode>[^/]*?)/@is'),
	'regexEmbedCode' => '@<embed.*?viddler.com/player/(?P<embedCode>[^/]*?)/@is',
	'regexUrlFeed' => 'viddler.com'
	);	
	
$embed['yahoovideo'] = array(
	'name' => 'Yahoo!Video',
	'url' => 'videos.yahoo.com',
	'regexUrl' => 'video.yahoo.com',
	'regexCorrectEmbed' => '<embed.*?yahoo.com',
	'regexImageUrl' => '/rel="image_src.*?href="(?P<imageUrl>.*?)"/is',
	'embedHTML' => '<embed src="http://d.yimg.com/static.video.yahoo.com/yep/YV_YEP.swf?ver=2.1.10" type="application/x-shockwave-flash" width="{width}" height="{height}" allowFullScreen="true" flashVars="id={embedCode}&lang=en-us&intl=us"></embed>',
	'getCodeAction' => array(
		'method' => 'RegexURL',
		'regex' => '@^http://.*?yahoo.com/watch/.*?/(?P<embedCode>\d+)@i'),
	'regexEmbedCode' => '@yahoo.com.*?flashvars="id=(?P<embedCode>\d+)&vid=(?P<embedCode2>\d+)&@is',
	'urlByCode' => 'http://video.yahoo.com/watch/{embedCode2}/{embedCode}',
	'regexUrlFeed' => 'video.yahoo.com/rss'
	);	

$embed['bliptv'] = array(
	'name' => 'BlipTv',
	'url' => 'www.blip.tv',
	'regexUrl' => 'blip.tv',
	'regexCorrectEmbed' => '<embed.*?blip.tv',
	'regexImageUrl' => '/rel="image_src.*?href="(?P<imageUrl>.*?)"/is',
	'embedHTML' => '<embed src="http://blip.tv/scripts/flash/showplayer.swf?enablejs=true&file=http%3A%2F%2Fblip%2Etv%2Frss%2Fflash%2F{embedCode}&showplayerpath=http%3A%2F%2Fblip%2Etv%2Fscripts%2Fflash%2Fshowplayer%2Eswf" quality="best" width="{width}" height="{height}" name="showplayer" type="application/x-shockwave-flash"></embed>',
	'getCodeAction' => array(
		'method' => 'RegexPage', 
		'regex' => '/playerSelector.load\((?P<embedCode>.*?),/i'),
	'regexEmbedCode' => '@<embed.*?blip.tv.*?flash%2F(?P<embedCode>\d+)@is',
	'regexUrlFeed' => 'blip.tv/rss'
	);

$embed['brightcove'] = array(
	'name' => 'BrightCove',
	'url' => 'www.brightcove.com',
	'regexUrl' => 'brightcove.tv',
	'regexCorrectEmbed' => '<embed.*?brightcove.tv',
	'regexImageUrl' => '/rel="image_src.*?href="(?P<imageUrl>.*?)"/is',
	'embedHTML' => '<embed src="http://www.brightcove.tv/playerswf" bgcolor="#FFFFFF" flashVars="initVideoId={embedCode}&servicesURL=http://www.brightcove.tv&viewerSecureGatewayURL=https://www.brightcove.tv&cdnURL=http://admin.brightcove.com&autoStart=false" base="http://admin.brightcove.com" name="bcPlayer" width="{width}" height="{height}" allowFullScreen="true" allowScriptAccess="always" seamlesstabbing="false" type="application/x-shockwave-flash" swLiveConnect="true" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash"></embed>',
	'getCodeAction' => array(
		'method' => 'RegexURL',
		'regex' => '@^http://www.brightcove.tv/title.jsp\?title=(?P<embedCode>\d+)@i'),
	'regexEmbedCode' => '@brightcove.tv.*?initVideoId=(?P<embedCode>\d+)&@is',
	'urlByCode' => 'http://www.brightcove.tv/title.jsp?title={embedCode}'
	// rss not found
	);	
	
$embed['collegehumor'] = array(
	'name' => 'CollegeHumor',
	'url' => 'www.collegehumor.com',
	'regexUrl' => 'collegehumor.com/video:',
	'regexCorrectEmbed' => '<object.*?collegehumor.com',
	'regexImageUrl' => '/rel="image_src.*?href="(?P<imageUrl>.*?)"/is',
	'embedHTML' => '<embed type="application/x-shockwave-flash" src="http://www.collegehumor.com/moogaloop/moogaloop.swf?clip_id={embedCode}&fullscreen=1" width="{width}" height="{height}"></embed>',
	'getCodeAction' => array(
		'method' => 'RegexURL',
		'regex' => '@^http://www.collegehumor.com/video:(?P<embedCode>\d+)@i'),
	'regexEmbedCode' => '@collegehumor.com.*?clip_id=(?P<embedCode>\d+)&@is',
	'urlByCode' => 'http://www.collegehumor.com/video:{embedCode}',
	'regexUrlFeed' => 'collegehumor.com.*?rss'
	);		
	
$embed['dailymotion'] = array(
	'name' => 'DailyMotion',
	'url' => 'www.dailymotion.com',
	'regexUrl' => 'dailymotion.com',
	'regexCorrectEmbed' => '<object.*?dailymotion.com',
	'regexImageUrl' => '/rel="image_src.*?href="(?P<imageUrl>.*?)"/is',
	'embedHTML' => '<embed src="http://www.dailymotion.com/swf/{embedCode}" type="application/x-shockwave-flash" width="{width}" height="{height}" allowFullScreen="true" allowScriptAccess="always"></embed>',
	'getCodeAction' => array(
		'method' => 'RegexURL',
		'regex' => '@^http://www.dailymotion.com/video/(?P<embedCode>.*?)_@i'),
	'regexEmbedCode' => '@<object.*?dailymotion.com/swf/(?P<embedCode>.*?)"@is',
	'regexUrlFeed' => 'dailymotion.com/rss'
	);		
	
$embed['expertvillage'] = array(
	'name' => 'ExpertVillage',
	'url' => 'www.expertvillage.com',
	'regexUrl' => 'expertvillage.com',
	'regexCorrectEmbed' => '<embed.*?expertvillage.com',
	'regexImageUrl' => '/DisplayNone\sselected.*?<img\ssrc="(?P<imageUrl>.*?)"/is',
	'embedHTML' => '<embed src="http://www.expertvillage.com/player.swf?flv={embedCode}" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="{width}" height="{height}"></embed>',
	'getCodeAction' => array(
		'method' => 'RegexURL',
		'regex' => '@^http://www.expertvillage.com/video/.*?_(?P<embedCode>.*?)\.@i'),
	'regexEmbedUrl' => '@expertvillage.com.*?</embed>.*?<a href="(?P<embedUrl>.*?)"@is'
	// rss not found
	);		
	
$embed['funnyordie'] = array(
	'name' => 'FunnyOrDie',
	'url' => 'www.funnyordie.com',
	'regexUrl' => 'funnyordie.com',
	'regexCorrectEmbed' => '<embed.*?funnyordie.com',
	'regexImageUrl' => '/rel="image_src.*?href="(?P<imageUrl>.*?)"/is',
	'embedHTML' => '<embed width="{width}" height="{height}" flashvars="key={embedCode}" allowfullscreen="true" quality="high" src="http://player.ordienetworks.com/flash/fodplayer.swf" type="application/x-shockwave-flash"></embed>',
	'getCodeAction' => array(
		'method' => 'RegexURL',
		'regex' => '@^http://www.funnyordie.com/videos/(?P<embedCode>.*)/@i'),
	'regexEmbedUrl' => '@funnyordie.com.*?</embed>.*?<a href="(?P<embedUrl>.*?)"@is',
	'regexUrlFeed' => 'funnyordie.com.*?rss'
	);	
		
	
$embed['madblast'] = array(
	'name' => 'MadBlast',
	'url' => 'www.madblast.com',
	'regexUrl' => 'madblast.com',
	'regexCorrectEmbed' => '<embed.*?madblast.com',
	'regexImageUrl' => '/thumbnail.*?value="(?P<imageUrl>.*?)"/is',
	'embedHTML' => '<script src="http://www.madblast.com/js/embed.js?{embedCode}.html"></script>',
	'getCodeAction' => array(
		'method' => 'RegexURL',
		'regex' => '@^http://www.madblast.com/funny/(?P<embedCode>.*)\.@i'),
	'regexEmbedUrl' => '@<embed.*?demand_page_url=(?P<embedUrl>.*?)&.*?dmdentertainment.com@is'
	// rss not found
	);	
	
$embed['myspace'] = array(
	'name' => 'MySpace Videos',
	'url' => 'videos.myspace.com',
	'regexUrl' => 'myspace',
	'regexCorrectEmbed' => '<embed.*?myspace.com',
	'embedHTML' => '<embed src="http://lads.myspace.com/videos/vplayer.swf" flashvars="m={embedCode}&v=2&type=video" type="application/x-shockwave-flash" width="{width}" height="{height}"></embed>',
	'getCodeAction' => array(
		'method' => 'RegexURL',
		'regex' => '@^http://.*?myspace.*?\.com.*?videoid=(?P<embedCode>.*)@i'),
	'regexEmbedCode' => '@myspace.com.*?flashvars="m=(?P<embedCode>.*?)&@is'
	// rss not found
	);	

$embed['veoh'] = array(
	'name' => 'Veoh',
	'url' => 'www.veoh.com',
	'regexUrl' => 'veoh.com',
	'regexCorrectEmbed' => '<embed.*?veoh.com',
	'regexImageUrl' => '/rel="image_src.*?href="(?P<imageUrl>.*?)"/is',
	'embedHTML' => '<embed src="http://www.veoh.com/videodetails2.swf?permalinkId={embedCode}&id=anonymous&player=videodetailsembedded&videoAutoPlay=0" allowFullScreen="true" width="{width}" height="{height}" bgcolor="#FFFFFF" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer"></embed>',
	'getCodeAction' => array(
		'method' => 'RegexURL',
		'regex' => '@^http://www.veoh.com/videos/(?P<embedCode>.*)@i'),
	'regexEmbedCode' => '@veoh.com.*?permalinkId=(?P<embedCode>.*?)&@is',
	'urlByCode' => 'http://www.veoh.com/videos/{embedCode}'
	// rss not found
	);	
	
$embed['youtube'] = array(
	'name' => 'YouTube',
	'url' => 'www.youtube.com',
	'regexUrl' => 'youtube.com',
	'regexCorrectEmbed' => '<embed.*?youtube.com',
	'embedHTML' => '<embed src="http://www.youtube.com/v/{embedCode}&hl=en" type="application/x-shockwave-flash" wmode="transparent" width="{width}" height="{height}"></embed>',
	'getCodeAction' => array(
		'method' => 'RegexURL',
		'regex' => '@^http://.*?youtube.com/watch\?v=(?P<embedCode>[-_\w\d]*)@i'),
	'regexEmbedCode' => '@<embed.*?youtube.com/v/(?P<embedCode>.*?)&@is',
	'regexUrlFeed' => 'gdata.youtube.com/feeds'
	);
	
$embed['vimeo'] = array(
	'name' => 'Vimeo',
	'url' => 'www.vimeo.com',
	'regexUrl' => 'vimeo.com',
	'regexCorrectEmbed' => '<embed.*?vimeo.com',
	'regexImageUrl' => '/rel="image_src.*?href="(?P<imageUrl>.*?)"/is',
	'embedHTML' => '<embed src="http://vimeo.com/moogaloop.swf?clip_id={embedCode}&amp;server=vimeo.com&amp;show_title=1&amp;show_byline=1&amp;show_portrait=0&amp;color=&amp;fullscreen=1" type="application/x-shockwave-flash" allowfullscreen="true" allowscriptaccess="always" width="{width}" height="{height}"></embed>',
	'getCodeAction' => array(
		'method' => 'RegexURL', 
		'regex' => '@^http://.*?vimeo.com/(?P<embedCode>.*)@i'),
	'regexEmbedCode' => '@vimeo.com.*?clip_id=(?P<embedCode>.*?)&@is',
	'urlByCode' => 'http://www.vimeo.com/{embedCode}',
	'regexUrlFeed' => 'vimeo.com/.*?/rss'
	);	
	
?>