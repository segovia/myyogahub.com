<?



/***************************************************************************

*                            Dolphin Smart Community Builder

*                              -----------------

*     begin                : Mon Mar 23 2006

*     copyright            : (C) 2006 BoonEx Group

*     website              : http://www.boonex.com/

* This file is part of Dolphin - Smart Community Builder

*

* Dolphin is free software. This work is licensed under a Creative Commons Attribution 3.0 License. 

* http://creativecommons.org/licenses/by/3.0/

*

* Dolphin is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;

* without even the implied warranty of  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

* See the Creative Commons Attribution 3.0 License for more details. 

* You should have received a copy of the Creative Commons Attribution 3.0 License along with Dolphin, 

* see license.txt file; if not, write to marketing@boonex.com

***************************************************************************/

require_once( 'inc/header.inc.php' );
require_once( BX_DIRECTORY_PATH_INC . 'design.inc.php' );
require_once( BX_DIRECTORY_PATH_INC . 'images.inc.php' );
require_once( BX_DIRECTORY_PATH_INC . 'utils.inc.php' );
require_once( BX_DIRECTORY_PATH_ROOT . "templates/tmpl_{$tmpl}/scripts/BxTemplProfileView.php" );
require_once ( BX_DIRECTORY_PATH_ROOT . 'phptal/PHPTAL.php');
require_once( BX_DIRECTORY_PATH_INC . 'videoservices.inc.php' );


// --------------- page variables and login


if (isset($_COOKIE['memberID']))
{
	checkCommunityPermissions($_COOKIE['memberID']);
	
	$oProfile = new BxTemplProfileView( $_COOKIE['memberID'] );
	$_page['extra_css'] = $oProfile -> genProfileCSS( $_COOKIE['memberID'] );
}

$_page['name_index'] 	= 58;

$_page['css_name']		= 'profile_customize.css';

$ADMIN = member_auth( 1, false );
$logged['admin'] = $ADMIN;

// Check if moderator logged in.
$moderator = member_auth(3, false);
$logged['moderator'] = $moderator;
// Give moderator all admin rights for this page.
$ADMIN = $ADMIN || $moderator;
//
if (!$ADMIN)
{
	$logged['member'] = member_auth( 0 );
	
	if ( !getParam('enable_customization') )
	{
		$_page['name_index'] = 0;
		$_page_cont[0]['page_main_code'] = '';
		PageCode();
		exit();
	}
	
	$ID = (int)$_COOKIE['memberID'];
}
else
	$ID = (int)$_REQUEST['memberID'];

$mylinks_maxrows = 10;
$mylinksImageName = 'small_folder';

$errorMsg = '';

if (isset($_POST['resetBG'])) // reset background settings
{
	$query = "SELECT `BackgroundFilename` FROM `ProfilesSettings` WHERE `IDMember` = '$ID';";
	$custom_arr = db_arr( $query );

	if ( strlen($custom_arr['BackgroundFilename']) && file_exists($dir['profileBackground'] . $custom_arr['BackgroundFilename']) && is_file($dir['profileBackground'] . $custom_arr['BackgroundFilename']) )
	    @unlink($dir['profileBackground'] . $custom_arr['BackgroundFilename']);

	$query = "UPDATE `ProfilesSettings` 
		SET `BackgroundFilename` = '', `BackgroundColor` = '', `FontColor` = '', `FontSize` = '', `FontFamily` = ''
		WHERE `IDMember`='$ID'";
	db_res( $query );
}
elseif (isset($_POST['resetNF'])) // reset newsfeed settings
{
	$query = "UPDATE `ProfilesSettings` 
		SET `NewsOnProfilePage` = 5
		WHERE `IDMember`='$ID'";
	db_res( $query );
	
	// Clear newsfeed off types for this member
	db_res("DELETE FROM `NewsFeedOff` WHERE ownerid = $ID");	
}
else if ($_POST['save'] || $_POST['submitbylink'] == '1') // save settings for correspond section
{
	$query = "SELECT * FROM `ProfilesSettings` WHERE `IDMember` = '$ID';";
	$custom_arr = db_arr( $query );
	$record_created = $custom_arr['IDMember'] ? 'ok' : '';

// bg image --------------------------------------------------------------------

    if ( $_FILES['bgimg']['name'] && !$_POST['bgdel'] )
    {
	    if ( strlen($custom_arr['BackgroundFilename']) && file_exists($dir['profileBackground'] . $custom_arr['BackgroundFilename']) && is_file($dir['profileBackground'] . $custom_arr['BackgroundFilename']) )
	        @unlink($dir['profileBackground'] . $custom_arr['BackgroundFilename']);

		srand(time());

		$pic_name = $ID . '_bg_' . rand(100, 999);

		if ( !is_int($ext = moveUploadedImage( $_FILES, 'bgimg', $dir['profileBackground'] . $pic_name, '',false) ) )
		{
		    if ( !$record_created )
		    {
		        $query = "INSERT INTO ProfilesSettings (`IDMember`, `BackgroundFilename` ) VALUES ( '$ID', '$pic_name$ext' )";
				$record_created = 'ok';
		    }
		    else
		        $query = "UPDATE ProfilesSettings SET `BackgroundFilename` = '$pic_name$ext', `Status` = 'Approval' WHERE `IDMember` = '$ID'";

		    $res = db_res( $query );
		}
    }

    else if ( $_POST['bgdel'] )
    {
        if ( $custom_arr['BackgroundFilename'] )
		{
	    	if (file_exists($dir['profileBackground'] . $custom_arr['BackgroundFilename'])) unlink($dir['profileBackground'] . $custom_arr['BackgroundFilename']);
			    $query = "UPDATE ProfilesSettings SET `BackgroundFilename` = '' WHERE `IDMember` = '$ID'";
			    $res = db_res( $query );
		}
    }

// bg color --------------------------------------------------------------------

    if ( $_POST['bgcolor'] && $_POST['bgcolor'] != $custom_arr['BackgroundColor'] )
    {
		if ( !$record_created )
		{
		    $query = "INSERT INTO ProfilesSettings (`IDMember`, `BackgroundColor` ) VALUES ( '$ID', '{$_POST['bgcolor']}' )";
		    $record_created = 'ok';
		}
		else
		    $query = "UPDATE ProfilesSettings SET `BackgroundColor` = '{$_POST['bgcolor']}' WHERE `IDMember` = '$ID'";

		$res = db_res( $query );
    }

// font color ------------------------------------------------------------------

    if ( $_POST['fontcolor'] && $_POST['fontcolor'] != $custom_arr['FontColor'] )
    {
		if ( !$record_created )
		{
		    $query = "INSERT INTO ProfilesSettings (`IDMember`, `FontColor` ) VALUES ( '$ID', '{$_POST['fontcolor']}' )";
		    $record_created = 'ok';
		}
		else
		    $query = "UPDATE ProfilesSettings SET `FontColor` = '{$_POST['fontcolor']}' WHERE `IDMember` = '$ID'";

		$res = db_res( $query );
    }

// font size -------------------------------------------------------------------

    if ( $_POST['fontsize'] && $_POST['fontsize'] != $custom_arr['FontSize'] )
    {
		if ( !$record_created )
		{
		    $query = "INSERT INTO ProfilesSettings (`IDMember`, `FontSize` ) VALUES ( '$ID', '{$_POST['fontsize']}' )";
		    $record_created = 'ok';
		}
		else
		    $query = "UPDATE ProfilesSettings SET `FontSize` = '{$_POST['fontsize']}' WHERE `IDMember` = '$ID'";
	
		$res = db_res( $query );
    }

// font family -----------------------------------------------------------------

    if ( $_POST['fontfamily'] && $_POST['fontfamily'] != $custom_arr['FontFamily'] )
    {
		if ( !$record_created )
		{
		    $query = "INSERT INTO ProfilesSettings (`IDMember`, `FontFamily` ) VALUES ( '$ID', '{$_POST['fontfamily']}' )";
		    $record_created = 'ok';
		}
		else
		    $query = "UPDATE ProfilesSettings SET `FontFamily` = '{$_POST['fontfamily']}' WHERE `IDMember` = '$ID'";
	
		$res = db_res( $query );
    }
    
// news-feed -------------------------------------------------------------------

	if ($_POST['newsonprofile'])
	{
		if (!is_array($custom_arr))
	    	$res = db_res("INSERT INTO `ProfilesSettings`
	    		(IDMember, NewsOnProfilePage)
	    		VALUES
	    		($ID, {$_POST['newsonprofile']})");
	    else
			// Update news count on profile page
    		$res = db_res("UPDATE ProfilesSettings SET NewsOnProfilePage = {$_POST['newsonprofile']} WHERE IDMember = $ID");
    
	    // News-Feed types count
	    $newsFeedTypesCount = db_value("SELECT COUNT(id) FROM NewsFeedTypes");
	    $newsFeedTypesOff = '';
	    for ($i = 1; $i <= $newsFeedTypesCount; $i++)
	    {
	    	if (!isset($_POST["newsfeedtype$i"]))
	    	{
	    		if (strlen($newsFeedTypesOff) > 0) $newsFeedTypesOff .= ', ';
	    		$newsFeedTypesOff .= "($ID, $i)";
	    	}
	    }
	    
	    // Clear newsfeed off types for this member
	    db_res("DELETE FROM NewsFeedOff WHERE ownerid = $ID");
	    
	    if (strlen($newsFeedTypesOff) > 0)
	    	db_res("INSERT INTO NewsFeedOff (ownerid, typeid) VALUES $newsFeedTypesOff");
	}
 
// myblog ----------------------------------------------------------------------

	if ($_POST['postsonprofile'])
	{
		$PostsOnProfile = isset($_POST['postsonprofile']) ? $_POST['postsonprofile'] : 2;
		$LinesInSummaryOnProfile = isset($_POST['linesonprofile']) ? 
			$_POST['linesonprofile'] : 3;
		
		if (!is_array($custom_arr))
		{
	    	$res = db_res("INSERT INTO `ProfilesSettings`
				(IDMember, PostsOnProfilePage, LinesInSummaryOnProfile)
				VALUES
				($ID, $PostsOnProfile, $LinesInSummaryOnProfile)"); 
			$custom_arr = db_arr("SELECT * FROM `ProfilesSettings` WHERE `IDMember`=$ID");
		}
		else
	    	$res = db_res("UPDATE ProfilesSettings SET 
	    		`PostsOnProfilePage` = $PostsOnProfile,
	    		`LinesInSummaryOnProfile` = $LinesInSummaryOnProfile
	    		WHERE IDMember = $ID");    	
	}
    
// RSS Blog Feed ---------------------------------------------------------------
	
	if ($_POST['RSSPostsOnProfile'])
	{
		$showRSSOnProfile = isset($_POST['showRSSOnProfile']) ? 1 : 0;
		$showRSSOnBlog = isset($_POST['showRSSOnBlog']) ? 1 : 0;
		$RSSPostsOnProfile = isset($_POST['RSSPostsOnProfile']) ? 
			$_POST['RSSPostsOnProfile'] : 3;
		$RSSPostsOnBlog = isset($_POST['RSSPostsOnBlog']) ? 
			$_POST['RSSPostsOnBlog'] : 3;
		$RSSBlogFeedURL = process_db_input(trim($_POST['RSSBlogFeedURL'])); 
		$RSSFeedHomepage = process_db_input(trim($_POST['RSSFeedHomepage']));
		$RSSTitle = process_db_input($_POST['RSSTitle']);
		$RSSCategoryIconNum = isset($_POST['RSSCategoryIconNum']) ? $_POST['RSSCategoryIconNum'] : '1';
		$RSSCategoryIcon = "{$mylinksImageName}{$RSSCategoryIconNum}.png";
		
		if (!is_array($custom_arr))
		{		
	    	$res = db_res("INSERT INTO `ProfilesSettings`
	    		(IDMember, ShowRSSBlogFeed, ShowRSSOnBlog, RSSBlogFeedURL,
	    		RSSTitle, RSSFeedHomepage, RSSCategoryIcon, RSSPostsOnProfilePage,
	    		RSSPostsOnBlog, RSSLinesInSummary, RSSLinesInSummaryOnBlog)
	    		VALUES
	    		($ID, $showRSSOnProfile, $showRSSOnBlog, '$RSSBlogFeedURL',
	    		'$RSSTitle', '$RSSFeedHomepage', '$RSSCategoryIcon', $RSSPostsOnProfile,
	    		$RSSPostsOnBlog, {$_POST['RSSLinesInSummary']}, {$_POST['RSSLinesInSummaryOnBlog']})");		
		}
	    else
		    $res = db_res("UPDATE `ProfilesSettings` SET 
		    	`ShowRSSBlogFeed` = $showRSSOnProfile, 
		    	`ShowRSSOnBlog` = $showRSSOnBlog,
		    	`RSSBlogFeedURL` = '$RSSBlogFeedURL',
		    	`RSSTitle` = '$RSSTitle',
		    	`RSSFeedHomepage` = '$RSSFeedHomepage',	 
		    	`RSSCategoryIcon` = '$RSSCategoryIcon',   	
		    	`RSSPostsOnProfilePage` = $RSSPostsOnProfile,
				`RSSPostsOnBlog` = $RSSPostsOnBlog,
		    	`RSSLinesInSummary` = {$_POST['RSSLinesInSummary']},
		    	`RSSLinesInSummaryOnBlog` = {$_POST['RSSLinesInSummaryOnBlog']}
		    	WHERE IDMember = $ID");
	}
    	
// mylinks ---------------------------------------------------------------------

	if ($_POST['mylinks_box_title'])
	{
		$mylinksBoxTitle = addslashes($_POST['mylinks_box_title']);
		$showMyLinksOnBlog = isset($_POST['showMyLinksOnBlog']) ? 1 : 0;
		if (!is_array($custom_arr)) // profile setting not exists
			$res = db_res("INSERT INTO `ProfilesSettings`
				(IDMember, MyLinksBoxTitle, ShowMyLinksOnBlog)
				VALUES ($ID, '$mylinksBoxTitle', $showMyLinksOnBlog)");
		else
			$res = db_res("UPDATE ProfilesSettings SET 
				`MyLinksBoxTitle` = '$mylinksBoxTitle',
				`ShowMyLinksOnBlog` = $showMyLinksOnBlog
				WHERE IDMember = $ID");
		
		// Clear all mylinks of this member
		$res = db_res("DELETE FROM MyLinks WHERE owner=$ID");
	
		// Get all posted links
		$myLinks = getPostedLinks($mylinks_maxrows, $mylinksImageName);
		
		// Save links to database
		saveLinks($myLinks, $ID, 'MyLinks', 'owner');		
	}
	
// videos ----------------------------------------------------------------------

	if (isset($_POST['syndVideoUrl']))
	{
		$syndVideoUrl = mysql_escape_string(trim($_POST['syndVideoUrl']));
		$syndVideoPerm = ($_POST['syndVideoPerm']) ? 1 : 0;
		if (!is_array($custom_arr)) // profile setting not exists
			$res = db_res("INSERT INTO `ProfilesSettings`
				(IDMember, synd_video_url, synd_video_perm)
				VALUES ($ID, '$syndVideoUrl', $syndVideoPerm)");
		else
			$res = db_res("UPDATE ProfilesSettings SET 
				synd_video_url = '$syndVideoUrl',
				synd_video_perm = $syndVideoPerm
				WHERE IDMember = $ID");
	}

	// Update member CSS
	if (!$ADMIN)
	$_page['extra_css'] = $oProfile -> genProfileCSS( $_COOKIE['memberID'] );
	
	if ($_POST['submitbylink'] == '1')
		header('location:'.$site['url'].getNickName($ID));
}

// -------------------------------------------------------------------------------------

// ============================================================================== end ==

// -------------------------------------------------------------------------------------


$_page['header'] = _t("_Customize");

$show = (isset($_REQUEST['show'])) ? $_REQUEST['show'] : 'bg'; // show bg settings by default

switch ($show) {
	case 'newsfeed': 
		$_page['header_text'] = 'Customize News-Feed';
		break;
	case 'blog':
		$_page['header_text'] = 'Customize Blog';
		break;
	case 'links':
		$_page['header_text'] = 'Customize Links';
		break;
	case 'videos':
		$_page['header_text'] = 'Video Settings';
		break;		
	case 'bg':
	default:
		$_page['header_text'] = 'Customize Background';
		break;
}

// --------------- page components



$_ni = $_page['name_index'];

$_page_cont[$_ni]['page_main_code'] = PageCompPageMainCode();



// --------------- [END] page components



PageCode();



// --------------- page components functions



/**

 * page code function

 */

function PageCompPageMainCode()
{
	global $ID, $site, $tmpl, $mylinks_maxrows, $mylinksImageName, $show, $_page, $embed;
	
	$query = "SELECT * FROM `ProfilesSettings` WHERE `IDMember` = '$ID'";
	$custom_arr = db_arr( $query );
	
	$mylinksMinImage = 1;
	$mylinksMaxImage = 11;	

	ob_start();

?>

	<script>
		function SelectOption(selectElementName, valueToSelect)
		{
			var selectElements = document.getElementsByTagName('select');
			for (var i = 0; i < selectElements.length; i++)
				if (selectElements[i].name == selectElementName)
					selectElements[i].value = valueToSelect;
		}
	</script>



	<div class="customize_content">

		<form id="cprofile" name="cprofile" enctype="multipart/form-data" 
			action="<?= $_SERVER['PHP_SELF'] ?>?show=<?=$show?>" method="post">

			<input name="memberID" value="<?= $ID ?>" type="hidden" />
			<input name="submitbylink" value="2" type="hidden"/>

			<?php 
			if ($show == 'bg')
				$resetPage = 'resetBG';
			elseif ($show == 'newsfeed')
				$resetPage = 'resetNF';
			else
				$resetPage = '';
			
/*****************************/
			if ($show == 'bg')
			{			
$bgimage = $custom_arr['BackgroundFilename'];
$bgimage = $bgimage ? "<img src=\"{$site['profileBackground']}$bgimage\" alt=\"background\" width=\"110\" height=\"110\">" : '';
$imgContainerHeight = $bgimage ? ' style="height:130px"' : '';

$bgColor = ($custom_arr['BackgroundColor']) ? $custom_arr['BackgroundColor'] : 'default';

if (!$custom_arr['BackgroundColor'] || $custom_arr['BackgroundColor'] == 'default')
{
	$bgcolorStyle = "background-image:url({$site['images']}bgcolor.gif)";
	$defaultColorTextSpan = '<span id="defaultColorText" style="display:block; color:white; float:left; 
		padding-top:15px; padding-left:42px">Default</span>';		
}
else 
{
	if (strpos($custom_arr['BackgroundColor'], '#') === 0)
		$hexColor = substr($custom_arr['BackgroundColor'], 1, 6);
	$bgcolorStyle = "background-color:{$custom_arr['BackgroundColor']}";
	$defaultColorTextSpan = '<span id="defaultColorText" style="display:none; color:white; float:left; 
		padding-top:15px; padding-left:42px">Default</span>';
}	
$bgColorsHint = ($bgimage) ? '<div style="padding-left:80px; margin-top:5px; margin-bottom:10px; color:grey; font-size:10px; width:80%">
	<img src="'. $site['icons'] .'sign.gif" style="float:left; padding-right:5px"/>
	<span>Background colors will not work if you have uploaded an image for your background. You must first delete your image before you can use the color palette.</span>
	</div>' : '';			
			?>
<script src="inc/js/classes/rgbcolor.js"></script>
<script>
	function backgroundSelectColor(color, imagesPath)
	{
		var selectedBgColor = document.getElementById('selectedBgColor');
		var defaultColorText = document.getElementById('defaultColorText');
		if (selectedBgColor)
		{
			if (color == 'default')
			{
				selectedBgColor.style.backgroundImage = 'url("'+imagesPath+'bgcolor.gif")';
				defaultColorText.style.display = 'block';
				document.forms.cprofile.bgcolor.value = 'default';
			}
			else
			{
				selectedBgColor.style.backgroundColor = color;
				selectedBgColor.style.backgroundImage = '';
				defaultColorText.style.display = 'none';
				document.forms.cprofile.bgcolor.value = color;
			}
		}
		
		// Fill hex field by hex code of the selected color
		FillHexField(color);
	}
	
	function FillHexField(colorName)
	{
		if (colorName == 'default')
			document.getElementById('hexcolor').value = '';
		else
		{
			var colorParser = new RGBColor(colorName); 
			document.getElementById('hexcolor').value = colorParser.toHex().substr(1, 6);
		}
	}
	
	function InitHexField()
	{
		var color = document.forms.cprofile.bgcolor.value;
		FillHexField(color);
	}
</script>
			<input name="bgcolor" value="<?= $bgColor ?>" type="hidden" />
			<div class="customize_header_box">
				<b><?= _t('_Customize Profile') ?>:</b>
			</div>
			<div id="profile_bg_color" class="customize_box">
				<table>
					<tr style="vertical-align:top"><td style="width:150px">
					<span class="left_box"><b><?= _t('_Background color') ?></b></span>
					<span style="float:left;margin-top:5px; margin-bottom:2px; clear:left; width:100%">Current color:</span>
					<div id="selectedBgColor" style="<?= $bgcolorStyle ?>; width:135px; height:50px; 
						float:left; clear:left; border: 1px solid black">
						<?= $defaultColorTextSpan ?>
					</div>
					<div style="float:left; clear:left; margin-top:10px; width:100%">
						<a href="javascript:backgroundSelectColor('default', '<?= $site['images'] ?>');">Reset to default color</a>
					</div>
					</td><td style="width:270px">
					<table cellpadding="0" cellspacing="2" id="colorsTable">
                <tr>
                    <td onclick="backgroundSelectColor('green');" style="background-color: green; border:1px solid #00ffff; border-collapse:collapse"/>
                    <td onclick="backgroundSelectColor('lime')" style="background-color: lime"/>
                    <td onclick="backgroundSelectColor('teal')" style="background-color: teal"/>
                    <td onclick="backgroundSelectColor('aqua')" style="background-color: aqua"/>
                    <td onclick="backgroundSelectColor('navy')" style="background-color: navy"/>
                    <td onclick="backgroundSelectColor('blue')" style="background-color: blue"/>
                    <td onclick="backgroundSelectColor('purple')" style="background-color: purple"/>
                    <td onclick="backgroundSelectColor('fuchsia')" style="background-color: fuchsia"/>
                    <td onclick="backgroundSelectColor('maroon')" style="background-color: maroon"/>
                    <td onclick="backgroundSelectColor('red')" style="background-color: red"/>
                    <td onclick="backgroundSelectColor('olive')" style="background-color: olive"/>
                    <td onclick="backgroundSelectColor('yellow')" style="background-color: yellow"/>
                    <td onclick="backgroundSelectColor('white')" style="background-color: white"/>
                    <td onclick="backgroundSelectColor('silver')" style="background-color: silver"/>
                    <td onclick="backgroundSelectColor('gray')" style="background-color: gray"/>
                    <td onclick="backgroundSelectColor('black')" style="background-color: black"/>
                </tr>
                <tr>
                    <td onclick="backgroundSelectColor('darkolivegreen')" style="background-color: darkolivegreen;"/>
                    <td onclick="backgroundSelectColor('darkgreen')" style="background-color: darkgreen"/>
                    <td onclick="backgroundSelectColor('darkslategray')" style="background-color: darkslategray"/>
                    <td onclick="backgroundSelectColor('slategray')" style="background-color: slategray"/>
                    <td onclick="backgroundSelectColor('darkblue')" style="background-color: darkblue"/>
                    <td onclick="backgroundSelectColor('midnightblue')" style="background-color: midnightblue"/>
                    <td onclick="backgroundSelectColor('indigo')" style="background-color: indigo"/>
                    <td onclick="backgroundSelectColor('darkmagenta')" style="background-color: darkmagenta"/>
                    <td onclick="backgroundSelectColor('brown')" style="background-color: brown"/>
                    <td onclick="backgroundSelectColor('darkred')" style="background-color: darkred"/>
                    <td onclick="backgroundSelectColor('sienna')" style="background-color: sienna"/>
                    <td onclick="backgroundSelectColor('saddlebrown')" style="background-color: saddlebrown"/>
                    <td onclick="backgroundSelectColor('darkgoldenrod')" style="background-color: darkgoldenrod"/>
                    <td onclick="backgroundSelectColor('beige')" style="background-color: beige"/>
                    <td onclick="backgroundSelectColor('honeydew')" style="background-color: honeydew"/>
                    <td onclick="backgroundSelectColor('dimgray')" style="background-color: dimgray"/>
                </tr>
                <tr>
                    <td onclick="backgroundSelectColor('olivedrab')" style="background-color: olivedrab"/>
                    <td onclick="backgroundSelectColor('forestgreen')" style="background-color: forestgreen"/>
                    <td onclick="backgroundSelectColor('darkcyan')" style="background-color: darkcyan"/>
                    <td onclick="backgroundSelectColor('lightslategray')" style="background-color: lightslategray"/>
                    <td onclick="backgroundSelectColor('mediumblue')" style="background-color: mediumblue"/>
                    <td onclick="backgroundSelectColor('darkslateblue')" style="background-color: darkslateblue"/>
                    <td onclick="backgroundSelectColor('darkviolet')" style="background-color: darkviolet"/>
                    <td onclick="backgroundSelectColor('mediumvioletred')" style="background-color: mediumvioletred"/>
                    <td onclick="backgroundSelectColor('indianred')" style="background-color: indianred"/>
                    <td onclick="backgroundSelectColor('firebrick')" style="background-color: firebrick"/>
                    <td onclick="backgroundSelectColor('chocolate')" style="background-color: chocolate"/>
                    <td onclick="backgroundSelectColor('peru')" style="background-color: peru"/>
                    <td onclick="backgroundSelectColor('goldenrod')" style="background-color: goldenrod"/>
                    <td onclick="backgroundSelectColor('lightgoldenrodyellow')" style="background-color: lightgoldenrodyellow"/>
                    <td onclick="backgroundSelectColor('mintcream')" style="background-color: mintcream"/>
                    <td onclick="backgroundSelectColor('darkgray')" style="background-color: darkgray"/>
                </tr>
                <tr>
                    <td onclick="backgroundSelectColor('yellowgreen')" style="background-color: yellowgreen"/>
                    <td onclick="backgroundSelectColor('seagreen')" style="background-color: seagreen"/>
                    <td onclick="backgroundSelectColor('cadetblue')" style="background-color: cadetblue"/>
                    <td onclick="backgroundSelectColor('steelblue')" style="background-color: steelblue"/>
                    <td onclick="backgroundSelectColor('royalblue')" style="background-color: royalblue"/>
                    <td onclick="backgroundSelectColor('blueviolet')" style="background-color: blueviolet"/>
                    <td onclick="backgroundSelectColor('darkorchid')" style="background-color: darkorchid"/>
                    <td onclick="backgroundSelectColor('deeppink')" style="background-color: deeppink"/>
                    <td onclick="backgroundSelectColor('rosybrown')" style="background-color: rosybrown"/>
                    <td onclick="backgroundSelectColor('crimson')" style="background-color: crimson"/>
                    <td onclick="backgroundSelectColor('darkorange')" style="background-color: darkorange"/>
                    <td onclick="backgroundSelectColor('burlywood')" style="background-color: burlywood"/>
                    <td onclick="backgroundSelectColor('darkkhaki')" style="background-color: darkkhaki"/>
                    <td onclick="backgroundSelectColor('lightyellow')" style="background-color: lightyellow"/>
                    <td onclick="backgroundSelectColor('azure')" style="background-color: azure"/>
                    <td onclick="backgroundSelectColor('lightgrey')" style="background-color: lightgrey"/>
                </tr>
                <tr>
                    <td onclick="backgroundSelectColor('lawngreen')" style="background-color: lawngreen"/>
                    <td onclick="backgroundSelectColor('mediumseagreen')" style="background-color: mediumseagreen"/>
                    <td onclick="backgroundSelectColor('lightseagreen')" style="background-color: lightseagreen"/>
                    <td onclick="backgroundSelectColor('deepskyblue')" style="background-color: deepskyblue"/>
                    <td onclick="backgroundSelectColor('dodgerblue')" style="background-color: dodgerblue"/>
                    <td onclick="backgroundSelectColor('slateblue')" style="background-color: slateblue"/>
                    <td onclick="backgroundSelectColor('mediumorchid')" style="background-color: mediumorchid"/>
                    <td onclick="backgroundSelectColor('palevioletred')" style="background-color: palevioletred"/>
                    <td onclick="backgroundSelectColor('salmon')" style="background-color: salmon"/>
                    <td onclick="backgroundSelectColor('orangered')" style="background-color: orangered"/>
                    <td onclick="backgroundSelectColor('sandybrown')" style="background-color: sandybrown"/>
                    <td onclick="backgroundSelectColor('tan')" style="background-color: tan"/>
                    <td onclick="backgroundSelectColor('gold')" style="background-color: gold"/>
                    <td onclick="backgroundSelectColor('ivory')" style="background-color: ivory"/>
                    <td onclick="backgroundSelectColor('ghostwhite')" style="background-color: ghostwhite"/>
                    <td onclick="backgroundSelectColor('gainsboro')" style="background-color: gainsboro"/>
                </tr>
                <tr>
                    <td onclick="backgroundSelectColor('chartreuse')" style="background-color: chartreuse"/>
                    <td onclick="backgroundSelectColor('limegreen')" style="background-color: limegreen"/>
                    <td onclick="backgroundSelectColor('mediumaquamarine')" style="background-color: mediumaquamarine"/>
                    <td onclick="backgroundSelectColor('darkturquoise')" style="background-color: darkturquoise"/>
                    <td onclick="backgroundSelectColor('cornflowerblue')" style="background-color: cornflowerblue"/>
                    <td onclick="backgroundSelectColor('mediumslateblue')" style="background-color: mediumslateblue"/>
                    <td onclick="backgroundSelectColor('orchid')" style="background-color: orchid"/>
                    <td onclick="backgroundSelectColor('hotpink')" style="background-color: hotpink"/>
                    <td onclick="backgroundSelectColor('lightcoral')" style="background-color: lightcoral"/>
                    <td onclick="backgroundSelectColor('tomato')" style="background-color: tomato"/>
                    <td onclick="backgroundSelectColor('orange')" style="background-color: orange"/>
                    <td onclick="backgroundSelectColor('bisque')" style="background-color: bisque"/>
                    <td onclick="backgroundSelectColor('khaki')" style="background-color: khaki"/>
                    <td onclick="backgroundSelectColor('cornsilk')" style="background-color: cornsilk"/>
                    <td onclick="backgroundSelectColor('linen')" style="background-color: linen"/>
                    <td onclick="backgroundSelectColor('whitesmoke')" style="background-color: whitesmoke"/>
                </tr>
                <tr>
                    <td onclick="backgroundSelectColor('greenyellow')" style="background-color: greenyellow"/>
                    <td onclick="backgroundSelectColor('darkseagreen')" style="background-color: darkseagreen"/>
                    <td onclick="backgroundSelectColor('turquoise')" style="background-color: turquoise"/>
                    <td onclick="backgroundSelectColor('mediumturquoise')" style="background-color: mediumturquoise"/>
                    <td onclick="backgroundSelectColor('skyblue')" style="background-color: skyblue"/>
                    <td onclick="backgroundSelectColor('mediumpurple')" style="background-color: mediumpurple"/>
                    <td onclick="backgroundSelectColor('violet')" style="background-color: violet"/>
                    <td onclick="backgroundSelectColor('lightpink')" style="background-color: lightpink"/>
                    <td onclick="backgroundSelectColor('darksalmon')" style="background-color: darksalmon"/>
                    <td onclick="backgroundSelectColor('coral')" style="background-color: coral"/>
                    <td onclick="backgroundSelectColor('navajowhite')" style="background-color: navajowhite"/>
                    <td onclick="backgroundSelectColor('blanchedalmond')" style="background-color: blanchedalmond"/>
                    <td onclick="backgroundSelectColor('palegoldenrod')" style="background-color: palegoldenrod"/>
                    <td onclick="backgroundSelectColor('oldlace')" style="background-color: oldlace"/>
                    <td onclick="backgroundSelectColor('seashell')" style="background-color: seashell"/>
                    <td onclick="backgroundSelectColor('ghostwhite')" style="background-color: ghostwhite"/>
                </tr>
                <tr>
                    <td onclick="backgroundSelectColor('palegreen')" style="background-color: palegreen"/>
                    <td onclick="backgroundSelectColor('springgreen')" style="background-color: springgreen"/>
                    <td onclick="backgroundSelectColor('aquamarine')" style="background-color: aquamarine"/>
                    <td onclick="backgroundSelectColor('powderblue')" style="background-color: powderblue"/>
                    <td onclick="backgroundSelectColor('lightskyblue')" style="background-color: lightskyblue"/>
                    <td onclick="backgroundSelectColor('lightsteelblue')" style="background-color: lightsteelblue"/>
                    <td onclick="backgroundSelectColor('plum')" style="background-color: plum"/>
                    <td onclick="backgroundSelectColor('pink')" style="background-color: pink"/>
                    <td onclick="backgroundSelectColor('lightsalmon')" style="background-color: lightsalmon"/>
                    <td onclick="backgroundSelectColor('wheat')" style="background-color: wheat"/>
                    <td onclick="backgroundSelectColor('moccasin')" style="background-color: moccasin"/>
                    <td onclick="backgroundSelectColor('antiquewhite')" style="background-color: antiquewhite"/>
                    <td onclick="backgroundSelectColor('lemonchiffon')" style="background-color: lemonchiffon"/>
                    <td onclick="backgroundSelectColor('floralwhite')" style="background-color: floralwhite"/>
                    <td onclick="backgroundSelectColor('snow')" style="background-color: snow"/>
                    <td onclick="backgroundSelectColor('aliceblue')" style="background-color: aliceblue"/>
                </tr>
                <tr>
                    <td onclick="backgroundSelectColor('lightgreen')" style="background-color: lightgreen"/>
                    <td onclick="backgroundSelectColor('mediumspringgreen')" style="background-color: mediumspringgreen"/>
                    <td onclick="backgroundSelectColor('paleturquoise')" style="background-color: paleturquoise"/>
                    <td onclick="backgroundSelectColor('lightcyan')" style="background-color: lightcyan"/>
                    <td onclick="backgroundSelectColor('lightblue')" style="background-color: lightblue"/>
                    <td onclick="backgroundSelectColor('lavender')" style="background-color: lavender"/>
                    <td onclick="backgroundSelectColor('thistle')" style="background-color: thistle"/>
                    <td onclick="backgroundSelectColor('mistyrose')" style="background-color: mistyrose"/>
                    <td onclick="backgroundSelectColor('peachpuff')" style="background-color: peachpuff"/>
                    <td onclick="backgroundSelectColor('papayawhip')" style="background-color: papayawhip"/>
                    <td span="6"/>
                </tr>
            </table>
					</td><td style="vertical-align:top">
						<b>Advanced Color Input:</b><br/>
						<span style="font-size:10px">Example: #0000FF</span><br/><br/>
						Hex: #<input type="text" id="hexcolor" name="hexcolor" maxlength="6" 
							value="<?= $hexColor ?>" style="text-transform:uppercase; width:50px"/>
						<a href="javascript:void(0);" 
							onclick="
								if (document.forms.cprofile.hexcolor.value == '')
									document.forms.cprofile.hexcolor.value = 'FFFFFF';
								backgroundSelectColor('#' + document.forms.cprofile.hexcolor.value);">use</a>
					</td></tr>
				</table>
				
			</div>
			
			<?= $bgColorsHint ?>

			<div id="profile_bg_image" class="customize_box_pic" <?= $imgContainerHeight ?>>
				<div style="float:left; padding-right:5px"><b><?= _t('_Background picture') ?></b><br /><?= $bgimage ?></div>
				<input type="checkbox" id="bgdel" name="bgdel" /><label for="bgdel"><?= _t('_Delete') ?></label>
				<span class="right_box">
					<input class="no" id="bgimg" name="bgimg" value="upload" type="file" /><br />
				</span>
			</div>
<script>InitHexField();</script>			
			<?php
			}
/**************************************/			
			elseif ($show == 'newsfeed')
			{
// Get news count on profile page
$newsCount = $custom_arr['NewsOnProfilePage'];
if (!$newsCount) $newsCount = 10;

// Get newsfeed types off
$newsFeedTypesOff = fill_assoc_array(db_res("SELECT typeid FROM NewsFeedOff WHERE ownerid = $ID"));
$newsFeedTypesOffArr = array();
foreach ($newsFeedTypesOff as $newsFeedTypeOff)
	$newsFeedTypesOffArr[] = $newsFeedTypeOff['typeid'];

// News-Feed types table
$newsFeedTypes = fill_assoc_array(db_res("SELECT id, title FROM NewsFeedTypes"));
$newsFeedTypesHTML = '';
if (count($newsFeedTypes) > 0)
{
	$newsFeedTypesHTML = '<div style="float:left; clear:left; margin-top:10px; width:100%">
		<table style="width:100%">';	
	foreach ($newsFeedTypes as $newsFeedType)
	{
		$odd = fmod($newsFeedType['id'], 2) != 0;
		if ($odd) $newsFeedTypesHTML .= '<tr>';
		$checkedStr = (in_array($newsFeedType['id'], $newsFeedTypesOffArr)) ? '' : 'checked';
		$newsFeedTypesHTML .= "<td><input type=\"checkbox\" 
			name=\"newsfeedtype{$newsFeedType['id']}\" $checkedStr/>{$newsFeedType['title']}</td>";
		if (!$odd) $newsFeedTypesHTML .= '</tr>';
	}
	$newsFeedTypesHTML .= '</table></div>';
}	
			?>
			<div id="profile_feednews" class="customize_box">
				<b style="float:left">News-Feed</b>
				<span class="selection">
					Show <select name="newsonprofile">
						<option value="3">3</option>
						<option value="5">5</option>
						<option value="10">10</option>
						<option value="15">15</option>
					</select> news on my profile page
				</span>
				<?= $newsFeedTypesHTML ?>
			</div>
<script>SelectOption('newsonprofile', <?= $newsCount ?>);</script>			
			<?php
			}
/***********************************/			
			elseif ($show == 'blog')
			{
// Get posts count on profile page
$postsCount = $custom_arr['PostsOnProfilePage'];
if (!$postsCount) $postsCount = 2;
$RSSpostsCount = $custom_arr['RSSPostsOnProfilePage'];
if (!$RSSpostsCount) $RSSpostsCount = 3;
$RSSpostsOnBlogCount = $custom_arr['RSSPostsOnBlog'];
if (!$RSSpostsOnBlogCount) $RSSpostsOnBlogCount = 3;

// Show RSS Blog Feed
$ShowRSSOnProfileChecked = ($custom_arr['ShowRSSBlogFeed']) ? ' checked' : '';
$ShowRSSOnBlogChecked = ($custom_arr['ShowRSSOnBlog']) ? ' checked' : '';
$RSSCategoryIconName = ($custom_arr['RSSCategoryIcon']) ? $custom_arr['RSSCategoryIcon'] 
	: "{$mylinksImageName}1.png";
$RSSCategoryIcon = "templates/tmpl_$tmpl/images/icons/$RSSCategoryIconName";				
			?>
<script>
	// Save old RSS Feed URL
	var oldRSSURL = '<?= $custom_arr['RSSBlogFeedURL'] ?>';	
	
	function ChangeCategoryIcon(RSSCategoryIconNum)
	{
		var img = document.getElementById('RSSCategoryIcon');
		img.src = <?= "'templates/tmpl_$tmpl/images/icons/$mylinksImageName'" ?> + 
			RSSCategoryIconNum + '.png';
	}
</script>			
			<div id="profile_myblog" class="customize_box" style="height:75px; min-height:40px">
				<b style="float:left">My YogaHub Blog</b>			
				<span class="selection" style="float:left; clear:left; margin-top:10px">			
					Show <select name="postsonprofile">
						<?php 
							for ($i = 1; $i <= 5; $i++)
								echo "<option value=\"$i\">$i</option>";
						?>
					</select> blog posts on my profile
				</span>							
				<span class="selection" style="float:left; clear:left; margin-top:5px">
					Show <select name="linesonprofile">
						<?php 
							for ($i = 0; $i <= 5; $i++)
								echo "<option value=\"$i\">$i</option>";
						?>
					</select> lines of blog snippet text on My Profile page
				</span>				
			</div>		
			<?php
				$showRSSFeedSection = db_value("SELECT `StudioFeatures` FROM `Profiles` WHERE `ID`=$ID");
				if ($showRSSFeedSection)
				{
				?>
			<div id="profile_rssblogfeed" class="customize_box" style="height:500px">
				<b style="float:left">Advanced RSS Blog Feed</b>
				
				<div style="float:right; color:firebrick; font-weight:bold">
					<img style="vertical-align: middle;" src="<?= $site['icons'] ?>small_folder4.png"/>
					Exlusive My YogaHub Feature
				</div>
				<div style="float:left; width:100%; margin-top:10px; margin-bottom:20px">
					The RSS Blog Feed feature allows you to post updated blog entries 
					onto your main profile page and within your My YogaHub blog sidebar 
					directly from an external blog you may have. For example, if you already 
					have a blog with Blogger, Moveable Type, Wordpress, TypePad, Live Journal
					or any other blog that supports RSS (Real Simple Syndication), you can
					continue blogging on your existing platform and have all your blog
					entries show up within your My YogaHub profile pages.
				</div>
				<span style="float:left">Title: 
					<input type="text" name="RSSTitle" maxlength="25" size="25" 
						value="<?= $custom_arr['RSSTitle'] ?>"/></span>
				<span style="float:left; clear:both; width:100%; margin:10px 0">RSS Feed URL: 
					<input type="text" name="RSSBlogFeedURL" size="40" 
						value="<?= $custom_arr['RSSBlogFeedURL'] ?>"/>
					<span style="font-size:11px">E.g. http://www.yogahub.org/feed</span>
				</span>
				<span style="float:left; clear:both; width:100%">Feed Homepage: 
					<input type="text" name="RSSFeedHomepage" size="37" 
						value="<?= $custom_arr['RSSFeedHomepage'] ?>"/>
					<span style="font-size:11px">E.g. http://www.yogahub.org</span>
				</span>	
				<span style="float:left; clear:both; margin:10px 0">
					<img id="RSSCategoryIcon" style="vertical-align:middle" 
						src="<?= $RSSCategoryIcon ?>"/>
					<select name="RSSCategoryIconNum" 
						onchange="ChangeCategoryIcon(this.value);">
					<?php
						for ($i = $mylinksMinImage; $i <= $mylinksMaxImage; $i++)
							echo "<option name='categoryIcon_image$i' value='$i'>$i</option>";
					?>
					</select>
					Choose Category Icon
				</span>		
				<div class="RSSSettings">
					<b style="float:left">Advanced RSS Blog Feed: My Profile</b>
					<span class="selection" style="float:left; margin-top:10px; width:100%">
						<input type="checkbox" name="showRSSOnProfile" <?= $ShowRSSOnProfileChecked ?>/>
						Show 
						<select name="RSSPostsOnProfile" id="RSSPostsOnProfile">
						<?php 
							for ($i = 1; $i <= 5; $i++)
								echo "<option value=\"$i\">$i</option>";
						?>
						</select>
						 RSS blog posts on my profile
					</span>					
					<span class="selection" style="float:left; margin-top:5px; 
						margin-left:25px">
						Show 
						<select name="RSSLinesInSummary">
						<?php 
							for ($i = 0; $i <= 5; $i++)
								echo "<option value=\"$i\">$i</option>";
						?>
						</select>
						 lines of blog snippet text on My Profile page
					</span>
				</div>	
				<div class="RSSSettings">
					<b style="float:left">Advanced RSS Blog Feed: My YogaHub Blog</b>
					<span class="selection" style="float:left; margin-top:10px; width:100%">
						<input type="checkbox" name="showRSSOnBlog" <?= $ShowRSSOnBlogChecked ?>/>
						Show 
						<select name="RSSPostsOnBlog">
						<?php 
							for ($i = 1; $i <= 5; $i++)
								echo "<option value=\"$i\">$i</option>";
						?>
						</select>
						 RSS blog posts on My YogaHub blog sidebar
					</span>					
					<span class="selection" style="float:left; margin-top:5px; 
						margin-left:25px">
						Show 
						<select name="RSSLinesInSummaryOnBlog">
						<?php 
							for ($i = 0; $i <= 5; $i++)
								echo "<option value=\"$i\">$i</option>";
						?>
						</select>
						 lines of blog snippet text on My YogaHub blog sidebar
					</span>					
				</div>	
				<div class="legend">
					<b style="margin-right:10px">Legend:</b>
				<?php
					for ($i = $mylinksMinImage; $i <= $mylinksMaxImage; $i++)
						echo "$i <img src=\"templates/tmpl_$tmpl/images/icons/{$mylinksImageName}$i.png\"
							style=\"margin-right:10px\"/>";
				?>
				</div>
			</div>	
<!-- RSS Only -->	
<script>
// Set RSS Category Icon Number
var RSSCategoryIcon	= document.getElementById('RSSCategoryIcon');
var RSSCategoryIconNumMatches = RSSCategoryIcon.src.match(/(\d+)\./m);
var RSSCategoryIconNum = (RSSCategoryIconNumMatches[1] != '') ? RSSCategoryIconNumMatches[1] : 1;
SelectOption('RSSCategoryIconNum', RSSCategoryIconNum);

// Set RSS Lines in Summary
SelectOption('RSSLinesInSummary', <?= $custom_arr['RSSLinesInSummary'] ?>);
SelectOption('RSSLinesInSummaryOnBlog', <?= $custom_arr['RSSLinesInSummaryOnBlog'] ?>);
SelectOption('RSSPostsOnProfile', <?= $RSSpostsCount ?>);
SelectOption('RSSPostsOnBlog', <?= $RSSpostsOnBlogCount ?>);
</script>	
				<?php
				}
				?>
<script>
	SelectOption('postsonprofile', <?= $postsCount ?>);
	SelectOption('linesonprofile', <?= $custom_arr['LinesInSummaryOnProfile'] ?>);
</script>				
			<?php
			}
/************************************/			
			elseif ($show == 'links')
			{
if (!is_array($custom_arr))	
	$ShowMyLinksOnBlogChecked = ' checked';
else
	$ShowMyLinksOnBlogChecked = ($custom_arr['ShowMyLinksOnBlog']) ? ' checked' : '';

// Get member's mylinks box title
$mylinksBoxTitle = db_value("SELECT MyLinksBoxTitle FROM ProfilesSettings WHERE IDMember = $ID");
if (trim($mylinksBoxTitle) == '')
	$mylinksBoxTitle = _t('_Link to my other sites default');

// Get member's mylinks from database
$savedMyLinks = fill_assoc_array(db_res("SELECT * FROM MyLinks WHERE owner = $ID ORDER BY position, title"));	
	
$mylinks = getLinksMacro($savedMyLinks, $mylinks_maxrows, $mylinksImageName);
			?>
		
            <div id="profile_mylinks" class="customize_box_mylinks">
				<div style="float:left; margin-bottom:10px; width:100%"><b style="float:left">My Links</b>
					<span style="float:right">Box title 
						<input type="text" name="mylinks_box_title" value="<?= $mylinksBoxTitle ?>" /></span>
				</div>
				<div style="float:left; clear:both; margin-bottom:10px; width:100%">
					<input type="checkbox" name="showMyLinksOnBlog" <?= $ShowMyLinksOnBlogChecked ?>/>
					Show links on My YogaHub blog sidebar
				</div>
				
				<?= $mylinks ?>

			</div>
			<?php
			}
/************************************/			
			elseif ($show == 'videos')
			{			
	$_page['extra_js'] .= '<script type="text/javascript" src="inc/js/videoservices.js"></script>';			
	
	// Check is member have Unconfirmed status
	$status = db_value("SELECT `Status` FROM `Profiles` WHERE ID = $ID");
	if ($status == 'Unconfirmed')
	{
	?>
		<div style="color:red">Sorry, you cannot upload video while your account status is unconfirmed.</div>
	<?php 				
	}
	else {
	?>
<script>
	// Save old syndicate video url
	var oldSyndVideoUrl = '<?= $custom_arr['synd_video_url'] ?>';	
	var embed = new Array();
<?php 
$i = 0;
foreach ($embed as $embedKey => $embedData)
{
	$embedName = $embed[$embedKey]['name'];
	$regexUrlFeed = (isset($embed[$embedKey]['regexUrlFeed'])) ?
		$embed[$embedKey]['regexUrlFeed'] : '';
	echo "embed[$i] = new Array(2);";
	echo "embed[$i]['name'] = '$embedName';";
	echo "embed[$i]['regexUrlFeed'] = '$regexUrlFeed';";
	$i++;
}
?>
</script>
<div class="syndVideoFeed">
	<div class="customizeSubheader">Syndicate an existing video feed</div>
	<div>Url (RSS 2.0, Atom): <input type="text" name="syndVideoUrl"
		class="syndVideoUrl" value="<?= $custom_arr['synd_video_url'] ?>"
		onkeyup="checkSyndVideoService()"/>&nbsp; 
		<span id="syndVideoService"/>
	</div>
	<div class="hint">If specified, new videos will be fetched from feed and added to your videos automatically.
	Keep empty if you prefer to post videos manually.</div><br/>
	<div class="customizeSubheader">Video Permissions</div>
	<input name="syndVideoPerm" type="radio" value="0" 
		<?php if (!$custom_arr['synd_video_perm']) echo 'checked="1"';?>/> public<br/>
	<input name="syndVideoPerm" type="radio" value="1" 
		<?php if ($custom_arr['synd_video_perm']) echo 'checked="1"';?>/> friends only
</div>
<script>checkSyndVideoService();</script>
			<?php
			}
		}			
		?>

			<div class="customize_footer_box">
				<input class="button" name="save" value="<?= _t('_Save Changes') ?>" 
					type="submit" style="width: 100px;" onclick="return checkFormSubmit();"/>
					
			<?php 
				if ($resetPage)
					echo '&nbsp;<input class="button" name="'.$resetPage.'" value="' .
						_t('_Reset') . '" type="submit" style="width: 60px;" />';
			?>
				<br /><br />
				<a href="javascript:submitCProfileForm();"
					style="font-weight: bold;">Save and Return to My Profile</a>
			</div>

		</form>

	</div>
	<script>
		var xmlhttp;
		
		function CheckRSSURL()
		{
			var isShowBlog = <?= ($show == 'blog') ? 1 : 0 ?>;
			var isAdvRSS = document.getElementById('RSSPostsOnProfile');
			if (isShowBlog != 1 || !isAdvRSS) return true;
			
			// Check is show RSS Blog Feed and RSS URL not empty
			var showRSSOnProfile = document.cprofile.showRSSOnProfile.checked;
			var showRSSOnBlog = document.cprofile.showRSSOnBlog.checked;
			if (showRSSOnProfile || showRSSOnBlog)
			{
				var RSSURL = document.cprofile.RSSBlogFeedURL.value;
				var RSSFeedHomepage = document.cprofile.RSSFeedHomepage.value;
				if (RSSURL == '')
				{
					alert('Please specify RSS Feed URL');
					return false;
				}
				else if (RSSFeedHomepage == '')
				{
					alert('Please specify Feed Homepage');
					return false;
				}				
				else if (oldRSSURL != RSSURL) // Check is correct RSS URL
					return loadXMLDoc(RSSURL, 'simplepie/rss_validate.php');
				else
					return true;
			}
			else
				return true;
		}

		function checkSyndVideoUrl()
		{
			if (!document.cprofile.syndVideoUrl) return true;
			var syndVideoUrl = document.cprofile.syndVideoUrl.value;
			if (!syndVideoUrl || syndVideoUrl == oldSyndVideoUrl) return true;
			return loadXMLDoc(syndVideoUrl, 'simplepie/rss_validate.php');
		}

		function checkFormSubmit()
		{
			return CheckRSSURL() && checkSyndVideoUrl();
		}
			
		function submitCProfileForm()
		{
			if (checkFormSubmit())
			{
				document.cprofile.submitbylink.value = '1';
				document.cprofile.submit();
			}
		}
	</script>
<?
	$ret = ob_get_contents();

	ob_end_clean();

	return $ret;
}
?>